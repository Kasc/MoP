DELETE FROM `rbac_permissions` WHERE `id` = 844;
INSERT INTO `rbac_permissions` (`id`, `name`) VALUES (844, 'Command: .debug neargraveyard');

DELETE FROM `rbac_linked_permissions` WHERE `id` = 196 AND `linkedId` = 844;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`) VALUES (196, 844);
