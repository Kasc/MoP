--
DELETE FROM `rbac_permissions` WHERE `id` IN (840,841,842,843);
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES (840,"Command: npc evade"), (841,"Command: pet level"), (842,"Command: server shutdown force"), (843,"Command: server restart force");

DELETE FROM `rbac_linked_permissions` WHERE `linkedId` IN (840,841,842,843);
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES (196,840),(196,841),(196,842),(196,843);
