DROP TABLE IF EXISTS `creature`;
CREATE TABLE `creature` (
    `ID` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `Type` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `ItemID1` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `ItemID2` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `ItemID3` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `Mount` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `DisplayID1` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `DisplayID2` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `DisplayID3` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `DisplayID4` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `DisplayIDProbability1` FLOAT NOT NULL DEFAULT '0',
    `DisplayIDProbability2` FLOAT NOT NULL DEFAULT '0',
    `DisplayIDProbability3` FLOAT NOT NULL DEFAULT '0',
    `DisplayIDProbability4` FLOAT NOT NULL DEFAULT '0',
    `Name` TEXT NOT NULL,
    `FemaleName` TEXT NOT NULL,
    `SubName` TEXT NOT NULL,
    `FemaleSubName` TEXT NOT NULL,
    `Rank` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `InhabitType` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
    `VerifiedBuild` SMALLINT(5) NOT NULL DEFAULT '0',
    PRIMARY KEY (`ID`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;