ALTER TABLE `characters`
  DROP `instance_mode_mask`,
  ADD `dungeonDifficulty` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' AFTER `instance_id`,
  ADD `raidDifficulty` TINYINT(3) UNSIGNED NOT NULL DEFAULT '14' AFTER `dungeonDifficulty`;

ALTER TABLE `groups`
  CHANGE `difficulty` `difficulty` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' AFTER `groupType`,
  CHANGE `raiddifficulty` `raidDifficulty` TINYINT(3) UNSIGNED NOT NULL DEFAULT '14' AFTER `difficulty`;