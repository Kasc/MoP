--
-- Table structure for table `character_aura_effect`
--

DROP TABLE IF EXISTS `character_aura_effect`;
CREATE TABLE `character_aura_effect` (
  `guid` BIGINT(20) UNSIGNED NOT NULL COMMENT 'Global Unique Identifier',
  `casterGuid` BIGINT(20) NOT NULL COMMENT 'Full Global Unique Identifier' ,
  `itemGuid` BIGINT(20) NOT NULL,
  `spell` INT(10) UNSIGNED NOT NULL ,
  `effectMask` INT(10) UNSIGNED NOT NULL,
  `effectIndex` TINYINT(3) UNSIGNED NOT NULL,
  `amount` INT(11) NOT NULL DEFAULT 0,
  `baseAmount` INT(11) NOT NULL DEFAULT 0,
PRIMARY KEY (`guid`,`casterGuid`,`itemGuid`,`spell`,`effectMask`,`effectIndex`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Player System';

--
-- Table structure for table `pet_aura_effect`
--

DROP TABLE IF EXISTS `pet_aura_effect`;
CREATE TABLE `pet_aura_effect` (
  `guid` INT(10) UNSIGNED NOT NULL COMMENT 'Global Unique Identifier',
  `casterGuid` BIGINT(20) NOT NULL COMMENT 'Full Global Unique Identifier' ,
  `spell` INT(10) UNSIGNED NOT NULL,
  `effectMask` INT(10) UNSIGNED NOT NULL,
  `effectIndex` TINYINT(3) UNSIGNED NOT NULL,
  `amount` INT(11) NOT NULL DEFAULT 0,
  `baseAmount` INT(11) NOT NULL DEFAULT 0,
PRIMARY KEY (`guid`,`casterGuid`,`spell`,`effectMask`,`effectIndex`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Pet System';

TRUNCATE `character_aura`;
ALTER TABLE `character_aura`
  CHANGE `caster_guid` `casterGuid` BIGINT(20) NOT NULL COMMENT 'Full Global Unique Identifier',
  CHANGE `item_guid` `itemGuid` BIGINT(20) NOT NULL,
  CHANGE `spell` `spell` INT(10) UNSIGNED NOT NULL,
  CHANGE `effect_mask` `effectMask` INT(10) UNSIGNED NOT NULL,
  CHANGE `recalculate_mask` `recalculateMask` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  CHANGE `stackcount` `stackCount` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  DROP `amount0`,
  DROP `amount1`,
  DROP `amount2`,
  DROP `base_amount0`,
  DROP `base_amount1`,
  DROP `base_amount2`,
  CHANGE `maxDuration` `maxDuration` INT(11) NOT NULL DEFAULT 0,
  CHANGE `remainTime` `remainTime` INT(11) NOT NULL DEFAULT 0,
  CHANGE `remainCharges` `remainCharges` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0;

TRUNCATE `pet_aura`;
ALTER TABLE `pet_aura`
  CHANGE `caster_guid` `casterGuid` BIGINT(20) NOT NULL COMMENT 'Full Global Unique Identifier',
  CHANGE `spell` `spell` INT(10) UNSIGNED NOT NULL,
  CHANGE `effect_mask` `effectMask` INT(10) UNSIGNED NOT NULL,
  CHANGE `recalculate_mask` `recalculateMask` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  CHANGE `stackcount` `stackCount` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  DROP `amount0`,
  DROP `amount1`,
  DROP `amount2`,
  DROP `base_amount0`,
  DROP `base_amount1`,
  DROP `base_amount2`,
  CHANGE `maxDuration` `maxDuration` INT(11) NOT NULL DEFAULT 0,
  CHANGE `remainTime` `remainTime` INT(11) NOT NULL DEFAULT 0,
  CHANGE `remainCharges` `remainCharges` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0;
