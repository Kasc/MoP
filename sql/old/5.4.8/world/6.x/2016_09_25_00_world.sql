DELETE FROM `command` WHERE `name`='debug send playscene';
DELETE FROM `command` WHERE `name` IN ('scene','scene debug','scene play','scene play package','scene cancel','list scenes');
INSERT INTO `command` (`name`,`permission`,`help`) VALUES
('scene', 845, ''),
('scene debug', 846, 'Syntax: .scene debug\nToggle debug mode for scenes. In debug mode GM will be notified in chat when scenes start/stop/trigger event'),
('scene play', 847, 'Syntax: .scene play #sceneId\nPlays scene with id for targeted player'),
('scene play package', 848, 'Syntax: .scene play package #scenePackageId #playbackFlags\nPlays scene with package id and playback flags for targeted player'),
('scene cancel', 849, 'Syntax: .scene cancel #scenePackageId\nCancels scene with package id for targeted player'),
('list scenes', 850, 'Syntax: .list scenes\nList of all active scenes for targeted character.');

CREATE TABLE IF NOT EXISTS `scene_template` (
  `SceneId` INT(10) UNSIGNED NOT NULL,
  `Flags` INT(10) UNSIGNED NOT NULL DEFAULT '16',
  `ScriptPackageID` INT(10) UNSIGNED NOT NULL,
  `ScriptName` CHAR(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`SceneId`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

DELETE FROM `trinity_string` WHERE `entry` IN ( 5062, 5063, 5064, 5065, 5066, 5067, 5068, 5069 );
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
( 5062, 'Scene debugging turned on.' ),
( 5063, 'Scene debugging turned off.' ),
( 5064, 'Scene : started (instance : %u - package : %u - flags : %u)' ),
( 5065, 'Scene : triggered (instance : %u - event : "%s")' ),
( 5066, 'Scene : canceled (instance : %u)' ),
( 5067, 'Scene : completed (instance : %u)' ),
( 5068, 'Player have %u active(s) scene(s)' ),
( 5069, 'ScenePackageId : %u - SceneInstanceId : %u' );
