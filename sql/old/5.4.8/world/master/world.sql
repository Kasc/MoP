--
UPDATE `gameobject_template` SET `AIName`='SmartGameObjectAI' WHERE `entry` IN (190549,194115,194555,202443);
DELETE FROM `spell_script_names` WHERE `ScriptName` IN ('spell_sha_lava_burst', 'spell_sha_path_of_flames_spread','spell_sha_ancestral_guidance','spell_sha_ancestral_guidance_heal');
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(51505,'spell_sha_lava_burst'),
(210621,'spell_sha_path_of_flames_spread'),
(108281,'spell_sha_ancestral_guidance'),
(114911,'spell_sha_ancestral_guidance_heal');
--
UPDATE `quest_template` SET `RewardBonusMoney`= 60, -- 60 copper
 `RewardChoiceItemID1`= 159, `RewardChoiceItemQuantity1`= 5, -- Refreshing Spring Water
 `RewardChoiceItemID2`= 4536, `RewardChoiceItemQuantity2`= 5, -- Shiny Red Apple
 `RewardFactionValue1`= 2, `RewardFactionOverride1`= 0 -- column values swapped
WHERE `ID`= 8350;
--
DELETE FROM `creature` WHERE `guid` = 250025 AND `id` =6491;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseId`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `VerifiedBuild`) VALUES
(250025, 6491, 0, 1497, 1497, 169, 1, 0, 0, 1819.41, 219.233, 60.0732, 0.337883, 60, 0, 0, 4120, 0, 0, 0, 0, 0, 0);
--
