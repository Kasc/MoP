DELETE FROM `smart_scripts` WHERE `entryorguid`=39157 AND `id`=3;
DELETE FROM `smart_scripts` WHERE `entryorguid`=39072;
UPDATE `creature_template` SET `AIName`='', `ScriptName`='npc_najtess' WHERE `entry`=39072;

DELETE FROM `conditions` WHERE `SourceEntry`=58178 AND `SourceTypeOrReferenceId`=13;
INSERT INTO `conditions` VALUES
(13,1,58178,0,0,31,0,3,39157,0,0,0,0,'','58178 CSA Dummy Effect (25 yards) targets Lost bloodtalon hatcling');
