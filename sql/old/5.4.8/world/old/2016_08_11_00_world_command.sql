DELETE FROM `command` WHERE `permission`=793;
INSERT INTO `command` VALUES
('guild setflag',793,'Syntax: .guild setflag "$GuildName" #Flag #Apply\r\n\r\Set/Unset guild flags #Flag to guild with $GuildName.\r\n\r\Guild name must in quotes.');

DELETE FROM `trinity_string` WHERE `entry`=1193;
INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(1193,'Guild flags changed succesfully.');
