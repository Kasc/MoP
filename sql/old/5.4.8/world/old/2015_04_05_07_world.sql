DROP TABLE IF EXISTS `gameobject_addon`;
CREATE TABLE `gameobject_addon` (
  `guid` INT(20) UNSIGNED NOT NULL DEFAULT '0',
  `invisibilityType` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `invisibilityValue` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
