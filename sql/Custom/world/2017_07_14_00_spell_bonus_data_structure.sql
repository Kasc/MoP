DROP TABLE IF EXISTS `spell_bonus_data`;
CREATE TABLE `spell_bonus_data` (
   `spell_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
   `effect_index` INT(11) UNSIGNED NOT NULL DEFAULT '0',
   `spell_bonus` FLOAT NOT NULL DEFAULT '0',
   `ap_bonus` FLOAT NOT NULL DEFAULT '0',
   PRIMARY KEY (`spell_id`,`effect_index`)
 ) ENGINE=INNODB DEFAULT CHARSET=utf8;