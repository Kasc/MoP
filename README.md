Sources of unreleased project Aurora, based on SkyFire with tons of merges from TrinityCore.
Do what you want with it sheeple, if you're not a twat then give credit where credit is due in your changelogs, if you don't then fuck you but we don't really care



## Supported Client Version
**MoP 5.4.8 Build 18414**

[![build status](https://git.siof.pl/tod/WoWMoP/badges/master/build.svg)](https://git.siof.pl/tod/WoWMoP/commits/master)

## Client Patch
[SkyFire-Community-Tools](https://github.com/ProjectSkyfire/SkyFire-Community-Tools).

## Requirements
+ Platform: Linux, Windows or Mac
+ Processor with SSE2 support
+ ACE = 5.8.3 (included for Windows)
+ MySQL = 5.1.0 (included for Windows)
+ CMake = 2.8.11.2 / 2.8.9 (Windows / Linux)
+ OpenSSL = 0.9.8o
+ GCC = 4.7.2 (Linux only)
+ MS Visual Studio = 14 (2015.3) (Windows only)

## Install
Detailed installation guides are available in the wiki for

[Windows](http://wiki.projectskyfire.org/index.php?title=Installation_Windows),
[Linux](http://wiki.projectskyfire.org/index.php?title=Installation_Linux) and
[Mac OSX](http://wiki.projectskyfire.org/index.php?title=Installation_Mac_OS_X).

## Copyright
License: GPL 3.0

Read file [COPYING](COPYING.md)

## Authors &amp; Contributors
Read file [THANKS](THANKS.md)

## To-Do List
Read File [TO-DO](TODO.md)

## Links
Forum [http://www.projectskyfire.org](http://www.projectskyfire.org)

Database [http://www.projectskyfire.org/index.php?/files/](http://www.projectskyfire.org/index.php?/files/)

Wiki [http://wiki.projectskyfire.org](http://wiki.projectskyfire.org)
