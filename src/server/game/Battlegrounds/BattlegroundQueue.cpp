/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "RatedInfo.h"
#include "RatedMgr.h"
#include "BattlegroundMgr.h"
#include "BattlegroundQueue.h"
#include "Chat.h"
#include "GameTime.h"
#include "Group.h"
#include "Log.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Player.h"

/*********************************************************/
/***            BATTLEGROUND QUEUE SYSTEM              ***/
/*********************************************************/

BattlegroundQueue::BattlegroundQueue()
{
    for (uint32 i = 0; i < BG_TEAMS_COUNT; ++i)
    {
        for (uint32 j = 0; j < MAX_BATTLEGROUND_BRACKETS; ++j)
        {
            m_SumOfWaitTimes[i][j] = 0;
            m_WaitTimeLastPlayer[i][j] = 0;
            for (uint32 k = 0; k < COUNT_OF_PLAYERS_TO_AVERAGE_WAIT_TIME; ++k)
                m_WaitTimes[i][j][k] = 0;
        }
    }
}

BattlegroundQueue::~BattlegroundQueue()
{
    m_events.KillAllEvents(false);
    m_QueuedPlayers.clear();

    for (int i = 0; i < MAX_BATTLEGROUND_BRACKETS; ++i)
    {
        for (uint32 j = 0; j < BG_QUEUE_GROUP_TYPES_COUNT; ++j)
        {
            for (GroupsQueueType::iterator itr = m_QueuedGroups[i][j].begin(); itr!= m_QueuedGroups[i][j].end(); ++itr)
                delete (*itr);

            m_QueuedGroups[i][j].clear();
        }
    }
}

/*********************************************************/
/***      BATTLEGROUND QUEUE SELECTION POOLS           ***/
/*********************************************************/

void BattlegroundQueue::SelectionPool::Init()
{
    SelectedGroups.clear();
    PlayerCount = 0;
}

bool BattlegroundQueue::SelectionPool::KickGroup(uint32 size, uint32 minSize)
{
    uint32 currentSize = GetPlayerCount();

    if (!currentSize || currentSize <= minSize)
        return false;

    uint32 maxSizeToKick = currentSize - minSize;

    bool found = false;

    GroupsQueueType::iterator groupToKick = SelectedGroups.begin();
    for (GroupsQueueType::iterator itr = groupToKick; itr != SelectedGroups.end(); ++itr)
    {
        uint32 playerSize = (*itr)->players.size();

        if (playerSize > maxSizeToKick)
            continue;

        if (abs((int32)(playerSize - size)) <= 1)
        {
            groupToKick = itr;
            found = true;
            break;
        }
        else if ((*itr)->players.size() >= (*groupToKick)->players.size())
        {
            groupToKick = itr;
            found = true;
        }
    }

    if (!found)
        return false;

    GroupQueueInfo* ginfo = *groupToKick;
    SelectedGroups.remove(ginfo);
    PlayerCount -= ginfo->players.size();

    return true;
}

void BattlegroundQueue::SelectionPool::KickGroup(GroupQueueInfo* ginfo)
{
    if (GetPlayerCount())
    {
        SelectedGroups.remove(ginfo);
        PlayerCount -= ginfo->players.size();
    }
}

bool BattlegroundQueue::SelectionPool::AddGroup(GroupQueueInfo* ginfo, uint32 desiredCount)
{
    if (!ginfo->IsInvitedToBGInstanceGUID && desiredCount >= PlayerCount + ginfo->players.size())
    {
        SelectedGroups.push_back(ginfo);
        PlayerCount += ginfo->players.size();
        return true;
    }

    if (PlayerCount < desiredCount)
        return true;

    return false;
}

/*********************************************************/
/***               BATTLEGROUND QUEUES                 ***/
/*********************************************************/

GroupQueueInfo* BattlegroundQueue::AddGroup(Player* leader, Group* grp, BattlegroundTypeId bgTypeId, PvPDifficultyEntry const*  bracketEntry, RatedType ratedType, bool IsRated, bool isPremade, uint32 arenaRating, uint32 matchmakerRating, bool IsRandomBGQueue)
{
    BattlegroundBracketId bracketId = bracketEntry->GetBracketId();

    GroupQueueInfo* ginfo                   = new GroupQueueInfo;
    ginfo->bgTypeId                         = bgTypeId;
    ginfo->ratedType                        = ratedType;
    ginfo->IsRated                          = IsRated;
    ginfo->IsInvitedToBGInstanceGUID        = 0;
    ginfo->JoinTime                         = GameTime::GetGameTimeMS();
    ginfo->removeInviteTime                 = 0;
    ginfo->leaderGUID                       = leader->GetGUID();
    ginfo->team                             = leader->GetTeam();
    ginfo->teamRating                       = arenaRating;
    ginfo->teamMatchmakerRating             = matchmakerRating;
    ginfo->OpponentsTeamRating              = 0;
    ginfo->OpponentsTeamMatchmakerRating    = 0;
    ginfo->Role                             = BG_ROLE_DPS;
    ginfo->IsRandomBGQueue                  = IsRandomBGQueue;

    ginfo->players.clear();

    uint32 index = BG_QUEUE_PREMADE_ALLIANCE;

    if (!IsRated && !isPremade)
        index += BG_TEAMS_COUNT;

    if (ginfo->team == HORDE)
        index++;

    TC_LOG_DEBUG("bg.battleground", "Adding Group to BattlegroundQueue bgTypeId : %u, bracket_id : %u, index : %u", bgTypeId, bracketId, index);

    uint32 lastOnlineTime = GameTime::GetGameTimeMS();

    if (IsRated && sWorld->getBoolConfig(CONFIG_ARENA_QUEUE_ANNOUNCER_ENABLE))
        sWorld->SendWorldText(LANG_ARENA_QUEUE_ANNOUNCE_WORLD_JOIN, leader->GetName().c_str(), ginfo->ratedType, ginfo->ratedType, ginfo->teamRating);

    if (grp)
    {
        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* member = itr->GetSource();
            if (!member)
                continue;
            PlayerQueueInfo& pl_info = m_QueuedPlayers[member->GetGUID()];
            pl_info.LastOnlineTime   = lastOnlineTime;
            pl_info.GroupInfo        = ginfo;
            ginfo->players[member->GetGUID()]  = &pl_info;
        }
    }
    else
    {
        PlayerQueueInfo& pl_info = m_QueuedPlayers[leader->GetGUID()];
        pl_info.LastOnlineTime   = lastOnlineTime;
        pl_info.GroupInfo        = ginfo;
        ginfo->players[leader->GetGUID()]  = &pl_info;
    }

    {
        m_QueuedGroups[bracketId][index].push_back(ginfo);

        if (!IsRated && !isPremade && sWorld->getBoolConfig(CONFIG_BATTLEGROUND_QUEUE_ANNOUNCER_ENABLE))
        {
            if (Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(ginfo->bgTypeId))
            {
                char const* bgName = bg->GetName();
                uint32 MinPlayers = bg->GetMinPlayersPerTeam();
                uint32 qHorde = 0;
                uint32 qAlliance = 0;
                uint32 q_min_level = bracketEntry->MinLevel;
                uint32 q_max_level = bracketEntry->MaxLevel;
                GroupsQueueType::const_iterator itr;
                for (itr = m_QueuedGroups[bracketId][BG_QUEUE_NORMAL_ALLIANCE].begin(); itr != m_QueuedGroups[bracketId][BG_QUEUE_NORMAL_ALLIANCE].end(); ++itr)
                    if (!(*itr)->IsInvitedToBGInstanceGUID)
                        qAlliance += (*itr)->players.size();
                for (itr = m_QueuedGroups[bracketId][BG_QUEUE_NORMAL_HORDE].begin(); itr != m_QueuedGroups[bracketId][BG_QUEUE_NORMAL_HORDE].end(); ++itr)
                    if (!(*itr)->IsInvitedToBGInstanceGUID)
                        qHorde += (*itr)->players.size();

                if (sWorld->getBoolConfig(CONFIG_BATTLEGROUND_QUEUE_ANNOUNCER_PLAYERONLY))
                    ChatHandler(leader->GetSession()).PSendSysMessage(LANG_BG_QUEUE_ANNOUNCE_SELF, bgName, q_min_level, q_max_level, qAlliance, (MinPlayers > qAlliance) ? MinPlayers - qAlliance : (uint32)0, qHorde, (MinPlayers > qHorde) ? MinPlayers - qHorde : (uint32)0);
                else
                    sWorld->SendWorldText(LANG_BG_QUEUE_ANNOUNCE_WORLD, bgName, q_min_level, q_max_level, qAlliance, (MinPlayers > qAlliance) ? MinPlayers - qAlliance : (uint32)0, qHorde, (MinPlayers > qHorde) ? MinPlayers - qHorde : (uint32)0);
            }
        }
    }

    return ginfo;
}

void BattlegroundQueue::PlayerInvitedToBGUpdateAverageWaitTime(GroupQueueInfo* ginfo, BattlegroundBracketId bracket_id)
{
    uint32 timeInQueue = getMSTimeDiff(ginfo->JoinTime, GameTime::GetGameTimeMS());
    uint8 team_index = TEAM_ALLIANCE;
    if (!ginfo->ratedType)
    {
        if (ginfo->team == HORDE)
            team_index = TEAM_HORDE;
    }
    else
    {
        if (ginfo->IsRated)
            team_index = TEAM_HORDE;
    }

    uint32* lastPlayerAddedPointer = &(m_WaitTimeLastPlayer[team_index][bracket_id]);
    m_SumOfWaitTimes[team_index][bracket_id] -= m_WaitTimes[team_index][bracket_id][(*lastPlayerAddedPointer)];
    m_WaitTimes[team_index][bracket_id][(*lastPlayerAddedPointer)] = timeInQueue;
    m_SumOfWaitTimes[team_index][bracket_id] += timeInQueue;
    (*lastPlayerAddedPointer)++;
    (*lastPlayerAddedPointer) %= COUNT_OF_PLAYERS_TO_AVERAGE_WAIT_TIME;
}

uint32 BattlegroundQueue::GetAverageQueueWaitTime(GroupQueueInfo* ginfo, BattlegroundBracketId bracket_id) const
{
    uint8 team_index = TEAM_ALLIANCE;
    if (!ginfo->ratedType)
    {
        if (ginfo->team == HORDE)
            team_index = TEAM_HORDE;
    }
    else
    {
        if (ginfo->IsRated)
            team_index = TEAM_HORDE;
    }
    if (m_WaitTimes[team_index][bracket_id][COUNT_OF_PLAYERS_TO_AVERAGE_WAIT_TIME - 1])
        return (m_SumOfWaitTimes[team_index][bracket_id] / COUNT_OF_PLAYERS_TO_AVERAGE_WAIT_TIME);
    else
        return 0;
}

//remove player from queue and from group info, if group info is empty then remove it too
void BattlegroundQueue::RemovePlayer(ObjectGuid guid, bool decreaseInvitedCount)
{
    int32 bracket_id = -1;
    QueuedPlayersMap::iterator itr;

    itr = m_QueuedPlayers.find(guid);
    if (itr == m_QueuedPlayers.end())
    {
        std::string playerName = "Unknown";
        if (Player* player = ObjectAccessor::FindPlayer(guid))
            playerName = player->GetName();
        TC_LOG_DEBUG("bg.battleground", "BattlegroundQueue: couldn't find player %s (GUID: %u)", playerName.c_str(), guid.GetCounter());
        return;
    }

    GroupQueueInfo* group = itr->second.GroupInfo;
    GroupsQueueType::iterator group_itr;
    uint32 index = (group->team == HORDE) ? BG_QUEUE_PREMADE_HORDE : BG_QUEUE_PREMADE_ALLIANCE;

    for (int32 bracket_id_tmp = MAX_BATTLEGROUND_BRACKETS - 1; bracket_id_tmp >= 0 && bracket_id == -1; --bracket_id_tmp)
    {
        for (uint32 j = index; j < BG_QUEUE_GROUP_TYPES_COUNT; j += BG_TEAMS_COUNT)
        {
            GroupsQueueType::iterator k = m_QueuedGroups[bracket_id_tmp][j].begin();
            for (; k != m_QueuedGroups[bracket_id_tmp][j].end(); ++k)
            {
                if ((*k) == group)
                {
                    bracket_id = bracket_id_tmp;
                    group_itr = k;
                    index = j;
                    break;
                }
            }
        }
    }

    if (bracket_id == -1)
    {
        TC_LOG_ERROR("bg.battleground", "BattlegroundQueue: ERROR Cannot find groupinfo for player GUID: %u", guid.GetCounter());
        return;
    }
    TC_LOG_DEBUG("bg.battleground", "BattlegroundQueue: Removing player GUID %u, from bracket_id %u", guid.GetCounter(), (uint32)bracket_id);

    std::map<ObjectGuid, PlayerQueueInfo*>::iterator pitr = group->players.find(guid);
    if (pitr != group->players.end())
        group->players.erase(pitr);

    if (decreaseInvitedCount && group->IsInvitedToBGInstanceGUID)
        if (Battleground* bg = sBattlegroundMgr->GetBattleground(group->IsInvitedToBGInstanceGUID, group->bgTypeId))
            bg->DecreaseInvitedCount(group->team);

    m_QueuedPlayers.erase(itr);

    // announce to world if arena/rated players left queue for rated match, show only once
    if (group->ratedType && group->IsRated && group->players.empty() && sWorld->getBoolConfig(CONFIG_ARENA_QUEUE_ANNOUNCER_ENABLE))
        if (Player* player = ObjectAccessor::FindPlayer(guid))
            sWorld->SendWorldText(LANG_ARENA_QUEUE_ANNOUNCE_WORLD_EXIT, player->GetName().c_str(), group->ratedType, group->ratedType, group->teamRating);

    if (group->IsInvitedToBGInstanceGUID && group->IsRated && decreaseInvitedCount)
    {
        TC_LOG_DEBUG("bg.battleground", "UPDATING memberLost's personal arena rating for %u by opponents rating: %u", guid.ToString().c_str(), group->OpponentsTeamRating);

        int16 personalRatingChange, matchmakerRatingChange;
        RatedInfo* rInfo = nullptr;

        if (Player* player = ObjectAccessor::FindPlayer(guid))
        {
            rInfo = sRatedMgr->GetRatedInfo(player->GetGUID());
            rInfo->UpdateStats(group->ratedType, group->OpponentsTeamMatchmakerRating, personalRatingChange, matchmakerRatingChange, false, false);
        }
        else
        {
            rInfo = sRatedMgr->GetRatedInfo(guid);
            rInfo->UpdateStats(group->ratedType, group->OpponentsTeamMatchmakerRating, personalRatingChange, matchmakerRatingChange, false, true);
        }
    }

    if (group->players.empty())
    {
        m_QueuedGroups[bracket_id][index].erase(group_itr);
        delete group;
        return;
    }

    if (!group->IsInvitedToBGInstanceGUID && group->IsRated)
    {
        if (Player* plr2 = ObjectAccessor::FindConnectedPlayer(group->players.begin()->first))
        {
            Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(group->bgTypeId);
            BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(plr2->GetOriginalBattlegroundTypeId(), group->ratedType);
            uint32 queueSlot = plr2->GetBattlegroundQueueIndex(bgQueueTypeId);

            plr2->RemoveBattlegroundQueueId(bgQueueTypeId);

            sBattlegroundMgr->BuildBattlegroundNoneStatus(plr2, queueSlot, group->JoinTime, group->ratedType);
        }

        RemovePlayer(group->players.begin()->first, decreaseInvitedCount);
    }
}

//returns true when player pl_guid is in queue and is invited to bgInstanceGuid
bool BattlegroundQueue::IsPlayerInvited(ObjectGuid pl_guid, const uint32 bgInstanceGuid, const uint32 removeTime)
{
    QueuedPlayersMap::const_iterator qItr = m_QueuedPlayers.find(pl_guid);
    return (qItr != m_QueuedPlayers.end()  && qItr->second.GroupInfo->IsInvitedToBGInstanceGUID == bgInstanceGuid && qItr->second.GroupInfo->removeInviteTime == removeTime);
}

GroupQueueInfo* BattlegroundQueue::GetPlayerGroupInfoData(ObjectGuid guid)
{
    QueuedPlayersMap::const_iterator qItr = m_QueuedPlayers.find(guid);
    if (qItr == m_QueuedPlayers.end())
        return nullptr;

    return qItr->second.GroupInfo;
}

bool BattlegroundQueue::InviteGroupToBG(GroupQueueInfo* ginfo, Battleground* bg, uint32 side)
{
    if (side)
        ginfo->team = side;

    if (!ginfo->IsInvitedToBGInstanceGUID)
    {
        ginfo->IsInvitedToBGInstanceGUID = bg->GetInstanceID();
        BattlegroundTypeId bgTypeId = bg->GetTypeID();
        BattlegroundBracketId bracket_id = bg->GetBracketId();

        BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, bg->GetRatedType());

        bool GroupKickedFromSelectionGroups = false;

        ginfo->removeInviteTime = GameTime::GetGameTimeMS() + INVITE_ACCEPT_WAIT_TIME;

        // loop through the players
        for (std::map<ObjectGuid, PlayerQueueInfo*>::iterator itr = ginfo->players.begin(); itr != ginfo->players.end(); ++itr)
        {
            Player* player = ObjectAccessor::FindConnectedPlayer(itr->first);
            if (!player)
                continue;

            BattlegroundQueueTypeId bgQueueTypeId2 = BattlegroundMgr::BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), bg->GetRatedType());

            uint32 queueSlot = player->GetBattlegroundQueueIndex(bgQueueTypeId2);

            if (player->IsSuspendedQueueForBattlegroundQueueIndex(queueSlot))
                continue;

            PlayerInvitedToBGUpdateAverageWaitTime(ginfo, bracket_id);
            bg->IncreaseInvitedCount(ginfo->team);
            player->SetInviteForBattlegroundQueueType(bgQueueTypeId2, ginfo->IsInvitedToBGInstanceGUID);
            BGQueueInviteEvent* inviteEvent = new BGQueueInviteEvent(player->GetGUID(), ginfo->IsInvitedToBGInstanceGUID, bgTypeId, ginfo->ratedType, ginfo->removeInviteTime, ginfo->Role);
            m_events.AddEvent(inviteEvent, m_events.CalculateTime(INVITATION_REMIND_TIME));
            BGQueueRemoveEvent* removeEvent = new BGQueueRemoveEvent(player->GetGUID(), ginfo->IsInvitedToBGInstanceGUID, bgQueueTypeId, bgQueueTypeId2, ginfo->removeInviteTime);
            m_events.AddEvent(removeEvent, m_events.CalculateTime(INVITE_ACCEPT_WAIT_TIME));

            TC_LOG_DEBUG("bg.battleground", "Battleground: invited player %s (%u) to BG instance %u queueindex %u bgtype %u", player->GetName().c_str(), player->GetGUID().GetCounter(), bg->GetInstanceID(), queueSlot, bg->GetTypeID());

            sBattlegroundMgr->BuildBattlegroundWaitJoinStatus(bg, player, queueSlot, INVITE_ACCEPT_WAIT_TIME, ginfo->JoinTime, ginfo->ratedType, ginfo->Role);

            player->SuspendOtherQueues(true);

            // if rbg queue, remove from all the rest, except the present one
            if (!bg->IsWarGame() && bgQueueTypeId2 == BATTLEGROUND_QUEUE_RB)
            {
                player->SetRandomBGForPlayer(bg->GetTypeID());

                for (uint8 i = BATTLEGROUND_QUEUE_AV; i < BATTLEGROUND_QUEUE_RB; ++i)
                {
                    bgQueueTypeId = BattlegroundQueueTypeId(i);
                    bgTypeId = sBattlegroundMgr->BGTemplateId(bgQueueTypeId);

                    if (bgTypeId == bg->GetTypeID())
                        continue;

                    BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
                    bgQueue.RemovePlayer(player->GetGUID(), true);

                    if (!GroupKickedFromSelectionGroups)
                    {
                        uint8 team = ginfo->team == ALLIANCE ? TEAM_ALLIANCE : TEAM_HORDE;
                        bgQueue.m_SelectionPools[team].KickGroup(ginfo);
                    }
                }

                GroupKickedFromSelectionGroups = true;
            }
        }

        return true;
    }

    return false;
}

void BattlegroundQueue::FillPlayersToBG(Battleground* bg, BattlegroundBracketId bracket_id)
{
    if (m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].empty() && m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].empty() &&
        m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE].empty() && m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_HORDE].empty())
        return;

    m_SelectionPools[TEAM_ALLIANCE].Init();
    m_SelectionPools[TEAM_HORDE].Init();

    int32 hordeFree = bg->GetFreeSlotsForTeam(HORDE);
    int32 aliFree   = bg->GetFreeSlotsForTeam(ALLIANCE);

    GroupsQueueType::const_iterator Ali_itr = m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE].begin();
    uint32 aliCount = m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE].size();
    uint32 aliIndex = 0;
    for (; aliIndex < aliCount && m_SelectionPools[TEAM_ALLIANCE].AddGroup((*Ali_itr), aliFree); aliIndex++)
        ++Ali_itr;

    GroupsQueueType::const_iterator Horde_itr = m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_HORDE].begin();
    uint32 hordeCount = m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_HORDE].size();
    uint32 hordeIndex = 0;
    for (; hordeIndex < hordeCount && m_SelectionPools[TEAM_HORDE].AddGroup((*Horde_itr), hordeFree); hordeIndex++)
        ++Horde_itr;

    for (uint32 i = 0; i < BG_TEAMS_COUNT; i++)
        for (GroupsQueueType::const_iterator citr = m_SelectionPools[i].SelectedGroups.begin(); citr != m_SelectionPools[i].SelectedGroups.end(); ++citr)
            InviteGroupToBG((*citr), bg, (*citr)->team);
}

bool BattlegroundQueue::CheckPremadeMatch(BattlegroundBracketId bracket_id, uint32 MaxPlayersPerTeam)
{
    GroupsQueueType::const_iterator ali_group, horde_group;

    bool AlliancePremadeFound = false;
    bool HordePremadeFound = false;

    // groups queue for specific bg first, then randoms
    for (ali_group = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].begin(); ali_group != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].end(); ++ali_group)
        if (!(*ali_group)->IsInvitedToBGInstanceGUID && !(*ali_group)->teamMatchmakerRating && !(*ali_group)->IsRandomBGQueue)
        {
            AlliancePremadeFound = true;
            break;
        }

    if (!AlliancePremadeFound)
        for (ali_group = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].begin(); ali_group != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].end(); ++ali_group)
            if (!(*ali_group)->IsInvitedToBGInstanceGUID && !(*ali_group)->teamMatchmakerRating && (*ali_group)->IsRandomBGQueue)
            {
                AlliancePremadeFound = true;
                break;
            }

    // groups queue for specific bg first, then randoms
    for (horde_group = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].begin(); horde_group != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].end(); ++horde_group)
        if (!(*horde_group)->IsInvitedToBGInstanceGUID && !(*horde_group)->teamMatchmakerRating && !(*horde_group)->IsRandomBGQueue)
        {
            HordePremadeFound = true;
            break;
        }

    if (!HordePremadeFound)
        for (horde_group = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].begin(); horde_group != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].end(); ++horde_group)
            if (!(*horde_group)->IsInvitedToBGInstanceGUID && !(*horde_group)->teamMatchmakerRating && (*horde_group)->IsRandomBGQueue)
            {
                HordePremadeFound = true;
                break;
            }

    if (AlliancePremadeFound && HordePremadeFound)
    {
        m_SelectionPools[TEAM_ALLIANCE].AddGroup((*ali_group), MaxPlayersPerTeam);
        m_SelectionPools[TEAM_HORDE].AddGroup((*horde_group), MaxPlayersPerTeam);

        for (uint8 i = 0; i < BG_TEAMS_COUNT; i++)
        {
            bool EnoughPlayers = false;

            // groups queue for specific bg first, then randoms
            for (auto itr : m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE + i])
                if (!itr->IsInvitedToBGInstanceGUID && !itr->IsRandomBGQueue)
                    if (!m_SelectionPools[i].AddGroup(itr, MaxPlayersPerTeam))
                    {
                        EnoughPlayers = true;
                        break;
                    }

            if (!EnoughPlayers)
                for (auto itr : m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE + i])
                    if (!itr->IsInvitedToBGInstanceGUID && itr->IsRandomBGQueue)
                        if (!m_SelectionPools[i].AddGroup(itr, MaxPlayersPerTeam))
                            break;
        }

        return true;
    }

    // now check if we can move group from Premade queue to normal queue (timer has expired) or group size lowered!!
    // this could be 2 cycles but i'm checking only first team in queue - it can cause problem -
    // if first is invited to BG and seconds timer expired, but we can ignore it, because players have only 80 seconds to click to enter bg
    // and when they click or after 80 seconds the queue info is removed from queue
    uint32 time_before = GameTime::GetGameTimeMS() - sWorld->getIntConfig(CONFIG_BATTLEGROUND_PREMADE_GROUP_WAIT_FOR_MATCH);
    for (uint8 i = 0; i < BG_TEAMS_COUNT; i++)
        if (!m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE + i].empty())
        {
            GroupsQueueType::iterator itr = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE + i].begin();
            if (!(*itr)->IsInvitedToBGInstanceGUID && (*itr)->JoinTime < time_before)
            {
                m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE + i].push_front((*itr));
                m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE + i].erase(itr);
            }
        }

    return false;
}

bool BattlegroundQueue::CheckNormalMatch(BattlegroundBracketId bracket_id, uint32 minPlayers, uint32 maxPlayers)
{
    for (uint8 i = 0; i < BG_TEAMS_COUNT; i++)
    {
        bool EnoughPlayers = false;

        // groups queue for specific bg first, then randoms
        for (auto itr : m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE + i])
            if (!itr->IsInvitedToBGInstanceGUID && !itr->IsRandomBGQueue)
                if (!m_SelectionPools[i].AddGroup(itr, maxPlayers))
                {
                    EnoughPlayers = true;
                    break;
                }

        if (!EnoughPlayers)
            for (auto itr : m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE + i])
                if (!itr->IsInvitedToBGInstanceGUID && itr->IsRandomBGQueue)
                    if (!m_SelectionPools[i].AddGroup(itr, maxPlayers))
                        break;
    }

    if (sBattlegroundMgr->IsTesting() && (m_SelectionPools[TEAM_ALLIANCE].GetPlayerCount() || m_SelectionPools[TEAM_HORDE].GetPlayerCount()))
        return true;

    return m_SelectionPools[TEAM_ALLIANCE].GetPlayerCount() >= minPlayers && m_SelectionPools[TEAM_HORDE].GetPlayerCount() >= minPlayers;
}

void BattlegroundQueue::CheckWarGameQueue(Battleground* bg, PvPDifficultyEntry const* bracketEntry, uint32 WarGameID)
{
    BattlegroundTypeId bgTypeId = bg->GetTypeID();
    BattlegroundBracketId bracket_id = bracketEntry->GetBracketId();

    if (m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].empty() && m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].empty())
        return;

    GroupsQueueType::iterator itr_teams[BG_TEAMS_COUNT];
    uint8 team = TEAM_ALLIANCE;
    bool found = false;

    for (GroupsQueueType::iterator itr = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].begin(); itr != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].end(); ++itr)
        if (!(*itr)->IsInvitedToBGInstanceGUID && !(*itr)->IsRated && (*itr)->teamMatchmakerRating == WarGameID)
        {
            itr_teams[team] = itr;
            ++team;

            if (team > TEAM_HORDE)
            {
                found = true;
                break;
            }
        }

    if (!found)
        for (GroupsQueueType::iterator itr = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].begin(); itr != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].end(); ++itr)
            if (!(*itr)->IsInvitedToBGInstanceGUID && !(*itr)->IsRated && (*itr)->teamMatchmakerRating == WarGameID)
            {
                itr_teams[team] = itr;

                ++team;

                if (team > TEAM_HORDE)
                {
                    found = true;
                    break;
                }
            }

    if (!found)
        return;

    GroupQueueInfo* aTeam = *itr_teams[TEAM_ALLIANCE];
    GroupQueueInfo* hTeam = *itr_teams[TEAM_HORDE];

    uint32 teamSize = aTeam->players.size();
    uint32 hTeamSize = hTeam->players.size();

    if (teamSize < hTeamSize)
        teamSize = hTeamSize;

    Battleground* WarGame = sBattlegroundMgr->CreateNewBattleground(bgTypeId, bracketEntry, aTeam->ratedType, false, true, teamSize);
    if (!WarGame)
    {
        TC_LOG_ERROR("bg.battleground", "BattlegroundQueue::Update couldn't create WarGame!");
        return;
    }

    if (aTeam->team != ALLIANCE)
    {
        m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].push_front(aTeam);
        m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].erase(itr_teams[TEAM_ALLIANCE]);
    }

    if (hTeam->team != HORDE)
    {
        m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].push_front(hTeam);
        m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].erase(itr_teams[TEAM_HORDE]);
    }

    InviteGroupToBG(aTeam, WarGame, ALLIANCE);
    InviteGroupToBG(hTeam, WarGame, HORDE);

    TC_LOG_DEBUG("bg.battleground", "Starting WarGame!");

    WarGame->StartBattleground();
}

void BattlegroundQueue::UpdateEvents(uint32 diff)
{
    m_events.Update(diff);
}

void BattlegroundQueue::BalanceQueues(uint32 minPlayers)
{
    int32 allianceCount = int32(m_SelectionPools[TEAM_ALLIANCE].GetPlayerCount());
    int32 hordeCount = int32(m_SelectionPools[TEAM_HORDE].GetPlayerCount());

    while (abs(allianceCount - hordeCount) > 1 && (allianceCount > 0 || hordeCount > 0))
    {
        if (allianceCount > hordeCount)
        {
            if (!m_SelectionPools[TEAM_ALLIANCE].KickGroup(allianceCount - hordeCount, minPlayers))
                break;
        }
        else
        {
            if (!m_SelectionPools[TEAM_HORDE].KickGroup(hordeCount - allianceCount, minPlayers))
                break;
        }

        allianceCount = int32(m_SelectionPools[TEAM_ALLIANCE].GetPlayerCount());
        hordeCount = int32(m_SelectionPools[TEAM_HORDE].GetPlayerCount());
    }
}

/*
this method is called when group is inserted, or player / group is removed from BG Queue - there is only one player's status changed, so we don't use while (true) cycles to invite whole queue
it must be called after fully adding the members of a group to ensure group joining
should be called from Battleground::RemovePlayer function in some cases
*/
uint32 BattlegroundQueue::BattlegroundQueueUpdate(Battleground* bg_template, PvPDifficultyEntry const* bracketEntry, RatedType ratedType, bool IsRated, uint32 arenaRating)
{
    BattlegroundTypeId bgTypeId = bg_template->GetTypeID();
    BattlegroundBracketId bracket_id = bracketEntry->GetBracketId();

    m_SelectionPools[TEAM_ALLIANCE].Init();
    m_SelectionPools[TEAM_HORDE].Init();

    if (m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].empty() && m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].empty() &&
        m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_ALLIANCE].empty() && m_QueuedGroups[bracket_id][BG_QUEUE_NORMAL_HORDE].empty())
        return 0;

    uint32 MinPlayersPerTeam = bg_template->GetMinPlayersPerTeam();
    uint32 MaxPlayersPerTeam = bg_template->GetMaxPlayersPerTeam();

    if (bg_template->IsArena())
    {
        MaxPlayersPerTeam = ratedType;
        MinPlayersPerTeam = sBattlegroundMgr->IsArenaTesting() ? 1 : ratedType;
    }
    else if (sBattlegroundMgr->IsTesting())
        MinPlayersPerTeam = 1;

    if (!IsRated)
    {
        if (arenaRating > 0)
        {
            CheckWarGameQueue(bg_template, bracketEntry, arenaRating);
            return 0;
        }

        if (CheckPremadeMatch(bracket_id, MaxPlayersPerTeam))
            return MaxPlayersPerTeam;

        if (CheckNormalMatch(bracket_id, MinPlayersPerTeam, MaxPlayersPerTeam))
            return MaxPlayersPerTeam;

        return 0;
    }

    GroupsQueueType::iterator itr_teams[BG_TEAMS_COUNT];

    uint8 found = 0;

    uint32 currentTime = GameTime::GetGameTimeMS();
    uint32 discardTime = sBattlegroundMgr->GetRatingDiscardTimer(); // 5 min

    uint32 arenaMinRating = 0;
    uint32 arenaMaxRating = 0; // team rating bounds from external loop (itr0)

    uint32 timedRatingChange = 0; // scales with waiting time
    uint32 matchmakerRating = 0;

    do
    {
        GroupQueueInfo* team1 = nullptr;
        GroupQueueInfo* team2 = nullptr;

        found = 0;

        GroupsQueueType::iterator itr0, itr1, itr2;

        // for every team search another team with longest queue time and proper rating
        for (uint8 i = BG_QUEUE_PREMADE_ALLIANCE; i < BG_QUEUE_NORMAL_ALLIANCE; ++i)
        {
            itr0 = m_QueuedGroups[bracket_id][i].begin();
            for (; itr0 != m_QueuedGroups[bracket_id][i].end(); ++itr0)
            {
                team1 = nullptr;
                team2 = nullptr;

                if ((*itr0)->IsInvitedToBGInstanceGUID)
                    continue;

                if (!(*itr0)->IsRated)
                    continue;

                // every 5 seconds add 7.5 rating
                timedRatingChange = (currentTime - (*itr0)->JoinTime) / 1000 * 1.5;
                matchmakerRating = (*itr0)->teamMatchmakerRating;

                // for teams above 2500 2v2 rating and 2000 3v3/5v5 rating lower matchmakerRating
                if (ratedType == RATED_TYPE_2v2)
                {
                    if (matchmakerRating > 2500)
                        matchmakerRating = 2500;
                }
                else if (matchmakerRating > 2000)
                    matchmakerRating = 2000;

                // arenaMaxRating calculated from original MMR
                arenaMaxRating = (*itr0)->teamMatchmakerRating + sBattlegroundMgr->GetMaxRatingDifference() + timedRatingChange;

                // arenaMinRating calculated from lowered MMR
                if (matchmakerRating >= sBattlegroundMgr->GetMaxRatingDifference() + timedRatingChange)
                    arenaMinRating = matchmakerRating - sBattlegroundMgr->GetMaxRatingDifference() - timedRatingChange;
                else
                    arenaMinRating = 0;

                // search for team that match calculated min/max ratings
                itr1 = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].begin();
                for (; itr1 != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].end(); ++itr1)
                {
                    if ((*itr1)->IsInvitedToBGInstanceGUID || !(*itr1)->IsRated || (*itr1)->leaderGUID == (*itr0)->leaderGUID)
                        continue;

                    if (arenaMinRating <= (*itr1)->teamMatchmakerRating && arenaMaxRating >= (*itr1)->teamMatchmakerRating || (*itr0)->JoinTime + discardTime < currentTime)
                    {
                        team1 = *itr1;
                        break;
                    }
                }

                itr2 = m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].begin();
                for (; itr2 != m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].end(); ++itr2)
                {
                    if ((*itr2)->IsInvitedToBGInstanceGUID || !(*itr2)->IsRated || (*itr2)->leaderGUID == (*itr0)->leaderGUID)
                        continue;

                    if (arenaMinRating <= (*itr2)->teamMatchmakerRating && arenaMaxRating >= (*itr2)->teamMatchmakerRating || (*itr0)->JoinTime + discardTime < currentTime)
                    {
                        team2 = *itr2;
                        break;
                    }
                }

                // if no matching teams found, continue
                if (!team1 && !team2)
                    continue;

                // if found alliance & horde, select team with longer waiting time
                if (team1 && team2)
                {
                    if (team1->JoinTime < team2->JoinTime)
                        itr_teams[found++] = itr1;
                    else itr_teams[found++] = itr2;
                }
                else if (team1)
                    itr_teams[found++] = itr1;
                else
                    itr_teams[found++] = itr2;

                itr_teams[found++] = itr0;
                break;
            }

            // we have 2 matching teams, now arena should start
            if (found == 2)
                break;
        }

        if (found == 2)
        {
            GroupQueueInfo* aTeam = *itr_teams[TEAM_ALLIANCE];
            GroupQueueInfo* hTeam = *itr_teams[TEAM_HORDE];

            Battleground* arena = sBattlegroundMgr->CreateNewBattleground(bgTypeId, bracketEntry, ratedType, true, false);
            if (!arena)
            {
                TC_LOG_ERROR("bg.battleground", "BattlegroundQueue::Update couldn't create arena instance for rated arena match!");
                return 0;
            }

            aTeam->OpponentsTeamRating = hTeam->teamRating;
            hTeam->OpponentsTeamRating = aTeam->teamRating;
            aTeam->OpponentsTeamMatchmakerRating = hTeam->teamMatchmakerRating;
            hTeam->OpponentsTeamMatchmakerRating = aTeam->teamMatchmakerRating;

            TC_LOG_DEBUG("bg.battleground", "setting oposite teamrating to %u", aTeam->OpponentsTeamRating);
            TC_LOG_DEBUG("bg.battleground", "setting oposite teamrating to %u", hTeam->OpponentsTeamRating);

            if (aTeam->team != ALLIANCE)
            {
                m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].push_front(aTeam);
                m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].erase(itr_teams[TEAM_ALLIANCE]);
            }

            if (hTeam->team != HORDE)
            {
                m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_HORDE].push_front(hTeam);
                m_QueuedGroups[bracket_id][BG_QUEUE_PREMADE_ALLIANCE].erase(itr_teams[TEAM_HORDE]);
            }

            arena->SetTeamMatchmakerRating(ALLIANCE, aTeam->teamMatchmakerRating);
            arena->SetTeamMatchmakerRating(HORDE, hTeam->teamMatchmakerRating);

            InviteGroupToBG(aTeam, arena, ALLIANCE);
            InviteGroupToBG(hTeam, arena, HORDE);

            TC_LOG_DEBUG("bg.battleground", "Starting rated arena match!");

            arena->StartBattleground();
        }
    }
    while (found == 2);

    return 0;
}

void BattlegroundQueue::StartBattleground(BattlegroundTypeId bgTypeId, BattlegroundBracketId bracketId, bool IsWarGame)
{
    Battleground* bg_template = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeId);
    if (!bg_template)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground: StartBattleground: bg template not found for %u", bgTypeId);
        return;
    }

    uint32 MinPlayersPerTeam = bg_template->GetMinPlayersPerTeam();
    uint32 MaxPlayersPerTeam = bg_template->GetMaxPlayersPerTeam();

    if (sBattlegroundMgr->IsTesting())
    {
        MinPlayersPerTeam = 1;
        if (m_SelectionPools[TEAM_ALLIANCE].GetPlayerCount() < MinPlayersPerTeam && m_SelectionPools[TEAM_HORDE].GetPlayerCount() < MinPlayersPerTeam)
            return;
    }
    else
    {
        if (m_SelectionPools[TEAM_ALLIANCE].GetPlayerCount() < MinPlayersPerTeam || m_SelectionPools[TEAM_HORDE].GetPlayerCount() < MinPlayersPerTeam)
            return;
    }

    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketById(bg_template->GetMapId(), bracketId);
    if (!bracketEntry)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground: StartBattleground: bg bracket entry not found for map %u bracket id %u", bg_template->GetMapId(), bracketId);
        return;
    }

    Battleground* bg = sBattlegroundMgr->CreateNewBattleground(bgTypeId, bracketEntry, RATED_TYPE_NOT_RATED, false, IsWarGame);
    if (!bg)
    {
        TC_LOG_ERROR("bg.battleground", "BattlegroundQueue::StartBattleground - Cannot create battleground: %u", bgTypeId);
        return;
    }

    // Balance queues before inviting players
    BalanceQueues(MinPlayersPerTeam);

    for (uint32 i = 0; i < BG_TEAMS_COUNT; i++)
        for (GroupsQueueType::const_iterator citr = m_SelectionPools[TEAM_ALLIANCE + i].SelectedGroups.begin(); citr != m_SelectionPools[TEAM_ALLIANCE + i].SelectedGroups.end(); ++citr)
            InviteGroupToBG((*citr), bg, (*citr)->team);

    bg->StartBattleground();
}

/*********************************************************/
/***            BATTLEGROUND QUEUE EVENTS              ***/
/*********************************************************/

bool BGQueueInviteEvent::Execute(uint64 /*e_time*/, uint32 /*p_time*/)
{
    Player* player = ObjectAccessor::FindConnectedPlayer(m_PlayerGuid);
    if (!player)
        return true;

    Battleground* bg = sBattlegroundMgr->GetBattleground(m_BgInstanceGUID, m_BgTypeId);
    if (!bg)
        return true;

    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), m_RatedType);
    BattlegroundQueueTypeId bgQueueTypeId2 = BattlegroundMgr::BGQueueTypeId(m_BgTypeId, m_RatedType);

    uint32 queueSlot = player->GetBattlegroundQueueIndex(bgQueueTypeId);

    if (player->IsSuspendedQueueForBattlegroundQueueIndex(queueSlot))
        return true;

    if (queueSlot < PLAYER_MAX_BATTLEGROUND_QUEUES)
    {
        BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId2);
        if (bgQueue.IsPlayerInvited(m_PlayerGuid, m_BgInstanceGUID, m_RemoveTime))
            sBattlegroundMgr->BuildBattlegroundWaitJoinStatus(bg, player, queueSlot, INVITE_ACCEPT_WAIT_TIME - INVITATION_REMIND_TIME, player->GetBattlegroundQueueJoinTime(bgQueueTypeId), m_RatedType, m_Role);
    }
    return true;
}

void BGQueueInviteEvent::Abort(uint64 /*e_time*/)
{
    //do nothing
}

/*
    this event has many possibilities when it is executed:
    1. player is in battleground (he clicked enter on invitation window)
    2. player left battleground queue and he isn't there any more
    3. player left battleground queue and he joined it again and IsInvitedToBGInstanceGUID = 0
    4. player left queue and he joined again and he has been invited to same battleground again -> we should not remove him from queue yet
    5. player is invited to bg and he didn't choose what to do and timer expired - only in this condition we should call queue::RemovePlayer
    we must remove player in the 5. case even if battleground object doesn't exist!
*/
bool BGQueueRemoveEvent::Execute(uint64 /*e_time*/, uint32 /*p_time*/)
{
    Player* player = ObjectAccessor::FindConnectedPlayer(m_PlayerGuid);
    if (!player)
        return true;

    uint32 queueSlot = player->GetBattlegroundQueueIndex(m_BgOriginalQueueTypeId);
    if (queueSlot < PLAYER_MAX_BATTLEGROUND_QUEUES)
    {
        player->SuspendOtherQueues(false);

        BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(m_BgQueueTypeId);
        if (bgQueue.IsPlayerInvited(m_PlayerGuid, m_BgInstanceGUID, m_RemoveTime))
        {
            TC_LOG_DEBUG("bg.battleground", "Battleground: removing player %u from bg queue for instance %u because of not pressing enter battle in time.", player->GetGUID().GetCounter(), m_BgInstanceGUID);

            bgQueue.RemovePlayer(m_PlayerGuid, true);

            sBattlegroundMgr->BuildBattlegroundNoneStatus(player, queueSlot, player->GetBattlegroundQueueJoinTime(m_BgOriginalQueueTypeId), RATED_TYPE_NOT_RATED);

            player->RemoveBattlegroundQueueId(m_BgOriginalQueueTypeId);
        }
    }
    return true;
}

void BGQueueRemoveEvent::Abort(uint64 /*e_time*/)
{
    //do nothing
}
