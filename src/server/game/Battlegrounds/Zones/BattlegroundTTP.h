/*
* Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BATTLEGROUNDTTP_H
#define __BATTLEGROUNDTTP_H

class Battleground;

enum BattlegroundTTPObjectTypes
{
    BG_TTP_OBJECT_DOOR_1         = 0,
    BG_TTP_OBJECT_DOOR_2         = 1,

    BG_TTP_OBJECT_BUFF_1         = 2,
    BG_TTP_OBJECT_BUFF_2         = 3,

    BG_TTP_OBJECT_MAX            = 4
};

enum BattlegroundTTPObjects
{
    BG_TTP_OBJECT_TYPE_DOOR_1    = 212921,
    BG_TTP_OBJECT_TYPE_DOOR_2    = 212921,

    BG_TTP_OBJECT_TYPE_BUFF_1    = 184663,
    BG_TTP_OBJECT_TYPE_BUFF_2    = 184664
};

struct BattlegroundTTPScore : public BattlegroundScore
{
    BattlegroundTTPScore() { };
    ~BattlegroundTTPScore() { };
};

class BattlegroundTTP : public Battleground
{
    public:
        BattlegroundTTP();
        ~BattlegroundTTP();

        /* Inherited from Battleground class. */

        void Reset() override;
        bool SetupBattleground() override;

        /* Players. */
        void AddPlayer(Player* player) override;
        void RemovePlayer(Player* player, ObjectGuid guid, uint32 team) override;
        void HandleKillPlayer(Player* player, Player* killer) override;

        /* Doors. */
        void StartingEventCloseDoors() override;
        void StartingEventOpenDoors() override;

        /* WorldStates. */
        void FillInitialWorldStates(WorldStateBuilder& builder) override;

        /* Areatriggers. */
        void HandleAreaTrigger(Player* Source, uint32 Trigger, bool Entered) override;

        /* Cheaters / Players under map. */
        bool HandlePlayerUnderMap(Player* player) override;
};

#endif
