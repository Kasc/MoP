/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_BATTLEGROUNDBFG_H
#define SF_BATTLEGROUNDBFG_H

class Battleground;

enum BG_BFG_WorldStates 
{
	BG_BFG_OP_OCCUPIED_BASES_HORDE  = 1778,
	BG_BFG_OP_OCCUPIED_BASES_ALLY   = 1779,
	BG_BFG_OP_RESOURCES_ALLY        = 1776,
	BG_BFG_OP_RESOURCES_HORDE       = 1777,
	BG_BFG_OP_RESOURCES_MAX         = 1780,
	BG_BFG_OP_RESOURCES_WARNING     = 1955,

    WS_WATERWORKS_NEUTRAL           = 1846,
    WS_WATERWORKS_A                 = 1782,
    WS_WATERWORKS_CON_A             = 1784,
    WS_WATERWORKS_H                 = 1783,
    WS_WATERWORKS_CON_H             = 1785,

    WS_LIGHTHOUSE_NEUTRAL           = 1842,
    WS_LIGHTHOUSE_A                 = 1767,
    WS_LIGHTHOUSE_CON_A             = 1769,
    WS_LIGHTHOUSE_H                 = 1768,
    WS_LIGHTHOUSE_CON_H             = 1770,

    WS_MINES_NEUTRAL                = 1845,
    WS_MINES_A                      = 1772,
    WS_MINES_CON_A                  = 1774,
    WS_MINES_H                      = 1773,
    WS_MINES_CON_H                  = 1775
};

const uint32 BG_BFG_OP_NODESTATES[3]    = { 1767, 1782, 1772 };

const uint32 BG_BFG_OP_NODEICONS[3]     = { 1842, 1846, 1845 };

enum BG_BFG_NodeObjectId
{
    BG_BFG_OBJECTID_NODE_BANNER_0   = 205555,      // Waterworks banner
    BG_BFG_OBJECTID_NODE_BANNER_1   = 205556,      // Mine banner
    BG_BFG_OBJECTID_NODE_BANNER_2   = 205557       // Lighthouse banner
};

enum BG_BFG_ObjectType
{
    BG_BFG_OBJECT_BANNER_NEUTRAL            = 0,
    BG_BFG_OBJECT_BANNER_CONT_A             = 1,
    BG_BFG_OBJECT_BANNER_CONT_H             = 2,
    BG_BFG_OBJECT_BANNER_ALLY               = 3,
    BG_BFG_OBJECT_BANNER_HORDE              = 4,
    BG_BFG_OBJECT_AURA_ALLY                 = 5,
    BG_BFG_OBJECT_AURA_HORDE                = 6,
    BG_BFG_OBJECT_AURA_CONTESTED            = 7,
    BG_BFG_OBJECT_GATE_A                    = 25,
    BG_BFG_OBJECT_GATE_H                    = 26,
	//buffs
	BG_BFG_OBJECT_SPEEDBUFF_LIGHTHOUSE      = 27,
	BG_BFG_OBJECT_REGENBUFF_LIGHTHOUSE      = 28,
	BG_BFG_OBJECT_BERSERKBUFF_LIGHTHOUSE    = 29,
	BG_BFG_OBJECT_SPEEDBUFF_MINE            = 30,
	BG_BFG_OBJECT_REGENBUFF_MINE            = 31,
	BG_BFG_OBJECT_BERSERKBUFF_MINE          = 32,
	BG_BFG_OBJECT_SPEEDBUFF_WATERWORKS      = 33,
	BG_BFG_OBJECT_REGENBUFF_WATERWORKS      = 34,
	BG_BFG_OBJECT_BERSERKBUFF_WATERWORKS    = 35,
	BG_BFG_OBJECT_MAX                       = 36
};

/* Object id templates from DB */
enum BG_BFG_ObjectTypes
{
    BG_BFG_OBJECTID_BANNER_A        = 208673,
    BG_BFG_OBJECTID_BANNER_CONT_A   = 208763,
    BG_BFG_OBJECTID_BANNER_H        = 208748,
    BG_BFG_OBJECTID_BANNER_CONT_H   = 208733,

	BG_BFG_OBJECTID_AURA_A          = 180100,
	BG_BFG_OBJECTID_AURA_H          = 180101,
	BG_BFG_OBJECTID_AURA_C          = 180102,

    BG_BFG_OBJECTID_GATE_A          = 207177,
    BG_BFG_OBJECTID_GATE_H          = 207178
};

enum BG_BFG_Timers
{
    BG_BFG_FLAG_CAPTURING_TIME      = 60000
};

enum BG_BFG_Score
{
    BG_BFG_WARNING_NEAR_VICTORY_SCORE   = 1800,
    BG_BFG_MAX_TEAM_SCORE               = 2000
};

/* do NOT change the order, else wrong behaviour */
enum BG_BFG_BattlegroundNodes
{
    BG_BFG_NODE_LIGHTHOUSE      = 0,
    BG_BFG_NODE_WATERWORKS      = 1,
    BG_BFG_NODE_MINE            = 2,

    BG_BFG_DYNAMIC_NODES_COUNT  = 3,                        // dynamic nodes that can be captured(it's normally 4)

    BG_BFG_SPIRIT_ALIANCE       = 3,
    BG_BFG_SPIRIT_HORDE         = 4,

    BG_BFG_ALL_NODES_COUNT      = 5                         // all nodes (dynamic and static)
};

enum BG_BFG_NodeStatus
{
    BG_BFG_NODE_TYPE_NEUTRAL            = 0,
    BG_BFG_NODE_TYPE_CONTESTED          = 1,
    BG_BFG_NODE_STATUS_ALLY_CONTESTED   = 1,
    BG_BFG_NODE_STATUS_HORDE_CONTESTED  = 2,
    BG_BFG_NODE_TYPE_OCCUPIED           = 3,
    BG_BFG_NODE_STATUS_ALLY_OCCUPIED    = 3,
    BG_BFG_NODE_STATUS_HORDE_OCCUPIED   = 4
};

enum BG_BFG_Sounds
{
    BG_BFG_SOUND_NODE_CLAIMED               = 8192,
    BG_BFG_SOUND_NODE_CAPTURED_ALLIANCE     = 8173,
    BG_BFG_SOUND_NODE_CAPTURED_HORDE        = 8213,
    BG_BFG_SOUND_NODE_ASSAULTED_ALLIANCE    = 8212,
    BG_BFG_SOUND_NODE_ASSAULTED_HORDE       = 8174,
    BG_BFG_SOUND_NEAR_VICTORY               = 8456
};

enum BG_BFG_Objectives
{
    BG_OBJECTIVE_ASSAULT_BASE               = 122,
    BG_OBJECTIVE_DEFEND_BASE                = 123
};

#define BG_BFG_NotBGBGWeekendHonorTicks      130
#define BG_BFG_BGBGWeekendHonorTicks         100

// x, y, z, o
const float BG_BFG_NodePositions[BG_BFG_DYNAMIC_NODES_COUNT][4] =
{
    { 980.08f,      948.707f,       12.7478f,   2.74016f    },  // Waterwork
    { 1251.010f,    958.3939f,      5.680f,     2.7698f     },  // Mine
    { 1057.7800f,   1278.260010f,   3.192400f,  1.864820f   }   // Lighthouse
};

// x, y, z, o, rot0, rot1, rot2, rot3
const float BG_BFG_DoorPositions[2][8] =
{
    { 918.876f, 1336.56f, 27.6195f, 2.77481f, 0.0f, 0.0f, 0.983231f, 0.182367f },
    { 1396.15f, 977.014f, 7.43169f, 6.27043f, 0.0f, 0.0f, 0.006378f, -0.99998f }
};

const uint32 BG_BFG_TickIntervals[4] = { 0, 12000, 6000, 1000 };
const uint32 BG_BFG_TickPoints[4] = { 0, 10, 10, 30 };

// WorldSafeLocs ids for 3 nodes, and for ally, and horde starting location
const uint32 BG_BFG_GraveyardIds[BG_BFG_ALL_NODES_COUNT] = { 1736, 1738, 1735, 1740, 1739 };

// x, y, z, o
const float BG_BFG_BuffPositions[BG_BFG_DYNAMIC_NODES_COUNT][4] =
{
    { 1063.39f, 1309.09f, 4.910f, 3.98f },   // Lighthouse
	{ 990.950f, 984.460f, 13.01f, 4.57f },   // Waterworks
    { 1196.65f, 1020.01f, 7.970f, 5.74f }    // Mine
//	{ 1107.57f, 912.180f, 27.54f, 5.53f }    // To be named
};

// x, y, z, o
const float BG_BFG_SpiritGuidePos[BG_BFG_ALL_NODES_COUNT][4] =
{
    { 1036.32f, 1341.61f,   11.55f, 4.78f },    // Lighthouse
	{ 886.44f,  938.06f,    24.13f, 0.53f },    // Waterworks
	{ 1252.39f, 831.77f,    27.78f, 1.59f },    // Mine
    { 898.15f,  1341.58f,   27.66f, 6.06f },    // alliance starting base
    { 1408.16f, 977.34f,    7.44f,  3.18f }     // horde starting base
};

struct BG_BFG_BannerTimer
{
    uint32      timer;
    uint8       type;
    uint8       teamIndex;
};

struct BattlegroundBFGScore : public BattlegroundScore
{
    BattlegroundBFGScore() : BasesAssaulted(0), BasesDefended(0) { };
    ~BattlegroundBFGScore() { };

    uint32 BasesAssaulted;
    uint32 BasesDefended;
};

class BattlegroundBFG: public Battleground
{
    public:
        BattlegroundBFG();
        ~BattlegroundBFG();

        void AddPlayer(Player* player) override;
        void RemovePlayer(Player* player, ObjectGuid guid, uint32 /*team*/) override;

	    void StartingEventCloseDoors() override;
	    void StartingEventOpenDoors() override;

	    void HandleAreaTrigger(Player* Source, uint32 Trigger, bool entered) override;

	    bool SetupBattleground() override;

	    void Reset() override;

	    void EndBattleground(uint32 winner) override;

	    WorldSafeLocsEntry const* GetClosestGraveYard(Player* player) override;

	    /* Scorekeeping */
	    void UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor = true) override;

	    void FillInitialWorldStates(WorldStateBuilder& builder) override;

	    /* Nodes occupying */
	    void EventPlayerClickedOnFlag(Player* source, WorldObject* obj) override;

	    /* achievement req. */
	    bool IsAllNodesConrolledByTeam(uint32 team) const;
	    bool IsTeamScores500Disadvantage(uint32 team) const { return m_TeamScores500Disadvantage[GetTeamIndexByTeamId(team)]; }

        uint32 GetPrematureWinner() override;

    private:
	    void PostUpdateImpl(uint32 diff) override;

	    /* Gameobject spawning/despawning */
	    void _CreateBanner(uint8 node, uint8 type, uint8 teamIndex, bool delay);
	    void _DelBanner(uint8 node, uint8 type, uint8 teamIndex);
	    void _SendNodeUpdate(uint8 node);

	    /* Creature spawning/despawning */
	    // TODO: working, scripted peons spawning
	    void _NodeOccupied(uint8 node, Team team);
	    void _NodeDeOccupied(uint8 node);

	    int32 _GetNodeNameId(uint8 node);

	    /* Nodes info:
	     0: neutral
	     1: ally contested
	     2: horde contested
	     3: ally occupied
	     4: horde occupied     */
	    uint8 m_Nodes[BG_BFG_DYNAMIC_NODES_COUNT];
	    uint8 m_prevNodes[BG_BFG_DYNAMIC_NODES_COUNT];

	    BG_BFG_BannerTimer m_BannerTimers[BG_BFG_DYNAMIC_NODES_COUNT];

	    uint32 m_NodeTimers[BG_BFG_DYNAMIC_NODES_COUNT];

	    uint32 m_lastTick[BG_TEAMS_COUNT];
	    uint32 m_HonorScoreTics[BG_TEAMS_COUNT];

	    bool m_IsInformedNearVictory;
	    uint32 m_HonorTics;

	    // need for achievements
	    bool m_TeamScores500Disadvantage[BG_TEAMS_COUNT];
};

#endif
