/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_BATTLEGROUNDMGR_H
#define SF_BATTLEGROUNDMGR_H

#include "Common.h"
#include "DBCEnums.h"
#include "Battleground.h"
#include "BattlegroundQueue.h"
#include "RatedInfo.h"

#include "Singleton/Singleton.hpp"

typedef std::map<uint32, Battleground*> BattlegroundContainer;
typedef std::set<uint32> BattlegroundClientIdsContainer;

typedef std::unordered_map<uint32, BattlegroundTypeId> BattleMastersMap;

enum BattlegroundMisc
{
    BATTLEGROUND_OBJECTIVE_UPDATE_INTERVAL = 1000
};

struct BattlegroundTemplate
{
    BattlegroundTypeId Id;
    uint16 MinPlayersPerTeam;
    uint16 MaxPlayersPerTeam;
    uint8 MinLevel;
    uint8 MaxLevel;
    Position StartLocation[BG_TEAMS_COUNT];
    float MaxStartDistSq;
    uint8 Weight;
    uint32 ScriptId;
    BattlemasterListEntry const* BattlemasterEntry;

    bool IsArena() const { return BattlemasterEntry->InstanceType == MAP_ARENA; }
};

struct BattlegroundData
{
    BattlegroundContainer m_Battlegrounds;
    BattlegroundClientIdsContainer m_ClientBattlegroundIds[MAX_BATTLEGROUND_BRACKETS];
};

struct BattlegroundQueueStarted
{
    BattlegroundQueueStarted() : queueTypeID(BATTLEGROUND_QUEUE_NONE), typeID(BATTLEGROUND_TYPE_NONE), bracketID(BG_BRACKET_ID_FIRST), countIndex(0) { }

    BattlegroundQueueTypeId queueTypeID;
    BattlegroundTypeId typeID;
    BattlegroundBracketId bracketID;
    uint8 countIndex;
};

struct BattlegroundRunning
{
    BattlegroundRunning() : battleground(nullptr), queueTypeID(BATTLEGROUND_QUEUE_NONE), bracketID(BG_BRACKET_ID_FIRST), freeSlots(0) { }

    Battleground* battleground;
    BattlegroundQueueTypeId queueTypeID;
    BattlegroundBracketId bracketID;
    uint32 freeSlots;
};

struct BattlegroundRunningFreeSlotsOrder
{
    BattlegroundRunningFreeSlotsOrder() { }

    bool operator()(BattlegroundRunning bg1, BattlegroundRunning bg2) const
    {
        uint32 freeSlots1 = bg1.freeSlots;
        uint32 freeSlots2 = bg2.freeSlots;

        return  freeSlots1 > freeSlots2;
    }
};

class BattlegroundMgr
{
    friend class Tod::Singleton<BattlegroundMgr>;

    BattlegroundMgr();
    ~BattlegroundMgr();

public:
    void Update(uint32 diff);

    /* Packet Building */
    void BuildPlayerJoinedBattlegroundPacket(WorldPacket* data, ObjectGuid guid);
    void BuildPlayerLeftBattlegroundPacket(WorldPacket* data, ObjectGuid guid);
    void BuildBattlegroundListPacket(ObjectGuid guid, Player* player, BattlegroundTypeId bgTypeId);
    void BuildStatusFailedPacket(Battleground* bg, Player* player, uint32 QueueSlot, uint32 ArenaType, GroupJoinBattlegroundResult result, ObjectGuid const* guid = nullptr);
    void BuildUpdateWorldStatePacket(WorldPacket* data, uint32 field, uint32 value, bool Hidden);
    void BuildPvpLogDataPacket(WorldPacket* data, Battleground* bg);
    void BuildPlaySoundPacket(WorldPacket* data, uint32 soundId);
    void SendAreaSpiritHealerQueryOpcode(Player* player, Battleground* bg, ObjectGuid guid);
    void HandleStartTimer(Player* player, uint32 TimeRemaining, uint32 TotalTime, uint32 Type);

    /* Battleground status*/
    void BuildBattlegroundNoneStatus(Player* player, uint32 QueueSlot, uint32 JoinTime, RatedType ratedType);
    void BuildBattlegroundWaitQueueStatus(Battleground* bg, Player* player, uint32 queueSlot, uint32 time1, uint32 JoinTime, RatedType ratedType, bool asGroup, bool SuspendQueue = false);
    void BuildBattlegroundWaitJoinStatus(Battleground* bg, Player* player, uint32 queueSlot, uint32 time1, uint32 JoinTime, RatedType ratedType, uint8 role);
    void BuildBattlegroundInProgressStatus(Battleground* bg, Player* player, uint32 queueSlot, uint32 time1, uint32 JoinTime, RatedType ratedType, bool leftEarly = false);

    /* Battlegrounds */
    Battleground* GetBattleground(uint32 InstanceID, BattlegroundTypeId bgTypeId);
    Battleground* GetBattlegroundTemplate(BattlegroundTypeId bgTypeId);
    Battleground* CreateNewBattleground(BattlegroundTypeId bgTypeId, PvPDifficultyEntry const* bracketEntry, RatedType ratedType, bool IsRated, bool IsWarGame, uint32 MinPlayers = 0);

    void AddBattleground(Battleground* bg);
    void RemoveBattleground(BattlegroundTypeId bgTypeId, uint32 instanceId);

    void LoadBattlegroundTemplates();
    void DeleteAllBattlegrounds();

    void LoadBattlegroundItemLevelCaps();

    void SendToBattleground(Player* player, uint32 InstanceID, BattlegroundTypeId bgTypeId);

    /* Battleground queues */
    BattlegroundQueue& GetBattlegroundQueue(BattlegroundQueueTypeId bgQueueTypeId) { return m_BattlegroundQueues[bgQueueTypeId]; }
    void ScheduleQueueUpdate(uint32 arenaMatchmakerRating, RatedType ratedType, BattlegroundQueueTypeId bgQueueTypeId, BattlegroundTypeId bgTypeId, BattlegroundBracketId bracket_id);
    uint32 GetPrematureFinishTime() const;

    void ToggleArenaTesting();
    void ToggleTesting();

    void SetHolidayWeekends(uint32 mask);

    bool IsRatedBattlegroundTesting() const { return m_RatedBattlegroundTesting; }
    bool IsArenaTesting() const { return m_ArenaTesting; }
    bool IsTesting() const { return m_Testing; }

    static BattlegroundQueueTypeId BGQueueTypeId(BattlegroundTypeId bgTypeId, RatedType ratedType);
    static BattlegroundTypeId BGTemplateId(BattlegroundQueueTypeId bgQueueTypeId);
    static RatedType GetRatedTypeByQueue(BattlegroundQueueTypeId bgQueueTypeId);

    static HolidayIds BGTypeToWeekendHolidayId(BattlegroundTypeId bgTypeId);
    static BattlegroundTypeId WeekendHolidayIdToBGType(HolidayIds holiday);
    static bool IsBGWeekend(BattlegroundTypeId bgTypeId);

    uint32 GetMaxRatingDifference() const;
    uint32 GetRatingDiscardTimer()  const;
    void LoadBattleMastersEntry();
    void CheckBattleMasters();

    BattlegroundTypeId GetBattleMasterBG(uint32 entry) const
    {
        BattleMastersMap::const_iterator itr = mBattleMastersMap.find(entry);
        if (itr != mBattleMastersMap.end())
            return itr->second;
        return BATTLEGROUND_TYPE_NONE;
    }

    uint32 GetBattlegroundItemLevelCap(uint32 maxLevel) const
    {
        BattlegroundItemLevelCap::const_iterator itr = _battlegroundItemLevelCaps.find(maxLevel);
        if (itr != _battlegroundItemLevelCaps.end())
            return itr->second;
        return 0;
    }

    // War Games
    void CreateWarGameQueue(Battleground* bg, Player* challenger, Player* opponent);
    uint32 GenerateWarGameId();

private:
    bool CreateBattleground(BattlegroundTemplate const* bgTemplate);
    uint32 CreateClientVisibleInstanceId(BattlegroundTypeId bgTypeId, BattlegroundBracketId bracket_id);
    static bool IsArenaType(BattlegroundTypeId bgTypeId);
    static bool IsRatedBattlegroundTemplate(BattlegroundTypeId bgTypeId);
    BattlegroundTypeId GetRandomBG(BattlegroundTypeId id, uint32 MinPlayers);

    typedef std::map<BattlegroundTypeId, BattlegroundData> BattlegroundDataContainer;
    BattlegroundDataContainer bgDataStore;

    BattlegroundQueue m_BattlegroundQueues[MAX_BATTLEGROUND_QUEUE_TYPES];

    typedef std::map<BattlegroundTypeId, uint8> BattlegroundSelectionWeightMap; // TypeId and its selectionWeight

    GuidVector m_QueueUpdateScheduler;
    uint32 m_NextRatedArenaUpdate;
    bool   m_RatedBattlegroundTesting;
    uint32 m_UpdateTimer;
    bool   m_ArenaTesting;
    bool   m_Testing;
    BattleMastersMap mBattleMastersMap;
    uint32 m_BattlegroundQueueUpdateTimer;

    BattlegroundTemplate const* GetBattlegroundTemplateByTypeId(BattlegroundTypeId id)
    {
        BattlegroundTemplateMap::const_iterator itr = _battlegroundTemplates.find(id);
        if (itr != _battlegroundTemplates.end())
            return &itr->second;
        return nullptr;
    }

    BattlegroundTemplate const* GetBattlegroundTemplateByMapId(uint32 mapId)
    {
        BattlegroundMapTemplateContainer::const_iterator itr = _battlegroundMapTemplates.find(mapId);
        if (itr != _battlegroundMapTemplates.end())
            return itr->second;
        return nullptr;
    }

    typedef std::map<BattlegroundTypeId, BattlegroundTemplate> BattlegroundTemplateMap;
    typedef std::map<uint32 /*mapId*/, BattlegroundTemplate*> BattlegroundMapTemplateContainer;
    typedef std::map<uint32, uint32> BattlegroundItemLevelCap;
    BattlegroundTemplateMap _battlegroundTemplates;
    BattlegroundMapTemplateContainer _battlegroundMapTemplates;
    BattlegroundItemLevelCap _battlegroundItemLevelCaps;

    uint32 _WarGameId;
};

#define sBattlegroundMgr Tod::Singleton<BattlegroundMgr>::GetSingleton()
#endif
