/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Battleground.h"
#include "BattlegroundMgr.h"
#include "RatedMgr.h"
#include "RatedInfo.h"
#include "BattlePetMgr.h"
#include "Creature.h"
#include "Chat.h"
#include "Formulas.h"
#include "GridNotifiersImpl.h"
#include "Group.h"
#include "Guild.h"
#include "GuildMgr.h"
#include "MapManager.h"
#include "Object.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ReputationMgr.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "Util.h"
#include "World.h"
#include "WorldPacket.h"
#include "Pet.h"
#include "Transport.h"
#include "GameTime.h"

namespace Trinity
{
    class BattlegroundChatBuilder
    {
    public:
        BattlegroundChatBuilder(ChatMsg msgtype, int32 textId, Player const* source, va_list* args = NULL) : _msgtype(msgtype), _textId(textId), _source(source), _args(args) { }

        void operator()(WorldPacket& data, LocaleConstant loc_idx)
        {
            char const* text = sObjectMgr->GetTrinityString(_textId, loc_idx);
            if (_args)
            {
                // we need copy va_list before use or original va_list will corrupted
                va_list ap;
                va_copy(ap, *_args);

                char str[2048];
                vsnprintf(str, 2048, text, ap);
                va_end(ap);
                do_helper(data, &str[0]);
            }
            else
                do_helper(data, text);
        }
    private:
        void do_helper(WorldPacket& data, char const* text)
        {
            ChatHandler::BuildChatPacket(data, _msgtype, LANG_UNIVERSAL, _source, _source, text);
        }

        ChatMsg _msgtype;
        int32 _textId;
        Player const* _source;
        va_list* _args;
    };

    class Battleground2ChatBuilder
    {
    public:
        Battleground2ChatBuilder(ChatMsg msgtype, int32 textId, Player const* source, int32 arg1, int32 arg2) : _msgtype(msgtype), _textId(textId), _source(source), _arg1(arg1), _arg2(arg2) { }

        void operator()(WorldPacket& data, LocaleConstant loc_idx)
        {
            char const* text = sObjectMgr->GetTrinityString(_textId, loc_idx);
            char const* arg1str = _arg1 ? sObjectMgr->GetTrinityString(_arg1, loc_idx) : "";
            char const* arg2str = _arg2 ? sObjectMgr->GetTrinityString(_arg2, loc_idx) : "";

            char str[2048];
            snprintf(str, 2048, text, arg1str, arg2str);
            uint64 target_guid = _source  ? _source->GetGUID() : 0;
            ChatHandler::BuildChatPacket(data, _msgtype, LANG_UNIVERSAL, _source, _source, str);
        }
    private:
        ChatMsg _msgtype;
        int32 _textId;
        Player const* _source;
        int32 _arg1;
        int32 _arg2;
    };
    class Battleground3ChatBuilder
    {
    public:
        Battleground3ChatBuilder(int32 textId, int32 arg1, int32 arg2) : _textId(textId), _arg1(arg1), _arg2(arg2) { }

        void operator()(WorldPacket& data, LocaleConstant loc_idx)
        {
            char const* text = sObjectMgr->GetTrinityString(_textId, loc_idx);
            char const* arg1str = _arg1 ? sObjectMgr->GetTrinityString(_arg1, loc_idx) : "";
            char const* arg2str = _arg2 ? sObjectMgr->GetTrinityString(_arg2, loc_idx) : "";

            char str[2048];
            snprintf(str, 2048, text, arg1str, arg2str);

            ChatHandler::BuildChatPacket(data, CHAT_MSG_RAID_BOSS_EMOTE, LANG_UNIVERSAL, NULL, NULL, str);
        }
    private:
        int32 _textId;
        int32 _arg1;
        int32 _arg2;
    };
} // namespace Trinity

template<class Do>
void Battleground::BroadcastWorker(Do& _do)
{
    for (auto players : m_Players)
        if (Player* player = ObjectAccessor::FindPlayer(players.first))
            _do(player);
}

Battleground::Battleground()
{
    m_TypeID                        = BATTLEGROUND_TYPE_NONE;
    m_queueId                       = ObjectGuid::Empty;
    m_InstanceID                    = 0;
    m_Status                        = STATUS_NONE;
    m_ClientInstanceID              = 0;
    m_EndTime                       = 0;
    m_LastResurrectTime             = 0;
    m_BracketId                     = BG_BRACKET_ID_FIRST;
    m_InvitedAlliance               = 0;
    m_InvitedHorde                  = 0;
    m_RatedType                     = RATED_TYPE_NOT_RATED;
    m_IsArena                       = false;
    m_IsWarGame                     = false;
    m_Winner                        = 2;
    m_StartTime                     = 0;
    m_CountdownTimer                = 0;
    m_ResetStatTimer                = 0;
    m_ValidStartPositionTimer       = 0;
    m_Events                        = 0;
    m_StartDelayTime                = 0;
    m_IsRated                       = false;
    m_BuffChange                    = false;
    m_LevelMin                      = 0;
    m_LevelMax                      = 0;
    m_SetDeleteThis                 = false;

    m_MaxPlayersPerTeam             = 0;
    m_MaxPlayers                    = 0;
    m_MinPlayersPerTeam             = 0;
    m_MinPlayers                    = 0;

    m_MapId                         = 0;
    m_Map                           = NULL;
    m_StartMaxDist                  = 0.0f;

    ScriptId                        = 0;

    m_BgRaids[TEAM_ALLIANCE]        = NULL;
    m_BgRaids[TEAM_HORDE]           = NULL;

    m_PlayersCount[TEAM_ALLIANCE]   = 0;
    m_PlayersCount[TEAM_HORDE]      = 0;

    m_TeamScores[TEAM_ALLIANCE]     = 0;
    m_TeamScores[TEAM_HORDE]        = 0;

    m_PrematureCountDown            = false;

    m_HonorMode                     = BG_NORMAL;

    StartDelayTimes[BG_STARTING_EVENT_FIRST]  = BG_START_DELAY_2M;
    StartDelayTimes[BG_STARTING_EVENT_SECOND] = BG_START_DELAY_1M;
    StartDelayTimes[BG_STARTING_EVENT_THIRD]  = BG_START_DELAY_30S;
    StartDelayTimes[BG_STARTING_EVENT_FOURTH] = BG_START_DELAY_NONE;
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_BG_WS_START_TWO_MINUTES;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_BG_WS_START_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_BG_WS_START_HALF_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_BG_WS_HAS_BEGUN;

    m_DampeningApplied = false;
}

Battleground::~Battleground()
{
    uint32 size = uint32(BgCreatures.size());
    for (uint32 i = 0; i < size; ++i)
        DelCreature(i);

    size = uint32(BgObjects.size());
    for (uint32 i = 0; i < size; ++i)
        DelObject(i);

    sBattlegroundMgr->RemoveBattleground(GetTypeID(), GetInstanceID());

    if (m_Map)
    {
        m_Map->SetUnload();
        m_Map->SetBG(NULL);
        m_Map = NULL;
    }

    for (auto itr : PlayerScores)
        delete itr.second;
}

void Battleground::Update(uint32 diff)
{
    if (!PreUpdateImpl(diff))
        return;

    if (!GetPlayersSize())
    {
        if (!GetInvitedCount(HORDE) && !GetInvitedCount(ALLIANCE))
            m_SetDeleteThis = true;
        return;
    }

    switch (GetStatus())
    {
        case STATUS_WAIT_JOIN:
            if (GetPlayersSize())
            {
                _ProcessJoin(diff);
                _CheckSafePositions(diff);
            }
            break;
        case STATUS_IN_PROGRESS:
            _ProcessOfflineQueue();
            if (IsArena())
            {
                uint32 timerToDampening = GetRatedType() == RATED_TYPE_2v2 ? 5 * MINUTE * IN_MILLISECONDS : 10 * MINUTE*IN_MILLISECONDS;
                if (GetElapsedTime() >= timerToDampening)
                {
                    if (!m_DampeningApplied)
                    {
                        m_DampeningApplied = true;

                        for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
                            if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                                player->CastSpell(player, SPELL_ARENA_DAMPENING, false);
                    }
                }

                if (GetElapsedTime() >= 20 * MINUTE*IN_MILLISECONDS)
                {
                    UpdateArenaWorldState();
                    CheckArenaAfterTimerConditions();
                    return;
                }
            }
            else
            {
                _ProcessResurrect(diff);
                if (sBattlegroundMgr->GetPrematureFinishTime() && (GetPlayersCountByTeam(ALLIANCE) < GetMinPlayersPerTeam() || GetPlayersCountByTeam(HORDE) < GetMinPlayersPerTeam()))
                    _ProcessProgress(diff);
                else if (m_PrematureCountDown)
                    m_PrematureCountDown = false;
            }
            break;
        case STATUS_WAIT_LEAVE:
            _ProcessLeave(diff);
            break;
        default:
            break;
    }

    SetElapsedTime(GetElapsedTime() + diff);
    if (GetStatus() == STATUS_WAIT_JOIN)
    {
        m_ResetStatTimer += diff;
        m_CountdownTimer += diff;
    }

    PostUpdateImpl(diff);
}

inline void Battleground::_CheckSafePositions(uint32 diff)
{
    float maxDist = GetStartMaxDist();
    if (!maxDist)
        return;

    m_ValidStartPositionTimer += diff;
    if (m_ValidStartPositionTimer >= CHECK_PLAYER_POSITION_INVERVAL)
    {
        m_ValidStartPositionTimer = 0;

        for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
        {
            if (Player* player = ObjectAccessor::FindPlayer(itr->first))
            {
                if (player->IsGameMaster())
                    continue;

                Position pos = player->GetPosition();
                Position const* startPos = GetTeamStartPosition(Battleground::GetTeamIndexByTeamId(player->GetBGTeam()));
                if (pos.GetExactDistSq(startPos) > maxDist)
                {
                    TC_LOG_DEBUG("bg.battleground", "BATTLEGROUND: Sending %s back to start location (map: %u) (possible exploit)", player->GetName().c_str(), GetMapId());
                    player->TeleportTo(GetMapId(), startPos->GetPositionX(), startPos->GetPositionY(), startPos->GetPositionZ(), startPos->GetOrientation());
                }
            }
        }
    }
}

inline void Battleground::_ProcessOfflineQueue()
{
    if (!m_OfflineQueue.empty())
    {
        BattlegroundPlayerMap::iterator itr = m_Players.find(*(m_OfflineQueue.begin()));
        if (itr != m_Players.end())
        {
            if (itr->second.OfflineRemoveTime <= GameTime::GetGameTime())
            {
                RemovePlayerAtLeave(itr->first, true, true);
                m_OfflineQueue.pop_front();
            }
        }
    }
}

inline void Battleground::_ProcessResurrect(uint32 diff)
{
    // *********************************************************
    // ***        BATTLEGROUND RESURRECTION SYSTEM           ***
    // *********************************************************

    m_LastResurrectTime += diff;
    if (m_LastResurrectTime >= RESURRECTION_INTERVAL)
    {
        if (GetReviveQueueSize())
        {
            for (std::map<ObjectGuid, GuidVector>::iterator itr = m_ReviveQueue.begin(); itr != m_ReviveQueue.end(); ++itr)
            {
                Creature* sh = NULL;
                for (GuidVector::const_iterator itr2 = (itr->second).begin(); itr2 != (itr->second).end(); ++itr2)
                {
                    Player* player = ObjectAccessor::FindPlayer(*itr2);
                    if (!player)
                        continue;

                    if (!sh && player->IsInWorld())
                    {
                        sh = player->GetMap()->GetCreature(itr->first);
                        if (sh)
                            sh->CastSpell(sh, SPELL_SPIRIT_HEAL, true);
                    }

                    player->CastSpell(player, SPELL_RESURRECTION_VISUAL, true);
                    m_ResurrectQueue.push_back(*itr2);
                }
                (itr->second).clear();
            }

            m_ReviveQueue.clear();
            m_LastResurrectTime = 0;
        }
        else
            m_LastResurrectTime = 0;
    }
    else if (m_LastResurrectTime > 500)
    {
        for (GuidVector::const_iterator itr = m_ResurrectQueue.begin(); itr != m_ResurrectQueue.end(); ++itr)
        {
            Player* player = ObjectAccessor::FindPlayer(*itr);
            if (!player)
                continue;
            player->ResurrectPlayer(1.0f);
            player->CastSpell(player, 6962, true);
            player->CastSpell(player, SPELL_SPIRIT_HEAL_MANA, true);
            player->SpawnCorpseBones(false);
        }
        m_ResurrectQueue.clear();
    }
}

uint32 Battleground::GetPrematureWinner()
{
    uint32 winner = 0;
    if (GetPlayersCountByTeam(ALLIANCE) >= GetMinPlayersPerTeam())
        winner = ALLIANCE;
    else if (GetPlayersCountByTeam(HORDE) >= GetMinPlayersPerTeam())
        winner = HORDE;

    return winner;
}

inline void Battleground::_ProcessProgress(uint32 diff)
{
    // *********************************************************
    // ***           BATTLEGROUND BALLANCE SYSTEM            ***
    // *********************************************************
    if (!m_PrematureCountDown)
    {
        m_PrematureCountDown = true;
        m_PrematureCountDownTimer = sBattlegroundMgr->GetPrematureFinishTime();
    }
    else if (m_PrematureCountDownTimer < diff)
    {
        EndBattleground(GetPrematureWinner());
        m_PrematureCountDown = false;
    }
    else if (!sBattlegroundMgr->IsTesting())
    {
        uint32 newtime = m_PrematureCountDownTimer - diff;
        if (newtime > (MINUTE * IN_MILLISECONDS))
        {
            if (newtime / (MINUTE * IN_MILLISECONDS) != m_PrematureCountDownTimer / (MINUTE * IN_MILLISECONDS))
                PSendMessageToAll(LANG_BATTLEGROUND_PREMATURE_FINISH_WARNING, CHAT_MSG_SYSTEM, NULL, (uint32)(m_PrematureCountDownTimer / (MINUTE * IN_MILLISECONDS)));
        }
        else
        {
            if (newtime / (15 * IN_MILLISECONDS) != m_PrematureCountDownTimer / (15 * IN_MILLISECONDS))
                PSendMessageToAll(LANG_BATTLEGROUND_PREMATURE_FINISH_WARNING_SECS, CHAT_MSG_SYSTEM, NULL, (uint32)(m_PrematureCountDownTimer / IN_MILLISECONDS));
        }
        m_PrematureCountDownTimer = newtime;
    }
}

inline void Battleground::_ProcessJoin(uint32 diff)
{
    // *********************************************************
    // ***           BATTLEGROUND STARTING SYSTEM            ***
    // *********************************************************
    ModifyStartDelayTime(diff);

    if (!IsArena())
        SetRemainingTime(300000);

    if (m_ResetStatTimer > 5000)
    {
        m_ResetStatTimer = 0;
        for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
            if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                if (player->IsAlive())
                    player->ResetAllPowers();
    }

    if (m_CountdownTimer >= 10000)
    {
        uint32 countdownMaxForBGType = IsArena() ? ARENA_COUNTDOWN_MAX : BATTLEGROUND_COUNTDOWN_MAX;
        uint32 tTime = (countdownMaxForBGType - (GetElapsedTime() / 1000));

        for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
            if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                sBattlegroundMgr->HandleStartTimer(player, countdownMaxForBGType, tTime, 0);

        m_CountdownTimer = 0;
    }

    if (!(m_Events & BG_STARTING_EVENT_1))
    {
        m_Events |= BG_STARTING_EVENT_1;

        if (!FindBgMap())
        {
            TC_LOG_ERROR("bg.battleground", "Battleground::_ProcessJoin: map (map id: %u, instance id: %u) is not created!", GetMapId(), GetInstanceID());
            EndNow();
            return;
        }

        if (!SetupBattleground())
        {
            EndNow();
            return;
        }

        StartingEventCloseDoors();
        SetStartDelayTime(StartDelayTimes[BG_STARTING_EVENT_FIRST]);
        SendMessageToAll(StartMessageIds[BG_STARTING_EVENT_FIRST], CHAT_MSG_BG_SYSTEM_NEUTRAL);
    }
    else if (GetStartDelayTime() <= StartDelayTimes[BG_STARTING_EVENT_SECOND] && !(m_Events & BG_STARTING_EVENT_2))
    {
        m_Events |= BG_STARTING_EVENT_2;
        SendMessageToAll(StartMessageIds[BG_STARTING_EVENT_SECOND], CHAT_MSG_BG_SYSTEM_NEUTRAL);
    }
    else if (GetStartDelayTime() <= StartDelayTimes[BG_STARTING_EVENT_THIRD] && !(m_Events & BG_STARTING_EVENT_3))
    {
        m_Events |= BG_STARTING_EVENT_3;
        SendMessageToAll(StartMessageIds[BG_STARTING_EVENT_THIRD], CHAT_MSG_BG_SYSTEM_NEUTRAL);
    }
    else if (GetStartDelayTime() <= 0 && !(m_Events & BG_STARTING_EVENT_4))
    {
        m_Events |= BG_STARTING_EVENT_4;

        StartingEventOpenDoors();

        SendWarningToAll(StartMessageIds[BG_STARTING_EVENT_FOURTH]);
        SetStatus(STATUS_IN_PROGRESS);
        SetStartDelayTime(StartDelayTimes[BG_STARTING_EVENT_FOURTH]);

        if (IsArena())
        {
            for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
                if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                {
                    player->UpdateCriteria(CRITERIA_TYPE_PLAY_ARENA, GetMapId());
                    player->RemoveAurasDueToSpell(SPELL_ARENA_PREPARATION);
                    player->ResetAllPowers();
                    if (!player->IsGameMaster())
                    {
                        Unit::AuraApplicationMap & auraMap = player->GetAppliedAuras();
                        for (Unit::AuraApplicationMap::iterator iter = auraMap.begin(); iter != auraMap.end();)
                        {
                            AuraApplication * aurApp = iter->second;
                            Aura* aura = aurApp->GetBase();
                            if (!aura->IsPermanent() && aura->GetDuration() <= 30 * IN_MILLISECONDS && aurApp->IsPositive() &&
                                (!(aura->GetSpellInfo()->HasAttribute(SPELL_ATTR0_UNAFFECTED_BY_INVULNERABILITY))) &&
                                (!aura->HasEffectType(SPELL_AURA_MOD_INVISIBILITY)))
                                player->RemoveAura(iter);
                            else
                                ++iter;
                        }
                    }
                }

            CheckArenaWinConditions();
        }
        else
        {
            PlaySoundToAll(SOUND_BG_START);

            for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
                if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                {
                    player->RemoveAurasDueToSpell(SPELL_PREPARATION);
                    player->ResetAllPowers();
                }

            if (sWorld->getBoolConfig(CONFIG_BATTLEGROUND_QUEUE_ANNOUNCER_ENABLE))
                sWorld->SendWorldText(LANG_BG_STARTED_ANNOUNCE_WORLD, GetName(), GetMinLevel(), GetMaxLevel());
        }
    }

    if (IsRated())
    {
        for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
        {
            if (Player* player = ObjectAccessor::FindPlayer(itr->first))
            {
                RatedInfo* rInfo = sRatedMgr->GetRatedInfo(player->GetGUID());
                const StatsBySlot* stats = rInfo->GetStatsBySlot(GetRatedType());
                UpdatePlayerScore(player, SCORE_PRE_MATCH_MMR, rInfo->GetMatchMakerRating());
                UpdatePlayerScore(player, SCORE_PRE_MATCH_PERSONAL_RATING, stats->PersonalRating);
            }
        }
    }

    if (GetRemainingTime() > 0 && (m_EndTime -= diff) > 0)
        SetRemainingTime(GetRemainingTime() - diff);
}

inline void Battleground::_ProcessLeave(uint32 diff)
{
    // *********************************************************
    // ***           BATTLEGROUND ENDING SYSTEM              ***
    // *********************************************************
    SetRemainingTime(GetRemainingTime() - diff);
    if (GetRemainingTime() <= 0)
    {
        SetRemainingTime(0);
        BattlegroundPlayerMap::iterator itr, next;
        for (itr = m_Players.begin(); itr != m_Players.end(); itr = next)
        {
            next = itr;
            ++next;
            RemovePlayerAtLeave(itr->first, true, true);
        }
    }
}

Player* Battleground::_GetPlayer(ObjectGuid guid, bool offlineRemove, char const* context) const
{
    Player* player = NULL;
    if (!offlineRemove)
    {
        // should this be ObjectAccessor::FindConnectedPlayer() to return players teleporting ?
        player = ObjectAccessor::FindPlayer(guid);
        if (!player)
            TC_LOG_ERROR("bg.battleground", "Battleground::%s: player (GUID: %u) not found for BG (map: %u, instance id: %u)!",
                context, guid.GetCounter(), m_MapId, m_InstanceID);
    }
    return player;
}

inline Player* Battleground::_GetPlayerForTeam(uint32 teamId, BattlegroundPlayerMap::const_iterator itr, char const* context) const
{
    Player* player = _GetPlayer(itr, context);
    if (player)
    {
        uint32 team = itr->second.Team;
        if (!team)
            team = player->GetBGTeam();
        if (team != teamId)
            player = NULL;
    }
    return player;
}

void Battleground::SetTeamStartPosition(TeamId teamId, Position const& pos)
{
    ASSERT(teamId < TEAM_NEUTRAL);
    StartPosition[teamId] = pos;
}

Position const* Battleground::GetTeamStartPosition(TeamId teamId) const
{
    ASSERT(teamId < TEAM_NEUTRAL);
    return &StartPosition[teamId];
}

void Battleground::SendPacketToAll(WorldPacket* packet)
{
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = _GetPlayer(itr, "SendPacketToAll"))
            player->SendDirectMessage(packet);
}

void Battleground::SendPacketToTeam(uint32 TeamID, WorldPacket* packet, Player* sender, bool self)
{
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
    {
        if (Player* player = _GetPlayerForTeam(TeamID, itr, "SendPacketToTeam"))
        {
            if (self || sender != player)
                player->SendDirectMessage(packet);
        }
    }
}

void Battleground::PlaySoundToAll(uint32 SoundID)
{
    WorldPacket data;
    sBattlegroundMgr->BuildPlaySoundPacket(&data, SoundID);
    SendPacketToAll(&data);
}

void Battleground::PlaySoundToTeam(uint32 SoundID, uint32 TeamID)
{
    WorldPacket data;
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = _GetPlayerForTeam(TeamID, itr, "PlaySoundToTeam"))
        {
            sBattlegroundMgr->BuildPlaySoundPacket(&data, SoundID);
            player->SendDirectMessage(&data);
        }
}

void Battleground::CastSpellOnTeam(uint32 SpellID, uint32 TeamID)
{
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = _GetPlayerForTeam(TeamID, itr, "CastSpellOnTeam"))
            player->CastSpell(player, SpellID, true);
}

void Battleground::RemoveAuraOnTeam(uint32 SpellID, uint32 TeamID)
{
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = _GetPlayerForTeam(TeamID, itr, "RemoveAuraOnTeam"))
            player->RemoveAura(SpellID);
}

void Battleground::RewardHonorToTeam(uint32 Honor, uint32 TeamID)
{
    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = _GetPlayerForTeam(TeamID, itr, "RewardHonorToTeam"))
            UpdatePlayerScore(player, SCORE_BONUS_HONOR, Honor);
}

void Battleground::RewardReputationToTeam(uint32 faction_id, uint32 Reputation, uint32 TeamID)
{
    FactionEntry const* factionEntry = sFactionStore.LookupEntry(faction_id);
    if (!factionEntry)
        return;

    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
    {
        Player* player = _GetPlayerForTeam(TeamID, itr, "RewardReputationToTeam");
        if (!player)
            continue;

        uint32 repGain = Reputation;
        AddPct(repGain, player->GetTotalAuraModifier(SPELL_AURA_MOD_REPUTATION_GAIN));
        AddPct(repGain, player->GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_FACTION_REPUTATION_GAIN, faction_id));
        player->GetReputationMgr().ModifyReputation(factionEntry, repGain);
    }
}

void Battleground::UpdateWorldState(uint32 Field, uint32 Value, bool Hidden/*= false*/)
{
    WorldPacket data;
    sBattlegroundMgr->BuildUpdateWorldStatePacket(&data, Field, Value, Hidden);
    SendPacketToAll(&data);
}

void Battleground::UpdateWorldStateForPlayer(uint32 field, uint32 value, Player* player, bool Hidden/*= false*/)
{
    WorldPacket data;
    sBattlegroundMgr->BuildUpdateWorldStatePacket(&data, field, value, Hidden);
    player->SendDirectMessage(&data);
}

void Battleground::EndBattleground(uint32 winner)
{
    int32 winmsg_id = 0;

    if (winner == ALLIANCE)
    {
        winmsg_id = IsBattleground() ? LANG_BG_A_WINS : LANG_ARENA_GOLD_WINS;
        PlaySoundToAll(SOUND_ALLIANCE_WINS);
        SetWinner(WINNER_ALLIANCE);
    }
    else if (winner == HORDE)
    {
        winmsg_id = IsBattleground() ? LANG_BG_H_WINS : LANG_ARENA_GREEN_WINS;
        PlaySoundToAll(SOUND_HORDE_WINS);
        SetWinner(WINNER_HORDE);
    }
    else
    {
        SetWinner(3);
    }

    SetStatus(STATUS_WAIT_LEAVE);
    SetRemainingTime(TIME_AUTOCLOSE_BATTLEGROUND);

    bool GuildAlreadyAwarded = false;
    uint8 aliveWinners = GetAlivePlayersCountByTeam(winner);
    for (BattlegroundPlayerMap::iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
    {
        Player* player = _GetPlayer(itr, "EndBattleground");
        uint32 team = itr->second.Team;

        RatedInfo* rInfo = sRatedMgr->GetRatedInfo(itr->first);
        ASSERT(rInfo && "RatedInfo must not be nullptr");

        int16 personalRatingChange = 0, matchmakerRatingChange = 0;
        uint16 winnerMatchmakerRating = GetTeamMatchmakerRating(winner);
        uint16 loserMatchmakerRating = GetTeamMatchmakerRating(GetOtherTeam(winner));

        if (itr->second.OfflineRemoveTime)
        {
            if (IsRated())
            {
                if (team == winner)
                    rInfo->UpdateStats(GetRatedType(), loserMatchmakerRating, personalRatingChange, matchmakerRatingChange, true, true);
                else
                    rInfo->UpdateStats(GetRatedType(), winnerMatchmakerRating, personalRatingChange, matchmakerRatingChange, false, true);
            }
            continue;
        }

        if (!player)
            continue;

        if (player->HasAuraType(SPELL_AURA_SPIRIT_OF_REDEMPTION))
            player->RemoveAurasByType(SPELL_AURA_MOD_SHAPESHIFT);

        if (team == winner && GetRatedType() == RATED_TYPE_5v5 && player->IsAlive() && aliveWinners == 1)
            player->CastSpell(player, SPELL_THE_LAST_STANDING, true);

        if (!player->IsAlive())
        {
            player->ResurrectPlayer(1.0f);
            player->SpawnCorpseBones();
        }
        else
        {
            player->CombatStop();
            player->GetHostileRefManager().deleteReferences();
        }

        if (team == winner)
        {
            if (IsArena())
            {
                player->UpdateCriteria(CRITERIA_TYPE_WIN_ARENA, GetMapId());

                if (IsRated())
                {
                    rInfo->UpdateStats(GetRatedType(), loserMatchmakerRating, personalRatingChange, matchmakerRatingChange, true, false);
                    player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_ARENA_META, sWorld->getIntConfig(CONFIG_CURRENCY_CONQUEST_POINTS_ARENA_REWARD));
                    player->UpdateCriteria(CRITERIA_TYPE_WIN_RATED_ARENA, 1);
                }
            }
            else if (IsRated())
            {
                player->UpdateCriteria(CRITERIA_TYPE_WIN_RATED_BATTLEGROUND, 1);
                player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_RANDOM_BG_META, sWorld->getIntConfig(CONFIG_CURRENCY_CONQUEST_POINTS_ARENA_REWARD));
                rInfo->UpdateStats(GetRatedType(), loserMatchmakerRating, personalRatingChange, matchmakerRatingChange, true, false);
            }
        }
        else
        {
            if (IsArena())
                player->ResetCriteria(CRITERIA_CONDITION_NO_LOSE_ARENA);
            else
            {
                // Rated Battleground placeholder
            }

            if (IsRated())
                rInfo->UpdateStats(GetRatedType(), winnerMatchmakerRating, personalRatingChange, matchmakerRatingChange, false, false);
        }

        UpdatePlayerScore(player, SCORE_PERSONAL_RATING_CHANGE, personalRatingChange);
        UpdatePlayerScore(player, SCORE_MMR_CHANGE, matchmakerRatingChange);

        UpdateAverageMMR();

        uint32 winnerKills = player->GetRandomAndWeekendWinner() ? sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_HONOR_LAST) : sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_HONOR_FIRST);
        uint32 loserKills = player->GetRandomAndWeekendWinner() ? sWorld->getIntConfig(CONFIG_BG_REWARD_LOSER_HONOR_LAST) : sWorld->getIntConfig(CONFIG_BG_REWARD_LOSER_HONOR_FIRST);

        player->RemoveAura(SPELL_HONORABLE_DEFENDER_25Y);
        player->RemoveAura(SPELL_HONORABLE_DEFENDER_60Y);

        if (team == winner)
        {
            if (player->IsRandomBGForPlayer(GetTypeID()) || BattlegroundMgr::IsBGWeekend(GetTypeID()))
            {
                UpdatePlayerScore(player, SCORE_BONUS_HONOR, GetBonusHonorFromKill(winnerKills));
                if (!player->GetRandomAndWeekendWinner())
                {
                    // 100cp awarded for the first random battleground won each day
                    player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_ARENA_META, sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_CONQUEST_FIRST));

                    if (player->IsRandomBGForPlayer(GetTypeID()))
                        player->SetRandomWinner(true);
                    else
                        player->SetWeekendWinner(true);
                }
            }
            else // 50cp awarded for each non-rated battleground won
                player->ModifyCurrency(CURRENCY_TYPE_CONQUEST_ARENA_META, sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_CONQUEST_LAST));

            player->UpdateCriteria(CRITERIA_TYPE_WIN_BG, player, 1);

            if (Guild* guild = sGuildMgr->GetGuildById(player->GetGuildId()))
            {
                bool IsGuildGroup = false;
                if (Group* group = player->GetGroup())
                    IsGuildGroup = group->IsGuildGroupForPlayer(player) > 0;

                if (!GuildAlreadyAwarded)
                    guild->UpdateCriteria(CRITERIA_TYPE_WIN_BG, player, player, 1);

                GuildAlreadyAwarded = true;

                uint32 GuildXPAwarded = 0;
                if (IsRated())
                {
                    if (IsBattleground())
                    {
                        GuildXPAwarded = GUILD_XP_FROM_WON_RATED_BATTLEGROUND;
                        if (!GuildAlreadyAwarded)
                        {
                            guild->UpdateCriteria(CRITERIA_TYPE_WIN_RATED_BATTLEGROUND, player, 1);
                            GuildAlreadyAwarded = true;
                        }
                    }
                    else
                    {
                        GuildXPAwarded = GUILD_XP_FROM_WON_RATED_ARENA;
                        if (!GuildAlreadyAwarded)
                        {
                            guild->UpdateCriteria(CRITERIA_TYPE_WIN_RATED_ARENA, player, 1);
                            GuildAlreadyAwarded = true;
                        }
                    }
                }

                if (IsGuildGroup)
                    if (GuildXPAwarded)
                        guild->GiveXP(GuildXPAwarded, player);
            }
        }
        else
        {
            if (player->IsRandomBGForPlayer(GetTypeID()) || BattlegroundMgr::IsBGWeekend(GetTypeID()))
                UpdatePlayerScore(player, SCORE_BONUS_HONOR, GetBonusHonorFromKill(loserKills));
        }

        player->ResetAllPowers();
        player->CombatStopWithPets(true);

        BlockMovement(player);

        player->UpdateCriteria(CRITERIA_TYPE_COMPLETE_BATTLEGROUND, player, 1);
    }

    WorldPacket pvpLogData;
    sBattlegroundMgr->BuildPvpLogDataPacket(&pvpLogData, this);

    for (BattlegroundPlayerMap::iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
    {
        Player* player = _GetPlayer(itr, "EndBattleground");
        if (!player)
            continue;

        player->SendDirectMessage(&pvpLogData);

        BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), GetRatedType());

        sBattlegroundMgr->BuildBattlegroundInProgressStatus(this, player, player->GetBattlegroundQueueIndex(bgQueueTypeId), player->GetBattlegroundQueueJoinTime(bgQueueTypeId), GetElapsedTime(), GetRatedType());
    }

    // If there are some locked players, unlock them
    for (ObjectGuid guid : m_LeftPlayers)
    {
        Player* player = ObjectAccessor::FindConnectedPlayer(guid);
        if (!player)
            continue;

        BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), GetRatedType());

        sBattlegroundMgr->BuildBattlegroundNoneStatus(player, player->GetBattlegroundQueueIndex(bgQueueTypeId), player->GetBattlegroundQueueJoinTime(bgQueueTypeId), GetRatedType());

        player->SetLockedToRatedBG(false);
    }

    m_LeftPlayers.clear();

    if (winmsg_id)
        SendMessageToAll(winmsg_id, CHAT_MSG_BG_SYSTEM_NEUTRAL);
}

uint32 Battleground::GetBonusHonorFromKill(uint32 kills) const
{
    uint32 maxLevel = std::min<uint32>(GetMaxLevel(), 90U);
    return Trinity::Honor::hk_honor_at_level(maxLevel, float(kills));
}

void Battleground::BlockMovement(Player* player)
{
    // This effect will be automatically removed by client when the player is teleported from the battleground
    // So there is no need to send it again with uint8(1) in RemovePlayerAtLeave()
    player->SetClientControl(player, false);
}

void Battleground::RemovePlayerAtLeave(ObjectGuid guid, bool Transport, bool SendPacket)
{
    uint32 team = GetPlayerTeam(guid);
    bool participant = false;
    BattlegroundPlayerMap::iterator itr = m_Players.find(guid);
    if (itr != m_Players.end())
    {
        UpdatePlayersCountByTeam(team, true);
        m_Players.erase(itr);
        participant = true;
    }

    BattlegroundScoreMap::iterator itr2 = PlayerScores.find(guid);
    if (itr2 != PlayerScores.end())
    {
        delete itr2->second;
        PlayerScores.erase(itr2);
    }

    RemovePlayerFromResurrectQueue(guid);

    Player* player = ObjectAccessor::FindPlayer(guid);

    if (player)
    {
        // should remove spirit of redemption
        if (player->HasAuraType(SPELL_AURA_SPIRIT_OF_REDEMPTION))
            player->RemoveAurasByType(SPELL_AURA_MOD_SHAPESHIFT);

        player->RemoveAurasByType(SPELL_AURA_MOUNTED);

        if (!player->IsAlive())                              // resurrect on exit
        {
            player->ResurrectPlayer(1.0f);
            player->SpawnCorpseBones();
        }
    }
    else
    {
        SQLTransaction trans(nullptr);
        Player::OfflineResurrect(guid, trans);
    }

    RemovePlayer(player, guid, team);

    BattlegroundTypeId bgTypeId = GetTypeID();
    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(GetTypeID(), GetRatedType());

    if (participant)
    {
        if (player)
        {
            player->ClearAfkReports();

            if (!team)
                team = player->GetBGTeam();

            if (IsRated() && GetStatus() == STATUS_IN_PROGRESS)
            {
                RatedInfo* rInfo = sRatedMgr->GetRatedInfo(player->GetGUID());
                int16 ratingChange, matchmakerRatingChange;
                rInfo->UpdateStats(GetRatedType(), GetTeamMatchmakerRating(GetOtherTeam(team)), ratingChange, matchmakerRatingChange, false, false);
            }

            if (IsArena())
            {
                bgTypeId = BATTLEGROUND_AA;

                if (Pet* pet = player->GetPet())
                {
                    pet->RemoveArenaAuras();
                    player->RemovePet(pet, PET_SLOT_ACTUAL_PET_SLOT, false, pet->IsStampeded());
                }

                player->ResummonPetTemporaryUnSummonedIfAny();
                player->GetBattlePetMgr()->ResummonLastBattlePet();
            }

            BattlegroundQueueTypeId bgQueueTypeId2 = BattlegroundMgr::BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), GetRatedType());

            if (SendPacket)
            {
                uint32 queueIndex = player->GetBattlegroundQueueIndex(bgQueueTypeId2);

                // if it is rated, locked to this bg and remove from other
                if (IsRated() && GetStatus() == STATUS_IN_PROGRESS)
                {
                    m_LeftPlayers.push_back(player->GetGUID());
                    sBattlegroundMgr->BuildBattlegroundInProgressStatus(this, player, queueIndex, player->GetBattlegroundQueueJoinTime(bgQueueTypeId2), GetElapsedTime(), GetRatedType(), true);

                    uint32 otherQueueIndex = !queueIndex ? 1 : 0;
                    player->RemoveFromQueuesOnLeftEarly(otherQueueIndex);
                }
                else
                    sBattlegroundMgr->BuildBattlegroundNoneStatus(player, queueIndex, player->GetBattlegroundQueueJoinTime(bgQueueTypeId2), GetRatedType());
            }

            player->RemoveBattlegroundQueueId(bgQueueTypeId2);
        }
        else
        {
            if (IsRated() && GetStatus() == STATUS_IN_PROGRESS)
            {
                RatedInfo* rInfo = sRatedMgr->GetRatedInfo(guid);
                int16 ratingChange, matchmakerRatingChange;
                rInfo->UpdateStats(GetRatedType(), GetTeamMatchmakerRating(GetOtherTeam(team)), ratingChange, matchmakerRatingChange, false, true);
            }
        }

        if (Group* group = GetBgRaid(team))
        {
            if (!group->RemoveMember(guid))
                SetBgRaid(team, NULL);
        }
        DecreaseInvitedCount(team);

        WorldPacket data;
        sBattlegroundMgr->BuildPlayerLeftBattlegroundPacket(&data, guid);
        SendPacketToTeam(team, &data, player, false);
    }

    if (player)
    {
        player->SetRandomBGForPlayer(BATTLEGROUND_TYPE_NONE);
        player->SetOriginalBGForPlayer(BATTLEGROUND_TYPE_NONE);

        if ((IsRated() || IsWarGame()) && player->GetTeam() != player->GetBGTeam())
            player->RemoveAurasDueToSpell(player->GetBGTeam() == ALLIANCE ? SPELL_FACTION_ALLIANCE : SPELL_FACTION_HORDE);

        player->SetBattlegroundId(0, BATTLEGROUND_TYPE_NONE);
        player->SetBGTeam(0);

        player->RemoveAurasDueToSpell(SPELL_ENTERING_BATTLEGROUND);

        if (IsArena())
            player->RemoveAurasDueToSpell(SPELL_ARENA_DAMPENING);

        if (Transport)
            player->TeleportToBGEntryPoint();

        TC_LOG_DEBUG("bg.battleground", "Removed player %s from Battleground.", player->GetName().c_str());
    }
}

void Battleground::Reset()
{
    SetWinner(WINNER_NONE);
    SetStatus(STATUS_WAIT_QUEUE);
    SetElapsedTime(0);
    SetRemainingTime(0);
    SetLastResurrectTime(0);
    m_Events = 0;
    SetRated(false);

    if (m_InvitedAlliance > 0 || m_InvitedHorde > 0)
        TC_LOG_ERROR("bg.battleground", "Battleground::Reset: one of the counters is not 0 (alliance: %u, horde: %u) for BG (map: %u, instance id: %u)!",
            m_InvitedAlliance, m_InvitedHorde, GetMapId(), GetInstanceID());

    m_InvitedAlliance = 0;
    m_InvitedHorde = 0;

    m_Players.clear();
    m_LeftPlayers.clear();

    for (auto scores : PlayerScores)
        delete scores.second;

    PlayerScores.clear();
    ResetBGSubclass();

    for (uint8 i = PLAYER_POSITION_ARENA_SLOT_NONE; i < MAX_PLAYER_POSITIONS; ++i)
        m_FlagKeepers[i].Clear();
}

void Battleground::StartBattleground()
{
    SetElapsedTime(0);
    SetLastResurrectTime(0);

    sBattlegroundMgr->AddBattleground(this);

    if (IsRated())
        TC_LOG_DEBUG("bg.arena", "Rated match type: %u started.", GetRatedType());
}

void Battleground::TeleportPlayerToExploitLocation(Player* player)
{
    if (WorldSafeLocsEntry const* loc = GetExploitTeleportLocation(Team(player->GetBGTeam())))
        player->TeleportTo(loc->MapID, loc->Loc.X, loc->Loc.Y, loc->Loc.Z, loc->Facing);
}

void Battleground::AddPlayer(Player* player)
{
    if (player->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_AFK))
        player->ToggleAFK();

    // score struct must be created in inherited class

    uint32 team = player->GetBGTeam();

    BattlegroundPlayer bp;
    bp.OfflineRemoveTime = 0;
    bp.Team = team;
    bp.ActiveSpec = player->GetSpecId(player->GetActiveTalentGroup());

    // Add to list/maps
    m_Players[player->GetGUID()] = bp;

    UpdatePlayersCountByTeam(team, false);

    WorldPacket data;
    sBattlegroundMgr->BuildPlayerJoinedBattlegroundPacket(&data, player->GetGUID());
    SendPacketToTeam(team, &data, player, false);

    BattlegroundQueueTypeId bgQueueTypeId = sBattlegroundMgr->BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), GetRatedType());
    uint32 queueSlot = player->GetBattlegroundQueueIndex(bgQueueTypeId);

    sBattlegroundMgr->BuildBattlegroundInProgressStatus(this, player, queueSlot, player->GetBattlegroundQueueJoinTime(bgQueueTypeId), GetElapsedTime(), GetRatedType());

    player->RemoveAurasByType(SPELL_AURA_MOUNTED);

    if (IsArena())
    {
        player->RemoveArenaEnchantments(TEMP_ENCHANTMENT_SLOT);
        if (team == ALLIANCE)
        {
            if (player->GetTeam() == HORDE)
                player->CastSpell(player, SPELL_HORDE_GOLD_FLAG, true);
            else
                player->CastSpell(player, SPELL_ALLIANCE_GOLD_FLAG, true);
        }
        else
        {
            if (player->GetTeam() == HORDE)
                player->CastSpell(player, SPELL_HORDE_GREEN_FLAG, true);
            else
                player->CastSpell(player, SPELL_ALLIANCE_GREEN_FLAG, true);
        }

        player->DestroyConjuredItems(true);
        player->UnsummonPetTemporaryIfAny();

        if (GetStatus() == STATUS_WAIT_JOIN)
        {
            player->CastSpell(player, SPELL_ARENA_PREPARATION, true);
            player->ResetAllPowers();
        }

        if (GetAlivePlayersCountByTeam(player->GetBGTeam()) == uint32(GetRatedType()))
            BuildArenaOpponentSpecializations(player);
    }
    else
    {
        if (GetStatus() == STATUS_WAIT_JOIN)
        {
            player->CastSpell(player, SPELL_PREPARATION, true);

            int32 countdownMaxForBGType = BATTLEGROUND_COUNTDOWN_MAX;
            uint32 tTime = (countdownMaxForBGType - (GetElapsedTime() / 1000));
            sBattlegroundMgr->HandleStartTimer(player, countdownMaxForBGType, tTime, 0);
        }
    }

    player->ResetCriteria(CRITERIA_CONDITION_BG_MAP, GetMapId(), true);

    PlayerAddedToBGCheckIfBGIsRunning(player);
    AddOrSetPlayerToCorrectBgGroup(player, team);

    player->CastSpell(player, SPELL_ENTERING_BATTLEGROUND, true);

    if ((IsRated() || IsWarGame()) && player->GetTeam() != player->GetBGTeam())
        player->CastSpell(player, player->GetBGTeam() == ALLIANCE ? SPELL_FACTION_ALLIANCE : SPELL_FACTION_HORDE, true);
}

void Battleground::AddOrSetPlayerToCorrectBgGroup(Player* player, uint32 team)
{
    ObjectGuid playerGuid = player->GetGUID();
    Group* group = GetBgRaid(team);
    if (!group)
    {
        group = new Group;
        SetBgRaid(team, group);
        group->Create(player);
    }
    else
    {
        if (group->IsMember(playerGuid))
        {
            uint8 subgroup = group->GetMemberGroup(playerGuid);
            player->SetBattlegroundOrBattlefieldRaid(group, subgroup);
        }
        else
        {
            group->AddMember(player);
            if (Group* originalGroup = player->GetOriginalGroup())
                if (originalGroup->IsLeader(playerGuid))
                {
                    group->ChangeLeader(playerGuid);
                    group->SendUpdate();
                }
        }
    }
}

void Battleground::EventPlayerLoggedIn(Player* player)
{
    ObjectGuid guid = player->GetGUID();
    // player is correct pointer
    for (GuidDeque::iterator itr = m_OfflineQueue.begin(); itr != m_OfflineQueue.end(); ++itr)
    {
        if (*itr == guid)
        {
            m_OfflineQueue.erase(itr);
            break;
        }
    }
    m_Players[guid].OfflineRemoveTime = 0;
    PlayerAddedToBGCheckIfBGIsRunning(player);
}

void Battleground::EventPlayerLoggedOut(Player* player)
{
    ObjectGuid guid = player->GetGUID();
    if (!IsPlayerInBattleground(guid))  // Check if this player really is in battleground (might be a GM who teleported inside)
        return;

    m_OfflineQueue.push_back(player->GetGUID());
    m_Players[guid].OfflineRemoveTime = GameTime::GetGameTime() + MAX_OFFLINE_TIME;
    if (GetStatus() == STATUS_IN_PROGRESS)
    {
        RemovePlayer(player, guid, GetPlayerTeam(guid));

        if (IsArena())
            if (GetAlivePlayersCountByTeam(player->GetBGTeam()) <= 1 && GetPlayersCountByTeam(GetOtherTeam(player->GetBGTeam())))
                EndBattleground(GetOtherTeam(player->GetBGTeam()));
    }
}

uint32 Battleground::GetFreeSlotsForTeam(uint32 Team) const
{
    if (GetStatus() == STATUS_WAIT_JOIN)
        return (GetInvitedCount(Team) < GetMaxPlayersPerTeam()) ? GetMaxPlayersPerTeam() - GetInvitedCount(Team) : 0;

    uint32 otherTeam = 0;
    uint32 otherIn = 0;

    if (Team == ALLIANCE)
    {
        otherTeam = GetInvitedCount(HORDE);
        otherIn = GetPlayersCountByTeam(HORDE);
    }
    else
    {
        otherTeam = GetInvitedCount(ALLIANCE);
        otherIn = GetPlayersCountByTeam(ALLIANCE);
    }

    if (GetStatus() == STATUS_IN_PROGRESS)
    {
        uint32 diff = 0;
        if (otherTeam == GetInvitedCount(Team))
            diff = 1;
        else if (otherTeam > GetInvitedCount(Team))
            diff = otherTeam - GetInvitedCount(Team);

        uint32 diff2 = (GetInvitedCount(Team) < GetMaxPlayersPerTeam()) ? GetMaxPlayersPerTeam() - GetInvitedCount(Team) : 0;
        uint32 diff3 = 0;
        if (otherIn == GetPlayersCountByTeam(Team))
            diff3 = 1;
        else if (otherIn > GetPlayersCountByTeam(Team))
            diff3 = otherIn - GetPlayersCountByTeam(Team);
        else if (GetInvitedCount(Team) <= GetMinPlayersPerTeam())
            diff3 = GetMinPlayersPerTeam() - GetInvitedCount(Team) + 1;

        diff = std::min(diff, diff2);
        return std::min(diff, diff3);
    }
    return 0;
}

uint32 Battleground::GetFreeSlots() const
{
    uint32 InvitedCount = 0;
    for (uint8 i = 0; i < BG_TEAMS_COUNT; ++i)
        InvitedCount += GetInvitedCount(TeamId(i));

    uint32 CurrentPlayersSize = GetPlayersSize() + InvitedCount;

    if (CurrentPlayersSize >= GetMaxPlayers())
        return 0;

    return GetMaxPlayers() - CurrentPlayersSize;
}

void Battleground::UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor)
{
    BattlegroundScoreMap::const_iterator itr = PlayerScores.find(Source->GetGUID());
    if (itr == PlayerScores.end())
        return;

    switch (type)
    {
        case SCORE_KILLING_BLOWS:
            itr->second->KillingBlows += value;
            break;
        case SCORE_DEATHS:
            itr->second->Deaths += value;
            break;
        case SCORE_HONORABLE_KILLS:
            itr->second->HonorableKills += value;
            break;
        case SCORE_BONUS_HONOR:
            if (IsBattleground())
            {
                if (doAddHonor)
                    Source->RewardHonor(NULL, 1, value);
                else
                    itr->second->BonusHonor += value;
            }
            break;
        case SCORE_DAMAGE_DONE:
            itr->second->DamageDone += value;
            break;
        case SCORE_HEALING_DONE:
            itr->second->HealingDone += value;
            break;
        case SCORE_PRE_MATCH_PERSONAL_RATING:
            itr->second->PreMatchPersonalRating = value;
            break;
        case SCORE_PRE_MATCH_MMR:
            itr->second->PreMatchMatchmakerRating = value;
            break;
        case SCORE_PERSONAL_RATING_CHANGE:
            itr->second->PersonalRatingChange = value;
            break;
        case SCORE_MMR_CHANGE:
            itr->second->MatchmakerRatingChange = value;
            break;
        default:
            TC_LOG_ERROR("bg.battleground", "Battleground::UpdatePlayerScore: unknown score type (%u) for BG (map: %u, instance id: %u)!", type, GetMapId(), GetInstanceID());
            break;
    }
}

void Battleground::AddPlayerToResurrectQueue(ObjectGuid npc_guid, ObjectGuid player_guid)
{
    m_ReviveQueue[npc_guid].push_back(player_guid);

    Player* player = ObjectAccessor::FindPlayer(player_guid);
    if (!player)
        return;

    player->CastSpell(player, SPELL_WAITING_FOR_RESURRECT, true);
}

void Battleground::RemovePlayerFromResurrectQueue(ObjectGuid player_guid)
{
    for (std::map<ObjectGuid, GuidVector>::iterator itr = m_ReviveQueue.begin(); itr != m_ReviveQueue.end(); ++itr)
    {
        for (GuidVector::iterator itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2)
        {
            if (*itr2 == player_guid)
            {
                itr->second.erase(itr2);
                if (Player* player = ObjectAccessor::FindPlayer(player_guid))
                    player->RemoveAurasDueToSpell(SPELL_WAITING_FOR_RESURRECT);
                return;
            }
        }
    }
}

void Battleground::SendFlagsPositions()
{
    std::unordered_map<uint8, Player*> players;

    for (uint8 i = PLAYER_POSITION_ARENA_SLOT_NONE; i < MAX_PLAYER_POSITIONS; ++i)
        if (Player* player = ObjectAccessor::FindPlayer(GetFlagPickerGUID(i)))
            players[i] = player;

    WorldPacket data(SMSG_BATTLEGROUND_PLAYER_POSITIONS, 3 + players.size() * (1 + 8 + 1 + 4 + 4 + 1));

    ByteBuffer buffer;

    data.WriteBits(players.size(), 20);

    for (auto itr = players.begin(); itr != players.end(); ++itr)
    {
        Player* player = itr->second;
        if (!player)
            continue;

        uint8 slot = itr->first;

        uint8 flagId = player->GetBGTeam() == ALLIANCE ? PLAYER_POSITION_BG_FLAG_HORDE : PLAYER_POSITION_BG_FLAG_ALLIANCE;

        ObjectGuid guid = player->GetGUID();

        data.WriteGuidMask(guid, 1, 5, 0, 4, 6, 3, 7, 2);

        buffer.WriteGuidBytes(guid, 0, 3, 7);

        buffer << uint8(flagId);

        buffer << float(player->GetPositionX());

        buffer.WriteGuidBytes(guid, 4, 2, 1, 5);

        buffer << float(player->GetPositionY());

        buffer << uint8(slot);

        buffer.WriteGuidBytes(guid, 6);
    }

    if (!buffer.empty())
        data.append(buffer);

    SendPacketToAll(&data);
}

void Battleground::RelocateDeadPlayers(ObjectGuid guideGuid)
{
    // Those who are waiting to resurrect at this node are taken to the closest own node's graveyard
    GuidVector& ghostList = m_ReviveQueue[guideGuid];
    if (!ghostList.empty())
    {
        WorldSafeLocsEntry const* closestGrave = NULL;
        for (GuidVector::const_iterator itr = ghostList.begin(); itr != ghostList.end(); ++itr)
        {
            Player* player = ObjectAccessor::FindPlayer(*itr);
            if (!player)
                continue;

            if (!closestGrave)
                closestGrave = GetClosestGraveYard(player);

            if (closestGrave)
                player->TeleportTo(GetMapId(), closestGrave->Loc.X, closestGrave->Loc.Y, closestGrave->Loc.Z, player->GetOrientation());
        }
        ghostList.clear();
    }
}

bool Battleground::AddObject(uint32 type, uint32 entry, float x, float y, float z, float o, float rotation0, float rotation1, float rotation2, float rotation3, uint32 /*respawnTime*/)
{
    ASSERT(type < BgObjects.size());

    Map* map = FindBgMap();
    if (!map)
        return false;

    G3D::Quat rot(rotation0, rotation1, rotation2, rotation3);
    // Temporally add safety check for bad spawns and send log (object rotations need to be rechecked in sniff)
    if (!rotation0 && !rotation1 && !rotation2 && !rotation3)
    {
        TC_LOG_DEBUG("bg.battleground", "Battleground::AddObject: gameoobject [entry: %u, object type: %u] for BG (map: %u) has zeroed rotation fields, "
            "orientation used temporally, but please fix the spawn", entry, type, m_MapId);

        rot = G3D::Matrix3::fromEulerAnglesZYX(o, 0.f, 0.f);
    }

    // Must be created this way, adding to godatamap would add it to the base map of the instance
    // and when loading it (in go::LoadFromDB()), a new guid would be assigned to the object, and a new object would be created
    // So we must create it specific for this instance
    GameObject* go = nullptr;
    if (sObjectMgr->GetGameObjectTypeByEntry(entry) == GAMEOBJECT_TYPE_TRANSPORT)
        go = new Transport();
    else
        go = new GameObject();

    if (!go->Create(entry, GetBgMap(), Position(x, y, z, o), rot, 255, GO_STATE_READY))
    {
        TC_LOG_ERROR("bg.battleground", "Battleground::AddObject: cannot create gameobject (entry: %u) for BG (map: %u, instance id: %u)!", entry, GetMapId(), GetInstanceID());
        delete go;
        return false;
    }

    go->InitModel();

    if (!map->AddToMap(go))
    {
        delete go;
        return false;
    }

    BgObjects[type] = go->GetGUID();
    return true;
}

// Some doors aren't despawned so we cannot handle their closing in gameobject::update()
// It would be nice to correctly implement GO_ACTIVATED state and open/close doors in gameobject code
void Battleground::DoorClose(uint32 type)
{
    if (GameObject* obj = GetBgMap()->GetGameObject(BgObjects[type]))
    {
        if (obj->GetLootState() == GO_ACTIVATED && obj->GetGoState() != GO_STATE_READY)
        {
            obj->SetLootState(GO_READY);
            obj->SetGoState(GO_STATE_READY);
        }
    }
    else
        TC_LOG_ERROR("bg.battleground", "Battleground::DoorClose: door gameobject (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
            type, BgObjects[type].GetCounter(), m_MapId, m_InstanceID);
}

void Battleground::DoorOpen(uint32 type)
{
    if (GameObject* obj = GetBgMap()->GetGameObject(BgObjects[type]))
    {
        obj->SetLootState(GO_ACTIVATED);
        obj->SetGoState(GO_STATE_ACTIVE);
    }
    else
        TC_LOG_ERROR("bg.battleground", "Battleground::DoorOpen: door gameobject (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
            type, BgObjects[type].GetCounter(), m_MapId, m_InstanceID);
}

GameObject* Battleground::GetBGObject(uint32 type, bool logError)
{
    GameObject* obj = GetBgMap()->GetGameObject(BgObjects[type]);
    if (!obj)
    {
        if (logError)
            TC_LOG_ERROR("bg.battleground", "Battleground::GetBGObject: gameobject (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
                type, BgObjects[type].GetCounter(), m_MapId, m_InstanceID);
        else
            TC_LOG_INFO("bg.battleground", "Battleground::GetBGObject: gameobject (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
                type, BgObjects[type].GetCounter(), m_MapId, m_InstanceID);
    }
    return obj;
}

Creature* Battleground::GetBGCreature(uint32 type, bool logError)
{
    Creature* creature = GetBgMap()->GetCreature(BgCreatures[type]);
    if (!creature)
    {
        if (logError)
            TC_LOG_ERROR("bg.battleground", "Battleground::GetBGCreature: creature (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
                type, BgCreatures[type].GetCounter(), m_MapId, m_InstanceID);
        else
            TC_LOG_INFO("bg.battleground", "Battleground::GetBGCreature: creature (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
                type, BgCreatures[type].GetCounter(), m_MapId, m_InstanceID);
    }
    return creature;
}

void Battleground::SpawnBGObject(uint32 type, uint32 respawntime)
{
    if (Map* map = FindBgMap())
        if (GameObject* obj = map->GetGameObject(BgObjects[type]))
        {
            if (respawntime)
                obj->SetLootState(GO_JUST_DEACTIVATED);
            else
                if (obj->GetLootState() == GO_JUST_DEACTIVATED)
                    obj->SetLootState(GO_READY);
            obj->SetRespawnTime(respawntime);
            map->AddToMap(obj);
        }
}

Creature* Battleground::AddCreature(uint32 entry, uint32 type, float x, float y, float z, float o, uint32 respawntime)
{
    ASSERT(type < BgCreatures.size());

    Map* map = FindBgMap();
    if (!map)
        return NULL;

    Creature* creature = new Creature();
    if (!creature->Create(map->GenerateLowGuid<HighGuid::Creature>(), map, entry, 0, x, y, z, o))
    {
        TC_LOG_ERROR("bg.battleground", "Battleground::AddCreature: cannot create creature (entry: %u) for BG (map: %u, instance id: %u)!", entry, GetMapId(), GetInstanceID());
        delete creature;
        return NULL;
    }

    creature->SetHomePosition(x, y, z, o);

    CreatureTemplate const* cinfo = sObjectMgr->GetCreatureTemplate(entry);
    if (!cinfo)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground::AddCreature: creature template (entry: %u) does not exist for BG (map: %u, instance id: %u)!", entry, GetMapId(), GetInstanceID());
        delete creature;
        return NULL;
    }
    creature->SetSpeedRate(MOVE_WALK, cinfo->speed_walk);
    creature->SetSpeedRate(MOVE_RUN, cinfo->speed_run);

    if (!map->AddToMap(creature))
    {
        delete creature;
        return NULL;
    }

    BgCreatures[type] = creature->GetGUID();

    if (respawntime)
        creature->SetRespawnDelay(respawntime);

    return creature;
}

bool Battleground::DelCreature(uint32 type)
{
    if (!BgCreatures[type])
        return true;

    if (Creature* creature = GetBgMap()->GetCreature(BgCreatures[type]))
    {
        creature->AddObjectToRemoveList();
        BgCreatures[type].Clear();
        return true;
    }

    TC_LOG_ERROR("bg.battleground", "Battleground::DelCreature: creature (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
        type, BgCreatures[type].GetCounter(), m_MapId, m_InstanceID);
    BgCreatures[type].Clear();
    return false;
}

bool Battleground::DelObject(uint32 type)
{
    if (!BgObjects[type])
        return true;

    if (GameObject* obj = GetBgMap()->GetGameObject(BgObjects[type]))
    {
        obj->SetRespawnTime(0);
        obj->Delete();
        BgObjects[type].Clear();
        return true;
    }
    TC_LOG_ERROR("bg.battleground", "Battleground::DelObject: gameobject (type: %u, GUID: %u) not found for BG (map: %u, instance id: %u)!",
        type, BgObjects[type].GetCounter(), m_MapId, m_InstanceID);
    BgObjects[type].Clear();
    return false;
}

bool Battleground::AddSpiritGuide(uint32 type, float x, float y, float z, float o, uint32 teamId /*= 0*/)
{
    uint32 entry = (Team(teamId) == ALLIANCE) ? BG_CREATURE_ENTRY_A_SPIRITGUIDE : BG_CREATURE_ENTRY_H_SPIRITGUIDE;

    if (Creature* creature = AddCreature(entry, type, x, y, z, o))
    {
        creature->SetDeathState(DEAD);
        creature->SetChannelObjectGuid(creature->GetGUID());
        // aura
        /// @todo Fix display here
        // creature->SetVisibleAura(0, SPELL_SPIRIT_HEAL_CHANNEL);
        // casting visual effect
        creature->SetUInt32Value(UNIT_CHANNEL_SPELL, SPELL_SPIRIT_HEAL_CHANNEL);
        // correct cast speed
        creature->SetFloatValue(UNIT_MOD_CASTING_SPEED, 1.0f);
        //creature->CastSpell(creature, SPELL_SPIRIT_HEAL_CHANNEL, true);
        return true;
    }
    TC_LOG_ERROR("bg.battleground", "Battleground::AddSpiritGuide: cannot create spirit guide (type: %u, entry: %u) for BG (map: %u, instance id: %u)!", type, entry, GetMapId(), GetInstanceID());
    EndNow();
    return false;
}

void Battleground::SendMessageToAll(uint32 entry, ChatMsg type, Player const* source)
{
    if (!entry)
        return;

    Trinity::BattlegroundChatBuilder bg_builder(type, entry, source);
    Trinity::LocalizedPacketDo<Trinity::BattlegroundChatBuilder> bg_do(bg_builder);
    BroadcastWorker(bg_do);
}

void Battleground::PSendMessageToAll(uint32 entry, ChatMsg type, Player const* source, ...)
{
    if (!entry)
        return;

    va_list ap;
    va_start(ap, source);

    Trinity::BattlegroundChatBuilder bg_builder(type, entry, source, &ap);
    Trinity::LocalizedPacketDo<Trinity::BattlegroundChatBuilder> bg_do(bg_builder);
    BroadcastWorker(bg_do);

    va_end(ap);
}

void Battleground::SendWarningToAll(uint32 entry, ...)
{
    if (!entry)
        return;

    std::map<uint32, WorldPacket> localizedPackets;

    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
        {
            if (localizedPackets.find(player->GetSession()->GetSessionDbLocaleIndex()) == localizedPackets.end())
            {
                char const* format = sObjectMgr->GetTrinityString(entry, player->GetSession()->GetSessionDbLocaleIndex());

                char str[1024];
                va_list ap;
                va_start(ap, entry);
                vsnprintf(str, 1024, format, ap);
                va_end(ap);

                ChatHandler::BuildChatPacket(localizedPackets[player->GetSession()->GetSessionDbLocaleIndex()], CHAT_MSG_RAID_BOSS_EMOTE, LANG_UNIVERSAL, NULL, NULL, str);
            }
            player->SendDirectMessage(&localizedPackets[player->GetSession()->GetSessionDbLocaleIndex()]);
        }
}

void Battleground::SendWarning2ToAll(uint32 entry, uint32 strId1, uint32 strId2)
{
    Trinity::Battleground3ChatBuilder bg_builder(entry, strId1, strId2);
    Trinity::LocalizedPacketDo<Trinity::Battleground3ChatBuilder> bg_do(bg_builder);
    BroadcastWorker(bg_do);
}

void Battleground::SendMessage2ToAll(uint32 entry, ChatMsg type, Player const* source, uint32 arg1, uint32 arg2)
{
    Trinity::Battleground2ChatBuilder bg_builder(type, entry, source, arg1, arg2);
    Trinity::LocalizedPacketDo<Trinity::Battleground2ChatBuilder> bg_do(bg_builder);
    BroadcastWorker(bg_do);
}

void Battleground::EndNow()
{
    SetStatus(STATUS_WAIT_LEAVE);
    SetRemainingTime(0);
}

// IMPORTANT NOTICE:
// buffs aren't spawned/despawned when players captures anything
// buffs are in their positions when battleground starts
void Battleground::HandleTriggerBuff(ObjectGuid go_guid)
{
    GameObject* obj = GetBgMap()->GetGameObject(go_guid);
    if (!obj || obj->GetGoType() != GAMEOBJECT_TYPE_TRAP || !obj->IsSpawned())
        return;

    int32 index = BgObjects.size() - 1;
    while (index >= 0 && BgObjects[index] != go_guid)
        index--;
    if (index < 0)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground::HandleTriggerBuff: cannot find buff gameobject (GUID: %u, entry: %u, type: %u) in internal data for BG (map: %u, instance id: %u)!",
            go_guid.GetCounter(), obj->GetEntry(), obj->GetGoType(), m_MapId, m_InstanceID);
        return;
    }

    uint8 buff = Math::Rand(0, 2);
    uint32 entry = obj->GetEntry();
    if (m_BuffChange && entry != Buff_Entries[buff])
    {
        SpawnBGObject(index, RESPAWN_ONE_DAY);
        for (uint8 currBuffTypeIndex = 0; currBuffTypeIndex < 3; ++currBuffTypeIndex)
            if (entry == Buff_Entries[currBuffTypeIndex])
            {
                index -= currBuffTypeIndex;
                index += buff;
            }
    }

    SpawnBGObject(index, BUFF_RESPAWN_TIME);
}

void Battleground::HandleKillPlayer(Player* victim, Player* killer)
{
    UpdatePlayerScore(victim, SCORE_DEATHS, 1);
    if (killer)
    {
        if (killer == victim)
            return;

        UpdatePlayerScore(killer, SCORE_HONORABLE_KILLS, 1);
        UpdatePlayerScore(killer, SCORE_KILLING_BLOWS, 1);

        for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        {
            Player* creditedPlayer = ObjectAccessor::FindPlayer(itr->first);
            if (!creditedPlayer || creditedPlayer == killer)
                continue;

            if (creditedPlayer->GetBGTeam() == killer->GetBGTeam() && creditedPlayer->IsAtGroupRewardDistance(victim))
                UpdatePlayerScore(creditedPlayer, SCORE_HONORABLE_KILLS, 1);
        }
    }

    if (!IsArena())
    {
        victim->SetFlag(UNIT_FLAGS, UNIT_FLAG_SKINNABLE);
        RewardXPAtKill(killer, victim);
    }
}

// Return the player's team based on battlegroundplayer info
// Used in same faction arena matches mainly
uint32 Battleground::GetPlayerTeam(ObjectGuid guid) const
{
    BattlegroundPlayerMap::const_iterator itr = m_Players.find(guid);
    if (itr != m_Players.end())
        return itr->second.Team;
    return 0;
}

uint32 Battleground::GetOtherTeam(uint32 teamId) const
{
    return teamId ? ((teamId == ALLIANCE) ? HORDE : ALLIANCE) : 0;
}

bool Battleground::IsPlayerInBattleground(ObjectGuid guid) const
{
    BattlegroundPlayerMap::const_iterator itr = m_Players.find(guid);
    if (itr != m_Players.end())
        return true;
    return false;
}

void Battleground::PlayerAddedToBGCheckIfBGIsRunning(Player* player)
{
    if (GetStatus() != STATUS_WAIT_LEAVE)
        return;

    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), GetRatedType());

    BlockMovement(player);

    WorldPacket data;
    sBattlegroundMgr->BuildPvpLogDataPacket(&data, this);
    player->SendDirectMessage(&data);

    sBattlegroundMgr->BuildBattlegroundInProgressStatus(this, player, player->GetBattlegroundQueueIndex(bgQueueTypeId), player->GetBattlegroundQueueJoinTime(bgQueueTypeId), GetElapsedTime(), GetRatedType());
}

uint32 Battleground::GetAlivePlayersCountByTeam(uint32 Team) const
{
    int count = 0;
    for (auto players : m_Players)
    {
        if (players.second.Team == Team)
        {
            Player* player = ObjectAccessor::FindPlayer(players.first);
            if (player && player->IsAlive() && !player->HasByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_SHAPESHIFT_FORM, FORM_SPIRIT_OF_REDEMPTION))
                ++count;
        }
    }
    return count;
}

void Battleground::SetHoliday(bool is_holiday)
{
    m_HonorMode = is_holiday ? BG_HOLIDAY : BG_NORMAL;
}

int32 Battleground::GetObjectType(ObjectGuid guid)
{
    for (uint32 i = 0; i < BgObjects.size(); ++i)
        if (BgObjects[i] == guid)
            return i;

    TC_LOG_ERROR("bg.battleground", "Battleground::GetObjectType: player used gameobject (GUID: %u) which is not in internal data for BG (map: %u, instance id: %u), cheating?",
        guid.GetCounter(), m_MapId, m_InstanceID);
    return -1;
}

void Battleground::HandleKillUnit(Creature* /*victim*/, Player* /*killer*/) { }

void Battleground::CheckArenaAfterTimerConditions()
{
    // Let's do the draw!
    uint32 winner = Math::Rand(WINNER_HORDE, WINNER_ALLIANCE);
    EndBattleground(winner);
}

void Battleground::CheckArenaWinConditions()
{
    if (!GetAlivePlayersCountByTeam(ALLIANCE) && GetPlayersCountByTeam(HORDE))
        EndBattleground(HORDE);
    else if (GetPlayersCountByTeam(ALLIANCE) && !GetAlivePlayersCountByTeam(HORDE))
        EndBattleground(ALLIANCE);
}

void Battleground::UpdateArenaWorldState()
{
    UpdateWorldState(0xe10, GetAlivePlayersCountByTeam(HORDE));
    UpdateWorldState(0xe11, GetAlivePlayersCountByTeam(ALLIANCE));
}

void Battleground::SetBgRaid(uint32 TeamID, Group* bg_raid)
{
    Group*& old_raid = TeamID == ALLIANCE ? m_BgRaids[TEAM_ALLIANCE] : m_BgRaids[TEAM_HORDE];
    if (old_raid)
        old_raid->SetBattlegroundGroup(NULL);
    if (bg_raid)
        bg_raid->SetBattlegroundGroup(this);
    old_raid = bg_raid;
}

WorldSafeLocsEntry const* Battleground::GetClosestGraveYard(Player* player)
{
    return sObjectMgr->GetClosestGraveYard(player, player->GetBGTeam());
}

void Battleground::StartCriteriaTimer(CriteriaTimedTypes type, uint32 entry)
{
    for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
            player->StartCriteriaTimer(type, entry);
}

void Battleground::SetBracket(PvPDifficultyEntry const* bracketEntry)
{
    m_BracketId = bracketEntry->GetBracketId();
    SetLevelRange(bracketEntry->MinLevel, bracketEntry->MaxLevel);
}

void Battleground::RewardXPAtKill(Player* killer, Player* victim)
{
    if (sWorld->getBoolConfig(CONFIG_BG_XP_FOR_KILL) && killer && victim)
        killer->RewardPlayerAndGroupAtKill(victim, true);
}

uint32 Battleground::GetTeamScore(uint32 teamId) const
{
    if (teamId == TEAM_ALLIANCE || teamId == TEAM_HORDE)
        return m_TeamScores[teamId];

    TC_LOG_ERROR("bg.battleground", "GetTeamScore with wrong Team %u for BG %u", teamId, GetTypeID());
    return 0;
}

void Battleground::HandleAreaTrigger(Player* player, uint32 trigger, bool /*entered*/)
{
    TC_LOG_DEBUG("bg.battleground", "Unhandled AreaTrigger %u in Battleground %u. Player coords (x: %f, y: %f, z: %f)", trigger, player->GetMapId(), player->GetPositionX(), player->GetPositionY(), player->GetPositionZ());
}

void Battleground::UpdateAverageMMR()
{
    for (uint8 team = 0; team < BG_TEAMS_COUNT; ++team)
    {
        Group* group = m_BgRaids[team];
        if (!group)
            return;

        GroupRatedStats statsByGroup = group->GetRatedStats(GetRatedType());
        SetTeamMatchmakerRating(team, statsByGroup.averageMMR);
    }
}

void Battleground::BuildArenaOpponentSpecializations(Player* player)
{
    uint32 team = player->GetBGTeam();

    std::vector<Player*> teamMates;

    for (BattlegroundPlayerMap::const_iterator itr = m_Players.begin(); itr != m_Players.end(); ++itr)
        if (Player* player = _GetPlayerForTeam(team, itr, "BuildArenaOpponentSpecializations"))
            teamMates.push_back(player);

    if (teamMates.empty())
        return;

    ByteBuffer dataBuffer;

    WorldPacket data(SMSG_ARENA_OPPONENTS_SPECIALIZATION, 3 + teamMates.size() * (1 + 8 + 4));

    data.WriteBits(teamMates.size(), 21);

    for (Player* player : teamMates)
    {
        ObjectGuid guid = player->GetGUID();

        data.WriteGuidMask(guid, 0, 2, 1, 6, 7, 3, 5, 4);

        dataBuffer.WriteGuidBytes(guid, 5, 1, 7);

        dataBuffer << int32(player->GetSpecId(player->GetActiveTalentGroup()));

        dataBuffer.WriteGuidBytes(guid, 6, 3, 0, 2, 4);
    }

    data.FlushBits();

    data.append(dataBuffer);

    SendPacketToTeam(GetOtherTeam(team), &data, player, false);
}
