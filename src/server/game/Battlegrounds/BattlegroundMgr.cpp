/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "ObjectMgr.h"
#include "World.h"
#include "WorldPacket.h"
#include "RatedMgr.h"
#include "RatedInfo.h"
#include "BattlegroundMgr.h"
#include "BattlegroundAV.h"
#include "BattlegroundAB.h"
#include "BattlegroundEY.h"
#include "BattlegroundWS.h"
#include "BattlegroundNA.h"
#include "BattlegroundBE.h"
#include "BattlegroundRL.h"
#include "BattlegroundSA.h"
#include "BattlegroundDS.h"
#include "BattlegroundIC.h"
#include "BattlegroundBFG.h"
#include "BattlegroundTP.h"
#include "BattlegroundTK.h"
#include "BattlegroundDG.h"
#include "BattlegroundSM.h"
#include "BattlegroundTV.h"
#include "BattlegroundTTP.h"
#include "Chat.h"
#include "Map.h"
#include "MapInstanced.h"
#include "MapManager.h"
#include "Player.h"
#include "GameEventMgr.h"
#include "SharedDefines.h"
#include "Formulas.h"
#include "DisableMgr.h"
#include "Opcodes.h"
#include "Group.h"

/*********************************************************/
/***            BATTLEGROUND MANAGER                   ***/
/*********************************************************/

BattlegroundMgr::BattlegroundMgr() :
m_NextRatedArenaUpdate(sWorld->getIntConfig(CONFIG_ARENA_RATED_UPDATE_TIMER)), m_ArenaTesting(false), m_Testing(false), _WarGameId(1), m_UpdateTimer(0)
{
    m_BattlegroundQueueUpdateTimer = 30 * IN_MILLISECONDS;
}

BattlegroundMgr::~BattlegroundMgr()
{
    DeleteAllBattlegrounds();
}

void BattlegroundMgr::DeleteAllBattlegrounds()
{
    for (BattlegroundDataContainer::iterator itr1 = bgDataStore.begin(); itr1 != bgDataStore.end(); ++itr1)
    {
        BattlegroundData& data = itr1->second;

        while (!data.m_Battlegrounds.empty())
            delete data.m_Battlegrounds.begin()->second;
        data.m_Battlegrounds.clear();
    }

    bgDataStore.clear();
}

void BattlegroundMgr::Update(uint32 diff)
{
    m_UpdateTimer += diff;
    if (m_UpdateTimer > BATTLEGROUND_OBJECTIVE_UPDATE_INTERVAL)
    {
        for (BattlegroundDataContainer::iterator itr1 = bgDataStore.begin(); itr1 != bgDataStore.end(); ++itr1)
        {
            BattlegroundContainer& bgs = itr1->second.m_Battlegrounds;
            BattlegroundContainer::iterator itrDelete = bgs.begin();
            // first one is template and should not be deleted
            for (BattlegroundContainer::iterator itr = ++itrDelete; itr != bgs.end();)
            {
                itrDelete = itr++;
                Battleground* bg = itrDelete->second;

                bg->Update(m_UpdateTimer);

                if (bg->ToBeDeleted())
                {
                    itrDelete->second = nullptr;
                    bgs.erase(itrDelete);
                    BattlegroundClientIdsContainer& clients = itr1->second.m_ClientBattlegroundIds[bg->GetBracketId()];
                    if (!clients.empty())
                        clients.erase(bg->GetClientInstanceID());

                    delete bg;
                }
            }
        }

        m_UpdateTimer = 0;
    }

    for (int qtype = BATTLEGROUND_QUEUE_NONE; qtype < MAX_BATTLEGROUND_QUEUE_TYPES; ++qtype)
        m_BattlegroundQueues[qtype].UpdateEvents(diff);

    // update battleground queues every 30 seconds
    if (m_BattlegroundQueueUpdateTimer <= diff)
    {
        // First, fill players to less-populated bgs

        // check if any running battlegrounds have free slots
        std::unordered_map<uint8, std::vector<BattlegroundRunning>> battlegroundsWithFreeSlots;

        for (BattlegroundDataContainer::iterator itr = bgDataStore.begin(); itr != bgDataStore.end(); ++itr)
        {
            BattlegroundContainer& bgs = itr->second.m_Battlegrounds;
            if (!bgs.empty())
            {
                for (BattlegroundContainer::iterator itr = bgs.begin(); itr != bgs.end(); ++itr)
                    if (Battleground* bg = itr->second)
                        if (!bg->IsRated() && !bg->IsWarGame() && bg->GetStatus() > STATUS_WAIT_QUEUE && bg->GetStatus() < STATUS_WAIT_LEAVE)
                        {
                            uint32 freeSlots = bg->GetFreeSlots();

                            if (!freeSlots)
                                continue;

                            PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketById(bg->GetMapId(), bg->GetBracketId());
                            if (!bracketEntry)
                                continue;

                            BattlegroundQueueTypeId bgQueueTypeId = BGQueueTypeId(bg->GetTypeID(), RATED_TYPE_NOT_RATED);

                            BattlegroundRunning bgWithFreeSlots;
                            bgWithFreeSlots.battleground = bg;
                            bgWithFreeSlots.bracketID = bg->GetBracketId();
                            bgWithFreeSlots.queueTypeID = bgQueueTypeId;
                            bgWithFreeSlots.freeSlots = freeSlots;

                            battlegroundsWithFreeSlots[bracketEntry->MinLevel].push_back(bgWithFreeSlots);
                        }
            }
        }

        if (!battlegroundsWithFreeSlots.empty())
        {
            // for every bracket check bgs with free slots, sort them and fill players
            for (uint8 i = BG_BRACKET_ID_FIRST; i < MAX_BATTLEGROUND_BRACKETS; ++i)
            {
                uint8 level = 10 + (i * 5);

                if (battlegroundsWithFreeSlots[level].empty())
                    continue;

                // sort running battleground by free slots
                if (battlegroundsWithFreeSlots[level].size() > 1)
                    std::sort(battlegroundsWithFreeSlots[level].begin(), battlegroundsWithFreeSlots[level].end(), BattlegroundRunningFreeSlotsOrder());

                for (auto & itr : battlegroundsWithFreeSlots[level])
                {
                    BattlegroundQueueTypeId bgQueueTypeId = itr.queueTypeID;
                    BattlegroundBracketId bracket_id = itr.bracketID;
                    Battleground* bg = itr.battleground;
                    if (!bg)
                        continue;

                    m_BattlegroundQueues[bgQueueTypeId].FillPlayersToBG(bg, bracket_id);
                }
            }
        }

        if (!m_QueueUpdateScheduler.empty())
        {
            // And now create new battlegrounds, if possible

            GuidVector scheduled;
            std::swap(scheduled, m_QueueUpdateScheduler);

            std::unordered_map<uint8, std::vector<BattlegroundQueueStarted>> battlegroundsToStart;

            for (ObjectGuid schedule : scheduled)
            {
                uint32 arenaMMRating = schedule >> 48;
                RatedType ratedType = RatedType(schedule >> 40 & 0xFF);
                BattlegroundQueueTypeId bgQueueTypeId = BattlegroundQueueTypeId(schedule >> 24 & 0xFFFF);
                BattlegroundTypeId bgTypeId = BattlegroundTypeId((schedule >> 8) & 0xFFFF);
                BattlegroundBracketId bracket_id = BattlegroundBracketId(schedule & 0xFF);

                Battleground* bg_template = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeId);
                if (!bg_template)
                    continue;

                PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketById(bg_template->GetMapId(), bracket_id);
                if (!bracketEntry)
                    continue;

                // check if have any players ready to join the bg
                uint32 maxPlayers = m_BattlegroundQueues[bgQueueTypeId].BattlegroundQueueUpdate(bg_template, bracketEntry, ratedType, ratedType != RATED_TYPE_NOT_RATED, arenaMMRating);

                if (maxPlayers > 0)
                {
                    BattlegroundQueueStarted queueStarted;
                    queueStarted.queueTypeID = bgQueueTypeId;
                    queueStarted.typeID = bgTypeId;
                    queueStarted.bracketID = bracket_id;
                    queueStarted.countIndex = maxPlayers / 15;

                    battlegroundsToStart[bracketEntry->MinLevel].push_back(queueStarted);
                }
            }

            if (!battlegroundsToStart.empty())
            {
                // for every bracket check possible new battlegrounds, sort them and start
                for (uint8 i = BG_BRACKET_ID_FIRST; i < MAX_BATTLEGROUND_BRACKETS; ++i)
                {
                    uint8 level = 10 + (i * 5);

                    if (battlegroundsToStart[level].empty())
                        continue;

                    std::vector<BattlegroundQueueStarted> sortedBattlegrounds;

                    // Sort battlegrounds by players count (big battlegrounds have prior)...
                    // ... but hey! We have 50% chance of shuffle the list!
                    if (Math::RollUnder(50.0f))
                    {
                        // sorted by countIndex
                        // 0 - small bgs (WSG), 1 - normal (AB), 2 - big (AV)
                        std::unordered_map<uint8, std::vector<BattlegroundQueueStarted>> countedBattlegrounds;
                        for (auto & itr : battlegroundsToStart[level])
                            countedBattlegrounds[itr.countIndex].push_back(itr);

                        // sort by count - big bgs (2) first
                        for (int j = 2; j >= 0; --j)
                        {
                            std::random_shuffle(countedBattlegrounds[j].begin(), countedBattlegrounds[j].end());
                            for (auto & itr : countedBattlegrounds[j])
                                sortedBattlegrounds.push_back(itr);
                        }
                    }
                    else
                    {
                        std::random_shuffle(battlegroundsToStart[level].begin(), battlegroundsToStart[level].end());
                        for (auto & itr : battlegroundsToStart[level])
                            sortedBattlegrounds.push_back(itr);
                    }

                    for (auto & itr : sortedBattlegrounds)
                    {
                        BattlegroundQueueTypeId bgQueueTypeId = itr.queueTypeID;
                        BattlegroundTypeId bgTypeId = itr.typeID;
                        BattlegroundBracketId bracket = itr.bracketID;

                        m_BattlegroundQueues[bgQueueTypeId].StartBattleground(bgTypeId, bracket, false);
                    }
                }
            }
        }

        m_BattlegroundQueueUpdateTimer = 30 * IN_MILLISECONDS;
    }
    else
        m_BattlegroundQueueUpdateTimer -= diff;

    if (sWorld->getIntConfig(CONFIG_ARENA_MAX_RATING_DIFFERENCE) && sWorld->getIntConfig(CONFIG_ARENA_RATED_UPDATE_TIMER))
    {
        if (m_NextRatedArenaUpdate < diff)
        {
            TC_LOG_TRACE("bg.arena", "BattlegroundMgr: UPDATING ARENA QUEUES");
            for (int qtype = BATTLEGROUND_QUEUE_2v2; qtype <= BATTLEGROUND_QUEUE_5v5; ++qtype)
                for (int bracket = BG_BRACKET_ID_FIRST; bracket < MAX_BATTLEGROUND_BRACKETS; ++bracket)
                {
                    Battleground* bg_template = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
                    if (!bg_template)
                        continue;

                    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketById(bg_template->GetMapId(), BattlegroundBracketId(bracket));
                    if (!bracketEntry)
                        continue;

                    m_BattlegroundQueues[qtype].BattlegroundQueueUpdate(bg_template, bracketEntry, BattlegroundMgr::GetRatedTypeByQueue(BattlegroundQueueTypeId(qtype)), true, 0);
                }

            m_NextRatedArenaUpdate = sWorld->getIntConfig(CONFIG_ARENA_RATED_UPDATE_TIMER);
        }
        else
            m_NextRatedArenaUpdate -= diff;
    }
}

void BattlegroundMgr::BuildBattlegroundNoneStatus(Player* player, uint32 QueueSlot, uint32 JoinTime, RatedType ratedType)
{
    ObjectGuid playerGuid = player->GetGUID();

    WorldPacket data(SMSG_BATTLEGROUND_STATUS, 1 + 8 + 4 + 4 + 4);

    data << uint32(JoinTime);
    data << uint32(ratedType);
    data << uint32(QueueSlot);

    data.WriteGuidMask(playerGuid, 2, 0, 4, 5, 3, 7, 1, 6);
    data.WriteGuidBytes(playerGuid, 3, 5, 6, 4, 2, 0, 1, 7);

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::BuildBattlegroundWaitQueueStatus(Battleground* bg, Player* player, uint32 QueueSlot, uint32 Time1, uint32 JoinTime, RatedType ratedType, bool AsGroup, bool SuspendQueue)
{
    ObjectGuid playerGuid = player->GetGUID();
    ObjectGuid queueId = bg->GetQueueId();
    uint32 ClientInstanceId = bg->GetClientInstanceID();

    bool IsSuspendedQueue = SuspendQueue;
    bool EligibleForMatchmaking = true; // always true

    WorldPacket data(SMSG_BATTLEGROUND_STATUS_QUEUED, 2 * (1 + 8) + 1 + 4 + 4 + 1 + 4 + 1 + 4 + 4 + 1 + 4);

    data.WriteGuidMask(queueId, 1, 5);

    data.WriteGuidMask(playerGuid, 0);

    data.WriteGuidMask(queueId, 7);

    data.WriteBit(bg->IsRated());
    data.WriteBit(AsGroup);
    data.WriteBit(EligibleForMatchmaking);

    data.WriteGuidMask(playerGuid, 6, 4, 1, 3, 7, 5);

    data.WriteBit(IsSuspendedQueue);

    data.WriteGuidMask(queueId, 3, 0, 2);

    data.WriteGuidMask(playerGuid, 2);

    data.WriteGuidMask(queueId, 6, 4);

    data.FlushBits();

    data.WriteGuidBytes(queueId, 4);

    data.WriteGuidBytes(playerGuid, 6);

    data << uint32(ratedType);

    data.WriteGuidBytes(queueId, 5);

    data << uint32(Time1);

    data.WriteGuidBytes(queueId, 3);

    data.WriteGuidBytes(playerGuid, 3, 4, 0);

    data << uint8(bg->GetMinLevel());

    data.WriteGuidBytes(queueId, 0);

    data << uint32(JoinTime);
    data << uint8(bg->IsArena() ? ratedType : 1);

    data.WriteGuidBytes(queueId, 1);

    data << uint32(GetMSTimeDiffToNow(JoinTime));

    data.WriteGuidBytes(queueId, 7);

    data << uint32(QueueSlot);

    data.WriteGuidBytes(playerGuid, 2);

    data.WriteGuidBytes(queueId, 6);

    data << uint8(bg->GetMaxLevel());

    data.WriteGuidBytes(queueId, 2);

    data.WriteGuidBytes(playerGuid, 5, 1);

    data << uint32(ClientInstanceId);

    data.WriteGuidBytes(playerGuid, 7);

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::BuildBattlegroundWaitJoinStatus(Battleground* bg, Player* player, uint32 QueueSlot, uint32 Time1, uint32 JoinTime, RatedType ratedType, uint8 Role)
{
    ObjectGuid playerGuid = player->GetGUID();
    ObjectGuid queueId = bg->GetQueueId();
    uint32 ClientInstanceId = bg->GetClientInstanceID();

    bool HasAssignedRole = Role != BG_ROLE_DPS;

    WorldPacket data(SMSG_BATTLEGROUND_STATUS_NEEDCONFIRMATION, 2 * (1 + 8) + 1 + 1 + 4 + (HasAssignedRole ? 1 : 0) + 4 + 4 + 1 + 4 + 1 + 4 + 4);

    data.WriteGuidMask(playerGuid, 7, 5, 4, 1);

    data.WriteGuidMask(queueId, 3, 2);

    data.WriteBit(!HasAssignedRole);

    data.WriteGuidMask(queueId, 0);

    data.WriteGuidMask(playerGuid, 0, 6);

    data.WriteGuidMask(queueId, 7, 4, 1);

    data.WriteBit(bg->IsRated());

    data.WriteGuidMask(playerGuid, 2);

    data.WriteGuidMask(queueId, 6);

    data.WriteGuidMask(playerGuid, 3);

    data.WriteGuidMask(queueId, 5);

    data.FlushBits();

    data.WriteGuidBytes(playerGuid, 1);

    data.WriteGuidBytes(queueId, 1);

    data.WriteGuidBytes(playerGuid, 2);

    data << uint8(bg->IsRated() ? ratedType : 0);
    data << uint32(QueueSlot);

    if (HasAssignedRole)
        data << uint8(Role);

    data << uint32(ClientInstanceId);

    data.WriteGuidBytes(queueId, 6, 7);

    data << uint32(JoinTime);

    data.WriteGuidBytes(playerGuid, 7);

    data << uint8(bg->GetMaxLevel());

    data.WriteGuidBytes(playerGuid, 4);

    data.WriteGuidBytes(queueId, 2, 4);

    data << uint32(Time1);
    data << uint8(bg->GetMinLevel());

    data.WriteGuidBytes(playerGuid, 3);

    data.WriteGuidBytes(queueId, 0);

    data.WriteGuidBytes(playerGuid, 5, 6);

    data << uint32(ratedType);

    data.WriteGuidBytes(queueId, 3);

    data.WriteGuidBytes(playerGuid, 0);

    data.WriteGuidBytes(queueId, 5);

    data << uint32(bg->GetMapId());

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::BuildBattlegroundInProgressStatus(Battleground* bg, Player* player, uint32 QueueSlot, uint32 JoinTime, uint32 Time1, RatedType ratedType, bool LeftEarly)
{
    ObjectGuid playerGuid = player->GetGUID();
    ObjectGuid queueId = bg->GetQueueId();
    uint32 ClientInstanceId = bg->GetClientInstanceID();

    WorldPacket data(SMSG_BATTLEGROUND_STATUS_ACTIVE, 2 * (1 + 8) + 1 + 4 + 4 + 4 + 1 + 4 + 1 + 4 + 4 + 1 + 4);

    data.WriteGuidMask(playerGuid, 0);

    data.WriteGuidMask(queueId, 3);

    data.WriteGuidMask(playerGuid, 3);

    data.WriteGuidMask(queueId, 2);

    data.WriteGuidMask(playerGuid, 2);

    data.WriteGuidMask(queueId, 5, 1);

    data.WriteGuidMask(playerGuid, 7);

    data.WriteBit(LeftEarly);

    data.WriteGuidMask(playerGuid, 6);

    data.WriteGuidMask(queueId, 0);

    data.WriteBit(player->GetBGTeam() == ALLIANCE ? 1 : 0);

    data.WriteGuidMask(queueId, 6, 7, 4);

    data.WriteGuidMask(playerGuid, 1, 4, 5);

    data.WriteBit(bg->IsRated());

    data.FlushBits();

    data.WriteGuidBytes(playerGuid, 3);

    data << uint32(JoinTime);
    data << uint32(bg->GetRemainingTime());

    data.WriteGuidBytes(queueId, 7, 5);

    data.WriteGuidBytes(playerGuid, 1);

    data.WriteGuidBytes(queueId, 6);

    data << uint32(Time1);
    data << uint8(bg->GetMaxLevel());

    data.WriteGuidBytes(queueId, 1, 2);

    data << uint32(QueueSlot);

    data.WriteGuidBytes(playerGuid, 4);

    data << uint8(bg->GetMinLevel());

    data.WriteGuidBytes(playerGuid, 6);

    data << uint32(bg->GetMapId());

    data.WriteGuidBytes(playerGuid, 0, 5, 7);

    data.WriteGuidBytes(queueId, 4);

    data << uint32(ClientInstanceId);

    data.WriteGuidBytes(playerGuid, 2);

    data << uint8(bg->IsRated() ? ratedType : 1);
    data << uint32(ratedType);

    data.WriteGuidBytes(queueId, 3, 0);

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::BuildPvpLogDataPacket(WorldPacket* data, Battleground* bg)
{
    int8 Winner = -1;
    if (bg->GetStatus() == STATUS_WAIT_LEAVE)
        Winner = bg->GetWinner();

    bool HasWinner = Winner != -1;
    bool HasUnk = false;

    int32 GroupPreRatingChange[BG_TEAMS_COUNT] = { };
    int32 GroupPostRatingChange[BG_TEAMS_COUNT] = { };
    int32 GroupMMRRatingChange[BG_TEAMS_COUNT] = { };
    uint32 groupStatsSize[BG_TEAMS_COUNT] = { };

    Battleground::BattlegroundPlayerMap const& bgPlayers = bg->GetPlayers();

    data->Initialize(SMSG_PVP_LOG_DATA, 1 + 1 + 3 +
        bg->GetPlayerScoresSize() * (1 + 8 + 4 + 4 + 4 + (!bg->IsArena() ? 12 : 0) + 4 + 4 + 4 + 4 + 4 + 20 + 4) +
        (bg->IsRated() ? (BG_TEAMS_COUNT * (4 + 4 + 4 + 4 + 4 + 4)) : 0) + (HasWinner ? 1 : 0));

    ByteBuffer buff;

    *data << uint8(bg->GetPlayersCountByTeam(ALLIANCE));
    *data << uint8(bg->GetPlayersCountByTeam(HORDE));

    data->WriteBit(HasWinner);
    data->WriteBit(HasUnk);

    /*
    if ( HasUnk )
      {
        *((_BYTE *)v4 + 230) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 219) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 224) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 229) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 218) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 220) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 217) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 226) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 228) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 227) = CDataStore::ReadBit((int)&v18);
        LOBYTE(v20) = 0;
        *(_DWORD *)((char *)v4 + 113) = CDataStore::ReadBits7((int)&v18, v20);
        *((_BYTE *)v4 + 231) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 223) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 225) = CDataStore::ReadBit((int)&v18);
        LOBYTE(v20) = 0;
        *((_DWORD *)v4 + 4) = CDataStore::ReadBits7((int)&v18, v20);
        *((_BYTE *)v4 + 222) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 221) = CDataStore::ReadBit((int)&v18);
        *((_BYTE *)v4 + 216) = CDataStore::ReadBit((int)&v18);
      }
    */

    data->WriteBit(bg->IsRated());

    data->WriteBits(bg->GetPlayerScoresSize(), 19);

    for (Battleground::BattlegroundScoreMap::const_iterator itr = bg->GetPlayerScoresBegin(); itr != bg->GetPlayerScoresEnd(); ++itr)
    {
        if (!bg->IsPlayerInBattleground(itr->first))
        {
            TC_LOG_ERROR("network", "Player %s has scoreboard entry for battleground %u but is not in battleground!", itr->first.ToString().c_str(), bg->GetTypeID());
            continue;
        }

        uint32 PlayerTeam = 0;
        uint32 ActiveSpec= 0;
        bool InWorld = false;

        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
        {
            PlayerTeam = player->GetBGTeam();
            ActiveSpec = player->GetSpecId(player->GetActiveTalentGroup());
            InWorld = player->IsInWorld();
        }
        else
        {
            Battleground::BattlegroundPlayerMap::const_iterator itr2 = bgPlayers.find(itr->first);
            if (itr2 == bgPlayers.end())
            {
                TC_LOG_ERROR("bg.battleground", "Player %s has scoreboard entry for battleground %u but do not have battleground data!", itr->first.ToString().c_str(), bg->GetTypeID());
                continue;
            }

            PlayerTeam = itr2->second.Team;
            ActiveSpec = itr2->second.ActiveSpec;
        }

        ObjectGuid playerGUID = itr->first;
        BattlegroundScore* score = itr->second;

        uint8 bgTeam = PlayerTeam == HORDE ? 0 : 1;

        data->WriteGuidMask(playerGUID, 6);

        data->WriteBit(bgTeam);
        data->WriteBit(score->PersonalRatingChange != 0);

        data->WriteGuidMask(playerGUID, 0);

        data->WriteBit(score->MatchmakerRatingChange != 0);

        data->WriteGuidMask(playerGUID, 7);

        data->WriteBit(score->PreMatchMatchmakerRating != 0);

        data->WriteGuidMask(playerGUID, 3);

        data->WriteBit(score->PreMatchPersonalRating != 0);

        data->WriteGuidMask(playerGUID, 4, 1);

        buff << uint32(score->HealingDone);

        buff.WriteGuidBytes(playerGUID, 4, 5, 2);

        if (score->PreMatchMatchmakerRating)
            buff << uint32(score->PreMatchMatchmakerRating);

        if (!bg->IsArena())
        {
            buff << uint32(score->Deaths);
            buff << uint32(score->HonorableKills);
            buff << uint32(score->BonusHonor / 100);
        }

        buff.WriteGuidBytes(playerGUID, 3);

        buff << uint32(score->DamageDone);
        buff << uint32(score->KillingBlows);

        if (score->PersonalRatingChange)
            buff << int32(score->PersonalRatingChange);

        buff.WriteGuidBytes(playerGUID, 1, 6, 7);

        if (score->PreMatchPersonalRating)
            buff << uint32(score->PreMatchPersonalRating);

        buff.WriteGuidBytes(playerGUID, 0);

        buff << uint32(ActiveSpec);

        switch (bg->GetTypeID())
        {
            case BATTLEGROUND_AV:
                data->WriteBits(5, 22);
                buff << uint32(((BattlegroundAVScore*)score)->GraveyardsAssaulted);
                buff << uint32(((BattlegroundAVScore*)score)->GraveyardsDefended);
                buff << uint32(((BattlegroundAVScore*)score)->TowersAssaulted);
                buff << uint32(((BattlegroundAVScore*)score)->TowersDefended);
                buff << uint32(((BattlegroundAVScore*)score)->MinesCaptured);
                break;
            case BATTLEGROUND_WS:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundWGScore*)score)->FlagCaptures);
                buff << uint32(((BattlegroundWGScore*)score)->FlagReturns);
                break;
            case BATTLEGROUND_AB:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundABScore*)score)->BasesAssaulted);
                buff << uint32(((BattlegroundABScore*)score)->BasesDefended);
                break;
            case BATTLEGROUND_EY:
            case BATTLEGROUND_RATED_EY:
                data->WriteBits(1, 22);
                buff << uint32(((BattlegroundEYScore*)score)->FlagCaptures);
                break;
            case BATTLEGROUND_SA:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundSAScore*)score)->DemolishersDestroyed);
                buff << uint32(((BattlegroundSAScore*)score)->GatesDestroyed);
                break;
            case BATTLEGROUND_IC:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundICScore*)score)->BasesAssaulted);
                buff << uint32(((BattlegroundICScore*)score)->BasesDefended);
                break;
            case BATTLEGROUND_TP:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundTPScore*)score)->FlagCaptures);
                buff << uint32(((BattlegroundTPScore*)score)->FlagReturns);
                break;
            case BATTLEGROUND_BFG:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundBFGScore*)score)->BasesAssaulted);
                buff << uint32(((BattlegroundBFGScore*)score)->BasesDefended);
                break;
            case BATTLEGROUND_TOK:
                data->WriteBits(2, 22);
                buff << uint32(((BattlegroundTKScore*)score)->OrbHandles);
                buff << uint32(((BattlegroundTKScore*)score)->Score);
                break;
            case BATTLEGROUND_SM:
                data->WriteBits(1, 22);
                buff << uint32(((BattlegroundSMScore*)score)->MineCartCaptures);
                break;
            case BATTLEGROUND_DG:
                data->WriteBits(4, 22);
                buff << uint32(((BattlegroundDGScore*)score)->CartsCaptures);
                buff << uint32(((BattlegroundDGScore*)score)->CartsReturns);
                buff << uint32(((BattlegroundDGScore*)score)->MinesAssaulted);
                buff << uint32(((BattlegroundDGScore*)score)->MinesDefended);
                break;
            case BATTLEGROUND_NA:
            case BATTLEGROUND_BE:
            case BATTLEGROUND_AA:
            case BATTLEGROUND_RL:
            case BATTLEGROUND_DS:
            case BATTLEGROUND_TA:
            case BATTLEGROUND_TTP:
                data->WriteBits(0, 22);
                break;
            default:
                data->WriteBits(0, 22);
                break;
        }

        data->WriteBit(InWorld);
        data->WriteBit(!bg->IsArena());

        data->WriteGuidMask(playerGUID, 5, 2);

        if (score->MatchmakerRatingChange)
            buff << int32(score->MatchmakerRatingChange);

        if (bg->IsRated())
        {
            GroupPreRatingChange[bgTeam] += score->PreMatchPersonalRating;
            GroupPostRatingChange[bgTeam] += score->PersonalRatingChange;
            GroupMMRRatingChange[bgTeam] += score->MatchmakerRatingChange;
            ++groupStatsSize[bgTeam];
        }
    }

    data->append(buff);

    if (bg->IsRated())
    {
        for (uint8 i = 0; i < BG_TEAMS_COUNT; ++i)
        {
            if (!groupStatsSize[i])
                continue;

            GroupPreRatingChange[i] /= groupStatsSize[i];
            GroupPostRatingChange[i] /= groupStatsSize[i];
            GroupMMRRatingChange[i] /= groupStatsSize[i];
        }

        *data << int32(GroupPostRatingChange[1]);
        *data << int32(GroupPostRatingChange[0]);
        *data << int32(GroupMMRRatingChange[1]);
        *data << int32(GroupPreRatingChange[0]);
        *data << int32(GroupPreRatingChange[1]);
        *data << int32(GroupMMRRatingChange[0]);
    }

    /*
    if ( *((_BYTE *)v4 + 232) )
      {
        if ( *((_BYTE *)v4 + 229) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 229) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 221) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 221) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 218) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 218) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 225) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 225) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 223) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 223) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 219) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 219) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 231) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 231) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 227) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 227) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 228) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 228) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 217) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 217) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 216) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 216) ^= BYTE3(a2);
        }
        v16 = *(_DWORD *)((char *)v4 + 113);
        sub_40F5A2(v3, (char *)v4 + 113, *(_DWORD *)((char *)v4 + 113));
        *((_BYTE *)v4 + v16 + 113) = 0;
        if ( *((_BYTE *)v4 + 230) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 230) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 226) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 226) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 220) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 220) ^= BYTE3(a2);
        }
        v17 = *((_DWORD *)v4 + 4);
        sub_40F5A2(v3, (char *)v4 + 16, *((_DWORD *)v4 + 4));
        *((_BYTE *)v4 + v17 + 16) = 0;
        if ( *((_BYTE *)v4 + 222) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 222) ^= BYTE3(a2);
        }
        if ( *((_BYTE *)v4 + 224) )
        {
          BYTE3(a2) = 0;
          CDataStore::GetInt8(v3, (int)((char *)&a2 + 3));
          *((_BYTE *)v4 + 224) ^= BYTE3(a2);
        }
      }
    */

    if (HasWinner)
        *data << uint8(Winner);
}

void BattlegroundMgr::BuildStatusFailedPacket(Battleground* bg, Player* player, uint32 QueueSlot, uint32 ArenaType, GroupJoinBattlegroundResult result, ObjectGuid const* errorGuid /*= nullptr*/)
{
    ObjectGuid playerGuid = player->GetGUID();
    ObjectGuid battlegroundGuid = bg->GetQueueId();
    ObjectGuid ClientID;
    if (errorGuid && (result == ERR_BATTLEGROUND_NOT_IN_BATTLEGROUND || result == ERR_BATTLEGROUND_JOIN_TIMED_OUT))
        ClientID = *errorGuid;

    BattlegroundQueueTypeId bgQueueTypeId = sBattlegroundMgr->BGQueueTypeId(player->GetOriginalBattlegroundTypeId(), bg->GetRatedType());
    uint32 clientInstanceId = bg->GetClientInstanceID();
    uint32 JoinTime = player->GetBattlegroundQueueJoinTime(bgQueueTypeId);

    WorldPacket data(SMSG_BATTLEGROUND_STATUS_FAILED, 3 * (1 + 8) + 4 + 4 + 4 + 4);

    data << uint32(JoinTime);
    data << uint32(ArenaType);
    data << uint32(QueueSlot);
    data << uint32(result);

    data.WriteGuidMask(ClientID, 7);

    data.WriteGuidMask(battlegroundGuid, 2, 7);

    data.WriteGuidMask(ClientID, 5);

    data.WriteGuidMask(playerGuid, 2);

    data.WriteGuidMask(battlegroundGuid, 6);

    data.WriteGuidMask(playerGuid, 7, 3);

    data.WriteGuidMask(battlegroundGuid, 0, 3);

    data.WriteGuidMask(ClientID, 4);

    data.WriteGuidMask(playerGuid, 1);

    data.WriteGuidMask(ClientID, 0);

    data.WriteGuidMask(playerGuid, 0);

    data.WriteGuidMask(ClientID, 2);

    data.WriteGuidMask(battlegroundGuid, 4);

    data.WriteGuidMask(playerGuid, 4);

    data.WriteGuidMask(battlegroundGuid, 1);

    data.WriteGuidMask(ClientID, 3);

    data.WriteGuidMask(battlegroundGuid, 5);

    data.WriteGuidMask(ClientID, 1);

    data.WriteGuidMask(playerGuid, 6, 5);

    data.WriteGuidMask(ClientID, 6);

    data.WriteGuidBytes(ClientID, 1, 2, 7);

    data.WriteGuidBytes(battlegroundGuid, 6, 0);

    data.WriteGuidBytes(playerGuid, 5, 0);

    data.WriteGuidBytes(battlegroundGuid, 1, 7);

    data.WriteGuidBytes(playerGuid, 6);

    data.WriteGuidBytes(ClientID, 0);

    data.WriteGuidBytes(battlegroundGuid, 5);

    data.WriteGuidBytes(ClientID, 6);

    data.WriteGuidBytes(playerGuid, 1);

    data.WriteGuidBytes(battlegroundGuid, 2);

    data.WriteGuidBytes(playerGuid, 7, 2, 3);

    data.WriteGuidBytes(ClientID, 5);

    data.WriteGuidBytes(playerGuid, 4);

    data.WriteGuidBytes(ClientID, 3);

    data.WriteGuidBytes(battlegroundGuid, 3);

    data.WriteGuidBytes(ClientID, 4);

    data.WriteGuidBytes(battlegroundGuid, 4);

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::BuildUpdateWorldStatePacket(WorldPacket* data, uint32 field, uint32 value, bool Hidden)
{
    data->Initialize(SMSG_UPDATE_WORLD_STATE, 1 + 4 + 4);

    data->WriteBit(Hidden);

    data->FlushBits();

    *data << uint32(value);
    *data << uint32(field);
}

void BattlegroundMgr::BuildPlaySoundPacket(WorldPacket* data, uint32 soundid)
{
    data->Initialize(SMSG_PLAY_SOUND, 1 + 4);

    data->WriteBits(0, 8);

    *data << uint32(soundid);
}

void BattlegroundMgr::BuildPlayerLeftBattlegroundPacket(WorldPacket* data, ObjectGuid guid)
{
    data->Initialize(SMSG_BATTLEGROUND_PLAYER_LEFT);

    data->WriteGuidMask(guid, 3, 5, 6, 0, 1, 2, 7, 4);
    data->WriteGuidBytes(guid, 0, 6, 5, 7, 2, 1, 3, 4);
}

void BattlegroundMgr::BuildPlayerJoinedBattlegroundPacket(WorldPacket* data, ObjectGuid Guid)
{
    data->Initialize(SMSG_BATTLEGROUND_PLAYER_JOINED);

    data->WriteGuidMask(Guid, 7, 1, 0, 4, 2, 5, 6, 3);
    data->WriteGuidBytes(Guid, 0, 3, 7, 5, 2, 6, 4, 1);
}

Battleground* BattlegroundMgr::GetBattleground(uint32 instanceId, BattlegroundTypeId bgTypeId)
{
    if (!instanceId)
        return NULL;

    BattlegroundDataContainer::const_iterator begin, end;

    if (bgTypeId == BATTLEGROUND_TYPE_NONE)
    {
        begin = bgDataStore.begin();
        end = bgDataStore.end();
    }
    else
    {
        end = bgDataStore.find(bgTypeId);
        if (end == bgDataStore.end())
            return NULL;

        begin = end++;
    }

    for (BattlegroundDataContainer::const_iterator it = begin; it != end; ++it)
    {
        BattlegroundContainer const& bgs = it->second.m_Battlegrounds;
        BattlegroundContainer::const_iterator itr = bgs.find(instanceId);
        if (itr != bgs.end())
            return itr->second;
    }

    return NULL;
}

Battleground* BattlegroundMgr::GetBattlegroundTemplate(BattlegroundTypeId bgTypeId)
{
    BattlegroundDataContainer::const_iterator itr = bgDataStore.find(bgTypeId);
    if (itr == bgDataStore.end())
        return NULL;

    BattlegroundContainer const& bgs = itr->second.m_Battlegrounds;
    return bgs.empty() ? NULL : bgs.begin()->second;
}

uint32 BattlegroundMgr::CreateClientVisibleInstanceId(BattlegroundTypeId bgTypeId, BattlegroundBracketId bracket_id)
{
    if (IsArenaType(bgTypeId))
        return 0;

    BattlegroundClientIdsContainer& clientIds = bgDataStore[bgTypeId].m_ClientBattlegroundIds[bracket_id];
    uint32 lastId = 0;
    for (auto itr : clientIds)
    {
        if ((++lastId) != itr)
            break;
        lastId = itr;
    }

    clientIds.insert(++lastId);
    return lastId;
}

Battleground* BattlegroundMgr::CreateNewBattleground(BattlegroundTypeId originalBgTypeId, PvPDifficultyEntry const* bracketEntry, RatedType ratedType, bool IsRated, bool IsWarGame, uint32 MinPlayers)
{
    BattlegroundTypeId bgTypeId = originalBgTypeId;

    switch (originalBgTypeId)
    {
        case BATTLEGROUND_RB:
        case BATTLEGROUND_AA:
        case BATTLEGROUND_RATED_10_VS_10:
            bgTypeId = GetRandomBG(originalBgTypeId, MinPlayers);
            break;
        default:
            break;
    }

    Battleground* bg_template = GetBattlegroundTemplate(bgTypeId);
    if (!bg_template)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground: CreateNewBattleground - bg template not found for %u", bgTypeId);
        return NULL;
    }

    Battleground* bg = NULL;
    switch (bgTypeId)
    {
        case BATTLEGROUND_AV:
            bg = new BattlegroundAV(*(BattlegroundAV*)bg_template);
            break;
        case BATTLEGROUND_WS:
            bg = new BattlegroundWS(*(BattlegroundWS*)bg_template);
            break;
        case BATTLEGROUND_AB:
            bg = new BattlegroundAB(*(BattlegroundAB*)bg_template);
            break;
        case BATTLEGROUND_NA:
            bg = new BattlegroundNA(*(BattlegroundNA*)bg_template);
            break;
        case BATTLEGROUND_BE:
            bg = new BattlegroundBE(*(BattlegroundBE*)bg_template);
            break;
        case BATTLEGROUND_EY:
        case BATTLEGROUND_RATED_EY:
            bg = new BattlegroundEY(*(BattlegroundEY*)bg_template);
            break;
        case BATTLEGROUND_RL:
            bg = new BattlegroundRL(*(BattlegroundRL*)bg_template);
            break;
        case BATTLEGROUND_SA:
            bg = new BattlegroundSA(*(BattlegroundSA*)bg_template);
            break;
        case BATTLEGROUND_DS:
            bg = new BattlegroundDS(*(BattlegroundDS*)bg_template);
            break;
        case BATTLEGROUND_IC:
            bg = new BattlegroundIC(*(BattlegroundIC*)bg_template);
            break;
        case BATTLEGROUND_TP:
            bg = new BattlegroundTP(*(BattlegroundTP*)bg_template);
            break;
        case BATTLEGROUND_BFG:
            bg = new BattlegroundBFG(*(BattlegroundBFG*)bg_template);
            break;
        case BATTLEGROUND_TOK:
            bg = new BattlegroundTK(*(BattlegroundTK*)bg_template);
            break;
        case BATTLEGROUND_DG:
            bg = new BattlegroundDG(*(BattlegroundDG*)bg_template);
            break;
        case BATTLEGROUND_SM:
            bg = new BattlegroundSM(*(BattlegroundSM*)bg_template);
            break;
        case BATTLEGROUND_TA:
            bg = new BattlegroundTV(*(BattlegroundTV*)bg_template);
            break;
        case BATTLEGROUND_TTP:
            bg = new BattlegroundTTP(*(BattlegroundTTP*)bg_template);
            break;
        case BATTLEGROUND_RB:
        case BATTLEGROUND_AA:
        case BATTLEGROUND_RATED_10_VS_10:
            bg = new Battleground(*bg_template);
            break;
        default:
            return NULL;
    }

    bg->SetBracket(bracketEntry);
    bg->SetInstanceID(sMapMgr->GenerateInstanceId());
    bg->SetClientInstanceID(CreateClientVisibleInstanceId(originalBgTypeId, bracketEntry->GetBracketId()));
    bg->Reset();
    bg->SetStatus(STATUS_WAIT_JOIN);
    bg->SetRatedType(ratedType);
    bg->SetTypeID(bgTypeId);
    bg->SetRated(IsRated);
    bg->SetWarGame(IsWarGame);

    uint32 GuidOffset = 0x0;
    if (IsRated)
    {
        if (bg->IsArena())
            GuidOffset = 0x10000;
        else
            GuidOffset = 0x100000;
    }
    else if (IsWarGame)
        GuidOffset = 0x30000;

    bg->SetQueueId(ObjectGuid::Create<HighGuid::BattleGround1>(uint32(originalBgTypeId) | GuidOffset));

    if (IsRated)
    {
        uint32 maxPlayersPerTeam = 0;

        switch (ratedType)
        {
            case RATED_TYPE_2v2:
                maxPlayersPerTeam = 2;
                break;
            case RATED_TYPE_3v3:
                maxPlayersPerTeam = 3;
                break;
            case RATED_TYPE_5v5:
                maxPlayersPerTeam = 5;
                break;
            case RATED_TYPE_10v10:
                maxPlayersPerTeam = 10;
                break;
        }

        bg->SetMaxPlayersPerTeam(maxPlayersPerTeam);
        bg->SetMaxPlayers(maxPlayersPerTeam * 2);
    }

    return bg;
}

bool BattlegroundMgr::CreateBattleground(BattlegroundTemplate const* bgTemplate)
{
    Battleground* bg = GetBattlegroundTemplate(bgTemplate->Id);
    if (!bg)
    {
        // Create the BG
        switch (bgTemplate->Id)
        {
            case BATTLEGROUND_AV:
                bg = new BattlegroundAV();
                break;
            case BATTLEGROUND_WS:
                bg = new BattlegroundWS();
                break;
            case BATTLEGROUND_AB:
                bg = new BattlegroundAB();
                break;
            case BATTLEGROUND_NA:
                bg = new BattlegroundNA();
                break;
            case BATTLEGROUND_BE:
                bg = new BattlegroundBE();
                break;
            case BATTLEGROUND_EY:
            case BATTLEGROUND_RATED_EY:
                bg = new BattlegroundEY();
                break;
            case BATTLEGROUND_RL:
                bg = new BattlegroundRL();
                break;
            case BATTLEGROUND_SA:
                bg = new BattlegroundSA();
                break;
            case BATTLEGROUND_DS:
                bg = new BattlegroundDS();
                break;
            case BATTLEGROUND_IC:
                bg = new BattlegroundIC();
                break;
            case BATTLEGROUND_TP:
                bg = new BattlegroundTP();
                break;
            case BATTLEGROUND_BFG:
                bg = new BattlegroundBFG();
                break;
            case BATTLEGROUND_TOK:
                bg = new BattlegroundTK();
                break;
            case BATTLEGROUND_DG:
                bg = new BattlegroundDG();
                break;
            case BATTLEGROUND_SM:
                bg = new BattlegroundSM();
                break;
            case BATTLEGROUND_TTP:
                bg = new BattlegroundTTP();
                break;
            case BATTLEGROUND_TA:
                bg = new BattlegroundTV();
                break;
            case BATTLEGROUND_RB:
            case BATTLEGROUND_AA:
            case BATTLEGROUND_RATED_10_VS_10:
                bg = new Battleground();
                break;
            default:
                return false;
        }

        bg->SetTypeID(bgTemplate->Id);
    }

    bg->SetMapId(bgTemplate->BattlemasterEntry->MapID[0]);
    bg->SetName(bgTemplate->BattlemasterEntry->Name);
    bg->SetInstanceID(0);
    bg->SetArenaorBGType(bgTemplate->IsArena());
    bg->SetMinPlayersPerTeam(bgTemplate->MinPlayersPerTeam);
    bg->SetMaxPlayersPerTeam(bgTemplate->MaxPlayersPerTeam);
    bg->SetMinPlayers(bgTemplate->MinPlayersPerTeam * 2);
    bg->SetMaxPlayers(bgTemplate->MaxPlayersPerTeam * 2);
    bg->SetTeamStartPosition(TEAM_ALLIANCE, bgTemplate->StartLocation[TEAM_ALLIANCE]);
    bg->SetTeamStartPosition(TEAM_HORDE, bgTemplate->StartLocation[TEAM_HORDE]);
    bg->SetStartMaxDist(bgTemplate->MaxStartDistSq);
    bg->SetLevelRange(bgTemplate->MinLevel, bgTemplate->MaxLevel);
    bg->SetScriptId(bgTemplate->ScriptId);
    bg->SetQueueId(ObjectGuid::Create<HighGuid::BattleGround1>(uint32(bgTemplate->Id)));

    AddBattleground(bg);

    return true;
}

void BattlegroundMgr::LoadBattlegroundTemplates()
{
    uint32 oldMSTime = getMSTime();

    _battlegroundMapTemplates.clear();
    _battlegroundTemplates.clear();

    //                                               0   1                  2                  3       4       5                 6              7             8       9
    QueryResult result = WorldDatabase.Query("SELECT ID, MinPlayersPerTeam, MaxPlayersPerTeam, MinLvl, MaxLvl, AllianceStartLoc, HordeStartLoc, StartMaxDist, Weight, ScriptName FROM battleground_template");
    if (!result)
    {
        TC_LOG_ERROR("server.loading", ">> Loaded 0 battlegrounds. DB table `battleground_template` is empty.");
        return;
    }

    uint32 count = 0;

    do
    {
        Field* fields = result->Fetch();

        BattlegroundTypeId bgTypeId = BattlegroundTypeId(fields[0].GetUInt32());

        if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, bgTypeId, NULL))
            continue;

        // can be overwrite by values from DB
        BattlemasterListEntry const* bl = sBattlemasterListStore.LookupEntry(bgTypeId);
        if (!bl)
        {
            TC_LOG_ERROR("bg.battleground", "Battleground ID %u not found in BattlemasterList.dbc. Battleground not created.", bgTypeId);
            continue;
        }

        BattlegroundTemplate bgTemplate;
        bgTemplate.Id = bgTypeId;
        bgTemplate.MinPlayersPerTeam = fields[1].GetUInt16();
        bgTemplate.MaxPlayersPerTeam = fields[2].GetUInt16();
        bgTemplate.MinLevel = fields[3].GetUInt8();
        bgTemplate.MaxLevel = fields[4].GetUInt8();
        float dist = fields[7].GetFloat();
        bgTemplate.MaxStartDistSq = dist * dist;
        bgTemplate.Weight = fields[8].GetUInt8();
        bgTemplate.ScriptId = sObjectMgr->GetScriptId(fields[9].GetString());
        bgTemplate.BattlemasterEntry = bl;

        if (bgTemplate.MaxPlayersPerTeam == 0 || bgTemplate.MinPlayersPerTeam > bgTemplate.MaxPlayersPerTeam)
        {
            TC_LOG_ERROR("sql.sql", "Table `battleground_template` for ID %u has bad values for MinPlayersPerTeam (%u) and MaxPlayersPerTeam(%u)",
                bgTemplate.Id, bgTemplate.MinPlayersPerTeam, bgTemplate.MaxPlayersPerTeam);
            continue;
        }

        if (bgTemplate.MinLevel == 0 || bgTemplate.MaxLevel == 0 || bgTemplate.MinLevel > bgTemplate.MaxLevel)
        {
            TC_LOG_ERROR("sql.sql", "Table `battleground_template` for ID %u has bad values for MinLevel (%u) and MaxLevel (%u)",
                bgTemplate.Id, bgTemplate.MinLevel, bgTemplate.MaxLevel);
            continue;
        }

        if (bgTemplate.Id != BATTLEGROUND_AA && bgTemplate.Id != BATTLEGROUND_RB && bgTemplate.Id != BATTLEGROUND_RATED_10_VS_10)
        {
            uint32 startId = fields[5].GetUInt32();
            if (WorldSafeLocsEntry const* start = sWorldSafeLocsStore.LookupEntry(startId))
            {
                bgTemplate.StartLocation[TEAM_ALLIANCE].Relocate(start->Loc.X, start->Loc.Y, start->Loc.Z, (start->Facing * M_PI) / 180);
            }
            else
            {
                TC_LOG_ERROR("sql.sql", "Table `battleground_template` for ID %u a non-existant WorldSafeLoc (ID: %u) in field `AllianceStartLoc`. BG not created.", bgTemplate.Id, startId);
                continue;
            }

            startId = fields[6].GetUInt32();
            if (WorldSafeLocsEntry const* start = sWorldSafeLocsStore.LookupEntry(startId))
            {
                bgTemplate.StartLocation[TEAM_HORDE].Relocate(start->Loc.X, start->Loc.Y, start->Loc.Z, (start->Facing * M_PI) / 180);
            }
            else
            {
                TC_LOG_ERROR("sql.sql", "Table `battleground_template` for ID %u has a non-existant WorldSafeLoc (ID: %u) in field `HordeStartLoc`. BG not created.", bgTemplate.Id, startId);
                continue;
            }
        }

        if (!CreateBattleground(&bgTemplate))
            continue;

        _battlegroundTemplates[bgTypeId] = bgTemplate;

        if (bgTemplate.BattlemasterEntry->MapID[1] == -1) // in this case we have only one mapId
            _battlegroundMapTemplates[bgTemplate.BattlemasterEntry->MapID[0]] = &_battlegroundTemplates[bgTypeId];

        ++count;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u battlegrounds in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlegroundMgr::LoadBattlegroundItemLevelCaps()
{
    _battlegroundItemLevelCaps.clear();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLEGROUND_ITEM_LEVEL_CAP);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
        return;

    do
    {
        Field* fields = result->Fetch();

        uint32 maxLevel = fields[0].GetUInt32();
        uint32 itemLevel = fields[1].GetUInt32();

        _battlegroundItemLevelCaps[maxLevel] = itemLevel;

    } while (result->NextRow());
}

void BattlegroundMgr::BuildBattlegroundListPacket(ObjectGuid guid, Player* player, BattlegroundTypeId bgTypeId)
{
    if (!player)
        return;

    BattlegroundDataContainer::iterator it = bgDataStore.find(bgTypeId);
    if (it == bgDataStore.end())
        return;

    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(it->second.m_Battlegrounds.begin()->second->GetMapId(), player->GetLevel());
    if (!bracketEntry)
        return;

    uint32 winnerConquest = (player->GetRandomAndWeekendWinner() ? sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_CONQUEST_LAST) : sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_CONQUEST_FIRST)) / CURRENCY_PRECISION;
    uint32 winnerHonor = (player->GetRandomAndWeekendWinner() ? sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_HONOR_LAST) : sWorld->getIntConfig(CONFIG_BG_REWARD_WINNER_HONOR_FIRST)) / CURRENCY_PRECISION;
    uint32 loserHonor = (!player->GetRandomAndWeekendWinner() ? sWorld->getIntConfig(CONFIG_BG_REWARD_LOSER_HONOR_LAST) : sWorld->getIntConfig(CONFIG_BG_REWARD_LOSER_HONOR_FIRST)) / CURRENCY_PRECISION;

    bool PvpAnywhere = guid.IsEmpty();
    bool IsRandomBG = bgTypeId == BATTLEGROUND_RB;
    bool HasHolidayWinToday = player->GetWeekendWinner();
    bool HasRandomWinToday = player->GetRandomWinner();

    BattlegroundBracketId bracketId = bracketEntry->GetBracketId();
    BattlegroundClientIdsContainer& clientIds = it->second.m_ClientBattlegroundIds[bracketId];

    WorldPacket data(SMSG_BATTLEGROUND_LIST, 1 + 8 + 4 + 4 + 1 + 4 + 4 + 4 + 4 + 1 + 4 + 4 + (clientIds.size() * 4));

    data << uint32(winnerConquest);         // Winner Conquest Reward or Random Winner Conquest Reward
    data << uint32(loserHonor);             // Loser Honor Reward or Random Loser Honor Reward
    data << uint8(bracketEntry->MinLevel);  // min level
    data << uint32(winnerConquest);         // Winner Conquest Reward or Random Winner Conquest Reward
    data << uint32(winnerHonor);            // Winner Honor Reward or Random Winner Honor Reward
    data << uint32(bgTypeId);               // battleground id
    data << uint32(winnerHonor);            // Winner Honor Reward or Random Winner Honor Reward
    data << uint8(bracketEntry->MaxLevel);  // max level
    data << uint32(loserHonor);             // Loser Honor Reward or Random Loser Honor Reward

    data.WriteGuidMask(guid, 0);

    data.WriteBit(HasHolidayWinToday);

    data.WriteGuidMask(guid, 4);

    data.WriteBit(HasRandomWinToday);

    data.WriteGuidMask(guid, 2);

    data.WriteBit(IsRandomBG);

    data.WriteGuidMask(guid, 7, 6, 5, 1);

    data.WriteBit(PvpAnywhere);

    data.WriteGuidMask(guid, 3);

    data.WriteBits(clientIds.size(), 22);

    data.FlushBits();

    data.WriteGuidBytes(guid, 7, 3, 4, 0, 5, 6, 1, 2);

    for (uint32 bgID : clientIds)
        data << uint32(bgID);

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::SendToBattleground(Player* player, uint32 instanceId, BattlegroundTypeId bgTypeId)
{
    if (Battleground* bg = GetBattleground(instanceId, bgTypeId))
    {
        uint32 mapid = bg->GetMapId();
        uint32 team = player->GetBGTeam();

        Position const* pos = bg->GetTeamStartPosition(Battleground::GetTeamIndexByTeamId(team));
        TC_LOG_DEBUG("bg.battleground", "BattlegroundMgr::SendToBattleground: Sending %s to map %u, %s (bgType %u)", player->GetName().c_str(), mapid, pos->ToString().c_str(), bgTypeId);
        player->TeleportTo(mapid, pos->GetPositionX(), pos->GetPositionY(), pos->GetPositionZ(), pos->GetOrientation());
    }
    else
        TC_LOG_ERROR("bg.battleground", "BattlegroundMgr::SendToBattleground: Instance %u (bgType %u) not found while trying to teleport player %s", instanceId, bgTypeId, player->GetName().c_str());
}

void BattlegroundMgr::SendAreaSpiritHealerQueryOpcode(Player* player, Battleground* bg, ObjectGuid guid)
{
    uint32 time = 30000 - bg->GetLastResurrectTime();      // resurrect every 30 seconds
    if (time == uint32(-1))
        time = 0;

    WorldPacket data(SMSG_AREA_SPIRIT_HEALER_TIME, 1 + 8 + 4);

    data.WriteGuidMask(guid, 0, 2, 7, 6, 1, 0, 3, 4);

    data.WriteGuidBytes(guid, 2, 3, 5, 4, 6);

    data << uint32(time);

    data.WriteGuidBytes(guid, 7, 0, 1);

    player->SendDirectMessage(&data);
}

void BattlegroundMgr::HandleStartTimer(Player* player, uint32 TimeRemaining, uint32 TotalTime, uint32 Type)
{
    WorldPacket data(SMSG_START_TIMER, 4 + 4 + 4);

    data << uint32(TimeRemaining);
    data << uint32(TotalTime);
    data << uint32(Type);

    player->SendDirectMessage(&data);
}

bool BattlegroundMgr::IsArenaType(BattlegroundTypeId bgTypeId)
{
    return bgTypeId == BATTLEGROUND_AA || bgTypeId == BATTLEGROUND_BE || bgTypeId == BATTLEGROUND_NA ||
        bgTypeId == BATTLEGROUND_DS || bgTypeId == BATTLEGROUND_RL ||
        bgTypeId == BATTLEGROUND_TA || bgTypeId == BATTLEGROUND_TTP;
}

bool BattlegroundMgr::IsRatedBattlegroundTemplate(BattlegroundTypeId bgTypeId)
{
    return bgTypeId == BATTLEGROUND_RATED_10_VS_10 || bgTypeId == BATTLEGROUND_RATED_EY || bgTypeId == BATTLEGROUND_WS || bgTypeId == BATTLEGROUND_AB ||
        bgTypeId == BATTLEGROUND_EY || bgTypeId == BATTLEGROUND_BFG || bgTypeId == BATTLEGROUND_TP;
}

BattlegroundQueueTypeId BattlegroundMgr::BGQueueTypeId(BattlegroundTypeId bgTypeId, RatedType ratedType)
{
    switch (bgTypeId)
    {
        case BATTLEGROUND_AV:
            return BATTLEGROUND_QUEUE_AV;
        case BATTLEGROUND_WS:
            return BATTLEGROUND_QUEUE_WS;
        case BATTLEGROUND_AB:
            return BATTLEGROUND_QUEUE_AB;
        case BATTLEGROUND_EY:
            return BATTLEGROUND_QUEUE_EY;
        case BATTLEGROUND_IC:
            return BATTLEGROUND_QUEUE_IC;
        case BATTLEGROUND_SA:
            return BATTLEGROUND_QUEUE_SA;
        case BATTLEGROUND_TP:
            return BATTLEGROUND_QUEUE_TP;
        case BATTLEGROUND_BFG:
            return BATTLEGROUND_QUEUE_BFG;
        case BATTLEGROUND_RB:
            return BATTLEGROUND_QUEUE_RB;
        case BATTLEGROUND_TOK:
            return BATTLEGROUND_QUEUE_KT;
        case BATTLEGROUND_SM:
            return BATTLEGROUND_QUEUE_SSM;
        case BATTLEGROUND_DG:
            return BATTLEGROUND_QUEUE_DG;
        case BATTLEGROUND_AA:
        case BATTLEGROUND_BE:
        case BATTLEGROUND_DS:
        case BATTLEGROUND_NA:
        case BATTLEGROUND_RL:
        case BATTLEGROUND_TA:
        case BATTLEGROUND_TTP:
        case BATTLEGROUND_RATED_10_VS_10:
            switch (ratedType)
            {
                case RATED_TYPE_2v2:
                    return BATTLEGROUND_QUEUE_2v2;
                case RATED_TYPE_3v3:
                    return BATTLEGROUND_QUEUE_3v3;
                case RATED_TYPE_5v5:
                    return BATTLEGROUND_QUEUE_5v5;
                case RATED_TYPE_10v10:
                    return BATTLEGROUND_QUEUE_10v10;
                default:
                    return BATTLEGROUND_QUEUE_NONE;
            }
        default:
            return BATTLEGROUND_QUEUE_NONE;
    }
}

BattlegroundTypeId BattlegroundMgr::BGTemplateId(BattlegroundQueueTypeId bgQueueTypeId)
{
    switch (bgQueueTypeId)
    {
        case BATTLEGROUND_QUEUE_WS:
            return BATTLEGROUND_WS;
        case BATTLEGROUND_QUEUE_AB:
            return BATTLEGROUND_AB;
        case BATTLEGROUND_QUEUE_AV:
            return BATTLEGROUND_AV;
        case BATTLEGROUND_QUEUE_EY:
            return BATTLEGROUND_EY;
        case BATTLEGROUND_QUEUE_SA:
            return BATTLEGROUND_SA;
        case BATTLEGROUND_QUEUE_IC:
            return BATTLEGROUND_IC;
        case BATTLEGROUND_QUEUE_TP:
            return BATTLEGROUND_TP;
        case BATTLEGROUND_QUEUE_BFG:
            return BATTLEGROUND_BFG;
        case BATTLEGROUND_QUEUE_RB:
            return BATTLEGROUND_RB;
        case BATTLEGROUND_QUEUE_KT:
            return BATTLEGROUND_TOK;
        case BATTLEGROUND_QUEUE_SSM:
            return BATTLEGROUND_SM;
        case BATTLEGROUND_QUEUE_DG:
            return BATTLEGROUND_DG;
        case BATTLEGROUND_QUEUE_2v2:
        case BATTLEGROUND_QUEUE_3v3:
        case BATTLEGROUND_QUEUE_5v5:
            return BATTLEGROUND_AA;
        case BATTLEGROUND_QUEUE_10v10:
            return BATTLEGROUND_RATED_10_VS_10;
        default:
            return BATTLEGROUND_TYPE_NONE;
    }
}

RatedType BattlegroundMgr::GetRatedTypeByQueue(BattlegroundQueueTypeId bgQueueTypeId)
{
    switch (bgQueueTypeId)
    {
        case BATTLEGROUND_QUEUE_2v2:
            return RATED_TYPE_2v2;
        case BATTLEGROUND_QUEUE_3v3:
            return RATED_TYPE_3v3;
        case BATTLEGROUND_QUEUE_5v5:
            return RATED_TYPE_5v5;
        case BATTLEGROUND_QUEUE_10v10:
            return RATED_TYPE_10v10;
        default:
            return RATED_TYPE_NOT_RATED;
    }
}

void BattlegroundMgr::ToggleTesting()
{
    m_Testing = !m_Testing;
    sWorld->SendWorldText(m_Testing ? LANG_DEBUG_BG_ON : LANG_DEBUG_BG_OFF);
}

void BattlegroundMgr::ToggleArenaTesting()
{
    m_ArenaTesting = !m_ArenaTesting;
    sWorld->SendWorldText(m_ArenaTesting ? LANG_DEBUG_ARENA_ON : LANG_DEBUG_ARENA_OFF);
}

void BattlegroundMgr::SetHolidayWeekends(uint32 mask)
{
    // The current code supports battlegrounds up to BattlegroundTypeId(31)
    for (uint32 bgtype = 1; bgtype < MAX_BATTLEGROUND_TYPE_ID && bgtype < 32; ++bgtype)
        if (Battleground* bg = GetBattlegroundTemplate(BattlegroundTypeId(bgtype)))
            bg->SetHoliday(mask & (1 << bgtype));
}

void BattlegroundMgr::ScheduleQueueUpdate(uint32 arenaMatchmakerRating, RatedType ratedType, BattlegroundQueueTypeId bgQueueTypeId, BattlegroundTypeId bgTypeId, BattlegroundBracketId bracket_id)
{
    ObjectGuid scheduleId = ObjectGuid(uint64(uint64(arenaMatchmakerRating) << 48) | (uint64(ratedType) << 40) | (uint32(bgQueueTypeId) << 24) | (uint32(bgTypeId) << 8) | bracket_id);
    if (std::find(m_QueueUpdateScheduler.begin(), m_QueueUpdateScheduler.end(), scheduleId) == m_QueueUpdateScheduler.end())
        m_QueueUpdateScheduler.push_back(scheduleId);
}

uint32 BattlegroundMgr::GetMaxRatingDifference() const
{
    uint32 diff = sWorld->getIntConfig(CONFIG_ARENA_MAX_RATING_DIFFERENCE);
    if (diff == 0)
        diff = 5000;
    return diff;
}

uint32 BattlegroundMgr::GetRatingDiscardTimer() const
{
    return sWorld->getIntConfig(CONFIG_ARENA_RATING_DISCARD_TIMER);
}

uint32 BattlegroundMgr::GetPrematureFinishTime() const
{
    return sWorld->getIntConfig(CONFIG_BATTLEGROUND_PREMATURE_FINISH_TIMER);
}

void BattlegroundMgr::LoadBattleMastersEntry()
{
    uint32 oldMSTime = getMSTime();

    mBattleMastersMap.clear();

    QueryResult result = WorldDatabase.Query("SELECT entry, bg_template FROM battlemaster_entry");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 battlemaster entries. DB table `battlemaster_entry` is empty!");
        return;
    }

    uint32 count = 0;

    do
    {
        ++count;

        Field* fields = result->Fetch();

        uint32 entry = fields[0].GetUInt32();

        if (CreatureTemplate const* cInfo = sObjectMgr->GetCreatureTemplate(entry))
        {
            if ((cInfo->npcflag & UNIT_NPC_FLAG_BATTLEMASTER) == 0)
                TC_LOG_ERROR("sql.sql", "Creature (Entry: %u) listed in `battlemaster_entry` is not a battlemaster.", entry);
        }
        else
        {
            TC_LOG_ERROR("sql.sql", "Creature (Entry: %u) listed in `battlemaster_entry` does not exist.", entry);
            continue;
        }

        uint32 bgTypeId  = fields[1].GetUInt32();
        if (!sBattlemasterListStore.LookupEntry(bgTypeId))
        {
            TC_LOG_ERROR("sql.sql", "Table `battlemaster_entry` contain entry %u for not existed battleground type %u, ignored.", entry, bgTypeId);
            continue;
        }

        mBattleMastersMap[entry] = BattlegroundTypeId(bgTypeId);
    } while (result->NextRow());

    CheckBattleMasters();

    TC_LOG_INFO("server.loading", ">> Loaded %u battlemaster entries in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlegroundMgr::CheckBattleMasters()
{
    CreatureTemplateContainer const* ctc = sObjectMgr->GetCreatureTemplates();
    for (CreatureTemplateContainer::const_iterator itr = ctc->begin(); itr != ctc->end(); ++itr)
    {
        if ((itr->second.npcflag & UNIT_NPC_FLAG_BATTLEMASTER) && mBattleMastersMap.find(itr->second.Entry) == mBattleMastersMap.end())
        {
            TC_LOG_ERROR("sql.sql", "CreatureTemplate (Entry: %u) has UNIT_NPC_FLAG_BATTLEMASTER but no data in `battlemaster_entry` table. Removing flag!", itr->second.Entry);
            const_cast<CreatureTemplate*>(&itr->second)->npcflag &= ~UNIT_NPC_FLAG_BATTLEMASTER;
        }
    }
}

HolidayIds BattlegroundMgr::BGTypeToWeekendHolidayId(BattlegroundTypeId bgTypeId)
{
    switch (bgTypeId)
    {
        case BATTLEGROUND_AV:  return HOLIDAY_CALL_TO_ARMS_AV;
        case BATTLEGROUND_EY:  return HOLIDAY_CALL_TO_ARMS_EY;
        case BATTLEGROUND_WS:  return HOLIDAY_CALL_TO_ARMS_WS;
        case BATTLEGROUND_SA:  return HOLIDAY_CALL_TO_ARMS_SA;
        case BATTLEGROUND_AB:  return HOLIDAY_CALL_TO_ARMS_AB;
        case BATTLEGROUND_IC:  return HOLIDAY_CALL_TO_ARMS_IC;
        case BATTLEGROUND_TP:  return HOLIDAY_CALL_TO_ARMS_TP;
        case BATTLEGROUND_BFG: return HOLIDAY_CALL_TO_ARMS_BFG;
        case BATTLEGROUND_TOK: return HOLIDAY_CALL_TO_ARMS_TOK;
        case BATTLEGROUND_DG:  return HOLIDAY_CALL_TO_ARMS_DG;
        case BATTLEGROUND_SM:  return HOLIDAY_CALL_TO_ARMS_SM;
        default:               return HOLIDAY_NONE;
    }
}

BattlegroundTypeId BattlegroundMgr::WeekendHolidayIdToBGType(HolidayIds holiday)
{
    switch (holiday)
    {
        case HOLIDAY_CALL_TO_ARMS_AV:  return BATTLEGROUND_AV;
        case HOLIDAY_CALL_TO_ARMS_EY:  return BATTLEGROUND_EY;
        case HOLIDAY_CALL_TO_ARMS_WS:  return BATTLEGROUND_WS;
        case HOLIDAY_CALL_TO_ARMS_SA:  return BATTLEGROUND_SA;
        case HOLIDAY_CALL_TO_ARMS_AB:  return BATTLEGROUND_AB;
        case HOLIDAY_CALL_TO_ARMS_IC:  return BATTLEGROUND_IC;
        case HOLIDAY_CALL_TO_ARMS_TP:  return BATTLEGROUND_TP;
        case HOLIDAY_CALL_TO_ARMS_BFG: return BATTLEGROUND_BFG;
        case HOLIDAY_CALL_TO_ARMS_TOK: return BATTLEGROUND_TOK;
        case HOLIDAY_CALL_TO_ARMS_DG:  return BATTLEGROUND_DG;
        case HOLIDAY_CALL_TO_ARMS_SM:  return BATTLEGROUND_SM;
        default:                       return BATTLEGROUND_TYPE_NONE;
    }
}

bool BattlegroundMgr::IsBGWeekend(BattlegroundTypeId bgTypeId)
{
    return IsHolidayActive(BGTypeToWeekendHolidayId(bgTypeId));
}

BattlegroundTypeId BattlegroundMgr::GetRandomBG(BattlegroundTypeId bgTypeId, uint32 MinPlayers)
{
    if (BattlegroundTemplate const* bgTemplate = GetBattlegroundTemplateByTypeId(bgTypeId))
    {
        uint32 weight = 0;
        BattlegroundSelectionWeightMap selectionWeights;

        for (int32 mapId : bgTemplate->BattlemasterEntry->MapID)
        {
            if (mapId == -1)
                break;

            if (BattlegroundTemplate const* bg = GetBattlegroundTemplateByMapId(mapId))
            {
                if (MinPlayers && MinPlayers < bg->MinPlayersPerTeam)
                    continue;

                weight += bg->Weight;
                selectionWeights[bg->Id] = bg->Weight;
            }
        }

        // there is only one bg to select
        if (selectionWeights.size() == 1)
            return selectionWeights.begin()->first;

        if (weight)
        {
            // Select a random value
            uint32 selectedWeight = Math::Rand(0u, weight - 1);
            // Select the correct bg (if we have in DB A(10), B(20), C(10), D(15) --> [0---A---9|10---B---29|30---C---39|40---D---54])
            weight = 0;
            for (auto & it : selectionWeights)
            {
                weight += it.second;
                if (selectedWeight < weight)
                    return it.first;
            }
        }
    }

    return BATTLEGROUND_TYPE_NONE;
}

void BattlegroundMgr::AddBattleground(Battleground* bg)
{
    if (bg)
        bgDataStore[bg->GetTypeID()].m_Battlegrounds[bg->GetInstanceID()] = bg;
}

void BattlegroundMgr::RemoveBattleground(BattlegroundTypeId bgTypeId, uint32 instanceId)
{
    bgDataStore[bgTypeId].m_Battlegrounds.erase(instanceId);
}

uint32 BattlegroundMgr::GenerateWarGameId()
{
    if (_WarGameId >= 0xFFFFFF)
    {
        TC_LOG_ERROR("misc", "WarGames ids overflow!! Can't continue, shutting down server. ");
        World::StopNow(ERROR_EXIT_CODE);
    }

    return _WarGameId++;
}

void BattlegroundMgr::CreateWarGameQueue(Battleground* bg, Player* challenger, Player* opponent)
{
    if (!bg)
        return;

    Group* challengerGroup = challenger->GetGroup();
    Group* opponentGroup = opponent->GetGroup();
    if (!challengerGroup || !opponentGroup)
        return;

    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg->GetMapId(), challenger->GetLevel());
    if (!bracketEntry)
        return;

    if (bg->GetMinPlayersPerTeam() > opponentGroup->GetMembersCount() && bg->GetMinPlayersPerTeam() > challengerGroup->GetMembersCount())
        return;

    RatedType arenatype = RATED_TYPE_NOT_RATED;
    if (bg->IsArena())
    {
        if (challengerGroup->GetMembersCount() < 3)
            arenatype = RATED_TYPE_2v2;
        else if (challengerGroup->GetMembersCount() < 5)
            arenatype = RATED_TYPE_3v3;
        else
            arenatype = RATED_TYPE_5v5;
    }

    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bg->GetTypeID(), arenatype);

    bool IsRandomBGQueue = bgQueueTypeId == BATTLEGROUND_QUEUE_RB;

    uint32 WarGameId = GenerateWarGameId();

    GroupQueueInfo* ginfo1 = nullptr;
    GroupQueueInfo* ginfo2 = nullptr;

    uint32 avgTime1 = 0;
    uint32 avgTime2 = 0;

    ObjectGuid errorGuid;
    GroupJoinBattlegroundResult err = challengerGroup->CanJoinBattlegroundQueue(bg, bgQueueTypeId, arenatype, arenatype, false, 0, errorGuid, true);
    if (!err)
        err = opponentGroup->CanJoinBattlegroundQueue(bg, bgQueueTypeId, arenatype, arenatype, false, 0, errorGuid, true);

    if (!err)
    {
        TC_LOG_DEBUG("bg.battleground", "Battleground: Group (GUID: %s), Leader (%s, GUID: %u) just queued WarGame Type %u",
            challengerGroup->GetGUID().ToString().c_str(), challenger->GetName().c_str(), challenger->GetGUID(), arenatype);

        BattlegroundQueue& bgQueue = GetBattlegroundQueue(bgQueueTypeId);
        ginfo1 = bgQueue.AddGroup(challenger, challengerGroup, bg->GetTypeID(), bracketEntry, arenatype, false, true, 0, WarGameId, IsRandomBGQueue);
        ginfo2 = bgQueue.AddGroup(opponent, opponentGroup, bg->GetTypeID(), bracketEntry, arenatype, false, true, 0, WarGameId, IsRandomBGQueue);

        if (ginfo1)
            avgTime1 = bgQueue.GetAverageQueueWaitTime(ginfo1, bracketEntry->GetBracketId());

        if (ginfo2)
            avgTime2 = bgQueue.GetAverageQueueWaitTime(ginfo2, bracketEntry->GetBracketId());
    }

    // Challenger Group
    for (GroupReference* itr = challengerGroup->GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member)
            continue;

        if (err)
        {
            sBattlegroundMgr->BuildStatusFailedPacket(bg, member, 0, arenatype, err, &errorGuid);
            continue;
        }

        uint32 queueSlot = member->AddBattlegroundQueueId(bgQueueTypeId);

        member->SetOriginalBGForPlayer(bg->GetTypeID());

        sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, member, queueSlot, avgTime1, ginfo1->JoinTime, arenatype, true);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player joined queue for WarGame as group bg queue type %u bg type %u: GUID %u, NAME %s", bgQueueTypeId, bg->GetTypeID(), member->GetGUID().GetCounter(), member->GetName().c_str());
    }

    // Opponent Group
    for (GroupReference* itr = opponentGroup->GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member)
            continue;

        if (err)
        {
            sBattlegroundMgr->BuildStatusFailedPacket(bg, member, 0, arenatype, err, &errorGuid);
            continue;
        }

        uint32 queueSlot = member->AddBattlegroundQueueId(bgQueueTypeId);

        member->SetOriginalBGForPlayer(bg->GetTypeID());

        sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, member, queueSlot, avgTime2, ginfo2->JoinTime, arenatype, true);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player joined queue for arena as group bg queue type %u bg type %u: GUID %u, NAME %s", bgQueueTypeId, bg->GetTypeID(), member->GetGUID().GetCounter(), member->GetName().c_str());
    }

    sBattlegroundMgr->ScheduleQueueUpdate(WarGameId, RATED_TYPE_NOT_RATED, bgQueueTypeId, bg->GetTypeID(), bracketEntry->GetBracketId());
}
