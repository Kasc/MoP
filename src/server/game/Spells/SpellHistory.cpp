/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Spell.h"
#include "SpellHistory.h"
#include "Pet.h"
#include "Player.h"
#include "SpellInfo.h"
#include "World.h"

SpellHistory::Clock::duration const SpellHistory::InfinityCooldownDelay = std::chrono::duration_cast<SpellHistory::Clock::duration>(std::chrono::seconds(MONTH));

template<>
struct SpellHistory::PersistenceHelper<Player>
{
    static CharacterDatabaseStatements const CooldownsDeleteStatement = CHAR_DEL_CHAR_SPELL_COOLDOWNS;
    static CharacterDatabaseStatements const CooldownsInsertStatement = CHAR_INS_CHAR_SPELL_COOLDOWN;
    static CharacterDatabaseStatements const ChargesDeleteStatement = CHAR_DEL_CHAR_SPELL_CHARGES;
    static CharacterDatabaseStatements const ChargesInsertStatement = CHAR_INS_CHAR_SPELL_CHARGES;

    static void SetIdentifier(PreparedStatement* stmt, uint8 index, Unit* owner)
    {
        //stmt->setUInt32(index, owner->GetGUID().GetCounter());
        stmt->setUInt32(index, owner->GetGUID().GetCounter());
    }

    static bool ReadCooldown(Field* fields, uint32* spellId, CooldownEntry* cooldownEntry)
    {
        *spellId = fields[0].GetUInt32();
        if (!sSpellMgr->GetSpellInfo(*spellId))
            return false;

        cooldownEntry->SpellId = *spellId;
        cooldownEntry->CooldownEnd = Clock::from_time_t(time_t(fields[2].GetUInt32()));
        cooldownEntry->ItemId = fields[1].GetUInt32();
        cooldownEntry->CategoryId = fields[3].GetUInt32();
        cooldownEntry->CategoryEnd = Clock::from_time_t(time_t(fields[4].GetUInt32()));
        return true;
    }

    static bool ReadCharge(Field* fields, uint32* categoryId, ChargeEntry* chargeEntry)
    {
        *categoryId = fields[0].GetUInt32();
        if (!sSpellCategoryStore.LookupEntry(*categoryId))
            return false;

        chargeEntry->RechargeStart = Clock::from_time_t(time_t(fields[1].GetUInt32()));
        chargeEntry->RechargeEnd = Clock::from_time_t(time_t(fields[2].GetUInt32()));
        return true;
    }

    static void WriteCooldown(PreparedStatement* stmt, uint8& index, CooldownStorageType::value_type const& cooldown)
    {
        stmt->setUInt32(index++, cooldown.first);
        stmt->setUInt32(index++, cooldown.second.ItemId);
        stmt->setUInt32(index++, uint32(Clock::to_time_t(cooldown.second.CooldownEnd)));
        stmt->setUInt32(index++, cooldown.second.CategoryId);
        stmt->setUInt32(index++, uint32(Clock::to_time_t(cooldown.second.CategoryEnd)));
    }

    static void WriteCharge(PreparedStatement* stmt, uint8& index, uint32 chargeCategory, ChargeEntry const& charge)
    {
        stmt->setUInt32(index++, chargeCategory);
        stmt->setUInt32(index++, uint32(Clock::to_time_t(charge.RechargeStart)));
        stmt->setUInt32(index++, uint32(Clock::to_time_t(charge.RechargeEnd)));
    }
};

template<>
struct SpellHistory::PersistenceHelper<Pet>
{
    static CharacterDatabaseStatements const CooldownsDeleteStatement = CHAR_DEL_PET_SPELL_COOLDOWNS;
    static CharacterDatabaseStatements const CooldownsInsertStatement = CHAR_INS_PET_SPELL_COOLDOWN;
    static CharacterDatabaseStatements const ChargesDeleteStatement = CHAR_DEL_PET_SPELL_CHARGES;
    static CharacterDatabaseStatements const ChargesInsertStatement = CHAR_INS_PET_SPELL_CHARGES;

    static void SetIdentifier(PreparedStatement* stmt, uint8 index, Unit* owner) { stmt->setUInt32(index, owner->GetCharmInfo()->GetPetNumber()); }

    static bool ReadCooldown(Field* fields, uint32* spellId, CooldownEntry* cooldownEntry)
    {
        *spellId = fields[0].GetUInt32();
        if (!sSpellMgr->GetSpellInfo(*spellId))
            return false;

        cooldownEntry->SpellId = *spellId;
        cooldownEntry->CooldownEnd = Clock::from_time_t(time_t(fields[1].GetUInt32()));
        cooldownEntry->ItemId = 0;
        cooldownEntry->CategoryId = fields[2].GetUInt32();
        cooldownEntry->CategoryEnd = Clock::from_time_t(time_t(fields[3].GetUInt32()));
        return true;
    }

    static bool ReadCharge(Field* fields, uint32* categoryId, ChargeEntry* chargeEntry)
    {
        *categoryId = fields[0].GetUInt32();
        if (!sSpellCategoryStore.LookupEntry(*categoryId))
            return false;

        chargeEntry->RechargeStart = Clock::from_time_t(time_t(fields[1].GetUInt32()));
        chargeEntry->RechargeEnd = Clock::from_time_t(time_t(fields[2].GetUInt32()));
        return true;
    }

    static void WriteCooldown(PreparedStatement* stmt, uint8& index, CooldownStorageType::value_type const& cooldown)
    {
        stmt->setUInt32(index++, cooldown.first);
        stmt->setUInt32(index++, uint32(Clock::to_time_t(cooldown.second.CooldownEnd)));
        stmt->setUInt32(index++, cooldown.second.CategoryId);
        stmt->setUInt32(index++, uint32(Clock::to_time_t(cooldown.second.CategoryEnd)));
    }

    static void WriteCharge(PreparedStatement* stmt, uint8& index, uint32 chargeCategory, ChargeEntry const& charge)
    {
        stmt->setUInt32(index++, chargeCategory);
        stmt->setUInt32(index++, uint32(Clock::to_time_t(charge.RechargeStart)));
        stmt->setUInt32(index++, uint32(Clock::to_time_t(charge.RechargeEnd)));
    }
};

template<class OwnerType>
void SpellHistory::LoadFromDB(PreparedQueryResult cooldownsResult, PreparedQueryResult chargesResult)
{
    typedef PersistenceHelper<OwnerType> StatementInfo;

    if (cooldownsResult)
    {
        do
        {
            uint32 spellId;
            CooldownEntry cooldown;
            if (StatementInfo::ReadCooldown(cooldownsResult->Fetch(), &spellId, &cooldown))
            {
                _spellCooldowns[spellId] = cooldown;
                if (cooldown.CategoryId)
                    _categoryCooldowns[cooldown.CategoryId] = &_spellCooldowns[spellId];
            }

        } while (cooldownsResult->NextRow());
    }

    if (chargesResult)
    {
        do
        {
            Field* fields = chargesResult->Fetch();
            uint32 categoryId = 0;
            ChargeEntry charges;
            if (StatementInfo::ReadCharge(fields, &categoryId, &charges))
                _categoryCharges[categoryId].push_back(charges);

        } while (chargesResult->NextRow());
    }
}

template<class OwnerType>
void SpellHistory::SaveToDB(SQLTransaction& trans)
{
    typedef PersistenceHelper<OwnerType> StatementInfo;

    uint8 index = 0;
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(StatementInfo::CooldownsDeleteStatement);
    StatementInfo::SetIdentifier(stmt, index++, _owner);
    trans->Append(stmt);

    for (auto const& p : _spellCooldowns)
    {
        if (!p.second.OnHold)
        {
            index = 0;
            stmt = CharacterDatabase.GetPreparedStatement(StatementInfo::CooldownsInsertStatement);
            StatementInfo::SetIdentifier(stmt, index++, _owner);
            StatementInfo::WriteCooldown(stmt, index, p);
            trans->Append(stmt);
        }
    }

    stmt = CharacterDatabase.GetPreparedStatement(StatementInfo::ChargesDeleteStatement);
    StatementInfo::SetIdentifier(stmt, 0, _owner);
    trans->Append(stmt);

    for (auto const& p : _categoryCharges)
    {
        for (ChargeEntry const& charge : p.second)
        {
            index = 0;
            stmt = CharacterDatabase.GetPreparedStatement(StatementInfo::ChargesInsertStatement);
            StatementInfo::SetIdentifier(stmt, index++, _owner);
            StatementInfo::WriteCharge(stmt, index, p.first, charge);
            trans->Append(stmt);
        }
    }
}

void SpellHistory::Update()
{
    Clock::time_point now = GameTime::GetGameTimeSystemPoint();
    for (auto itr = _categoryCooldowns.begin(); itr != _categoryCooldowns.end();)
    {
        if (itr->second->CategoryEnd < now)
            itr = _categoryCooldowns.erase(itr);
        else
            ++itr;
    }

    for (auto itr = _spellCooldowns.begin(); itr != _spellCooldowns.end();)
    {
        if (itr->second.CooldownEnd < now)
            itr = EraseCooldown(itr);
        else
            ++itr;
    }

    for (auto& p : _categoryCharges)
    {
        std::deque<ChargeEntry>& chargeRefreshTimes = p.second;
        while (!chargeRefreshTimes.empty() && chargeRefreshTimes.front().RechargeEnd <= now)
            chargeRefreshTimes.pop_front();
    }
}

void SpellHistory::HandleCooldowns(SpellInfo const* spellInfo, Item const* item, Spell* spell /*= nullptr*/)
{
    if (ConsumeCharge(spellInfo->ChargeCategoryEntry))
        return;

    if (Player* player = _owner->ToPlayer())
    {
        // potions start cooldown until exiting combat
        if (item && (item->IsPotion() || spellInfo->IsCooldownStartedOnEvent()))
        {
            player->SetLastPotionId(item->GetEntry());
            return;
        }
    }

    if (spellInfo->IsCooldownStartedOnEvent() || spellInfo->IsPassive() || (spell && spell->IsIgnoringCooldowns()))
        return;

    if (_owner->HasAuraTypeWithAffectMask(SPELL_AURA_IGNORE_SPELL_COOLDOWN, spellInfo))
        return;

    StartCooldown(spellInfo, item ? item->GetEntry() : 0, spell);
}

bool SpellHistory::IsReady(SpellInfo const* spellInfo, uint32 itemId /*= 0*/, bool ignoreCategoryCooldown /*= false*/) const
{
    if (spellInfo->PreventionType & SPELL_PREVENTION_TYPE_SILENCE)
        if (IsSchoolLocked(spellInfo->GetSchoolMask()))
            return false;

    if (HasCooldown(spellInfo->Id, itemId, ignoreCategoryCooldown))
        return false;

    if (!HasCharge(spellInfo->ChargeCategoryEntry))
        return false;

    return true;
}

void SpellHistory::StartCooldown(SpellInfo const* spellInfo, uint32 itemId, Spell* spell /*= nullptr*/, bool onHold /*= false*/)
{
    // init cooldown values
    uint32 categoryId = 0;
    int32 cooldown = -1;
    int32 categoryCooldown = -1;

    GetCooldownDurations(spellInfo, itemId, &cooldown, &categoryId, &categoryCooldown);

    Clock::time_point curTime = GameTime::GetGameTimeSystemPoint();
    Clock::time_point catrecTime;
    Clock::time_point recTime;
    bool needsCooldownPacket = false;

    // overwrite time for selected category
    if (onHold)
    {
        // use +MONTH as infinite cooldown marker
        catrecTime = categoryCooldown > 0 ? (curTime + InfinityCooldownDelay) : curTime;
        recTime = cooldown > 0 ? (curTime + InfinityCooldownDelay) : catrecTime;
    }
    else
    {
        // shoot spells used equipped item cooldown values already assigned in SetBaseAttackTime(RANGED_ATTACK)
        // prevent 0 cooldowns set by another way
        if (cooldown <= 0 && categoryCooldown <= 0 && (categoryId == 76 || (spellInfo->IsAutoRepeatRangedSpell() && spellInfo->Id != 75)))
            cooldown = _owner->GetUInt32Value(UNIT_RANGED_ATTACK_ROUND_BASE_TIME);

        // Now we have cooldown data (if found any), time to apply mods
        if (Player* modOwner = _owner->GetSpellModOwner())
        {
            if (cooldown >= 0)
                modOwner->ApplySpellMod(spellInfo->Id, SPELLMOD_COOLDOWN, cooldown, spell);

            if (categoryCooldown >= 0 && !spellInfo->HasAttribute(SPELL_ATTR6_IGNORE_CATEGORY_COOLDOWN_MODS))
                modOwner->ApplySpellMod(spellInfo->Id, SPELLMOD_COOLDOWN, categoryCooldown, spell);
        }

        if (_owner->HasAuraTypeWithAffectMask(SPELL_AURA_MOD_SPELL_COOLDOWN_BY_HASTE, spellInfo))
        {
            cooldown = int32(cooldown * _owner->GetFloatValue(UNIT_MOD_SPELL_HASTE));
            categoryCooldown = int32(categoryCooldown * _owner->GetFloatValue(UNIT_MOD_SPELL_HASTE));
        }

        if (_owner->HasAuraTypeWithAffectMask(SPELL_AURA_MOD_COOLDOWN_BY_HASTE_REGEN, spellInfo))
        {
            cooldown = int32(cooldown * _owner->GetFloatValue(UNIT_MOD_HASTE_REGEN));
            categoryCooldown = int32(categoryCooldown * _owner->GetFloatValue(UNIT_MOD_HASTE_REGEN));
        }

        if (int32 cooldownMod = _owner->GetTotalAuraModifier(SPELL_AURA_MOD_COOLDOWN))
        {
            // Apply SPELL_AURA_MOD_COOLDOWN only to own spells
            Player* playerOwner = GetPlayerOwner();
            if (!playerOwner || playerOwner->HasSpell(spellInfo->Id))
            {
                needsCooldownPacket = true;
                cooldown += cooldownMod * IN_MILLISECONDS;   // SPELL_AURA_MOD_COOLDOWN does not affect category cooldows, verified with shaman shocks
            }
        }

        // Apply SPELL_AURA_MOD_SPELL_CATEGORY_COOLDOWN modifiers
        // Note: This aura applies its modifiers to all cooldowns of spells with set category, not to category cooldown only
        if (categoryId)
        {
            if (int32 categoryModifier = _owner->GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_SPELL_CATEGORY_COOLDOWN, categoryId))
            {
                if (cooldown > 0)
                    cooldown += categoryModifier;

                if (categoryCooldown > 0)
                    categoryCooldown += categoryModifier;
            }

            SpellCategoryEntry const* categoryEntry = sSpellCategoryStore.AssertEntry(categoryId);
            if (categoryEntry->Flags & SPELL_CATEGORY_FLAG_COOLDOWN_EXPIRES_AT_DAILY_RESET)
                categoryCooldown = int32(std::chrono::duration_cast<std::chrono::milliseconds>(Clock::from_time_t(sWorld->GetNextDailyQuestsResetTime()) - GameTime::GetGameTimeSystemPoint()).count());
        }

        // replace negative cooldowns by 0
        if (cooldown < 0)
            cooldown = 0;

        if (categoryCooldown < 0)
            categoryCooldown = 0;

        // no cooldown after applying spell mods
        if (cooldown == 0 && categoryCooldown == 0)
            return;

        catrecTime = categoryCooldown ? curTime + std::chrono::duration_cast<Clock::duration>(std::chrono::milliseconds(categoryCooldown)) : curTime;
        recTime = cooldown ? curTime + std::chrono::duration_cast<Clock::duration>(std::chrono::milliseconds(cooldown)) : catrecTime;
    }

    // self spell cooldown
    if (recTime != curTime)
    {
        AddCooldown(spellInfo->Id, itemId, recTime, categoryId, catrecTime, onHold);

        if (needsCooldownPacket)
            BuildCooldownPacket(SPELL_COOLDOWN_FLAG_NONE, spellInfo->Id, uint32(cooldown));
    }
}

void SpellHistory::SendCooldownEvent(SpellInfo const* spellInfo, uint32 itemId /*= 0*/, Spell* spell /*= nullptr*/, bool startCooldown /*= true*/)
{
    // Send activate cooldown timer (possible 0) at client side
    if (Player* player = GetPlayerOwner())
    {
        ObjectGuid guid = player->GetGUID();

        uint32 category = spellInfo->GetCategory();
        GetCooldownDurations(spellInfo, itemId, nullptr, &category, nullptr);

        auto categoryItr = _categoryCooldowns.find(category);
        if (categoryItr != _categoryCooldowns.end() && categoryItr->second->SpellId != spellInfo->Id)
        {
            WorldPacket data(SMSG_COOLDOWN_EVENT, 1 + 8 + 4);

            data.WriteGuidMask(guid, 4, 7, 1, 5, 6, 0, 2, 3);

            data.WriteGuidBytes(guid, 5, 7);

            data << uint32(categoryItr->second->SpellId);

            data.WriteGuidBytes(guid, 3, 1, 2, 4, 6, 0);

            player->SendDirectMessage(&data);

            if (startCooldown)
                StartCooldown(sSpellMgr->AssertSpellInfo(categoryItr->second->SpellId), itemId, spell);
        }

        WorldPacket data(SMSG_COOLDOWN_EVENT, 1 + 8 + 4);

        data.WriteGuidMask(guid, 4, 7, 1, 5, 6, 0, 2, 3);

        data.WriteGuidBytes(guid, 5, 7);

        data << uint32(spellInfo->Id);

        data.WriteGuidBytes(guid, 3, 1, 2, 4, 6, 0);

        player->SendDirectMessage(&data);
    }

    // start cooldowns at server side, if any
    if (startCooldown)
        StartCooldown(spellInfo, itemId, spell);
}

void SpellHistory::AddCooldown(uint32 spellId, uint32 itemId, Clock::time_point cooldownEnd, uint32 categoryId, Clock::time_point categoryEnd, bool onHold /*= false*/)
{
    CooldownEntry& cooldownEntry = _spellCooldowns[spellId];
    cooldownEntry.SpellId = spellId;
    cooldownEntry.CooldownEnd = cooldownEnd;
    cooldownEntry.ItemId = itemId;
    cooldownEntry.CategoryId = categoryId;
    cooldownEntry.CategoryEnd = categoryEnd;
    cooldownEntry.OnHold = onHold;

    if (categoryId)
        _categoryCooldowns[categoryId] = &cooldownEntry;
}

void SpellHistory::ModifyCooldown(uint32 spellId, int32 cooldownModMs)
{
    Clock::duration offset = std::chrono::duration_cast<Clock::duration>(std::chrono::milliseconds(cooldownModMs));
    ModifyCooldown(spellId, offset);
}

void SpellHistory::ModifyCooldown(uint32 spellId, Clock::duration offset)
{
    auto itr = _spellCooldowns.find(spellId);
    if (!offset.count() || itr == _spellCooldowns.end())
        return;

    Clock::time_point now = GameTime::GetGameTimeSystemPoint();

    if (itr->second.CooldownEnd + offset > now)
        itr->second.CooldownEnd += offset;
    else
        EraseCooldown(itr);

    if (Player* playerOwner = GetPlayerOwner())
    {
        WorldPacket data(SMSG_MODIFY_COOLDOWN, 1 + 8 + 4 + 4);

        ObjectGuid playerGUID = playerOwner->GetGUID();

        data.WriteGuidMask(playerGUID, 2, 1, 0, 4, 7, 3, 6, 5);

        data.WriteGuidBytes(playerGUID, 4, 1);

        data << int32(std::chrono::duration_cast<std::chrono::milliseconds>(offset).count());

        data.WriteGuidBytes(playerGUID, 3, 6, 7, 5, 0);

        data << uint32(spellId);            // Spell ID

        data.WriteGuidBytes(playerGUID, 2);

        playerOwner->SendDirectMessage(&data);
    }
}

void SpellHistory::ResetCooldown(uint32 spellId, bool update /*= false*/)
{
    auto itr = _spellCooldowns.find(spellId);
    if (itr == _spellCooldowns.end())
        return;

    ResetCooldown(itr, update);
}

void SpellHistory::ResetCooldown(CooldownStorageType::iterator& itr, bool update /*= false*/)
{
    if (update)
        if (Player* playerOwner = GetPlayerOwner())
        {
            bool ClearOnHold = false;

            ObjectGuid guid = playerOwner->GetGUID();

            WorldPacket data(SMSG_CLEAR_COOLDOWN, 1 + 8 + 1 + 4);

            data.WriteGuidMask(guid, 3, 6, 2);

            data.WriteBit(ClearOnHold);

            data.WriteGuidMask(guid, 7, 0, 1, 5, 4);

            data.WriteGuidBytes(guid, 0, 1, 2, 7, 3, 6, 5);

            data << uint32(itr->first);

            data.WriteGuidBytes(guid, 4);

            playerOwner->SendDirectMessage(&data);
        }

    itr = EraseCooldown(itr);
}

void SpellHistory::ResetAllCooldowns()
{
    if (GetPlayerOwner())
    {
        std::vector<int32> cooldowns;
        cooldowns.reserve(_spellCooldowns.size());
        for (auto const& p : _spellCooldowns)
            cooldowns.push_back(p.first);

        SendClearCooldowns(cooldowns);
    }

    _categoryCooldowns.clear();
    _spellCooldowns.clear();
}

bool SpellHistory::HasCooldown(SpellInfo const* spellInfo, uint32 itemId /*= 0*/, bool ignoreCategoryCooldown /*= false*/) const
{
    if (_spellCooldowns.count(spellInfo->Id) != 0)
        return true;

    if (ignoreCategoryCooldown)
        return false;

    uint32 category = 0;
    GetCooldownDurations(spellInfo, itemId, nullptr, &category, nullptr);

    if (!category)
        return false;

    return _categoryCooldowns.count(category) != 0;
}

bool SpellHistory::HasCooldown(uint32 spellId, uint32 itemId /*= 0*/, bool ignoreCategoryCooldown /*= false*/) const
{
    return HasCooldown(sSpellMgr->AssertSpellInfo(spellId), itemId, ignoreCategoryCooldown);
}

uint32 SpellHistory::GetRemainingCooldown(uint32 spellID) const
{
    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellID);
    if (!spellInfo)
        return 0;

    return GetRemainingCooldown(spellInfo);
}

uint32 SpellHistory::GetRemainingCooldown(SpellInfo const* spellInfo) const
{
    if (!spellInfo)
        return 0;

    Clock::time_point end;
    auto itr = _spellCooldowns.find(spellInfo->Id);
    if (itr != _spellCooldowns.end())
        end = itr->second.CooldownEnd;
    else
    {
        auto catItr = _categoryCooldowns.find(spellInfo->GetCategory());
        if (catItr == _categoryCooldowns.end())
            return 0;

        end = catItr->second->CategoryEnd;
    }

    Clock::time_point now = GameTime::GetGameTimeSystemPoint();
    if (end < now)
        return 0;

    Clock::duration remaining = end - now;
    return uint32(std::chrono::duration_cast<std::chrono::milliseconds>(remaining).count());
}

void SpellHistory::LockSpellSchool(SpellSchoolMask schoolMask, uint32 lockoutTime)
{
    Clock::time_point now = GameTime::GetGameTimeSystemPoint();
    Clock::time_point lockoutEnd = now + std::chrono::duration_cast<Clock::duration>(std::chrono::milliseconds(lockoutTime));
    for (uint32 i = 0; i < MAX_SPELL_SCHOOL; ++i)
        if (SpellSchoolMask(1 << i) & schoolMask)
            _schoolLockouts[i] = lockoutEnd;

    std::set<uint32> knownSpells;
    if (Player* plrOwner = _owner->ToPlayer())
    {
        for (auto const& p : plrOwner->GetSpellMap())
            if (p.second->state != PLAYERSPELL_REMOVED)
                knownSpells.insert(p.first);
    }
    else if (Pet* petOwner = _owner->ToPet())
    {
        for (auto const& p : petOwner->m_spells)
            if (p.second.state != PETSPELL_REMOVED)
                knownSpells.insert(p.first);
    }
    else
    {
        Creature* creatureOwner = _owner->ToCreature();
        for (uint8 i = 0; i < MAX_CREATURE_SPELLS; ++i)
            if (creatureOwner->m_spells[i])
                knownSpells.insert(creatureOwner->m_spells[i]);
    }

    PacketCooldowns spellCooldowns;
    for (uint32 spellId : knownSpells)
    {
        SpellInfo const* spellInfo = sSpellMgr->AssertSpellInfo(spellId);
        if (spellInfo->IsCooldownStartedOnEvent())
            continue;

        if (!(spellInfo->PreventionType & SPELL_PREVENTION_TYPE_SILENCE))
            continue;

        if ((schoolMask & spellInfo->GetSchoolMask()) && GetRemainingCooldown(spellInfo) < lockoutTime)
        {
            spellCooldowns[spellId] = lockoutTime;
            AddCooldown(spellId, 0, lockoutEnd, 0, now);
        }
    }

    if (!spellCooldowns.empty())
        BuildCooldownPacket(SPELL_COOLDOWN_FLAG_NONE, spellCooldowns);
}

bool SpellHistory::IsSchoolLocked(SpellSchoolMask schoolMask) const
{
    Clock::time_point now = GameTime::GetGameTimeSystemPoint();
    for (uint32 i = 0; i < MAX_SPELL_SCHOOL; ++i)
        if (SpellSchoolMask(1 << i) & schoolMask)
            if (_schoolLockouts[i] > now)
                return true;

    return false;
}

bool SpellHistory::ConsumeCharge(SpellCategoryEntry const* chargeCategoryEntry)
{
    if (!chargeCategoryEntry)
        return false;

    int32 chargeRecovery = GetChargeRecoveryTime(chargeCategoryEntry);
    if (chargeRecovery > 0 && GetMaxCharges(chargeCategoryEntry) > 0)
    {
        Clock::time_point recoveryStart;
        std::deque<ChargeEntry>& charges = _categoryCharges[chargeCategoryEntry->ID];
        if (charges.empty())
            recoveryStart = GameTime::GetGameTimeSystemPoint();
        else
            recoveryStart = charges.back().RechargeEnd;

        charges.emplace_back(recoveryStart, std::chrono::milliseconds(chargeRecovery));
        return true;
    }

    return false;
}

void SpellHistory::RestoreCharge(SpellCategoryEntry const* chargeCategoryEntry)
{
    if (!chargeCategoryEntry)
        return;

    auto itr = _categoryCharges.find(chargeCategoryEntry->ID);
    if (itr != _categoryCharges.end() && !itr->second.empty())
    {
        itr->second.pop_back();

        if (Player* player = GetPlayerOwner())
        {
            int32 maxCharges = GetMaxCharges(chargeCategoryEntry);
            int32 usedCharges = itr->second.size();
            float count = float(maxCharges - usedCharges);
            if (usedCharges)
            {
                ChargeEntry& charge = itr->second.front();
                std::chrono::milliseconds remaining = std::chrono::duration_cast<std::chrono::milliseconds>(charge.RechargeEnd - GameTime::GetGameTimeSystemPoint());
                std::chrono::milliseconds recharge = std::chrono::duration_cast<std::chrono::milliseconds>(charge.RechargeEnd - charge.RechargeStart);
                count += 1.0f - float(remaining.count()) / float(recharge.count());
            }

            // SMSG_SET_SPELL_CHARGES
        }
    }
}

void SpellHistory::ResetCharges(SpellCategoryEntry const* chargeCategoryEntry)
{
    if (!chargeCategoryEntry)
        return;

    auto itr = _categoryCharges.find(chargeCategoryEntry->ID);
    if (itr != _categoryCharges.end())
    {
        _categoryCharges.erase(itr);

        if (Player* player = GetPlayerOwner())
        {
            ObjectGuid guid = player->GetGUID();

            WorldPacket data(SMSG_CLEAR_SPELL_CHARGES, 1 + 8 + 4);

            data.WriteGuidMask(guid, 4, 6, 2, 3, 1, 7, 0, 5);

            data.WriteGuidBytes(guid, 1, 5, 2, 4);

            data << uint32(chargeCategoryEntry->ID);

            data.WriteGuidBytes(guid, 7, 6, 3, 0);

            player->SendDirectMessage(&data);
        }
    }
}

void SpellHistory::ResetAllCharges()
{
    _categoryCharges.clear();

    if (Player* player = GetPlayerOwner())
    {
        ObjectGuid guid = player->GetGUID();

        WorldPacket data(SMSG_CLEAR_ALL_SPELL_CHARGES, 1 + 8);

        data.WriteGuidMask(guid, 7, 5, 3, 1, 2, 0, 6, 4);
        data.WriteGuidBytes(guid, 6, 2, 3, 0, 1, 7, 5, 0);

        player->SendDirectMessage(&data);
    }
}

bool SpellHistory::HasCharge(SpellCategoryEntry const* chargeCategoryEntry) const
{
    if (!chargeCategoryEntry)
        return true;

    // Check if the spell is currently using charges (untalented warlock Dark Soul)
    int32 maxCharges = GetMaxCharges(chargeCategoryEntry);
    if (maxCharges <= 0)
        return true;

    auto itr = _categoryCharges.find(chargeCategoryEntry->ID);
    return itr == _categoryCharges.end() || itr->second.size() < uint32(maxCharges);
}

int32 SpellHistory::GetMaxCharges(SpellCategoryEntry const* chargeCategoryEntry) const
{
    if (!chargeCategoryEntry)
        return 0;

    uint32 charges = chargeCategoryEntry->MaxCharges;
    charges += _owner->GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_MAX_CHARGES, chargeCategoryEntry->ID);
    return charges;
}

int32 SpellHistory::GetChargeRecoveryTime(SpellCategoryEntry const* chargeCategoryEntry) const
{
    if (!chargeCategoryEntry)
        return 0;

    return chargeCategoryEntry->ChargeRecoveryTime;
}

bool SpellHistory::HasGlobalCooldown(SpellInfo const* spellInfo) const
{
    auto itr = _globalCooldowns.find(spellInfo->StartRecoveryCategory);
    return itr != _globalCooldowns.end() && itr->second > GameTime::GetGameTimeSystemPoint();
}

void SpellHistory::AddGlobalCooldown(SpellInfo const* spellInfo, uint32 duration)
{
    _globalCooldowns[spellInfo->StartRecoveryCategory] = GameTime::GetGameTimeSystemPoint() + std::chrono::duration_cast<Clock::duration>(std::chrono::milliseconds(duration));
}

void SpellHistory::CancelGlobalCooldown(SpellInfo const* spellInfo)
{
    _globalCooldowns[spellInfo->StartRecoveryCategory] = Clock::time_point(Clock::duration(0));
}

Player* SpellHistory::GetPlayerOwner() const
{
    return _owner->GetCharmerOrOwnerPlayerOrPlayerItself();
}

void SpellHistory::SendClearCooldowns(std::vector<int32> const& cooldowns) const
{
    if (Player* playerOwner = GetPlayerOwner())
    {
        ObjectGuid guid = playerOwner->GetGUID();

        WorldPacket data(SMSG_CLEAR_COOLDOWNS, 1 + 8 + 3 + cooldowns.size() * (4));

        data.WriteGuidMask(guid, 5, 6, 7, 3, 2);

        data.WriteBits(cooldowns.size(), 22);

        data.WriteGuidMask(guid, 1, 0, 4);

        data.WriteGuidBytes(guid, 0, 1, 7, 4, 3, 5, 6);

        for (uint32 spellID : cooldowns)
            data << uint32(spellID);

        data.WriteGuidBytes(guid, 2);

        playerOwner->SendDirectMessage(&data);
    }
}

void SpellHistory::BuildCooldownPacket(SpellCooldownFlags flags, uint32 spellId, uint32 cooldown)
{
    Player* player = GetPlayerOwner();
    if (!player)
        return;

    WorldPacket data(SMSG_SPELL_COOLDOWN, 1 + 8 + 3 + 4 + 4 + ((flags != SPELL_COOLDOWN_FLAG_NONE) ? 1 : 0));

    ObjectGuid guid = player->GetGUID();

    data.WriteGuidMask(guid, 0, 6);

    data.WriteBit(flags == SPELL_COOLDOWN_FLAG_NONE);

    data.WriteGuidMask(guid, 7, 3, 1, 5);

    data.WriteBits(1, 21);

    data.WriteGuidMask(guid, 2, 4);

    data.FlushBits();

    data << uint32(spellId);
    data << uint32(cooldown);

    data.WriteGuidBytes(guid, 5, 3, 7);

    if (flags != SPELL_COOLDOWN_FLAG_NONE)
        data << uint8(flags);

    data.WriteGuidBytes(guid, 4, 1, 0, 2, 6);

    player->SendDirectMessage(&data);
}

void SpellHistory::BuildCooldownPacket(SpellCooldownFlags flags, PacketCooldowns const& cooldowns)
{
    Player* player = GetPlayerOwner();
    if (!player)
        return;

    WorldPacket data(SMSG_SPELL_COOLDOWN, 1 + 8 + 3 + cooldowns.size() * (4 + 4) + ((flags != SPELL_COOLDOWN_FLAG_NONE) ? 1 : 0));

    ObjectGuid guid = player->GetGUID();

    data.WriteGuidMask(guid, 0, 6);

    data.WriteBit(flags == SPELL_COOLDOWN_FLAG_NONE);

    data.WriteGuidMask(guid, 7, 3, 1, 5);

    data.WriteBits(cooldowns.size(), 21);

    data.WriteGuidMask(guid, 2, 4);

    data.FlushBits();

    for (std::unordered_map<uint32, uint32>::const_iterator itr = cooldowns.begin(); itr != cooldowns.end(); ++itr)
    {
        data << uint32(itr->first);
        data << uint32(itr->second);
    }

    data.WriteGuidBytes(guid, 5, 3, 7);

    if (flags != SPELL_COOLDOWN_FLAG_NONE)
        data << uint8(flags);

    data.WriteGuidBytes(guid, 4, 1, 0, 2, 6);

    player->SendDirectMessage(&data);
}

void SpellHistory::InitSpellHistory()
{
    Player* player = GetPlayerOwner();
    if (!player)
        return;

    WorldPacket data(SMSG_SEND_SPELL_HISTORY, 3 + _spellCooldowns.size() * (4 + 4 + 4 + 4 + 4));

    ByteBuffer buffer;

    data.WriteBits(_spellCooldowns.size(), 19);

    Clock::time_point now = GameTime::GetGameTimeSystemPoint();

    for (auto const& p : _spellCooldowns)
    {
        uint32 Category = 0;
        uint32 RecoveryTime = 0;
        uint32 CategoryRecoveryTime = 0;

        std::chrono::milliseconds cooldownDuration = std::chrono::duration_cast<std::chrono::milliseconds>(p.second.CooldownEnd - now);
        if (cooldownDuration.count() > 0)
            RecoveryTime = uint32(cooldownDuration.count());

        std::chrono::milliseconds categoryDuration = std::chrono::duration_cast<std::chrono::milliseconds>(p.second.CategoryEnd - now);
        if (categoryDuration.count() > 0)
        {
            Category = p.second.CategoryId;
            CategoryRecoveryTime = uint32(categoryDuration.count());
        }

        data.WriteBit(p.second.OnHold);

        buffer << uint32(CategoryRecoveryTime);
        buffer << uint32(p.second.ItemId);
        buffer << uint32(RecoveryTime);
        buffer << uint32(Category);
        buffer << uint32(p.first);
    }

    if (!buffer.empty())
        data.append(buffer);
    else
        data.FlushBits();

    player->SendDirectMessage(&data);
}

void SpellHistory::InitSpellCharges()
{
    Player* player = GetPlayerOwner();
    if (!player)
        return;

    WorldPacket data(SMSG_SEND_SPELL_CHARGES, 3 + _categoryCharges.size() * (4 + 1 + 4));

    data.WriteBits(_categoryCharges.size(), 21);

    data.FlushBits();

    Clock::time_point now = GameTime::GetGameTimeSystemPoint();

    for (auto const& p : _categoryCharges)
    {
        if (!p.second.empty())
        {
            std::chrono::milliseconds cooldownDuration = std::chrono::duration_cast<std::chrono::milliseconds>(p.second.front().RechargeEnd - now);
            if (cooldownDuration.count() <= 0)
                continue;

            data << uint32(cooldownDuration.count());
            data << uint8(p.second.size());
            data << uint32(p.first);
        }
    }

    player->SendDirectMessage(&data);
}

void SpellHistory::GetCooldownDurations(SpellInfo const* spellInfo, uint32 itemId, int32* cooldown, uint32* categoryId, int32* categoryCooldown)
{
    ASSERT(cooldown || categoryId || categoryCooldown);
    int32 tmpCooldown = -1;
    uint32 tmpCategoryId = 0;
    int32 tmpCategoryCooldown = -1;

    // cooldown information stored in ItemEffect.db2, overriding normal cooldown and category
    if (itemId)
        if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(itemId))
            for (ItemEffectEntry const* itemEffect : proto->Effects)
                if (itemEffect->SpellID == spellInfo->Id)
                {
                    tmpCooldown = itemEffect->Cooldown;
                    tmpCategoryId = itemEffect->Category;
                    tmpCategoryCooldown = itemEffect->CategoryCooldown;
                    break;
                }

    // if no cooldown found above then base at DBC data
    if (tmpCooldown < 0 && tmpCategoryCooldown < 0)
    {
        tmpCooldown = spellInfo->RecoveryTime;
        tmpCategoryId = spellInfo->GetCategory();
        tmpCategoryCooldown = spellInfo->CategoryRecoveryTime;
    }

    if (cooldown)
        *cooldown = tmpCooldown;
    if (categoryId)
        *categoryId = tmpCategoryId;
    if (categoryCooldown)
        *categoryCooldown = tmpCategoryCooldown;
}

template void SpellHistory::LoadFromDB<Player>(PreparedQueryResult cooldownsResult, PreparedQueryResult chargesResult);
template void SpellHistory::LoadFromDB<Pet>(PreparedQueryResult cooldownsResult, PreparedQueryResult chargesResult);
template void SpellHistory::SaveToDB<Player>(SQLTransaction& trans);
template void SpellHistory::SaveToDB<Pet>(SQLTransaction& trans);
