/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PET_SPAWN_MGR
#define BATTLE_PET_SPAWN_MGR

#include "Singleton/Singleton.hpp"

#include "BattlePet.h"
#include "Common.h"

struct WildBattlePetInfo
{
    WildBattlePetInfo(ObjectGuid creatureGUID, uint32 speciesId, uint32 zoneId, uint32 mapId, uint32 zoneLimit, uint8 minLevel, uint8 maxLevel) : 
                    CreatureGUID(creatureGUID), SpeciesID(speciesId), ZoneID(zoneId), MapID(mapId), ZoneLimit(zoneLimit), MinLevel(minLevel), MaxLevel(maxLevel)
                    { }

    ObjectGuid CreatureGUID;
    ObjectGuid WildPetGUID;
    uint32 SpeciesID;
    uint32 ZoneID;
    uint32 MapID;
    uint32 ZoneLimit;
    uint8 MinLevel;
    uint8 MaxLevel;
};

typedef std::vector<WildBattlePetInfo*> WildBattlePetInfoStore;

typedef std::unordered_map<ObjectGuid, BattlePet*> BattlePetInfoStore;

typedef std::array<uint8, BATTLE_PET_FAMILY_COUNT> ZoneLimit;
typedef std::unordered_map<uint32, ZoneLimit> WildPetZoneLimit;

// handles global spawning of battle pets
class BattlePetSpawnMgr
{
public:
    BattlePetSpawnMgr();
    ~BattlePetSpawnMgr();

    BattlePet* GetWildBattlePet(ObjectGuid guid);

    void AddWildCreature(Creature* creature, uint32 zoneId, int32 mapId);
    void AddWildCreature(Creature* creature, uint32 zoneId);

    void SpawnWildBattlePet(WildBattlePetInfo* wildPet);
    void RemoveWildBattlePet(Creature* creature, bool deleteCreature = false);

    void EnteredBattle(Creature* creature);
    void LeftBattle(Creature* creature, bool killed);

    void Update(uint32 diff);

private:
    WildBattlePetInfoStore _wildBattlePetInfoStore;
    BattlePetInfoStore _battlePetInfoStore;
    WildPetZoneLimit _wildBattlePetZoneLimitsStore;

    uint32 _spawnTimer;
};

#define sBattlePetSpawnMgr Tod::Singleton<BattlePetSpawnMgr>::GetSingleton()

#endif
