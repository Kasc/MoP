/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PET_MGR_H
#define BATTLE_PET_MGR_H

#include "BattlePet.h"
#include "Common.h"
#include "TemporarySummon.h"

class BattlePet;

typedef std::vector<BattlePet*> BattlePetVec;

#define BATTLE_PET_MAX_JOURNAL_SPECIES 3
#define BATTLE_PET_MAX_JOURNAL_PETS    500

#define BATTLE_PET_ABILITY_TRAP          427
#define BATTLE_PET_ABILITY_STRONG_TRAP   77
#define BATTLE_PET_ABILITY_PRISTINE_TRAP 135
#define BATTLE_PET_ABILITY_GM_TRAP       549

enum BattlePetLoadoutSlots
{
    BATTLE_PET_LOADOUT_SLOT_1                   = 0,
    BATTLE_PET_LOADOUT_SLOT_2                   = 1,
    BATTLE_PET_LOADOUT_SLOT_3                   = 2,
    BATTLE_PET_LOADOUT_SLOT_NONE                = 3
};

// custom flags used in 'account_battle_pet_slots' table
enum BattlePetLoadoutFlags
{
    BATTLE_PET_LOADOUT_SLOT_FLAG_NONE           = 0x00,
    BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_1         = 0x01,
    BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_2         = 0x02,
    BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_3         = 0x04,
    BATTLE_PET_LOADOUT_TRAP                     = 0x08,
    BATTLE_PET_LOADOUT_STRONG_TRAP              = 0x10,
    BATTLE_PET_LOADOUT_PRISTINE_TRAP            = 0x20,
    BATTLE_PET_LOADOUT_GM_TRAP                  = 0x40
};

enum BattlePetJournalFlags
{
    BATTLE_PET_JOURNAL_FLAG_NONE                = 0x00,
    BATTLE_PET_JOURNAL_FLAG_FAVORITES           = 0x01,
    BATTLE_PET_JOURNAL_FLAG_COLLECTED           = 0x02,
    BATTLE_PET_JOURNAL_FLAG_NOT_COLLECTED       = 0x04,
    BATTLE_PET_JOURNAL_FLAG_UNKNOWN_1           = 0x08,
    BATTLE_PET_JOURNAL_FLAG_ABILITY_1           = 0x10, // ability flags are set if the second ability for that slot is selected
    BATTLE_PET_JOURNAL_FLAG_ABILITY_2           = 0x20, // ...
    BATTLE_PET_JOURNAL_FLAG_ABILITY_3           = 0x40  // ...
};

enum BattlePetSpells
{
    SPELL_BATTLE_PET_TRAINING_PASSIVE           = 119467,
    SPELL_BATTLE_PET_TRAINING                   = 125610,
    SPELL_TRACK_PETS                            = 122026,
    SPELL_REVIVE_BATTLE_PETS                    = 125439
};

enum BattlePetFlagModes
{
    BATTLE_PET_FLAG_MODE_UNSET  = 0,
    BATTLE_PET_FLAG_MODE_SET    = 3
};

class BattlePetMgr
{
public:
    BattlePetMgr(Player* owner) : m_owner(owner), m_summon(NULL), m_loadoutFlags(0), m_loadoutSave(false) { }

    ~BattlePetMgr();

    void LoadFromDB(PreparedQueryResult result);
    void SaveToDB(SQLTransaction& trans);
    void LoadSlotsFromDB(PreparedQueryResult result);
    void SaveSlotsToDB(SQLTransaction& trans);

    BattlePet* Create(uint32 speciesId, uint8 level = 1, uint8 breed = 0, uint8 quality = 0, uint8 family = 0, bool ignoreChecks = false);
    void Delete(BattlePet* battlePet);

    Player* GetOwner() const { return m_owner; }
    uint8 GetBattlePetCount(uint32 speciesId) const;

    BattlePet* GetBattlePet(ObjectGuid guid) const;

    uint8 GetLoadoutSlotForBattlePet(ObjectGuid guid) const;

    ObjectGuid GetCurrentSummonGUID() const { return m_summonGUID; }
    TempSummon* GetCurrentSummon() const { return m_summon; }
    void SetCurrentSummonGUID(ObjectGuid summonGUID) { m_summonGUID = summonGUID; }
    void SetCurrentSummon(TempSummon* summon) { m_summon = summon; }

    bool HasLoadoutSlot(uint8 slot) const;
    ObjectGuid GetLoadoutSlot(uint8 slot) const;
    void SetLoadoutSlot(uint8 slot, ObjectGuid guid, bool save);
    void UnlockLoadoutSlot(uint8 slot);

    bool HasLoadoutFlag(uint8 flag) const { return (m_loadoutFlags & flag) != 0; };
    void SetLoadoutFlag(uint8 flag);

    bool CanStoreBattlePet(uint32 species) const;
    uint32 GetTrapAbility() const;

    void UnSummonCurrentBattlePet(bool temporary);
    void ResummonLastBattlePet();

    void SendBattlePetJournal();
    void SendBattlePetUpdate(BattlePet* battlePet, bool notification);
    void SendPetBattleSlotUpdate(bool notification, uint8 slot);

    BattlePetVec BattlePets;

private:
    Player* m_owner;

    TempSummon* m_summon;

    ObjectGuid m_summonGUID;
    ObjectGuid m_summonLastId;

    bool m_loadoutSave;
    uint8 m_loadoutFlags;
    std::array<ObjectGuid, BATTLE_PET_MAX_LOADOUT_SLOTS> m_loadout;
};

#endif
