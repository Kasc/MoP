/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePetQueue.h"
#include "ObjectMgr.h"

// 98147 Invisibility

bool PetBattleQueueRemoveEvent::Execute(uint64 /*e_time*/, uint32 /*p_time*/)
{
    if (PetBattleQueueInfo* queueInfo = sPetBattleQueueMgr->GetPetBattleQueueInfo(challengerQueueGUID))
        if (!queueInfo->OpponentGUID.IsEmpty())
            if (Player* player = ObjectAccessor::FindPlayer(queueInfo->OwnerGUID))
            {
                sPetBattleQueueMgr->RemovePlayerFromPetBattleQueue(queueInfo->OwnerGUID);

                if (PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(player->GetGUID()))
                    petBattle->FinishBattle(nullptr);

                player->GetSession()->SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_PROPOSAL_TIMEOUT);
            }

    if (PetBattleQueueInfo* queueInfo = sPetBattleQueueMgr->GetPetBattleQueueInfo(opponentQueueGUID))
        if (!queueInfo->OpponentGUID.IsEmpty())
            if (Player* player = ObjectAccessor::FindPlayer(queueInfo->OwnerGUID))
            {
                sPetBattleQueueMgr->RemovePlayerFromPetBattleQueue(queueInfo->OwnerGUID);

                if (PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(player->GetGUID()))
                    petBattle->FinishBattle(nullptr);

                player->GetSession()->SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_PROPOSAL_TIMEOUT);
            }

    return true;
}

void PetBattleQueueRemoveEvent::Abort(uint64 /*e_time*/)
{
    //do nothing
}

PetBattleQueue::PetBattleQueue()
{
    _queueTimer = PET_BATTLE_TIMER;
    _averageSumTimer = 0;
    _averageCount = 0;
}

PetBattleQueue::~PetBattleQueue()
{
    m_events.KillAllEvents(false);

    for (auto itr : _petBattleQueueList)
        delete itr.second;

    _petBattleQueueList.clear();
}

uint32 PetBattleQueue::AddPlayerToPetBattleQueue(ObjectGuid guid, uint32 petLevel)
{
    PetBattleQueueInfo* queueInfo = new PetBattleQueueInfo(guid, petLevel, getMSTime());

    _petBattleQueueList[guid] = queueInfo;

    if (!_averageSumTimer)
        return 0;

    return _averageSumTimer / _averageCount;
}

void PetBattleQueue::RemovePlayerFromPetBattleQueue(ObjectGuid guid)
{
    auto itr = _petBattleQueueList.find(guid);
    if (itr != _petBattleQueueList.end())
        if (PetBattleQueueInfo* queueInfo = itr->second)
        {
            _petBattleQueueList.erase(guid);
            delete queueInfo;
            queueInfo = nullptr;
        }
}

PetBattleQueueSlotError PetBattleQueue::GetPetBattleQueueSlotError(BattlePetMgr const* battlePetMgr, uint8 Index) const
{
    if (!battlePetMgr->HasLoadoutSlot(Index))
        return PET_BATTLE_QUEUE_ERROR_SLOT_EMPTY;

    ObjectGuid petGUID = battlePetMgr->GetLoadoutSlot(Index);
    if (petGUID.IsEmpty())
        return PET_BATTLE_QUEUE_ERROR_SLOT_NO_TRACKER;

    BattlePet const* battlePet = battlePetMgr->GetBattlePet(petGUID);
    if (!battlePet)
        return PET_BATTLE_QUEUE_ERROR_SLOT_NO_PET;

    if (!battlePet->IsAlive())
        return PET_BATTLE_QUEUE_ERROR_SLOT_DEAD;

    return PET_BATTLE_QUEUE_ERROR_SLOT_NONE;
}

void PetBattleQueue::Update(uint32 diff)
{
    m_events.Update(diff);

    if (_queueTimer <= diff)
    {
        _queueTimer = PET_BATTLE_TIMER;

        for (auto itr : _petBattleQueueList)
        {
            PetBattleQueueInfo* queueInfo = itr.second;
            if (!queueInfo)
                continue;

            Player* challenger = ObjectAccessor::FindPlayer(queueInfo->OwnerGUID);
            if (!challenger)
                continue;

            if (queueInfo->IsSuspended)
                continue;

            if (!queueInfo->OpponentGUID.IsEmpty())
                continue;

            uint32 petLevel = queueInfo->PetLevel;

            // every 30 sec add 2 pet levels
            uint32 timeDiff = GetMSTimeDiffToNow(queueInfo->JoinTime);
            int32 petDiff = (timeDiff / 30 * IN_MILLISECONDS) * 2;

            PetBattleQueueInfo* opponentQueueInfo = nullptr;

            for (auto itr2 : _petBattleQueueList)
            {
                PetBattleQueueInfo* queueInfo2 = itr2.second;
                if (!queueInfo2)
                    continue;

                Player* opponent = ObjectAccessor::FindPlayer(queueInfo2->OwnerGUID);
                if (!opponent)
                    continue;

                if (queueInfo->OwnerGUID == queueInfo2->OwnerGUID)
                    continue;

                if (challenger->GetMapId() != opponent->GetMapId())
                    continue;

                if (queueInfo2->IsSuspended)
                    continue;

                if (!queueInfo2->OpponentGUID.IsEmpty())
                    continue;

                int32 petDiff2 = abs(int32(petLevel - queueInfo2->PetLevel));
                if (petDiff2 <= petDiff)
                {
                    petDiff = petDiff2;
                    opponentQueueInfo = queueInfo2;
                }
            }

            if (!opponentQueueInfo)
                continue;

            Player* opponent = ObjectAccessor::FindPlayer(opponentQueueInfo->OwnerGUID);
            if (!opponent)
                continue;

            // opponent found, now find battle pet location
            std::vector<BattlePetPvPLocation>* locations = sObjectMgr->GetBattlePetPvPLocationsForMap(challenger->GetMapId());
            if (!locations)
                continue;

            std::vector<BattlePetPvPLocation*> randomLocations;
            for (BattlePetPvPLocation& location : *locations)
            {
                if (location.factionID)
                {
                    // check teams
                    if (challenger->GetTeam() != opponent->GetTeam())
                        continue;

                    if (challenger->GetTeam() != location.factionID)
                        continue;
                }

                randomLocations.push_back(&location);
            }

            if (randomLocations.empty())
                continue;

            BattlePetPvPLocation* pvpLoc = Trinity::Containers::SelectRandomContainerElement(randomLocations);
            if (!pvpLoc)
                continue;

            queueInfo->OpponentGUID = opponentQueueInfo->OwnerGUID;
            queueInfo->BattlePosition = pvpLoc->pos;
            queueInfo->PlayerPosition.WorldRelocate(challenger->GetMapId(), pvpLoc->pos.GetPositionX() - 4.40f, pvpLoc->pos.GetPositionY() - 2.40f, pvpLoc->pos.GetPositionZ());
            queueInfo->PlayerPosition.SetOrientation(queueInfo->PlayerPosition.GetAngle(pvpLoc->pos.GetPositionX(), pvpLoc->pos.GetPositionY()));

            opponentQueueInfo->OpponentGUID = queueInfo->OwnerGUID;
            opponentQueueInfo->BattlePosition = pvpLoc->pos;
            opponentQueueInfo->PlayerPosition.WorldRelocate(opponent->GetMapId(), pvpLoc->pos.GetPositionX() + 4.40f, pvpLoc->pos.GetPositionY() + 2.40f, pvpLoc->pos.GetPositionZ());
            opponentQueueInfo->PlayerPosition.SetOrientation(opponentQueueInfo->PlayerPosition.GetAngle(pvpLoc->pos.GetPositionX(), pvpLoc->pos.GetPositionY()));

            PetBattleQueueRemoveEvent* removeEvent = new PetBattleQueueRemoveEvent(queueInfo->OwnerGUID, opponentQueueInfo->OwnerGUID);
            m_events.AddEvent(removeEvent, m_events.CalculateTime(PET_BATTLE_TIMER));

            challenger->GetSession()->SetPetBattleQueueProposeMatch();
            opponent->GetSession()->SetPetBattleQueueProposeMatch();
        }
    }
    else
        _queueTimer -= diff;
}

uint32 PetBattleQueue::GetAverageWaitTimer() const
{
    if (!_averageSumTimer)
        return 0;

    return _averageSumTimer / _averageCount;
}

void PetBattleQueue::UpdateAverageWaitTimer(uint32 waitedTimer)
{
    ++_averageCount;
    _averageSumTimer += waitedTimer;
}
