/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PET_ABILITY_EFFECT_H
#define BATTLE_PET_ABILITY_EFFECT_H

#include "Common.h"
#include "BattlePet.h"
#include "DB2Stores.h"

static const uint32 FailFlags = PET_BATTLE_EFFECT_FLAG_MISS | PET_BATTLE_EFFECT_FLAG_BLOCKED | PET_BATTLE_EFFECT_FLAG_DODGE | PET_BATTLE_EFFECT_FLAG_UNKILLABLE | PET_BATTLE_EFFECT_FLAG_REFLECT | PET_BATTLE_EFFECT_FLAG_IMMUNE;

enum PetBattleAbilityTarget
{
    PET_BATTLE_TARGET_ACTIVE_ALLY               = 0,
    PET_BATTLE_TARGET_ALLY_PET_2                = 1,
    PET_BATTLE_TARGET_ALLY_PET_3                = 2,
    PET_BATTLE_TARGET_ACTIVE_ENEMY              = 3,
    PET_BATTLE_TARGET_ENEMY_PET_2               = 4,
    PET_BATTLE_TARGET_ENEMY_PET_3               = 5,
    PET_BATTLE_TARGET_ALLY_TEAM                 = 6,
    PET_BATTLE_TARGET_ENEMY_TEAM                = 7,
    PET_BATTLE_TARGET_AURA_OWNER                = 8,
    PET_BATTLE_TARGET_AURA_CASTER               = 9
};

#define PET_BATTLE_TOTAL_ABILITY_EFFECTS 205

class BattlePetAbilityEffect
{
public:
    BattlePetAbilityEffect() : m_flags(0), m_reportFailAsImmune(0), m_chainFailure(false), m_isPeriodic(false), m_isTeamAuraEffect(false) { }

    void SetAbilityInfo(uint32 ability, BattlePetAbilityEffectEntry const* effectEntry, uint8 family);

    void SetCaster(BattlePet* caster)           { m_caster = caster; }
    void SetParentBattle(PetBattle* petBattle)  { m_petBattle = petBattle; }

    void SetTarget(BattlePet* auraCaster = nullptr, BattlePet* auraOwner = nullptr, int8 teamIndex = -1);

    uint32 CalculateDamage(uint32 damage);
    uint32 CalculateHeal(uint32 heal);
    void CalculateHit(int32 accuracy);

    void Damage(BattlePet* target, uint32 damage);
    void Heal(BattlePet* target, uint32 heal);

    void SetHealth(BattlePet* target, uint32 value);

    void Execute();

    void HandleNull() { }

    void HandleHeal();
    void HandleDamage();
    void HandleDamageOnState();
    void HandleCatch();
    void HandlePositiveAura();
    void HandleRampingDamage();
    void HandleSetState();
    void HandleHealPctDealt();
    void HandleHealPctTaken();
    void HandleRemoveAura();
    void HandleNegativeAura();
    void HandleAddExtraAura();
    void HandleHealPct();
    void HandlePeriodicTrigger();
    void HandleDamageOnLowerHealth();
    void HandleConsumeCorpse();
    void HandleDamagePct();
    void HandlePeriodicPositiveTrigger();
    void HandleDamageWithBonus();
    void HandleDamageOnHealthDeficit();
    void HandleEqualizeHealth();
    void HandleDamageCasterPct();
    void HandleHealToggleAura();
    void HandleDamageToggleAura();
    void HandlePlanted();
    void HandleStateChanged();
    void HandleWeather();
    void HandleRemoveAurasWithState();
    void HandleGravity();
    void HandleDamageHitState();
    void HandleHealWithPetType();
    void HandleExtraAttackFirst();
    void HandleHealState();
    void HandleForceSwapPet();
    void HandleResurrect();
    void HandleSetHealthPct();
    void HandleLockNextAbility();
    void HandleInterruptOpponentsRound();
    void HandleInstantKill();
    void HandleDamageTripple();
    void HandleStateChangedOnState();
    void HandleForceSwapPetWithLowestHP();
    void HandleRefreshAura();
    void HandleSplitDamage();
    void HandleForceSwapPetWithHighestHP();
    void HandleDamageNonLethal();
    void HandlePositiveAura2();
    void HandleDamageIfSlower();
    void HandleDamageOnWeatherState();
    void HandleHealOnWeatherState();
    void HandleAuraWithAccuracyMod();
    void HandlePowerlessAura();
    void HandleDamageLastTaken();
    void HandleDamageReflected();

private:
    uint32 m_abilityId;
    uint8 m_abilityFamily;
    uint32 m_flags;

    BattlePet* m_caster;
    BattlePet* m_target;                                        // current target being executed on

    BattlePetAbilityEffectEntry const* m_effectEntry;
    BattlePetAbilityTurnEntry* m_turnEntry;
    PetBattle* m_petBattle;

    bool m_reportFailAsImmune;
    bool m_chainFailure;
    bool m_isPeriodic;

    bool m_isTeamAuraEffect;
};

#endif
