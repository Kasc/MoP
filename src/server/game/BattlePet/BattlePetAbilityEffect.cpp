/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePetAbilityEffect.h"
#include "Chat.h"
#include "DBCStores.h"
#include "Util.h"
#include "Containers.h"

#include <sstream>

struct BattlePetAbilityEffectHandler
{
    void (BattlePetAbilityEffect::*Handle)();
};

static BattlePetAbilityEffectHandler Handlers[PET_BATTLE_TOTAL_ABILITY_EFFECTS] =
{
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 022 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 023 */ { &BattlePetAbilityEffect::HandleHeal,                         },
    /* Effect 024 */ { &BattlePetAbilityEffect::HandleDamage,                       },
    /* Effect 025 */ { &BattlePetAbilityEffect::HandleCatch,                        },
    /* Effect 026 */ { &BattlePetAbilityEffect::HandlePositiveAura,                 },
    /* Effect 027 */ { &BattlePetAbilityEffect::HandleRampingDamage,                },
    /* Effect 028 */ { &BattlePetAbilityEffect::HandlePositiveAura,                 },
    /* Effect 029 */ { &BattlePetAbilityEffect::HandleDamageOnState,                },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 031 */ { &BattlePetAbilityEffect::HandleSetState,                     },
    /* Effect 032 */ { &BattlePetAbilityEffect::HandleHealPctDealt,                 },
    /* Effect 033 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 043 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 044 */ { &BattlePetAbilityEffect::HandleHealPctTaken,                 },
    /* Effect 045 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 049 */ { &BattlePetAbilityEffect::HandleRemoveAura,                   },
    /* Effect 050 */ { &BattlePetAbilityEffect::HandleNegativeAura,                 },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 052 */ { &BattlePetAbilityEffect::HandleAddExtraAura,                 },
    /* Effect 053 */ { &BattlePetAbilityEffect::HandleHealPct,                      },
    /* Effect 054 */ { &BattlePetAbilityEffect::HandlePeriodicTrigger,              },
    /* Effect 055 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 056 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 057 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 058 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 059 */ { &BattlePetAbilityEffect::HandleDamageOnLowerHealth,          },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 061 */ { &BattlePetAbilityEffect::HandleConsumeCorpse,                },
    /* Effect 062 */ { &BattlePetAbilityEffect::HandleDamagePct,                    },
    /* Effect 063 */ { &BattlePetAbilityEffect::HandlePeriodicPositiveTrigger,      },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 065 */ { &BattlePetAbilityEffect::HandleDamageWithBonus,              },
    /* Effect 066 */ { &BattlePetAbilityEffect::HandleDamageOnHealthDeficit,        },
    /* Effect 067 */ { &BattlePetAbilityEffect::HandleEqualizeHealth,               },
    /* Effect 068 */ { &BattlePetAbilityEffect::HandleDamageCasterPct,              },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 072 */ { &BattlePetAbilityEffect::HandleHealToggleAura,               },
    /* Effect 073 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 074 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 075 */ { &BattlePetAbilityEffect::HandleDamageToggleAura,             },
    /* Effect 076 */ { &BattlePetAbilityEffect::HandleDamageToggleAura,             },
    /* Effect 077 */ { &BattlePetAbilityEffect::HandleDamageToggleAura,             },
    /* Effect 078 */ { &BattlePetAbilityEffect::HandlePlanted,                      },
    /* Effect 079 */ { &BattlePetAbilityEffect::HandleStateChanged,                 },
    /* Effect 080 */ { &BattlePetAbilityEffect::HandleWeather,                      },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 085 */ { &BattlePetAbilityEffect::HandleRemoveAurasWithState,         },
    /* Effect 086 */ { &BattlePetAbilityEffect::HandleGravity,                      },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 096 */ { &BattlePetAbilityEffect::HandleDamageHitState,               },
    /* Effect 097 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 099 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 100 */ { &BattlePetAbilityEffect::HandleHealWithPetType,              },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 103 */ { &BattlePetAbilityEffect::HandleExtraAttackFirst,             },
    /* Effect 104 */ { &BattlePetAbilityEffect::HandleHealState,                    },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 107 */ { &BattlePetAbilityEffect::HandleForceSwapPet,                 },
    /* Effect 108 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 111 */ { &BattlePetAbilityEffect::HandleResurrect,                    },
    /* Effect 112 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 116 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 117 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 121 */ { &BattlePetAbilityEffect::HandleNull,                         }, // CreateClone (deprecated)
    /* Effect 122 */ { &BattlePetAbilityEffect::HandleNull,                         }, // CreateClone (deprecated)
    /* Effect 123 */ { &BattlePetAbilityEffect::HandleNull,                         }, // CreateClone (deprecated)
    /* Effect 124 */ { &BattlePetAbilityEffect::HandleNull,                         }, // CreateClone (deprecated)
    /* Effect 125 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 128 */ { &BattlePetAbilityEffect::HandleSetHealthPct,                 },
    /* Effect 129 */ { &BattlePetAbilityEffect::HandleLockNextAbility,              },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 131 */ { &BattlePetAbilityEffect::HandleInterruptOpponentsRound,      },
    /* Effect 132 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 133 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 134 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 135 */ { &BattlePetAbilityEffect::HandleInstantKill,                  },
    /* Effect 136 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 137 */ { &BattlePetAbilityEffect::HandleDamageTripple,                },
    /* Effect 138 */ { &BattlePetAbilityEffect::HandleStateChangedOnState,          },
    /* Effect 139 */ { &BattlePetAbilityEffect::HandleForceSwapPetWithLowestHP,     },
    /* Effect 140 */ { &BattlePetAbilityEffect::HandleRefreshAura,                  },
    /* Effect 141 */ { &BattlePetAbilityEffect::HandleSplitDamage,                  },
    /* Effect 142 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 143 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 144 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 145 */ { &BattlePetAbilityEffect::HandleForceSwapPetWithHighestHP,    },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 147 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 149 */ { &BattlePetAbilityEffect::HandleDamageNonLethal,              },
    /* Effect 150 */ { &BattlePetAbilityEffect::HandlePositiveAura2,                },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 156 */ { &BattlePetAbilityEffect::HandleSetState,                     },
    /* Effect 157 */ { &BattlePetAbilityEffect::HandleSetState,                     },
    /* Effect 158 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 159 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 160 */ { &BattlePetAbilityEffect::HandleDamageIfSlower,               },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 164 */ { &BattlePetAbilityEffect::HandleDamage,                       },
    /* Effect 165 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 168 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 169 */ { &BattlePetAbilityEffect::HandleDamageOnState,                },
    /* Effect 170 */ { &BattlePetAbilityEffect::HandleDamageOnWeatherState,         },
    /* Effect 171 */ { &BattlePetAbilityEffect::HandleHealOnWeatherState,           },
    /* Effect 172 */ { &BattlePetAbilityEffect::HandleAuraWithAccuracyMod,          },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 177 */ { &BattlePetAbilityEffect::HandleSetState,                     },
    /* Effect 178 */ { &BattlePetAbilityEffect::HandlePowerlessAura,                },
    /* Effect 179 */ { &BattlePetAbilityEffect::HandleDamage,                       },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 194 */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 197 */ { &BattlePetAbilityEffect::HandleDamageLastTaken,              },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /*   UNUSED   */ { &BattlePetAbilityEffect::HandleNull,                         },
    /* Effect 204 */ { &BattlePetAbilityEffect::HandleDamageReflected,              }
};

void BattlePetAbilityEffect::SetAbilityInfo(uint32 ability, BattlePetAbilityEffectEntry const* effectEntry, uint8 family)
{
    // ability information
    m_abilityId     = ability;
    m_effectEntry   = effectEntry;
    m_abilityFamily = family;
}

void BattlePetAbilityEffect::Execute()
{
    if (!Handlers[m_effectEntry->EffectProperty].Handle)
        return;

    if (!m_target)
        return;

    if (!m_target->IsTargetable())
        m_flags |= PET_BATTLE_EFFECT_FLAG_MISS;

    (this->*Handlers[m_effectEntry->EffectProperty].Handle)();

    m_petBattle->Cast(m_target, m_abilityId, 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_ABILITY);
}

void BattlePetAbilityEffect::SetTarget(BattlePet* auraCaster, BattlePet* auraOwner, int8 TeamIndex/* = -1*/)
{
    int8 targetID = sObjectMgr->GetEffectIdForAbilityEffect(m_effectEntry->Id);
    if (targetID == -1)
        return;

    if (TeamIndex == -1)
        TeamIndex = m_caster->GetTeamIndex();

    switch (targetID)
    {
        case PET_BATTLE_TARGET_ACTIVE_ALLY:
            if (PetBattleTeam* allyTeam = m_petBattle->Teams[TeamIndex])
                m_target = allyTeam->GetActivePet();
            break;
        case PET_BATTLE_TARGET_ALLY_PET_2:
        case PET_BATTLE_TARGET_ALLY_PET_3:
        {
            uint8 Pos = PET_BATTLE_TARGET_ACTIVE_ALLY;
            if (PetBattleTeam* allyTeam = m_petBattle->Teams[TeamIndex])
                for (BattlePet* battlePet : allyTeam->BattlePets)
                {
                    if (battlePet->GetGlobalIndex() == allyTeam->GetActivePet()->GetGlobalIndex())
                        continue;

                    if (++Pos == targetID)
                    {
                        m_target = battlePet;
                        break;
                    }
                }

            break;
        }
        case PET_BATTLE_TARGET_ACTIVE_ENEMY:
            if (PetBattleTeam* enemyTeam = m_petBattle->Teams[!TeamIndex])
                m_target = enemyTeam->GetActivePet();
            break;
        case PET_BATTLE_TARGET_ENEMY_PET_2:
        case PET_BATTLE_TARGET_ENEMY_PET_3:
        {
            uint8 Pos = PET_BATTLE_TARGET_ACTIVE_ENEMY;
            if (PetBattleTeam* enemyTeam = m_petBattle->Teams[!TeamIndex])
                for (BattlePet* battlePet : enemyTeam->BattlePets)
                {
                    if (battlePet->GetGlobalIndex() == enemyTeam->GetActivePet()->GetGlobalIndex())
                        continue;

                    if (++Pos == targetID)
                    {
                        m_target = battlePet;
                        break;
                    }
                }

            break;
        }
        case PET_BATTLE_TARGET_ALLY_TEAM:
            if (PetBattleTeam* allyTeam = m_petBattle->Teams[TeamIndex])
                m_target = allyTeam->GetActivePet();
            m_isTeamAuraEffect = true;
            break;
        case PET_BATTLE_TARGET_ENEMY_TEAM:
            if (PetBattleTeam* enemyTeam = m_petBattle->Teams[!TeamIndex])
                m_target = enemyTeam->GetActivePet();
            m_isTeamAuraEffect = true;
            break;
        case PET_BATTLE_TARGET_AURA_OWNER:
            m_target = auraOwner;
            break;
        case PET_BATTLE_TARGET_AURA_CASTER:
            m_target = auraCaster;
            break;
        default:
            break;
    }
}

// -------------------------------------------------------------------------------

void BattlePetAbilityEffect::CalculateHit(int32 accuracy)
{
    accuracy += m_caster->States[BATTLE_PET_STATE_STAT_ACCURACY];
    accuracy += m_caster->States[BATTLE_PET_STATE_STAT_KHARMA];

    // accuracy is reduced by 2% for every level higher the opponent is
    if (m_target->GetLevel() > m_caster->GetLevel())
        accuracy -= (m_target->GetLevel() - m_caster->GetLevel()) * 2;

    // accuracy is less than 100%, calculate miss chance
    if (accuracy < 100 && !Math::RollUnder(accuracy))
        m_flags |= PET_BATTLE_EFFECT_FLAG_MISS;
}

uint32 BattlePetAbilityEffect::CalculateHeal(uint32 heal)
{
    int32 modPercent = CalculatePct(m_caster->States[BATTLE_PET_STATE_STAT_POWER], 5);

    modPercent += m_caster->States[BATTLE_PET_STATE_MOD_HEALING_DEALT_PCT];
    modPercent += m_target->States[BATTLE_PET_STATE_MOD_HEALING_TAKEN_PCT];

    return heal + CalculatePct(heal, modPercent);
}

uint32 BattlePetAbilityEffect::CalculateDamage(uint32 damage)
{
    int32 modPct = CalculatePct(m_caster->States[BATTLE_PET_STATE_STAT_POWER], 5);

    // calculate family damage modifier
    int32 modPetTypePct = -100;

    GtBattlePetTypeDamageModEntry const* damageModEntry = sGtBattlePetTypeDamageModStore.EvaluateTable(m_abilityFamily, m_target->GetFamily());
    if (!damageModEntry)
        return 0;

    modPetTypePct += damageModEntry->Modifier * 100;

    if (modPetTypePct > 0)
        m_flags |= PET_BATTLE_EFFECT_FLAG_STRONG;
    else if (modPetTypePct < 0)
        m_flags |= PET_BATTLE_EFFECT_FLAG_WEAK;

    // calculate critical chance
    if (Math::RollUnder(m_target->States[BATTLE_PET_STATE_STAT_CRITICAL_CHANCE]))
    {
        modPct += 100;
        m_flags |= PET_BATTLE_EFFECT_FLAG_CRITICAL;
    }

    if (m_caster->States[BATTLE_PET_STATE_MOD_PET_TYPE_ID] == m_caster->GetFamily())
        modPetTypePct += m_caster->States[BATTLE_PET_STATE_MOD_PET_TYPE_DAMAGE_DEALT_PERCENT];

    if (m_target->States[BATTLE_PET_STATE_MOD_PET_TYPE_ID] == m_target->GetFamily())
        modPetTypePct += m_target->States[BATTLE_PET_STATE_MOD_PET_TYPE_DAMAGE_TAKEN_PERCENT];

    damage += m_caster->States[BATTLE_PET_STATE_ADD_FLAT_DAMAGE_DEALT];
    damage += m_target->States[BATTLE_PET_STATE_ADD_FLAT_DAMAGE_TAKEN];

    damage += CalculatePct(damage, modPct);
    damage += CalculatePct(damage, m_caster->States[BATTLE_PET_STATE_MOD_DAMAGE_DEALT_PCT]);
    damage += CalculatePct(damage, m_target->States[BATTLE_PET_STATE_MOD_DAMAGE_TAKEN_PCT]);
    damage += CalculatePct(damage, modPetTypePct);

    // Aquatic passive aura
    if (m_isPeriodic && m_target->States[BATTLE_PET_STATE_PASSIVE_AQUATIC])
        damage /= 2;

    // Magic passive aura
    if (m_target->States[BATTLE_PET_STATE_PASSIVE_MAGIC])
        if (damage * 100 / m_target->GetMaxHealth() >= 35)
            damage = CalculatePct(m_target->GetMaxHealth(), 35);

    return damage;
}

// -------------------------------------------------------------------------------

void BattlePetAbilityEffect::Damage(BattlePet* target, uint32 damage)
{
    if (!m_caster->IsAlive() || !target->IsAlive())
        return;

    // handle block
    if (m_target->States[BATTLE_PET_STATE_SPECIAL_BLOCKED_ATTACK_COUNT] > 0)
    {
        bool BlockOnlyEnemyCasts = m_caster->GetTeamIndex() != m_target->GetTeamIndex();

        if (BlockOnlyEnemyCasts || m_target->States[BATTLE_PET_STATE_SPECIAL_BLOCKED_FRIENDLY_MODE] > 0)
        {
            m_flags |= PET_BATTLE_EFFECT_FLAG_BLOCKED;
            --m_target->States[BATTLE_PET_STATE_SPECIAL_BLOCKED_ATTACK_COUNT];

            if (!m_target->States[BATTLE_PET_STATE_SPECIAL_BLOCKED_ATTACK_COUNT])
                if (PetBattleAura* aura = m_target->GetAura(m_target->States[BATTLE_PET_STATE_SPECIAL_OBJECT_REDIRECTION_AURA_ID]))
                {
                    aura->Expire();
                    m_target->States[BATTLE_PET_STATE_SPECIAL_OBJECT_REDIRECTION_AURA_ID] = 0;
                }
        }
    }

    // handle dodge
    if (m_target->States[BATTLE_PET_STATE_STAT_DODGE] && Math::RollUnder(m_target->States[BATTLE_PET_STATE_STAT_DODGE]))
        m_flags |= PET_BATTLE_EFFECT_FLAG_DODGE;

    if ((m_flags & FailFlags) != 0)
        damage = 0;

    // update target health and notify client
    uint32 health = damage > target->GetCurrentHealth() ? 0 : target->GetCurrentHealth() - damage;
    SetHealth(target, health);

    if (damage)
    {
        // update target states
        m_target->States[BATTLE_PET_STATE_CONDITION_WAS_DAMAGED_THIS_ROUND] = 1;
        m_target->States[BATTLE_PET_STATE_LAST_HIT_TAKEN] = damage;

        // update caster states
        m_caster->States[BATTLE_PET_STATE_CONDITION_DID_DAMAGE_THIS_ROUND] = 1;
        m_caster->States[BATTLE_PET_STATE_LAST_HIT_DEALT] = damage;
        
        // target proc casts
        if (PetBattleTeam* targetTeam = m_petBattle->Teams[m_target->GetTeamIndex()])
            if (BattlePet* targetActivePet = targetTeam->GetActivePet())
                if (target->GetGlobalIndex() == targetActivePet->GetGlobalIndex())
                    m_petBattle->Cast(m_target, targetTeam->ActiveAbility.AbilityId, 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_DAMAGE_TAKEN);

        for (PetBattleAura* aura : m_target->Auras)
            m_petBattle->Cast(m_target, aura->GetAbility(), 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_DAMAGE_TAKEN);

        // caste proc casts
        if (PetBattleTeam* casterTeam = m_petBattle->Teams[m_caster->GetTeamIndex()])
            if (BattlePet* casterActivePet = casterTeam->GetActivePet())
                if (m_caster->GetGlobalIndex() == casterActivePet->GetGlobalIndex())
                    m_petBattle->Cast(m_caster, casterTeam->ActiveAbility.AbilityId, 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_DAMAGE_DEALT);

        for (PetBattleAura* aura : m_caster->Auras)
            m_petBattle->Cast(m_caster, aura->GetAbility(), 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_DAMAGE_DEALT);
    }
}

void BattlePetAbilityEffect::Heal(BattlePet* target, uint32 heal)
{
    if (!m_caster->IsAlive() || !target->IsAlive())
        return;

    if ((m_flags & FailFlags) != 0)
        heal = 0;

    // make sure heal doesn't exceed targets max health
    uint32 health = target->GetCurrentHealth();
    uint32 maxHealth = target->GetMaxHealth();

    if (target->GetCurrentHealth() + heal > target->GetMaxHealth())
        heal = target->GetMaxHealth() - target->GetCurrentHealth();

    if (heal)
        m_flags |= PET_BATTLE_EFFECT_FLAG_HEAL;

    // update target health and notify client
    health += heal;
    SetHealth(target, health);
}

// -------------------------------------------------------------------------------

void BattlePetAbilityEffect::SetHealth(BattlePet* target, uint32 value)
{
    if (value > target->GetMaxHealth())
        value = target->GetMaxHealth();

    if ((m_flags & FailFlags) == 0 && value == target->GetCurrentHealth())
        m_flags |= PET_BATTLE_EFFECT_FLAG_MISS;

    if ((m_flags & FailFlags) == 0)
    {
        target->SetCurrentHealth(value);
        
        if (target->IsAlive())
        {
            // target battle pet has died
            if (!value)
            {
                if (!target->IsUnkillable())
                    m_flags |= PET_BATTLE_EFFECT_FLAG_KILL;
                else
                    m_flags |= PET_BATTLE_EFFECT_FLAG_UNKILLABLE;
            }
            else
            {
                // Beast and Flying passibe auras
                if (target->GetFamily() == BATTLE_PET_FAMILY_BEAST || target->GetFamily() == BATTLE_PET_FAMILY_FLYING)
                {
                    bool SetState = false;
                    bool InverseResult = target->GetFamily() == BATTLE_PET_FAMILY_FLYING;

                    uint32 health = CalculatePct(target->GetMaxHealth(), 50);
                    SetState = InverseResult ? target->GetCurrentHealth() > health : target->GetCurrentHealth() < health;

                    target->SetPassiveStateValue(SetState);
                }

                // Dragonkin passive aura
                if (m_caster->GetFamily() == BATTLE_PET_FAMILY_DRAGONKIN && target->GetGlobalIndex() != m_caster->GetGlobalIndex())
                {
                    bool SetState = false;

                    if (!m_caster->GetPassiveStateValue())
                    {
                        uint32 health = CalculatePct(target->GetMaxHealth(), 50);
                        SetState = target->GetCurrentHealth() < health;
                    }

                    m_caster->SetPassiveStateValue(SetState);
                }
            }
        }
    }

    // notify client of health change
    PetBattleEffect effect(PET_BATTLE_EFFECT_SET_HEALTH, m_caster->GetGlobalIndex(), m_flags, m_effectEntry->Id, m_petBattle->TurnInstance++, 0, 1);
    effect.TargetUpdateHealth(target->GetGlobalIndex(), value);

    m_petBattle->Effects.push_back(effect);

    if ((m_flags & PET_BATTLE_EFFECT_FLAG_KILL) != 0)
        m_petBattle->Kill(m_caster, target, m_effectEntry->Id, m_flags);

    // Undead/Mechanical passive auras
    if ((target->States[BATTLE_PET_STATE_PASSIVE_UNDEAD] || target->States[BATTLE_PET_STATE_PASSIVE_MECHANICAL]) && !target->IsAlive())
    {
        uint32 auraID = BATTLE_PET_SPELL_DAMNED;
        if (target->States[BATTLE_PET_STATE_PASSIVE_MECHANICAL])
            auraID = BATTLE_PET_SPELL_FAILSAFE;

        m_petBattle->Cast(target, auraID, 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_NONE);
    }
}

// -------------------------------------------------------------------------------

// Effect 23: Points, Accuracy
void BattlePetAbilityEffect::HandleHeal()
{
    CalculateHit(m_effectEntry->Properties[1]);
    Heal(m_target, CalculateHeal(m_effectEntry->Properties[0]));
}

// Effect 24: Points, Accuracy, IsPeriodic, OverideIndex
void BattlePetAbilityEffect::HandleDamage()
{
    m_isPeriodic = m_effectEntry->Properties[2];

    CalculateHit(m_effectEntry->Properties[1]);

    Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));
}

// Effect 25: BaseChangeToSucceed, IncreasePerToss
void BattlePetAbilityEffect::HandleCatch()
{
    // calculate catch chance
    uint32 chance = m_effectEntry->Properties[0] + (m_target->States[BATTLE_PET_STATE_INTERNAL_CAPTURE_BOOST] * m_effectEntry->Properties[1]);
    bool caught = Math::RollUnder(chance);

    // notify client of catch
    PetBattleEffect effect(PET_BATTLE_EFFECT_CATCH, m_caster->GetGlobalIndex(), caught ? 0 : PET_BATTLE_EFFECT_FLAG_MISS, m_effectEntry->Id, m_petBattle->TurnInstance++, 0, 1);
    effect.TargetUpdateStat(m_target->GetGlobalIndex(), caught ? 1 : 0);

    m_petBattle->Effects.push_back(effect);

    if (caught)
        m_petBattle->Catch(m_caster, m_target, m_effectEntry->Id);
    else
        m_target->States[BATTLE_PET_STATE_INTERNAL_CAPTURE_BOOST]++;
}

// Effect 26: Unused, Accuracy, Duration, Unused
void BattlePetAbilityEffect::HandlePositiveAura()
{
    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, 1);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, 1);
}

// Effect 27: Points, Accuracy, PointsIncreasePerUse, PointsMax, StateToTriggerMaxPoints
void BattlePetAbilityEffect::HandleRampingDamage()
{
    CalculateHit(m_effectEntry->Properties[1]);

    PetBattleTeam* opponentTeam = m_petBattle->Teams[m_target->GetTeamIndex()];
    if (!opponentTeam)
        return;

    int32 petIndex = opponentTeam->ConvertToLocalIndex(m_target->GetGlobalIndex());

    // initial setup for ramping ability
    if (m_caster->States[BATTLE_PET_STATE_RAMPING_DAMAGE_ID] != m_abilityId || m_caster->States[BATTLE_PET_STATE_RAMPING_PBOID] != petIndex)
    {
        m_petBattle->UpdatePetState(m_caster, m_caster, m_effectEntry->Id, BATTLE_PET_STATE_RAMPING_DAMAGE_ID, m_abilityId);
        m_petBattle->UpdatePetState(m_caster, m_caster, m_effectEntry->Id, BATTLE_PET_STATE_RAMPING_DAMAGE_USES, 0);
        m_petBattle->UpdatePetState(m_caster, m_caster, m_effectEntry->Id, BATTLE_PET_STATE_RAMPING_PBOID, petIndex);
    }

    if (m_caster->States[BATTLE_PET_STATE_RAMPING_DAMAGE_USES] < m_effectEntry->Properties[3])
    {
        int32 newValue = m_caster->States[BATTLE_PET_STATE_RAMPING_DAMAGE_USES] + 1;
        m_petBattle->UpdatePetState(m_caster, m_caster, m_effectEntry->Id, BATTLE_PET_STATE_RAMPING_DAMAGE_USES, newValue);
    }

    uint32 damage = CalculateDamage(m_effectEntry->Properties[0] + m_effectEntry->Properties[2] * m_caster->States[BATTLE_PET_STATE_RAMPING_DAMAGE_USES]);
    Damage(m_target, damage);
}

// Effect 29: Points, Accuracy, RequiredCasterState, RequiredTargetState, IsPeriodic
void BattlePetAbilityEffect::HandleDamageOnState()
{
    int32 CasterRequiredState = m_effectEntry->Properties[2];
    int32 TargetRequiredState = m_effectEntry->Properties[3];

    if (CasterRequiredState && !m_caster->States[CasterRequiredState])
        return;

    if (TargetRequiredState && !m_target->States[TargetRequiredState])
        return;

    m_isPeriodic = m_effectEntry->Properties[4];

    CalculateHit(m_effectEntry->Properties[1]);

    uint32 damage = CalculateDamage(m_effectEntry->Properties[0]);
    Damage(m_target, damage);
}

// Effect 31: State,  StateValue
void BattlePetAbilityEffect::HandleSetState()
{
    m_petBattle->UpdatePetState(m_caster, m_target, m_effectEntry->Id, m_effectEntry->Properties[0], m_effectEntry->Properties[1]);
};

// Effect 32: Points, Accuracy, ChainFailure
void BattlePetAbilityEffect::HandleHealPctDealt()
{
    if (m_effectEntry->Properties[2] && m_chainFailure)
        return;

    CalculateHit(m_effectEntry->Properties[1]);
    Heal(m_target, CalculateHeal(CalculatePct(m_target->States[BATTLE_PET_STATE_LAST_HIT_DEALT], m_effectEntry->Properties[0])));
};

// Effect 44: Points, Accuracy
void BattlePetAbilityEffect::HandleHealPctTaken()
{
    CalculateHit(m_effectEntry->Properties[1]);
    Heal(m_target, CalculateHeal(CalculatePct(m_target->States[BATTLE_PET_STATE_LAST_HIT_TAKEN], m_effectEntry->Properties[0])));
}

// Effect 49: Chance, Unused
void BattlePetAbilityEffect::HandleRemoveAura()
{
    CalculateHit(m_effectEntry->Properties[0]);

    if ((m_flags & FailFlags) != 0)
        return;

    if (m_isTeamAuraEffect)
    {
        if (PetBattleTeam* team = m_petBattle->Teams[m_caster->GetTeamIndex()])
            if (PetBattleTeamAura* aura = team->GetTeamAura(m_effectEntry->TriggerAbility))
                aura->Expire();
    }
    else
    {
        if (PetBattleAura* aura = m_caster->GetAura(m_effectEntry->TriggerAbility))
            aura->Expire();
    }
}

// Effect 50: ChainFailure, Accuracy, Duration, MaxAllowed, CasterState, TargetState
void BattlePetAbilityEffect::HandleNegativeAura()
{
    // TODO: handle ChainFailure, CasterState, TargetState

    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, m_effectEntry->Properties[3]);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, m_effectEntry->Properties[3]);
}

// Effect 52: ChainFailure, Accuracy, Duration
void BattlePetAbilityEffect::HandleAddExtraAura()
{
    if (m_effectEntry->Properties[0] && m_chainFailure)
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
}

// Effect 53: Percentage, Accuracy
void BattlePetAbilityEffect::HandleHealPct()
{
    CalculateHit(m_effectEntry->Properties[1]);

    int32 heal = CalculatePct(m_target->GetMaxHealth(), m_effectEntry->Properties[0]);
    Heal(m_target, CalculateHeal(heal));
}

// Effect 54: ChainFailure, Accuracy, Duration, MaxAllowed
void BattlePetAbilityEffect::HandlePeriodicTrigger()
{
    if (m_effectEntry->Properties[0] && m_chainFailure)
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, m_effectEntry->Properties[3]);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, m_effectEntry->Properties[3]);
}

// Effect 59: Points, Accuracy, IsPeriodic
void BattlePetAbilityEffect::HandleDamageOnLowerHealth()
{
    int32 CasterHealth = m_caster->GetCurrentHealth();
    int32 TargetHealth = m_target->GetCurrentHealth();

    if (CasterHealth >= TargetHealth)
        return;

    m_isPeriodic = m_effectEntry->Properties[2];

    CalculateHit(m_effectEntry->Properties[1]);

    Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));
}

// Effect 61: Points, Accuracy, RequiredCasterState, RequiredTargetState
void BattlePetAbilityEffect::HandleConsumeCorpse()
{
    int32 CasterRequiredState = m_effectEntry->Properties[2];
    int32 TargetRequiredState = m_effectEntry->Properties[3];

    if (CasterRequiredState && !m_caster->States[CasterRequiredState])
        return;

    BattlePet* target = nullptr;

    if (PetBattleTeam* team = m_petBattle->Teams[m_caster->GetTeamIndex()])
        for (BattlePet* battlePet : team->BattlePets)
        {
            if (battlePet->GetGlobalIndex() == m_caster->GetGlobalIndex())
                continue;

            if (battlePet->IsAlive())
                continue;

            if (TargetRequiredState && battlePet->States[TargetRequiredState])
                continue;

            target = battlePet;
            break;
        }

    if (!target)
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    if ((m_flags & FailFlags) != 0)
        return;

    target->States[BATTLE_PET_STATE_SPECIAL_CONSUMED_CORPSE] = 1;

    Heal(m_caster, CalculateHeal(CalculatePct(m_caster->GetMaxHealth(), m_effectEntry->Properties[0])));
}

// Effect 62: Points, Accuracy, IsPeriodic
void BattlePetAbilityEffect::HandleDamagePct()
{
    m_isPeriodic = m_effectEntry->Properties[2];

    CalculateHit(m_effectEntry->Properties[1]);

    int32 damage = CalculatePct(m_target->GetMaxHealth(), m_effectEntry->Properties[0]);
    Damage(m_target, CalculateDamage(damage));
}

// Effect 63: Unused, Accuracy, Duration
void BattlePetAbilityEffect::HandlePeriodicPositiveTrigger()
{
    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
}

// Effect 65: Points, Accuracy, BonusPoints, BonusState, IsPeriodic, OverrideIndex
void BattlePetAbilityEffect::HandleDamageWithBonus()
{
    m_isPeriodic = m_effectEntry->Properties[4];

    // TODO: OverrideIndex
    CalculateHit(m_effectEntry->Properties[1]);

    int32 damage = m_effectEntry->Properties[0];

    if (m_target->States[m_effectEntry->Properties[3]])
        damage += m_effectEntry->Properties[2];

    Damage(m_target, CalculateDamage(damage));
}

// Effect 66: Points, Accuracy, Boost
void BattlePetAbilityEffect::HandleDamageOnHealthDeficit()
{
    CalculateHit(m_effectEntry->Properties[1]);

    int32 damage = m_effectEntry->Properties[0];

    if ((m_target->GetCurrentHealth() / m_target->GetMaxHealth()) * 100 <= 25)
        damage += CalculatePct(damage, (m_effectEntry->Properties[2] / 100));

    Damage(m_target, CalculateDamage(damage));
}

// Effect 67: Accuracy: 100
void BattlePetAbilityEffect::HandleEqualizeHealth()
{
    CalculateHit(m_effectEntry->Properties[0]);

    if ((m_flags & FailFlags) != 0)
        return;

    int32 casterHealth = m_caster->GetCurrentHealth();
    int32 targetHealth = m_target->GetCurrentHealth();

    int32 healthDiff = targetHealth - casterHealth;
    healthDiff /= 2;

    int32 casterNewHealth = casterHealth + healthDiff;
    int32 targetNewHealth = targetHealth - healthDiff;

    SetHealth(m_caster, casterNewHealth);
    SetHealth(m_target, targetNewHealth);
}

// Effect 68: Points, Accuracy, IsPeriodic
void BattlePetAbilityEffect::HandleDamageCasterPct()
{
    m_isPeriodic = m_effectEntry->Properties[2];

    CalculateHit(m_effectEntry->Properties[1]);

    int32 damage = CalculatePct(m_caster->GetCurrentHealth(), m_effectEntry->Properties[0]);
    Damage(m_target, CalculateDamage(damage));
}

// Effect 72: Points, Accuracy, Duration 
void BattlePetAbilityEffect::HandleHealToggleAura()
{
    CalculateHit(m_effectEntry->Properties[1]);

    if ((m_flags & FailFlags) != 0)
        return;

    // aura has already been applied, expire and handle heal
    if (m_isTeamAuraEffect)
    {
        if (PetBattleTeam* team = m_petBattle->Teams[m_caster->GetTeamIndex()])
        {
            if (PetBattleTeamAura* aura = team->GetTeamAura(m_effectEntry->TriggerAbility))
            {
                aura->Expire();
                Heal(m_target, CalculateHeal(m_effectEntry->Properties[0]));
            }
            // aura has yet to be applied
            else
                m_petBattle->AddTeamAura(m_caster, m_caster, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
        }
    }
    else
    {
        if (PetBattleAura* aura = m_caster->GetAura(m_effectEntry->TriggerAbility))
        {
            aura->Expire();
            Heal(m_target, CalculateHeal(m_effectEntry->Properties[0]));
        }
        // aura has yet to be applied
        else
            m_petBattle->AddAura(m_caster, m_caster, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
    }
}

// Effect 76: Points, Accuracy
void BattlePetAbilityEffect::HandleDamageToggleAura()
{
    CalculateHit(m_effectEntry->Properties[1]);
    if ((m_flags & FailFlags) != 0)
        return;

    BattlePet* auraTarget = m_caster;
    if (m_effectEntry->EffectProperty == 77)
        auraTarget = m_target;

    // aura has already been applied, expire and handle damage
    if (m_isTeamAuraEffect)
    {
        if (PetBattleTeam* team = m_petBattle->Teams[auraTarget->GetTeamIndex()])
        {
            if (PetBattleTeamAura* aura = team->GetTeamAura(m_effectEntry->TriggerAbility))
            {
                aura->Expire();
                Damage(auraTarget, CalculateDamage(m_effectEntry->Properties[0]));
            }
            // aura has yet to be applied
            else
            {
                m_petBattle->AddTeamAura(m_caster, auraTarget, m_effectEntry->TriggerAbility, m_effectEntry->Id, 1, m_flags);
            }
        }
    }
    else
    {
        if (PetBattleAura* aura = auraTarget->GetAura(m_effectEntry->TriggerAbility))
        {
            aura->Expire();
            Damage(auraTarget, CalculateDamage(m_effectEntry->Properties[0]));
        }
        // aura has yet to be applied
        else
        {
            m_petBattle->AddAura(m_caster, auraTarget, m_effectEntry->TriggerAbility, m_effectEntry->Id, 1, m_flags);
        }
    }
}

// Effect 78: Points, State, MaxPoints, StateToMultiplyAgainst
void BattlePetAbilityEffect::HandlePlanted()
{
    PetBattleAura* aura = m_target->GetAura(m_effectEntry->TriggerAbility);

    // aura has already been applied, expire and handle heal
    if (aura && aura->HasExpired())
    {
        int32 plantedRounds = m_caster->States[m_effectEntry->Properties[3]];
        if (plantedRounds > m_effectEntry->Properties[2])
            plantedRounds = m_effectEntry->Properties[2];

        m_target->States[m_effectEntry->Properties[1]] = 0;

        int32 heal = m_effectEntry->Properties[0] * plantedRounds;
        Heal(m_target, CalculateHeal(heal));
    }
    // aura has yet to be applied
    else
    {
        m_target->States[m_effectEntry->Properties[1]] = 1;
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, 0, m_flags);
    }
}

// Effect 79: State, StateChange, StateMin, StateMax
void BattlePetAbilityEffect::HandleStateChanged()
{
    int32 oldValue = m_caster->States[m_effectEntry->Properties[0]];

    int32 stateMax = m_effectEntry->Properties[3];
    if (stateMax && oldValue >= stateMax)
        return;

    int32 newValue = oldValue + m_effectEntry->Properties[1];
    m_petBattle->UpdatePetState(m_caster, m_target, m_effectEntry->Id, m_effectEntry->Properties[0], newValue);
}

// Effect 80: Points, Accuracy, Duration, ChainFailure
void BattlePetAbilityEffect::HandleWeather()
{
    if (m_effectEntry->Properties[3] && m_chainFailure)
        return;

    int32 duration = m_effectEntry->Properties[2];
    if (!duration && duration != -1)
        duration++;

    PetBattleWeather* weather = m_petBattle->CreateWeather(m_effectEntry->TriggerAbility, m_effectEntry->Id, m_caster, duration);
    if (!weather)
        return;

    for (PetBattleTeam* team : m_petBattle->Teams)
        for (BattlePet* target : team->BattlePets)
        {
            m_flags = 0;

            CalculateHit(m_effectEntry->Properties[1]);

            if ((m_flags & FailFlags) != 0)
                continue;
            
            weather->AddTarget(target);
        }

    weather->OnApply();
}

// Effect 85: State
void BattlePetAbilityEffect::HandleRemoveAurasWithState()
{
    if (m_isTeamAuraEffect)
    {
        if (PetBattleTeam* team = m_petBattle->Teams[m_target->GetTeamIndex()])
        {
            auto itr = team->TeamAurasByState.find(m_effectEntry->Properties[0]);
            if (itr == team->TeamAurasByState.end())
                return;

            for (PetBattleTeamAura* aura : itr->second)
                if (!aura->HasExpired())
                    aura->Expire();
        }
    }
    else
    {
        auto itr = m_target->AurasByState.find(m_effectEntry->Properties[0]);
        if (itr == m_target->AurasByState.end())
            return;

        for (PetBattleAura* aura : itr->second)
            if (!aura->HasExpired())
                aura->Expire();
    }
}

// Effect 86: Points, Accuracy, Duration, AuraID, Duration2
void BattlePetAbilityEffect::HandleGravity()
{
    CalculateHit(m_effectEntry->Properties[1]);

    if ((m_flags & FailFlags) != 0)
        return;

    if (m_isTeamAuraEffect)
    {
        if (PetBattleTeam* team = m_petBattle->Teams[m_caster->GetTeamIndex()])
        {
            if (PetBattleTeamAura* aura = team->GetTeamAura(m_effectEntry->Properties[3]))
            {
                aura->Expire();
                m_petBattle->AddTeamAura(m_caster, m_caster, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
            }
            else
            {
                Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));

                if (!team->GetTeamAura(m_effectEntry->TriggerAbility))
                    m_petBattle->AddTeamAura(m_caster, m_caster, m_effectEntry->Properties[3], m_effectEntry->Id, m_effectEntry->Properties[4], m_flags);
            }
        }
    }
    else
    {
        if (PetBattleAura* aura = m_caster->GetAura(m_effectEntry->Properties[3]))
        {
            aura->Expire();
            m_petBattle->AddAura(m_caster, m_caster, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
        }
        else
        {
            Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));

            if (!m_caster->GetAura(m_effectEntry->TriggerAbility))
                m_petBattle->AddAura(m_caster, m_caster, m_effectEntry->Properties[3], m_effectEntry->Id, m_effectEntry->Properties[4], m_flags);
        }
    }
}

// Effect: 96 Points, Accuracy, CasterState, TargetState, IsPeriodic
void BattlePetAbilityEffect::HandleDamageHitState()
{
    m_isPeriodic = m_effectEntry->Properties[4];

    if ((!m_effectEntry->Properties[2] || !m_caster->States[m_effectEntry->Properties[2]])
        && (!m_effectEntry->Properties[3] || !m_target->States[m_effectEntry->Properties[3]]))
    {
        CalculateHit(m_effectEntry->Properties[1]);
    }

    Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));
}

// Effect 100: Points, Accuracy, RequiredCasterPetType, RequiredTargetPetType
void BattlePetAbilityEffect::HandleHealWithPetType()
{
    int8 RequiredCasterPetType = m_effectEntry->Properties[2];
    int8 RequiredTargetPetType = m_effectEntry->Properties[3];

    if (RequiredCasterPetType && m_caster->GetFamily() != RequiredCasterPetType)
        return;

    if (RequiredTargetPetType && m_target->GetFamily() != RequiredTargetPetType)
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    Heal(m_target, CalculateHeal(m_effectEntry->Properties[0]));
}

// Effect 103: Points, Accuracy
void BattlePetAbilityEffect::HandleExtraAttackFirst()
{
    if (m_petBattle->GetFirstAttackingTeam() != m_caster->GetTeamIndex())
        return;

    CalculateHit(m_effectEntry->Properties[1]);
    Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));
}

// Effect 104: Points, Accuracy, RequiredCasterState, RequiredTargetState
void BattlePetAbilityEffect::HandleHealState()
{
    if (!m_caster->States[m_effectEntry->Properties[2]] && m_effectEntry->Properties[2])
        return;

    if (!m_target->States[m_effectEntry->Properties[3]] && m_effectEntry->Properties[3])
        return;

    CalculateHit(m_effectEntry->Properties[1]);
    Heal(m_caster, CalculateHeal(m_effectEntry->Properties[0]));
}

// Effect 107: Accuracy
void BattlePetAbilityEffect::HandleForceSwapPet()
{
    CalculateHit(m_effectEntry->Properties[0]);
    
    if ((m_flags & FailFlags) != 0)
        return;

    PetBattleTeam* opponentTeam = m_petBattle->Teams[m_target->GetTeamIndex()];
    if (!opponentTeam)
        return;

    BattlePetStore avaliablePets;
    for (BattlePet* battlePet : opponentTeam->BattlePets)
    {
        if (battlePet->GetGUID() == m_target->GetGUID())
            continue;

        if (!battlePet->IsAlive())
            continue;

        // make sure local pet can be swapped with active
        if (!opponentTeam->CanSwap(battlePet, true))
            continue;

        avaliablePets.push_back(battlePet);
    }

    if (avaliablePets.empty())
        return;

    BattlePet* battlePetTarget = Trinity::Containers::SelectRandomContainerElement(avaliablePets);
    if (!battlePetTarget)
        return;

    if (opponentTeam->CanSwap(battlePetTarget))
        m_petBattle->SwapActivePet(opponentTeam, battlePetTarget);
}

// Effect 111: Percentage, Unused, Unused, Unused
void BattlePetAbilityEffect::HandleResurrect()
{
    if (m_petBattle->ResurrectedBattlePets.find(m_target->GetGUID()) != m_petBattle->ResurrectedBattlePets.end())
        return;

    SetHealth(m_target, CalculatePct(m_target->GetMaxHealth(), m_effectEntry->Properties[0]));
    m_petBattle->Resurrect(m_target, m_effectEntry->Id);
}

// Effect 128: Heal
void BattlePetAbilityEffect::HandleSetHealthPct()
{
    SetHealth(m_target, CalculatePct(m_target->GetMaxHealth(), m_effectEntry->Properties[0]));
}

// Effect 129: LockDuration
void BattlePetAbilityEffect::HandleLockNextAbility()
{
    // notify client of ability change
    PetBattleEffect effect(PET_BATTLE_EFFECT_ABILITY_CHANGE, m_caster->GetGlobalIndex(), 0, m_effectEntry->Id, m_petBattle->TurnInstance++, 0, 1);
    effect.TargetUpdateAbility(m_caster->GetGlobalIndex(), 0, 0, m_effectEntry->Properties[0]);

    m_petBattle->Effects.push_back(effect);
}

// Effect 131: ChainFailure, Accuracy, Duration,
void BattlePetAbilityEffect::HandleInterruptOpponentsRound()
{
    if (m_petBattle->GetFirstAttackingTeam() != m_caster->GetTeamIndex())
        return;

    if (m_effectEntry->Properties[0] && m_chainFailure)
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
}

// Effect 135: Accuracy, CasterImmuneState, TargetImmuneState, enableReverse
void BattlePetAbilityEffect::HandleInstantKill()
{
    CalculateHit(m_effectEntry->Properties[0]);

    if ((m_flags & FailFlags) != 0)
        return;

    int32 CasterImmuneState = m_effectEntry->Properties[1];
    int32 TargetImmuneState = m_effectEntry->Properties[2];

    if (CasterImmuneState && m_caster->States[CasterImmuneState])
        return;

    if (TargetImmuneState && m_target->States[TargetImmuneState])
        return;

    // TODO: enableReverse
    SetHealth(m_target, 0);
}

// Effect 137: CasterState, CasterStateValue, Duration, TargetState, TargetStateValue, ChainFailure
void BattlePetAbilityEffect::HandleDamageTripple()
{
    if (m_effectEntry->Properties[5] && m_chainFailure)
        return;

    int32 CasterState = m_effectEntry->Properties[0];
    int32 CasterStateValue = m_effectEntry->Properties[1];

    if (CasterState && m_caster->States[CasterStateValue] != CasterStateValue)
        return;

    int32 TargetState = m_effectEntry->Properties[3];
    int32 TargetStateValue = m_effectEntry->Properties[4];

    if (TargetState && m_target->States[TargetState] != TargetStateValue)
        return;

    m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
}

// Effect 138: State, StateChange, StateMin, StateMax, TargetTestState, TargetTestStateValue
void BattlePetAbilityEffect::HandleStateChangedOnState()
{
    if (m_target->States[m_effectEntry->Properties[4]] != m_effectEntry->Properties[4])
        return;

    int32 oldValue = m_caster->States[m_effectEntry->Properties[0]];

    int32 stateMax = m_effectEntry->Properties[3];
    if (stateMax && oldValue >= stateMax)
        return;

    int32 newValue = oldValue + m_effectEntry->Properties[1];
    m_petBattle->UpdatePetState(m_caster, m_target, m_effectEntry->Id, m_effectEntry->Properties[0], newValue);
}

// Effect 139: Accuracy
void BattlePetAbilityEffect::HandleForceSwapPetWithLowestHP()
{
    CalculateHit(m_effectEntry->Properties[0]);

    if ((m_flags & FailFlags) != 0)
        return;

    PetBattleTeam* opponentTeam = m_petBattle->Teams[m_target->GetTeamIndex()];
    if (!opponentTeam)
        return;

    BattlePet* battlePetTarget = nullptr;
    for (BattlePet* battlePet : opponentTeam->BattlePets)
    {
        if (battlePet->GetGUID() == m_target->GetGUID())
            continue;

        if (!battlePet->IsAlive())
            continue;

        // make sure local pet can be swapped with active
        if (!opponentTeam->CanSwap(battlePet, true))
            continue;

        if (!battlePetTarget || battlePetTarget->GetCurrentHealth() < battlePet->GetCurrentHealth())
            battlePetTarget = battlePet;
    }

    if (!battlePetTarget)
        return;

    if (opponentTeam->CanSwap(battlePetTarget))
        m_petBattle->SwapActivePet(opponentTeam, battlePetTarget);
}

// Effect 140: NewDuration
void BattlePetAbilityEffect::HandleRefreshAura()
{
    if (m_isTeamAuraEffect)
    {
        if (PetBattleTeam* team = m_petBattle->Teams[m_target->GetTeamIndex()])
            if (PetBattleTeamAura* aura = team->GetTeamAura(m_effectEntry->TriggerAbility))
                if (!aura->HasExpired())
                    aura->SetMaxDuration(m_effectEntry->Properties[0]);
    }
    else
    {
        if (PetBattleAura* aura = m_target->GetAura(m_effectEntry->TriggerAbility))
            if (!aura->HasExpired())
                aura->SetMaxDuration(m_effectEntry->Properties[0]);
    }
}

// Effect 141: Points: 40 Accuracy: 100
void BattlePetAbilityEffect::HandleSplitDamage()
{
    PetBattleTeam* opponentTeam = m_petBattle->Teams[m_target->GetTeamIndex()];
    if (!opponentTeam)
        return;

    CalculateHit(m_effectEntry->Properties[0]);

    int32 damage = CalculateDamage(m_effectEntry->Properties[0]) / opponentTeam->BattlePets.size();
    Damage(m_target, damage);
}

// Effect 145: Accuracy
void BattlePetAbilityEffect::HandleForceSwapPetWithHighestHP()
{
    CalculateHit(m_effectEntry->Properties[0]);

    if ((m_flags & FailFlags) != 0)
        return;

    PetBattleTeam* opponentTeam = m_petBattle->Teams[m_target->GetTeamIndex()];
    if (!opponentTeam)
        return;

    BattlePet* battlePetTarget = nullptr;
    for (BattlePet* battlePet : opponentTeam->BattlePets)
    {
        if (battlePet->GetGUID() == m_target->GetGUID())
            continue;

        if (!battlePet->IsAlive())
            continue;

        // make sure local pet can be swapped with active
        if (!opponentTeam->CanSwap(battlePet, true))
            continue;

        if (!battlePetTarget || battlePetTarget->GetCurrentHealth() > battlePet->GetCurrentHealth())
            battlePetTarget = battlePet;
    }

    if (!battlePetTarget)
        return;

    if (opponentTeam->CanSwap(battlePetTarget))
        m_petBattle->SwapActivePet(opponentTeam, battlePetTarget);
}

// Effect 149: Points, Accuracy, IsPeriodic
void BattlePetAbilityEffect::HandleDamageNonLethal()
{
    m_isPeriodic = m_effectEntry->Properties[2];

    CalculateHit(m_effectEntry->Properties[1]);

    // make sure damage doesn't exceed targets health
    uint32 damage = CalculateDamage(m_effectEntry->Properties[0]);
    if (damage >= m_target->GetCurrentHealth())
        damage = m_target->GetCurrentHealth() - 1;

    Damage(m_target, damage);
}

// Effect 150: ChainFailure, Accuracy
void BattlePetAbilityEffect::HandlePositiveAura2()
{
    if (m_chainFailure && m_effectEntry->Properties[1])
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    if (m_isTeamAuraEffect)
        m_petBattle->AddTeamAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, 1);
    else
        m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags, 1);
}

// Effect 160: Points, Accuracy,
void BattlePetAbilityEffect::HandleDamageIfSlower()
{
    if (m_target->GetSpeed() <= m_caster->GetSpeed())
        return;

    CalculateHit(m_effectEntry->Properties[1]);
    Damage(m_target, CalculateDamage(m_effectEntry->Properties[0]));
}

// Effect 170: Points, Accuracy, weatherState, Unused, IsPeriodic
void BattlePetAbilityEffect::HandleDamageOnWeatherState()
{
    int32 weatherState = m_effectEntry->Properties[2];
    if (weatherState && !m_target->States[weatherState])
        return;

    m_isPeriodic = m_effectEntry->Properties[4];

    CalculateHit(m_effectEntry->Properties[1]);

    uint32 damage = CalculateDamage(m_effectEntry->Properties[0]);
    Damage(m_target, damage);
}

// Effect 171: Points, Accuracy, RequiredCasterState, RequiredTargetState
void BattlePetAbilityEffect::HandleHealOnWeatherState()
{
    int32 CasterRequiredWeatherState = m_effectEntry->Properties[2];
    int32 TargetRequiredWeatherState = m_effectEntry->Properties[3];

    if (CasterRequiredWeatherState && !m_caster->States[CasterRequiredWeatherState])
        return;

    if (TargetRequiredWeatherState && !m_target->States[TargetRequiredWeatherState])
        return;

    CalculateHit(m_effectEntry->Properties[1]);

    uint32 heal = CalculateHeal(m_effectEntry->Properties[0]);
    Heal(m_target, heal);
}

// Effect 172: StatePoints, CasterState, Duration, TargetState, ChainFailure, Accuracy
void BattlePetAbilityEffect::HandleAuraWithAccuracyMod()
{
    if (m_chainFailure && m_effectEntry->Properties[4])
        return;

    if ((!m_effectEntry->Properties[1] || !m_caster->States[m_effectEntry->Properties[1]])
        && (!m_effectEntry->Properties[3] || !m_target->States[m_effectEntry->Properties[3]]))
    {
        CalculateHit(m_effectEntry->Properties[5]);
    }

    m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
}

// Effect 178: StatePoints, Accuracy, Duration, TargetState, ChainFailure, ReportFailAsImmune
void BattlePetAbilityEffect::HandlePowerlessAura()
{
    if (m_effectEntry->Properties[5])
        m_reportFailAsImmune = true;

    // make sure target doesn't already have state value
    if (m_effectEntry->Properties[3] && m_target->States[m_effectEntry->Properties[3]] == m_effectEntry->Properties[0])
    {
        if (m_reportFailAsImmune)
            m_flags |= PET_BATTLE_EFFECT_FLAG_IMMUNE;
        else
            return;
    }

    // Critters Passive Aura
    if (m_target->States[BATTLE_PET_STATE_PASSIVE_CRITTER])
    {
        if (m_reportFailAsImmune)
            m_flags |= PET_BATTLE_EFFECT_FLAG_IMMUNE;
        else
            return;
    }

    CalculateHit(m_effectEntry->Properties[1]);

    m_petBattle->AddAura(m_caster, m_target, m_effectEntry->TriggerAbility, m_effectEntry->Id, m_effectEntry->Properties[2], m_flags);
}

// Effect 197: Power, Accuracy
void BattlePetAbilityEffect::HandleDamageLastTaken()
{
    CalculateHit(m_effectEntry->Properties[1]);

    int32 damage = m_target->States[BATTLE_PET_STATE_LAST_HIT_TAKEN] * (m_effectEntry->Properties[0] / 100);
    Damage(m_target, CalculateDamage(damage));
}

// Effect 204: Points, Accuracy, IsPeriodic, OverrideIndex
void BattlePetAbilityEffect::HandleDamageReflected()
{
    m_isPeriodic = m_effectEntry->Properties[2];

    CalculateHit(m_effectEntry->Properties[1]);

    m_flags |= PET_BATTLE_EFFECT_FLAG_REFLECT;

    int32 damage = m_caster->States[BATTLE_PET_STATE_LAST_HIT_TAKEN] * (m_effectEntry->Properties[0] / 100);
    Damage(m_target, CalculateDamage(damage));
}
