/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AchievementMgr.h"
#include "BattlePet.h"
#include "BattlePetAbilityEffect.h"
#include "BattlePetSpawnMgr.h"
#include "DBCStores.h"
#include "DB2Enums.h"
#include "DB2Stores.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "SharedDefines.h"
#include "Util.h"
#include "WorldSession.h"
#include "Scenario.h"

#include <algorithm>

// -------------------------------------------------------------------------------
// Battle Pet
// -------------------------------------------------------------------------------

void BattlePet::Initialize(uint8 level, int32 health)
{
    // existence is checked before this, no problem should arise
    m_npc = sBattlePetSpeciesStore.LookupEntry(m_species)->NpcId;

    uint8 gender = GENDER_MALE;
    uint32 displayID = ObjectMgr::ChooseDisplayId(sObjectMgr->GetCreatureTemplate(m_npc));
    if (CreatureModelInfo const* minfo = sObjectMgr->GetCreatureModelRandomGender(&displayID))
    {
        m_displayId = displayID;
        gender = minfo->gender;
        if (minfo->gender == GENDER_FEMALE)
            m_breed += 10;
    }

    // setup initial battle pet states
    InitializeStates(level, health, gender);
}

void BattlePet::InitializeStates(uint8 level, int32 health, uint8 gender)
{
    // setup initial states
    States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL] = level;
    States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH] = health;

    States[BATTLE_PET_STATE_STAT_GENDER] = gender;

    States[BATTLE_PET_STATE_STAT_CRITICAL_CHANCE] = 5;
    States[BATTLE_PET_STATE_MOD_PET_TYPE_ID] = m_family;

    // set breed specific states
    for (BattlePetBreedStateEntry const* breedStateEntry : sBattlePetBreedStateStore)
    {
        if (breedStateEntry->BreedId != m_breed)
            continue;

        // handle main stat states
        if (IsMainStatState(breedStateEntry->StateId))
            m_mainStates[breedStateEntry->StateId - BATTLE_PET_STATE_STAT_POWER] += breedStateEntry->Modifier;
        // other states
        else
            States[breedStateEntry->StateId] += breedStateEntry->Modifier;
    }

    // set species specific states
    for (BattlePetSpeciesStateEntry const* speciesStateEntry : sBattlePetSpeciesStateStore)
    {
        if (speciesStateEntry->SpeciesId != m_species)
            continue;

        // handle main stat states
        if (IsMainStatState(speciesStateEntry->StateId))
            m_mainStates[speciesStateEntry->StateId - BATTLE_PET_STATE_STAT_POWER] += speciesStateEntry->Modifier;
        else
        // other states
            States[speciesStateEntry->StateId] += speciesStateEntry->Modifier;
    }

    // apply quality modifier
    for (uint8 i = 0; i < BATTLE_PET_MAX_MAIN_STATES; i++)
    {
        BattlePetBreedQualityEntry const* breedQualityEntry = sBattlePetBreedQualityStore.LookupEntry(7 + m_quality);
        ASSERT(breedQualityEntry);

        m_mainStates[i] *= breedQualityEntry->Multiplier;
    }

    // calculate main stats
    CalculateStats(!health);

    if (!States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH])
        States[BATTLE_PET_STATE_IS_DEAD] = 1;
}

void BattlePet::InitializeAbilities(bool wild)
{
    // calculate abilites for the battle pet
    for (BattlePetSpeciesXAbilityEntry const* abilityEntry : sBattlePetSpeciesXAbilityStore)
    {
        if (abilityEntry->SpeciesId != m_species)
            continue;

        if (abilityEntry->RequiredLevel > uint8(States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL]))
            continue;

        // first ability in the tier
        if (abilityEntry->RequiredLevel < 5)
            AbilitiyIds[abilityEntry->SlotId] = abilityEntry->AbilityId;
        // second ability in the tier
        else
        {
            // wild battle pets have their second ability decided by chance
            if (wild && Math::Rand(0, 1))
                AbilitiyIds[abilityEntry->SlotId] = abilityEntry->AbilityId;
            // player battle pets have their second ability decided by journal flags
            else if (!wild)
            {
                switch (abilityEntry->SlotId)
                {
                    case 0:
                        if (HasFlag(BATTLE_PET_JOURNAL_FLAG_ABILITY_1))
                            AbilitiyIds[abilityEntry->SlotId] = abilityEntry->AbilityId;
                        break;
                    case 1:
                        if (HasFlag(BATTLE_PET_JOURNAL_FLAG_ABILITY_2))
                            AbilitiyIds[abilityEntry->SlotId] = abilityEntry->AbilityId;
                        break;
                    case 2:
                        if (HasFlag(BATTLE_PET_JOURNAL_FLAG_ABILITY_3))
                            AbilitiyIds[abilityEntry->SlotId] = abilityEntry->AbilityId;
                        break;
                }
            }
        }
    }
}

void BattlePet::SetBattleAbilities()
{
    // initialise abilities for battle
    for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
    {
        if (!AbilitiyIds[i])
        {
            Abilities[i] = nullptr;
            continue;
        }

        BattlePetAbility* battlePetAbility = new BattlePetAbility();
        battlePetAbility->AbilityId  = AbilitiyIds[i];
        battlePetAbility->OnCooldown = false;
        battlePetAbility->Cooldown   = 0;
        battlePetAbility->Lockdown   = 0;

        Abilities[i] = battlePetAbility;
    }
}

void BattlePet::CalculateStats(bool currentHealth)
{
    // initial values
    for (uint8 i = 0; i < BATTLE_PET_MAX_MAIN_STATES; i++)
        States[BATTLE_PET_STATE_STAT_POWER + i] = m_mainStates[i];

    // apply level modifier
    States[BATTLE_PET_STATE_STAT_POWER] *= States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL];
    States[BATTLE_PET_STATE_STAT_SPEED] *= States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL];

    // calculate health
    uint32 m_maxHealth = (10000 + (States[BATTLE_PET_STATE_STAT_STAMINA] * 5) * States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL]) / 100;

    States[BATTLE_PET_STATE_STAT_STAMINA] = m_maxHealth;

    if (currentHealth)
        States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH] = m_maxHealth;

    // flag battle pet for save
    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

void BattlePet::SetFlag(uint32 flag)
{
    if (HasFlag(flag))
        return;

    m_flags |= flag;
    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

void BattlePet::UnSetFlag(uint32 flag)
{
    if (!HasFlag(flag))
        return;

    m_flags &= ~flag;
    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

void BattlePet::SetCurrentHealth(uint32 health)
{
    if (health > GetMaxHealth())
        health = GetMaxHealth();

    States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH] = health;
    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

void BattlePet::SetNickname(std::string nickname)
{
    m_nickname = nickname;
    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

void BattlePet::SetXP(uint32 xpGain)
{
    if (!xpGain)
        return;

    if (States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL] == BATTLE_PET_MAX_LEVEL)
        return;

    uint8 level = States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL];

    if (level >= sGtBattlePetXpStore.GetTableRowCount())
        level = sGtBattlePetXpStore.GetTableRowCount() - 1;

    GtBattlePetXpEntry const* bpx = sGtBattlePetXpStore.EvaluateTable(level - 1, 1);
    if (!bpx)
        return;

    uint16 baseValue = bpx->Value;

    bpx = sGtBattlePetXpStore.EvaluateTable(level - 1, 0);
    if (!bpx)
        return;

    uint16 multiplier = bpx->Value;

    uint16 maxXpForLevel = baseValue * multiplier;

    // battle pet has leveled
    if (m_xp + xpGain >= maxXpForLevel)
    {
        m_xp = (m_xp + xpGain) - maxXpForLevel;
        SetLevel(States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL] + 1);
    }
    else
        m_xp += xpGain;

    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

void BattlePet::SetLevel(uint8 level)
{
    // make sure level is valid
    if (!level || level > BATTLE_PET_MAX_LEVEL || level == States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL])
        return;

    if (States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL] == BATTLE_PET_MAX_LEVEL || level < States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL])
        m_xp = 0;

    States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL] = level;

    m_owner->UpdateCriteria(CRITERIA_TYPE_LEVEL_BATTLE_PET, GetSpecies(), level);

    CalculateStats(true);
    InitializeAbilities(false);

    // update world object if battle pet is currently summoned
    if (m_owner->GetBattlePetMgr()->GetCurrentSummonGUID() == m_guid)
    {
        if (TempSummon* worldPet = m_owner->GetBattlePetMgr()->GetCurrentSummon())
        {
            worldPet->SetUInt32Value(UNIT_WILD_BATTLE_PET_LEVEL, GetLevel());
            worldPet->SetMaxHealth(GetMaxHealth());
            worldPet->SetHealth(GetCurrentHealth());
        }
    }
}

void BattlePet::SetQuality(uint8 quality)
{
    m_quality = quality;

    m_dbState = BATTLE_PET_DB_STATE_SAVE;
}

uint8 BattlePet::GetFamily() const
{
    if (States[BATTLE_PET_STATE_SPECIAL_TYPE_OVERRIDE] > 0)
        return States[BATTLE_PET_STATE_SPECIAL_TYPE_OVERRIDE];

    return m_family;
}

uint32 BattlePet::GetSpeed() const
{
    int32 modPercent = States[BATTLE_PET_STATE_MOD_SPEED_PERCENT];

    return (States[BATTLE_PET_STATE_STAT_SPEED] + CalculatePct(States[BATTLE_PET_STATE_STAT_SPEED], modPercent)) / 100;
}

uint32 BattlePet::GetPower() const
{
    return (States[BATTLE_PET_STATE_STAT_POWER] + CalculatePct(States[BATTLE_PET_STATE_STAT_POWER], States[BATTLE_PET_STATE_MOD_DAMAGE_DEALT_PCT])) / 100;
}

uint32 BattlePet::GetMaxHealth() const
{
    int32 baseMaxHealth = States[BATTLE_PET_STATE_STAT_STAMINA];
    baseMaxHealth += States[BATTLE_PET_STATE_MAX_HEALTH_BONUS];

    return baseMaxHealth + CalculatePct(baseMaxHealth, States[BATTLE_PET_STATE_MOD_MAX_HEALTH_PERCENT]);
}

void BattlePet::ResetMechanicStates()
{
    States[BATTLE_PET_STATE_MECHANIC_POISONED]  = 0;
    States[BATTLE_PET_STATE_MECHANIC_STUNNED]   = 0;
    States[BATTLE_PET_STATE_MOD_SPEED_PERCENT]  = 0;
    States[BATTLE_PET_STATE_UNTARGETABLE]       = 0;
    States[BATTLE_PET_STATE_MECHANIC_UNDERGROUND]   = 0;
    States[BATTLE_PET_STATE_MECHANIC_FLYING]    = 0;
    States[BATTLE_PET_STATE_MECHANIC_BURNING]   = 0;
    States[BATTLE_PET_STATE_TURN_LOCK]          = 0;
    States[BATTLE_PET_STATE_SWAP_OUT_LOCK]      = 0;
    States[BATTLE_PET_STATE_MECHANIC_CHILLED]   = 0;
    States[BATTLE_PET_STATE_MECHANIC_WEBBED]    = 0;
    States[BATTLE_PET_STATE_MECHANIC_INVISIBLE] = 0;
    States[BATTLE_PET_STATE_UNKILLABLE]         = 0;
    States[BATTLE_PET_STATE_MECHANIC_BLEEDING]  = 0;
    States[BATTLE_PET_STATE_MECHANIC_BLIND]     = 0;
    States[BATTLE_PET_STATE_SWAP_IN_LOCK]       = 0;
    States[BATTLE_PET_STATE_MECHANIC_BOMB]      = 0;
    States[BATTLE_PET_STATE_RESILITANT]         = 0;
}

PetBattleAura* BattlePet::GetAura(uint32 abilityId) const
{
    for (PetBattleAura* aura : Auras)
        if (aura->GetAbility() == abilityId)
            return aura;

    return nullptr;
}

void BattlePet::InitializePassiveStates(PetBattle* battle)
{
    // reset passive auras
    States[battlePetsPassiveInfoData[GetFamily()].State] = 0;

    int8 State = -1;
    int32 AuraID = -1;

    switch (GetFamily())
    {
        case BATTLE_PET_FAMILY_HUMANOID:
        case BATTLE_PET_FAMILY_CRITTER:
        case BATTLE_PET_FAMILY_AQUATIC:
        case BATTLE_PET_FAMILY_ELEMENTAL:
        case BATTLE_PET_FAMILY_MAGIC:
            State = battlePetsPassiveInfoData[GetFamily()].State;
            AuraID = battlePetsPassiveInfoData[GetFamily()].Aura;
            break;
        case BATTLE_PET_FAMILY_UNDEAD:
        case BATTLE_PET_FAMILY_MECHANICAL:
            State = battlePetsPassiveInfoData[GetFamily()].State;
            break;
        case BATTLE_PET_FAMILY_BEAST:
        {
            int32 health = CalculatePct(States[BATTLE_PET_STATE_STAT_STAMINA], 50);
            if (States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH] < health)
            {
                State = battlePetsPassiveInfoData[GetFamily()].State;
                AuraID = battlePetsPassiveInfoData[GetFamily()].Aura;
            }
            break;
        }
        case BATTLE_PET_FAMILY_FLYING:
        {
            int32 health = CalculatePct(States[BATTLE_PET_STATE_STAT_STAMINA], 50);
            if (States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH] > health)
            {
                State = battlePetsPassiveInfoData[GetFamily()].State;
                AuraID = battlePetsPassiveInfoData[GetFamily()].Aura;
            }
            break;
        }
        default:
            break;
    }

    if (State < 0)
        return;

    States[State] = 1;

    if (AuraID > 0)
        battle->AddInitPassiveAura(this, AuraID);
}

void BattlePet::SetPassiveStateValue(bool set)
{
    States[battlePetsPassiveInfoData[GetFamily()].State] = set ? 1 : 0;
}

uint32 BattlePet::GetPassiveState() const
{
    return battlePetsPassiveInfoData[GetFamily()].State;
}

uint32 BattlePet::GetPassiveAuraID() const
{
    return battlePetsPassiveInfoData[GetFamily()].Aura;
}

int32 BattlePet::GetPassiveStateValue() const
{
    return States[battlePetsPassiveInfoData[GetFamily()].State];
}

// -------------------------------------------------------------------------------
// Pet Battle
// -------------------------------------------------------------------------------

void PetBattleAura::OnApply()
{
    // apply any state modifiers for the aura
    for (BattlePetAbilityStateEntry const* stateEntry : sBattlePetAbilityStateStore)
    {
        if (stateEntry->AbilityId != m_ability)
            continue;

        // store state and modifier for removal
        if (m_auraStates.find(stateEntry->StateId) == m_auraStates.end())
            m_auraStates[stateEntry->StateId] = 0;

        int32 newValue = stateEntry->Value + m_target->States[stateEntry->StateId];
        m_auraStates[stateEntry->StateId] = newValue;

        m_target->AurasByState[stateEntry->StateId].push_back(this);

        // update state
        m_petBattle->UpdatePetState(m_caster, m_target, 0, stateEntry->StateId, newValue);
    }
}

void PetBattleAura::OnExpire()
{
    // remove any state modifiers for the aura
    for (auto stateEntry : m_auraStates)
    {
        m_target->AurasByState[stateEntry.first].remove(this);

        int32 newValue = m_target->States[stateEntry.first] - stateEntry.second;

        m_petBattle->UpdatePetState(m_caster, m_target, 0, stateEntry.first, newValue);
    }

    m_auraStates.clear();
}

void PetBattleAura::Process()
{
    // expire aura if it has reached max duration
    if (m_duration != -1 && m_turn > m_maxDuration)
    {
        Expire();
        return;
    }

    // handle aura effects
    if (BattlePetAbilityEntry const* abilityEntry = sBattlePetAbilityStore.LookupEntry(m_ability))
    {
        uint32 turnCount = 0;
        uint32 topMaxTurnId = 0;

        // find longest duration effect
        for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
        {
            if (abilityTurnEntry->AbilityId != m_ability)
                continue;

            turnCount++;
            topMaxTurnId = std::max(topMaxTurnId, abilityTurnEntry->Duration);
        }

        for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
        {
            if (abilityTurnEntry->AbilityId != m_ability)
                continue;

            // make sure ability has reached duration required for effect
            if (abilityTurnEntry->Duration != m_turn && turnCount != 1 && topMaxTurnId != 1)
                continue;

            for (BattlePetAbilityEffectEntry const* abilityEffectEntry : sBattlePetAbilityEffectStore)
            {
                if (abilityEffectEntry->AbilityTurnId != abilityTurnEntry->Id)
                    continue;

                // initialise ability effect
                BattlePetAbilityEffect abilityEffect;

                abilityEffect.SetAbilityInfo(m_ability, abilityEffectEntry, abilityEntry->TypeId);
                abilityEffect.SetCaster(m_target);
                abilityEffect.SetParentBattle(m_petBattle);

                abilityEffect.SetTarget(m_caster, m_target);
                abilityEffect.Execute();
            }
        }
    }

    // notify client of aura update
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_CHANGE, m_caster->GetGlobalIndex());
    effect.TargetUpdateAura(m_target->GetGlobalIndex(), m_id, m_ability, m_duration, m_turn);

    m_petBattle->Effects.push_back(effect);

    m_turn++;

    if (m_duration != -1)
        m_duration--;
}

void PetBattleAura::Expire(bool OnDeath /*=false*/)
{
    if (m_expired || (OnDeath && !m_expireOnDeath))
        return;

    m_expired = true;

    // notify client of aura removal
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_REMOVE, m_caster->GetGlobalIndex(), 0, 0, 1, 0, 1);
    effect.TargetUpdateAura(m_target->GetGlobalIndex(), m_id, m_ability, 0, 0);

    m_petBattle->Effects.push_back(effect);

    // cast any removal procs the ability might have
    m_petBattle->Cast(m_target, m_ability, m_turn, PET_BATTLE_ABILITY_TURN0_PROC_ON_AURA_REMOVED);

    OnExpire();
}

void PetBattleTeamAura::OnApply()
{
    // apply any state modifiers for the aura
    for (BattlePetAbilityStateEntry const* stateEntry : sBattlePetAbilityStateStore)
    {
        if (stateEntry->AbilityId != m_ability)
            continue;

        for (BattlePet* target : m_targets)
        {
            // store state and modifier for removal
            if (m_teamStates[target->GetGlobalIndex()].find(stateEntry->StateId) == m_teamStates[target->GetGlobalIndex()].end())
                m_teamStates[target->GetGlobalIndex()][stateEntry->StateId] = 0;

            int32 newValue = stateEntry->Value + target->States[stateEntry->StateId];
            m_teamStates[target->GetGlobalIndex()][stateEntry->StateId] += newValue;

            // update state
            m_petBattle->UpdatePetState(m_caster, target, 0, stateEntry->StateId, newValue);
        }

        m_team->TeamAurasByState[stateEntry->StateId].push_back(this);
    }
}

void PetBattleTeamAura::OnExpire()
{
    // remove any state modifiers for the aura
    for (BattlePet* target : m_targets)
        for (auto stateEntry : m_teamStates[target->GetGlobalIndex()])
        {
            int32 newValue = target->States[stateEntry.first] - stateEntry.second;
            m_petBattle->UpdatePetState(m_caster, target, 0, stateEntry.first, newValue);

            m_team->TeamAurasByState[stateEntry.first].remove(this);
        }

    m_teamStates.clear();
}

void PetBattleTeamAura::Process()
{
    // expire aura if it has reached max duration
    if (m_duration != -1 && m_turn > m_maxDuration)
    {
        Expire();
        return;
    }

    // handle aura effects
    if (BattlePetAbilityEntry const* abilityEntry = sBattlePetAbilityStore.LookupEntry(m_ability))
    {
        uint32 turnCount = 0;
        uint32 topMaxTurnId = 0;

        // find longest duration effect
        for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
        {
            if (abilityTurnEntry->AbilityId != m_ability)
                continue;

            turnCount++;
            topMaxTurnId = std::max(topMaxTurnId, abilityTurnEntry->Duration);
        }

        for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
        {
            if (abilityTurnEntry->AbilityId != m_ability)
                continue;

            // make sure ability has reached duration required for effect
            if (abilityTurnEntry->Duration != m_turn && turnCount != 1 && topMaxTurnId != 1)
                continue;

            for (BattlePetAbilityEffectEntry const* abilityEffectEntry : sBattlePetAbilityEffectStore)
            {
                if (abilityEffectEntry->AbilityTurnId != abilityTurnEntry->Id)
                    continue;

                // initialise ability effect
                BattlePetAbilityEffect abilityEffect;

                abilityEffect.SetAbilityInfo(m_ability, abilityEffectEntry, abilityEntry->TypeId);
                abilityEffect.SetCaster(m_caster);
                abilityEffect.SetParentBattle(m_petBattle);

                abilityEffect.SetTarget(m_caster, nullptr, m_team->GetTeamIndex());
                abilityEffect.Execute();
            }
        }
    }

    // notify client of aura update
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_CHANGE, m_caster->GetGlobalIndex());
    effect.TargetUpdateAura(m_team->GetGlobalIndex(), m_id, m_ability, m_duration, m_turn);

    m_petBattle->Effects.push_back(effect);

    m_turn++;

    if (m_duration != -1)
        m_duration--;
}

void PetBattleTeamAura::Expire()
{
    if (m_expired)
        return;

    m_expired = true;

    // notify client of aura removal
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_REMOVE, m_caster->GetGlobalIndex(), 0, 0, 1, 0, 1);
    effect.TargetUpdateAura(m_team->GetGlobalIndex(), m_id, m_ability, 0, 0);

    m_petBattle->Effects.push_back(effect);

    // cast any removal procs the ability might have
    m_petBattle->Cast(m_team->GetActivePet(), m_ability, m_turn, PET_BATTLE_ABILITY_TURN0_PROC_ON_AURA_REMOVED);

    OnExpire();
}

void PetBattleTeamAura::AddTarget(BattlePet* pet)
{
    m_targets.push_back(pet);
}

void PetBattleWeather::OnApply()
{
    // apply any state modifiers for the aura
    for (BattlePetAbilityStateEntry const* stateEntry : sBattlePetAbilityStateStore)
    {
        if (stateEntry->AbilityId != m_ability)
            continue;

        for (BattlePet* target : m_targets)
        {
            // Elemental passive aura
            if (target->States[BATTLE_PET_STATE_PASSIVE_ELEMENTAL])
            {
                bool IsNegativeState = false;

                switch (stateEntry->StateId)
                {
                    case BATTLE_PET_STATE_STAT_ACCURACY:
                    case BATTLE_PET_STATE_MOD_HEALING_TAKEN_PCT:
                        if (stateEntry->Value < 0)
                            IsNegativeState = true;
                        break;
                    case BATTLE_PET_STATE_MECHANIC_BURNING:
                    case BATTLE_PET_STATE_MECHANIC_CHILLED:
                    case BATTLE_PET_STATE_MECHANIC_BLIND:
                        if (stateEntry->Value > 0)
                            IsNegativeState = true;
                        break;
                    default:
                        break;
                }

                if (IsNegativeState)
                    continue;
            }

            // store state and modifier for removal
            if (m_weatherStates[target->GetGlobalIndex()].find(stateEntry->StateId) == m_weatherStates[target->GetGlobalIndex()].end())
                m_weatherStates[target->GetGlobalIndex()][stateEntry->StateId] = 0;

            int32 newValue = stateEntry->Value + target->States[stateEntry->StateId];
            m_weatherStates[target->GetGlobalIndex()][stateEntry->StateId] += newValue;

            // update state
            m_petBattle->UpdatePetState(m_caster, target, 0, stateEntry->StateId, newValue);
        }
    }
}

void PetBattleWeather::OnExpire()
{
    // remove any state modifiers for the aura
    for (BattlePet* target : m_targets)
        for (auto stateEntry : m_weatherStates[target->GetGlobalIndex()])
        {
            int32 newValue = target->States[stateEntry.first] - stateEntry.second;
            m_petBattle->UpdatePetState(m_caster, target, 0, stateEntry.first, newValue);
        }

    m_weatherStates.clear();
}

void PetBattleWeather::Process()
{
    // expire aura if it has reached max duration
    if (m_duration != -1 && m_turn > m_maxDuration)
    {
        Expire();
        return;
    }

    // handle aura effects
    if (BattlePetAbilityEntry const* abilityEntry = sBattlePetAbilityStore.LookupEntry(m_ability))
    {
        uint32 turnCount = 0;
        uint32 topMaxTurnId = 0;

        // find longest duration effect
        for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
        {
            if (abilityTurnEntry->AbilityId != m_ability)
                continue;

            turnCount++;
            topMaxTurnId = std::max(topMaxTurnId, abilityTurnEntry->Duration);
        }

        for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
        {
            if (abilityTurnEntry->AbilityId != m_ability)
                continue;

            // make sure ability has reached duration required for effect
            if (abilityTurnEntry->Duration != m_turn && turnCount != 1 && topMaxTurnId != 1)
                continue;

            for (BattlePetAbilityEffectEntry const* abilityEffectEntry : sBattlePetAbilityEffectStore)
            {
                if (abilityEffectEntry->AbilityTurnId != abilityTurnEntry->Id)
                    continue;

                // initialise ability effect
                BattlePetAbilityEffect abilityEffect;

                abilityEffect.SetAbilityInfo(m_ability, abilityEffectEntry, abilityEntry->TypeId);
                abilityEffect.SetCaster(m_caster);
                abilityEffect.SetParentBattle(m_petBattle);

                abilityEffect.SetTarget();
                abilityEffect.Execute();
            }
        }
    }

    // notify client of aura update
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_CHANGE, m_caster->GetGlobalIndex());
    effect.TargetUpdateAura(PET_BATTLE_TARGET_INDEX_WEATHER, m_id, m_ability, m_duration, m_turn);

    m_petBattle->Effects.push_back(effect);

    m_turn++;

    if (m_duration != -1)
        m_duration--;
}

void PetBattleWeather::Expire()
{
    if (m_expired)
        return;

    m_expired = true;

    // notify client of aura removal
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_REMOVE, m_caster->GetGlobalIndex(), 0, 0, 1, 0, 1);
    effect.TargetUpdateAura(PET_BATTLE_TARGET_INDEX_WEATHER, m_id, m_ability, 0, 0);

    m_petBattle->Effects.push_back(effect);

    OnExpire();
}

uint32 PetBattleWeather::GetId() const
{
    return m_id;
}

uint32 PetBattleWeather::GetAbilityId() const
{
    return m_ability;
}

int32 PetBattleWeather::GetDuration() const
{
    return m_duration;
}

int32 PetBattleWeather::GetTurn() const
{
    return m_turn;
}

void PetBattleWeather::ResetWeather()
{
    m_duration = m_maxDuration;
    m_turn = 1;
}

void PetBattleWeather::AddTarget(BattlePet* pet)
{
    m_targets.push_back(pet);
}

// -------------------------------------------------------------------------------

void PetBattleTeam::AddPlayer(Player* player)
{
    m_owner = player;
    m_ownerGuid = player->GetGUID();

    // add player loadout battle pets to team
    uint8 localIndex = 0;
    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
    {
        ObjectGuid battlePetGUID = player->GetBattlePetMgr()->GetLoadoutSlot(i);
        if (battlePetGUID.IsEmpty())
            continue;

        BattlePet* battlePet = player->GetBattlePetMgr()->GetBattlePet(battlePetGUID);
        if (!battlePet || !battlePet->IsAlive())
            continue;

        // bind pet battle information to battle pet
        battlePet->SetBattleInfo(m_teamIndex, ConvertToGlobalIndex(localIndex++));
        battlePet->SetBattleAbilities();

        // in PvE matches, the first pet in loadout is initially set to active pet
        if (m_petBattle->GetType() == PET_BATTLE_TYPE_PVE && !m_activePet)
            m_activePet = battlePet;

        BattlePets.push_back(battlePet);
    }

    returnTele = *player;
}

void PetBattleTeam::AddWildBattlePet(Creature* creature)
{
    m_owner = creature;

    BattlePet* battlePet = sBattlePetSpawnMgr->GetWildBattlePet(creature->GetGUID());
    ASSERT(battlePet);

    battlePet->SetBattleInfo(m_teamIndex, ConvertToGlobalIndex(BattlePets.size()));
    battlePet->SetBattleAbilities();

    BattlePets.push_back(battlePet);

    // only update creature if it's the first to the team
    if (!m_wildBattlePet)
    {
        m_wildBattlePet = creature;
        m_activePet = battlePet;
    }
}

void PetBattleTeam::Update(uint32 diff)
{
    if (m_ready)
        return;

    if (m_petBattle->GetType() != PET_BATTLE_TYPE_PVE && m_petBattle->GetRound())
    {
        if (m_roundTimer <= diff)
            m_ready = true;
        else
            m_roundTimer -= diff;
    }

    if (!m_activePet || !m_activePet->IsAlive())
    {
        BattlePetStore avaliablePets;
        GetAvaliablePets(avaliablePets);

        // team is dead, finish battle
        if (!avaliablePets.size())
        {
            m_petBattle->FinishBattle(this, false);
            return;
        }

        // final pet, auto swap
        if (avaliablePets.size() == 1)
        {
            m_petBattle->SwapActivePet(this, avaliablePets[0]);
            m_ready = true;
        }
    }
    else
    {
        // team has no move if multiple turn ability active
        if (HasMultipleTurnAbility())
        {
            m_ready = true;
            return;
        }

        // wild battle pets currently choose a random ability that isn't on cooldown, no other AI
        if (m_petBattle->GetType() == PET_BATTLE_TYPE_PVE && m_teamIndex == PET_BATTLE_OPPONENT_TEAM)
        {
            AvaliableAbilities avaliableAbilities;
            for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
                if (m_activePet->Abilities[i])
                    if (CanActivePetCast(m_activePet->Abilities[i]->AbilityId))
                        avaliableAbilities.push_back(m_activePet->Abilities[i]->AbilityId);

            // cast ability
            if (avaliableAbilities.size())
            {
                uint32 spell = Trinity::Containers::SelectRandomContainerElement( avaliableAbilities );
                SetPendingMove( PET_BATTLE_MOVE_TYPE_CAST, spell, nullptr );
            }
            // skip turn
            else
                SetPendingMove(PET_BATTLE_MOVE_TYPE_SWAP_OR_PASS, 0, m_activePet);
        }
    }
}

void PetBattleTeam::ActivePetPrepareCast(uint32 abilityId)
{
    if (!abilityId)
        return;

    // find the longest duration effect for ability
    uint8 duration = 0;
    for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
    {
        if (abilityTurnEntry->AbilityId != abilityId)
            continue;

        if (abilityTurnEntry->Duration >= duration)
            duration = abilityTurnEntry->Duration;
    }

    ActiveAbility.AbilityId   = abilityId;
    ActiveAbility.TurnsPassed = 1;
    ActiveAbility.TurnsTotal  = duration;
}

bool PetBattleTeam::CanActivePetCast(uint32 abilityId) const
{
    if (HasMultipleTurnAbility())
        return false;

    if (!sBattlePetAbilityStore.LookupEntry(abilityId))
        return false;

    // make sure active pet has ability and it isn't on cooldown
    for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
        if (m_activePet->Abilities[i] && m_activePet->Abilities[i]->AbilityId == abilityId && !m_activePet->Abilities[i]->OnCooldown)
            return true;

    return false;
}

uint8 PetBattleTeam::GetTrapStatus() const
{
    if (m_petBattle->GetType() != PET_BATTLE_TYPE_PVE)
        return PET_BATTLE_TRAP_STATUS_NOT_CAPTURABLE;

    if (m_teamIndex == PET_BATTLE_OPPONENT_TEAM)
        return PET_BATTLE_TRAP_STATUS_DISABLED;

    BattlePetMgr const* battlePetMgr = GetPlayerOwner()->GetBattlePetMgr();
    if (!battlePetMgr)
        return PET_BATTLE_TRAP_STATUS_DISABLED;

    // player needs to have a trap ability
    if (!battlePetMgr->GetTrapAbility())
        return PET_BATTLE_TRAP_STATUS_DISABLED;

    // player can only catch a single pet per battle
    if (m_petBattle->GetCagedPet())
        return PET_BATTLE_TRAP_STATUS_ALREADY_TRAPPED;

    PetBattleTeam const* team = m_petBattle->Teams[PET_BATTLE_OPPONENT_TEAM];
    if (!team)
        return PET_BATTLE_TRAP_STATUS_DISABLED;

    BattlePet const* battlePet = team->GetActivePet();
    if (!battlePet)
        return PET_BATTLE_TRAP_STATUS_DISABLED;

    if (!battlePet->IsAlive())
        return PET_BATTLE_TRAP_STATUS_CANT_TRAP_DEAD_PET;

    // make sure player can store more battle pets
    if (!battlePetMgr->CanStoreBattlePet(battlePet->GetSpecies()))
        return PET_BATTLE_TRAP_STATUS_TOO_MANY_PETS;

    // check if pet is below 35% health
    if (battlePet->GetCurrentHealth() > CalculatePct(battlePet->GetMaxHealth(), 35))
        return PET_BATTLE_TRAP_STATUS_HEALTH_TOO_HIGH;

    return PET_BATTLE_TRAP_STATUS_ENABLED;
}

bool PetBattleTeam::CanSwap(BattlePet* battlePet, bool ignoreAlive) const
{
    if (HasMultipleTurnAbility())
        return false;

    if (m_activePet)
    {
        if (!m_activePet->IsAlive() && !ignoreAlive)
            return false;

        // used for abilities such as Sticky Web (339)
        if (m_activePet->States[BATTLE_PET_STATE_SWAP_OUT_LOCK])
            return false;
    }

    if (battlePet)
    {
        if (!battlePet->IsAlive())
            return false;

        // used for abilities such as Banished (717)
        if (battlePet->States[BATTLE_PET_STATE_SWAP_IN_LOCK])
            return false;
    }

    return true;
}

bool PetBattleTeam::HasMultipleTurnAbility() const
{
    if (ActiveAbility.AbilityId && ActiveAbility.TurnsTotal != 1 && ActiveAbility.TurnsPassed <= ActiveAbility.TurnsTotal)
        return true;

    return false;
}

void PetBattleTeam::DoCasts(int8 procType)
{
    if (!m_activePet)
        return;

    if (!m_activePet->IsAlive())
        return;

    if (!m_activePet->CanAttack())
        return;

    bool noProc = procType == PET_BATTLE_ABILITY_TURN0_PROC_ON_NONE;
    m_petBattle->Cast(m_activePet, ActiveAbility.AbilityId, noProc ? ActiveAbility.TurnsPassed : 0, procType);
}

void PetBattleTeam::GetAvaliablePets(BattlePetStore& avaliablePets) const
{
    for (BattlePet* battlePet : BattlePets)
    {
        // make sure local pet can be swapped with active
        if (!CanSwap(battlePet, true))
            continue;

        avaliablePets.push_back(battlePet);
    }
}

// checks if a battle pet belongs to a pet battle team
bool PetBattleTeam::IsValidBattlePet(BattlePet* battlePet) const
{
    bool isValid = false;
    for (BattlePet* battlePetTeam : BattlePets)
        if (battlePetTeam == battlePet)
        {
            isValid = true;
            break;
        }

    return isValid;
}

uint8 PetBattleTeam::GetInputStatusFlags() const
{
    uint8 flags = PET_BATTLE_TEAM_INPUT_FLAG_NONE;

    // TODO: more checks probably required
    if (HasMultipleTurnAbility())
        flags |= (PET_BATTLE_TEAM_INPUT_FLAG_LOCK_ABILITY_1 | PET_BATTLE_TEAM_INPUT_FLAG_LOCK_ABILITY_2);

    if (!CanSwap())
        flags |= PET_BATTLE_TEAM_INPUT_FLAG_LOCK_PET_SWAP;

    if (!m_activePet || !m_activePet->IsAlive())
    {
        BattlePetStore avaliablePets;
        GetAvaliablePets(avaliablePets);

        if (avaliablePets.size() > 1)
            flags |= PET_BATTLE_TEAM_INPUT_FLAG_SELECT_NEW_PET;
        else
            flags |= PET_BATTLE_TEAM_INPUT_FLAG_LOCK_PET_SWAP;
    }

    return flags;
}

void PetBattleTeam::ResetActiveAbility()
{
    ActiveAbility.AbilityId   = 0;
    ActiveAbility.TurnsPassed = 0;
    ActiveAbility.TurnsTotal  = 0;
}

void PetBattleTeam::ProcessAuras()
{
    // process team auras
    for (PetBattleTeamAura* aura : TeamAuras)
        if (!aura->HasExpired())
            aura->Process();

    // process any active auras
    for (BattlePet* battlePet : BattlePets)
    {
        for (PetBattleAura* aura : battlePet->Auras)
        {
            if (!aura->HasExpired())
                aura->Process();

            if (m_activePet->States[BATTLE_PET_STATE_COSMECTIC_STEALTHED] && aura->GetTurn() > 2 && m_activePet->States[BATTLE_PET_STATE_CONDITION_DID_DAMAGE_THIS_ROUND])
                aura->Expire();
        }
    }
}

void PetBattleTeam::RemoveExpiredAuras()
{
    // find any expired auras that need removing
    TeamAuraRemovalStore teamRemovalList;
    for (PetBattleTeamAura* aura : TeamAuras)
        if (aura->HasExpired())
            teamRemovalList.push_back(aura);

    // delete expired auras
    for (PetBattleTeamAura* aura : teamRemovalList)
    {
        TeamAuras.remove(aura);
        delete aura;
    }

    teamRemovalList.clear();

    AuraRemovalStore removalList;
    for (BattlePet* battlePet : BattlePets)
    {
        for (PetBattleAura* aura : battlePet->Auras)
            if (aura->HasExpired())
                removalList.push_back(aura);

        // delete expired auras
        for (PetBattleAura* aura : removalList)
        {
            battlePet->Auras.remove(aura);
            delete aura;
        }

        removalList.clear();
    }
}

void PetBattleTeam::SetPendingMove(uint8 moveType, uint32 abilityId, BattlePet* newActivePet)
{
    m_pendingMove.MoveType  = moveType;
    m_pendingMove.AbilityId = abilityId;
    m_pendingMove.battlePet = newActivePet;
    m_ready = true;
}

// -------------------------------------------------------------------------------

uint8 PetBattleTeam::ConvertToGlobalIndex(uint8 localPetIndex) const
{
    ASSERT(localPetIndex < PET_BATTLE_MAX_TEAM_PETS);
    return localPetIndex + (m_teamIndex == PET_BATTLE_CHALLANGER_TEAM ? 0 : PET_BATTLE_MAX_TEAM_PETS);
}

uint8 PetBattleTeam::ConvertToLocalIndex(uint8 globalPetIndex) const
{
    ASSERT(globalPetIndex < (PET_BATTLE_MAX_TEAM_PETS * PET_BATTLE_MAX_TEAMS));
    return globalPetIndex - (m_teamIndex == PET_BATTLE_CHALLANGER_TEAM ? 0 : PET_BATTLE_MAX_TEAM_PETS);
}

void PetBattleTeam::AddRoundTimer(uint32 abilitiesCount)
{
    m_nextRoundTimer = 30 + (2 * abilitiesCount);
    m_roundTimer = m_nextRoundTimer * IN_MILLISECONDS;
}

Player* PetBattleTeam::GetPlayerOwner() const
{
    if (m_owner && m_owner->GetTypeId() == TYPEID_PLAYER)
        return m_owner->ToPlayer();

    return nullptr;
}

PetBattleTeamAura* PetBattleTeam::GetTeamAura(uint32 abilityId) const
{
    for (PetBattleTeamAura* aura : TeamAuras)
        if (aura->GetAbility() == abilityId)
            return aura;

    return nullptr;
}

// -------------------------------------------------------------------------------

PetBattle::~PetBattle()
{
    // clean up teams
    for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; i++)
        delete Teams[i];
}

void PetBattle::StartBattle()
{
    // in PvE matches the initial active pet is locked
    for (PetBattleTeam* team : Teams)
    {
        if (team->CanSwap())
            SwapActivePet(team, team->GetActivePet());

        for (BattlePet * pet : team->BattlePets)
            pet->InitializePassiveStates(this);
    }

    for (PetBattleTeam* team : Teams)
    {
        // initial team setup only required for players
        if (team->GetTeamIndex() == PET_BATTLE_OPPONENT_TEAM && m_type == PET_BATTLE_TYPE_PVE)
            continue;

        Player* owner = team->GetPlayerOwner();
        if (!owner)
            continue;

        owner->GetBattlePetMgr()->UnSummonCurrentBattlePet(true);

        // send initial packet update
        owner->GetSession()->SendPetBattleInitialUpdate(this, team->GetTeamIndex() == PET_BATTLE_OPPONENT_TEAM);

        if (GetType() == PET_BATTLE_TYPE_PVE)
            owner->GetSession()->SendPetBattleFirstRound(this);
    }

    // round setup
    m_phase = PET_BATTLE_PHASE_IN_PROGRESS;

    if (GetType() == PET_BATTLE_TYPE_PVE)
    {
        Effects.clear();
        m_round++;
    }
}

void PetBattle::FinishBattle(PetBattleTeam* lostTeam, bool forfeit)
{
    // if no winning team is specified, battle was forcefully ended
    m_winningTeam = nullptr;
    if (lostTeam)
        m_winningTeam = Teams[!lostTeam->GetTeamIndex()];

    if (m_weather)
    {
        if (!m_weather->HasExpired())
            m_weather->Expire();

        delete m_weather;
    }

    for (PetBattleTeam* team : Teams)
    {
        // remove team auras
        for (PetBattleTeamAura* aura : team->TeamAuras)
        {
            aura->OnExpire();
            delete aura;
        }

        team->TeamAuras.clear();

        for (BattlePet* battlePet : team->BattlePets)
        {
            // remove abilities
            for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
            {
                if (BattlePetAbility* ability = battlePet->Abilities[i])
                {
                    delete ability;
                    battlePet->Abilities[i] = nullptr;
                }
            }

            // remove auras
            for (PetBattleAura* aura : battlePet->Auras)
            {
                aura->OnExpire();
                delete aura;
            }

            battlePet->Auras.clear();

            // remove any remaining mechanic states not cleaned up on aura removal
            battlePet->ResetMechanicStates();
        }

        if (Player* player = team->GetPlayerOwner())
        {
            for (BattlePet* battlePet : team->BattlePets)
            {
                // make sure battle wasn't forcefully ended
                if (m_winningTeam)
                {
                    // remove 10% health on forfeit
                    if (team != m_winningTeam && forfeit)
                    {
                        uint16 reduction = CalculatePct(battlePet->GetCurrentHealth(), 10);
                        battlePet->SetCurrentHealth(battlePet->GetCurrentHealth() - reduction);
                    }

                    // award XP to battle pets that actively participated in the battle
                    if (team->SeenAction.find(battlePet->GetGUID()) != team->SeenAction.end()
                        && GetType() == PET_BATTLE_TYPE_PVE
                        && m_winningTeam == team
                        && battlePet->GetLevel() != BATTLE_PET_MAX_LEVEL
                        && battlePet->IsAlive())
                    {
                        // XP gain formular from: http://www.wowwiki.com/Pet_Battle_System#Calculating_experience
                        uint16 xp = 0;
                        for (BattlePet* opponentBattlePet : Teams[!team->GetTeamIndex()]->BattlePets)
                        {
                            int levelDifference = opponentBattlePet->GetLevel() - battlePet->GetLevel();

                            // level difference roof capped at +2
                            if (levelDifference > 2)
                                levelDifference = 2;
                            // level difference floor capped at -4
                            else if (levelDifference < -4)
                                levelDifference = -4;

                            xp += ((opponentBattlePet->GetLevel() + 9) * (levelDifference + 5)) / team->SeenAction.size();

                            // SPELL_AURA_MOD_BATTLE_PET_XP_PCT
                            xp *= player->GetTotalAuraMultiplier(SPELL_AURA_MOD_BATTLE_PET_XP_PCT);
                        }

                        battlePet->SetXP(xp);
                    }

                    // update battle pet clientside
                    player->GetBattlePetMgr()->SendBattlePetUpdate(battlePet, false);
                }
            }

            // make sure battle wasn't forcefully ended
            if (m_winningTeam)
            {
                // save captured pets
                if (m_winningTeam == team)
                {
                    if (GetType() == PET_BATTLE_TYPE_PVE)
                    {
                        if (BattlePet* battlePet = GetCagedPet())
                        {
                            BattlePetMgr* battlePetMgr = player->GetBattlePetMgr();
                            if (!battlePetMgr)
                                continue;

                            if (!battlePetMgr->CanStoreBattlePet(battlePet->GetSpecies()))
                                continue;

                            uint8 level = battlePet->GetLevel();

                            // level 16-20 pets lose 1 level when caught
                            if (level >= 16 && level <= 20)
                                level--;
                            // level 21-25 pets lose 2 levels when caught
                            else if (level >= 21 && level <= 25)
                                level -= 2;

                            BattlePet* newBattlePet = battlePetMgr->Create(battlePet->GetSpecies(), level, battlePet->GetBreed(), battlePet->GetQuality(), battlePet->GetFamily());

                            uint32 HealtPct = (battlePet->GetCurrentHealth() / battlePet->GetMaxHealth()) * 100;

                            player->UpdateCriteria(CRITERIA_TYPE_CAPTURE_BATTLE_PET, player, battlePet->GetSpecies(), HealtPct, 7 + battlePet->GetQuality());
                        }
                    }

                    player->UpdateCriteria(CRITERIA_TYPE_WIN_PET_BATTLE, lostTeam ? lostTeam->GetOwner() : nullptr, 1);
                    if (Scenario* scenario = player->GetScenario())
                        scenario->UpdateCriteria(CRITERIA_TYPE_WIN_PET_BATTLE, player, lostTeam ? lostTeam->GetOwner() : nullptr, 1);
                }
                else
                {
                    player->ResetCriteria(CRITERIA_CONDITION_NO_LOSE_PET_BATTLE);
                    if (GetType() != PET_BATTLE_TYPE_PVE)
                        player->ResetCriteria(CRITERIA_CONDITION_NO_LOSE_PVP_PET_BATTLE);
                }
            }

            if (m_type != PET_BATTLE_TYPE_PVP_MATCHMAKING)
            {
                player->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_PACIFIED | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC);
                player->SetControlled(false, UNIT_STATE_ROOT);
            }

            // alert client of pet battle end
            if (!m_winningTeam)
                // instant battle end
                player->GetSession()->SendPetBattleFinished();
            else
                // delayed battle end and statistics are displayed
                player->GetSession()->SendPetBattleFinalRound(this);
        }

        if (Creature* creature = team->GetWildBattlePet())
            sBattlePetSpawnMgr->LeftBattle(creature, team == lostTeam);
    }

    m_phase = PET_BATTLE_PHASE_FINISHED;
}

void PetBattle::HandleRound()
{
    TurnInstance = 1;

    int32 BattlePetOldStates[PET_BATTLE_MAX_TEAMS * PET_BATTLE_MAX_TEAM_PETS][BATTLE_PET_MAX_STATES] = { };

    // reset state conditions
    for (PetBattleTeam* team : Teams)
    {
        for (BattlePet* battlePet : team->BattlePets)
        {
            battlePet->States[BATTLE_PET_STATE_CONDITION_WAS_DAMAGED_THIS_ROUND] = 0;
            battlePet->States[BATTLE_PET_STATE_CONDITION_DID_DAMAGE_THIS_ROUND] = 0;

            // store main stat states before they are modified during the round
            BattlePetOldStates[battlePet->GetGlobalIndex()][BATTLE_PET_STATE_STAT_SPEED] = battlePet->GetSpeed();
            BattlePetOldStates[battlePet->GetGlobalIndex()][BATTLE_PET_STATE_STAT_POWER] = battlePet->GetPower();
            BattlePetOldStates[battlePet->GetGlobalIndex()][BATTLE_PET_STATE_STAT_STAMINA] = battlePet->GetMaxHealth();
            BattlePetOldStates[battlePet->GetGlobalIndex()][battlePet->GetPassiveState()] = battlePet->GetPassiveStateValue();
        }
    }

    uint8 firstTeam = GetFirstAttackingTeam();

    // -------------------------------------------------------------------------------

    // cast abilities that have round start proc type (example: Deflection(490))
    Teams[firstTeam]->DoCasts(PET_BATTLE_ABILITY_TURN0_PROC_ON_ROUND_START);
    Teams[!firstTeam]->DoCasts(PET_BATTLE_ABILITY_TURN0_PROC_ON_ROUND_START);

    // -------------------------------------------------------------------------------

    // cast non proc abilities
    Teams[firstTeam]->DoCasts();
    Teams[!firstTeam]->DoCasts();

    // -------------------------------------------------------------------------------

    PetBattleEffect auraStartEffect(PET_BATTLE_EFFECT_AURA_PROCESSING_BEGIN);
    auraStartEffect.TargetActivePet();

    Effects.push_back(auraStartEffect);

    // process weather
    if (m_weather)
    {
        if (!m_weather->HasExpired())
            m_weather->Process();
        else
            delete m_weather;
    }

    // process active battle pet auras
    Teams[firstTeam]->ProcessAuras();
    Teams[!firstTeam]->ProcessAuras();

    PetBattleEffect auraEndEffect(PET_BATTLE_EFFECT_AURA_PROCESSING_END);
    auraEndEffect.TargetActivePet();

    Effects.push_back(auraEndEffect);

    // -------------------------------------------------------------------------------

    // cast abilities that have round end proc type
    Teams[firstTeam]->DoCasts(PET_BATTLE_ABILITY_TURN0_PROC_ON_ROUND_END);
    Teams[!firstTeam]->DoCasts(PET_BATTLE_ABILITY_TURN0_PROC_ON_ROUND_END);

    // -------------------------------------------------------------------------------

    // send stat updates if they changed this round
    for (PetBattleTeam* team : Teams)
    {
        for (BattlePet* battlePet : team->BattlePets)
        {
            if (BattlePetOldStates[battlePet->GetGlobalIndex()][BATTLE_PET_STATE_STAT_SPEED] != battlePet->GetSpeed())
            {
                // alert client of speed update
                PetBattleEffect speedEffect(PET_BATTLE_EFFECT_SET_SPEED, battlePet->GetGlobalIndex());
                speedEffect.TargetUpdateStat(battlePet->GetGlobalIndex(), battlePet->GetSpeed());

                Effects.push_back(speedEffect);
            }

            if (BattlePetOldStates[battlePet->GetGlobalIndex()][BATTLE_PET_STATE_STAT_POWER] != battlePet->GetPower())
            {
                // alert client of power update
                PetBattleEffect powerEffect(PET_BATTLE_EFFECT_SET_POWER, battlePet->GetGlobalIndex());
                powerEffect.TargetUpdateStat(battlePet->GetGlobalIndex(), battlePet->GetPower());

                Effects.push_back(powerEffect);
            }

            if (BattlePetOldStates[battlePet->GetGlobalIndex()][BATTLE_PET_STATE_STAT_STAMINA] != battlePet->GetMaxHealth())
            {
                // alert client of max health update
                PetBattleEffect maxHealthEffect(PET_BATTLE_EFFECT_SET_MAX_HEALTH, battlePet->GetGlobalIndex());
                maxHealthEffect.TargetUpdateStat(battlePet->GetGlobalIndex(), battlePet->GetMaxHealth());

                Effects.push_back(maxHealthEffect);
            }

            // old passive states
            if (BattlePetOldStates[battlePet->GetGlobalIndex()][battlePet->GetPassiveState()] != battlePet->GetPassiveStateValue())
            {
                uint32 PassiveAuraID = battlePet->GetPassiveAuraID();
                if (!battlePet->GetPassiveStateValue())
                {
                    if (PetBattleAura* aura = battlePet->GetAura(PassiveAuraID))
                        if (!aura->HasExpired())
                            aura->Expire();
                }
                else
                    AddAura(battlePet, battlePet, PassiveAuraID, PassiveAuraID, -1);
            }

            // Humanoid passive aura
            if (battlePet->States[BATTLE_PET_STATE_CONDITION_DID_DAMAGE_THIS_ROUND] && battlePet->States[BATTLE_PET_STATE_PASSIVE_HUMANOID])
                Cast(battlePet, BATTLE_PET_SPELL_RECOVERY, 0, PET_BATTLE_ABILITY_TURN0_PROC_ON_NONE);

            // special case for Planted
            if (battlePet->States[BATTLE_PET_STATE_SPECIAL_PLANT] > 0)
                ++battlePet->States[BATTLE_PET_STATE_SPECIAL_PLANT];
        }
    }

    // must be incremented before sending round result
    for (PetBattleTeam* team : Teams)
    {
        team->ActiveAbility.TurnsPassed++;

        // check abilites count and increase pvp timer
        if (m_type != PET_BATTLE_TYPE_PVE)
        {
            size_t effectsCount = std::count_if(Effects.begin(), Effects.end(), [](PetBattleEffect const& effect)
            {
                switch (effect.GetType())
                {
                    case PET_BATTLE_EFFECT_AURA_APPLY:
                    case PET_BATTLE_EFFECT_AURA_CHANGE:
                    case PET_BATTLE_EFFECT_AURA_PROCESSING_BEGIN:
                    case PET_BATTLE_EFFECT_AURA_PROCESSING_END:
                        return false;
                    default:
                        return true;
                }
            });

            team->AddRoundTimer(effectsCount);
        }
    }

    // send round result to players
    if (m_roundResult == PET_BATTLE_ROUND_RESULT_NONE)
        m_roundResult = PET_BATTLE_ROUND_RESULT_NORMAL;

    for (PetBattleTeam* team : Teams)
        if (Player* player = team->GetPlayerOwner())
        {
            if (!m_round)
                player->GetSession()->SendPetBattleFirstRound(this);
            else
                player->GetSession()->SendPetBattleRoundResult(this);
        }

    // initialise data for the next round
    for (PetBattleTeam* team : Teams)
    {
        team->RemoveExpiredAuras();

        // reduce ability cooldowns
        for (BattlePet* battlePet : team->BattlePets)
            for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
                if (battlePet->Abilities[i] && battlePet->Abilities[i]->Cooldown)
                    battlePet->Abilities[i]->Cooldown--;

        team->IsReady(false);
    }

    m_round++;
    m_roundResult = PET_BATTLE_ROUND_RESULT_NONE;

    Effects.clear();
}

void PetBattle::Update(uint32 diff)
{
    if (m_phase != PET_BATTLE_PHASE_IN_PROGRESS)
        return;

    if (m_isProccededRound)
        return;

    m_timeSinceLastRound += diff;

    // if m_timeSinceLastRound not resets for 2 minutes, finish battle
    if (m_timeSinceLastRound >= 120 * IN_MILLISECONDS)
    {
        FinishBattle(nullptr, false);
        return;
    }

    // check if all teams are ready to progress round
    bool progressRound = true;
    for (PetBattleTeam* team : Teams)
    {
        team->Update(diff);

        if (!team->IsReady())
            progressRound = false;
    }

    if (progressRound)
    {
        m_timeSinceLastRound = 0;
        m_isProccededRound = true;

        // execute pending moves before handling the round
        for (PetBattleTeam* team : Teams)
        {
            PendingRoundMove pendingMove = team->GetPendingMove();
            switch (pendingMove.MoveType)
            {
                case PET_BATTLE_MOVE_TYPE_CAST:
                {
                    if (team->CanActivePetCast(pendingMove.AbilityId))
                        team->ActivePetPrepareCast(pendingMove.AbilityId);
                    break;
                }
                case PET_BATTLE_MOVE_TYPE_CATCH:
                {
                    if (team->GetTrapStatus() == PET_BATTLE_TRAP_STATUS_ENABLED)
                        team->ActivePetPrepareCast(team->GetPlayerOwner()->GetBattlePetMgr()->GetTrapAbility());
                    break;
                }
                case PET_BATTLE_MOVE_TYPE_SWAP_OR_PASS:
                {
                    if ((!team->GetActivePet() || team->GetActivePet()->GetGlobalIndex() != pendingMove.battlePet->GetGlobalIndex()) && team->CanSwap(pendingMove.battlePet))
                        SwapActivePet(team, pendingMove.battlePet);
                    else if (!team->HasMultipleTurnAbility())
                        team->ResetActiveAbility();
                    break;
                }
            }
        }

        HandleRound();

        m_isProccededRound = false;
    }
}

bool PetBattle::Cast(BattlePet* caster, uint32 abilityId, uint8 turn, int8 procType)
{
    // make sure the ability exists
    BattlePetAbilityEntry const* abilityEntry = sBattlePetAbilityStore.LookupEntry(abilityId);
    if (!abilityEntry)
        return false;

    // prevent states being updated every tick for multiple turn abilities
    if (!turn)
    {
        // update any states the ability modifies
        for (BattlePetAbilityStateEntry const* abilityStateEntry : sBattlePetAbilityStateStore)
        {
            if (abilityStateEntry->AbilityId != abilityId)
                continue;

            // update battle pet state
            UpdatePetState(caster, caster, 0, abilityStateEntry->StateId, caster->States[abilityStateEntry->StateId] + abilityStateEntry->Value);
        }
    }

    // handle ability effects
    for (BattlePetAbilityTurnEntry const* abilityTurnEntry : sBattlePetAbilityTurnStore)
    {
        if (abilityTurnEntry->AbilityId != abilityId)
            continue;

        // make sure multiple turn ability has done it's full duration
        if (abilityTurnEntry->Duration > 1 && abilityTurnEntry->Duration != turn)
            continue;

        if (abilityTurnEntry->ProcType != procType)
            continue;

        for (BattlePetAbilityEffectEntry const* abilityEffectEntry : sBattlePetAbilityEffectStore)
        {
            if (abilityEffectEntry->AbilityTurnId != abilityTurnEntry->Id)
                continue;

            // initialise ability effect
            BattlePetAbilityEffect abilityEffect;
            abilityEffect.SetAbilityInfo(abilityId, abilityEffectEntry, abilityEntry->TypeId);
            abilityEffect.SetCaster(caster);
            abilityEffect.SetParentBattle(this);

            abilityEffect.SetTarget();
            abilityEffect.Execute();
        }
    }

    // update ability cooldown
    for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
    {
        if (caster->Abilities[i] && caster->Abilities[i]->AbilityId == abilityEntry->Id)
        {
            // make sure ability has a cooldown
            if (uint32 cooldown = abilityEntry->RoundCooldown)
            {
                caster->Abilities[i]->Cooldown   = cooldown;
                caster->Abilities[i]->OnCooldown = true;
            }
        }
    }

    return true;
}

void PetBattle::SwapActivePet(PetBattleTeam* team, BattlePet* battlePet)
{
    if (!team->IsValidBattlePet(battlePet))
        return;

    team->DoCasts(PET_BATTLE_ABILITY_TURN0_PROC_ON_SWAP_OUT);

    // update team information
    team->ResetActiveAbility();
    team->SetActivePet(battlePet);
    team->SeenAction.insert(battlePet->GetGUID());

    // alert client of active pet swap
    PetBattleEffect effect(PET_BATTLE_EFFECT_ACTIVE_PET, battlePet->GetGlobalIndex());
    effect.TargetActivePet(battlePet->GetGlobalIndex());

    Effects.push_back(effect);

    team->DoCasts(PET_BATTLE_ABILITY_TURN0_PROC_ON_SWAP_IN);
}

void PetBattle::UpdatePetState(BattlePet* source, BattlePet* target, uint32 abilityEffect, uint32 state, int32 value, uint32 flags)
{
    if (!sBattlePetStateStore.LookupEntry(state))
        return;

    if (target->States[state] == value)
        return;

    target->States[state] = value;

    // notify client of state change
    PetBattleEffect effect(PET_BATTLE_EFFECT_SET_STATE, source->GetGlobalIndex(), flags, abilityEffect, TurnInstance++, 0, 1);
    effect.TargetUpdateState(target->GetGlobalIndex(), state, value);

    Effects.push_back(effect);
}

void PetBattle::AddAura(BattlePet* source, BattlePet* target, uint32 ability, uint32 abilityEffect, int32 duration, uint32 flags, uint8 maxAllowed)
{
    if (!source->IsAlive() || !target->IsAlive())
        return;

    // count current auras for ability
    uint32 id = 0;
    uint32 auraCount = 0;

    for (PetBattleAura* aura : target->Auras)
    {
        if (aura->GetId() > id)
            id = aura->GetId();

        if (!aura->HasExpired() && aura->GetAbility() == ability)
            auraCount++;
    }

    ++id;

    if (!duration && duration != -1)
        duration++;

    // notify client of aura update
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_APPLY, source->GetGlobalIndex(), flags, abilityEffect, TurnInstance++, 0, 1);
    effect.TargetUpdateAura(target->GetGlobalIndex(), id, ability, duration, 0);

    Effects.push_back(effect);

    if ((flags & FailFlags) != 0)
        return;

    // expire auras above the allowed count
    if (maxAllowed && auraCount >= maxAllowed)
    {
        uint8 removeCount = 1 + auraCount - maxAllowed;
        for (PetBattleAura* aura : target->Auras)
        {
            if (!removeCount)
                continue;

            if (!aura->HasExpired() && aura->GetAbility() == ability)
            {
                aura->Expire();
                removeCount--;
            }
        }
    }

    // create and apply aura
    PetBattleAura* aura = new PetBattleAura(this, id, ability, abilityEffect, source, target, duration);
    target->Auras.push_back(aura);

    aura->OnApply();
}

void PetBattle::AddInitPassiveAura(BattlePet* source, uint32 ability)
{
    uint32 id = 0;

    for (PetBattleAura* aura : source->Auras)
        if (aura->GetId() > id)
            id = aura->GetId();

    ++id;

    // create and apply aura
    PetBattleAura* aura = new PetBattleAura(this, id, ability, ability, source, source, -1, false);
    source->Auras.push_back(aura);

    aura->OnApply();
}

void PetBattle::AddTeamAura(BattlePet* source, BattlePet* target, uint32 ability, uint32 abilityEffect, int32 duration, uint32 flags, uint8 maxAllowed)
{
    if (!source->IsAlive() || !target->IsAlive())
        return;

    PetBattleTeam* team = Teams[target->GetTeamIndex()];
    if (!team)
        return;

    // count current auras for ability
    uint32 id = 0;
    uint32 auraCount = 0;

    for (PetBattleTeamAura* aura : team->TeamAuras)
    {
        if (aura->GetId() > id)
            id = aura->GetId();

        if (!aura->HasExpired() && aura->GetAbility() == ability)
            auraCount++;
    }

    ++id;

    if (!duration && duration != -1)
        duration++;

    // notify client of aura update
    PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_APPLY, source->GetGlobalIndex(), flags, abilityEffect, TurnInstance++, 0, 1);
    effect.TargetUpdateAura(team->GetGlobalIndex(), id, ability, duration, 0);

    Effects.push_back(effect);

    if ((flags & FailFlags) != 0)
        return;

    // expire auras above the allowed count
    if (maxAllowed && auraCount >= maxAllowed)
    {
        uint8 removeCount = 1 + auraCount - maxAllowed;
        for (PetBattleTeamAura* aura : team->TeamAuras)
        {
            if (!removeCount)
                continue;

            if (!aura->HasExpired() && aura->GetAbility() == ability)
            {
                aura->Expire();
                removeCount--;
            }
        }
    }

    // create and apply aura
    PetBattleTeamAura* aura = new PetBattleTeamAura(this, id, ability, abilityEffect, source, team, duration);
    team->TeamAuras.push_back(aura);

    for (BattlePet* pet : team->BattlePets)
        aura->AddTarget(pet);

    aura->OnApply();
}

void PetBattle::Kill(BattlePet* killer, BattlePet* victim, uint32 abilityEffect, uint32 flags)
{
    UpdatePetState(killer, victim, abilityEffect, BATTLE_PET_STATE_IS_DEAD, 1, flags);

    PetBattleTeam* victimTeam = Teams[victim->GetTeamIndex()];
    if (victimTeam && victimTeam->GetActivePet() == victim)
        victimTeam->ResetActiveAbility();

    // remove any auras the victim has
    for (PetBattleAura* aura : victim->Auras)
        aura->Expire(true);

    m_roundResult = PET_BATTLE_ROUND_RESULT_CATCH_OR_KILL;
}

void PetBattle::Catch(BattlePet* source, BattlePet* target, uint32 abilityEffect)
{
    Kill(source, target, abilityEffect, 0);
    m_cagedPet = target;

    m_roundResult = PET_BATTLE_ROUND_RESULT_CATCH_OR_KILL;
}

void PetBattle::Resurrect(BattlePet* pet, uint32 abilityEffect)
{
    ResurrectedBattlePets.insert(pet->GetGUID());

    UpdatePetState(pet, pet, abilityEffect, BATTLE_PET_STATE_IS_DEAD, 0);

    m_roundResult = PET_BATTLE_ROUND_RESULT_NORMAL;
}

uint8 PetBattle::GetFirstAttackingTeam() const
{
    BattlePet* challangerPet = Teams[PET_BATTLE_CHALLANGER_TEAM]->GetActivePet();
    BattlePet* opponentPet = Teams[PET_BATTLE_OPPONENT_TEAM]->GetActivePet();

    if (!challangerPet || !opponentPet)
        return 0;

    // return random team if active pet speed is the same
    if (challangerPet->GetSpeed() == opponentPet->GetSpeed())
        return Math::Rand(PET_BATTLE_CHALLANGER_TEAM, PET_BATTLE_OPPONENT_TEAM);

    return (challangerPet->GetSpeed() < opponentPet->GetSpeed());
}

PetBattleTeam* PetBattle::GetTeam(ObjectGuid guid) const
{
    for (PetBattleTeam* team : Teams)
        if (team->GetOwner()->GetGUID() == guid)
            return team;

    return nullptr;
}

PetBattleWeather* PetBattle::CreateWeather(uint32 ability, uint32 trigger, BattlePet* caster, int32 duration)
{
    if (m_weather && !m_weather->HasExpired())
    {
        m_weather->ResetWeather();

        // notify client of aura update
        PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_CHANGE, caster->GetGlobalIndex());
        effect.TargetUpdateAura(PET_BATTLE_TARGET_INDEX_WEATHER, m_weather->GetId(), m_weather->GetAbilityId(), m_weather->GetDuration(), m_weather->GetTurn());

        Effects.push_back(effect);
    }
    else
    {
        m_weather = new PetBattleWeather(this, 255, ability, trigger, caster, duration);

        // notify client of aura update
        PetBattleEffect effect(PET_BATTLE_EFFECT_AURA_APPLY, caster->GetGlobalIndex(), 0, trigger, TurnInstance++, 0, 1);
        effect.TargetUpdateAura(PET_BATTLE_TARGET_INDEX_WEATHER, 255, ability, duration, 0);

        Effects.push_back(effect);
    }

    return m_weather;
}

// -------------------------------------------------------------------------------

void PetBattleEffect::TargetActivePet(int8 targetPet)
{
    PetBattleEffectTarget target;
    target.Type   = PET_BATTLE_EFFECT_TARGET_ACTIVE_PET;
    target.Target = targetPet;

    Targets.push_back(target);
}

void PetBattleEffect::TargetUpdateHealth(int8 targetPet, uint32 health)
{
    PetBattleEffectTarget target;
    target.Type   = PET_BATTLE_EFFECT_TARGET_UPDATE_HEALTH;
    target.Target = targetPet;
    target.Health = health;

    Targets.push_back(target);
}

void PetBattleEffect::TargetUpdateState(int8 targetPet, uint32 state, uint32 value)
{
    PetBattleEffectTarget target;
    target.Type          = PET_BATTLE_EFFECT_TARGET_UPDATE_STATE;
    target.Target        = targetPet;
    target.State.StateId = state;
    target.State.Value   = value;

    Targets.push_back(target);
}

void PetBattleEffect::TargetUpdateAura(int8 targetPet, uint32 auraInstance, uint32 ability, int32 duration, uint32 turn)
{
    PetBattleEffectTarget target;
    target.Type                 = PET_BATTLE_EFFECT_TARGET_UPDATE_AURA;
    target.Target               = targetPet;
    target.Aura.AuraInstanceId  = auraInstance;
    target.Aura.AbilityId       = ability;
    target.Aura.CurrentRound    = turn;
    target.Aura.RoundsRemaining = duration;

    Targets.push_back(target);
}

void PetBattleEffect::TargetUpdateStat(int8 targetPet, uint32 value)
{
    PetBattleEffectTarget target;
    target.Type      = PET_BATTLE_EFFECT_TARGET_UPDATE_STAT;
    target.Target    = targetPet;
    target.StatValue = value;

    Targets.push_back(target);
}

void PetBattleEffect::TargetUpdateAbility(int8 targetPet, int32 changedAbilityID, int32 cooldownRemaining, int32 lockdownRemaining)
{
    PetBattleEffectTarget target;
    target.Type = PET_BATTLE_EFFECT_TARGET_UPDATE_ABILITY;
    target.Target = targetPet;
    target.Ability.ChangedAbilityId = changedAbilityID;
    target.Ability.CooldownRemaining = cooldownRemaining;
    target.Ability.LockdownRemaining = lockdownRemaining;

    Targets.push_back(target);
}

// -------------------------------------------------------------------------------

PetBattleSystem::~PetBattleSystem()
{
    m_events.KillAllEvents(false);

    for (auto itr : m_petBattles)
        if (PetBattle* petBattle = itr.second)
            delete petBattle;

    for (PetBattle* petBattle : m_petBattlesToRemove)
        delete petBattle;
}

void PetBattleSystem::Update(uint32 diff)
{
    m_events.Update(diff);

    m_removePetBattlesTimer += diff;

    // remove all pending deletion pet battles
    if (m_removePetBattlesTimer >= PET_BATTLE_SYSTEM_REMOVE_BATTLE_TIMER)
    {
        m_removePetBattlesTimer = 0;

        for (PetBattle* petBattleToRemove : m_petBattlesToRemove)
            Remove(petBattleToRemove);

        m_petBattlesToRemove.clear();
    }

    // update all pet battles
    for (auto petBattleSet : m_petBattles)
    {
        PetBattle* petBattle = petBattleSet.second;
        if (!petBattle)
            continue;

        switch (petBattle->GetPhase())
        {
            // update in progress pet battles
            case PET_BATTLE_PHASE_IN_PROGRESS:
                petBattle->Update(diff);
                break;
            // mark pet battle for deletion
            case PET_BATTLE_PHASE_FINISHED:
                m_petBattlesToRemove.push_back(petBattle);
                break;
        }
    }
}

PetBattle* PetBattleSystem::Create(WorldObject* challenger, WorldObject* opponent, uint8 type)
{
    uint32 battleId = m_globalPetBattleId++;
    PetBattle* petBattle = new PetBattle(battleId, time(nullptr), type);

    // challenger will should only be a player
    PetBattleTeam* challengerTeam = new PetBattleTeam(petBattle, PET_BATTLE_CHALLANGER_TEAM);
    challengerTeam->AddPlayer(challenger->ToPlayer());
    challengerTeam->ResetActiveAbility();

    petBattle->Teams.push_back(challengerTeam);

    // opponent can be another player or a wild battle pet
    PetBattleTeam* opponentTeam = new PetBattleTeam(petBattle, PET_BATTLE_OPPONENT_TEAM);
    opponentTeam->ResetActiveAbility();

    if (type == PET_BATTLE_TYPE_PVE)
    {
        opponentTeam->AddWildBattlePet(opponent->ToCreature());

        // TODO: nearby wild battle pets should join the pet battle as well
        // ...
    }
    else
        opponentTeam->AddPlayer(opponent->ToPlayer());

    petBattle->Teams.push_back(opponentTeam);

    // add players to system
    m_playerPetBattles[challenger->GetGUID()] = battleId;
    if (type != PET_BATTLE_TYPE_PVE)
        m_playerPetBattles[opponent->GetGUID()] = battleId;

    // add pet battle to system
    m_petBattles[battleId] = petBattle;

    return petBattle;
}

void PetBattleSystem::Remove(PetBattle* petBattle)
{
    if (!petBattle)
        return;

    PetBattleMatchmakingTeleportEvent* teleportEvent = nullptr;
    if (petBattle->GetType() == PET_BATTLE_TYPE_PVP_MATCHMAKING)
    {
        teleportEvent = new PetBattleMatchmakingTeleportEvent();
        m_events.AddEvent(teleportEvent, m_events.CalculateTime(PET_BATTLE_MATCHMAKING_TELEPORT_TIMER));
    }

    // remove players from the system
    for (PetBattleTeam const* team : petBattle->Teams)
        if (ObjectGuid guid = team->GetOwnerGuid())
        {
            m_playerPetBattles.erase(guid);

            if (teleportEvent)
            {
                PetBattleMatchmakingTeleportInfo teleportInfo = PetBattleMatchmakingTeleportInfo(guid, team->returnTele);
                teleportEvent->AddPlayerToTeleportList(teleportInfo);
            }
        }

    m_petBattles.erase(petBattle->GetId());

    delete petBattle;
}

void PetBattleSystem::ForfeitBattle(ObjectGuid guid)
{
    // make sure player is currently in a pet battle
    PetBattle* petBattle = GetPlayerPetBattle(guid);
    if (!petBattle)
        return;

    PetBattleTeam* team = petBattle->GetTeam(guid);
    ASSERT(team);

    ForfeitBattle(petBattle, team);
}

void PetBattleSystem::ForfeitBattle(PetBattle* petBattle, PetBattleTeam* team)
{
    if (!petBattle || !team)
        return;

    petBattle->FinishBattle(team, true);
}

PetBattle* PetBattleSystem::GetPlayerPetBattle(ObjectGuid guid) const
{
    // find players pet battle id
    auto playerPetBattle = m_playerPetBattles.find(guid);
    if (playerPetBattle == m_playerPetBattles.end())
        return nullptr;

    // find players pet battle instance
    auto petBattle = m_petBattles.find(playerPetBattle->second);
    if (petBattle == m_petBattles.end())
        return nullptr;

    if (petBattle->second->GetPhase() == PET_BATTLE_PHASE_FINISHED)
        return nullptr;

    return petBattle->second;
}

void PetBattleSystem::AddPetBattleDuelRequest(ObjectGuid guid, PetBattleRequest& request)
{
    _petBattleDuels[guid] = request;
}

void PetBattleSystem::RemovePetBattleDuelRequest(ObjectGuid guid)
{
    _petBattleDuels.erase(guid);
}

bool PetBattleMatchmakingTeleportEvent::Execute(uint64 /*e_time*/, uint32 /*p_time*/)
{
    for (auto itr : _teleportList)
        if (Player* player = ObjectAccessor::FindPlayer(itr.Guid))
        {
            player->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_PACIFIED | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC);
            player->SetVisible(true);
            player->SetControlled(false, UNIT_STATE_ROOT);

            player->TeleportTo(itr.Teleport);
        }

    return true;
}

void PetBattleMatchmakingTeleportEvent::Abort(uint64 /*e_time*/)
{
    //do nothing
}

PetBattleRequest* PetBattleSystem::GetPetBattleDuelRequest(ObjectGuid guid)
{
    auto itr = _petBattleDuels.find(guid);
    if (itr != _petBattleDuels.end())
        return &itr->second;

    return nullptr;
}
