/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PET_H
#define BATTLE_PET_H

#include "BattlePetMgr.h"
#include "Common.h"
#include "Object.h"
#include "Player.h"

#include "Singleton/Singleton.hpp"

// -------------------------------------------------------------------------------
// Battle Pet
// -------------------------------------------------------------------------------

#define BATTLE_PET_MAX_LEVEL        25
#define BATTLE_PET_MAX_NAME_LENGTH  16
#define BATTLE_PET_MAX_ABILITIES    3

// 3-12:  Male
// 13-22: Female
#define BATTLE_PET_MAX_BREED        22

#define BATTLE_PET_MAX_STATES       177
#define BATTLE_PET_MAX_MAIN_STATES  3

#define PET_CAGE                    82800

enum BattlePetStates
{
    BATTLE_PET_STATE_IS_DEAD                            = 1,
    BATTLE_PET_STATE_MAX_HEALTH_BONUS                   = 2,
    BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH            = 3,
    BATTLE_PET_STATE_STAT_KHARMA                        = 4,
    BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL             = 17,
    BATTLE_PET_STATE_STAT_POWER                         = 18,
    BATTLE_PET_STATE_STAT_STAMINA                       = 19,
    BATTLE_PET_STATE_STAT_SPEED                         = 20,
    BATTLE_PET_STATE_MECHANIC_POISONED                  = 21,
    BATTLE_PET_STATE_MECHANIC_STUNNED                   = 22,
    BATTLE_PET_STATE_MOD_DAMAGE_DEALT_PCT               = 23,
    BATTLE_PET_STATE_MOD_DAMAGE_TAKEN_PCT               = 24,
    BATTLE_PET_STATE_MOD_SPEED_PERCENT                  = 25,
    BATTLE_PET_STATE_RAMPING_DAMAGE_ID                  = 26,
    BATTLE_PET_STATE_RAMPING_DAMAGE_USES                = 27,
    BATTLE_PET_STATE_CONDITION_WAS_DAMAGED_THIS_ROUND   = 28,
    BATTLE_PET_STATE_UNTARGETABLE                       = 29,
    BATTLE_PET_STATE_MECHANIC_UNDERGROUND               = 30,
    BATTLE_PET_STATE_LAST_HIT_TAKEN                     = 31,
    BATTLE_PET_STATE_LAST_HIT_DEALT                     = 32,
    BATTLE_PET_STATE_MECHANIC_FLYING                    = 33,
    BATTLE_PET_STATE_MECHANIC_BURNING                   = 34,
    BATTLE_PET_STATE_TURN_LOCK                          = 35,
    BATTLE_PET_STATE_SWAP_OUT_LOCK                      = 36,
    BATTLE_PET_STATE_STAT_CRITICAL_CHANCE               = 40,
    BATTLE_PET_STATE_STAT_ACCURACY                      = 41,
    BATTLE_PET_STATE_PASSIVE_CRITTER                    = 42,
    BATTLE_PET_STATE_PASSIVE_BEAST                      = 43,
    BATTLE_PET_STATE_PASSIVE_HUMANOID                   = 44,
    BATTLE_PET_STATE_PASSIVE_FLYING                     = 45,
    BATTLE_PET_STATE_PASSIVE_DRAGON                     = 46,
    BATTLE_PET_STATE_PASSIVE_ELEMENTAL                  = 47,
    BATTLE_PET_STATE_PASSIVE_MECHANICAL                 = 48,
    BATTLE_PET_STATE_PASSIVE_MAGIC                      = 49,
    BATTLE_PET_STATE_PASSIVE_UNDEAD                     = 50,
    BATTLE_PET_STATE_PASSIVE_AQUATIC                    = 51,
    BATTLE_PET_STATE_MECHANIC_CHILLED                   = 52,
    BATTLE_PET_STATE_WEATHER_BURNT_EARTH                = 53,
    BATTLE_PET_STATE_WEATHER_ARCANE_STORM               = 54,
    BATTLE_PET_STATE_WEATHER_MOONLIGHT                  = 55,
    BATTLE_PET_STATE_WEATHER_DARKNESS                   = 56,
    BATTLE_PET_STATE_WEATHER_SANDSTORM                  = 57,
    BATTLE_PET_STATE_WEATHER_BLIZZARD                   = 58,
    BATTLE_PET_STATE_WEATHER_MUD                        = 59,
    BATTLE_PET_STATE_WEATHER_RAIN                       = 60,
    BATTLE_PET_STATE_WEATHER_SUNLIGHT                   = 61,
    BATTLE_PET_STATE_WEATHER_LIGHTNING_STORM            = 62,
    BATTLE_PET_STATE_WEATHER_WINDY                      = 63,
    BATTLE_PET_STATE_MECHANIC_WEBBED                    = 64,
    BATTLE_PET_STATE_MOD_HEALING_DEALT_PCT              = 65,
    BATTLE_PET_STATE_MOD_HEALING_TAKEN_PCT              = 66,
    BATTLE_PET_STATE_MECHANIC_INVISIBLE                 = 67,
    BATTLE_PET_STATE_UNKILLABLE                         = 68,
    BATTLE_PET_STATE_MECHANIC_IS_OBJECT                 = 69,
    BATTLE_PET_STATE_SPECIAL_PLANT                      = 70,
    BATTLE_PET_STATE_ADD_FLAT_DAMAGE_TAKEN              = 71,
    BATTLE_PET_STATE_ADD_FLAT_DAMAGE_DEALT              = 72,
    BATTLE_PET_STATE_STAT_DODGE                         = 73,
    BATTLE_PET_STATE_SPECIAL_BLOCKED_ATTACK_COUNT       = 74,
    BATTLE_PET_STATE_SPECIAL_OBJECT_REDIRECTION_AURA_ID = 75,
    BATTLE_PET_STATE_MECHANIC_BLEEDING                  = 77,
    BATTLE_PET_STATE_STAT_GENDER                        = 78,
    BATTLE_PET_STATE_MECHANIC_BLIND                     = 82,
    BATTLE_PET_STATE_COSMECTIC_STEALTHED                = 84,
    BATTLE_PET_STATE_WATER_BUBBLED                      = 85,
    BATTLE_PET_STATE_MOD_PET_TYPE_DAMAGE_DEALT_PERCENT  = 87,
    BATTLE_PET_STATE_MOD_PET_TYPE_DAMAGE_TAKEN_PERCENT  = 88,
    BATTLE_PET_STATE_MOD_PET_TYPE_ID                    = 89,
    BATTLE_PET_STATE_INTERNAL_CAPTURE_BOOST             = 90,
    BATTLE_PET_STATE_INTERNAL_EFFECT_SUCCEEDED          = 91,
    BATTLE_PET_STATE_SPECIAL_IS_COCKROACH               = 93,
    BATTLE_PET_STATE_SWAP_IN_LOCK                       = 98,
    BATTLE_PET_STATE_MOD_MAX_HEALTH_PERCENT             = 99,
    BATTLE_PET_STATE_CLONE_ACTIVE                       = 100,
    BATTLE_PET_STATE_CLONE_PBOID                        = 101,
    BATTLE_PET_STATE_CLONE_PET_ABILITY_1                = 102,
    BATTLE_PET_STATE_CLONE_PET_ABILITY_2                = 103,
    BATTLE_PET_STATE_CLONE_PET_ABILITY_3                = 104,
    BATTLE_PET_STATE_CLONE_HEALTH                       = 105,
    BATTLE_PET_STATE_CLONE_MAX_HEALTH                   = 106,
    BATTLE_PET_STATE_CLONE_LAST_ABILITY_ID              = 107,
    BATTLE_PET_STATE_CLONE_LAST_ABILITY_TURN            = 108,
    BATTLE_PET_STATE_IS_CHARGING                        = 113,
    BATTLE_PET_STATE_IS_RECOVERING                      = 114,
    BATTLE_PET_STATE_CLONE_CLONE_ABILITY_ID             = 117,
    BATTLE_PET_STATE_CLONE_CLONE_AURA_ID                = 118,
    BATTLE_PET_STATE_DARK_SIMULACRUM_ABILITY_ID         = 119, // deprecated
    BATTLE_PET_STATE_SPECIAL_CONSUMED_CORPSE            = 120,
    BATTLE_PET_STATE_RAMPING_PBOID                      = 121,
    BATTLE_PET_STATE_REFLECTING                         = 122,
    BATTLE_PET_STATE_SPECIAL_BLOCKED_FRIENDLY_MODE      = 123,
    BATTLE_PET_STATE_SPECIAL_TYPE_OVERRIDE              = 124,
    BATTLE_PET_STATE_MECHANIC_IS_WALL                   = 126,
    BATTLE_PET_STATE_CONDITION_DID_DAMAGE_THIS_ROUND    = 127,
    BATTLE_PET_STATE_COSMETIC_FLY_TIER                  = 128,
    BATTLE_PET_STATE_COSMETIC_FETISH_MASK               = 129,
    BATTLE_PET_STATE_MECHANIC_BOMB                      = 136,
    BATTLE_PET_STATE_SPECIAL_IS_CLEANSING               = 141,
    BATTLE_PET_STATE_COSMETIC_BIGLESSWORTH              = 144,
    BATTLE_PET_STATE_INTERNAL_HEALTH_BEFORE_INSTA_KILL  = 145,
    BATTLE_PET_STATE_RESILITANT                         = 149,
    BATTLE_PET_STATE_PASSIVE_ELITE                      = 153,
    BATTLE_PET_STATE_COSMETIC_CHAOS                     = 158,
    BATTLE_PET_STATE_PASSIVE_BOSS                       = 162,
    BATTLE_PET_STATE_COSMETIC_TREASURE_GOBLIN           = 176
};

enum BattlePetFamily
{
    BATTLE_PET_FAMILY_HUMANOID         = 0,
    BATTLE_PET_FAMILY_DRAGONKIN        = 1,
    BATTLE_PET_FAMILY_FLYING           = 2,
    BATTLE_PET_FAMILY_UNDEAD           = 3,
    BATTLE_PET_FAMILY_CRITTER          = 4,
    BATTLE_PET_FAMILY_MAGIC            = 5,
    BATTLE_PET_FAMILY_ELEMENTAL        = 6,
    BATTLE_PET_FAMILY_BEAST            = 7,
    BATTLE_PET_FAMILY_AQUATIC          = 8,
    BATTLE_PET_FAMILY_MECHANICAL       = 9,
    BATTLE_PET_FAMILY_COUNT
};

enum BattlePetDBState
{
    BATTLE_PET_DB_STATE_NONE            = 0,
    BATTLE_PET_DB_STATE_DELETE          = 1,
    BATTLE_PET_DB_STATE_SAVE            = 2
};

enum BattlePetAchievements
{
    BATTLE_PET_ACHIEVEMENT_NEWBIE       = 7433,
    BATTLE_PET_ACHIEVEMENT_JUST_A_PUP   = 6566
};

struct BattlePetAbility
{
    BattlePetAbility() : AbilityId(0), OnCooldown(false), Cooldown(0), Lockdown(0) { }

    uint32 AbilityId;
    bool OnCooldown;
    uint16 Cooldown;
    uint16 Lockdown;
};

class PetBattleAura;
class PetBattleTeamAura;

typedef std::list<PetBattleAura*> BattlePetAuraStore;
typedef std::list<PetBattleTeamAura*> BattlePetTeamAuraStore;

typedef std::vector<uint32> AvaliableAbilities;

typedef std::vector<PetBattleAura*> AuraRemovalStore;
typedef std::vector<PetBattleTeamAura*> TeamAuraRemovalStore;

typedef std::unordered_map<uint32, BattlePetAuraStore> BattlePetAuraStoreByState;
typedef std::unordered_map<uint32, BattlePetTeamAuraStore> BattlePetTeamAuraStoreByState;

class BattlePet
{
public:
    // new battle pet
    BattlePet(ObjectGuid guid, uint32 speciesId, uint8 family, uint8 level, uint8 quality, uint8 breedId, Player* owner = nullptr)
        : m_guid(guid), m_species(speciesId), m_family(family), m_breed(breedId), m_nickname(""), m_timestamp(0), m_xp(0),
        m_quality(quality), m_flags(0), m_dbState(BATTLE_PET_DB_STATE_SAVE), m_globalIndex(0), m_teamIndex(0), m_owner(owner)
    {
        Initialize(level);
    }

    // load from database
    BattlePet(ObjectGuid guid, uint32 speciesId, uint8 family, std::string nickname, uint32 timestamp,
        uint8 level, uint32 xp, uint32 curHealth, uint8 quality, uint8 breedId, uint16 flags, Player* owner)
        : m_guid(guid), m_species(speciesId), m_family(family), m_nickname(nickname), m_timestamp(timestamp), m_xp(xp),
        m_quality(quality), m_breed(breedId), m_flags(flags), m_dbState(BATTLE_PET_DB_STATE_NONE), m_globalIndex(0), m_teamIndex(0), m_owner(owner)
    {
        Initialize(level, curHealth);
    }

    void Initialize(uint8 level, int32 health = 0);
    void InitializeStates(uint8 level, int32 health, uint8 gender);

    Player* GetOwner() const            { return m_owner;   }
    ObjectGuid GetGUID() const          { return m_guid;    }

    uint32 GetSpecies() const           { return m_species; }

    std::string GetNickname() const     { return m_nickname; }

    uint32 GetTimestamp() const         { return m_timestamp; }
    uint32 GetXp() const                { return m_xp; }

    uint8 GetLevel() const              { return States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL]; }
    uint32 GetCurrentHealth() const     { return States[BATTLE_PET_STATE_INTERNAL_INITIAL_HEALTH]; }
    uint8 GetGender() const             { return States[BATTLE_PET_STATE_STAT_GENDER]; }

    uint8 GetFamily() const;
    uint32 GetPower() const;
    uint32 GetSpeed() const;
    uint32 GetMaxHealth() const;

    uint8 GetQuality() const            { return m_quality; }
    uint8 GetBreed() const              { return m_breed; }

    uint32 GetNpc() const               { return m_npc; }
    uint32 GetDisplayId() const         { return m_displayId; }

    void SetCurrentHealth(uint32 health);
    void SetNickname(std::string nickname);

    void SetTimestamp(uint32 timestamp) { m_timestamp = timestamp; }

    void SetXP(uint32 xpGain);
    void SetLevel(uint8 level);

    void SetQuality(uint8 quality);

    uint8 GetDBState() const            { return m_dbState; }
    void SetDBState(uint8 state)        { m_dbState = state; }

    uint32 GetFlags() const             { return m_flags; }
    bool HasFlag(uint32 flag) const     { return (m_flags & flag) != 0; }
    void SetFlag(uint32 flag);
    void UnSetFlag(uint32 flag);

    void InitializePassiveStates(PetBattle* battle);
    void SetPassiveStateValue(bool set);

    uint32 GetPassiveState() const;
    uint32 GetPassiveAuraID() const;
    int32 GetPassiveStateValue() const;

// -------------------------------------------------------------------------------

    void InitializeAbilities(bool wild);

    void SetBattleAbilities();

    void SetBattleInfo(uint8 teamIndex, uint8 globalPetId)
    {
        m_teamIndex = teamIndex;
        m_globalIndex = globalPetId;
        m_oldLevel = States[BATTLE_PET_STATE_INTERNAL_INITIAL_LEVEL];
    }

    uint8 GetGlobalIndex() const    { return m_globalIndex; }
    uint8 GetTeamIndex() const      { return m_teamIndex; }
    uint16 GetOldLevel() const      { return m_oldLevel; }

    PetBattleAura* GetAura(uint32 abilityId) const;

    void CalculateStats(bool currentHealth = false);

    bool IsMainStatState(uint32 state) const
    {
        return state == BATTLE_PET_STATE_STAT_POWER || state == BATTLE_PET_STATE_STAT_STAMINA || state == BATTLE_PET_STATE_STAT_SPEED;
    }

    bool IsAlive() const
    {
        return States[BATTLE_PET_STATE_IS_DEAD] == 0;
    }

    bool CanAttack() const
    {
        return !States[BATTLE_PET_STATE_TURN_LOCK] && !States[BATTLE_PET_STATE_MECHANIC_STUNNED] && !States[BATTLE_PET_STATE_MECHANIC_WEBBED];
    }

    bool IsTargetable() const
    {
        return States[BATTLE_PET_STATE_UNTARGETABLE] == 0;
    }

    bool IsUnkillable() const
    {
        return States[BATTLE_PET_STATE_UNKILLABLE];
    }

    void ResetMechanicStates();

    std::array<uint32, BATTLE_PET_MAX_ABILITIES> AbilitiyIds;
    std::array<BattlePetAbility*, BATTLE_PET_MAX_ABILITIES> Abilities;

    BattlePetAuraStore Auras;
    BattlePetAuraStoreByState AurasByState;

    uint16 m_oldLevel;

    std::array<int32, BATTLE_PET_MAX_STATES> States;

// -------------------------------------------------------------------------------

private:
    Player* m_owner;
    ObjectGuid m_guid;

    uint32 m_species;
    uint8 m_family;

    std::string m_nickname;

    uint32 m_timestamp;
    uint32 m_xp;

    uint8 m_quality;
    uint8 m_breed;

    uint32 m_flags;

    uint8 m_dbState;

    uint8 m_teamIndex;
    uint8 m_globalIndex;

    uint32 m_npc;
    uint32 m_displayId;

    std::array<int32, BATTLE_PET_MAX_MAIN_STATES> m_mainStates; // used to calculate stats on levelup
};

// -------------------------------------------------------------------------------
// Pet Battle
// -------------------------------------------------------------------------------

class PetBattle;

#define PET_BATTLE_MAX_TEAMS        2
#define PET_BATTLE_MAX_TEAM_PETS    3

#define PET_BATTLE_CHALLANGER_TEAM  0
#define PET_BATTLE_OPPONENT_TEAM    1

#define PET_BATTLE_NULL_PET_INDEX  -1

enum PetBattleRequestReason
{
    PET_BATTLE_REQUEST_FAILED                           = 0,
    PET_BATTLE_REQUEST_NOT_HERE                         = 1,
    PET_BATTLE_REQUEST_NOT_DURING_FLYING                = 2,
    PET_BATTLE_REQUEST_GROUND_NOT_ENOUGH_SMOOTH         = 3,
    PET_BATTLE_REQUEST_AREA_NOT_CLEAR                   = 4,
    PET_BATTLE_REQUEST_ALREADY_IN_COMBAT                = 5,
    PET_BATTLE_REQUEST_DEAD                             = 6,
    PET_BATTLE_REQUEST_SEATED                           = 7,
    PET_BATTLE_REQUEST_NOT_VALID_TARGET                 = 8,
    PET_BATTLE_REQUEST_TOO_FAR                          = 9,
    PET_BATTLE_REQUEST_INVALID_TARGET                   = 10,
    PET_BATTLE_REQUEST_NEED_TO_BE_TRAINER               = 11,
    PET_BATTLE_REQUEST_DECLINED                         = 12,
    PET_BATTLE_REQUEST_ALREADY_IN_PETBATTLE             = 13,
    PET_BATTLE_REQUEST_NEED_PET_IN_SLOTS                = 14,
    PET_BATTLE_REQUEST_PET_ALL_DEAD                     = 15,
    PET_BATTLE_REQUEST_NEED_AT_LEAST_1_PET_IN_SLOT      = 16,
    PET_BATTLE_REQUEST_CODEX_LOCKED_BY_AN_ANOTHER_USER  = 17,
    PET_BATTLE_REQUEST_TARGET_IN_A_BATTLEPET            = 18
};

enum PetBattleAbilityProcType
{
    PET_BATTLE_ABILITY_TURN0_PROC_ON_NONE               = -1,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_APPLY              = 0, // not used
    PET_BATTLE_ABILITY_TURN0_PROC_ON_DAMAGE_TAKEN       = 1,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_DAMAGE_DEALT       = 2,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_HEAL_TAKEN         = 3, // not used
    PET_BATTLE_ABILITY_TURN0_PROC_ON_HEAL_DEALT         = 4, // not used
    PET_BATTLE_ABILITY_TURN0_PROC_ON_AURA_REMOVED       = 5,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_ROUND_START        = 6,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_ROUND_END          = 7,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_TURN               = 8, // not used
    PET_BATTLE_ABILITY_TURN0_PROC_ON_ABILITY            = 9,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_SWAP_IN            = 10,
    PET_BATTLE_ABILITY_TURN0_PROC_ON_SWAP_OUT           = 11
};

enum PetBattleAbilityTargetIndexes
{
    PET_BATTLE_TARGET_INDEX_CHALLENGER_PET_1    = 0,
    PET_BATTLE_TARGET_INDEX_CHALLENGER_PET_2    = 1,
    PET_BATTLE_TARGET_INDEX_CHALLENGER_PET_3    = 2,
    PET_BATTLE_TARGET_INDEX_OPPONENT_PET_1      = 3,
    PET_BATTLE_TARGET_INDEX_OPPONENT_PET_2      = 4,
    PET_BATTLE_TARGET_INDEX_OPPONENT_PET_3      = 5,
    PET_BATTLE_TARGET_INDEX_CHALLENGER_TEAM     = 6,
    PET_BATTLE_TARGET_INDEX_OPPONENT_TEAM       = 7,
    PET_BATTLE_TARGET_INDEX_WEATHER             = 8
};

// -------------------------------------------------------------------------------

// used to calculate pet battle position
struct PetBattleRequest
{
    PetBattleRequest() : LocationResult(21)
    {
        BattlePosition.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
    }

    PetBattleRequest(Position battlePos, Position playerPosition) : LocationResult(21), BattlePosition(battlePos)
    {
        for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; ++i)
            TeamPositions[i] = playerPosition;
    }

    uint32 LocationResult;                              // name dumped from client, use unknown

    Position BattlePosition;
    std::array<Position, PET_BATTLE_MAX_TEAMS> TeamPositions;
};

// -------------------------------------------------------------------------------

// used to track abilities that last multiple turns
struct PetBattleActiveAbility
{
    PetBattleActiveAbility() : AbilityId(0), TurnsPassed(0), TurnsTotal(0) { }

    uint32 AbilityId;
    uint8 TurnsPassed;
    uint8 TurnsTotal;
};

// -------------------------------------------------------------------------------

enum BattlePetsPassiveAuras
{
    BATTLE_PET_PASSIVE_AURA_CRITTER     = 236,
    BATTLE_PET_PASSIVE_AURA_BEAST       = 237,
    BATTLE_PET_PASSIVE_AURA_HUMANOID    = 238,
    BATTLE_PET_PASSIVE_AURA_FLYING      = 239,
    BATTLE_PET_PASSIVE_AURA_AQUATIC     = 240,
    BATTLE_PET_PASSIVE_AURA_ELEMENTAL   = 241,
    BATTLE_PET_PASSIVE_AURA_UNDEAD      = 242,
    BATTLE_PET_PASSIVE_AURA_MAGIC       = 243,
    BATTLE_PET_PASSIVE_AURA_MECHANICAL  = 244,
    BATTLE_PET_PASSIVE_AURA_DRAGONKIN   = 245,

    BATTLE_PET_SPELL_FAILSAFE           = 723,
    BATTLE_PET_SPELL_DAMNED             = 724,
    BATTLE_PET_SPELL_RECOVERY           = 726
};

struct BattlePetsPassiveInfo
{
    BattlePetStates State;
    BattlePetsPassiveAuras Aura;
};

static const BattlePetsPassiveInfo battlePetsPassiveInfoData[BATTLE_PET_FAMILY_COUNT] =
{
    { BATTLE_PET_STATE_PASSIVE_HUMANOID,    BATTLE_PET_PASSIVE_AURA_HUMANOID    },
    { BATTLE_PET_STATE_PASSIVE_DRAGON,      BATTLE_PET_PASSIVE_AURA_DRAGONKIN   },
    { BATTLE_PET_STATE_PASSIVE_FLYING,      BATTLE_PET_PASSIVE_AURA_FLYING      },
    { BATTLE_PET_STATE_PASSIVE_UNDEAD,      BATTLE_PET_PASSIVE_AURA_UNDEAD      },
    { BATTLE_PET_STATE_PASSIVE_CRITTER,     BATTLE_PET_PASSIVE_AURA_CRITTER     },
    { BATTLE_PET_STATE_PASSIVE_MAGIC,       BATTLE_PET_PASSIVE_AURA_MAGIC       },
    { BATTLE_PET_STATE_PASSIVE_ELEMENTAL,   BATTLE_PET_PASSIVE_AURA_ELEMENTAL   },
    { BATTLE_PET_STATE_PASSIVE_BEAST,       BATTLE_PET_PASSIVE_AURA_BEAST       },
    { BATTLE_PET_STATE_PASSIVE_AQUATIC,     BATTLE_PET_PASSIVE_AURA_AQUATIC     },
    { BATTLE_PET_STATE_PASSIVE_MECHANICAL,  BATTLE_PET_PASSIVE_AURA_MECHANICAL  }
};

//              stateId  value
typedef std::unordered_map<uint32, int32> AuraStateStore;

typedef std::vector<BattlePet*> BattlePetStore;

class PetBattleAura
{
public:
    PetBattleAura(PetBattle* petBattle, uint32 id, uint32 ability, uint32 trigger, BattlePet* caster, BattlePet* target, int32 duration, bool expireOnDeath = true)
        : m_id(id), m_ability(ability), m_duration(duration), m_maxDuration(duration), m_caster(caster), m_target(target),
        m_petBattle(petBattle), m_turn(1), m_expired(false), m_expireOnDeath(expireOnDeath) { }

    void SetMaxDuration(int32 Duration) { m_maxDuration = Duration; }

    uint32 GetId() const        { return m_id; }
    uint32 GetAbility() const   { return m_ability; }
    int32 GetDuration() const   { return m_duration; }
    int32 GetTurn() const       { return m_turn; }
    bool HasExpired() const     { return m_expired; }

    void OnApply();
    void OnExpire();
    void Process();
    void Expire(bool OnDeath = false);

    uint8 GetCasterIndex() const { return m_caster->GetGlobalIndex(); }

private:
    uint32 m_id;
    uint32 m_ability;
    uint32 m_trigger;                                   // ability effect that trigger aura
    int32 m_turn;
    int32 m_duration;                                   // duration remaining
    int32 m_maxDuration;

    AuraStateStore m_auraStates;                        // contains states modified by the aura

    BattlePet* m_caster;
    BattlePet* m_target;
    PetBattle* m_petBattle;                             // parent pet battle

    bool m_expired;
    bool m_expireOnDeath;
};

typedef std::unordered_map<int8, AuraStateStore> TeamAurasStatesStore;

class PetBattleTeam;

class PetBattleTeamAura
{
public:
    PetBattleTeamAura(PetBattle* petBattle, uint32 id, uint32 ability, uint32 trigger, BattlePet* caster, PetBattleTeam* team, int32 duration)
        : m_id(id), m_ability(ability), m_duration(duration), m_maxDuration(duration), m_caster(caster), m_team(team),
        m_petBattle(petBattle), m_turn(1), m_expired(false) { }

    void SetMaxDuration(int32 Duration) { m_maxDuration = Duration; }

    uint32 GetId() const { return m_id; }
    uint32 GetAbility() const { return m_ability; }
    int32 GetDuration() const { return m_duration; }
    int32 GetTurn() const { return m_turn; }

    bool HasExpired() const { return m_expired; }

    void OnApply();
    void OnExpire();
    void Process();
    void Expire();

    void AddTarget(BattlePet* pet);

private:
    uint32 m_id;
    uint32 m_ability;
    uint32 m_trigger;                                   // ability effect that trigger aura
    int32 m_turn;
    int32 m_duration;                                   // duration remaining
    int32 m_maxDuration;

    TeamAurasStatesStore m_teamStates;                  // contains states modified by the team auras

    BattlePetStore m_targets;

    BattlePet* m_caster;
    PetBattleTeam* m_team;
    PetBattle* m_petBattle;                             // parent pet battle

    bool m_expired;
};

typedef std::unordered_map<int8, AuraStateStore> WeatherStatesStore;

class PetBattleWeather
{
public:
    PetBattleWeather(PetBattle* petBattle, uint32 id, uint32 ability, uint32 trigger, BattlePet* caster, int32 duration)
        : m_id(id), m_ability(ability), m_duration(duration), m_maxDuration(duration), m_caster(caster), m_petBattle(petBattle), m_turn(1), m_expired(false) { }

    bool HasExpired() const { return m_expired; }

    uint32 GetId() const;
    uint32 GetAbilityId() const;
    int32 GetDuration() const;
    int32 GetTurn() const;

    void OnApply();
    void OnExpire();
    void Process();
    void Expire();

    void ResetWeather();
    void AddTarget(BattlePet* target);

private:
    uint32 m_id;
    uint32 m_ability;
    uint32 m_trigger;                                   // ability effect that trigger weather
    int32 m_turn;
    int32 m_duration;                                   // duration remaining
    int32 m_maxDuration;

    WeatherStatesStore m_weatherStates;                     // contains states modified by the weather

    BattlePetStore m_targets;

    BattlePet* m_caster;
    PetBattle* m_petBattle;                             // parent pet battle

    bool m_expired;
};

// -------------------------------------------------------------------------------

#define PET_BATTLE_MOVE_TYPE_COUNT 5

enum PetBattleMoveType
{
    PET_BATTLE_MOVE_TYPE_REQUEST_LEAVE                  = 0,
    PET_BATTLE_MOVE_TYPE_CAST                           = 1,
    PET_BATTLE_MOVE_TYPE_SWAP_OR_PASS                   = 2,
    PET_BATTLE_MOVE_TYPE_CATCH                          = 3,
    PET_BATTLE_MOVE_TYPE_LEAVE_PETBATTLE                = 4
};

struct PendingRoundMove
{
    PendingRoundMove() : MoveType(0), AbilityId(0), battlePet(nullptr) { }

    uint8 MoveType;
    BattlePet* battlePet;
    uint32 AbilityId;
};

// -------------------------------------------------------------------------------

// TODO: check if there are anymore flags
enum PetBattleTeamInputFlags
{
    PET_BATTLE_TEAM_INPUT_FLAG_NONE                     = 0x00,
    PET_BATTLE_TEAM_INPUT_FLAG_LOCK_ABILITY_1           = 0x01,
    PET_BATTLE_TEAM_INPUT_FLAG_LOCK_ABILITY_2           = 0x02,
    PET_BATTLE_TEAM_INPUT_FLAG_LOCK_PET_SWAP            = 0x04,
    PET_BATTLE_TEAM_INPUT_FLAG_SELECT_NEW_PET           = 0x08
};

class PetBattleTeam
{
public:
    PetBattleTeam(PetBattle* parent, uint8 teamIndex)
        : m_petBattle(parent), m_teamIndex(teamIndex), m_owner(nullptr), m_wildBattlePet(nullptr),
        m_ready(false), m_activePet(nullptr), m_roundTimer(30 * IN_MILLISECONDS), m_nextRoundTimer(30)
    {
        if (teamIndex == PET_BATTLE_CHALLANGER_TEAM)
            m_globalTeamIndex = PET_BATTLE_TARGET_INDEX_CHALLENGER_TEAM;
        else
            m_globalTeamIndex = PET_BATTLE_TARGET_INDEX_OPPONENT_TEAM;
    }

    void Update(uint32 diff);

    Player* GetPlayerOwner() const;

    PetBattle* GetParentBattle() const      { return m_petBattle; }

    Unit* GetOwner() const                  { return m_owner; }

    ObjectGuid GetOwnerGuid() const         { return m_ownerGuid; }

    Creature* GetWildBattlePet() const      { return m_wildBattlePet; }

    uint8 GetTeamIndex() const              { return m_teamIndex; }

    uint8 ConvertToGlobalIndex(uint8 localPetIndex) const;
    uint8 ConvertToLocalIndex(uint8 globalPetIndex) const;

    BattlePet* GetActivePet() const         { return m_activePet; }
    void SetActivePet(BattlePet* battlePet) { m_activePet = battlePet; }

    bool IsValidBattlePet(BattlePet* battlePet) const;

    void GetAvaliablePets(BattlePetStore& avaliablePets) const;
    bool CanSwap(BattlePet* battlePet = nullptr, bool ignoreAlive = false) const;

    bool HasMultipleTurnAbility() const;

    bool IsReady() const        { return m_ready; }
    void IsReady(bool ready)    { m_ready = ready; }

    PendingRoundMove GetPendingMove() const { return m_pendingMove; }
    void SetPendingMove(uint8 moveType, uint32 abilityId, BattlePet* newActivePet);

    void ActivePetPrepareCast(uint32 abilityId);
    bool CanActivePetCast(uint32 abilityId) const;

    void DoCasts(int8 procType = PET_BATTLE_ABILITY_TURN0_PROC_ON_NONE);

    uint8 GetTrapStatus() const;

    void AddPlayer(Player* player);
    void AddWildBattlePet(Creature* creature);

    void ResetActiveAbility();

    void ProcessAuras();
    void RemoveExpiredAuras();

    uint8 GetInputStatusFlags() const;

    BattlePetStore BattlePets;
    GuidUnorderedSet SeenAction;            // contains battle pets that have contributed to the battle

    PetBattleActiveAbility ActiveAbility;

    WorldLocation returnTele;

    void AddRoundTimer(uint32 abilitiesCount);
    uint32 GetNextRoundTimer() const { return m_nextRoundTimer; }

    uint8 GetGlobalIndex() const { return m_globalTeamIndex; }

    PetBattleTeamAura* GetTeamAura(uint32 abilityId) const;

    BattlePetTeamAuraStore TeamAuras;
    BattlePetTeamAuraStoreByState TeamAurasByState;

private:
    PetBattle* m_petBattle;

    Unit* m_owner;

    ObjectGuid m_ownerGuid;

    Creature* m_wildBattlePet;

    BattlePet* m_activePet;

    uint8 m_teamIndex;                                  // 0 = challanger team, 1 = opponent team
    uint8 m_globalTeamIndex;

    bool m_ready;

    PendingRoundMove m_pendingMove;

    uint32 m_roundTimer;
    uint32 m_nextRoundTimer;
};

// -------------------------------------------------------------------------------

enum PetBattleEffectTargetType
{
    PET_BATTLE_EFFECT_TARGET_UPDATE_STAT        = 0,
    PET_BATTLE_EFFECT_TARGET_UPDATE_STATE       = 1,
    PET_BATTLE_EFFECT_TARGET_UPDATE_ABILITY     = 2,
    PET_BATTLE_EFFECT_TARGET_UPDATE_HEALTH      = 3,
    PET_BATTLE_EFFECT_TARGET_ACTIVE_PET         = 4,
    PET_BATTLE_EFFECT_TARGET_TRIGGER_ABILITY    = 5,
    PET_BATTLE_EFFECT_TARGET_NPC_EMOTE          = 6,
    PET_BATTLE_EFFECT_TARGET_UPDATE_AURA        = 7
};

struct PetBattleEffectTarget
{
    PetBattleEffectTarget() : Type(0), Target(0) { }

    uint8 Type;
    int8 Target;

    union
    {
        uint32 Health;
        uint32 StatValue;
        uint32 TriggerAbilityId;
        uint32 BroadcastTextId;

        struct
        {
            uint32 AuraInstanceId;
            uint32 AbilityId;
            int32 RoundsRemaining;
            uint32 CurrentRound;
        } Aura;

        struct
        {
            uint32 StateId;
            uint32 Value;
        } State;

        struct
        {
            uint32 ChangedAbilityId;
            uint32 CooldownRemaining;
            uint32 LockdownRemaining;
        } Ability;
    };
};

// -------------------------------------------------------------------------------

typedef std::vector<PetBattleEffectTarget> PetBattleEffectTargetStore;

enum PetBattleEffectType
{
    PET_BATTLE_EFFECT_SET_HEALTH            = 0,
    PET_BATTLE_EFFECT_AURA_APPLY            = 1,
    PET_BATTLE_EFFECT_AURA_REMOVE           = 2,
    PET_BATTLE_EFFECT_AURA_CHANGE           = 3,
    PET_BATTLE_EFFECT_ACTIVE_PET            = 4,
    PET_BATTLE_EFFECT_CATCH                 = 5,
    PET_BATTLE_EFFECT_SET_STATE             = 6,
    PET_BATTLE_EFFECT_SET_MAX_HEALTH        = 7,
    PET_BATTLE_EFFECT_SET_SPEED             = 8,
    PET_BATTLE_EFFECT_SET_POWER             = 9,
    PET_BATTLE_EFFECT_TRIGGER_ABILITY       = 10,
    PET_BATTLE_EFFECT_ABILITY_CHANGE        = 11,
    PET_BATTLE_EFFECT_NPC_EMOTE             = 12,
    PET_BATTLE_EFFECT_AURA_PROCESSING_BEGIN = 13,
    PET_BATTLE_EFFECT_AURA_PROCESSING_END   = 14
};

enum PetBattleEffectFlags
{
    PET_BATTLE_EFFECT_FLAG_SKIP_TURN        = 0x00001,
    PET_BATTLE_EFFECT_FLAG_MISS             = 0x00002,
    PET_BATTLE_EFFECT_FLAG_CRITICAL         = 0x00004,
    PET_BATTLE_EFFECT_FLAG_BLOCKED          = 0x00008,
    PET_BATTLE_EFFECT_FLAG_DODGE            = 0x00010,
    PET_BATTLE_EFFECT_FLAG_HEAL             = 0x00020,
    PET_BATTLE_EFFECT_FLAG_UNKILLABLE       = 0x00040,
    PET_BATTLE_EFFECT_FLAG_REFLECT          = 0x00080,
    PET_BATTLE_EFFECT_FLAG_ABSORB           = 0x00100,
    PET_BATTLE_EFFECT_FLAG_IMMUNE           = 0x00200,
    PET_BATTLE_EFFECT_FLAG_STRONG           = 0x00400,
    PET_BATTLE_EFFECT_FLAG_WEAK             = 0x00800,
    PET_BATTLE_EFFECT_FLAG_KILL             = 0x01000   // unknown
};

class PetBattleEffect
{
public:
    PetBattleEffect(uint8 type, int8 source = PET_BATTLE_NULL_PET_INDEX, uint32 flags = 0, uint32 abilityEffect = 0,
        uint16 turnInstance = 0, uint16 auraInstance = 0, uint8 stackDepth = 0)
        : m_type(type), m_source(source), m_flags(flags), m_abilityEffect(abilityEffect), m_turnInstance(turnInstance),
        m_auraInstance(auraInstance), m_stackDepth(stackDepth) { }

    uint8 GetType() const           { return m_type; }
    int8 GetSource() const          { return m_source; }
    uint32 GetFlags() const         { return m_flags; }
    uint32 GetAbilityEffect() const { return m_abilityEffect; }
    uint16 GetTurnInstance() const  { return m_turnInstance; }
    uint16 GetAuraInstance() const  { return m_auraInstance; }
    uint8 GetStackDepth() const     { return m_stackDepth; }

    void TargetActivePet(int8 target = PET_BATTLE_NULL_PET_INDEX);
    void TargetUpdateHealth(int8 target, uint32 health);
    void TargetUpdateState(int8 target, uint32 state, uint32 value);
    void TargetUpdateAura(int8 targetPet, uint32 auraInstance, uint32 ability, int32 duration, uint32 turn);
    void TargetUpdateStat(int8 targetPet, uint32 value);
    void TargetUpdateAbility(int8 targetPet, int32 changedAbilityID, int32 cooldownRemaining, int32 lockdownRemaining);

    PetBattleEffectTargetStore Targets;

private:
    uint8 m_type;
    uint16 m_flags;
    int8 m_source;
    uint32 m_abilityEffect;
    uint16 m_turnInstance;
    uint16 m_auraInstance;
    uint8 m_stackDepth;
};

// -------------------------------------------------------------------------------

enum PetBattleType
{
    PET_BATTLE_TYPE_PVE                     = 0,
    PET_BATTLE_TYPE_PVP_DUEL                = 1,
    PET_BATTLE_TYPE_PVP_MATCHMAKING         = 2
};

// internal values used to track battle phase
enum PetBattlePhase
{
    PET_BATTLE_PHASE_SETUP                  = 0,
    PET_BATTLE_PHASE_IN_PROGRESS            = 1,
    PET_BATTLE_PHASE_FINISHED               = 2
};

enum PetBattleRoundResult
{
    PET_BATTLE_ROUND_RESULT_NONE            = 0,
    PET_BATTLE_ROUND_RESULT_INITIALIZE      = 1,
    PET_BATTLE_ROUND_RESULT_NORMAL          = 2,
    PET_BATTLE_ROUND_RESULT_CATCH_OR_KILL   = 3
};

enum PetBattleTrapStatus
{
    PET_BATTLE_TRAP_STATUS_DISABLED             = 0,
    PET_BATTLE_TRAP_STATUS_ENABLED              = 1,
    PET_BATTLE_TRAP_STATUS_CANT_TRAP_DEAD_PET   = 3,
    PET_BATTLE_TRAP_STATUS_HEALTH_TOO_HIGH      = 4,
    PET_BATTLE_TRAP_STATUS_TOO_MANY_PETS        = 5,
    PET_BATTLE_TRAP_STATUS_NOT_CAPTURABLE       = 6,
    PET_BATTLE_TRAP_STATUS_CANT_TRAP_NPC_PET    = 7,
    PET_BATTLE_TRAP_STATUS_ALREADY_TRAPPED      = 8
};

typedef std::vector<PetBattleEffect> PetBattleEffectStore;
typedef std::vector<PetBattleTeam*> PetBattleTeamStore;
typedef std::vector<BattlePet*> ActivePetStore;

class PetBattle
{
public:
    PetBattle(uint32 battleId, uint32 timestamp, uint8 type)
        : m_battleId(battleId), m_timestamp(timestamp), m_type(type), m_phase(PET_BATTLE_PHASE_SETUP), m_round(0), TurnInstance(0),
        m_winningTeam(nullptr), m_weather(nullptr), m_roundResult(PET_BATTLE_ROUND_RESULT_NONE), m_cagedPet(nullptr),
        m_timeSinceLastRound(0), m_preBattleCount(0), m_isProccededRound(false) { }

    ~PetBattle();

    void StartBattle();
    void FinishBattle(PetBattleTeam* team, bool forfeit = false);
    void HandleRound();
    void Update(uint32 diff);

    bool Cast(BattlePet* caster, uint32 abilityId, uint8 turn, int8 procType);

    void SwapActivePet(PetBattleTeam* team, BattlePet* battlePet);
    void UpdatePetState(BattlePet* source, BattlePet* target, uint32 abilityEffect, uint32 state, int32 value, uint32 flags = 0);
    void AddAura(BattlePet* source, BattlePet* target, uint32 ability, uint32 abilityEffect, int32 duration, uint32 flags = 0, uint8 maxAllowed = 0);
    void AddInitPassiveAura(BattlePet* source, uint32 ability);
    void Kill(BattlePet* killer, BattlePet* victim, uint32 abilityEffect, uint32 flags);
    void Catch(BattlePet* source, BattlePet* target, uint32 abilityEffect);
    void Resurrect(BattlePet* pet, uint32 abilityEffect);

    uint8 GetFirstAttackingTeam() const;
    PetBattleTeam const* GetWinningTeam() const { return m_winningTeam; }

    uint32 GetId() const                    { return m_battleId; }
    uint8 GetType() const                   { return m_type; }
    uint32 GetRound() const                 { return m_round; }
    uint8 GetPhase() const                  { return m_phase; }
    uint8 GetRoundResult() const            { return m_roundResult; }
    BattlePet* GetCagedPet() const          { return m_cagedPet; }

    PetBattleTeam* GetTeam(ObjectGuid guid) const;

    uint8 GetTeamIndex(uint8 globalPetId)
    {
        return globalPetId < PET_BATTLE_MAX_TEAM_PETS ? PET_BATTLE_CHALLANGER_TEAM : PET_BATTLE_OPPONENT_TEAM;
    }

    uint32 TurnInstance;                                // dictates the order of spells and effects each round
    PetBattleTeamStore Teams;                           // teams participating in the battle
    PetBattleEffectStore Effects;                       // current round effects

    uint32 AddPreBattleCount() { return ++m_preBattleCount; }

    GuidSet ResurrectedBattlePets;

    PetBattleWeather* CreateWeather(uint32 ability, uint32 trigger, BattlePet* caster, int32 duration);

    void AddTeamAura(BattlePet* source, BattlePet* target, uint32 ability, uint32 abilityEffect, int32 duration, uint32 flags = 0, uint8 maxAllowed = 0);

private:
    uint32 m_battleId;                                  // global id of the pet battle
    uint32 m_timestamp;                                 // timestamp when pet battle started

    uint8 m_type;
    uint8 m_phase;
    uint32 m_round;
    uint8 m_roundResult;
    BattlePet* m_cagedPet;

    PetBattleWeather* m_weather;

    // different from team timers
    uint32 m_timeSinceLastRound;

    PetBattleTeam* m_winningTeam;

    uint8 m_preBattleCount;

    bool m_isProccededRound;
};

#define PET_BATTLE_MATCHMAKING_TELEPORT_TIMER 5 * IN_MILLISECONDS

struct PetBattleMatchmakingTeleportInfo
{
    PetBattleMatchmakingTeleportInfo(ObjectGuid& guid, WorldLocation const& loc) : Guid(guid), Teleport(loc) { }

    ObjectGuid Guid;
    WorldLocation Teleport;
};

class PetBattleMatchmakingTeleportEvent : public BasicEvent
{
    public:
        PetBattleMatchmakingTeleportEvent() { }

        virtual bool Execute(uint64 e_time, uint32 p_time) override;
        virtual void Abort(uint64 e_time) override;

        void AddPlayerToTeleportList(PetBattleMatchmakingTeleportInfo& teleportInfo)
        {
            _teleportList.push_back(teleportInfo);
        }

    private:
        std::vector<PetBattleMatchmakingTeleportInfo> _teleportList;
};

// -------------------------------------------------------------------------------

#define PET_BATTLE_SYSTEM_UPDATE_BATTLE_TIMER 200       // update pet battles every 200 miliseconds
#define PET_BATTLE_SYSTEM_REMOVE_BATTLE_TIMER 1000      // remove old pet battles every second

//                       petBattleId
typedef std::unordered_map<uint32, PetBattle*> PetBattleStore;
//                       playerGuid  petBattleId
typedef std::unordered_map<ObjectGuid, uint32> PlayerPetBattleStore;

typedef std::vector<PetBattle*> PetBattleRemoveStore;

typedef std::unordered_map<ObjectGuid, PetBattleRequest> PetBattleDuelStore;

class PetBattleSystem
{
public:
    PetBattleSystem() : m_globalPetBattleId(0), m_removePetBattlesTimer(0) { }

    ~PetBattleSystem();

    void Update(uint32 diff);

    PetBattle* Create(WorldObject* challenger, WorldObject* opponent, uint8 type);
    void Remove(PetBattle* petBattle);

    void ForfeitBattle(ObjectGuid guid);
    void ForfeitBattle(PetBattle* petBattle, PetBattleTeam* team);

    PetBattle* GetPlayerPetBattle(ObjectGuid guid) const;

    void AddPetBattleDuelRequest(ObjectGuid guid, PetBattleRequest& request);
    void RemovePetBattleDuelRequest(ObjectGuid guid);
    PetBattleRequest* GetPetBattleDuelRequest(ObjectGuid guid);

private:

    // Event handler
    EventProcessor m_events;

    uint32 m_globalPetBattleId;
    PetBattleStore m_petBattles;                        // contains battle ids to pet battle instances
    PlayerPetBattleStore m_playerPetBattles;            // contains player GUIDs to battle ids

    uint32 m_removePetBattlesTimer;
    PetBattleRemoveStore m_petBattlesToRemove;

    PetBattleDuelStore _petBattleDuels;
};

#define sPetBattleSystem Tod::Singleton<PetBattleSystem>::GetSingleton()

#endif
