/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITYCORE_PET_DEFINES_H
#define TRINITYCORE_PET_DEFINES_H

enum PetType
{
    SUMMON_PET              = 0,
    HUNTER_PET              = 1,
    MAX_PET_TYPE            = 4
};

// Stored in character_pet.slot
enum PetSlot
{
    // Some not-in-db slots.
    PET_SLOT_LOG_OUT            =   -5,     // Used on player's logout only
    PET_SLOT_FULL_LIST          =   -4,     // Used when there is no slot free for taming.
    PET_SLOT_SPECIAL_SLOT       =   -3,     // Used in some scripts.

    PET_SLOT_ACTUAL_PET_SLOT    =   -2,     // Save the pet in it's actual slot.
    PET_SLOT_DELETED            =   -1,     // Delete the pet.

    // Hunter pet slots, sent to client at stabling / unstabling pets.
    PET_SLOT_HUNTER_FIRST       =   0,      // PetType == HUNTER_PET
    PET_SLOT_HUNTER_LAST        =   4,      // PetType == HUNTER_PET

    PET_SLOT_STABLE_FIRST       =   5,
    PET_SLOT_STABLE_LAST        =   54,

    // Non-hunter pet slot.
    PET_SLOT_OTHER_PET          = 100         // PetType != HUNTER_PET
};

enum PetStableinfo
{
    PET_STABLE_NONE     = 0,
    PET_STABLE_ACTIVE   = 1,
    PET_STABLE_INACTIVE = 2
};

enum PetSpellState
{
    PETSPELL_UNCHANGED = 0,
    PETSPELL_CHANGED   = 1,
    PETSPELL_NEW       = 2,
    PETSPELL_REMOVED   = 3
};

enum PetSpellType
{
    PETSPELL_NORMAL = 0,
    PETSPELL_FAMILY = 1,
    PETSPELL_TALENT = 2
};

enum ActionFeedback
{
    FEEDBACK_NONE            = 0,
    FEEDBACK_PET_DEAD        = 1,
    FEEDBACK_NOTHING_TO_ATT  = 2,
    FEEDBACK_CANT_ATT_TARGET = 3
};

enum PetTalk
{
    PET_TALK_SPECIAL_SPELL  = 0,
    PET_TALK_ATTACK         = 1
};

enum StableResultCode
{
    STABLE_ERR_NONE         = 0x00,                         // Does nothing, just resets stable states.
    STABLE_ERR_MONEY        = 0x01,                         // "You don't have enough money"
    STABLE_ERR_INVALID_SLOT = 0x03,                         // "That slot is locked"
    STABLE_SUCCESS_STABLE   = 0x08,                         // Stable success
    STABLE_SUCCESS_UNSTABLE = 0x09,                         // Unstable / Swap success
    STABLE_SUCCESS_BUY_SLOT = 0x0A,                         // Buy slot success
    STABLE_ERR_EXOTIC       = 0x0B,                         // "You are unable to control exotic creatures"
    STABLE_ERR_STABLE       = 0x0C                          // "Internal pet error"
};

enum PetTameResult
{
    PET_TAME_ERROR_UNKNOWN_ERROR            = 0,
    PET_TAME_ERROR_INVALID_CREATURE         = 1,
    PET_TAME_ERROR_TOO_MANY_PETS            = 2,
    PET_TAME_ERROR_CREATURE_ALREADY_OWNED   = 3,
    PET_TAME_ERROR_NOT_TAMEABLE             = 4,
    PET_TAME_ERROR_ANOTHER_SUMMON_ACTIVE    = 5,
    PET_TAME_ERROR_YOU_CANT_TAME            = 6,
    PET_TAME_ERROR_NO_PET_AVAILABLE         = 7,
    PET_TAME_ERROR_INTERNAL_ERROR           = 8,
    PET_TAME_ERROR_TOO_HIGH_LEVEL           = 9,
    PET_TAME_ERROR_DEAD                     = 10,
    PET_TAME_ERROR_NOT_DEAD                 = 11,
    PET_TAME_ERROR_CANT_CONTROL_EXOTIC      = 12,
    PET_TAME_ERROR_INVALID_SLOT             = 13
};

// Stable Slots.
#define MAX_PET_STABLES         PET_SLOT_STABLE_LAST

// Movement defines.
#define PET_FOLLOW_DIST  1.0f
#define PET_FOLLOW_ANGLE float(M_PI/2)

#endif
