/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_UPDATEFIELDS_H
#define SF_UPDATEFIELDS_H

// Auto generated for version 5.4.8 18414

enum EObjectFields
{
    OBJECT_GUID                                     = 0x0, // Size: 2, Flags: UF_FLAG_PUBLIC
    OBJECT_DATA                                     = 0x2, // Size: 2, Flags: UF_FLAG_PUBLIC
    OBJECT_TYPE                                     = 0x4, // Size: 1, Flags: UF_FLAG_PUBLIC
    OBJECT_ENTRY                                    = 0x5, // Size: 1, Flags: UF_FLAG_VIEWER_DEPENDENT
    OBJECT_DYNAMIC_FLAGS                            = 0x6, // Size: 1, Flags: UF_FLAG_VIEWER_DEPENDENT, UF_FLAG_URGENT
    OBJECT_SCALE                                    = 0x7, // Size: 1, Flags: UF_FLAG_PUBLIC
    OBJECT_END                                      = 0x8
};

enum ObjectDynamicFields
{
    OBJECT_DYNAMIC_END                              = 0x000
};

enum EItemFields
{
    ITEM_OWNER                                      = OBJECT_END + 0x00, // Size: 2, Flags: UF_FLAG_PUBLIC
    ITEM_CONTAINED_IN                               = OBJECT_END + 0x02, // Size: 2, Flags: UF_FLAG_PUBLIC
    ITEM_CREATOR                                    = OBJECT_END + 0x04, // Size: 2, Flags: UF_FLAG_PUBLIC
    ITEM_GIFT_CREATOR                               = OBJECT_END + 0x06, // Size: 2, Flags: UF_FLAG_PUBLIC
    ITEM_STACK_COUNT                                = OBJECT_END + 0x08, // Size: 1, Flags: UF_FLAG_OWNER
    ITEM_EXPIRATION                                 = OBJECT_END + 0x09, // Size: 1, Flags: UF_FLAG_OWNER
    ITEM_SPELL_CHARGES                              = OBJECT_END + 0x0A, // Size: 5, Flags: UF_FLAG_OWNER
    ITEM_FLAGS                                      = OBJECT_END + 0x0F, // Size: 1, Flags: UF_FLAG_PUBLIC
    ITEM_ENCHANTMENT                                = OBJECT_END + 0x10, // Size: 39, Flags: UF_FLAG_PUBLIC
    ITEM_PROPERTY_SEED                              = OBJECT_END + 0x37, // Size: 1, Flags: UF_FLAG_PUBLIC
    ITEM_RANDOM_PROPERTIES_ID                       = OBJECT_END + 0x38, // Size: 1, Flags: UF_FLAG_PUBLIC
    ITEM_DURABILITY                                 = OBJECT_END + 0x39, // Size: 1, Flags: UF_FLAG_OWNER
    ITEM_MAX_DURABILITY                             = OBJECT_END + 0x3A, // Size: 1, Flags: UF_FLAG_OWNER
    ITEM_CREATE_PLAYED_TIME                         = OBJECT_END + 0x3B, // Size: 1, Flags: UF_FLAG_PUBLIC
    ITEM_MODIFIERS_MASK                             = OBJECT_END + 0x3C, // Size: 1, Flags: UF_FLAG_OWNER
    ITEM_END                                        = OBJECT_END + 0x3D
};

enum ItemDynamicFields
{
    ITEM_DYNAMIC_MODIFIERS                          = OBJECT_DYNAMIC_END + 0x0, // Size: 1, Flags: UF_FLAG_OWNER
    ITEM_DYNAMIC_END                                = OBJECT_DYNAMIC_END + 0x1
};

enum EContainerFields
{
    CONTAINER_SLOTS                                 = ITEM_END + 0x00, // Size: 72, Flags: UF_FLAG_PUBLIC
    CONTAINER_NUM_SLOTS                             = ITEM_END + 0x48, // Size: 1, Flags: UF_FLAG_PUBLIC
    CONTAINER_END                                   = ITEM_END + 0x49
};

enum ContainerDynamicFields
{
    CONTAINER_DYNAMIC_END                           = ITEM_DYNAMIC_END + 0x000
};

enum EUnitFields
{
    UNIT_CHARM                                      = OBJECT_END + 0x00, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_SUMMON                                     = OBJECT_END + 0x02, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_CRITTER                                    = OBJECT_END + 0x04, // Size: 2, Flags: UF_FLAG_PRIVATE
    UNIT_CHARMED_BY                                 = OBJECT_END + 0x06, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_SUMMONED_BY                                = OBJECT_END + 0x08, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_CREATED_BY                                 = OBJECT_END + 0x0A, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_DEMON_CREATOR                              = OBJECT_END + 0x0C, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_TARGET                                     = OBJECT_END + 0x0E, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_BATTLE_PET_COMPANION_GUID                  = OBJECT_END + 0x10, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_CHANNEL_OBJECT                             = OBJECT_END + 0x12, // Size: 2, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    UNIT_CHANNEL_SPELL                              = OBJECT_END + 0x14, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    UNIT_SUMMONED_BY_HOME_REALM                     = OBJECT_END + 0x15, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_BYTES_0                                    = OBJECT_END + 0x16, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_DISPLAY_POWER                              = OBJECT_END + 0x17, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_OVERRIDE_DISPLAY_POWER_ID                  = OBJECT_END + 0x18, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_HEALTH                                     = OBJECT_END + 0x19, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_POWER                                      = OBJECT_END + 0x1A, // Size: 5, Flags: UF_FLAG_PUBLIC
    UNIT_MAX_HEALTH                                 = OBJECT_END + 0x1F, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MAX_POWER                                  = OBJECT_END + 0x20, // Size: 5, Flags: UF_FLAG_PUBLIC
    UNIT_POWER_REGEN_FLAT_MODIFIER                  = OBJECT_END + 0x25, // Size: 5, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_UNIT_ALL
    UNIT_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER      = OBJECT_END + 0x2A, // Size: 5, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_UNIT_ALL
    UNIT_LEVEL                                      = OBJECT_END + 0x2F, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_EFFECTIVE_LEVEL                            = OBJECT_END + 0x30, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_FACTION_TEMPLATE                           = OBJECT_END + 0x31, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_VIRTUAL_ITEM_ID                            = OBJECT_END + 0x32, // Size: 3, Flags: UF_FLAG_PUBLIC
    UNIT_FLAGS                                      = OBJECT_END + 0x35, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_FLAGS2                                     = OBJECT_END + 0x36, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_AURA_STATE                                 = OBJECT_END + 0x37, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_ATTACK_ROUND_BASE_TIME                     = OBJECT_END + 0x38, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_RANGED_ATTACK_ROUND_BASE_TIME              = OBJECT_END + 0x3A, // Size: 1, Flags: UF_FLAG_PRIVATE
    UNIT_BOUNDING_RADIUS                            = OBJECT_END + 0x3B, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_COMBAT_REACH                               = OBJECT_END + 0x3C, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_DISPLAY_ID                                 = OBJECT_END + 0x3D, // Size: 1, Flags: UF_FLAG_VIEWER_DEPENDENT, UF_FLAG_URGENT
    UNIT_NATIVE_DISPLAY_ID                          = OBJECT_END + 0x3E, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    UNIT_MOUNT_DISPLAY_ID                           = OBJECT_END + 0x3F, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    UNIT_MIN_DAMAGE                                 = OBJECT_END + 0x40, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_EMPATH
    UNIT_MAX_DAMAGE                                 = OBJECT_END + 0x41, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_EMPATH
    UNIT_MIN_OFF_HAND_DAMAGE                        = OBJECT_END + 0x42, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_EMPATH
    UNIT_MAX_OFF_HAND_DAMAGE                        = OBJECT_END + 0x43, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_EMPATH
    UNIT_BYTES_1                                    = OBJECT_END + 0x44, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_PET_NUMBER                                 = OBJECT_END + 0x45, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_PET_NAME_TIMESTAMP                         = OBJECT_END + 0x46, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_PET_EXPERIENCE                             = OBJECT_END + 0x47, // Size: 1, Flags: UF_FLAG_OWNER
    UNIT_PET_NEXT_LEVEL_EXPERIENCE                  = OBJECT_END + 0x48, // Size: 1, Flags: UF_FLAG_OWNER
    UNIT_MOD_CASTING_SPEED                          = OBJECT_END + 0x49, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MOD_SPELL_HASTE                            = OBJECT_END + 0x4A, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MOD_HASTE                                  = OBJECT_END + 0x4B, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MOD_RANGED_HASTE                           = OBJECT_END + 0x4C, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MOD_HASTE_REGEN                            = OBJECT_END + 0x4D, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_CREATED_BY_SPELL                           = OBJECT_END + 0x4E, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_NPC_FLAGS                                  = OBJECT_END + 0x4F, // Size: 2, Flags: UF_FLAG_PUBLIC
    UNIT_EMOTE_STATE                                = OBJECT_END + 0x51, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_STATS                                      = OBJECT_END + 0x52, // Size: 5, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_STAT_POS_BUFF                              = OBJECT_END + 0x57, // Size: 5, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_STAT_NEG_BUFF                              = OBJECT_END + 0x5C, // Size: 5, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_RESISTANCES                                = OBJECT_END + 0x61, // Size: 7, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER, UF_FLAG_EMPATH
    UNIT_RESISTANCE_BUFF_MODS_POSITIVE              = OBJECT_END + 0x68, // Size: 7, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_RESISTANCE_BUFF_MODS_NEGATIVE              = OBJECT_END + 0x6F, // Size: 7, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_BASE_MANA                                  = OBJECT_END + 0x76, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_BASE_HEALTH                                = OBJECT_END + 0x77, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_BYTES_2                                    = OBJECT_END + 0x78, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_ATTACK_POWER                               = OBJECT_END + 0x79, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_ATTACK_POWER_MOD_POS                       = OBJECT_END + 0x7A, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_ATTACK_POWER_MOD_NEG                       = OBJECT_END + 0x7B, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_ATTACK_POWER_MULTIPLIER                    = OBJECT_END + 0x7C, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_RANGED_ATTACK_POWER                        = OBJECT_END + 0x7D, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_RANGED_ATTACK_POWER_MOD_POS                = OBJECT_END + 0x7E, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_RANGED_ATTACK_POWER_MOD_NEG                = OBJECT_END + 0x7F, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_RANGED_ATTACK_POWER_MULTIPLIER             = OBJECT_END + 0x80, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_MIN_RANGED_DAMAGE                          = OBJECT_END + 0x81, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_MAX_RANGED_DAMAGE                          = OBJECT_END + 0x82, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_POWER_COST_MODIFIER                        = OBJECT_END + 0x83, // Size: 7, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_POWER_COST_MULTIPLIER                      = OBJECT_END + 0x8A, // Size: 7, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_MAX_HEALTH_MODIFIER                        = OBJECT_END + 0x91, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_OWNER
    UNIT_HOVER_HEIGHT                               = OBJECT_END + 0x92, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MIN_ITEM_LEVEL                             = OBJECT_END + 0x93, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_MAX_ITEM_LEVEL                             = OBJECT_END + 0x94, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_WILD_BATTLE_PET_LEVEL                      = OBJECT_END + 0x95, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_BATTLE_PET_COMPANION_NAME_TIMESTAMP        = OBJECT_END + 0x96, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_INTERACT_SPELL_ID                          = OBJECT_END + 0x97, // Size: 1, Flags: UF_FLAG_PUBLIC
    UNIT_END                                        = OBJECT_END + 0x98
};

enum UnitDynamicFields
{
    UNIT_DYNAMIC_PASSIVE_SPELLS                     = OBJECT_DYNAMIC_END + 0x000, //  Flags: PUBLIC, URGENT
    UNIT_DYNAMIC_WORLD_EFFECTS                      = OBJECT_DYNAMIC_END + 0x001, //  Flags: PUBLIC, URGENT
    UNIT_DYNAMIC_END                                = OBJECT_DYNAMIC_END + 0x002
};

enum EPlayerFields
{
    PLAYER_DUEL_ARBITER                             = UNIT_END + 0x000, // Size: 2, Flags: UF_FLAG_PUBLIC
    PLAYER_FLAGS                                    = UNIT_END + 0x002, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_GUILD_RANK_ID                            = UNIT_END + 0x003, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_GUILD_DELETE_DATE                        = UNIT_END + 0x004, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_GUILD_LEVEL                              = UNIT_END + 0x005, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_BYTES                                    = UNIT_END + 0x006, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_BYTES_2                                  = UNIT_END + 0x007, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_BYTES_3                                  = UNIT_END + 0x008, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_DUEL_TEAM                                = UNIT_END + 0x009, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_GUILD_TIME_STAMP                         = UNIT_END + 0x00A, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_QUEST_LOG                                = UNIT_END + 0x00B, // Size: 750, Flags: UF_FLAG_PARTY
    PLAYER_VISIBLE_ITEMS                            = UNIT_END + 0x2F9, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_VISIBLE_ITEM_ENCHANTMENTS                = UNIT_END + 0x2FA, // Size: 37, Flags: UF_FLAG_PARTY
    PLAYER_TITLE                                    = UNIT_END + 0x31F, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_FAKE_INEBRIATION                         = UNIT_END + 0x320, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_VIRTUAL_PLAYER_REALM                     = UNIT_END + 0x321, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_CURRENT_SPEC_ID                          = UNIT_END + 0x322, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_TAXI_MOUNT_ANIM_KIT_ID                   = UNIT_END + 0x323, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_CURRENT_BATTLE_PET_BREED_QUALITY         = UNIT_END + 0x324, // Size: 1, Flags: UF_FLAG_PUBLIC
    PLAYER_INV_SLOTS                                = UNIT_END + 0x325, // Size: 46, Flags: UF_FLAG_PRIVATE
    PLAYER_PACK_SLOTS                               = UNIT_END + 0x353, // Size: 32, Flags: UF_FLAG_PRIVATE
    PLAYER_BANK_SLOTS                               = UNIT_END + 0x373, // Size: 56, Flags: UF_FLAG_PRIVATE
    PLAYER_BANKBAG_SLOTS                            = UNIT_END + 0x3AB, // Size: 14  Flags: UF_FLAG_PRIVATE
    PLAYER_VENDORBUYBACK_SLOTS                      = UNIT_END + 0x3B9, // Size: 24, Flags: UF_FLAG_PRIVATE
    PLAYER_FARSIGHT_OBJECT                          = UNIT_END + 0x3D1, // Size: 2, Flags: UF_FLAG_PRIVATE
    PLAYER_KNOWN_TITLES                             = UNIT_END + 0x3D3, // Size: 10, Flags: UF_FLAG_PRIVATE
    PLAYER_COINAGE                                  = UNIT_END + 0x3DD, // Size: 2, Flags: UF_FLAG_PRIVATE
    PLAYER_XP                                       = UNIT_END + 0x3DF, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_NEXT_LEVEL_XP                            = UNIT_END + 0x3E0, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL                                    = UNIT_END + 0x3E1, // Size: 448, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_LINEIDS                            = UNIT_END + 0x03E1, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_STEPS                              = UNIT_END + 0x0421, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_RANKS                              = UNIT_END + 0x0461, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_STARTING_RANKS                     = UNIT_END + 0x04A1, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_MAX_RANKS                          = UNIT_END + 0x04E1, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_MODIFIERS                          = UNIT_END + 0x0521, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_SKILL_TALENTS                            = UNIT_END + 0x0561, // Size: 64, Flags: UF_FLAG_PRIVATE
    PLAYER_CHARACTER_POINTS                         = UNIT_END + 0x5A1, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MAX_TALENT_TIERS                         = UNIT_END + 0x5A2, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_TRACK_CREATURE_MASK                      = UNIT_END + 0x5A3, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_TRACK_RESOURCE_MASK                      = UNIT_END + 0x5A4, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MAINHAND_EXPERTISE                       = UNIT_END + 0x5A5, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_OFFHAND_EXPERTISE                        = UNIT_END + 0x5A6, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_RANGED_EXPERTISE                         = UNIT_END + 0x5A7, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_COMBAT_RATING_EXPERTISE                  = UNIT_END + 0x5A8, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_BLOCK_PERCENTAGE                         = UNIT_END + 0x5A9, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_DODGE_PERCENTAGE                         = UNIT_END + 0x5AA, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_PARRY_PERCENTAGE                         = UNIT_END + 0x5AB, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_CRIT_PERCENTAGE                          = UNIT_END + 0x5AC, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_RANGED_CRIT_PERCENTAGE                   = UNIT_END + 0x5AD, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_OFFHAND_CRIT_PERCENTAGE                  = UNIT_END + 0x5AE, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_SPELL_CRIT_PERCENTAGE                    = UNIT_END + 0x5AF, // Size: 7, Flags: UF_FLAG_PRIVATE
    PLAYER_SHIELD_BLOCK                             = UNIT_END + 0x5B6, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_SHIELD_BLOCK_CRIT_PERCENTAGE             = UNIT_END + 0x5B7, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MASTERY                                  = UNIT_END + 0x5B8, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_PVP_POWER_DAMAGE                         = UNIT_END + 0x5B9, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_PVP_POWER_HEALING                        = UNIT_END + 0x5BA, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_EXPLORED_ZONES                           = UNIT_END + 0x5BB, // Size: 200, Flags: UF_FLAG_PRIVATE
    PLAYER_REST_STATE_EXPERIENCE                    = UNIT_END + 0x683, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_DAMAGE_DONE_POS                      = UNIT_END + 0x684, // Size: 7, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_DAMAGE_DONE_NEG                      = UNIT_END + 0x68B, // Size: 7, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_DAMAGE_DONE_PERCENT                  = UNIT_END + 0x692, // Size: 7, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_HEALING_DONE_POS                     = UNIT_END + 0x699, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_HEALING_PERCENT                      = UNIT_END + 0x69A, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_HEALING_DONE_PERCENT                 = UNIT_END + 0x69B, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_PERIODIC_HEALING_DONE_PERCENT        = UNIT_END + 0x69C, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_WEAPON_DMG_MULTIPLIERS                   = UNIT_END + 0x69D, // Size: 3, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_SPELL_POWER_PERCENT                  = UNIT_END + 0x6A0, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_RESILIENCE_PERCENT                   = UNIT_END + 0x6A1, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_OVERRIDE_SPELL_POWER_BY_AP_PERCENT       = UNIT_END + 0x6A2, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_OVERRIDE_AP_BY_SPELL_POWER_PERCENT       = UNIT_END + 0x6A3, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_TARGET_RESISTANCE                    = UNIT_END + 0x6A4, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_TARGET_PHYSICAL_RESISTANCE           = UNIT_END + 0x6A5, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_BYTES1                                   = UNIT_END + 0x6A6, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_SELF_RES_SPELL                           = UNIT_END + 0x6A7, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_PVP_MEDALS                               = UNIT_END + 0x6A8, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_BUYBACK_PRICE                            = UNIT_END + 0x6A9, // Size: 12, Flags: UF_FLAG_PRIVATE
    PLAYER_BUYBACK_TIMESTAMP                        = UNIT_END + 0x6B5, // Size: 12, Flags: UF_FLAG_PRIVATE
    PLAYER_KILLS                                    = UNIT_END + 0x6C1, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_LIFETIME_HONORABLE_KILLS                 = UNIT_END + 0x6C2, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_WATCHED_FACTION_INDEX                    = UNIT_END + 0x6C3, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_COMBAT_RATINGS                           = UNIT_END + 0x6C4, // Size: 27, Flags: UF_FLAG_PRIVATE
    PLAYER_PVP_INFO                                 = UNIT_END + 0x6DF, // Size: 24, Flags: UF_FLAG_PRIVATE
    PLAYER_MAX_LEVEL                                = UNIT_END + 0x6F7, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_RUNE_REGEN                               = UNIT_END + 0x6F8, // Size: 4, Flags: UF_FLAG_PRIVATE
    PLAYER_NO_REAGENT_COST_MASK                     = UNIT_END + 0x6FC, // Size: 4, Flags: UF_FLAG_PRIVATE
    PLAYER_GLYPH_SLOTS                              = UNIT_END + 0x700, // Size: 6, Flags: UF_FLAG_PRIVATE
    PLAYER_GLYPHS                                   = UNIT_END + 0x706, // Size: 6, Flags: UF_FLAG_PRIVATE
    PLAYER_GLYPH_SLOTS_ENABLED                      = UNIT_END + 0x70C, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_PET_SPELL_POWER                          = UNIT_END + 0x70D, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_RESEARCHING                              = UNIT_END + 0x70E, // Size: 8, Flags: UF_FLAG_PRIVATE
    PLAYER_PROFESSION_SKILL_LINE                    = UNIT_END + 0x716, // Size: 2, Flags: UF_FLAG_PRIVATE
    PLAYER_UI_HIT_MODIFIER                          = UNIT_END + 0x718, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_UI_SPELL_HIT_MODIFIER                    = UNIT_END + 0x719, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_HOME_REALM_TIME_OFFSET                   = UNIT_END + 0x71A, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_MOD_PET_HASTE                            = UNIT_END + 0x71B, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_SUMMONED_BATTLE_PET_GUID                 = UNIT_END + 0x71C, // Size: 2, Flags: UF_FLAG_PRIVATE
    PLAYER_BYTES2                                   = UNIT_END + 0x71E, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_URGENT_SELF_ONLY
    PLAYER_LFG_BONUS_FACTION_ID                     = UNIT_END + 0x71F, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_LOOT_SPEC_ID                             = UNIT_END + 0x720, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_OVERRIDE_ZONE_PVPTYPE                    = UNIT_END + 0x721, // Size: 1, Flags: UF_FLAG_PRIVATE, UF_FLAG_URGENT_SELF_ONLY
    PLAYER_ITEM_LEVEL_DELTA                         = UNIT_END + 0x722, // Size: 1, Flags: UF_FLAG_PRIVATE
    PLAYER_END                                      = UNIT_END + 0x723
};

enum PlayerDynamicFields
{
    PLAYER_DYNAMIC_RESERACH_SITE                    = UNIT_DYNAMIC_END + 0x000, //  Flags: PRIVATE
    PLAYER_DYNAMIC_RESEARCH_SITE_PROGRESS           = UNIT_DYNAMIC_END + 0x001, //  Flags: PRIVATE
    PLAYER_DYNAMIC_DAILY_QUESTS                     = UNIT_DYNAMIC_END + 0x002, //  Flags: PRIVATE
    PLAYER_DYNAMIC_END                              = UNIT_DYNAMIC_END + 0x003
};

enum EGameObjectFields
{
    GAMEOBJECT_CREATED_BY                           = OBJECT_END + 0x0, // Size: 2, Flags: UF_FLAG_PUBLIC
    GAMEOBJECT_DISPLAY_ID                           = OBJECT_END + 0x2, // Size: 1, Flags: UF_FLAG_PUBLIC
    GAMEOBJECT_FLAGS                                = OBJECT_END + 0x3, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    GAMEOBJECT_PARENT_ROTATION                      = OBJECT_END + 0x4, // Size: 4, Flags: UF_FLAG_PUBLIC
    GAMEOBJECT_FACTION_TEMPLATE                     = OBJECT_END + 0x8, // Size: 1, Flags: UF_FLAG_PUBLIC
    GAMEOBJECT_LEVEL                                = OBJECT_END + 0x9, // Size: 1, Flags: UF_FLAG_PUBLIC
    GAMEOBJECT_BYTES                                = OBJECT_END + 0xA, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    GAMEOBJECT_VISUAL_SPELL                         = OBJECT_END + 0xB, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    GAMEOBJECT_END                                  = OBJECT_END + 0xC
};

enum GameObjectDynamicFields
{
    GAMEOBJECT_DYNAMIC_END                          = OBJECT_DYNAMIC_END + 0x000
};

enum EDynamicObjectFields
{
    DYNAMICOBJECT_CASTER                            = OBJECT_END + 0x0, // Size: 2, Flags: UF_FLAG_PUBLIC
    DYNAMICOBJECT_SPELL_VISUAL_AND_TYPE             = OBJECT_END + 0x2, // Size: 1, Flags: UF_FLAG_VIEWER_DEPENDENT
    DYNAMICOBJECT_SPELL_ID                          = OBJECT_END + 0x3, // Size: 1, Flags: UF_FLAG_PUBLIC
    DYNAMICOBJECT_RADIUS                            = OBJECT_END + 0x4, // Size: 1, Flags: UF_FLAG_PUBLIC
    DYNAMICOBJECT_CAST_TIME                         = OBJECT_END + 0x5, // Size: 1, Flags: UF_FLAG_PUBLIC
    DYNAMICOBJECT_END                               = OBJECT_END + 0x6
};

enum DynamicObjectDynamicFields
{
    DYNAMICOBJECT_DYNAMIC_END                       = OBJECT_DYNAMIC_END + 0x000,
};

enum ECorpseFields
{
    CORPSE_OWNER                                    = OBJECT_END + 0x00, // Size: 2, Flags: UF_FLAG_PUBLIC
    CORPSE_PARTY_GUID                               = OBJECT_END + 0x02, // Size: 2, Flags: UF_FLAG_PUBLIC
    CORPSE_DISPLAY_ID                               = OBJECT_END + 0x04, // Size: 1, Flags: UF_FLAG_PUBLIC
    CORPSE_ITEMS                                    = OBJECT_END + 0x05, // Size: 19, Flags: UF_FLAG_PUBLIC
    CORPSE_SKIN_ID                                  = OBJECT_END + 0x18, // Size: 1, Flags: UF_FLAG_PUBLIC
    CORPSE_FACIAL_HAIR_STYLE_ID                     = OBJECT_END + 0x19, // Size: 1, Flags: UF_FLAG_PUBLIC
    CORPSE_FLAGS                                    = OBJECT_END + 0x1A, // Size: 1, Flags: UF_FLAG_PUBLIC
    CORPSE_DYNAMIC_FLAGS                            = OBJECT_END + 0x1B, // Size: 1, Flags: UF_FLAG_VIEWER_DEPENDENT
    CORPSE_END                                      = OBJECT_END + 0x1C
};

enum CorpseDynamicFields
{
    CORPSE_DYNAMIC_END                              = OBJECT_DYNAMIC_END + 0x000,
};

enum EAreaTriggerFields
{
    AREATRIGGER_CASTER                              = OBJECT_END + 0x0, // Size: 2, Flags: UF_FLAG_PUBLIC
    AREATRIGGER_DURATION                            = OBJECT_END + 0x2, // Size: 1, Flags: UF_FLAG_PUBLIC
    AREATRIGGER_SPELL_ID                            = OBJECT_END + 0x3, // Size: 1, Flags: UF_FLAG_PUBLIC
    AREATRIGGER_SPELL_VISUAL_ID                     = OBJECT_END + 0x4, // Size: 1, Flags: UF_FLAG_VIEWER_DEPENDENT
    AREATRIGGER_EXPLICIT_SCALE                      = OBJECT_END + 0x5, // Size: 1, Flags: UF_FLAG_PUBLIC, UF_FLAG_URGENT
    AREATRIGGER_END                                 = OBJECT_END + 0x6
};

enum AreaTriggerDynamicFields
{
    AREATRIGGER_DYNAMIC_END                         = OBJECT_DYNAMIC_END + 0x000,
};

enum ESceneObjectFields
{
    SCENEOBJECT_SCRIPT_PACKAGE_ID                   = OBJECT_END + 0x0, // Size: 1, Flags: UF_FLAG_PUBLIC
    SCENEOBJECT_RND_SEED_VAL                        = OBJECT_END + 0x1, // Size: 1, Flags: UF_FLAG_PUBLIC
    SCENEOBJECT_CREATED_BY                          = OBJECT_END + 0x2, // Size: 2, Flags: UF_FLAG_PUBLIC
    SCENEOBJECT_SCENE_TYPE                          = OBJECT_END + 0x4, // Size: 1, Flags: UF_FLAG_PUBLIC
    SCENEOBJECT_END                                 = OBJECT_END + 0x5
};

enum SceneObjectDynamicFields
{
    SCENEOBJECT_DYNAMIC_END                         = OBJECT_DYNAMIC_END + 0x000,
};

#endif // _UPDATEFIELDS_H
