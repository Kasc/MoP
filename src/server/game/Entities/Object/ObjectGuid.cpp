/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectGuid.h"
#include "World.h"
#include "ObjectMgr.h"
#include <sstream>
#include <iomanip>

ObjectGuid const ObjectGuid::Empty = ObjectGuid();
ObjectGuid const ObjectGuid::TradeItem = ObjectGuid(HighGuid::None, uint32(10));

char const* ObjectGuid::GetTypeName(HighGuid high)
{
    switch (high)
    {
        case HighGuid::Item:            return "Item";
        case HighGuid::Player:          return "Player";
        case HighGuid::GameObject:      return "Gameobject";
        case HighGuid::Creature:        return "Creature";
        case HighGuid::Pet:             return "Pet";
        case HighGuid::Vehicle:         return "Vehicle";
        case HighGuid::DynamicObject:   return "DynObject";
        case HighGuid::Corpse:          return "Corpse";
        case HighGuid::AreaTrigger:     return "AreaTrigger";
        case HighGuid::BattleGround1:   return "Battleground";
        case HighGuid::BattleGround2:   return "Battleground";
        case HighGuid::Transport:       return "Transport";
        case HighGuid::InstanceSave:    return "InstanceID";
        case HighGuid::Party:           return "Group";
        case HighGuid::Guild:           return "Guild";
        case HighGuid::LootObject:      return "Loot";
        default:
            return "<unknown>";
    }
}

std::string ObjectGuid::ToString() const
{
    std::ostringstream str;
    str << "GUID Full: 0x" << std::hex << std::uppercase << std::setw(16) << std::setfill('0') << _data._guid << std::dec << std::nouppercase;
    str << " Type: " << GetTypeName();
    if (HasEntry())
        str << (IsPet() ? " Pet number: " : " Entry: ") << GetEntry();

    str << " Low: " << GetCounter();
    return str.str();
}

ByteBuffer& operator<<(ByteBuffer& buf, PackedGuid const& guid)
{
    buf.append(guid._packedGuid);
    return buf;
}

ByteBuffer& operator>>(ByteBuffer& buf, PackedGuidReader const& guid)
{
    buf.readPackGUID(*reinterpret_cast<uint64*>(guid.GuidPtr));
    return buf;
}

ObjectGuid ObjectGuid::Global(HighGuid type, uint32 counter)
{
    return ObjectGuid(type, counter);
}

ObjectGuid ObjectGuid::MapSpecific(HighGuid type, uint32 entry, uint32 counter)
{
    return ObjectGuid(type, entry, counter);
}

void ObjectGuidGeneratorBase::HandleCounterOverflow(HighGuid high)
{
    TC_LOG_ERROR("misc", "%s guid overflow!! Can't continue, shutting down server. ", ObjectGuid::GetTypeName(high));
    World::StopNow(ERROR_EXIT_CODE);
}
