/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ObjectGuid_h__
#define ObjectGuid_h__

#include "Common.h"
#include "ByteBuffer.h"

#include <type_traits>
#include <functional>

enum TypeID
{
    TYPEID_OBJECT        = 0,
    TYPEID_ITEM          = 1,
    TYPEID_CONTAINER     = 2,
    TYPEID_UNIT          = 3,
    TYPEID_PLAYER        = 4,
    TYPEID_GAMEOBJECT    = 5,
    TYPEID_DYNAMICOBJECT = 6,
    TYPEID_CORPSE        = 7,
    TYPEID_AREATRIGGER   = 8,
    TYPEID_SCENEOBJECT   = 9
};

#define NUM_CLIENT_OBJECT_TYPES             10

enum TypeMask
{
    TYPEMASK_OBJECT         = 0x0001,
    TYPEMASK_ITEM           = 0x0002,
    TYPEMASK_CONTAINER      = 0x0004,
    TYPEMASK_UNIT           = 0x0008,
    TYPEMASK_PLAYER         = 0x0010,
    TYPEMASK_GAMEOBJECT     = 0x0020,
    TYPEMASK_DYNAMICOBJECT  = 0x0040,
    TYPEMASK_CORPSE         = 0x0080,
    TYPEMASK_AREATRIGGER    = 0x0100,
    TYPEMASK_SCENEOBJECT    = 0x0200,
    TYPEMASK_SEER           = TYPEMASK_PLAYER | TYPEMASK_UNIT | TYPEMASK_DYNAMICOBJECT
};

enum class HighGuid
{
    None            = -1,
    Player          = 0x018,
    BattleGround1   = 0x1F1,
    InstanceSave    = 0x1F4,
    Party           = 0x1F5,
    Account         = 0x1F8,
    BattleGround2   = 0x1F9, // NYI
    Transport       = 0x1FC,
    Guild           = 0x1FF,
    Item            = 0x400,
    DynamicObject   = 0xF10,
    GameObject      = 0xF11,
    Creature        = 0xF13,
    Pet             = 0xF14,
    Vehicle         = 0xF15,
    SceneObject     = 0xF16,
    LootObject      = 0xF19,
    AreaTrigger     = 0xF1B,
    Corpse          = 0xF1C
};

template<HighGuid high>
struct ObjectGuidTraits
{
    static bool const Global = false;
    static bool const MapSpecific = false;
};

#define GUID_TRAIT_GLOBAL(highguid) \
    template<> struct ObjectGuidTraits<highguid> \
    { \
        static bool const Global = true; \
        static bool const MapSpecific = false; \
    };

#define GUID_TRAIT_MAP_SPECIFIC(highguid) \
    template<> struct ObjectGuidTraits<highguid> \
    { \
        static bool const Global = false; \
        static bool const MapSpecific = true; \
    };

GUID_TRAIT_GLOBAL(HighGuid::Party)
GUID_TRAIT_GLOBAL(HighGuid::Account)
GUID_TRAIT_GLOBAL(HighGuid::BattleGround1)
GUID_TRAIT_GLOBAL(HighGuid::BattleGround2)
GUID_TRAIT_GLOBAL(HighGuid::Player)
GUID_TRAIT_GLOBAL(HighGuid::Item)
GUID_TRAIT_GLOBAL(HighGuid::Guild)
GUID_TRAIT_GLOBAL(HighGuid::InstanceSave)
GUID_TRAIT_GLOBAL(HighGuid::SceneObject)
GUID_TRAIT_GLOBAL(HighGuid::LootObject)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::Creature)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::Vehicle)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::Pet)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::GameObject)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::DynamicObject)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::AreaTrigger)
GUID_TRAIT_MAP_SPECIFIC(HighGuid::Corpse)

// Special case
// Global transports are loaded from `transports` table, Global part is used for them.
// after worldserver finishes loading, no more global transports can be created, only the ones existing within instances that never change maps
// here is where MapSpecific comes into play - each map takes over the responsibility to generate transport guids
// on top of this, regular elevators (GAMEOBJECT_TYPE_TRANSPORT) must also use Transport highguid type, otherwise client will reject seeing other players on them
template<>
struct ObjectGuidTraits<HighGuid::Transport>
{
    static bool const Global = true;
    static bool const MapSpecific = true;
};

class ObjectGuid;
class PackedGuid;

struct PackedGuidReader
{
    explicit PackedGuidReader(ObjectGuid& guid) : GuidPtr(&guid) { }
    ObjectGuid* GuidPtr;
};

class ObjectGuid
{
    public:
        static ObjectGuid const Empty;
        static ObjectGuid const TradeItem;

        template<HighGuid type>
        static typename std::enable_if<ObjectGuidTraits<type>::Global, ObjectGuid>::type Create(uint32 counter) { return Global(type, counter); }

        template<HighGuid type>
        static typename std::enable_if<ObjectGuidTraits<type>::MapSpecific && type != HighGuid::Transport, ObjectGuid>::type Create(uint32 entry, uint32 counter) { return MapSpecific(type, entry, counter); }

        ObjectGuid() { _data._guid = UI64LIT(0); }
        explicit ObjectGuid(uint64 guid)  { _data._guid = guid; }
        ObjectGuid(HighGuid hi, uint32 entry, uint32 counter) { _data._guid = uint64(counter ? uint64(counter) | (uint64(entry) << 32) | (uint64(hi) << 52) : 0); }
        ObjectGuid(HighGuid hi, uint32 counter) { _data._guid = uint64(counter ? uint64(counter) | (uint64(hi) << 52) : 0); }

        operator uint64() const { return _data._guid; }
        PackedGuidReader ReadAsPacked() { return PackedGuidReader(*this); }

        void Set(uint64 guid) { _data._guid = guid; }
        void Clear() { _data._guid = 0; }

        PackedGuid WriteAsPacked() const;

        uint64 GetRawValue() const { return _data._guid; }
        HighGuid GetHigh() const
        {
            return HighGuid((uint64(_data._guid) >> 52) & 0x00000FFF);
        }
        uint32 GetEntry() const { return HasEntry() ? uint32((_data._guid >> 32) & UI64LIT(0x00000000000FFFFF)) : 0; }
        uint32 GetCounter()  const
        {
            return uint32(_data._guid & UI64LIT(0x00000000FFFFFFFF));
        }

        static uint32 GetMaxCounter(HighGuid /*high*/)
        {
            return uint32(0xFFFFFFFF);
        }

        uint32 GetMaxCounter() const { return GetMaxCounter(GetHigh()); }

        uint8& operator[](uint32 index)
        {
            ASSERT(index < sizeof(uint64));
            return _data._bytes[index];
        }

        uint8 const& operator[](uint32 index) const
        {
            ASSERT(index < sizeof(uint64));
            return _data._bytes[index];
        }

        bool IsEmpty()              const { return _data._guid == 0; }

        bool IsPlayer()             const { return !IsEmpty() && GetHigh() == HighGuid::Player; }

        bool IsCreature()           const { return GetHigh() == HighGuid::Creature; }
        bool IsPet()                const { return GetHigh() == HighGuid::Pet; }
        bool IsVehicle()            const { return GetHigh() == HighGuid::Vehicle; }
        bool IsCreatureOrPet()      const { return IsCreature() || IsPet(); }
        bool IsCreatureOrVehicle()  const { return IsCreature() || IsVehicle(); }
        bool IsAnyTypeCreature()    const { return IsCreature() || IsPet() || IsVehicle(); }

        bool IsUnit()               const { return IsAnyTypeCreature() || IsPlayer(); }

        bool IsGameObject()         const { return GetHigh() == HighGuid::GameObject; }
        bool IsTransport()          const { return GetHigh() == HighGuid::Transport; }
        bool IsAnyTypeGameObject()  const { return IsGameObject() || IsTransport(); }

        bool IsItem()               const { return GetHigh() == HighGuid::Item; }
        bool IsDynamicObject()      const { return GetHigh() == HighGuid::DynamicObject; }
        bool IsCorpse()             const { return GetHigh() == HighGuid::Corpse; }

        bool IsBattleground()       const { return GetHigh() == HighGuid::BattleGround1; }

        bool IsAccount()            const { return GetHigh() == HighGuid::Account; }
        bool IsAreaTrigger()        const { return GetHigh() == HighGuid::AreaTrigger; }
        bool IsGroup()              const { return GetHigh() == HighGuid::Party; }
        bool IsGuild()              const { return GetHigh() == HighGuid::Guild; }
        bool IsInstance()           const { return GetHigh() == HighGuid::InstanceSave; }
        bool IsLoot()               const { return GetHigh() == HighGuid::LootObject; }

        static TypeID GetTypeId(HighGuid high)
        {
            switch (high)
            {
                case HighGuid::Item:            return TYPEID_ITEM;
                //case HighGuid::Container:     return TYPEID_CONTAINER; HighGuid::Container==HighGuid::Item currently
                case HighGuid::Creature:
                case HighGuid::Pet:
                case HighGuid::Vehicle:         return TYPEID_UNIT;
                case HighGuid::Player:          return TYPEID_PLAYER;
                case HighGuid::GameObject:
                case HighGuid::Transport:       return TYPEID_GAMEOBJECT;
                case HighGuid::DynamicObject:   return TYPEID_DYNAMICOBJECT;
                case HighGuid::Corpse:          return TYPEID_CORPSE;
                case HighGuid::AreaTrigger:     return TYPEID_AREATRIGGER;
                case HighGuid::SceneObject:     return TYPEID_SCENEOBJECT;
                case HighGuid::InstanceSave:
                case HighGuid::BattleGround1:
                case HighGuid::Party:
                case HighGuid::Guild:
                case HighGuid::LootObject:
                case HighGuid::Account:
                default:                    return TYPEID_OBJECT;
            }
        }

        TypeID GetTypeId() const { return GetTypeId(GetHigh()); }

        bool operator!() const { return IsEmpty(); }
        bool operator== (ObjectGuid const& guid) const { return GetRawValue() == guid.GetRawValue(); }
        bool operator!= (ObjectGuid const& guid) const { return GetRawValue() != guid.GetRawValue(); }
        bool operator< (ObjectGuid const& guid) const { return GetRawValue() < guid.GetRawValue(); }

        static char const* GetTypeName(HighGuid high);
        char const* GetTypeName() const { return !IsEmpty() ? GetTypeName(GetHigh()) : "None"; }
        std::string ToString() const;

    private:
        static bool HasEntry(HighGuid high)
        {
            switch (high)
            {
                case HighGuid::Creature:
                case HighGuid::Vehicle:
                case HighGuid::Pet:
                case HighGuid::GameObject:
                    return true;
                default:
                    return false;
            }
        }

        bool HasEntry() const { return HasEntry(GetHigh()); }

        static ObjectGuid Global(HighGuid type, uint32 counter);
        static ObjectGuid MapSpecific(HighGuid type, uint32 entry, uint32 counter);

        explicit ObjectGuid(uint32 const&) = delete;                 // no implementation, used to catch wrong type assignment
        ObjectGuid(HighGuid, uint32, uint64 counter) = delete;       // no implementation, used to catch wrong type assignment
        ObjectGuid(HighGuid, uint64 counter) = delete;               // no implementation, used to catch wrong type assignment

        union
        {
            uint64 _guid;
            uint8 _bytes[sizeof(uint64)];
        } _data;
};

// Some Shared defines
typedef std::set<ObjectGuid> GuidSet;
typedef std::list<ObjectGuid> GuidList;
typedef std::deque<ObjectGuid> GuidDeque;
typedef std::vector<ObjectGuid> GuidVector;
typedef std::unordered_set<ObjectGuid> GuidUnorderedSet;

// minimum buffer size for packed guid is 9 bytes
#define PACKED_GUID_MIN_BUFFER_SIZE 9

class PackedGuid
{
        friend ByteBuffer& operator<<(ByteBuffer& buf, PackedGuid const& guid);

    public:
        explicit PackedGuid() : _packedGuid(PACKED_GUID_MIN_BUFFER_SIZE) { _packedGuid.appendPackGUID(0); }
        explicit PackedGuid(uint64 guid) : _packedGuid(PACKED_GUID_MIN_BUFFER_SIZE) { _packedGuid.appendPackGUID(guid); }
        explicit PackedGuid(ObjectGuid guid) : _packedGuid(PACKED_GUID_MIN_BUFFER_SIZE) { _packedGuid.appendPackGUID(guid.GetRawValue()); }

        void Set(uint64 guid) { _packedGuid.wpos(0); _packedGuid.appendPackGUID(guid); }
        void Set(ObjectGuid guid) { _packedGuid.wpos(0); _packedGuid.appendPackGUID(guid.GetRawValue()); }

        size_t size() const { return _packedGuid.size(); }

    private:                                                // fields
        ByteBuffer _packedGuid;
};

class ObjectGuidGeneratorBase
{
public:
    ObjectGuidGeneratorBase(uint32 start = 1) : _nextGuid(start) { }

    virtual void Set(uint32 val) { _nextGuid = val; }
    virtual uint32 Generate() = 0;
    uint32 GetNextAfterMaxUsed() const { return _nextGuid; }

protected:
    static void HandleCounterOverflow(HighGuid high);
    uint32 _nextGuid;
};

template<HighGuid high>
class ObjectGuidGenerator : public ObjectGuidGeneratorBase
{
public:
    explicit ObjectGuidGenerator(uint32 start = 1) : ObjectGuidGeneratorBase(start) { }

    uint32 Generate() override
    {
        if (_nextGuid >= ObjectGuid::GetMaxCounter(high) - 1)
            HandleCounterOverflow(high);
        return _nextGuid++;
    }
};

ByteBuffer& operator<<(ByteBuffer& buf, PackedGuid const& guid);
ByteBuffer& operator>>(ByteBuffer& buf, PackedGuidReader const& guid);

inline PackedGuid ObjectGuid::WriteAsPacked() const { return PackedGuid(*this); }

namespace std
{
    template<>
    struct hash<ObjectGuid>
    {
        public:
            size_t operator()(ObjectGuid const& key) const
            {
                return hash<uint64>()(key.GetRawValue());
            }
    };
}

#endif // ObjectGuid_h__
