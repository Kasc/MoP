/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuestDef.h"
#include "GossipDef.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Formulas.h"

GossipMenu::GossipMenu()
{
    _menuId = 0;
    _locale = DEFAULT_LOCALE;
    _senderGUID.Clear();
}

GossipMenu::~GossipMenu()
{
    ClearMenu();
}

uint32 GossipMenu::AddMenuItem(int32 optionIndex, uint8 icon, std::string const& message, uint32 sender, uint32 action, std::string const& boxMessage, uint32 boxMoney, bool coded /*= false*/)
{
    ASSERT(_menuItems.size() <= GOSSIP_MAX_MENU_ITEMS);

    // Find a free new id - script case
    if (optionIndex == -1)
    {
        optionIndex = 0;
        if (!_menuItems.empty())
        {
            for (GossipMenuItemContainer::const_iterator itr = _menuItems.begin(); itr != _menuItems.end(); ++itr)
            {
                if (int32(itr->first) > optionIndex)
                    break;

                optionIndex = itr->first + 1;
            }
        }
    }

    GossipMenuItem& menuItem = _menuItems[optionIndex];

    menuItem.MenuItemIcon    = icon;
    menuItem.Message         = message;
    menuItem.IsCoded         = coded;
    menuItem.Sender          = sender;
    menuItem.OptionType      = action;
    menuItem.BoxMessage      = boxMessage;
    menuItem.BoxMoney        = boxMoney;
    return optionIndex;
}

/**
 * @name AddMenuItem
 * @brief Adds a localized gossip menu item from db by menu id and menu item id.
 * @param menuId Gossip menu id.
 * @param menuItemId Gossip menu item id.
 * @param sender Identifier of the current menu.
 * @param action Custom action given to GossipHello.
 */
void GossipMenu::AddMenuItem(uint32 menuId, uint32 menuItemId, uint32 sender, uint32 action)
{
    /// Find items for given menu id.
    GossipMenuItemsMapBounds bounds = sObjectMgr->GetGossipMenuItemsMapBounds(menuId);
    /// Return if there are none.
    if (bounds.first == bounds.second)
        return;

    /// Iterate over each of them.
    for (GossipMenuItemsContainer::const_iterator itr = bounds.first; itr != bounds.second; ++itr)
    {
        /// Find the one with the given menu item id.
        if (itr->second.OptionIndex != menuItemId)
            continue;

        /// Store texts for localization.
        std::string strOptionText, strBoxText;
        BroadcastTextEntry const* optionBroadcastText = sBroadcastTextStore.LookupEntry(itr->second.OptionBroadcastTextId);
        BroadcastTextEntry const* boxBroadcastText = sBroadcastTextStore.LookupEntry(itr->second.BoxBroadcastTextId);

        /// OptionText
        if (optionBroadcastText)
            strOptionText = sDB2Manager->GetBroadcastTextValue(optionBroadcastText, GetLocale());
        else
            strOptionText = itr->second.OptionText;

        /// BoxText
        if (boxBroadcastText)
            strBoxText = sDB2Manager->GetBroadcastTextValue(boxBroadcastText, GetLocale());
        else
            strBoxText = itr->second.BoxText;

        /// Add menu item with existing method. Menu item id -1 is also used in ADD_GOSSIP_ITEM macro.
        uint32 optionIndex = AddMenuItem(-1, itr->second.OptionIcon, strOptionText, sender, action, strBoxText, itr->second.BoxMoney, itr->second.BoxCoded);
        AddGossipMenuItemData(optionIndex, itr->second.ActionMenuId, itr->second.ActionPoiId, itr->second.TrainerId);
    }
}

void GossipMenu::AddGossipMenuItemData(uint32 optionIndex, uint32 gossipActionMenuId, uint32 gossipActionPoi, uint32 trainerId)
{
    GossipMenuItemData& itemData = _menuItemData[optionIndex];

    itemData.GossipActionMenuId  = gossipActionMenuId;
    itemData.GossipActionPoi     = gossipActionPoi;
    itemData.TrainerId           = trainerId;
}

uint32 GossipMenu::GetMenuItemSender(uint32 menuItemId) const
{
    GossipMenuItemContainer::const_iterator itr = _menuItems.find(menuItemId);
    if (itr == _menuItems.end())
        return 0;

    return itr->second.Sender;
}

uint32 GossipMenu::GetMenuItemAction(uint32 menuItemId) const
{
    GossipMenuItemContainer::const_iterator itr = _menuItems.find(menuItemId);
    if (itr == _menuItems.end())
        return 0;

    return itr->second.OptionType;
}

bool GossipMenu::IsMenuItemCoded(uint32 menuItemId) const
{
    GossipMenuItemContainer::const_iterator itr = _menuItems.find(menuItemId);
    if (itr == _menuItems.end())
        return false;

    return itr->second.IsCoded;
}

bool GossipMenu::HasMenuItemType(uint32 optionType) const
{
    for (auto const& menuItemPair : _menuItems)
        if (menuItemPair.second.OptionType == optionType)
            return true;

    return false;
}

void GossipMenu::ClearMenu()
{
    _menuItems.clear();
    _menuItemData.clear();
}

PlayerMenu::PlayerMenu(WorldSession* session) : _session(session)
{
    if (_session)
        _gossipMenu.SetLocale(_session->GetSessionDbLocaleIndex());
}

PlayerMenu::~PlayerMenu()
{
    ClearMenus();
}

void PlayerMenu::ClearMenus()
{
    _gossipMenu.ClearMenu();
    _questMenu.ClearMenu();
}

void PlayerMenu::SendGossipMenu(uint32 titleTextId, ObjectGuid objectGUID)
{
    _gossipMenu.SetSenderGUID(objectGUID);

    std::vector<std::pair<QuestMenuItem, std::string>> questVector;
    std::vector<std::pair<uint32, GossipMenuItem>> gossipVector;

    // Store this instead of checking the Singleton every loop iteration
    bool questLevelInTitle = sWorld->getBoolConfig(CONFIG_UI_QUESTLEVELS_IN_DIALOGS);

    uint32 questTitlesSize = 0;
    for (uint8 i = 0; i < _questMenu.GetMenuItemCount(); ++i)
    {
        uint32 questId = _questMenu.GetItem(i).QuestId;

        if (Quest const* quest = sObjectMgr->GetQuestTemplate(questId))
        {
            QuestMenuItem menuItem = _questMenu.GetItem(i);

            std::string title = quest->GetLogTitle();

            if (questLevelInTitle)
                AddQuestLevelToTitle(title, quest->GetQuestLevel());

            questVector.push_back(std::make_pair(std::move(_questMenu.GetItem(i)), title));

            questTitlesSize += title.length();
        }
    }

    uint32 BoxMessagesSize = 0;
    uint32 MessagesSize = 0;
    for (GossipMenuItemContainer::const_iterator itr = _gossipMenu.GetMenuItems().begin(); itr != _gossipMenu.GetMenuItems().end(); ++itr)
    {
        GossipMenuItem const& item = itr->second;

        BoxMessagesSize += item.BoxMessage.length();
        MessagesSize += item.Message.length();

        gossipVector.push_back(std::make_pair(itr->first, std::move(item)));
    }

    int32 FriendshipFactionID = 0;

    ByteBuffer questData;
    ByteBuffer gossipData;

    WorldPacket data(SMSG_GOSSIP_MESSAGE, 1 + 8 + 5 + 4 + 4 + 4 + questVector.size() * (2 + 4 + 4 + 4 + 4 + 4) + gossipVector.size() * (3 + 4 + 4 + 1 + 1) +
                    questTitlesSize + BoxMessagesSize + MessagesSize);

    data.WriteBits(questVector.size(), 19);

    for (auto & itr : questVector)
    {
        QuestMenuItem const& gossipMenu = itr.first;
        std::string title = itr.second;

        uint32 questId = gossipMenu.QuestId;

        Quest const* quest = sObjectMgr->GetQuestTemplate(questId);

        data.WriteBit(quest->IsRepeatable());

        data.WriteBits(title.length(), 9);

        questData.WriteString(title);

        questData << int32(quest->GetFlags());
        questData << int32(quest->GetQuestLevel());
        questData << int32(gossipMenu.QuestIcon);
        questData << int32(gossipMenu.QuestId);
        questData << int32(quest->GetFlags2());
    }

    data.WriteGuidMask(objectGUID, 5, 7, 4, 0);

    data.WriteBits(gossipVector.size(), 20);

    data.WriteGuidMask(objectGUID, 6, 2);

    for (auto & itr : gossipVector)
    {
        GossipMenuItem const& item = itr.second;

        data.WriteBits(item.BoxMessage.length(), 12);
        data.WriteBits(item.Message.length(), 12);

        gossipData << int32(item.BoxMoney);                       // money required to open menu, 2.0.3

        gossipData.WriteString(item.BoxMessage);                  // accept text (related to money) pop up box, 2.0.

        gossipData << int32(itr.first);
        gossipData << int8(item.IsCoded);                         // makes pop up box password

        gossipData.WriteString(item.Message);                     // text for gossip item

        gossipData << int8(item.MenuItemIcon);
    }

    data.WriteGuidMask(objectGUID, 3, 1);

    if (!questData.empty())
        data.append(questData);
    else
        data.FlushBits();

    data.WriteGuidBytes(objectGUID, 1, 0);

    data.append(gossipData);

    data.WriteGuidBytes(objectGUID, 5, 3);

    data << int32(_gossipMenu.GetMenuId());

    data.WriteGuidBytes(objectGUID, 2, 6, 4);

    data << int32(FriendshipFactionID);

    data.WriteGuidBytes(objectGUID, 7);

    data << int32(titleTextId);

    _session->SendPacket(&data);
}

void PlayerMenu::SendCloseGossip()
{
    _gossipMenu.SetSenderGUID(ObjectGuid::Empty);

    WorldPacket data(SMSG_GOSSIP_COMPLETE, 0);

    _session->SendPacket(&data);
}

void PlayerMenu::SendPointOfInterest(uint32 poiId) const
{
    PointOfInterest const* poi = sObjectMgr->GetPointOfInterest(poiId);
    if (!poi)
    {
        TC_LOG_ERROR("sql.sql", "Request to send non-existing POI (Id: %u), ignored.", poiId);
        return;
    }

    WorldPacket data(SMSG_GOSSIP_POI, 4 + 4 + 4 + 4 + 4 + poi->Name.length() + 1);

    data << uint32(poi->Flags);
    data << float(poi->Pos.x);
    data << float(poi->Pos.y);
    data << uint32(poi->Icon);
    data << uint32(poi->Importance);
    data << poi->Name;

    _session->SendPacket(&data);
}

/*********************************************************/
/***                    QUEST SYSTEM                   ***/
/*********************************************************/

QuestMenu::QuestMenu()
{
    _questMenuItems.reserve(16);                                   // can be set for max from most often sizes to speedup push_back and less memory use
}

QuestMenu::~QuestMenu()
{
    ClearMenu();
}

void QuestMenu::AddMenuItem(uint32 QuestId, uint8 Icon)
{
    if (!sObjectMgr->GetQuestTemplate(QuestId))
        return;

    ASSERT(_questMenuItems.size() <= GOSSIP_MAX_MENU_ITEMS);

    QuestMenuItem questMenuItem;

    questMenuItem.QuestId        = QuestId;
    questMenuItem.QuestIcon      = Icon;

    _questMenuItems.push_back(questMenuItem);
}

bool QuestMenu::HasItem(uint32 questId) const
{
    for (QuestMenuItemList::const_iterator i = _questMenuItems.begin(); i != _questMenuItems.end(); ++i)
        if (i->QuestId == questId)
            return true;

    return false;
}

void QuestMenu::ClearMenu()
{
    _questMenuItems.clear();
}

void PlayerMenu::SendQuestGiverQuestList(ObjectGuid guid)
{
    ByteBuffer questData;

    QuestGreeting const* questGreeting = sObjectMgr->GetQuestGreeting(guid);
    if (!questGreeting)
        TC_LOG_DEBUG("misc", "Guid: %s - No quest greeting found.", guid.ToString().c_str());

    std::vector<std::pair<QuestMenuItem, std::string>> questMenuVector;

    // Store this instead of checking the Singleton every loop iteration
    bool questLevelInTitle = sWorld->getBoolConfig(CONFIG_UI_QUESTLEVELS_IN_DIALOGS);

    uint32 questTitlesSize = 0;
    for (uint32 i = 0; i < _questMenu.GetMenuItemCount(); ++i)
    {
        QuestMenuItem const& qmi = _questMenu.GetItem(i);

        uint32 questID = qmi.QuestId;

        if (Quest const* quest = sObjectMgr->GetQuestTemplate(questID))
        {
            std::string title = quest->GetLogTitle();

            if (questLevelInTitle)
                AddQuestLevelToTitle(title, quest->GetQuestLevel());

            questMenuVector.push_back(std::make_pair(std::move(qmi), title));

            questTitlesSize += title.length();
        }
    }

    WorldPacket data(SMSG_QUEST_GIVER_QUEST_LIST, 1 + 8 + 4 + 4 + 4 + (questGreeting ? questGreeting->greeting.length() : 0) +
                    questMenuVector.size() * (2 + 4 + 4 + 4 + 4 + 4));

    data << uint32(questGreeting ? questGreeting->greetEmoteType : 0);
    data << uint32(questGreeting ? questGreeting->greetEmoteDelay : 0);

    data.WriteGuidMask(guid, 2);

    data.WriteBits(questGreeting ? questGreeting->greeting.length() : 0, 11);

    data.WriteGuidMask(guid, 6, 0);

    data.WriteBits(questMenuVector.size(), 19);

    for (auto & itr : questMenuVector)
    {
        QuestMenuItem const& qmi = itr.first;
        std::string title = itr.second;

        uint32 questID = qmi.QuestId;

        Quest const* quest = sObjectMgr->GetQuestTemplate(questID);

        data.WriteBit(quest->IsRepeatable());

        data.WriteBits(title.size(), 9);

        questData << uint32(quest->GetFlags());
        questData << uint32(questID);

        questData.WriteString(title);

        questData << uint32(quest->GetFlags2());
        questData << uint32(qmi.QuestIcon);
        questData << uint32(quest->GetQuestLevel());
    }

    data.WriteGuidMask(guid, 1, 3, 4, 5, 7);

    data.WriteGuidBytes(guid, 1, 0, 6, 7);

    data.append(questData);

    data.WriteGuidBytes(guid, 5, 3, 2);

    if (questGreeting)
        data.WriteString(questGreeting->greeting);

    data.WriteGuidBytes(guid, 4);

    _session->SendPacket(&data);
}

void PlayerMenu::SendQuestGiverStatus(uint32 questStatus, ObjectGuid guid) const
{
    WorldPacket data(SMSG_QUEST_GIVER_STATUS, 1 + 8 + 4);

    data.WriteGuidMask(guid, 1, 7, 4, 2, 5, 3, 6, 0);

    data.WriteGuidBytes(guid, 7);

    data << uint32(questStatus);

    data.WriteGuidBytes(guid, 4, 6, 1, 5, 2, 0, 3);

    _session->SendPacket(&data);

    TC_LOG_DEBUG("network", "WORLD: Sent SMSG_QUEST_GIVER_STATUS NPC=%s, status=%u", guid.ToString().c_str(), questStatus);
}

void PlayerMenu::SendQuestGiverQuestDetails(Quest const* quest, ObjectGuid npcGUID, bool activateAccept) const
{
    std::string questLogTitle        = quest->GetLogTitle();
    std::string questLogDescription  = quest->GetLogDescription();
    std::string questDescription     = quest->GetQuestDescription();
    std::string portraitGiverText    = quest->GetPortraitGiverText();
    std::string portraitGiverName    = quest->GetPortraitGiverName();
    std::string portraitTurnInText   = quest->GetPortraitTurnInText();
    std::string portraitTurnInName   = quest->GetPortraitTurnInName();

    if (sWorld->getBoolConfig(CONFIG_UI_QUESTLEVELS_IN_DIALOGS))
        AddQuestLevelToTitle(questLogTitle, quest->GetQuestLevel());

    uint32 rewItemDisplayId[QUEST_REWARD_ITEM_COUNT];
    uint32 rewChoiceItemDisplayId[QUEST_REWARD_CHOICES_COUNT];

    for (uint8 i = 0; i < QUEST_REWARD_ITEM_COUNT; i++)
    {
        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(quest->RewardItemId[i]))
            rewItemDisplayId[i] = itemTemplate->GetDisplayId();
        else
            rewItemDisplayId[i] = 0;
    }

    for (uint8 i = 0; i < QUEST_REWARD_CHOICES_COUNT; i++)
    {
        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(quest->RewardChoiceItemId[i]))
            rewChoiceItemDisplayId[i] = itemTemplate->GetDisplayId();
        else
            rewChoiceItemDisplayId[i] = 0;
    }

    ObjectGuid guid = _session->GetPlayer()->GetPlayerSharingQuest();
    ObjectGuid guid2 = npcGUID;

    uint32 LearnableSpellsCount = 0;
    if (quest->GetSrcSpell() != 0)
        LearnableSpellsCount = 1;

    bool IsDiscoveredQuest = quest->HasFlag(QUEST_FLAGS_AUTO_TAKE);
    bool CloseGossip = false;

    WorldPacket data(SMSG_QUEST_GIVER_QUEST_DETAILS, 2 * (1 + 8) + 4 + 4 + 4 + QUEST_REWARD_CURRENCY_COUNT * (4 + 4) + 19 * (4) + QUEST_REWARD_REPUTATIONS_COUNT * (4 + 4 + 4) +
                    26 * (4) + 17 + questLogTitle.length() + questLogDescription.length() + questDescription.length() + portraitGiverText.length() +
                    portraitGiverName.length() + portraitTurnInText.length() + portraitTurnInName.length() + quest->GetObjectives().size() * (1 + 4 + 4 + 4) +
                    QUEST_EMOTE_COUNT * (4 + 4) + (LearnableSpellsCount > 0 ? 4 : 0));

    data << uint32(quest->RewardItemCount[3]);
    data << uint32(rewChoiceItemDisplayId[4]);
    data << uint32(quest->RewardChoiceItemId[2]);

    for (uint8 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; i++)
    {
        uint32 currencyId = quest->RewardCurrencyId[i];
        CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(currencyId);

        int32 precision = (currency && (currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION)) ? CURRENCY_PRECISION : 1;
        int32 currencyCount = (quest->RewardCurrencyCount[i] + quest->RewardBonusCurrencyCount[i]) * precision;

        data << uint32(currencyCount);
        data << uint32(currencyId);
    }

    data << uint32(quest->GetRewChoiceItemsCount());
    data << uint32(quest->RewardChoiceItemCount[2]);
    data << uint32(quest->RewardItemCount[1]);
    data << uint32(rewChoiceItemDisplayId[5]);
    data << uint32(quest->RewardItemCount[0]);
    data << uint32(rewItemDisplayId[3]);
    data << uint32(quest->RewardChoiceItemId[0]);
    data << uint32(quest->RewardChoiceItemCount[3]);
    data << uint32(quest->GetQuestGiverPortrait());
    data << uint32(rewChoiceItemDisplayId[3]);
    data << uint32(quest->RewardItemId[0]);
    data << uint32(quest->GetQuestId());
    data << uint32(quest->GetSuggestedPlayers());
    data << uint32(rewChoiceItemDisplayId[0]);
    data << uint32(quest->RewardChoiceItemCount[4]);
    data << uint32(quest->RewardChoiceItemCount[5]);
    data << uint32(quest->GetBonusTalents());
    data << uint32(quest->RewardChoiceItemCount[1]);
    data << uint32(rewChoiceItemDisplayId[2]);

    for (uint8 i = 0; i < QUEST_REWARD_REPUTATIONS_COUNT; i++)
    {
        data << uint32(quest->RewardFactionOverride[i]);
        data << uint32(quest->RewardFactionValue[i]);
        data << uint32(quest->RewardFactionId[i]);
    }

    data << uint32(quest->RewardItemId[3]);
    data << uint32(quest->GetRewardSkillId());
    data << uint32(quest->XPValue(_session->GetPlayer()->GetLevel()) * sWorld->getRate(RATE_XP_QUEST));
    data << uint32(quest->GetRewardReputationMask());
    data << uint32(rewItemDisplayId[2]);
    data << uint32(quest->RewardItemId[1]);
    data << uint32(quest->RewardChoiceItemId[1]);
    data << uint32(quest->RewardChoiceItemId[5]);
    data << uint32(quest->GetRewSpell());
    data << uint32(quest->GetFlags());
    data << uint32(quest->GetRewTitle());
    data << uint32(quest->RewardItemId[2]);
    data << uint32(_session->GetPlayer()->GetQuestMoneyReward(quest));
    data << uint32(quest->RewardItemCount[2]);
    data << uint32(quest->GetFlags2());
    data << uint32(quest->GetRewDisplaySpell());
    data << uint32(quest->RewardChoiceItemId[3]);
    data << uint32(quest->GetRewItemsCount());
    data << uint32(quest->GetRewardSkillPoints());
    data << uint32(rewItemDisplayId[0]);
    data << uint32(quest->RewardChoiceItemId[4]);
    data << uint32(quest->GetQuestPackageID());
    data << uint32(quest->RewardChoiceItemCount[0]);
    data << uint32(rewItemDisplayId[1]);
    data << uint32(rewChoiceItemDisplayId[1]);
    data << uint32(quest->GetQuestTurnInPortrait());

    data.WriteGuidMask(guid, 7);

    data.WriteGuidMask(guid2, 1);

    data.WriteBits(portraitGiverName.size(), 8);

    data.WriteGuidMask(guid2, 2);

    data.WriteBits(portraitTurnInText.size(), 10);

    data.WriteBit(IsDiscoveredQuest);

    data.WriteGuidMask(guid, 2);

    data.WriteBits(questLogTitle.size(), 9);
    data.WriteBits(QUEST_EMOTE_COUNT, 21);

    data.WriteGuidMask(guid, 0);

    data.WriteGuidMask(guid2, 6, 5);

    data.WriteBits(portraitTurnInName.size(), 8);

    data.WriteGuidMask(guid2, 3);

    data.WriteGuidMask(guid, 1);

    data.WriteGuidMask(guid2, 0);

    data.WriteBit(CloseGossip);

    data.WriteGuidMask(guid2, 4);

    data.WriteGuidMask(guid, 3, 5, 4);

    data.WriteBits(portraitGiverText.size(), 10);

    data.WriteBit(activateAccept);

    data.WriteGuidMask(guid, 6);

    data.WriteGuidMask(guid2, 7);

    data.WriteBits(questDescription.size(), 12);
    data.WriteBits(LearnableSpellsCount, 22);
    data.WriteBits(quest->GetObjectives().size(), 20);
    data.WriteBits(questLogDescription.size(), 12);

    data.WriteGuidBytes(guid, 0);

    data.WriteString(portraitTurnInName);
    data.WriteString(portraitGiverText);
    data.WriteString(questLogTitle);

    data.WriteGuidBytes(guid2, 6);

    data.WriteString(questLogDescription);

    data.WriteGuidBytes(guid, 2);

    data.WriteString(portraitTurnInText);

    QuestObjectives const& objectives = quest->GetObjectives();
    for (uint32 i = 0; i < objectives.size(); ++i)
    {
        data << uint8(objectives[i].Type);
        data << uint32(objectives[i].ID);
        data << int32(objectives[i].Amount);
        data << uint32(objectives[i].ObjectID);
    }

    data.WriteString(portraitGiverName);
    data.WriteString(questDescription);

    data.WriteGuidBytes(guid, 5, 7);
    data.WriteGuidBytes(guid2, 7, 3, 0);

    for (uint8 i = 0; i < QUEST_EMOTE_COUNT; i++)
    {
        data << uint32(quest->DetailsEmoteDelay[i]);
        data << uint32(quest->DetailsEmote[i]);
    }

    data.WriteGuidBytes(guid, 4, 3);

    data.WriteGuidBytes(guid2, 5, 1, 2);

    data.WriteGuidBytes(guid, 1, 6);

    data.WriteGuidBytes(guid2, 4);

    if (LearnableSpellsCount > 0)
        data << uint32(quest->GetSrcSpell());

    _session->SendPacket(&data);

    TC_LOG_DEBUG("network", "WORLD: Sent SMSG_QUEST_GIVER_QUEST_DETAILS NPC=%s, questid=%u", npcGUID.ToString().c_str(), quest->GetQuestId());
}

void PlayerMenu::SendQuestQueryResponse(Quest const* quest) const
{
    std::string questLogTitle = quest->GetLogTitle();
    std::string questLogDescription = quest->GetLogDescription();
    std::string questDescription = quest->GetQuestDescription();
    std::string areaDescription = quest->GetAreaDescription();
    std::string questCompletionLog = quest->GetQuestCompletionLog();
    std::string portraitGiverText = quest->GetPortraitGiverText();
    std::string portraitGiverName = quest->GetPortraitGiverName();
    std::string portraitTurnInText = quest->GetPortraitTurnInText();
    std::string portraitTurnInName = quest->GetPortraitTurnInName();

    if (sWorld->getBoolConfig(CONFIG_UI_QUESTLEVELS_IN_DIALOGS))
        AddQuestLevelToTitle(questLogTitle, quest->GetQuestLevel());

    uint32 rewChoiceItemDisplayId[QUEST_REWARD_CHOICES_COUNT];
    for (uint8 i = 0; i < QUEST_REWARD_CHOICES_COUNT; i++)
    {
        if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(quest->RewardChoiceItemId[i]))
            rewChoiceItemDisplayId[i] = itemTemplate->GetDisplayId();
        else
            rewChoiceItemDisplayId[i] = 0;
    }

    uint32 DescriptionsSize = 0;
    uint32 VisualEffectsSize = 0;
    for (QuestObjective const& questObjective : quest->GetObjectives())
    {
        DescriptionsSize += questObjective.Description.length();
        VisualEffectsSize += questObjective.VisualEffects.size();
    }

    bool HasData = true;

    bool hiddenReward = quest->HasFlag(QUEST_FLAGS_HIDDEN_REWARDS);

    WorldPacket data(SMSG_QUEST_QUERY_RESPONSE, 4 + 1 + (HasData ? (14 + quest->GetObjectives().size() * (4 + 4 + 4 + 4 + 1 + 1 + 4) +
                    20 + QUEST_REWARD_CURRENCY_COUNT * (4 + 4) + 12 + QUEST_REWARD_REPUTATIONS_COUNT * (4 + 4 + 4) + 16 +
                    questLogTitle.length() + questLogDescription.length() + questDescription.length() + areaDescription.length() + questCompletionLog.length() +
                    portraitGiverText.length() + portraitGiverName.length() + portraitTurnInText.length() + portraitTurnInName.length() + 224 +
                    DescriptionsSize + VisualEffectsSize * (4)) : 0));

    data << uint32(quest->GetQuestId());

    data.WriteBit(HasData);

    if (HasData)
    {
        data.WriteBits(portraitTurnInText.size(), 10);
        data.WriteBits(questLogTitle.size(), 9);
        data.WriteBits(questCompletionLog.size(), 11);
        data.WriteBits(questDescription.size(), 12);
        data.WriteBits(portraitTurnInName.size(), 8);
        data.WriteBits(portraitGiverName.size(), 8);
        data.WriteBits(portraitGiverText.size(), 10);
        data.WriteBits(areaDescription.size(), 9);
        data.WriteBits(quest->GetObjectives().size(), 19);
        data.WriteBits(questLogDescription.size(), 12);

        ByteBuffer objData;
        for (QuestObjective const& questObjective : quest->GetObjectives())
        {
            std::string descriptionText = questObjective.Description;

            data.WriteBits(descriptionText.length(), 8);
            data.WriteBits(questObjective.VisualEffects.size(), 22);

            objData << int32(questObjective.Amount);
            objData << uint32(questObjective.ID);

            objData.WriteString(descriptionText);

            objData << uint32(questObjective.Flags);
            objData << uint8(questObjective.StorageIndex);
            objData << uint8(questObjective.Type);
            objData << uint32(questObjective.ObjectID);

            for (int32 visualEffect : questObjective.VisualEffects)
                objData << int32(visualEffect);
        }

        data.append(objData);

        data << uint32(quest->ItemDrop[0]);
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemId[4]);
        data << uint32(hiddenReward ? 0 : quest->RewardItemId[3]);
        data << uint32(hiddenReward ? 0 : quest->RewardItemCount[1]);
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemCount[2]);

        for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; i++)
        {
            uint32 currencyId = quest->RewardCurrencyId[i];
            CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(currencyId);

            int32 precision = (currency && (currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION)) ? CURRENCY_PRECISION : 1;
            int32 currencyCount = (quest->RewardCurrencyCount[i] + quest->RewardBonusCurrencyCount[i]) * precision;

            data << uint32(currencyId);
            data << uint32(currencyCount);
        }

        data << uint32(quest->GetBonusTalents());
        data << float(quest->GetPOIy());
        data << uint32(quest->GetSoundTurnIn());

        for (int i = 0; i < QUEST_REWARD_REPUTATIONS_COUNT; i++)
        {
            data << uint32(quest->RewardFactionValue[i]);
            data << uint32(quest->RewardFactionOverride[i]);
            data << uint32(quest->RewardFactionId[i]);
        }

        data << uint32(hiddenReward ? 0 : quest->RewardMoney < 0 ? quest->RewardMoney : _session->GetPlayer()->GetQuestMoneyReward(quest));
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemCount[4]);
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemCount[1]);
        data << uint32(quest->GetFlags2());

        data.WriteString(areaDescription);

        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemId[1]);
        data << uint32(quest->GetRewMoneyMaxLevel());
        data << uint32(hiddenReward ? 0 : quest->RewardItemId[0]);

        data.WriteString(questCompletionLog);

        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemId[3]);
        data << uint32(quest->GetRewHonor());

        data.WriteString(portraitGiverText);
        data.WriteString(questLogDescription);

        data << uint32(quest->GetRewardSkillPoints());
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemId[5]);
        data << uint32(quest->GetSuggestedPlayers());
        data << uint32(quest->GetQuestId());
        data << uint32(quest->ItemDrop[1]);
        data << uint32(hiddenReward ? 0 : quest->RewardItemId[1]);
        data << int32(quest->GetMinLevel());
        data << uint32(quest->GetRewardReputationMask());
        data << uint32(quest->GetPOIPriority());
        data << int32(quest->GetQuestLevel());
        data << uint32(quest->GetQuestType());
        data << uint32(quest->ItemDropQuantity[2]);
        data << uint32(quest->GetXPDifficulty());

        data.WriteString(questDescription);

        data << uint32(hiddenReward ? 0 : quest->RewardItemCount[0]);
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemCount[5]);
        data << uint32(hiddenReward ? 0 : quest->RewardItemCount[2]);
        data << uint32(quest->GetRewSpell());
        data << uint32(rewChoiceItemDisplayId[2]);

        data.WriteString(portraitTurnInName);

        data << uint32(rewChoiceItemDisplayId[1]);
        data << uint32(quest->ItemDropQuantity[1]);
        data << uint32(quest->ItemDrop[2]);
        data << uint32(quest->GetQuestTurnInPortrait());

        data.WriteString(questLogTitle);

        data << uint32(quest->GetQuestInfoID());
        data << uint32(quest->GetXPDifficulty());
        data << uint32(rewChoiceItemDisplayId[4]);
        data << uint32(quest->GetRewArenaPoints());
        data << uint32(quest->GetPOIContinent());
        data << uint32(quest->GetNextQuestInChain());
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemId[0]);

        data.WriteString(portraitGiverName);

        data << uint32(rewChoiceItemDisplayId[3]);
        data << uint32(quest->ItemDrop[3]);
        data << float(quest->GetPOIx());
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemId[2]);
        data << uint32(rewChoiceItemDisplayId[0]);
        data << uint32(hiddenReward ? 0 : quest->RewardItemCount[3]);
        data << uint32(quest->GetSoundAccept());
        data << uint32(hiddenReward ? 0 : quest->RewardItemId[2]);
        data << float(1.0f); // unk (old: rewardhonormultiplier)
        data << uint32(quest->GetRewTitle());

        data.WriteString(portraitTurnInText);

        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemCount[3]);
        data << uint32(quest->ItemDropQuantity[0]);
        data << int32(quest->GetZoneOrSort());
        data << uint32(quest->GetRewardSkillId());
        data << uint32(hiddenReward ? 0 : quest->RewardChoiceItemCount[0]);
        data << uint32(quest->GetRewDisplaySpell());
        data << uint32(quest->GetQuestGiverPortrait());
        data << uint32(rewChoiceItemDisplayId[5]);
        data << uint32(quest->ItemDropQuantity[3]);
        data << uint32(quest->GetFlags());
        data << uint32(quest->GetQuestPackageID());
        data << uint32(quest->GetSrcItemId());
    }
    else
        data.FlushBits();

    _session->SendPacket(&data);

    TC_LOG_DEBUG("network", "WORLD: Sent SMSG_QUEST_QUERY_RESPONSE questid=%u", quest->GetQuestId());
}

void PlayerMenu::SendQuestGiverOfferReward(Quest const* quest, ObjectGuid guid, bool enableNext) const
{
    std::string questTitle              = quest->GetLogTitle();
    std::string questOfferRewardText    = quest->GetOfferRewardText();
    std::string portraitGiverText       = quest->GetPortraitGiverText();
    std::string portraitGiverName       = quest->GetPortraitGiverName();
    std::string portraitTurnInText      = quest->GetPortraitTurnInText();
    std::string portraitTurnInName      = quest->GetPortraitTurnInName();

    if (sWorld->getBoolConfig(CONFIG_UI_QUESTLEVELS_IN_DIALOGS))
        AddQuestLevelToTitle(questTitle, quest->GetQuestLevel());

    uint32 rewItemDisplayId[QUEST_REWARD_ITEM_COUNT];
    for (uint8 i = 0; i < QUEST_REWARD_ITEM_COUNT; i++)
    {
        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(quest->RewardItemId[i]);
        rewItemDisplayId[i] = itemTemplate ? itemTemplate->GetDisplayId() : 0;
    }

    uint32 rewChoiceItemDisplayId[QUEST_REWARD_CHOICES_COUNT];
    for (uint8 i = 0; i < QUEST_REWARD_CHOICES_COUNT; i++)
    {
        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(quest->RewardChoiceItemId[i]);
        rewChoiceItemDisplayId[i] = itemTemplate ? itemTemplate->GetDisplayId() : 0;
    }

    uint32 rewEmoteCount = 0;
    for (uint8 i = 0; i < QUEST_EMOTE_COUNT; i++)
    {
        if (quest->OfferRewardEmote[i] <= 0)
            break;

        rewEmoteCount++;
    }

    uint32 QuestEnderID = guid.GetEntry();

    WorldPacket data(SMSG_QUEST_GIVER_OFFER_REWARD, 1 + 8 + 16 + QUEST_REWARD_REPUTATIONS_COUNT * (4 + 4 + 4) + 96 + QUEST_REWARD_CURRENCY_COUNT * (4 + 4) +
                    84 + 18 + questTitle.length() + questOfferRewardText.length() + portraitGiverText.length() + portraitGiverName.length() +
                    portraitTurnInText.length() + portraitTurnInName.length() + rewEmoteCount * (4 + 4));

    data << uint32(quest->RewardItemCount[2]);
    data << uint32(quest->GetQuestId());
    data << uint32(quest->RewardItemId[3]);
    data << uint32(rewChoiceItemDisplayId[2]);

    for (uint8 i = 0; i < QUEST_REWARD_REPUTATIONS_COUNT; i++)
    {
        data << uint32(quest->RewardFactionId[i]);
        data << uint32(quest->RewardFactionValue[i]);
        data << uint32(quest->RewardFactionOverride[i]);
    }

    data << uint32(quest->RewardItemCount[0]);
    data << uint32(quest->RewardItemCount[3]);
    data << uint32(rewItemDisplayId[3]);
    data << uint32(quest->RewardItemId[1]);
    data << uint32(quest->RewardChoiceItemId[3]);
    data << uint32(rewChoiceItemDisplayId[3]);
    data << uint32(quest->GetRewChoiceItemsCount());
    data << uint32(quest->GetRewDisplaySpell());
    data << uint32(rewItemDisplayId[1]);
    data << uint32(quest->RewardChoiceItemCount[5]);
    data << uint32(rewChoiceItemDisplayId[4]);
    data << uint32(quest->RewardChoiceItemCount[1]);
    data << uint32(rewChoiceItemDisplayId[0]);
    data << uint32(rewItemDisplayId[0]);
    data << uint32(quest->GetQuestPackageID());
    data << uint32(quest->GetQuestTurnInPortrait());
    data << uint32(quest->RewardItemCount[1]);
    data << uint32(quest->SuggestedPlayers);
    data << uint32(quest->RewardChoiceItemId[0]);
    data << uint32(quest->RewardChoiceItemCount[3]);
    data << uint32(quest->RewardChoiceItemCount[4]);
    data << uint32(quest->RewardChoiceItemId[1]);
    data << uint32(quest->GetBonusTalents());
    data << uint32(quest->GetRewardSkillId());

    for (uint8 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; i++)
    {
        uint32 currencyId = quest->RewardCurrencyId[i];
        CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(currencyId);

        int32 precision = (currency && (currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION)) ? CURRENCY_PRECISION : 1;
        int32 currencyCount = (quest->RewardCurrencyCount[i] + quest->RewardBonusCurrencyCount[i]) * precision;

        data << uint32(currencyId);
        data << uint32(currencyCount);
    }

    data << uint32(quest->GetFlags());
    data << uint32(quest->GetFlags2());
    data << uint32(quest->XPValue(_session->GetPlayer()->GetLevel()) * sWorld->getRate(RATE_XP_QUEST));
    data << uint32(quest->GetRewTitle());
    data << uint32(quest->RewardChoiceItemId[2]);
    data << uint32(quest->GetRewItemsCount());
    data << uint32(quest->GetRewardReputationMask());
    data << uint32(quest->RewardChoiceItemId[4]);
    data << uint32(QuestEnderID);
    data << uint32(quest->RewardItemId[2]);
    data << uint32(quest->RewardChoiceItemCount[0]);
    data << uint32(quest->GetRewardSkillPoints());
    data << uint32(quest->GetRewSpell());
    data << uint32(_session->GetPlayer()->GetQuestMoneyReward(quest));
    data << uint32(quest->RewardChoiceItemId[5]);
    data << uint32(rewChoiceItemDisplayId[1]);
    data << uint32(quest->RewardChoiceItemCount[2]);
    data << uint32(rewItemDisplayId[2]);
    data << uint32(quest->GetQuestGiverPortrait());
    data << uint32(quest->RewardItemId[0]);
    data << uint32(rewChoiceItemDisplayId[5]);

    data.WriteBits(portraitTurnInText.size(), 10);
    data.WriteBits(portraitGiverName.size(), 8);

    data.WriteGuidMask(guid, 6);

    data.WriteBits(rewEmoteCount, 21);

    data.WriteGuidMask(guid, 3, 7);

    data.WriteBits(questTitle.size(), 9);

    data.WriteGuidMask(guid, 4);

    data.WriteBits(portraitTurnInName.size(), 8);
    data.WriteBits(portraitGiverText.size(), 10);
    data.WriteBits(questOfferRewardText.size(), 12);

    data.WriteGuidMask(guid, 1, 2, 0, 5);

    data.WriteBit(enableNext);

    data.WriteString(portraitGiverName);
    data.WriteString(questTitle);

    for (uint8 i = 0; i < rewEmoteCount; i++)
    {
        data << uint32(quest->OfferRewardEmoteDelay[i]);
        data << uint32(quest->OfferRewardEmote[i]);
    }

    data.WriteGuidBytes(guid, 2);

    data.WriteString(questOfferRewardText);
    data.WriteString(portraitTurnInText);
    data.WriteString(portraitTurnInName);

    data.WriteGuidBytes(guid, 5, 1);

    data.WriteString(portraitGiverText);

    data.WriteGuidBytes(guid, 0, 7, 6, 4, 3);

    _session->SendPacket(&data);

    TC_LOG_DEBUG("network", "WORLD: Sent SMSG_QUEST_GIVER_OFFER_REWARD NPC=%s, questid=%u", guid.ToString().c_str(), quest->GetQuestId());
}

void PlayerMenu::SendQuestGiverRequestItems(Quest const* quest, ObjectGuid guid, bool canComplete, bool closeOnCancel) const
{
    // We can always call to RequestItems, but this packet only goes out if there are actually
    // items.  Otherwise, we'll skip straight to the OfferReward

    if (!quest->HasSpecialFlag(QUEST_SPECIAL_FLAGS_DELIVER) && canComplete)
    {
        SendQuestGiverOfferReward(quest, guid, true);
        return;
    }

    std::string questTitle = quest->GetLogTitle();
    std::string requestItemsText = quest->GetRequestItemsText();

    uint32 requiredMoney = 0;

    ByteBuffer currencyData;
    ByteBuffer itemData;

    uint32 currencyCounter = 0;
    uint32 itemCounter = 0;
    for (QuestObjective const& questObjective : quest->GetObjectives())
    {
        switch (questObjective.Type)
        {
            case QUEST_OBJECTIVE_ITEM:
            {
                if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(questObjective.ObjectID))
                    itemData << uint32(itemTemplate->GetDisplayId());
                else
                    itemData << uint32(0);

                itemData << uint32(questObjective.ObjectID);
                itemData << uint32(questObjective.Amount);
                ++itemCounter;
                break;
            }
            case QUEST_OBJECTIVE_CURRENCY:
            {
                currencyData << uint32(questObjective.ObjectID);
                currencyData << uint32(questObjective.Amount);
                ++currencyCounter;
                break;
            }
            case QUEST_OBJECTIVE_MONEY:
            {
                requiredMoney = questObjective.Amount;
                break;
            }
            default:
                break;
        }
    }

    if (sWorld->getBoolConfig(CONFIG_UI_QUESTLEVELS_IN_DIALOGS))
        AddQuestLevelToTitle(questTitle, quest->GetQuestLevel());

    uint32 QuestEnderID = guid.GetEntry();

    WorldPacket data(SMSG_QUEST_GIVER_REQUEST_ITEMS, 1 + 8 + 18 + 8 + questTitle.length() + requestItemsText.length() +
                    itemCounter * (4 + 4) + currencyCounter * (4 + 4));

    data << uint32(quest->GetSuggestedPlayers());
    data << uint32(quest->GetFlags());
    data << uint32(quest->GetFlags2());
    data << uint32(canComplete ? 0x5F : 0x5B);  // status flags
    data << uint32(requiredMoney);
    data << uint32(QuestEnderID);
    data << uint32(canComplete ? quest->EmoteOnCompleteDelay : quest->EmoteOnIncompleteDelay);
    data << uint32(canComplete ? quest->GetCompleteEmote() : quest->GetIncompleteEmote());
    data << uint32(quest->GetQuestId());

    data.WriteBits(currencyCounter, 21);

    data.WriteBit(closeOnCancel);

    data.WriteGuidMask(guid, 2, 5, 1);

    data.WriteBits(questTitle.size(), 9);
    data.WriteBits(requestItemsText.size(), 12);

    data.WriteGuidMask(guid, 6, 0);

    data.WriteBits(itemCounter, 20);

    data.WriteGuidMask(guid, 4, 7, 3);

    data.WriteGuidBytes(guid, 0, 2);

    data.WriteString(questTitle);

    data.append(currencyData);
    data.append(itemData);

    data.WriteGuidBytes(guid, 3, 1);

    data.WriteString(requestItemsText);

    data.WriteGuidBytes(guid, 4, 5, 7, 6);

    _session->SendPacket(&data);

    TC_LOG_DEBUG("network", "WORLD: Sent SMSG_QUEST_GIVER_REQUEST_ITEMS NPC=%s, questid=%u", guid.ToString().c_str(), quest->GetQuestId());
}

void PlayerMenu::AddQuestLevelToTitle(std::string &title, int32 level)
{
    // Adds the quest level to the front of the quest title
    // example: [13] Westfall Stew

    std::stringstream questTitlePretty;
    questTitlePretty << "[" << level << "] " << title;
    title = questTitlePretty.str();
}
