/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Unit.h"
#include "Player.h"
#include "Pet.h"
#include "Creature.h"
#include "SharedDefines.h"
#include "SpellAuras.h"
#include "SpellAuraEffects.h"
#include "SpellMgr.h"
#include "World.h"

inline bool _ModifyUInt32(bool apply, uint32& baseValue, int32& amount)
{
    // If amount is negative, change sign and value of apply.
    if (amount < 0)
    {
        apply = !apply;
        amount = -amount;
    }
    if (apply)
        baseValue += amount;
    else
    {
        // Make sure we do not get uint32 overflow.
        if (amount > int32(baseValue))
            amount = baseValue;
        baseValue -= amount;
    }
    return apply;
}

/*#######################################
########                         ########
########   PLAYERS STAT SYSTEM   ########
########                         ########
#######################################*/

bool Player::UpdateStats(Stats stat)
{
    if (stat > STAT_SPIRIT)
        return false;

    float value  = GetTotalStatValue(stat);

    SetStat(stat, int32(value));

    if (stat == STAT_STAMINA || stat == STAT_INTELLECT || stat == STAT_STRENGTH)
    {
        Pet* pet = GetPet();
        if (pet)
            pet->UpdateStats(stat);
    }

    switch (stat)
    {
        case STAT_AGILITY:
            UpdateArmor();
            UpdateAllCritPercentages();
            UpdateDodgePercentage();
            UpdateAttackPowerAndDamage(false);
            UpdateAttackPowerAndDamage(true);
            break;
        case STAT_STAMINA:
            UpdateMaxHealth();
            break;
        case STAT_INTELLECT:
            UpdateAllSpellCritChances();
            UpdateArmor();                                  //SPELL_AURA_MOD_RESISTANCE_OF_INTELLECT_PERCENT, only armor currently
            break;
        case STAT_SPIRIT:
            break;
        case STAT_STRENGTH:
            UpdateAttackPowerAndDamage(false);
            break;
        default:
            break;
    }

    UpdateSpellDamageAndHealingBonus();
    UpdateManaRegen();
    UpdateEnergyRegen();
    UpdateFocusRegen();

    // Update ratings in exist SPELL_AURA_MOD_RATING_FROM_STAT and only depends from stat
    uint32 mask = 0;
    AuraEffectList const& modRatingFromStat = GetAuraEffectsByType(SPELL_AURA_MOD_RATING_FROM_STAT);
    for (AuraEffectList::const_iterator i = modRatingFromStat.begin(); i != modRatingFromStat.end(); ++i)
        if (Stats((*i)->GetMiscValueB()) == stat)
            mask |= (*i)->GetMiscValue();
    if (mask)
    {
        for (uint32 rating = 0; rating < MAX_COMBAT_RATING; ++rating)
            if (mask & (1 << rating))
                ApplyRatingMod(CombatRating(rating), 0, true);
    }
    return true;
}

void Player::UpdatePvpPower()
{
    float precision = 100.0f;
    float value = GetRatingBonusValue(CR_PVP_POWER);
    value += GetTotalAuraModifier(SPELL_AURA_MOD_RATING);

    float pvpHealing = value / precision;
    float pvpDamage = value / precision;

    SetFloatValue(PLAYER_PVP_POWER_HEALING, pvpHealing);
    SetFloatValue(PLAYER_PVP_POWER_DAMAGE, pvpDamage);
}

void Player::ApplySpellPowerBonus(int32 amount, bool apply)
{
    if (HasAuraType(SPELL_AURA_OVERRIDE_SPELL_POWER_BY_AP_PCT))
        return;

    apply = _ModifyUInt32(apply, m_baseSpellPower, amount);

    // For speed just update for client
    ApplyModUInt32Value(PLAYER_MOD_HEALING_DONE_POS, amount, apply);
    for (int i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
        ApplyModUInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + i, amount, apply);

    if (HasAuraType(SPELL_AURA_OVERRIDE_ATTACK_POWER_BY_SP_PCT))
        UpdateAttackPowerAndDamage();
}

void Player::UpdateSpellDamageAndHealingBonus()
{
    // Magic damage modifiers implemented in Unit::SpellDamageBonusDone
    // This information for client side use only
    // Get healing bonus for all schools
    SetStatInt32Value(PLAYER_MOD_HEALING_DONE_POS, SpellBaseHealingBonusDone(SPELL_SCHOOL_MASK_ALL));
    // Get damage bonus for all schools
    Unit::AuraEffectList const& modDamageAuras = GetAuraEffectsByType(SPELL_AURA_MOD_DAMAGE_DONE);
    for (uint16 i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
    {
        SetInt32Value(PLAYER_MOD_DAMAGE_DONE_NEG + i, std::accumulate(modDamageAuras.begin(), modDamageAuras.end(), 0, [i](int32 negativeMod, AuraEffect const* aurEff)
        {
            if (aurEff->GetAmount() < 0 && aurEff->GetMiscValue() & (1 << i))
                negativeMod += aurEff->GetAmount();
            return negativeMod;
        }));
        SetStatInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + i, SpellBaseDamageBonusDone(SpellSchoolMask(1 << i)) - GetInt32Value(PLAYER_MOD_DAMAGE_DONE_NEG + i));
    }

    if (HasAuraType(SPELL_AURA_OVERRIDE_ATTACK_POWER_BY_SP_PCT))
        UpdateAttackPowerAndDamage();
}

bool Player::UpdateAllStats()
{
    for (uint8 i = STAT_STRENGTH; i < MAX_STATS; ++i)
    {
        float value = GetTotalStatValue(Stats(i));
        SetStat(Stats(i), int32(value));
    }

    UpdateArmor();
    // calls UpdateAttackPowerAndDamage() in UpdateArmor for SPELL_AURA_MOD_ATTACK_POWER_OF_ARMOR
    UpdateAttackPowerAndDamage(true);
    UpdateMaxHealth();

    for (uint8 i = POWER_MANA; i < MAX_POWERS; ++i)
        UpdateMaxPower(Powers(i));

    UpdateAllRatings();
    UpdateAllCritPercentages();
    UpdateAllSpellCritChances();
    UpdateBlockPercentage();
    UpdateParryPercentage();
    UpdateDodgePercentage();
    UpdateSpellDamageAndHealingBonus();
    UpdateManaRegen();
    UpdateEnergyRegen();
    UpdateFocusRegen();
    UpdateExpertise(BASE_ATTACK);
    UpdateExpertise(OFF_ATTACK);
    UpdateExpertise(RANGED_ATTACK);
    UpdateMastery();
    UpdateAllResistances();

    return true;
}

void Player::ApplySpellPenetrationBonus(int32 amount, bool apply)
{
    ApplyModInt32Value(PLAYER_MOD_TARGET_RESISTANCE, -amount, apply);
    m_spellPenetrationItemMod += apply ? amount : -amount;
}

void Player::UpdateResistances(uint32 school)
{
    if (school > SPELL_SCHOOL_NORMAL)
    {
        float value  = GetTotalAuraModValue(UnitMods(UNIT_MOD_RESISTANCE_START + school));
        SetResistance(SpellSchools(school), int32(value));

        Pet* pet = GetPet();
        if (pet)
            pet->UpdateResistances(school);
    }
    else
        UpdateArmor();
}

void Player::UpdateArmor()
{
    UnitMods unitMod = UNIT_MOD_ARMOR;

    float value = GetFlatModifierValue(unitMod, BASE_VALUE);    // base armor (from items)
    value *= GetPctModifierValue(unitMod, BASE_PCT);            // armor percent from items
    value += GetFlatModifierValue(unitMod, TOTAL_VALUE);

    //add dynamic flat mods
    AuraEffectList const& mResbyIntellect = GetAuraEffectsByType(SPELL_AURA_MOD_RESISTANCE_OF_STAT_PERCENT);
    for (AuraEffectList::const_iterator i = mResbyIntellect.begin(); i != mResbyIntellect.end(); ++i)
    {
        if ((*i)->GetMiscValue() & SPELL_SCHOOL_MASK_NORMAL)
            value += CalculatePct(GetStat(Stats((*i)->GetMiscValueB())), (*i)->GetAmount());
    }

    value *= GetPctModifierValue(unitMod, TOTAL_PCT);

    SetArmor(int32(value));

    Pet* pet = GetPet();
    if (pet)
        pet->UpdateArmor();

    UpdateAttackPowerAndDamage();                           // armor dependent auras update for SPELL_AURA_MOD_ATTACK_POWER_OF_ARMOR
}

float Player::GetHealthBonusFromStamina()
{
    // Taken from PaperDollFrame.lua - 5.4.8 18414
    float ratio = 14.0f;
    if (GtOCTHpPerStaminaEntry const* hpBase = sGtOCTHpPerStaminaStore.EvaluateTable(GetLevel() - 1, 0))
        ratio = hpBase->Ratio;

    float stamina = GetStat(STAT_STAMINA);
    return stamina * ratio;
}

void Player::UpdateMaxHealth()
{
    UnitMods unitMod = UNIT_MOD_HEALTH;

    float value = GetFlatModifierValue(unitMod, BASE_VALUE) + GetCreateHealth();
    value *= GetPctModifierValue(unitMod, BASE_PCT);
    value += GetFlatModifierValue(unitMod, TOTAL_VALUE) + GetHealthBonusFromStamina();
    value *= GetPctModifierValue(unitMod, TOTAL_PCT);

    SetMaxHealth((uint32)value);
}

void Player::UpdateMaxPower(Powers power)
{
    UnitMods unitMod = UnitMods(UNIT_MOD_POWER_START + power);

    float value = GetFlatModifierValue(unitMod, BASE_VALUE) + GetCreatePowers(power);
    value *= GetPctModifierValue(unitMod, BASE_PCT);
    value += GetFlatModifierValue(unitMod, TOTAL_VALUE);
    value *= GetPctModifierValue(unitMod, TOTAL_PCT);

    SetMaxPower(power, int32(value));
}

void Player::UpdateAttackPowerAndDamage(bool ranged)
{
    float val2 = 0.0f;
    float level = float(GetLevel());

    ChrClassesEntry const* entry = sChrClassesStore.AssertEntry(GetClass());
    UnitMods unitMod_pos = ranged ? UNIT_MOD_ATTACK_POWER_RANGED_POS : UNIT_MOD_ATTACK_POWER_POS;
    UnitMods unitMod_neg = ranged ? UNIT_MOD_ATTACK_POWER_RANGED_NEG : UNIT_MOD_ATTACK_POWER_NEG;

    uint16 index = UNIT_ATTACK_POWER;
    uint16 index_mod_pos = UNIT_ATTACK_POWER_MOD_POS;
    uint16 index_mod_neg = UNIT_ATTACK_POWER_MOD_NEG;
    uint16 index_mult = UNIT_ATTACK_POWER_MULTIPLIER;

    if (ranged)
    {
        index = UNIT_RANGED_ATTACK_POWER;
        index_mod_pos = UNIT_RANGED_ATTACK_POWER_MOD_POS;
        index_mod_neg = UNIT_RANGED_ATTACK_POWER_MOD_NEG;
        index_mult = UNIT_RANGED_ATTACK_POWER_MULTIPLIER;
    }

    if (!HasAuraType(SPELL_AURA_OVERRIDE_ATTACK_POWER_BY_SP_PCT))
    {
        if (!ranged)
        {
            float strengthValue = std::max(GetStat(STAT_STRENGTH) * entry->AttackPowerPerStrength, 0.0f);
            float agilityValue = std::max(GetStat(STAT_AGILITY) * entry->AttackPowerPerAgility, 0.0f);

            SpellShapeshiftFormEntry const* form = sSpellShapeshiftFormStore.LookupEntry(GetShapeshiftForm());
            // Directly taken from client, SHAPESHIFT_FLAG_AP_FROM_STRENGTH ?
            if (form && form->Flags & 0x20)
                agilityValue += std::max(GetStat(STAT_AGILITY) * entry->AttackPowerPerStrength, 0.0f);

            val2 = strengthValue + agilityValue;
        }
        else
            val2 = (level + std::max(GetStat(STAT_AGILITY), 0.0f)) * entry->RangedAttackPowerPerAgility;
    }
    else
    {
        int32 minSpellPower = GetInt32Value(PLAYER_MOD_HEALING_DONE_POS);
        for (int i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
            minSpellPower = std::min(minSpellPower, GetInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + i));

        val2 = CalculatePct(float(minSpellPower), GetFloatValue(PLAYER_OVERRIDE_AP_BY_SPELL_POWER_PERCENT));
    }

    SetStatFlatModifier(unitMod_pos, BASE_VALUE, val2);

    float base_attPower  = (GetFlatModifierValue(unitMod_pos, BASE_VALUE) - GetFlatModifierValue(unitMod_neg, BASE_VALUE)) * (GetPctModifierValue(unitMod_pos, BASE_PCT) - GetPctModifierValue(unitMod_neg, BASE_PCT));
    float attPowerMod_pos = GetFlatModifierValue(unitMod_pos, TOTAL_VALUE);
    float attPowerMod_neg = GetFlatModifierValue(unitMod_neg, TOTAL_VALUE);

    //add dynamic flat mods
    if (!ranged)
    {
        AuraEffectList const& mAPbyArmor = GetAuraEffectsByType(SPELL_AURA_MOD_ATTACK_POWER_OF_ARMOR);
        for (AuraEffectList::const_iterator iter = mAPbyArmor.begin(); iter != mAPbyArmor.end(); ++iter)
        {
            int32 temp = int32(GetArmor() / (*iter)->GetAmount());
            if (temp > 0)
                attPowerMod_pos += temp;
            else
                attPowerMod_neg += temp;
        }
    }

    float attPowerMultiplier = (GetPctModifierValue(unitMod_pos, TOTAL_PCT) - GetPctModifierValue(unitMod_neg, TOTAL_PCT)) - 1.0f;

    SetInt32Value(index, (uint32)base_attPower);            //UNIT_(RANGED)_ATTACK_POWER field
    SetInt32Value(index_mod_pos, (uint32)attPowerMod_pos);  //UNIT_(RANGED)_ATTACK_POWER_MOD_POS field
    SetInt32Value(index_mod_neg, (uint32)attPowerMod_neg);  //UNIT_(RANGED)_ATTACK_POWER_MOD_NEG field
    SetFloatValue(index_mult, attPowerMultiplier);          //UNIT_(RANGED)_ATTACK_POWER_MULTIPLIER field

    Pet* pet = GetPet();                                //update pet's AP
    Guardian* guardian = GetGuardianPet();
    //automatically update weapon damage after attack power modification
    if (ranged)
    {
        UpdateDamagePhysical(RANGED_ATTACK);
        if (pet && pet->IsHunterPet()) // At ranged attack change for hunter pet
            pet->UpdateAttackPowerAndDamage();
    }
    else
    {
        UpdateDamagePhysical(BASE_ATTACK);
        if (Item* offhand = GetWeaponForAttack(OFF_ATTACK, true))
            if (CanDualWield() || offhand->GetTemplate()->GetFlags3() & ITEM_PROTO_FLAG3_ALWAYS_ALLOW_DUAL_WIELD)
                UpdateDamagePhysical(OFF_ATTACK);

        if (HasAuraType(SPELL_AURA_MOD_SPELL_DAMAGE_OF_ATTACK_POWER) ||
            HasAuraType(SPELL_AURA_MOD_SPELL_HEALING_OF_ATTACK_POWER) ||
            HasAuraType(SPELL_AURA_OVERRIDE_SPELL_POWER_BY_AP_PCT))
            UpdateSpellDamageAndHealingBonus();

        if (pet && pet->IsPetGhoul()) // At melee attack power change for DK pet
            pet->UpdateAttackPowerAndDamage();

        if (guardian && guardian->IsSpiritWolf()) // At melee attack power change for Shaman feral spirit
            guardian->UpdateAttackPowerAndDamage();
    }
}

void Player::CalculateMinMaxDamage(WeaponAttackType attType, bool normalized, bool addTotalPct, float& minDamage, float& maxDamage) const
{
    UnitMods unitMod;

    switch (attType)
    {
        case BASE_ATTACK:
        default:
            unitMod = UNIT_MOD_DAMAGE_MAINHAND;
            break;
        case OFF_ATTACK:
            unitMod = UNIT_MOD_DAMAGE_OFFHAND;
            break;
        case RANGED_ATTACK:
            unitMod = UNIT_MOD_DAMAGE_RANGED;
            break;
    }

    float attackPowerMod = std::max(GetAPMultiplier(attType, normalized), 0.25f);

    float baseValue  = GetFlatModifierValue(unitMod, BASE_VALUE) + GetTotalAttackPowerValue(attType) / 14.0f * attackPowerMod;
    float basePct    = GetPctModifierValue(unitMod, BASE_PCT);
    float totalValue = GetFlatModifierValue(unitMod, TOTAL_VALUE);
    float totalPct   = addTotalPct ? GetPctModifierValue(unitMod, TOTAL_PCT) : 1.0f;

    float weaponMinDamage = GetWeaponDamageRange(attType, MINDAMAGE);
    float weaponMaxDamage = GetWeaponDamageRange(attType, MAXDAMAGE);

    SpellShapeshiftFormEntry const* shapeshift = sSpellShapeshiftFormStore.LookupEntry(GetShapeshiftForm());
    if (shapeshift && shapeshift->CombatRoundTime)
    {
        weaponMinDamage = weaponMinDamage * shapeshift->CombatRoundTime / 1000.0f / attackPowerMod;
        weaponMaxDamage = weaponMaxDamage * shapeshift->CombatRoundTime / 1000.0f / attackPowerMod;
    }
    else if (!CanUseAttackType(attType)) // check if player not in form but still can't use (disarm case)
    {
        // cannot use ranged/off attack, set values to 0
        if (attType != BASE_ATTACK)
        {
            minDamage = 0;
            maxDamage = 0;
            return;
        }
        weaponMinDamage = BASE_MINDAMAGE;
        weaponMaxDamage = BASE_MAXDAMAGE;
    }

    minDamage = ((weaponMinDamage + baseValue) * basePct + totalValue) * totalPct;
    maxDamage = ((weaponMaxDamage + baseValue) * basePct + totalValue) * totalPct;
}

void Creature::CalculateMinMaxDamage(WeaponAttackType attType, bool normalized, bool addTotalPct, float& minDamage, float& maxDamage) const
{
    float variance = 1.0f;
    UnitMods unitMod;
    switch (attType)
    {
        case BASE_ATTACK:
        default:
            variance = GetCreatureTemplate()->BaseVariance;
            unitMod = UNIT_MOD_DAMAGE_MAINHAND;
            break;
        case OFF_ATTACK:
            variance = GetCreatureTemplate()->BaseVariance;
            unitMod = UNIT_MOD_DAMAGE_OFFHAND;
            break;
        case RANGED_ATTACK:
            variance = GetCreatureTemplate()->RangeVariance;
            unitMod = UNIT_MOD_DAMAGE_RANGED;
            break;
    }

    if (attType == OFF_ATTACK && !HaveOffhandWeapon())
    {
        minDamage = 0.0f;
        maxDamage = 0.0f;
        return;
    }

    float weaponMinDamage = GetWeaponDamageRange(attType, MINDAMAGE);
    float weaponMaxDamage = GetWeaponDamageRange(attType, MAXDAMAGE);

    if (!CanUseAttackType(attType)) // disarm case
    {
        weaponMinDamage = 0.0f;
        weaponMaxDamage = 0.0f;
    }

    float attackSpeedMulti = GetAPMultiplier(attType, normalized);
    float baseValue = GetFlatModifierValue(unitMod, BASE_VALUE) + (GetTotalAttackPowerValue(attType) / 14.0f) * variance;
    float basePct = GetPctModifierValue(unitMod, BASE_PCT) * attackSpeedMulti;
    float totalValue = GetFlatModifierValue(unitMod, TOTAL_VALUE);
    float totalPct = addTotalPct ? GetPctModifierValue(unitMod, TOTAL_PCT) : 1.0f;
    float dmgMultiplier = GetCreatureTemplate()->ModDamage; // = ModDamage * _GetDamageMod(rank);

    minDamage = ((weaponMinDamage + baseValue) * dmgMultiplier * basePct + totalValue) * totalPct;
    maxDamage = ((weaponMaxDamage + baseValue) * dmgMultiplier * basePct + totalValue) * totalPct;
}

void Player::UpdateBlockPercentage()
{
    // No block
    float value = 0.0f;
    if (CanBlock())
    {
        // Base value
        value = 5.0f;
        // Increase from SPELL_AURA_MOD_BLOCK_PERCENT aura
        value += GetTotalAuraModifier(SPELL_AURA_MOD_BLOCK_PERCENT);

        // 76671 - Mastery : Divine Bulwark - Block Percentage
        if (GetTypeId() == TYPEID_PLAYER && HasAura(76671))
            value += GetFloatValue(PLAYER_MASTERY);

        // Custom MoP Script
        // 76857 - Mastery : Critical Block - Block Percentage
        if (GetTypeId() == TYPEID_PLAYER && HasAura(76857))
            value += GetFloatValue(PLAYER_MASTERY) / 2.0f;

        // Increase from rating
        value += GetRatingBonusValue(CR_BLOCK);

        if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
             value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_BLOCK) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_BLOCK) : value;

        value = value < 0.0f ? 0.0f : value;
    }
    SetStatFloatValue(PLAYER_BLOCK_PERCENTAGE, value);
}

void Player::UpdateCritPercentage(WeaponAttackType attType)
{
    BaseModGroup modGroup;
    uint16 index;
    CombatRating cr;

    switch (attType)
    {
        case OFF_ATTACK:
            modGroup = OFFHAND_CRIT_PERCENTAGE;
            index = PLAYER_OFFHAND_CRIT_PERCENTAGE;
            cr = CR_CRIT_MELEE;
            break;
        case RANGED_ATTACK:
            modGroup = RANGED_CRIT_PERCENTAGE;
            index = PLAYER_RANGED_CRIT_PERCENTAGE;
            cr = CR_CRIT_RANGED;
            break;
        case BASE_ATTACK:
        default:
            modGroup = CRIT_PERCENTAGE;
            index = PLAYER_CRIT_PERCENTAGE;
            cr = CR_CRIT_MELEE;
            break;
    }

    // flat = bonus from crit auras, pct = bonus from agility, combat rating = mods from items
    float value = GetBaseModValue(modGroup, FLAT_MOD) + GetBaseModValue(modGroup, PCT_MOD) + GetRatingBonusValue(cr);

    // Modify crit from weapon skill and maximized defense skill of same level victim difference
    value += (int32(GetMaxSkillValueForLevel()) - int32(GetMaxSkillValueForLevel())) * 0.04f;

    if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
         value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_CRIT) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_CRIT) : value;

    value = std::max(0.0f, value);
    SetStatFloatValue(index, value);
}

void Player::UpdateAllCritPercentages()
{
    float value = GetMeleeCritFromAgility();

    SetBaseModPctValue(CRIT_PERCENTAGE, value);
    SetBaseModPctValue(OFFHAND_CRIT_PERCENTAGE, value);
    SetBaseModPctValue(RANGED_CRIT_PERCENTAGE, value);

    UpdateCritPercentage(BASE_ATTACK);
    UpdateCritPercentage(OFF_ATTACK);
    UpdateCritPercentage(RANGED_ATTACK);
}

void Player::UpdateMastery()
{
    float value = 0.0f;

    if (HasAuraType(SPELL_AURA_MASTERY) && GetLevel() >= 80)
    {
        // Mastery from SPELL_AURA_MASTERY aura
        value += GetTotalAuraModifier(SPELL_AURA_MASTERY);
        // Mastery from rating
        value += GetRatingBonusValue(CR_MASTERY);
        value = value < 0.0f ? 0.0f : value;
    }

    SetFloatValue(PLAYER_MASTERY, value);

    // 76671 - Mastery : Divine Bulwark - Update Block Percentage
    // 76857 - Mastery : Critical Block - Update Block Percentage
    if (HasAura(76671) || HasAura(76857))
        UpdateBlockPercentage();
    // 77494 - Mastery : Nature's Guardian - Update Armor
    if (HasAura(77494))
        UpdateArmor();
}

const float m_diminishing_k[MAX_CLASSES] =
{
    0.9560f,  // Warrior
    0.9560f,  // Paladin
    0.9880f,  // Hunter
    0.9880f,  // Rogue
    0.9830f,  // Priest
    0.9560f,  // DK
    0.9880f,  // Shaman
    0.9830f,  // Mage
    0.9830f,  // Warlock
    0.9880f,  // Monk
    0.9720f   // Druid
};

// helper function
float CalculateDiminishingReturns(float const (&capArray)[MAX_CLASSES], uint8 playerClass, float nonDiminishValue, float diminishValue)
{
    //  1     1     k              cx
    // --- = --- + --- <=> x' = --------
    //  x'    c     x            x + ck

    // where:
    // k  is m_diminishing_k for that class
    // c  is capArray for that class
    // x  is chance before DR (diminishValue)
    // x' is chance after DR (our result)

    uint32 const classIdx = playerClass - 1;

    float const k = m_diminishing_k[classIdx];
    float const c = capArray[classIdx];

    float result = c * diminishValue / (diminishValue + c * k);
    result += nonDiminishValue;
    return result;
}

const float parry_cap[MAX_CLASSES] =
{
    47.003525f,     // Warrior
    47.003525f,     // Paladin
    145.560408f,    // Hunter
    145.560408f,    // Rogue
    0.0f,           // Priest
    47.003525f,     // DK
    145.560408f,    // Shaman
    0.0f,           // Mage
    0.0f,           // Warlock
    90.6425f,       // Monk
    0.0f            // Druid
};

void Player::UpdateParryPercentage()
{
    // No parry
    float value = 0.0f;
    uint32 pclass = GetClass() - 1;
    if (CanParry() && parry_cap[pclass] > 0.0f)
    {
        float nondiminishing  = 5.0f;
        // Parry from rating
        float diminishing = GetRatingBonusValue(CR_PARRY);
        // Parry from SPELL_AURA_MOD_PARRY_PERCENT aura
        nondiminishing += GetTotalAuraModifier(SPELL_AURA_MOD_PARRY_PERCENT);
        // apply diminishing formula to diminishing parry chance
        value = CalculateDiminishingReturns(parry_cap, GetClass(), nondiminishing, diminishing);

        if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
             value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_PARRY) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_PARRY) : value;

        value = value < 0.0f ? 0.0f : value;
    }
    SetStatFloatValue(PLAYER_PARRY_PERCENTAGE, value);
}

const float dodge_cap[MAX_CLASSES] =
{
    88.129021f,     // Warrior
    88.129021f,     // Paladin
    145.560408f,    // Hunter
    145.560408f,    // Rogue
    150.375940f,    // Priest
    88.129021f,     // DK
    145.560408f,    // Shaman
    150.375940f,    // Mage
    150.375940f,    // Warlock
    145.560408f,    // Monk
    116.890707f     // Druid
};

void Player::UpdateDodgePercentage()
{
    float diminishing = 0.0f, nondiminishing = 0.0f;
    GetDodgeFromAgility(diminishing, nondiminishing);
    // Dodge from SPELL_AURA_MOD_DODGE_PERCENT aura
    nondiminishing += GetTotalAuraModifier(SPELL_AURA_MOD_DODGE_PERCENT);
    // Dodge from rating
    diminishing += GetRatingBonusValue(CR_DODGE);
    // apply diminishing formula to diminishing dodge chance
    uint32 pclass = GetClass()-1;
    float value = CalculateDiminishingReturns(dodge_cap, GetClass(), nondiminishing, diminishing);

    if (sWorld->getBoolConfig(CONFIG_STATS_LIMITS_ENABLE))
         value = value > sWorld->getFloatConfig(CONFIG_STATS_LIMITS_DODGE) ? sWorld->getFloatConfig(CONFIG_STATS_LIMITS_DODGE) : value;

    value = value < 0.0f ? 0.0f : value;
    SetStatFloatValue(PLAYER_DODGE_PERCENTAGE, value);
}

void Player::UpdateSpellCritChance(uint32 school)
{
    // For normal school set zero crit chance
    if (school == SPELL_SCHOOL_NORMAL)
    {
        SetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE, 0.0f);
        return;
    }
    // For others recalculate it from:
    float crit = 0.0f;
    // Crit from Intellect
    crit += GetSpellCritFromIntellect();
    // Increase crit from SPELL_AURA_MOD_SPELL_CRIT_CHANCE
    crit += GetTotalAuraModifier(SPELL_AURA_MOD_SPELL_CRIT_CHANCE);
    // Increase crit from SPELL_AURA_MOD_CRIT_PCT
    crit += GetTotalAuraModifier(SPELL_AURA_MOD_CRIT_PCT);
    // Increase crit by school from SPELL_AURA_MOD_SPELL_CRIT_CHANCE_SCHOOL
    crit += GetTotalAuraModifierByMiscMask(SPELL_AURA_MOD_SPELL_CRIT_CHANCE_SCHOOL, 1<<school);
    // Increase crit from spell crit ratings
    crit += GetRatingBonusValue(CR_CRIT_SPELL);

    // Store crit value
    SetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE + school, crit);

    if (Pet* pet = GetPet())
        pet->m_baseSpellCritChance = crit;
}

void Player::UpdateMeleeHitChances()
{
    m_modMeleeHitChance = 7.5f + (float)GetTotalAuraModifier(SPELL_AURA_MOD_HIT_CHANCE);
    SetStatFloatValue(PLAYER_UI_HIT_MODIFIER, m_modMeleeHitChance);
    m_modMeleeHitChance += GetRatingBonusValue(CR_HIT_MELEE);

    if (Pet* pet = GetPet())
        pet->m_modMeleeHitChance = m_modMeleeHitChance;
}

void Player::UpdateRangedHitChances()
{
    m_modRangedHitChance = 7.5f + (float)GetTotalAuraModifier(SPELL_AURA_MOD_HIT_CHANCE);
    m_modRangedHitChance += GetRatingBonusValue(CR_HIT_RANGED);

    if (Pet* pet = GetPet())
        pet->m_modRangedHitChance = m_modRangedHitChance;
}

void Player::UpdateSpellHitChances()
{
    m_modSpellHitChance = 15.0f + (float)GetTotalAuraModifierByAffectMask(SPELL_AURA_MOD_SPELL_HIT_CHANCE, nullptr);
    SetStatFloatValue(PLAYER_UI_SPELL_HIT_MODIFIER, m_modSpellHitChance);
    m_modSpellHitChance += GetRatingBonusValue(CR_HIT_SPELL);

    if (Pet* pet = GetPet())
        pet->m_modSpellHitChance = m_modSpellHitChance;
}

void Player::UpdateAllSpellCritChances()
{
    for (int i = SPELL_SCHOOL_NORMAL; i < MAX_SPELL_SCHOOL; ++i)
        UpdateSpellCritChance(i);
}

void Player::UpdateExpertise(WeaponAttackType attack)
{
    float expertise = GetRatingBonusValue(CR_EXPERTISE);

    Item const* weapon = GetWeaponForAttack(attack, true);
    expertise += GetTotalAuraModifier(SPELL_AURA_MOD_EXPERTISE, [weapon](AuraEffect const* aurEff) -> bool
    {
        return aurEff->GetSpellInfo()->IsItemFitToSpellRequirements(weapon);
    });

    if (expertise < 0.0f)
        expertise = 0.0f;

    switch (attack)
    {
        case BASE_ATTACK:
            SetFloatValue(PLAYER_MAINHAND_EXPERTISE, expertise);
            break;
        case OFF_ATTACK:
            SetFloatValue(PLAYER_OFFHAND_EXPERTISE, expertise);
            break;
        case RANGED_ATTACK:
            SetFloatValue(PLAYER_RANGED_EXPERTISE, expertise);
            break;
        default:
            break;
    }
}

void Player::ApplyManaRegenBonus(int32 amount, bool apply)
{
    _ModifyUInt32(apply, m_baseManaRegen, amount);
    UpdateManaRegen();
}

void Player::ApplyHealthRegenBonus(int32 amount, bool apply)
{
    _ModifyUInt32(apply, m_baseHealthRegen, amount);
}

void Player::UpdateManaRegen()
{
    if (GetPowerType() != POWER_MANA)
        return;

    // MoP Mana regen system
    float base_regen = GetMaxPower(POWER_MANA) * 0.02f;

    // Mana regen from spirit
    float spirit_regen = OCTRegenMPPerSpirit();
    spirit_regen *= GetTotalAuraMultiplierByMiscValue(SPELL_AURA_MOD_POWER_REGEN_PERCENT, POWER_MANA);
    spirit_regen *= GetTotalAuraMultiplierByMiscValue(SPELL_AURA_MOD_POWER_REGEN_PERCENT_2, POWER_MANA);

    // Bonus Mp5 from auras (like repleinshment)
    float mod = (GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN, POWER_MANA) / 5.0f);

    // Calc regens
    float ooc_regen = floor(base_regen / 5.0f + 0.5f) + spirit_regen + mod;
    float ic_regen = floor(base_regen / 5.0f + 0.5f) + mod;

    // Mana Meditation && Meditation - Allows 50% of your mana regeneration from Spirit to continue while in combat.
    int32 modManaRegenInterrupt = GetTotalAuraModifier(SPELL_AURA_MOD_MANA_REGEN_INTERRUPT);
    if (modManaRegenInterrupt)
        ic_regen += spirit_regen / 100.0 * modManaRegenInterrupt;

    // New aura in MoP - Mp5 increase by haste
    if (HasAuraType(SPELL_AURA_MOD_MANA_REGEN_BY_HASTE))
    {
        float haste = GetRatingBonusValue(CR_HASTE_SPELL);
        ooc_regen += ooc_regen / 100 * haste;
        ic_regen += ic_regen / 100 * haste;
    }

    // Not In Combat : 2% of base mana + spirit_regen
    SetStatFloatValue(UNIT_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER, ic_regen);
    // In Combat : 2% of base mana
    SetStatFloatValue(UNIT_POWER_REGEN_FLAT_MODIFIER, ooc_regen);
}

void Player::UpdateEnergyRegen()
{
    int index = GetPowerIndex(POWER_MANA) != MAX_POWERS;
    if (index == MAX_POWERS)
        return;

    if (index == 0)
        return;

    float value = 10.0f + (0.1f * GetRatingBonusValue(CR_HASTE_MELEE));

    AddPct(value, GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN_PERCENT, index));
    value += GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN, index) / 5.0f;

    //No changes between in and out of combat regen
    SetStatFloatValue(UNIT_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + index, value - 10.0f);
    SetStatFloatValue(UNIT_POWER_REGEN_FLAT_MODIFIER + index, value - 10.0f);
}

void Player::UpdateFocusRegen()
{
    int index = GetPowerIndex(POWER_FOCUS) != MAX_POWERS;
    if (index == MAX_POWERS)
        return;

    if (index == 0)
        return;

    float value = 5.0f + (0.02f * GetRatingBonusValue(CR_HASTE_RANGED));

    AddPct(value, GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN_PERCENT, index));
    value += GetTotalAuraModifierByMiscValue(SPELL_AURA_MOD_POWER_REGEN, index) / 5.0f;

    //No changes between in and out of combat regen (Client need to added value)
    SetStatFloatValue(UNIT_POWER_REGEN_INTERRUPTED_FLAT_MODIFIER + index, value - 5.0f);
    SetStatFloatValue(UNIT_POWER_REGEN_FLAT_MODIFIER + index, value - 5.0f);
}

void Player::UpdateRuneRegen(RuneType rune)
{
    if (rune >= NUM_RUNE_TYPES)
        return;

    uint32 cooldown = 0;

    for (uint32 i = 0; i < MAX_RUNES; ++i)
        if (GetBaseRune(i) == rune)
        {
            cooldown = GetRuneBaseCooldown(i);
            break;
        }

    if (cooldown <= 0)
        return;

    float regen = float(1 * IN_MILLISECONDS) / float(cooldown);
    SetFloatValue(PLAYER_RUNE_REGEN + uint8(rune), regen);
}

void Player::UpdateAllRunesRegen()
{
    for (uint8 i = 0; i < NUM_RUNE_TYPES; ++i)
        if (uint32 cooldown = GetRuneTypeBaseCooldown(RuneType(i)))
            SetFloatValue(PLAYER_RUNE_REGEN + i, float(1 * IN_MILLISECONDS) / float(cooldown));
}

void Player::_ApplyAllStatBonuses()
{
    SetCanModifyStats(false);

    _ApplyAllAuraStatMods();
    _ApplyAllItemMods();

    SetCanModifyStats(true);

    UpdateAllStats();
}

void Player::_RemoveAllStatBonuses()
{
    SetCanModifyStats(false);

    _RemoveAllItemMods();
    _RemoveAllAuraStatMods();

    SetCanModifyStats(true);

    UpdateAllStats();
}

/*#######################################
########                         ########
########    MOBS STAT SYSTEM     ########
########                         ########
#######################################*/

void Unit::UpdateAllResistances()
{
    for (uint8 i = SPELL_SCHOOL_NORMAL; i < MAX_SPELL_SCHOOL; ++i)
        UpdateResistances(i);
}

bool Creature::UpdateStats(Stats /*stat*/)
{
    return true;
}

bool Creature::UpdateAllStats()
{
    UpdateMaxHealth();
    UpdateAttackPowerAndDamage();
    UpdateAttackPowerAndDamage(true);

    for (uint8 i = POWER_MANA; i < MAX_POWERS; ++i)
        UpdateMaxPower(Powers(i));

    UpdateAllResistances();

    return true;
}

void Creature::UpdateResistances(uint32 school)
{
    if (school > SPELL_SCHOOL_NORMAL)
    {
        float value  = GetTotalAuraModValue(UnitMods(UNIT_MOD_RESISTANCE_START + school));
        SetResistance(SpellSchools(school), int32(value));
    }
    else
        UpdateArmor();
}

void Creature::UpdateArmor()
{
    float value = GetTotalAuraModValue(UNIT_MOD_ARMOR);
    SetArmor(int32(value));
}

void Creature::UpdateMaxHealth()
{
    float value = GetTotalAuraModValue(UNIT_MOD_HEALTH);
    SetMaxHealth((uint32)value);
}

void Creature::UpdateMaxPower(Powers power)
{
    UnitMods unitMod = UnitMods(UNIT_MOD_POWER_START + power);

    float value  = GetTotalAuraModValue(unitMod);
    SetMaxPower(power, int32(value));
}

void Creature::UpdateAttackPowerAndDamage(bool ranged)
{
    UnitMods unitMod = ranged ? UNIT_MOD_ATTACK_POWER_RANGED_POS : UNIT_MOD_ATTACK_POWER_POS;

    uint16 index = UNIT_ATTACK_POWER;
    uint16 indexMod = UNIT_ATTACK_POWER_MOD_POS;
    uint16 indexMulti = UNIT_ATTACK_POWER_MULTIPLIER;

    if (ranged)
    {
        index = UNIT_RANGED_ATTACK_POWER;
        indexMod = UNIT_RANGED_ATTACK_POWER_MOD_POS;
        indexMulti = UNIT_RANGED_ATTACK_POWER_MULTIPLIER;
    }

    float baseAttackPower       = GetFlatModifierValue(unitMod, BASE_VALUE) * GetPctModifierValue(unitMod, BASE_PCT);
    float attackPowerMod        = GetFlatModifierValue(unitMod, TOTAL_VALUE);
    float attackPowerMultiplier = GetPctModifierValue(unitMod, TOTAL_PCT) - 1.0f;

    SetInt32Value(index, uint32(baseAttackPower));      // UNIT_(RANGED)_ATTACK_POWER
    SetInt32Value(indexMod, uint32(attackPowerMod));    // UNIT_(RANGED)_ATTACK_POWER_MOD_POS
    SetFloatValue(indexMulti, attackPowerMultiplier);   // UNIT_(RANGED)_ATTACK_POWER_MULTIPLIER

    //automatically update weapon damage after attack power modification
    if (ranged)
        UpdateDamagePhysical(RANGED_ATTACK);
    else
    {
        UpdateDamagePhysical(BASE_ATTACK);
        UpdateDamagePhysical(OFF_ATTACK);
    }
}

void Unit::UpdateDamagePhysical(WeaponAttackType attType)
{
    float minDamage = 0.0f;
    float maxDamage = 0.0f;

    CalculateMinMaxDamage(attType, false, true, minDamage, maxDamage);

    switch (attType)
    {
        case BASE_ATTACK:
        default:
            SetStatFloatValue(UNIT_MIN_DAMAGE, minDamage);
            SetStatFloatValue(UNIT_MAX_DAMAGE, maxDamage);
            break;
        case OFF_ATTACK:
            SetStatFloatValue(UNIT_MIN_OFF_HAND_DAMAGE, minDamage);
            SetStatFloatValue(UNIT_MAX_OFF_HAND_DAMAGE, maxDamage);
            break;
        case RANGED_ATTACK:
            SetStatFloatValue(UNIT_MIN_RANGED_DAMAGE, minDamage);
            SetStatFloatValue(UNIT_MAX_RANGED_DAMAGE, maxDamage);
            break;
    }
}

/*#######################################
########                         ########
########    PETS STAT SYSTEM     ########
########                         ########
#######################################*/

bool Guardian::UpdateStats(Stats stat)
{
    if (stat >= MAX_STATS)
        return false;

    float value  = GetTotalStatValue(stat);
    //ApplyStatBuffMod(stat, m_statFromOwner[stat], false);
    float ownersBonus = 0.0f;

    Unit* owner = GetOwner();
    // Handle Death Knight Glyphs and Talents
    float mod = 0.75f;
    switch (stat)
    {
        case STAT_STAMINA:
        {
           mod = 0.3f;
            if (IsPetGhoul() || IsPetGargoyle())
                mod = 0.45f;
            else if (owner->GetClass() == CLASS_WARLOCK && IsPet())
                mod = 0.75f;
            else if (owner->GetClass() == CLASS_MAGE && IsPet())
                mod = 0.75f;
            else
            {
                mod = 0.45f;
                if (IsPet())
                {
                    switch (ToPet()->GetSpecialization())
                    {
                        case PET_SPECIALIZATION_FEROCITY: mod = 0.67f;  break;
                        case PET_SPECIALIZATION_TENACITY: mod = 0.78f;  break;
                        case PET_SPECIALIZATION_CUNNING:  mod = 0.725f; break;
                    }
                }
            }
            ownersBonus = float(owner->GetStat(stat)) * mod;
            ownersBonus *= GetPctModifierValue(UNIT_MOD_STAT_STAMINA, TOTAL_PCT);
            value += ownersBonus;
            break;                                                         // Ravenous Dead edits the original scale
        }
        case STAT_STRENGTH:
        {
            mod = 0.7f;
            ownersBonus = owner->GetStat(stat) * mod;
            value += ownersBonus;
            break;
        }
        case STAT_INTELLECT:
        {
            if (owner->GetClass() == CLASS_WARLOCK || owner->GetClass() == CLASS_MAGE)
            {
                mod = 0.3f;
                ownersBonus = owner->GetStat(stat) * mod;
            }
            else if (owner->GetClass() == CLASS_DEATH_KNIGHT && GetEntry() == 31216)
            {
                mod = 0.3f;
                ownersBonus = owner->GetStat(stat) * mod;
            }
            value += ownersBonus;
            break;
        }
        default:
            break;
    }

    SetStat(stat, int32(value));
    m_statFromOwner[stat] = ownersBonus;
    UpdateStatBuffMod(stat);

    switch (stat)
    {
        case STAT_STRENGTH:
            UpdateAttackPowerAndDamage();
            break;
        case STAT_AGILITY:
            UpdateArmor();
            break;
        case STAT_STAMINA:
            UpdateMaxHealth();
            break;
        case STAT_INTELLECT:
            UpdateMaxPower(POWER_MANA);
            if (owner->GetClass() == CLASS_MAGE)
                UpdateAttackPowerAndDamage();
            break;
        case STAT_SPIRIT:
        default:
            break;
    }

    return true;
}

bool Guardian::UpdateAllStats()
{
    for (uint8 i = STAT_STRENGTH; i < MAX_STATS; ++i)
        UpdateStats(Stats(i));

    for (uint8 i = POWER_MANA; i < MAX_POWERS; ++i)
        UpdateMaxPower(Powers(i));

    UpdateAllResistances();

    return true;
}

void Guardian::UpdateResistances(uint32 school)
{
    if (school > SPELL_SCHOOL_NORMAL)
    {
        float value  = GetTotalAuraModValue(UnitMods(UNIT_MOD_RESISTANCE_START + school));

        // hunter and warlock pets gain 40% of owner's resistance
        if (IsPet())
            value += float(CalculatePct(m_owner->GetResistance(SpellSchools(school)), 40));

        SetResistance(SpellSchools(school), int32(value));
    }
    else
        UpdateArmor();
}

void Guardian::UpdateArmor()
{
    float value = 0.0f;
    UnitMods unitMod = UNIT_MOD_ARMOR;

    // All pets gain 100% of owner's armor value
    value = m_owner->GetArmor();
    value *= GetPctModifierValue(unitMod, BASE_PCT);
    value *= GetPctModifierValue(unitMod, TOTAL_PCT);

    if (Unit* owner = GetOwner())
    {
        // SPELL_AURA_MOD_PET_STAT_PCT
        Unit::AuraEffectList const& modPetStats = owner->GetAuraEffectsByType(SPELL_AURA_MOD_PET_STAT_PCT);
        if (!modPetStats.empty())
        {
            int32 multiplier = 1.0f;

            for (AuraEffect const* aurEff : modPetStats)
                if (aurEff->GetMiscValue() == PET_STAT_ARMOR && (!aurEff->GetMiscValueB() || aurEff->GetMiscValueB() == GetEntry()))
                    AddPct(multiplier, aurEff->GetAmount());

            value *= multiplier;
        }
    }

    SetArmor(int32(value));
}

void Guardian::UpdateMaxHealth()
{
    UnitMods unitMod = UNIT_MOD_HEALTH;
    float stamina = GetStat(STAT_STAMINA) - GetCreateStat(STAT_STAMINA);

    float multiplicator;
    switch (GetEntry())
    {
        case PET_ENTRY_IMP:
            multiplicator = 8.4f;
            break;
        case PET_ENTRY_VOIDWALKER:
        case PET_ENTRY_FELGUARD:
            multiplicator = 11.0f;
            break;
        case PET_ENTRY_SUCCUBUS:
            multiplicator = 9.1f;
            break;
        case PET_ENTRY_FELHUNTER:
            multiplicator = 9.5f;
            break;
        case PET_ENTRY_GHOUL:
        case PET_ENTRY_GARGOYLE:
            multiplicator = 15.0f;
            break;
        case PET_ENTRY_WATER_ELEMENTAL:
            multiplicator = 1.0f;
            stamina = 0.0f;
            break;
        case PET_ENTRY_BLOODWORM:
            SetMaxHealth(GetCreateHealth());
            return;
        case PET_ENTRY_SHADOWFIEND:
        case PET_ENTRY_SHADOWFIEND_SHA:
            // Improved Shadowfiend
            if (AuraEffect const* aurEff = GetOwner()->GetAuraEffect(37570, EFFECT_0))
                stamina += aurEff->GetAmount();
            // no break;
        default:
            multiplicator = 10.0f;
            break;
    }

    float value = GetFlatModifierValue(unitMod, BASE_VALUE) + GetCreateHealth();
    value *= GetPctModifierValue(unitMod, BASE_PCT);
    value += GetFlatModifierValue(unitMod, TOTAL_VALUE) + stamina * multiplicator;
    value *= GetPctModifierValue(unitMod, TOTAL_PCT);

    if (Unit* owner = GetOwner())
    {
        // SPELL_AURA_MOD_PET_HEALTH_PCT
        value += owner->CountPctFromMaxHealth(owner->GetTotalAuraModifier(SPELL_AURA_MOD_PET_HEALTH_PCT));

        // SPELL_AURA_MOD_PET_STAT_PCT
        Unit::AuraEffectList const& modPetStats = owner->GetAuraEffectsByType(SPELL_AURA_MOD_PET_STAT_PCT);
        if (!modPetStats.empty())
        {
            int32 multiplier = 1.0f;

            for (AuraEffect const* aurEff : modPetStats)
                if (aurEff->GetMiscValue() == PET_STAT_HEALTH && (!aurEff->GetMiscValueB() || aurEff->GetMiscValueB() == GetEntry()))
                    AddPct(multiplier, aurEff->GetAmount());

            value *= multiplier;
        }
    }

    SetMaxHealth((uint32)value);
}

void Guardian::UpdateMaxPower(Powers power)
{
    UnitMods unitMod = UnitMods(UNIT_MOD_POWER_START + power);

    float addValue = (power == POWER_MANA) ? GetStat(STAT_INTELLECT) - GetCreateStat(STAT_INTELLECT) : 0.0f;
    float multiplicator = 15.0f;

    switch (GetEntry())
    {
        case PET_ENTRY_IMP:
            multiplicator = 4.95f;
            break;
        case PET_ENTRY_VOIDWALKER:
        case PET_ENTRY_SUCCUBUS:
        case PET_ENTRY_FELHUNTER:
        case PET_ENTRY_FELGUARD:
            multiplicator = 11.5f;
            break;
        case PET_ENTRY_WATER_ELEMENTAL:
            multiplicator = 1.0f;
            addValue = 0.0f;
            break;
        default:
            multiplicator = 15.0f;
            break;
    }

    float value  = GetFlatModifierValue(unitMod, BASE_VALUE) + GetCreatePowers(power);
    value *= GetPctModifierValue(unitMod, BASE_PCT);
    value += GetFlatModifierValue(unitMod, TOTAL_VALUE) + addValue * multiplicator;
    value *= GetPctModifierValue(unitMod, TOTAL_PCT);

    SetMaxPower(power, int32(value));
}

void Guardian::UpdateAttackPowerAndDamage(bool ranged)
{
    if (ranged)
        return;

    float val = 0.0f;
    float bonusAP = 0.0f;
    UnitMods unitMod_pos = UNIT_MOD_ATTACK_POWER_POS;
    UnitMods unitMod_neg = UNIT_MOD_ATTACK_POWER_NEG;

    if (GetEntry() == PET_ENTRY_IMP)                                   // imp's attack power
        val = GetStat(STAT_STRENGTH) - 10.0f;
    else
        val = 2 * GetStat(STAT_STRENGTH) - 20.0f;

    Unit* owner = GetOwner();
    if (owner && owner->GetTypeId() == TYPEID_PLAYER)
    {
        switch (GetEntry())
        {
            case PET_ENTRY_GHOUL:
                bonusAP = owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.22f;
                SetBonusDamage(int32(owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.1287f));
                break;
            case PET_ENTRY_FERAL_SPIRIT:
            {
                float dmg_multiplier = 0.31f;
                if (m_owner->GetAuraEffect(63271, 0)) // Glyph of Feral Spirit
                    dmg_multiplier = 0.61f;

                bonusAP = owner->GetTotalAttackPowerValue(BASE_ATTACK) * dmg_multiplier;
                SetBonusDamage(int32(owner->GetTotalAttackPowerValue(BASE_ATTACK) * dmg_multiplier));
                break;
            }
            case PET_ENTRY_WATER_ELEMENTAL:
                SetBonusDamage(int32(m_owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_FROST)));
                break;
            default:
                break;
        }

        if (IsHunterPet())                      //hunter pets benefit from owner's attack power
        {
            bonusAP = owner->GetTotalAttackPowerValue(RANGED_ATTACK); // Hunter pets gain 100% of owner's AP
            SetBonusDamage(int32(owner->GetTotalAttackPowerValue(RANGED_ATTACK) * 0.5f)); // Bonus damage is equal to 50% of owner's AP
        }

        //demons benefit from warlocks shadow or fire damage
        else if (IsPet())
        {
            int32 spd = owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_SPELL);
            SetBonusDamage(int32(spd * 0.15f));
            bonusAP = spd;
        }
    }

    if (bonusAP > 0)
        SetStatFlatModifier(UNIT_MOD_ATTACK_POWER_POS, BASE_VALUE, val + bonusAP);
    else
    {
        SetStatFlatModifier(UNIT_MOD_ATTACK_POWER_POS, BASE_VALUE, val);
        SetStatFlatModifier(UNIT_MOD_ATTACK_POWER_NEG, BASE_VALUE, bonusAP);
    }

    float base_attPower   = (GetFlatModifierValue(unitMod_pos, BASE_VALUE) + GetFlatModifierValue(unitMod_neg, BASE_VALUE)) * (GetPctModifierValue(unitMod_pos, BASE_PCT) + (GetPctModifierValue(unitMod_neg, BASE_PCT) - 1));
    float attPowerMod_pos = GetFlatModifierValue(unitMod_pos, TOTAL_VALUE);
    float attPowerMod_neg = GetFlatModifierValue(unitMod_neg, TOTAL_VALUE);
    float attPowerMultiplier = ((GetPctModifierValue(unitMod_pos, TOTAL_PCT) - 1.0f) + (GetPctModifierValue(unitMod_neg, TOTAL_PCT) - 1.0f));

    SetInt32Value(UNIT_ATTACK_POWER, (int32)base_attPower);
    SetInt32Value(UNIT_ATTACK_POWER_MOD_POS, (int32)attPowerMod_pos);
    SetInt32Value(UNIT_ATTACK_POWER_MOD_NEG, (int32)attPowerMod_neg);
    SetFloatValue(UNIT_ATTACK_POWER_MULTIPLIER, attPowerMultiplier);

    UpdateDamagePhysical(BASE_ATTACK);
}

void Guardian::UpdateDamagePhysical(WeaponAttackType attType)
{
    if (attType > BASE_ATTACK)
        return;

    float bonusDamage = 0.0f;
    if (m_owner->GetTypeId() == TYPEID_PLAYER)
    {
        if (IsTreant()) // force of nature
        {
            int32 spellDmg = m_owner->GetInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + SPELL_SCHOOL_NATURE) - m_owner->GetInt32Value(PLAYER_MOD_DAMAGE_DONE_NEG + SPELL_SCHOOL_NATURE);
            if (spellDmg > 0)
                bonusDamage = spellDmg * 0.09f;
        }
        //greater fire elemental
        else if (GetEntry() == PET_ENTRY_FIRE_ELEMENTAL)
        {
            int32 spellDmg = m_owner->GetInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + SPELL_SCHOOL_FIRE) - m_owner->GetInt32Value(PLAYER_MOD_DAMAGE_DONE_NEG + SPELL_SCHOOL_FIRE);
            if (spellDmg > 0)
                bonusDamage = spellDmg * 0.4f;
        }
    }

    UnitMods unitMod = UNIT_MOD_DAMAGE_MAINHAND;

    float att_speed = float(GetBaseAttackTime(BASE_ATTACK))/1000.0f;

    float base_value  = GetFlatModifierValue(unitMod, BASE_VALUE) + GetTotalAttackPowerValue(attType) / 14.0f * att_speed + bonusDamage;
    float base_pct    = GetPctModifierValue(unitMod, BASE_PCT);
    float total_value = GetFlatModifierValue(unitMod, TOTAL_VALUE);
    float total_pct   = GetPctModifierValue(unitMod, TOTAL_PCT);

    if (Unit* owner = GetOwner())
    {
        // SPELL_AURA_MOD_PET_STAT_PCT
        Unit::AuraEffectList const& modPetStats = owner->GetAuraEffectsByType(SPELL_AURA_MOD_PET_STAT_PCT);
        if (!modPetStats.empty())
        {
            int32 multiplier = 1.0f;

            for (AuraEffect const* aurEff : modPetStats)
                if (aurEff->GetMiscValue() == PET_STAT_MELEE_DAMAGE && (!aurEff->GetMiscValueB() || aurEff->GetMiscValueB() == GetEntry()))
                    AddPct(multiplier, aurEff->GetAmount());

            total_pct *= multiplier;
        }
    }

    float weapon_mindamage = GetWeaponDamageRange(BASE_ATTACK, MINDAMAGE);
    float weapon_maxdamage = GetWeaponDamageRange(BASE_ATTACK, MAXDAMAGE);

    float mindamage = ((base_value + weapon_mindamage) * base_pct + total_value) * total_pct;
    float maxdamage = ((base_value + weapon_maxdamage) * base_pct + total_value) * total_pct;

    SetStatFloatValue(UNIT_MIN_DAMAGE, mindamage);
    SetStatFloatValue(UNIT_MAX_DAMAGE, maxdamage);
}

void Guardian::SetBonusDamage(int32 damage)
{
    m_bonusSpellDamage = damage;
    if (GetOwner()->GetTypeId() == TYPEID_PLAYER)
        GetOwner()->SetUInt32Value(PLAYER_PET_SPELL_POWER, damage);
}
