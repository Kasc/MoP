/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "DBCStores.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "ObjectMgr.h"
#include "GuildMgr.h"
#include "SpellMgr.h"
#include "LootMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "Spell.h"
#include "Totem.h"
#include "TemporarySummon.h"
#include "SpellAuras.h"
#include "CreatureAI.h"
#include "ScriptMgr.h"
#include "GameObjectAI.h"
#include "SpellAuraEffects.h"
#include "Player.h"
#include "DBCStores.h"
#include "Chat.h"
#include "Language.h"
#include "QueryCallback.h"

void WorldSession::HandleUseItemOpcode(WorldPacket& recvData)
{
    Player* pUser = _player;
    Unit* mover = _player->GetMover();

    if (mover != pUser)
    {
        recvData.rfinish();
        return;
    }

    uint8 bagIndex = 0;
    uint8 slot = 0;
    uint8 castCount = 0;
    uint8 castFlags = 0;

    uint32 spellId = 0;
    uint32 glyphIndex = 0;
    uint32 targetMask = 0;
    uint32 targetStringLength = 0;

    float elevation = 0.0f;
    float missileSpeed = 0.0f;

    ObjectGuid itemGuid;
    ObjectGuid targetGuid;
    ObjectGuid itemTargetGuid;
    ObjectGuid destTransportGuid;
    ObjectGuid srcTransportGuid;

    Position srcPos;
    Position destPos;

    std::string targetString;

    // Movement data
    MovementInfo movementInfo;

    ObjectGuid movementTransportGuid;
    ObjectGuid movementGuid;

    bool hasTargetGuid = false;
    bool hasItemTargetGuid = false;

    bool hasTransport = false;
    bool hasTransportTime2 = false;
    bool hasTransportTime3 = false;
    bool hasFallData = false;
    bool hasFallDirection = false;
    bool hasTimestamp = false;
    bool hasSplineElevation = false;
    bool hasPitch = false;
    bool hasOrientation = false;
    bool hasMoveTime = false;

    uint32 RemoveForcesCount = 0;

    Unit* caster = mover;

    recvData >> slot;
    recvData >> bagIndex;

    bool hasElevation = !recvData.ReadBit();

    recvData.ReadGuidMask(itemGuid, 6);

    bool hasTargetString = !recvData.ReadBit();

    recvData.ReadGuidMask(itemGuid, 1);

    bool hasCastFlags = !recvData.ReadBit();
    bool hasDestLocation = recvData.ReadBit();

    recvData.ReadGuidMask(itemGuid, 2, 7, 0);

    bool hasTargetMask = !recvData.ReadBit();
    bool hasMissileSpeed = !recvData.ReadBit();
    bool hasMovement = recvData.ReadBit();
    bool hasCastCount = !recvData.ReadBit();
    bool hasSpellId = !recvData.ReadBit();

    hasTargetGuid = !recvData.ReadBit();

    bool hasGlyphIndex = !recvData.ReadBit();

    hasItemTargetGuid = !recvData.ReadBit();

    recvData.ReadGuidMask(itemGuid, 4);

    bool hasSrcLocation = recvData.ReadBit();

    recvData.ReadGuidMask(itemGuid, 3, 5);

    uint8 SpellArchaeologyCount = recvData.ReadBits(2);
    std::vector<SpellArchaeology> SpellArchaeologyProjects(SpellArchaeologyCount);

    for (uint8 i = 0; i < SpellArchaeologyCount; ++i)
        SpellArchaeologyProjects[i].Type = recvData.ReadBits(2);

    if (hasMovement)
    {
        hasPitch = !recvData.ReadBit();
        hasTransport = recvData.ReadBit();

        bool hasSpline = !recvData.ReadBit();

        if (hasTransport)
        {
            recvData.ReadGuidMask(movementTransportGuid, 7, 2, 4, 5, 6, 0, 1);

            hasTransportTime3 = recvData.ReadBit();

            recvData.ReadGuidMask(movementTransportGuid, 4);

            hasTransportTime2 = recvData.ReadBit();
        }

        recvData.ReadGuidMask(movementGuid, 6, 2, 1);

        RemoveForcesCount = recvData.ReadBits(22);

        bool HeightChangeFailed = !recvData.ReadBit();

        bool hasMovementFlags2 = !recvData.ReadBit();
        hasFallData = recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 5);

        hasSplineElevation = !recvData.ReadBit();

        bool RemoteTimeValid = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 7, 0);

        if (hasFallData)
            hasFallDirection = recvData.ReadBit();

        hasOrientation = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 4, 3);

        hasTimestamp = !recvData.ReadBit();
        hasMoveTime = !recvData.ReadBit();
        bool hasMovementFlags = !recvData.ReadBit();

        if (hasMovementFlags2)
            movementInfo.flags2 = recvData.ReadBits(13);

        if (hasMovementFlags)
            movementInfo.flags = recvData.ReadBits(30);
    }

    if (hasSrcLocation)
        recvData.ReadGuidMask(srcTransportGuid, 3, 1, 7, 4, 2, 0, 6, 5);

    if (hasDestLocation)
        recvData.ReadGuidMask(destTransportGuid, 2, 4, 1, 7, 6, 0, 3, 5);

    if (hasTargetString)
        targetStringLength = recvData.ReadBits(7);

    recvData.ReadGuidMask(targetGuid, 1, 0, 5, 3, 6, 4, 7, 2);

    recvData.ReadGuidMask(itemTargetGuid, 4, 5, 0, 1, 3, 7, 6, 2);

    if (hasCastFlags)
        castFlags = recvData.ReadBits(5);

    if (hasTargetMask)
        targetMask = recvData.ReadBits(20);

    recvData.ReadGuidBytes(itemGuid, 0, 5, 6, 3, 4, 2, 1);

    for (uint8 i = 0; i < SpellArchaeologyCount; ++i)
    {
        recvData >> SpellArchaeologyProjects[i].ID;
        recvData >> SpellArchaeologyProjects[i].Quantity;
    }

    recvData.ReadGuidBytes(itemGuid, 7);

    if (hasMovement)
    {
        for (uint32 i = 0; i != RemoveForcesCount; i++)
        {
            uint32 RemoveForcesIDs = 0;
            recvData >> RemoveForcesIDs;
        }

        if (hasTransport)
        {
            recvData >> movementInfo.transport.pos.m_positionY;
            recvData >> movementInfo.transport.pos.m_positionZ;

            recvData.ReadGuidBytes(movementTransportGuid, 1);

            if (hasTransportTime3)
                recvData >> movementInfo.transport.vehicleId;

            recvData.ReadGuidBytes(movementTransportGuid, 7, 5, 2, 4);

            recvData >> movementInfo.transport.pos.m_positionX;
            movementInfo.transport.pos.SetOrientation(recvData.read<float>());

            recvData.ReadGuidBytes(movementTransportGuid, 0);

            recvData >> movementInfo.transport.seat;
            recvData >> movementInfo.transport.time;

            recvData.ReadGuidBytes(movementTransportGuid, 6, 3);

            if (hasTransportTime2)
                recvData >> movementInfo.transport.prevTime;
        }

        if (hasFallData)
        {
            recvData >> movementInfo.fall.zspeed;

            if (hasFallDirection)
            {
                recvData >> movementInfo.fall.sinAngle;
                recvData >> movementInfo.fall.cosAngle;
                recvData >> movementInfo.fall.xyspeed;
            }

            recvData >> movementInfo.fall.fallTime;
        }

        recvData.ReadGuidBytes(movementTransportGuid, 3, 7, 6, 1);

        recvData >> movementInfo.pos.m_positionY;

        if (hasSplineElevation)
            recvData >> movementInfo.splineElevation;

        if (hasMoveTime)
        {
            uint32 MoveTime = 0;
            recvData >> MoveTime;
        }

        if (hasOrientation)
            movementInfo.pos.SetOrientation(recvData.read<float>());

        recvData.ReadGuidBytes(movementTransportGuid, 2);

        recvData >> movementInfo.pos.m_positionZ;

        if (hasTimestamp)
            recvData >> movementInfo.time;

        recvData >> movementInfo.pos.m_positionX;

        recvData.ReadGuidBytes(movementTransportGuid, 5, 0);

        if (hasPitch)
            movementInfo.pitch = G3D::wrap(recvData.read<float>(), float(-M_PI), float(M_PI));

        recvData.ReadGuidBytes(movementTransportGuid, 4);
    }

    if (hasDestLocation)
    {
        float x, y, z;

        recvData.ReadGuidBytes(destTransportGuid, 7);

        recvData >> x;

        recvData.ReadGuidBytes(destTransportGuid, 0, 6, 1, 3);

        recvData >> y;

        recvData.ReadGuidBytes(destTransportGuid, 5);

        recvData >> z;

        recvData.ReadGuidBytes(destTransportGuid, 4, 2);

        destPos.Relocate(x, y, z);
    }
    else
    {
        destTransportGuid = caster->GetTransportGUID();

        if (destTransportGuid)
            destPos.Relocate(caster->GetTransportPosition());
        else
            destPos.Relocate(caster);
    }

    recvData.ReadGuidBytes(itemTargetGuid, 6, 7, 2, 0, 3, 4, 1, 5);

    if (hasSrcLocation)
    {
        float x, y, z;

        recvData.ReadGuidBytes(srcTransportGuid, 7);

        recvData >> x;

        recvData.ReadGuidBytes(srcTransportGuid, 1, 5, 4);

        recvData >> z;

        recvData.ReadGuidBytes(srcTransportGuid, 6, 0, 3);

        recvData >> y;

        recvData.ReadGuidBytes(srcTransportGuid, 2);

        srcPos.Relocate(x, y, z);
    }
    else
    {
        srcTransportGuid = caster->GetTransportGUID();
        if (srcTransportGuid)
            srcPos.Relocate(caster->GetTransportPosition());
        else
            srcPos.Relocate(caster);
    }

    if (hasSpellId)
        recvData >> spellId;

    recvData.ReadGuidBytes(targetGuid, 1, 4, 3, 6, 2, 0, 7, 5);

    if (hasTargetString)
        targetString = recvData.ReadString(targetStringLength);

    if (hasElevation)
        recvData >> elevation;

    if (hasGlyphIndex)
        recvData >> glyphIndex;

    if (hasMissileSpeed)
        recvData >> missileSpeed;

    if (hasCastCount)
        recvData >> castCount;

    if (glyphIndex >= MAX_GLYPH_SLOT_INDEX)
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, NULL, NULL);
        return;
    }

    Item* pItem = pUser->GetUseableItemByPos(bagIndex, slot);
    if (!pItem)
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, NULL, NULL);
        return;
    }

    if (pItem->GetGUID() != itemGuid)
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, NULL, NULL);
        return;
    }

    TC_LOG_DEBUG("network", "WORLD: CMSG_USE_ITEM packet, bagIndex: %u, slot: %u, castCount: %u, spellId: %u, Item: %u, glyphIndex: %u, data length = %i", bagIndex, slot, castCount, spellId, pItem->GetEntry(), glyphIndex, (uint32)recvData.size());

    ItemTemplate const* proto = pItem->GetTemplate();
    if (!proto)
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, pItem, NULL);
        return;
    }

    // some item classes can be used only in equipped state
    if (proto->GetInventoryType() != INVTYPE_NON_EQUIP && !pItem->IsEquipped())
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, pItem, NULL);
        return;
    }

    InventoryResult msg = pUser->CanUseItem(pItem);
    if (msg != EQUIP_ERR_OK)
    {
        pUser->SendEquipError(msg, pItem, NULL);
        return;
    }

    // only allow conjured consumable, bandage, poisons (all should have the 2^21 item flag set in DB)
    if (proto->GetClass() == ITEM_CLASS_CONSUMABLE && !(proto->GetFlags() & ITEM_PROTO_FLAG_IGNORE_DEFAULT_ARENA_RESTRICTIONS) && pUser->InRatedBG())
    {
        pUser->SendEquipError(EQUIP_ERR_NOT_DURING_ARENA_MATCH, pItem, NULL);
        return;
    }

    // don't allow items banned in arena
    if (proto->GetFlags() & ITEM_PROTO_FLAG_NOT_USEABLE_IN_ARENA && pUser->InRatedBG())
    {
        pUser->SendEquipError(EQUIP_ERR_NOT_DURING_ARENA_MATCH, pItem, NULL);
        return;
    }

    if (pUser->IsInCombat())
        for (uint32 i = 0; i < proto->Effects.size(); ++i)
            if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(proto->Effects[i]->SpellID))
                if (!spellInfo->CanBeUsedInCombat())
                {
                    pUser->SendEquipError(EQUIP_ERR_NOT_IN_COMBAT, pItem, NULL);
                    return;
                }

    // check also  BIND_WHEN_PICKED_UP and BIND_QUEST_ITEM for .additem or .additemset case by GM (not binded at adding to inventory)
    if (pItem->GetTemplate()->GetBonding() == BIND_ON_USE || pItem->GetTemplate()->GetBonding() == BIND_ON_ACQUIRE || pItem->GetTemplate()->GetBonding() == BIND_QUEST)
    {
        if (!pItem->IsSoulBound())
        {
            pItem->SetState(ITEM_CHANGED, pUser);
            pItem->SetBinding(true);
        }
    }

    SpellCastTargets targets(caster, targetMask, targetGuid, itemTargetGuid, srcTransportGuid, destTransportGuid, srcPos, destPos, elevation, missileSpeed, targetString);

    // Note: If script stop casting it must send appropriate data to client to prevent stuck item in gray state.
    if (!sScriptMgr->OnItemUse(pUser, pItem, targets))
    {
        // no script or script not process request by self
        pUser->CastItemUseSpell(pItem, targets, castCount, glyphIndex, SpellArchaeologyProjects);
    }
}

void WorldSession::HandleOpenItemOpcode(WorldPacket& recvData)
{
    Player* pUser = _player;

    // ignore for remote control state
    if (pUser->GetMover() != pUser)
    {
        recvData.rfinish();
        return;
    }

    // additional check, client outputs message on its own
    if (!pUser->IsAlive())
    {
        pUser->SendEquipError(EQUIP_ERR_PLAYER_DEAD, nullptr, nullptr);
        return;
    }

    uint8 bagIndex = 0;
    uint8 slot = 0;

    recvData >> bagIndex;
    recvData >> slot;

    TC_LOG_DEBUG("network", "bagIndex: %u, slot: %u", bagIndex, slot);

    Item* item = pUser->GetItemByPos(bagIndex, slot);
    if (!item)
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, nullptr, nullptr);
        return;
    }

    ItemTemplate const* proto = item->GetTemplate();
    if (!proto)
    {
        pUser->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, item, nullptr);
        return;
    }

    // Verify that the bag is an actual bag or wrapped item that can be used "normally"
    if (!(proto->GetFlags() & ITEM_PROTO_FLAG_HAS_LOOT) && !item->HasFlag(ITEM_FLAGS, ITEM_FLAG_WRAPPED))
    {
        pUser->SendEquipError(EQUIP_ERR_CLIENT_LOCKED_OUT, item, NULL);
        TC_LOG_ERROR("entities.player.cheat", "Possible hacking attempt: Player %s [%s] tried to open item [%s, entry: %u] which is not openable!",
            _player->GetName().c_str(), _player->GetGUID().ToString().c_str(), item->GetGUID().ToString().c_str(), proto->GetId());
        return;
    }

    // locked item
    uint32 lockId = proto->GetLockID();
    if (lockId)
    {
        LockEntry const* lockInfo = sLockStore.LookupEntry(lockId);

        if (!lockInfo)
        {
            pUser->SendEquipError(EQUIP_ERR_ITEM_LOCKED, item, NULL);
            TC_LOG_DEBUG("network", "WORLD::OpenItem: item [guid = %u] has an unknown lockId: %u!", item->GetGUID().GetCounter(), lockId);
            return;
        }

        // was not unlocked yet
        if (item->IsLocked())
        {
            pUser->SendEquipError(EQUIP_ERR_ITEM_LOCKED, item, nullptr);
            return;
        }
    }

    if (item->HasFlag(ITEM_FLAGS, ITEM_FLAG_WRAPPED))// wrapped?
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_GIFT_BY_ITEM);
        stmt->setUInt32(0, item->GetGUID().GetCounter());
        _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
            .WithPreparedCallback(std::bind(&WorldSession::HandleOpenWrappedItemCallback, this, item->GetPos(), item->GetGUID(), std::placeholders::_1)));
    }
    else
        pUser->SendLoot(item->loot);
}

void WorldSession::HandleOpenWrappedItemCallback(uint16 pos, ObjectGuid itemGuid, PreparedQueryResult result)
{
    if (!GetPlayer())
        return;

    Item* item = GetPlayer()->GetItemByPos(pos);
    if (!item)
        return;

    if (item->GetGUID() != itemGuid || !item->HasFlag(ITEM_FLAGS, ITEM_FLAG_WRAPPED)) // during getting result, gift was swapped with another item
        return;

    if (!result)
    {
        TC_LOG_ERROR("network", "Wrapped item %u don't have record in character_gifts table and will deleted", itemGuid.GetCounter());
        GetPlayer()->DestroyItem(item->GetBagSlot(), item->GetSlot(), true);
        return;
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    Field* fields = result->Fetch();
    uint32 entry = fields[0].GetUInt32();
    uint32 flags = fields[1].GetUInt32();

    item->SetGuidValue(ITEM_GIFT_CREATOR, ObjectGuid::Empty);
    item->SetEntry(entry);
    item->SetUInt32Value(ITEM_FLAGS, flags);
    item->SetUInt32Value(ITEM_MAX_DURABILITY, item->GetTemplate()->MaxDurability);
    item->SetState(ITEM_CHANGED, GetPlayer());

    GetPlayer()->SaveInventoryAndGoldToDB(trans);

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GIFT);
    stmt->setUInt32(0, itemGuid.GetCounter());
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);
}

void WorldSession::HandleGameObjectUseOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 6, 1, 3, 4, 0, 5, 7, 2);
    recvData.ReadGuidBytes(guid, 0, 1, 6, 2, 3, 4, 5, 7);

    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_GAMEOBJ_USE Message [%s]", guid.ToString().c_str());

    if (GameObject* obj = GetPlayer()->GetMap()->GetGameObject(guid))
    {
        // ignore for remote control state
        if (GetPlayer()->GetMover() != GetPlayer())
            if (!(GetPlayer()->IsOnVehicle(GetPlayer()->GetMover()) || GetPlayer()->IsMounted()) && !obj->GetGOInfo()->IsUsableMounted())
                return;

        obj->Use(GetPlayer());
    }
}

void WorldSession::HandleGameobjectReportUse(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 4, 7, 5, 3, 6, 1, 2, 0);
    recvData.ReadGuidBytes(guid, 7, 1, 6, 5, 0, 3, 2, 4);

    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_GAMEOBJ_REPORT_USE Message [%s]", guid.ToString().c_str());

    // ignore for remote control state
    if (_player->GetMover() != _player)
        return;

    if (GameObject* go = GetPlayer()->GetGameObjectIfCanInteractWith(guid))
    {
        if (go->AI()->OnReportUse(_player))
            return;

        _player->UpdateCriteria(CRITERIA_TYPE_USE_GAMEOBJECT, go->GetEntry());
    }
}

void WorldSession::HandleCastSpellOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (mover != _player && mover->GetTypeId() == TYPEID_PLAYER)
    {
        recvData.rfinish(); // prevent spam at ignore packet
        return;
    }

    uint8 castCount = 0;
    uint8 castFlags = 0;

    uint32 spellId = 0;
    uint32 miscData = 0;
    uint32 targetMask = 0;
    uint32 targetStringLength = 0;

    float elevation = 0.0f;
    float missileSpeed = 0.0f;

    ObjectGuid targetGuid;
    ObjectGuid itemTargetGuid;
    ObjectGuid destTransportGuid;
    ObjectGuid srcTransportGuid;

    Position srcPos;
    Position destPos;

    std::string targetString;

    // Movement data
    MovementInfo movementInfo;

    ObjectGuid movementTransportGuid;
    ObjectGuid movementGuid;

    bool hasTargetGuid = false;
    bool hasItemTargetGuid = false;

    bool hasTransport = false;
    bool hasTransportTime2 = false;
    bool hasTransportTime3 = false;
    bool hasFallData = false;
    bool hasFallDirection = false;
    bool hasTimestamp = false;
    bool hasSplineElevation = false;
    bool hasPitch = false;
    bool hasOrientation = false;
    bool hasMoveTime = false;

    uint32 RemoveForcesCount = 0;

    Unit* caster = mover;

    hasTargetGuid = !recvData.ReadBit();

    bool hasTargetString = !recvData.ReadBit();

    hasItemTargetGuid = !recvData.ReadBit();

    bool hasCastCount = !recvData.ReadBit();
    bool hasSrcLocation = recvData.ReadBit();
    bool hasDestLocation = recvData.ReadBit();
    bool hasSpellId = !recvData.ReadBit();

    uint8 SpellArchaeologyCount = recvData.ReadBits(2);
    std::vector<SpellArchaeology> SpellArchaeologyProjects(SpellArchaeologyCount);

    bool hasTargetMask = !recvData.ReadBit();
    bool hasMissileSpeed = !recvData.ReadBit();

    for (uint8 i = 0; i < SpellArchaeologyCount; ++i)
        SpellArchaeologyProjects[i].Type = recvData.ReadBits(2);

    bool hasMiscData = !recvData.ReadBit();
    bool hasMovement = recvData.ReadBit();
    bool hasElevation = !recvData.ReadBit();
    bool hasCastFlags = !recvData.ReadBit();

    recvData.ReadGuidMask(targetGuid, 5, 4, 2, 7, 1, 6, 3, 0);

    if (hasDestLocation)
        recvData.ReadGuidMask(destTransportGuid, 1, 3, 5, 0, 2, 6, 7, 4);

    if (hasMovement)
    {
        RemoveForcesCount = recvData.ReadBits(22);

        bool hasSpline = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 4);

        hasTransport = recvData.ReadBit();

        if (hasTransport)
        {
            hasTransportTime2 = recvData.ReadBit();

            recvData.ReadGuidMask(movementTransportGuid, 7, 4, 1, 0, 6, 3, 5);

            hasTransportTime3 = recvData.ReadBit();

            recvData.ReadGuidMask(movementTransportGuid, 2);
        }

        bool HeightChangeFailed = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 7);

        hasOrientation = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 6);

        hasSplineElevation = !recvData.ReadBit();
        hasPitch = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 0);

        bool RemoteTimeValid = !recvData.ReadBit();

        bool hasMovementFlags = !recvData.ReadBit();
        hasTimestamp = !recvData.ReadBit();
        hasMoveTime = !recvData.ReadBit();

        if (hasMovementFlags)
            movementInfo.flags = recvData.ReadBits(30);

        recvData.ReadGuidMask(movementGuid, 1, 3, 2, 5);

        hasFallData = recvData.ReadBit();

        if (hasFallData)
            hasFallDirection = recvData.ReadBit();

        bool hasMovementFlags2 = !recvData.ReadBit();

        if (hasMovementFlags2)
            movementInfo.flags2 = recvData.ReadBits(13);
    }

    recvData.ReadGuidMask(itemTargetGuid, 1, 0, 7, 4, 6, 5, 3, 2);

    if (hasSrcLocation)
        recvData.ReadGuidMask(srcTransportGuid, 4, 5, 3, 0, 7, 1, 6, 2);

    if (hasTargetMask)
        targetMask = recvData.ReadBits(20);

    if (hasCastFlags)
        castFlags = recvData.ReadBits(5);

    if (hasTargetString)
        targetStringLength = recvData.ReadBits(7);

    for (uint8 i = 0; i < SpellArchaeologyCount; ++i)
    {
        recvData >> SpellArchaeologyProjects[i].ID;
        recvData >> SpellArchaeologyProjects[i].Quantity;
    }

    if (hasMovement)
    {
        recvData >> movementInfo.pos.m_positionX;

        recvData.ReadGuidBytes(movementGuid, 0);

        if (hasTransport)
        {
            recvData.ReadGuidBytes(movementTransportGuid, 2);

            recvData >> movementInfo.transport.seat;

            recvData.ReadGuidBytes(movementTransportGuid, 3, 7);

            recvData >> movementInfo.transport.pos.m_positionX;

            recvData.ReadGuidBytes(movementTransportGuid, 5);

            if (hasTransportTime3)
                recvData >> movementInfo.transport.vehicleId;

            recvData >> movementInfo.transport.pos.m_positionZ;
            recvData >> movementInfo.transport.pos.m_positionY;

            recvData.ReadGuidBytes(movementTransportGuid, 6, 1);

            movementInfo.transport.pos.SetOrientation(recvData.read<float>());

            recvData.ReadGuidBytes(movementTransportGuid, 4);

            if (hasTransportTime2)
                recvData >> movementInfo.transport.prevTime;

            recvData.ReadGuidBytes(movementTransportGuid, 0);

            recvData >> movementInfo.transport.time;

        }

        recvData.ReadGuidBytes(movementGuid, 5);

        if (hasFallData)
        {
            recvData >> movementInfo.fall.fallTime;
            recvData >> movementInfo.fall.zspeed;

            if (hasFallDirection)
            {
                recvData >> movementInfo.fall.sinAngle;
                recvData >> movementInfo.fall.xyspeed;
                recvData >> movementInfo.fall.cosAngle;
            }
        }

        if (hasSplineElevation)
            recvData >> movementInfo.splineElevation;

        recvData.ReadGuidBytes(movementGuid, 6);

        if (hasMoveTime)
        {
            uint32 MoveTime = 0;
            recvData >> MoveTime;
        }

        recvData.ReadGuidBytes(movementGuid, 4);

        if (hasOrientation)
            movementInfo.pos.SetOrientation(recvData.read<float>());

        if (hasTimestamp)
            recvData >> movementInfo.time;

        recvData.ReadGuidBytes(movementGuid, 1);

        if (hasPitch)
            movementInfo.pitch = G3D::wrap(recvData.read<float>(), float(-M_PI), float(M_PI));

        recvData.ReadGuidBytes(movementGuid, 3);

        for (uint32 i = 0; i != RemoveForcesCount; i++)
        {
            uint32 RemoveForcesIDs = 0;
            recvData >> RemoveForcesIDs;
        }

        recvData >> movementInfo.pos.m_positionY;

        recvData.ReadGuidBytes(movementGuid, 7);

        recvData >> movementInfo.pos.m_positionZ;

        recvData.ReadGuidBytes(movementGuid, 2);
    }

    recvData.ReadGuidBytes(itemTargetGuid, 4, 2, 1, 5, 7, 3, 6, 0);

    if (hasDestLocation)
    {
        float x, y, z;

        recvData.ReadGuidBytes(destTransportGuid, 2);

        recvData >> x;

        recvData.ReadGuidBytes(destTransportGuid, 4, 1, 0, 3);

        recvData >> y;

        recvData.ReadGuidBytes(destTransportGuid, 7);

        recvData >> z;

        recvData.ReadGuidBytes(destTransportGuid, 5, 6);

        destPos.Relocate(x, y, z);
    }
    else
    {
        destTransportGuid = caster->GetTransportGUID();
        if (destTransportGuid)
            destPos.Relocate(caster->GetTransportPosition());
        else
            destPos.Relocate(caster);
    }

    recvData.ReadGuidBytes(targetGuid, 3, 4, 7, 6, 2, 0, 1, 5);

    if (hasSrcLocation)
    {
        float x, y, z;
        recvData >> y;

        recvData.ReadGuidBytes(srcTransportGuid, 5, 1, 7, 6);

        recvData >> x;

        recvData.ReadGuidBytes(srcTransportGuid, 3, 2, 0, 4);

        recvData >> z;

        srcPos.Relocate(x, y, z);
    }
    else
    {
        srcTransportGuid = caster->GetTransportGUID();
        if (srcTransportGuid)
            srcPos.Relocate(caster->GetTransportPosition());
        else
            srcPos.Relocate(caster);
    }

    if (hasTargetString)
        targetString = recvData.ReadString(targetStringLength);

    if (hasMissileSpeed)
        recvData >> missileSpeed;

    if (hasElevation)
        recvData >> elevation;

    if (hasCastCount)
        recvData >> castCount;

    if (hasSpellId)
        recvData >> spellId;

    if (hasMiscData)
        recvData >> miscData;

    TC_LOG_DEBUG("network", "WORLD: got cast spell packet, castCount: %u, spellId: %u, castFlags: %u, data length = %u", castCount, spellId, castFlags, (uint32)recvData.size());

    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId);
    if (!spellInfo)
    {
        TC_LOG_DEBUG("network", "WORLD: unknown spell id %u", spellId);
        return;
    }

    if (spellInfo->IsPassive())
        return;

    if (caster->GetTypeId() == TYPEID_UNIT && !caster->ToCreature()->HasSpell(spellId))
    {
        // If the vehicle creature does not have the spell but it allows the passenger to cast own spells
        // change caster to player and let him cast
        if (!_player->IsOnVehicle(caster) || spellInfo->CheckVehicle(_player) != SPELL_CAST_OK)
            return;

        caster = _player;
    }

    auto IsExcludedSpell = [spellInfo, castFlags]() -> bool
    {
        if (spellInfo->HasAttribute(SPELL_ATTR8_RAID_MARKER))
            return true;

        if (castFlags & 0x8)
            return true;

        switch (spellInfo->Id)
        {
            case 67869: // Knocking
                return true;
            default:
                return false;
        }
    };

    if (caster->GetTypeId() == TYPEID_PLAYER && !caster->ToPlayer()->HasActiveSpell(spellId) && !IsExcludedSpell())
        return;

    // Check possible spell cast overrides
    spellInfo = caster->GetCastSpellInfo(spellInfo);

    // Client is resending autoshot cast opcode when other spell is casted during shoot rotation
    // Skip it to prevent "interrupt" message
    if (spellInfo->IsAutoRepeatRangedSpell() && caster->GetCurrentSpell(CURRENT_AUTOREPEAT_SPELL)
        && caster->GetCurrentSpell(CURRENT_AUTOREPEAT_SPELL)->m_spellInfo == spellInfo)
        return;

    // can't use our own spells when we're in possession of another unit,
    if (_player->IsPossessing())
        return;

    // client provided targets
    SpellCastTargets targets(caster, targetMask, targetGuid, itemTargetGuid, srcTransportGuid, destTransportGuid, srcPos, destPos, elevation, missileSpeed, targetString);

    // auto-selection buff level base at target level (in spellInfo)
    if (targets.GetUnitTarget())
    {
        SpellInfo const* actualSpellInfo = spellInfo->GetAuraRankForLevel(targets.GetUnitTarget()->GetLevel());

        // if rank not found then function return NULL but in explicit cast case original spell can be casted and later failed with appropriate error message
        if (actualSpellInfo)
            spellInfo = actualSpellInfo;
    }

    Spell* spell = new Spell(caster, spellInfo, TRIGGERED_NONE, ObjectGuid::Empty, false);

    spell->m_cast_count = castCount;                       // set count of casts
    spell->m_misc.Data = miscData;

    spell->m_SpellArchaeologyProjects = SpellArchaeologyProjects;

    spell->prepare(&targets);
}

void WorldSession::HandleCancelCastOpcode(WorldPacket& recvData)
{
    uint32 spellId = 0;
    uint8 counter = 0;

    bool hasCounter = !recvData.ReadBit();
    bool hasSpellId = !recvData.ReadBit();

    if (hasSpellId)
        recvData >> spellId;

    if (hasCounter)
        recvData >> counter;

    if (_player->IsNonMeleeSpellCasted(false))
        _player->InterruptNonMeleeSpells(false, spellId, false);
}

void WorldSession::HandleCancelAuraOpcode(WorldPacket& recvData)
{
    uint32 spellId = 0;
    bool HasGuid = false;

    ObjectGuid guid;

    recvData >> spellId;

    HasGuid = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 6, 5, 1, 0, 4, 3, 2, 7);
    recvData.ReadGuidBytes(guid, 3, 2, 1, 0, 4, 7, 5, 6);

    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId);
    if (!spellInfo)
        return;

    // not allow remove spells with attr SPELL_ATTR0_CANT_CANCEL
    if (spellInfo->HasAttribute(SPELL_ATTR0_CANT_CANCEL))
        return;

    // channeled spell case (it currently casted then)
    if (spellInfo->IsChanneled())
    {
        if (Spell* curSpell = _player->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
            if (curSpell->m_spellInfo->Id == spellId)
                _player->InterruptSpell(CURRENT_CHANNELED_SPELL);
        return;
    }

    // non channeled case:
    // don't allow remove non positive spells
    // don't allow cancelling passive auras (some of them are visible)
    if (!spellInfo->IsPositive() || spellInfo->IsPassive())
        return;

    // maybe should only remove one buff when there are multiple?

    _player->RemoveOwnedAura(spellId, guid, 0, AURA_REMOVE_BY_CANCEL);
}

void WorldSession::HandleCancelMountAuraOpcode(WorldPacket& /*recvData*/)
{
    //If player is not mounted, so go out :)
    if (!_player->IsMounted())                              // not blizz like; no any messages on blizz
    {
        ChatHandler(this).SendSysMessage(LANG_CHAR_NON_MOUNTED);
        return;
    }

    if (_player->IsInFlight())                               // not blizz like; no any messages on blizz
    {
        ChatHandler(this).SendSysMessage(LANG_YOU_IN_FLIGHT);
        return;
    }

    _player->RemoveAurasByType(SPELL_AURA_MOUNTED, [](AuraApplication const* aurApp)
    {
        SpellInfo const* spellInfo = aurApp->GetBase()->GetSpellInfo();
        return !spellInfo->HasAttribute(SPELL_ATTR0_CANT_CANCEL) && spellInfo->IsPositive() && !spellInfo->IsPassive();
    });
}

void WorldSession::HandleCancelAutoRepeatSpellOpcode(WorldPacket& /*recvData*/)
{
    // may be better send SMSG_CANCEL_AUTO_REPEAT?
    // cancel and prepare for deleting
    _player->InterruptSpell(CURRENT_AUTOREPEAT_SPELL);
}

void WorldSession::HandleCancelChanneling(WorldPacket& recvData)
{
    recvData.read_skip<uint32>();                          // spellid, not used

    // ignore for remote control state (for player case)
    Unit* mover = _player->GetMover();
    if (mover != _player && mover->GetTypeId() == TYPEID_PLAYER)
        return;

    mover->InterruptSpell(CURRENT_CHANNELED_SPELL);
}

void WorldSession::HandleTotemDestroyed(WorldPacket& recvData)
{
    // ignore for remote control state
    if (_player->GetMover() != _player)
    {
        recvData.rfinish();
        return;
    }

    uint8 slotId = 0;

    ObjectGuid guid;

    recvData >> slotId;

    recvData.ReadGuidMask(guid, 4, 2, 1, 3, 0, 6, 7, 5);
    recvData.ReadGuidBytes(guid, 6, 2, 4, 1, 5, 0, 3, 7);

    ++slotId;
    if (slotId >= MAX_TOTEM_SLOT)
        return;

    if (!_player->m_SummonSlot[slotId])
        return;

    Creature* totem = GetPlayer()->GetMap()->GetCreature(_player->m_SummonSlot[slotId]);
    if (totem && totem->IsTotem() && totem->GetGUID() == guid)
        totem->ToTotem()->UnSummon();
}

void WorldSession::HandleSelfResOpcode(WorldPacket& /*recvData*/)
{
    // silent return, client should display error by itself and not send this opcode
    if (_player->HasAuraType(SPELL_AURA_PREVENT_RESURRECTION))
        return;

    if (_player->GetUInt32Value(PLAYER_SELF_RES_SPELL))
    {
        SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(_player->GetUInt32Value(PLAYER_SELF_RES_SPELL));
        if (spellInfo)
            _player->CastSpell(_player, spellInfo, false, nullptr);

        _player->SetUInt32Value(PLAYER_SELF_RES_SPELL, 0);
    }
}

void WorldSession::HandleSpellClick(WorldPacket& recvData)
{
    ObjectGuid guid;

    bool TryAutoDismount = false;

    recvData.ReadGuidMask(guid, 7, 4, 0, 3, 6, 5);

    TryAutoDismount = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 1, 2);

    recvData.ReadGuidBytes(guid, 6, 1, 5, 4, 7, 2, 3, 0);

    // this will get something not in world. crash
    Creature* unit = ObjectAccessor::GetCreatureOrPetOrVehicle(*_player, guid);
    if (!unit)
        return;

    /// @todo Unit::SetCharmedBy: 28782 is not in world but 0 is trying to charm it! -> crash
    if (!unit->IsInWorld())
        return;

    unit->HandleSpellClick(_player);
}

void WorldSession::HandleMirrorImageDataRequest(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint32 displayId = 0;

    recvData >> displayId;

    recvData.ReadGuidMask(guid, 0, 2, 1, 6, 5, 4, 7, 3);
    recvData.ReadGuidBytes(guid, 6, 0, 3, 5, 4, 2, 1, 7);

    // Get unit for which data is needed by client
    Unit* unit = ObjectAccessor::GetUnit(*_player, guid);
    if (!unit)
        return;

    if (!unit->HasAuraType(SPELL_AURA_CLONE_CASTER))
        return;

    // Get creator of the unit (SPELL_AURA_CLONE_CASTER does not stack)
    Unit* creator = unit->GetAuraEffectsByType(SPELL_AURA_CLONE_CASTER).front()->GetCaster();
    if (!creator)
        return;

    if (Player* player = creator->ToPlayer())
    {
        WorldPacket data(SMSG_MIRROR_IMAGE_COMPONENTED_DATA, 2 * (1 + 8) + 2 + 1 + 4 + 2 + 1 + 1 + 1 + (11 * 4) + 1 + 1 + 1);

        Guild* guild = player->GetGuild();
        ObjectGuid guildGuid = guild ? guild->GetGUID() : ObjectGuid::Empty;

        data.WriteGuidMask(guid, 4);

        data.WriteGuidMask(guildGuid, 3, 6);

        data.WriteGuidMask(guid, 0);

        data.WriteGuidMask(guildGuid, 7);

        data.WriteGuidMask(guid, 1, 5);

        data.WriteGuidMask(guildGuid, 2, 1);

        data.WriteGuidMask(guid, 7);

        data.WriteGuidMask(guildGuid, 4, 0);

        data.WriteGuidMask(guid, 2);

        data.WriteGuidMask(guildGuid, 5);

        data.WriteGuidMask(guid, 3);

        data.WriteBits(11, 22); // item slots count

        data.WriteGuidMask(guid, 6);

        data.FlushBits();

        data << uint8(player->GetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_COLOR_ID)); // haircolor
        data << uint32(displayId);
        data << uint8(player->GetByteValue(PLAYER_BYTES_2, PLAYER_BYTES_2_OFFSET_FACIAL_STYLE));    // facial hair

        data.WriteGuidBytes(guildGuid, 6, 4);

        data.WriteGuidBytes(guid, 7);

        data.WriteGuidBytes(guildGuid, 1);

        data.WriteGuidBytes(guid, 3);

        data << uint8(player->GetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_STYLE_ID)); // hair

        data.WriteGuidBytes(guid, 2, 0);

        data << uint8(creator->GetRace());

        data << uint8(player->GetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_SKIN_ID)); // skin

        data.WriteGuidBytes(guildGuid, 7);

        static EquipmentSlots const itemSlots[] =
        {
            EQUIPMENT_SLOT_HEAD,
            EQUIPMENT_SLOT_SHOULDERS,
            EQUIPMENT_SLOT_BODY,
            EQUIPMENT_SLOT_CHEST,
            EQUIPMENT_SLOT_WAIST,
            EQUIPMENT_SLOT_LEGS,
            EQUIPMENT_SLOT_FEET,
            EQUIPMENT_SLOT_WRISTS,
            EQUIPMENT_SLOT_HANDS,
            EQUIPMENT_SLOT_TABARD,
            EQUIPMENT_SLOT_BACK,
            EQUIPMENT_SLOT_END
        };

        // Display items in visible slots
        for (EquipmentSlots const* itr = &itemSlots[0]; *itr != EQUIPMENT_SLOT_END; ++itr)
        {
            if (*itr == EQUIPMENT_SLOT_HEAD && player->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_HIDE_HELM))
                data << uint32(0);
            else if (*itr == EQUIPMENT_SLOT_BACK && player->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_HIDE_CLOAK))
                data << uint32(0);
            else if (Item const* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, *itr))
                data << uint32(item->GetTemplate()->GetDisplayId());
            else
                data << uint32(0);
        }

        data.WriteGuidBytes(guid, 4);

        data << uint8(creator->GetClass());
        data << uint8(creator->GetGender());
        data << uint8(player->GetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_FACE_ID)); // face

        data.WriteGuidBytes(guid, 5);

        data.WriteGuidBytes(guildGuid, 3, 2);

        data.WriteGuidBytes(guid, 1);

        data.WriteGuidBytes(guildGuid, 0, 5);

        data.WriteGuidBytes(guid, 6);

        SendPacket(&data);
    }
    else
    {
        WorldPacket data(SMSG_MIRROR_IMAGE_CREATURE_DATA, 1 + 8 + 4);

        data.WriteGuidMask(guid, 0, 1, 3, 5, 7, 6, 4, 2);

        data.WriteGuidBytes(guid, 0, 3, 6, 5, 7);

        data << uint32(displayId);

        data.WriteGuidBytes(guid, 4, 2, 1);

        SendPacket(&data);
    }
}

void WorldSession::HandleUpdateProjectilePosition(WorldPacket& recvData)
{
    ObjectGuid casterGuid;
    uint32 spellId;
    uint8 castCount;
    float x, y, z;    // Position of missile hit

    //recvData >> casterGuid;
    recvData >> spellId;
    recvData >> castCount;
    recvData >> x;
    recvData >> y;
    recvData >> z;

    Unit* caster = ObjectAccessor::GetUnit(*_player, casterGuid);
    if (!caster)
        return;

    Spell* spell = caster->FindCurrentSpellBySpellId(spellId);
    if (!spell || !spell->m_targets.HasDst())
        return;

    Position pos = *spell->m_targets.GetDstPos();
    pos.Relocate(x, y, z);
    spell->m_targets.ModDst(pos);

    // we changed dest, recalculate flight time
    spell->RecalculateDelayMomentForDst();

    WorldPacket data(SMSG_SET_PROJECTILE_POSITION, 1 + 8 + 1 + 4 + 4 + 4);

    data.WriteGuidMask(casterGuid, 2, 7, 3, 0, 5, 6, 4, 1);

    data.WriteGuidBytes(casterGuid, 6, 5, 4);

    data << float(y);

    data.WriteGuidBytes(casterGuid, 2, 0, 3, 7);

    data << uint8(castCount);
    data << float(x);
    data << float(z);

    data.WriteGuidBytes(casterGuid, 1);

    caster->SendMessageToSet(&data, true);
}

void WorldSession::HandleRequestCategoryCooldowns(WorldPacket& /*recvData*/)
{
    _player->SendSpellCategoryCooldowns();
}
