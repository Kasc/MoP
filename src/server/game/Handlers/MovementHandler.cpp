/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Chat.h"
#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "Corpse.h"
#include "Player.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Transport.h"
#include "Battleground.h"
#include "WaypointMovementGenerator.h"
#include "InstanceSaveMgr.h"
#include "ObjectMgr.h"
#include "MovementStructures.h"
#include "BattlePetMgr.h"
#include "BattlePetQueue.h"
#include "Vehicle.h"
#include "Pet.h"
#include "GameTime.h"

void WorldSession::HandleMoveWorldportAckOpcode(WorldPacket & /*recvData*/)
{
    HandleMoveWorldportAckOpcode();
}

void WorldSession::HandleMoveWorldportAckOpcode()
{
    // ignore unexpected far teleports
    if (!GetPlayer()->IsBeingTeleportedFar())
        return;

    GetPlayer()->SetSemaphoreTeleportFar(false);

    // get the teleport destination
    WorldLocation const& loc = GetPlayer()->GetTeleportDest();

    // possible errors in the coordinate validity check
    if (!MapManager::IsValidMapCoord(loc))
    {
        LogoutPlayer(false);
        return;
    }

    // get the destination map entry, not the current one, this will fix homebind and reset greeting
    MapEntry const* mEntry = sMapStore.LookupEntry(loc.GetMapId());
    InstanceTemplate const* mInstance = sObjectMgr->GetInstanceTemplate(loc.GetMapId());

    // reset instance validity, except if going to an instance inside an instance
    if (GetPlayer()->m_InstanceValid == false && !mInstance)
        GetPlayer()->m_InstanceValid = true;

    Map* oldMap = GetPlayer()->GetMap();
    Map* newMap = sMapMgr->CreateMap(loc.GetMapId(), GetPlayer());

    if (GetPlayer()->IsInWorld())
    {
        TC_LOG_ERROR("network", "%s %s is still in world when teleported from map %s (%u) to new map %s (%u)", GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), oldMap->GetMapName(), oldMap->GetId(), newMap ? newMap->GetMapName() : "Unknown", loc.GetMapId());
        oldMap->RemovePlayerFromMap(GetPlayer(), false);
    }

    // relocate the player to the teleport destination
    // the CannotEnter checks are done in TeleporTo but conditions may change
    // while the player is in transit, for example the map may get full
    if (!newMap || newMap->CannotEnter(GetPlayer()))
    {
        TC_LOG_ERROR("network", "Map %d (%s) could not be created for player %d (%s), porting player to homebind", loc.GetMapId(), newMap ? newMap->GetMapName() : "Unknown", GetPlayer()->GetGUID().GetCounter(), GetPlayer()->GetName().c_str());
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }

    float x = loc.GetPositionX();
    float y = loc.GetPositionY();
    float z = loc.GetPositionZ();
    float o = loc.GetOrientation();

    if (GetPlayer()->HasMovementFlag(MOVEMENTFLAG_HOVER))
        z += GetPlayer()->GetFloatValue(UNIT_HOVER_HEIGHT);

    GetPlayer()->Relocate(x, y, z, o);

    GetPlayer()->SetFallInformation(GetPlayer()->GetPositionZ());

    GetPlayer()->ResetMap();
    GetPlayer()->SetMap(newMap);

    GetPlayer()->SendInitialPacketsBeforeAddToMap();

    if (!GetPlayer()->GetMap()->AddPlayerToMap(GetPlayer()))
    {
        TC_LOG_ERROR("network", "WORLD: failed to teleport player %s (%d) to map %d (%s) because of unknown reason!",
            GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().GetCounter(), loc.GetMapId(), newMap ? newMap->GetMapName() : "Unknown");
        GetPlayer()->ResetMap();
        GetPlayer()->SetMap(oldMap);
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }

    if (Transport* transport = _player->GetTeleportTransport())
    {
        transport->CalculatePassengerOffset(x, y, z, &o);
        _player->SetTransportPosition(x, y, z, o);

        transport->AddPassenger(_player);

        _player->ResetTeleportTransport();
    }

    // transport teleport couldn't teleport us to the same map (some other teleport pending, reqs not met, etc.), but we still have transport set until player moves! clear it if map differs (crashfix)
    if (Transport* t = _player->GetTransport())
        if (!t->IsInMap(_player))
        {
            t->RemovePassenger(_player);
            _player->m_transport = NULL;
            _player->ResetTransport();
        }

    // multithreading crashfix
    if (!_player->GetHostileRefManager().isEmpty())
        _player->GetHostileRefManager().deleteReferences();

    // battleground state prepare (in case join to BG), at relogin/tele player not invited
    // only add to bg group and object, if the player was invited (else he entered through command)
    if (_player->InBattleground())
    {
        // cleanup setting if outdated
        if (!mEntry->IsBattlegroundOrArena())
        {
            // We're not in BG
            _player->SetBattlegroundId(0, BATTLEGROUND_TYPE_NONE);
            // reset destination bg team
            _player->SetBGTeam(0);
        }
        // join to bg case
        else if (Battleground* bg = _player->GetBattleground())
        {
            if (_player->IsInvitedForBattlegroundInstance(_player->GetBattlegroundId()))
                bg->AddPlayer(_player);
        }
    }

    GetPlayer()->SendInitialPacketsAfterAddToMap();

    // flight fast teleport case
    if (GetPlayer()->GetMotionMaster()->GetCurrentMovementGeneratorType() == FLIGHT_MOTION_TYPE)
    {
        if (!_player->InBattleground())
        {
            // short preparations to continue flight
            FlightPathMovementGenerator* flight = (FlightPathMovementGenerator*)(GetPlayer()->GetMotionMaster()->top());
            flight->Initialize(GetPlayer());
            return;
        }

        // battleground state prepare, stop flight
        GetPlayer()->GetMotionMaster()->MovementExpired();
        GetPlayer()->CleanupAfterTaxiFlight();
    }

    // resurrect character at enter into instance where his corpse exist after add to map
    if (mEntry->IsDungeon() && !GetPlayer()->IsAlive())
        if (GetPlayer()->GetCorpseLocation().GetMapId() == mEntry->ID)
        {
            GetPlayer()->ResurrectPlayer(0.5f, false);
            GetPlayer()->SpawnCorpseBones();
        }

    bool allowMount = !mEntry->IsDungeon() || mEntry->IsBattlegroundOrArena();
    if (mInstance)
    {
        // check if this instance has a reset time and send it to player if so
        Difficulty diff = GetPlayer()->GetDifficultyID(mEntry);
        if (MapDifficultyEntry const* mapDiff = sDBCManager->GetMapDifficultyData(mEntry->ID, diff))
        {
            if (mapDiff->RaidDuration)
            {
                if (time_t timeReset = sInstanceSaveMgr->GetResetTimeFor(mEntry->ID, diff))
                {
                    uint32 timeleft = uint32(timeReset - time(NULL));
                    GetPlayer()->SendInstanceResetWarning(mEntry->ID, diff, timeleft, true);
                }
            }
        }

        // check if instance is valid
        if (!GetPlayer()->CheckInstanceValidity(false))
            GetPlayer()->m_InstanceValid = false;

        // instance mounting is handled in InstanceTemplate
        allowMount = mInstance->AllowMount;
    }

    // mount allow check
    if (!allowMount)
        _player->RemoveAurasByType(SPELL_AURA_MOUNTED);

    // honorless target
    if (GetPlayer()->pvpInfo.IsHostile)
        GetPlayer()->CastSpell(GetPlayer(), 2479, true);

    // in friendly area
    else if (GetPlayer()->IsPvP() && !GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
        GetPlayer()->UpdatePvP(false, false);

    // resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    // resummon battle pet
    GetPlayer()->GetBattlePetMgr()->ResummonLastBattlePet();

    //lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();

    // pvp pet battle system (on far teleport remove from queue and forfeit battle)
    if (PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(_player->GetGUID()))
    {
        sPetBattleQueueMgr->RemovePlayerFromPetBattleQueue(_player->GetGUID());

        if (PetBattleTeam* team = petBattle->GetTeam(_player->GetGUID()))
            sPetBattleSystem->ForfeitBattle(petBattle, team);

        SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_REMOVED);
    }
}

// Not implemented yet!
void WorldSession::HandleSuspendTokenResponse(WorldPacket& recvData)
{
    uint32 movementCount = 0;
    recvData >> movementCount;

    if (!_player->IsBeingTeleportedFar())
        return;

    WorldLocation const& loc = GetPlayer()->GetTeleportDest();

    if (sMapStore.AssertEntry(loc.GetMapId())->IsDungeon())
    {
        WorldPacket data(SMSG_UPDATE_LAST_INSTANCE, 4);
        data << uint32(loc.GetMapId());
        SendPacket(&data);
    }

    WorldPacket data(SMSG_NEW_WORLD, 4 + 4 + 4 + 4 + 4);
    data << float(loc.GetPositionX());
    data << int32(loc.GetMapId());
    data << float(loc.GetPositionY());
    data << float(loc.GetPositionZ());
    data << float(loc.GetOrientation());
    SendPacket(&data);
}

void WorldSession::HandleMoveTeleportAck(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint32 AckIndex = 0;
    uint32 Time = 0;

    recvData >> Time;
    recvData >> AckIndex;

    recvData.ReadGuidMask(guid, 0, 7, 3, 5, 4, 6, 1, 2);
    recvData.ReadGuidBytes(guid, 4, 1, 6, 7, 0, 2, 5, 3);

    Unit* mover = _player->GetMover();
    if (!mover)
        return;

    // skip old packet
    if (mover->GetMovementAckIndexValue(MOVEMENT_ACK_TELEPORT) != AckIndex)
        return;

    Player* plMover = mover->ToPlayer();
    if (!plMover || !plMover->IsBeingTeleportedNear())
        return;

    if (guid != plMover->GetGUID())
        return;

    plMover->FinishTeleportMove();
    plMover->SendTeleportUpdate(Time);
}

void WorldSession::HandleMovementOpcodes(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo info;
    info.ReadFromPacket(recvData);

    //Stop Emote
    if (Player* plrMover = mover->ToPlayer())
        if (plrMover->GetUInt32Value(UNIT_EMOTE_STATE) != 0)
            plrMover->SetUInt32Value(UNIT_EMOTE_STATE, EMOTE_ONESHOT_NONE);

    HandleMovementInfo(info, mover, recvData.GetOpcode());
}

void WorldSession::HandleForceSpeedChangeAck(WorldPacket& recvData)
{
    UnitMoveType move_type;
    switch (recvData.GetOpcode())
    {
        case CMSG_MOVE_FORCE_WALK_SPEED_CHANGE_ACK:
            move_type = MOVE_WALK;
            break;
        case CMSG_MOVE_FORCE_RUN_SPEED_CHANGE_ACK:
            move_type = MOVE_RUN;
            break;
        case CMSG_MOVE_FORCE_RUN_BACK_SPEED_CHANGE_ACK:
            move_type = MOVE_RUN_BACK;
            break;
        case CMSG_MOVE_FORCE_SWIM_SPEED_CHANGE_ACK:
            move_type = MOVE_SWIM;
            break;
        case CMSG_MOVE_FORCE_SWIM_BACK_SPEED_CHANGE_ACK:
            move_type = MOVE_SWIM_BACK;
            break;
        case CMSG_MOVE_FORCE_TURN_RATE_CHANGE_ACK:
            move_type = MOVE_TURN_RATE;
            break;
        case CMSG_MOVE_FORCE_FLIGHT_SPEED_CHANGE_ACK:
            move_type = MOVE_FLIGHT;
            break;
        case CMSG_MOVE_FORCE_FLIGHT_BACK_SPEED_CHANGE_ACK:
            move_type = MOVE_FLIGHT_BACK;
            break;
        case CMSG_MOVE_FORCE_PITCH_RATE_CHANGE_ACK:
            move_type = MOVE_PITCH_RATE;
            break;
        default:
            TC_LOG_ERROR("network", "WorldSession::HandleForceSpeedChangeAck: Unknown move type opcode: %u", recvData.GetOpcode());
            return;
    }

    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    /* extract packet */
    MovementInfo movementInfo;

    static MovementStatusElements const speedElement = MSEExtraFloat;
    ExtraMovementStatusElement extras(&speedElement);

    movementInfo.ReadFromPacket(recvData, &extras);

    float newSpeed = extras.GetData().floatData[0];
    if (newSpeed != mover->GetSpeed(move_type))
        return;

    static Opcodes const moveTypeToOpcode[MAX_MOVE_TYPE] =
    {
        SMSG_MOVE_UPDATE_WALK_SPEED,
        SMSG_MOVE_UPDATE_RUN_SPEED,
        SMSG_MOVE_UPDATE_RUN_BACK_SPEED,
        SMSG_MOVE_UPDATE_SWIM_SPEED,
        SMSG_MOVE_UPDATE_SWIM_BACK_SPEED,
        SMSG_MOVE_UPDATE_TURN_RATE,
        SMSG_MOVE_UPDATE_FLIGHT_SPEED ,
        SMSG_MOVE_UPDATE_FLIGHT_BACK_SPEED,
        SMSG_MOVE_UPDATE_PITCH_RATE
    };

    HandleMovementAck(movementInfo, mover, move_type, moveTypeToOpcode[move_type], &extras);
}

void WorldSession::HandleSetActiveMoverOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    bool Unset = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 3, 0, 2, 1, 5, 4, 7, 6);
    recvData.ReadGuidBytes(guid, 3, 4, 5, 2, 7, 0, 1, 6);

    ObjectGuid moverGUID = ObjectGuid::Empty;
    if (Unit* mover = _player->GetMover())
        if (mover->IsInWorld())
            moverGUID = mover->GetGUID();

    if (Unset)
    {
        if (moverGUID != _player->GetGUID())
            _player->SetMover(_player);
    }
    else if (_player->IsInWorld())
    {
        if (moverGUID != guid)
            TC_LOG_ERROR("network", "HandleSetActiveMoverOpcode: incorrect mover guid: mover is %s and should be %s", guid.ToString().c_str(), moverGUID.ToString().c_str());
    }
}

void WorldSession::HandleMountSpecialAnimOpcode(WorldPacket& /*recvData*/)
{
    ObjectGuid guid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_MOUNTSPECIAL_ANIM, 1 + 8);

    data.WriteGuidMask(guid, 5, 7, 0, 3, 2, 1, 4, 6);
    data.WriteGuidBytes(guid, 7, 2, 0, 4, 5, 6, 1, 3);

    GetPlayer()->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveKnockBackAck(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    if (HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_KNOCK_BACK, SMSG_MOVE_UPDATE_KNOCK_BACK))
        mover->SetLastVerticalSpeed(movementInfo.fall.zspeed);
}

void WorldSession::HandleSummonResponseOpcode(WorldPacket& recvData)
{
    if (!_player->IsAlive() || _player->IsInCombat())
        return;

    ObjectGuid summonerGuid;
    bool Agree = false;

    recvData.ReadGuidMask(summonerGuid, 1, 3, 5, 2);

    Agree = recvData.ReadBit();

    recvData.ReadGuidMask(summonerGuid, 7, 0, 4, 6);

    recvData.ReadGuidBytes(summonerGuid, 0, 1, 6, 3, 5, 4, 2, 7);

    _player->SummonIfPossible(Agree);
}

void WorldSession::HandleSetCollisionHeightAck(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    static MovementStatusElements const heightElement[] = { MSEExtraInt32, MSEExtraFloat, MSEExtraBits2 };
    ExtraMovementStatusElement extra(heightElement);

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData, &extra);

    float Height = extra.GetData().floatData[0];
    if (mover->GetCollisionHeight() != Height)
        return;

    UpdateCollisionHeightReason reason = UpdateCollisionHeightReason(extra.GetData().bitsData);
    if (mover->GetCollisionType() != reason)
        return;

    uint32 MountID = uint32(extra.GetData().intData[0]);
    if (mover->GetUInt32Value(UNIT_MOUNT_DISPLAY_ID) != MountID)
        return;

    MoveStateChange moveState;
    moveState.CollisionHeight = boost::in_place();
    moveState.CollisionHeight->Height = Height;
    moveState.CollisionHeight->Scale = mover->GetObjectScale();

    ExtraMovementStatusElement extra2;
    GetExtraMovementStatusElements(SMSG_MOVE_UPDATE_COLLISION_HEIGHT, moveState, extra2);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_COLLISION_HEIGHT, SMSG_MOVE_UPDATE_COLLISION_HEIGHT, &extra2);
}

void WorldSession::HandleMoveRootAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_ROOT);
}

void WorldSession::HandleFallAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_FALL);
}

void WorldSession::HandleGravityAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_GRAVITY);
}

void WorldSession::HandleHoverAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_HOVER);
}

void WorldSession::HandleTransitionAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_TRANSITION_SWIM_FLY);
}

void WorldSession::HandleTurnWhileFallingAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_TURN_WHILE_FALLING);
}

void WorldSession::HandleMoveSetCanFlyAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_CAN_FLY);
}

void WorldSession::HandleWaterWalkAckOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_WATER_WALK);
}

void WorldSession::HandleMoveSplineDoneOpcode(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    static MovementStatusElements const extraElement = MSEExtraInt32;
    ExtraMovementStatusElement extras(&extraElement);

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData, &extras);

    if (mover->GetGUID() != movementInfo.guid)
        return;

    uint32 splineID = extras.GetData().intData[0];
    if (mover->GetSplineId() != splineID)
        return;

    movementInfo.SanitizeFlags(mover);

    mover->SetMovementInfo(&movementInfo);

    // in taxi flight packet received in 2 case:
    // 1) end taxi path in far (multi-node) flight
    // 2) switch from one map to other in case multim-map taxi path
    // we need process only (1)

    Player* plrMover = mover->ToPlayer();
    if (!plrMover)
        return;

    uint32 curDest = plrMover->m_taxi.GetTaxiDestination();
    if (curDest)
    {
        TaxiNodesEntry const* curDestNode = sTaxiNodesStore.LookupEntry(curDest);

        // far teleport case
        if (curDestNode && curDestNode->MapID != plrMover->GetMapId())
        {
            if (plrMover->GetMotionMaster()->GetCurrentMovementGeneratorType() == FLIGHT_MOTION_TYPE)
            {
                // short preparations to continue flight
                FlightPathMovementGenerator* flight = (FlightPathMovementGenerator*)(plrMover->GetMotionMaster()->top());

                flight->SetCurrentNodeAfterTeleport();
                TaxiPathNodeEntry const* node = flight->GetPath()[flight->GetCurrentNode()];
                flight->SkipCurrentNode();

                plrMover->TeleportTo(curDestNode->MapID, node->Loc.X, node->Loc.Y, node->Loc.Z, plrMover->GetOrientation());
            }
        }

        return;
    }

    // at this point only 1 node is expected (final destination)
    if (plrMover->m_taxi.GetPath().size() != 1)
        return;

    plrMover->CleanupAfterTaxiFlight();
    plrMover->SetFallInformation(GetPlayer()->GetPositionZ());
    if (plrMover->pvpInfo.IsHostile)
        plrMover->CastSpell(GetPlayer(), 2479, true);
}

void WorldSession::HandleApplyMovementForceAckOpcode(WorldPacket& recvData)
{
    static MovementStatusElements const extraElement[] =
    {
        MSEExtraFloat,
        MSEExtraInt32,
        MSEExtraInt32,
        MSEExtraFloat
    };

    ExtraMovementStatusElement extras(extraElement);

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData, &extras);

    uint32 Unk1 = extras.GetData().intData[0];
    uint32 Unk2 = extras.GetData().intData[1];

    float Unk3 = extras.GetData().floatData[0];
    float Unk4 = extras.GetData().floatData[1];

    // TODO: implement it
}

void WorldSession::HandleRemoveMovementForceAckOpcode(WorldPacket& recvData)
{
    static MovementStatusElements const extraElement = MSEExtraInt32;
    ExtraMovementStatusElement extras(&extraElement);

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData, &extras);

    uint32 Unk1 = extras.GetData().intData[0];

    // TODO: implement it
}

void WorldSession::HandleMoveTimeSkippedOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    int32 time = 0;
    recvData >> time;

    recvData.ReadGuidMask(guid, 5, 0, 7, 4, 1, 2, 6, 3);
    recvData.ReadGuidBytes(guid, 7, 2, 0, 6, 1, 5, 3, 4);

    //@ToDo: Part of anticheat system!

    /*
        GetPlayer()->ModifyLastMoveTime(-int32(time_skipped));
    */
}

bool WorldSession::CanMovementBeProcessed(uint16 opcode)
{
    switch (opcode)
    {
        case CMSG_FORCE_MOVE_ROOT_ACK:
        case CMSG_MOVE_STOP:
            return true;
    }

    return false;
}

bool WorldSession::HandleMovementInfo(MovementInfo& movementInfo, Unit* mover, Opcodes receivedOpcode)
{
    ASSERT(mover != NULL);

    if (movementInfo.guid != mover->GetGUID() || !movementInfo.pos.IsPositionValid())
        return false;

    Player* plrMover = mover->ToPlayer();

    if (plrMover)
    {
        if (plrMover->IsBeingTeleported())
            return false;

        if (plrMover->IsSplineEnabled() && !CanMovementBeProcessed(receivedOpcode))
            return false;
    }

    uint32 mstime = GameTime::GetGameTimeMS();
    /*----------------------*/
    if (m_clientTimeDelay == 0)
        m_clientTimeDelay = mstime > movementInfo.time ? std::min(mstime - movementInfo.time, (uint32)100) : 0;

    /* process position-change */
    movementInfo.time += m_clientTimeDelay;
    movementInfo.guid = mover->GetGUID();

    HackDetectionTypes hackDetection = movementInfo.SanitizeFlags(mover, this);

    /* handle special cases */
    if (!movementInfo.transport.guid.IsEmpty())
    {
        // We were teleported, skip packets that were broadcast before teleport
        if (movementInfo.pos.GetExactDist2d(mover) > SIZE_OF_GRIDS)
            return false;

        // transports size limited
        // (also received at zeppelin leave by some reason with t_* as absolute in continent coordinates, can be safely skipped)
        if (fabs(movementInfo.transport.pos.GetPositionX()) > 75.0f || fabs(movementInfo.transport.pos.GetPositionY()) > 75.0f || fabs(movementInfo.transport.pos.GetPositionZ()) > 75.0f)
            return false;

        if (!Trinity::IsValidMapCoord(movementInfo.pos.GetPositionX() + movementInfo.transport.pos.GetPositionX(), movementInfo.pos.GetPositionY() + movementInfo.transport.pos.GetPositionY(),
            movementInfo.pos.GetPositionZ() + movementInfo.transport.pos.GetPositionZ(), movementInfo.pos.GetOrientation() + movementInfo.transport.pos.GetOrientation()))
            return false;

        // if we boarded a transport, add us to it
        if (plrMover)
        {
            if (!plrMover->GetTransport())
            {
                if (Transport* transport = plrMover->GetMap()->GetTransport(movementInfo.transport.guid))
                    transport->AddPassenger(plrMover);
            }
            else if (plrMover->GetTransport()->GetGUID() != movementInfo.transport.guid)
            {
                plrMover->GetTransport()->RemovePassenger(plrMover);
                if (Transport* transport = plrMover->GetMap()->GetTransport(movementInfo.transport.guid))
                    transport->AddPassenger(plrMover);
                else
                    movementInfo.transport.Reset();
            }
        }

        if (!mover->GetTransport() && !mover->GetVehicle())
            movementInfo.transport.Reset();
    }
    else if (plrMover && plrMover->GetTransport())                // if we were on a transport, leave
    {
        plrMover->GetTransport()->RemovePassenger(plrMover);
        movementInfo.transport.Reset();
    }

    // Anakin: Movement opcodes should not be proccessed while have ACK* messages in progress
    bool HasHackDetection = hackDetection != HACK_DETECTION_NONE && !mover->GetLastMovementAckIndex();
    if (HasHackDetection)
    {
        movementInfo.pos.Relocate(mover->GetMovementPosition());

        // build and send warning to all gms online
        std::ostringstream messageToSend;

        messageToSend << "Player |cff00ff00" << _player->GetGUID().ToString() << "|r, Name: |cff00ff00" << _player->GetName() << "|r is possibly cheating! ";

        MovementInfo::GetHacksDetected(messageToSend, hackDetection);

        ChatHandler::SendGlobalGMSysMessage(messageToSend.str().c_str());
    }

    mover->SetMovementInfo(&movementInfo);

    WorldPacket data(SMSG_PLAYER_MOVE);
    mover->WriteMovementInfo(data, true);
    if (HasHackDetection)
    {
        mover->SendMessageToSet(&data, true);
        return false;
    }
    else
        mover->SendMessageToSet(&data, _player);

    // Some vehicles allow the passenger to turn by himself
    if (Vehicle* vehicle = mover->GetVehicle())
    {
        if (VehicleSeatEntry const* seat = vehicle->GetSeatForPassenger(mover))
        {
            if (seat->Flags & VEHICLE_SEAT_FLAG_ALLOW_TURNING)
            {
                if (movementInfo.pos.GetOrientation() != mover->GetOrientation())
                {
                    mover->SetOrientation(movementInfo.pos.GetOrientation());
                    mover->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_TURNING);
                }
            }
        }

        return true;
    }

    // previously always mover->UpdatePosition(movementInfo.pos);
    if (!movementInfo.transport.guid.IsEmpty() && mover->GetTransport())
    {
        float x, y, z, o;
        movementInfo.transport.pos.GetPosition(x, y, z, o);
        mover->GetTransport()->CalculatePassengerPosition(x, y, z, &o);
        mover->UpdatePosition(x, y, z, o);
    }
    else
        mover->UpdatePosition(movementInfo.pos);

    // interrupt parachutes upon falling or landing in water
    if (receivedOpcode == CMSG_MOVE_FALL_LAND || receivedOpcode == CMSG_MOVE_START_SWIM)
    {
        // fall damage generation (ignore in flight case that can be triggered also at lags in moment teleportation to another map).
        // moved it here, previously StopMoving function called when player died relocated him to last saved coordinates (which were in air)
        if (receivedOpcode == CMSG_MOVE_FALL_LAND && plrMover && !plrMover->IsInFlight() && !plrMover->GetTransport())
            plrMover->HandleFall(movementInfo.pos);

        mover->SetLastVerticalSpeed(0.0f);

        mover->ClearUnitState(UNIT_STATE_JUMPING);
        mover->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_LANDING);
    }

    if (receivedOpcode == CMSG_MOVE_JUMP)
    {
        mover->SetLastVerticalSpeed(movementInfo.fall.zspeed);

        mover->AddUnitState(UNIT_STATE_JUMPING);
        mover->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_JUMP);

        mover->ProcSkillsAndAuras(nullptr, PROC_FLAG_JUMP, PROC_FLAG_NONE, PROC_SPELL_TYPE_MASK_ALL, PROC_SPELL_PHASE_NONE, PROC_HIT_NONE, nullptr, nullptr, nullptr);
    }

    if (plrMover)                                            // nothing is charmed, or player charmed
    {
        if (plrMover->IsSitState() && (movementInfo.flags & (MOVEMENTFLAG_MASK_MOVING | MOVEMENTFLAG_MASK_TURNING)))
            plrMover->SetStandState(UNIT_STAND_STATE_STAND);

        plrMover->UpdateFallInformationIfNeed(movementInfo, receivedOpcode);

        AreaTableEntry const* zone = sAreaStore.LookupEntry(plrMover->GetAreaId());
        float depth = zone ? zone->MaxDepth : plrMover->GetMap()->GetMinHeight(movementInfo.pos.GetPositionX(), movementInfo.pos.GetPositionY(), plrMover->GetTerrainSwaps());
        if (movementInfo.pos.GetPositionZ() < depth)
        {
            if (!(plrMover->GetBattleground() && plrMover->GetBattleground()->HandlePlayerUnderMap(plrMover)))
            {
                // NOTE: this is actually called many times while falling
                // even after the player has been teleported away
                /// @todo discard movement packets after the player is rooted
                if (plrMover->IsAlive())
                {
                    plrMover->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_IS_OUT_OF_BOUNDS);
                    plrMover->EnvironmentalDamage(DAMAGE_FALL_TO_VOID, GetPlayer()->GetMaxHealth());
                    // player can be alive if GM/etc
                    // change the death state to CORPSE to prevent the death timer from
                    // starting in the next player update
                    if (plrMover->IsAlive())
                        plrMover->KillPlayer();
                }
            }
        }
        else
            plrMover->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_IS_OUT_OF_BOUNDS);
    }

    return true;
}

bool WorldSession::HandleMovementAck(MovementInfo& movementInfo, Unit* mover, uint32 AckIndex, Opcodes dataOpcode /*= SMSG_PLAYER_MOVE*/, ExtraMovementStatusElement* extras /*= NULL*/)
{
    if (mover->GetMovementAckIndexValue(AckIndex) != movementInfo.ackIndex)
        return false;

    if (mover->GetGUID() != movementInfo.guid)
        return false;

    movementInfo.SanitizeFlags(mover);

    mover->SetMovementInfo(&movementInfo);

    // Each ACK* message above MOVEMENT_ACK_DIFFERENT_BROADCAST index send SMSG_PLAYER_MOVE
    // Send only ONE SMSG_PLAYER_MOVE for ALL ack messages of that type
    int32 LastLastMovementAckIndex = mover->GetLastMovementAckIndex();
    if (!LastLastMovementAckIndex || AckIndex <= MOVEMENT_ACK_DIFFERENT_BROADCAST || LastLastMovementAckIndex == AckIndex)
    {
        mover->ResetLastMovementAck();

        WorldPacket data(dataOpcode);
        mover->WriteMovementInfo(data, extras, true);
        mover->SendMessageToSet(&data, _player);
    }

    return true;
}

void WorldSession::HandleKeyboundOverride(WorldPacket& recvData)
{
    uint16 KeyboundID = 0;

    recvData >> KeyboundID;

    SpellKeyboundOverrideEntry const* keybound = sSpellKeyboundOverrideStore.LookupEntry(uint32(KeyboundID));
    if (!keybound)
        return;

    // ToDo: Finish this
    // Connected with SPELL_AURA_KEYBOUND_OVERRIDE
}
