/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CharacterCache.h"
#include "Common.h"
#include "DatabaseEnv.h"
#include "Group.h"
#include "GroupMgr.h"
#include "Log.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Pet.h"
#include "Player.h"
#include "SocialMgr.h"
#include "SpellAuras.h"
#include "Util.h"
#include "Vehicle.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "SpellAuraEffects.h"

void WorldSession::SendGroupResult(PartyOperation operation, const std::string& member, PartyResult res, uint32 val /* = 0 */)
{
    WorldPacket data(SMSG_GROUP_COMMAND_RESULT, 4 + member.length() + 1 + 4 + 4 + 8);

    data << uint32(operation);

    data << member;

    data << uint32(res);
    data << uint32(val);                                // LFD cooldown related (used with ERR_PARTY_LFG_BOOT_COOLDOWN_S and ERR_PARTY_LFG_BOOT_NOT_ELIGIBLE_S)
    data << uint64(0);                                  // player who caused error (in some cases).

    SendPacket(&data);
}

void WorldSession::SendGroupInviteNotification(const std::string& inviterName, uint32 ProposedRoles, bool inGroup)
{
    ObjectGuid invitedGuid = GetPlayer()->GetGUID();
    ObjectGuid accountGUID = ObjectGuid::Create<HighGuid::Account>(GetAccountId());

    std::string realmName = sObjectMgr->GetRealmName(realmID);
    std::string normalizedRealmName = sObjectMgr->GetNormalizedRealmName(realmID);

    bool Unk = false;
    bool AutoDecline = false;
    bool IsCrossRealmInvite = false;
    bool AdjustRealmTransfer = false; // "Accepting this invitation may transfer you to another realm"

    uint32 LfgSlotsSize = 0;
    uint32 LfgCompletedMask = 0;
    uint32 Unk1 = 0;

    WorldPacket data(SMSG_GROUP_INVITE, 1 + 8 + 7 + realmName.size() + normalizedRealmName.size() + 8 + 4 + 4 + 4 + 4 + LfgSlotsSize * (4));

    data.WriteBits(realmName.size(), 8);
    data.WriteBits(normalizedRealmName.size(), 8);

    data.WriteGuidMask(invitedGuid, 2);

    data.WriteBit(Unk);

    data.WriteBits(inviterName.size(), 6);

    data.WriteGuidMask(invitedGuid, 7, 5);

    data.WriteBit(!inGroup);
    data.WriteBit(AutoDecline);

    data.WriteGuidMask(invitedGuid, 1);

    data.WriteBit(IsCrossRealmInvite);
    data.WriteBit(AdjustRealmTransfer);

    data.WriteBits(LfgSlotsSize, 22);

    data.WriteGuidMask(invitedGuid, 3, 0, 4, 6);

    data.FlushBits();

    data.WriteGuidBytes(invitedGuid, 6);

    data.WriteString(realmName);

    data.WriteGuidBytes(invitedGuid, 7, 2, 0);

    data << uint64(accountGUID.GetRawValue());
    data << uint32(realmID);
    data << uint32(ProposedRoles);

    data.WriteGuidBytes(invitedGuid, 1, 5);

    data.WriteString(normalizedRealmName);

    data.WriteGuidBytes(invitedGuid, 1, 5, 4);

    data << uint32(LfgCompletedMask);

    data.WriteString(inviterName);

    data.WriteGuidBytes(invitedGuid, 3);

    data << uint32(Unk1);

    for (uint32 i = 0; i < LfgSlotsSize; ++i)
        data << int32(0);

    SendPacket(&data);
}

void WorldSession::HandleGroupRequestJoinUpdatesOpcode(WorldPacket & recvData)
{
    uint8 partyIndex = 0;
    recvData >> partyIndex;

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    group->SendTargetIconList(this, partyIndex);
    group->SendRaidMarkersUpdate(this, partyIndex);
}

void WorldSession::HandleGroupInviteOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_GROUP_INVITE");

    ObjectGuid targetGuid;

    std::string realmName;
    std::string memberName;

    uint8 realmLen = 0;
    uint8 nameLen = 0;
    uint8 PartyIndex = 0;

    uint32 TargetRealmID = 0;
    uint32 ProposedRoles = 0;

    recvData >> ProposedRoles;
    recvData >> PartyIndex;
    recvData >> TargetRealmID;

    recvData.ReadGuidMask(targetGuid, 7);

    realmLen = recvData.ReadBits(9);

    recvData.ReadGuidMask(targetGuid, 3);

    nameLen = recvData.ReadBits(9);

    recvData.ReadGuidMask(targetGuid, 2, 5, 4, 0, 1, 6);

    recvData.ReadGuidBytes(targetGuid, 7, 6, 0, 4);

    realmName = recvData.ReadString(realmLen);          // unused

    recvData.ReadGuidBytes(targetGuid, 1, 2, 3);

    memberName = recvData.ReadString(nameLen);

    recvData.ReadGuidBytes(targetGuid, 5);

    // cheating
    if (!normalizePlayerName(memberName))
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_BAD_PLAYER_NAME_S);
        return;
    }

    Player* invitingPlayer = GetPlayer();
    Player* invitedPlayer = ObjectAccessor::FindConnectedPlayerByName(memberName);

    // no player
    if (!invitedPlayer)
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_BAD_PLAYER_NAME_S);
        return;
    }

    // player trying to invite himself (most likely cheating)
    if (invitedPlayer == invitingPlayer)
    {
        SendGroupResult(PARTY_OP_INVITE, invitedPlayer->GetName(), ERR_BAD_PLAYER_NAME_S);
        return;
    }

    // restrict invite to GMs
    if (!sWorld->getBoolConfig(CONFIG_ALLOW_GM_GROUP) && !invitingPlayer->IsGameMaster() && invitedPlayer->IsGameMaster())
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_BAD_PLAYER_NAME_S);
        return;
    }

    // can't group with
    if (!invitingPlayer->IsGameMaster() && !sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_GROUP) && invitingPlayer->GetBGTeam() != invitedPlayer->GetBGTeam())
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_PLAYER_WRONG_FACTION);
        return;
    }

    if (invitingPlayer->GetInstanceId() != 0 && invitedPlayer->GetInstanceId() != 0 && invitingPlayer->GetInstanceId() != invitedPlayer->GetInstanceId() && invitingPlayer->GetMapId() == invitedPlayer->GetMapId())
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_TARGET_NOT_IN_INSTANCE_S);
        return;
    }

    // just ignore us
    if (invitedPlayer->GetInstanceId() != 0 && invitedPlayer->GetDungeonDifficultyID() != invitingPlayer->GetDungeonDifficultyID() &&
        invitedPlayer->GetScenarioDifficultyID() != invitingPlayer->GetScenarioDifficultyID())
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_IGNORING_YOU_S);
        return;
    }

    if (invitedPlayer->GetSocial()->HasIgnore(invitingPlayer->GetGUID()))
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_IGNORING_YOU_S);
        return;
    }

    if (!invitedPlayer->GetSocial()->HasFriend(invitingPlayer->GetGUID()) && invitingPlayer->GetLevel() < sWorld->getIntConfig(CONFIG_PARTY_LEVEL_REQ))
    {
        SendGroupResult(PARTY_OP_INVITE, invitedPlayer->GetName(), ERR_INVITE_RESTRICTED);
        return;
    }

    Group* group = invitingPlayer->GetGroup();
    if (group && group->IsBGGroup())
        group = invitingPlayer->GetOriginalGroup();
    if (!group)
        group = invitingPlayer->GetGroupInvite();

    Group* group2 = invitedPlayer->GetGroup();
    if (group2 && group2->IsBGGroup())
        group2 = invitedPlayer->GetOriginalGroup();
    // player already in another group or invited
    if (group2 || invitedPlayer->GetGroupInvite())
    {
        SendGroupResult(PARTY_OP_INVITE, memberName, ERR_ALREADY_IN_GROUP_S);

        if (group2)
        {
            // tell the player that they were invited but it failed as they were already in a group
            invitedPlayer->GetSession()->SendGroupInviteNotification(invitingPlayer->GetName(), ProposedRoles, true);
        }

        return;
    }

    if (group)
    {
        // not have permissions for invite
        if (!group->IsLeader(invitingPlayer->GetGUID()) && !group->IsAssistant(invitingPlayer->GetGUID()) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT))
        {
            if (group->IsCreated())
                SendGroupResult(PARTY_OP_INVITE, "", ERR_NOT_LEADER);
            return;
        }
        // not have place
        if (group->IsFull())
        {
            SendGroupResult(PARTY_OP_INVITE, "", ERR_GROUP_FULL);
            return;
        }
    }

    // ok, but group not exist, start a new group
    // but don't create and save the group to the DB until
    // at least one person joins
    if (!group)
    {
        group = new Group();
        // new group: if can't add then delete
        if (!group->AddLeaderInvite(invitingPlayer))
        {
            delete group;
            return;
        }

        if (!group->AddInvite(invitedPlayer))
        {
            group->RemoveAllInvites();
            delete group;
            return;
        }
    }
    else
    {
        // already existed group: if can't add then just leave
        if (!group->AddInvite(invitedPlayer))
        {
            return;
        }
    }

    // ok, we do it
    invitedPlayer->GetSession()->SendGroupInviteNotification(GetPlayer()->GetName(), ProposedRoles, false);

    SendGroupResult(PARTY_OP_INVITE, memberName, ERR_PARTY_RESULT_OK);
}

void WorldSession::HandleGroupInviteResponseOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_GROUP_INVITE_RESPONSE");

    uint8 partyIndex = 0;
    uint32 rolesDesired = 0;
    bool hasRolesDesired = false;
    bool accept = false;

    recvData >> partyIndex;

    hasRolesDesired = recvData.ReadBit();

    accept = recvData.ReadBit();

    if (hasRolesDesired)
        recvData >> rolesDesired;

    Group* group = GetPlayer()->GetGroupInvite();
    if (!group)
        return;

    if (accept)
    {
        // Remove player from invitees in any case
        group->RemoveInvite(GetPlayer());

        if (group->GetLeaderGUID() == GetPlayer()->GetGUID())
        {
            TC_LOG_ERROR("network", "HandleGroupAcceptOpcode: player %s(%d) tried to accept an invite to his own group", GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().GetCounter());
            return;
        }

        // Group is full
        if (group->IsFull())
        {
            SendGroupResult(PARTY_OP_INVITE, "", ERR_GROUP_FULL);
            return;
        }

        Player* leader = ObjectAccessor::FindPlayer(group->GetLeaderGUID());

        // Forming a new group, create it
        if (!group->IsCreated())
        {
            // This can happen if the leader is zoning. To be removed once delayed actions for zoning are implemented
            if (!leader)
            {
                group->RemoveAllInvites();
                return;
            }

            // If we're about to create a group there really should be a leader present
            ASSERT(leader);
            group->RemoveInvite(leader);
            group->Create(leader);
            sGroupMgr->AddGroup(group);
        }

        // Everything is fine, do it, PLAYER'S GROUP IS SET IN ADDMEMBER!!!
        if (!group->AddMember(GetPlayer()))
            return;

        group->BroadcastGroupUpdate();
    }
    else
    {
        // Remember leader if online (group pointer will be invalid if group gets disbanded)
        Player* leader = ObjectAccessor::FindConnectedPlayer(group->GetLeaderGUID());

        // uninvite, group can be deleted
        GetPlayer()->UninviteFromGroup();

        if (!leader || !leader->GetSession())
            return;

        WorldPacket data(SMSG_GROUP_DECLINE, GetPlayer()->GetName().size() + 1);

        data << GetPlayer()->GetName();

        leader->SendDirectMessage(&data);
    }
}

void WorldSession::HandleGroupUninviteGuidOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 partyIndex = 0;
    uint8 reasonLen = 0;

    std::string reason;

    recvData >> partyIndex;

    recvData.ReadGuidMask(guid, 6, 4, 3, 2, 0, 1, 7, 5);

    reasonLen = recvData.ReadBits(8);

    reason = recvData.ReadString(reasonLen);

    recvData.ReadGuidBytes(guid, 5, 6, 1, 4, 3, 2, 7, 0);

    //can't uninvite yourself
    if (guid == GetPlayer()->GetGUID())
    {
        TC_LOG_ERROR("network", "WorldSession::HandleGroupUninviteGuidOpcode: leader %s(%d) tried to uninvite himself from the group.",
            GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().GetCounter());
        return;
    }

    PartyResult res = GetPlayer()->CanUninviteFromGroup();
    if (res != ERR_PARTY_RESULT_OK)
    {
        SendGroupResult(PARTY_OP_UNINVITE, "", res);
        return;
    }

    Group* grp = GetPlayer()->GetGroup();
    if (!grp)
        return;

    if (grp->IsLeader(guid))
    {
        SendGroupResult(PARTY_OP_UNINVITE, "", ERR_NOT_LEADER);
        return;
    }

    if (grp->IsMember(guid))
    {
        Player::RemoveFromGroup(grp, guid, GROUP_REMOVEMETHOD_KICK, GetPlayer()->GetGUID(), reason.c_str());
        return;
    }

    if (Player* player = grp->GetInvited(guid))
    {
        player->UninviteFromGroup();
        return;
    }

    SendGroupResult(PARTY_OP_UNINVITE, "", ERR_TARGET_NOT_IN_GROUP_S);
}

void WorldSession::HandleGroupSetLeaderOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 partyIndex = 0;

    recvData >> partyIndex;

    recvData.ReadGuidMask(guid, 1, 7, 0, 2, 5, 3, 4, 6);
    recvData.ReadGuidBytes(guid, 1, 5, 7, 6, 0, 2, 4, 3);

    Player* player = ObjectAccessor::FindPlayer(guid);
    Group* group = GetPlayer()->GetGroup();
    if (!group || !player)
        return;

    if (!group->IsLeader(GetPlayer()->GetGUID()) || player->GetGroup() != group)
        return;

    // Everything's fine, accepted.
    group->ChangeLeader(guid, partyIndex);
    group->SendUpdate();
}

void WorldSession::HandleGroupSetRolesOpcode(WorldPacket& recvData)
{
    uint32 newRole = 0;
    uint8 partyIndex = 0;

    ObjectGuid targetGuid;

    recvData >> partyIndex;
    recvData >> newRole;

    recvData.ReadGuidMask(targetGuid, 2, 0, 5, 3, 1, 6, 4, 7);
    recvData.ReadGuidBytes(targetGuid, 1, 3, 2, 7, 4, 0, 6, 5);

    Group* group = GetPlayer()->GetGroup();
    uint8 oldRole = 0;
    if (group && group->IsCreated())
        oldRole = group->GetMemberRole(targetGuid);

    if (oldRole == newRole)
        return;

    ObjectGuid assignerGuid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_GROUP_SET_ROLE, 2 * (1 + 8) + 4 + 1 + 4);

    data.WriteGuidMask(assignerGuid, 1);

    data.WriteGuidMask(targetGuid, 7, 6, 4, 1, 0);

    data.WriteGuidMask(assignerGuid, 0, 7);

    data.WriteGuidMask(targetGuid, 3);

    data.WriteGuidMask(assignerGuid, 6);

    data.WriteGuidMask(targetGuid, 2);

    data.WriteGuidMask(assignerGuid, 4, 5, 2);

    data.WriteGuidMask(targetGuid, 5);

    data.WriteGuidMask(assignerGuid, 3);

    data.WriteGuidBytes(assignerGuid, 1, 6, 2);

    data.WriteGuidBytes(targetGuid, 3);

    data << uint32(oldRole);

    data.WriteGuidBytes(assignerGuid, 7);

    data.WriteGuidBytes(targetGuid, 5);

    data.WriteGuidBytes(assignerGuid, 3);

    data.WriteGuidBytes(targetGuid, 4, 7);

    data.WriteGuidBytes(assignerGuid, 5);

    data.WriteGuidBytes(targetGuid, 6, 2, 1, 0);

    data.WriteGuidBytes(assignerGuid, 4);

    data << uint8(partyIndex);

    data.WriteGuidBytes(assignerGuid, 0);

    data << uint32(newRole);

    if (group)
    {
        group->BroadcastPacket(&data, false);
        group->SetMemberRole(targetGuid, newRole);
    }
    else
        SendPacket(&data);
}

void WorldSession::HandleGroupDisbandOpcode(WorldPacket& /*recvData*/)
{
    Group* grp = GetPlayer()->GetGroup();
    Group* grpInvite = GetPlayer()->GetGroupInvite();
    if (!grp && !grpInvite)
        return;

    if (_player->InBattleground())
    {
        SendGroupResult(PARTY_OP_INVITE, "", ERR_INVITE_RESTRICTED);
        return;
    }

    /** error handling **/
    /********************/

    // everything's fine, do it
    if (grp)
    {
        SendGroupResult(PARTY_OP_LEAVE, GetPlayer()->GetName(), ERR_PARTY_RESULT_OK);
        GetPlayer()->RemoveFromGroup(GROUP_REMOVEMETHOD_LEAVE);
    }
    else if (grpInvite && grpInvite->GetLeaderGUID() == GetPlayer()->GetGUID())
    { // pending group creation being cancelled
        SendGroupResult(PARTY_OP_LEAVE, GetPlayer()->GetName(), ERR_PARTY_RESULT_OK);
        grpInvite->Disband();
    }
}

void WorldSession::HandleLootMethodOpcode(WorldPacket& recvData)
{
    ObjectGuid lootMaster;
    int8 partyIndex = 0;
    uint8 lootMethod = 0;
    uint32 lootThreshold = 0;

    recvData >> partyIndex;
    recvData >> lootMethod;
    recvData >> lootThreshold;

    recvData.ReadGuidMask(lootMaster, 7, 1, 2, 0, 4, 5, 6, 3);
    recvData.ReadGuidBytes(lootMaster, 7, 1, 3, 4, 6, 5, 0, 2);

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    /** error handling **/
    if (!group->IsLeader(GetPlayer()->GetGUID()))
        return;

    if (lootMethod > NEED_BEFORE_GREED)
        return;

    if (lootThreshold < ITEM_QUALITY_UNCOMMON || lootThreshold > ITEM_QUALITY_ARTIFACT)
        return;

    if (lootMethod == MASTER_LOOT && !group->IsMember(lootMaster))
        return;
    /********************/

    // everything's fine, do it
    group->SetLootMethod(LootMethod(lootMethod));
    group->SetMasterLooterGuid(lootMaster);
    group->SetLootThreshold((ItemQualities)lootThreshold);
    group->SendUpdate();
}

void WorldSession::HandleLootRoll(WorldPacket& recvData)
{
    ObjectGuid guid;
    uint8 ItemIndex = 0;
    uint8 RollType = 0;

    recvData >> ItemIndex;
    recvData >> RollType;               // 0: pass, 1: need, 2: greed

    recvData.ReadGuidMask(guid, 7, 1, 2, 0, 6, 3, 4, 5);
    recvData.ReadGuidBytes(guid, 0, 2, 7, 3, 1, 5, 4, 6);

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    group->CountRollVote(GetPlayer()->GetGUID(), guid, ItemIndex, RollType);

    switch (RollType)
    {
        case ROLL_NEED:
            GetPlayer()->UpdateCriteria(CRITERIA_TYPE_ROLL_NEED, 1);
            break;
        case ROLL_GREED:
            GetPlayer()->UpdateCriteria(CRITERIA_TYPE_ROLL_GREED, 1);
            break;
    }
}

void WorldSession::HandleMinimapPingOpcode(WorldPacket& recvData)
{
    if (!GetPlayer()->GetGroup())
        return;

    uint8 partyIndex = 0;

    float x = 0.0f;
    float y = 0.0f;

    recvData >> y;
    recvData >> x;
    recvData >> partyIndex;

    /** error handling **/
    /********************/

    ObjectGuid guid = GetPlayer()->GetGUID();

    // everything's fine, do it
    WorldPacket data(SMSG_MINIMAP_PING, 1 + 8 + 4 + 4);

    data << float(y);
    data << float(x);

    data.WriteGuidMask(guid, 0, 5, 2, 7, 1, 3, 6, 4);

    data.WriteGuidBytes(guid, 6, 5, 7, 2, 0, 3, 1, 4);

    GetPlayer()->GetGroup()->BroadcastPacket(&data, true, -1, GetPlayer()->GetGUID());
}

void WorldSession::HandleRandomRollOpcode(WorldPacket& recvData)
{
    uint8 partyIndex = 0;

    uint32 minimum = 0;
    uint32 maximum = 0;

    recvData >> maximum;
    recvData >> minimum;
    recvData >> partyIndex;

    if (minimum > maximum || maximum > 10000)                // < 32768 for urand call
        return;

    GetPlayer()->DoRandomRoll(minimum, maximum);
}

void WorldSession::HandleRaidTargetUpdateOpcode(WorldPacket& recvData)
{
    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    uint8 Symbol = 0;
    uint8 Index = 0;

    recvData >> Symbol >> Index;

    if (Symbol == 0xFF)
        group->SendTargetIconList(this, Index);
    else
    {
        // target icon update
        if (group->IsRaidGroup() && (!group->IsLeader(GetPlayer()->GetGUID()) && !group->IsAssistant(GetPlayer()->GetGUID()) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT)))
            return;

        ObjectGuid guid;

        recvData.ReadGuidMask(guid, 3, 2, 1, 5, 0, 6, 7, 4);
        recvData.ReadGuidBytes(guid, 2, 3, 0, 7, 5, 1, 6, 4);

        if (guid.IsPlayer())
        {
            Player* target = ObjectAccessor::FindPlayer(guid);
            if (!target || target->IsHostileTo(GetPlayer()))
                return;
        }

        group->SetTargetIcon(Symbol, _player->GetGUID(), guid, Index);
    }
}

void WorldSession::HandleRaidMarkerClear(WorldPacket& recvData)
{
    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    uint8 slotId;

    recvData >> slotId;

    group->ClearRaidMarkers(slotId);
}

void WorldSession::HandleGroupRaidConvertOpcode(WorldPacket& recvData)
{
    bool toRaid = false;

    toRaid = recvData.ReadBit();

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    if (_player->InBattleground())
        return;

    // error handling
    if (!group->IsLeader(GetPlayer()->GetGUID()) || group->GetMembersCount() < 2)
        return;

    // everything's fine, do it (is it 0 (PARTY_OP_INVITE) correct code)
    SendGroupResult(PARTY_OP_INVITE, "", ERR_PARTY_RESULT_OK);

    if (toRaid)
        group->ConvertToRaid();
    else
        group->ConvertToGroup();
}

void WorldSession::HandleGroupChangeSubGroupOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 groupNr = 0;
    uint8 partyIndex = 0;

    recvData >> groupNr;
    recvData >> partyIndex;

    recvData.ReadGuidMask(guid, 1, 4, 6, 3, 7, 2, 0, 5);
    recvData.ReadGuidBytes(guid, 2, 6, 1, 5, 3, 4, 0, 7);

    // we will get correct pointer for group here, so we don't have to check if group is BG raid
    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    if (guid.IsEmpty())
        return;

    if (groupNr >= MAX_RAID_SUBGROUPS)
        return;

    ObjectGuid senderGuid = GetPlayer()->GetGUID();

    if (!group->IsLeader(senderGuid) && !group->IsAssistant(senderGuid) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT))
        return;

    if (!group->HasFreeSlotSubGroup(groupNr))
        return;

    group->ChangeMembersGroup(guid, groupNr);
}

void WorldSession::HandleGroupSwapSubGroupOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_GROUP_SWAP_SUB_GROUP");

    ObjectGuid guid1;
    ObjectGuid guid2;

    uint8 partyIndex = 0;

    recvData >> partyIndex;

    recvData.ReadGuidMask(guid1, 5);

    recvData.ReadGuidMask(guid2, 1);

    recvData.ReadGuidMask(guid1, 0, 2, 7, 1);

    recvData.ReadGuidMask(guid2, 7, 2, 5);

    recvData.ReadGuidMask(guid1, 6);

    recvData.ReadGuidMask(guid2, 6, 0);

    recvData.ReadGuidMask(guid1, 4, 3);

    recvData.ReadGuidMask(guid2, 4, 3);

    recvData.ReadGuidBytes(guid1, 3, 7, 5);

    recvData.ReadGuidBytes(guid2, 0, 5, 4);

    recvData.ReadGuidBytes(guid1, 6, 1, 0, 2, 4);

    recvData.ReadGuidBytes(guid2, 6, 1, 2, 7, 3);

    if (guid1.IsEmpty() && guid2.IsEmpty())
        return;

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    uint8 groupNr1 = group->GetMemberGroup(guid2);
    uint8 groupNr2 = group->GetMemberGroup(guid1);

    if (groupNr1 >= MAX_RAID_SUBGROUPS || groupNr2 >= MAX_RAID_SUBGROUPS)
        return;

    ObjectGuid senderGuid = GetPlayer()->GetGUID();

    if (!group->IsLeader(senderGuid) && !group->IsAssistant(senderGuid) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT))
        return;

    group->ChangeMembersGroup(guid1, groupNr1);
    group->ChangeMembersGroup(guid2, groupNr2);
}

void WorldSession::HandleGroupAssistantLeaderOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    bool apply = false;

    uint8 partyIndex = 0;

    recvData >> partyIndex;

    recvData.ReadGuidMask(guid, 2, 0, 6, 3, 1);

    apply = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 4, 7, 5);

    recvData.ReadGuidBytes(guid, 5, 1, 0, 7, 3, 6, 2, 4);

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    if (!group->IsLeader(GetPlayer()->GetGUID()))
        return;

    group->SetGroupMemberFlag(guid, apply, MEMBER_FLAG_ASSISTANT);
}

void WorldSession::HandleSetEveryoneAssistantOpcode(WorldPacket& recvData)
{
    bool apply = false;

    uint8 partyIndex = 0;

    recvData >> partyIndex;

    apply = recvData.ReadBit();

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    if (!group->IsLeader(GetPlayer()->GetGUID()))
        return;

    group->ChangeFlagEveryoneAssistant(apply);
}

void WorldSession::HandleGroupAssignmentOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 assignment = 0;
    uint8 partyIndex = 0;

    bool apply = false;

    recvData >> assignment;

    apply = recvData.ReadBit();

    recvData >> partyIndex;

    recvData.ReadGuidMask(guid, 0, 4, 2, 1, 3, 6, 5, 7);
    recvData.ReadGuidBytes(guid, 5, 4, 7, 6, 3, 0, 1, 2);

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    ObjectGuid senderGuid = GetPlayer()->GetGUID();

    if (!group->IsLeader(senderGuid) && !group->IsAssistant(senderGuid) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT))
        return;

    GroupMemberFlags flag;
    switch (assignment)
    {
        case GROUP_ASSIGN_MAINASSIST:
            flag = MEMBER_FLAG_MAINASSIST;
            break;
        case GROUP_ASSIGN_MAINTANK:
            flag = MEMBER_FLAG_MAINTANK;
            break;
        default:
            break;
    }

    if (guid.IsEmpty())
    {
        guid = group->GetMemberByUniqueGroupMemberFlag(flag);
        apply = false;
    }

    group->SetGroupMemberFlag(guid, apply, flag);
}

void WorldSession::HandleRaidReadyCheckOpcode(WorldPacket& recvData)
{
    uint8 partyIndex = 0;

    recvData >> partyIndex;

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    ObjectGuid playerGuid = GetPlayer()->GetGUID();

    /** error handling **/
    if (!group->IsLeader(playerGuid) && !group->IsAssistant(playerGuid) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT))
        return;

    // check is also done client side
    if (group->ReadyCheckInProgress())
        return;
    /********************/

    uint32 readyCheckDuration = 35000;

    ObjectGuid groupGuid = group->GetGUID();

    // everything's fine, do it
    WorldPacket data(SMSG_RAID_READY_CHECK, 2 * (1 + 8) + 4 + 1);

    data.WriteGuidMask(groupGuid, 4, 2);

    data.WriteGuidMask(playerGuid, 4);

    data.WriteGuidMask(groupGuid, 3, 7, 1, 0);

    data.WriteGuidMask(playerGuid, 6, 5);

    data.WriteGuidMask(groupGuid, 6, 5);

    data.WriteGuidMask(playerGuid, 0, 1, 2, 7, 3);

    data << uint32(readyCheckDuration);

    data.WriteGuidBytes(groupGuid, 2, 7, 3);

    data.WriteGuidBytes(playerGuid, 4);

    data.WriteGuidBytes(groupGuid, 1, 0);

    data.WriteGuidBytes(playerGuid, 1, 2, 6, 5);

    data.WriteGuidBytes(groupGuid, 6);

    data.WriteGuidBytes(playerGuid, 0);

    data << uint8(partyIndex);

    data.WriteGuidBytes(playerGuid, 7);

    data.WriteGuidBytes(groupGuid, 4);

    data.WriteGuidBytes(playerGuid, 3);

    data.WriteGuidBytes(groupGuid, 5);

    group->BroadcastPacket(&data, false);

    group->ReadyCheck(true);
    group->ReadyCheckMemberHasResponded(playerGuid);
    group->OfflineReadyCheck();

    // leader keeps track of ready check timer
    GetPlayer()->SetReadyCheckTimer(readyCheckDuration);
}

void WorldSession::HandleRaidReadyCheckConfirmOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    bool status = false;

    uint8 partyIndex = 0;

    recvData >> partyIndex;

    recvData.ReadGuidMask(guid, 2, 1, 0, 3, 6);

    status = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 7, 4, 5);

    recvData.ReadGuidBytes(guid, 1, 0, 3, 2, 4, 5, 7, 6);

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    if (!group->ReadyCheckInProgress())
        return;

    ObjectGuid groupGuid = group->GetGUID();
    ObjectGuid playerGuid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_RAID_READY_CHECK_CONFIRM, 2 * (1 + 8) + 1);

    data.WriteGuidMask(groupGuid, 4);

    data.WriteGuidMask(playerGuid, 5, 3);

    data.WriteBit(status);

    data.WriteGuidMask(groupGuid, 2);

    data.WriteGuidMask(playerGuid, 6);

    data.WriteGuidMask(groupGuid, 3);

    data.WriteGuidMask(playerGuid, 0, 1);

    data.WriteGuidMask(groupGuid, 1, 5);

    data.WriteGuidMask(playerGuid, 7, 4);

    data.WriteGuidMask(groupGuid, 6);

    data.WriteGuidMask(playerGuid, 2);

    data.WriteGuidMask(groupGuid, 0, 7);

    data.FlushBits();

    data.WriteGuidBytes(playerGuid, 4, 2, 1);

    data.WriteGuidBytes(groupGuid, 4, 2);

    data.WriteGuidBytes(playerGuid, 0);

    data.WriteGuidBytes(groupGuid, 5, 3);

    data.WriteGuidBytes(playerGuid, 7);

    data.WriteGuidBytes(groupGuid, 6, 1);

    data.WriteGuidBytes(playerGuid, 6, 3, 5);

    data.WriteGuidBytes(groupGuid, 0, 7);

    group->BroadcastPacket(&data, false);
    group->ReadyCheckMemberHasResponded(playerGuid);

    if (group->ReadyCheckAllResponded())
    {
        Player* leader = ObjectAccessor::FindPlayer(group->GetLeaderGUID());
        if (leader)
            leader->SetReadyCheckTimer(0);

        group->ReadyCheck(false);
        group->ReadyCheckResetResponded();
        group->SendReadyCheckCompleted();
    }
}

void WorldSession::BuildPartyMemberStatsChangedPacket(Player* player, WorldPacket* data)
{
    uint32 mask = player->GetGroupUpdateFlag();
    if (mask == GROUP_UPDATE_FLAG_NONE)
        return;

    bool IsFullUpdate = (mask & GROUP_UPDATE_FULL) == GROUP_UPDATE_FULL;

    ObjectGuid guid = player->GetGUID();

    std::set<uint32> const& phases = player->GetPhases();

    // always add status flag
    mask |= GROUP_UPDATE_FLAG_STATUS;

    if (mask & GROUP_UPDATE_FLAG_POWER_TYPE)                // if update power type, update current/max power also
        mask |= (GROUP_UPDATE_FLAG_CUR_POWER | GROUP_UPDATE_FLAG_MAX_POWER);

    if (mask & GROUP_UPDATE_FLAG_PET_POWER_TYPE)            // same for pets
        mask |= (GROUP_UPDATE_FLAG_PET_CUR_POWER | GROUP_UPDATE_FLAG_PET_MAX_POWER);

    Pet* pet = player->GetPet();
    if (!pet)
        mask &= ~GROUP_UPDATE_PET;

    bool IsForEnemy = false;

    Unit::VisibleAuraContainer const& playerAppList = player->GetVisibleAuras();
    Unit::VisibleAuraContainer const* petAppList = pet ? &pet->GetVisibleAuras() : nullptr;

    ByteBuffer buffer;

    data->Initialize(SMSG_GROUP_MEMBER_STATS, 1 + 8 + 1 + 4 +
                    (((mask & GROUP_UPDATE_FLAG_STATUS) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_UNK_2) != 0) ? 2 * (1) : 0) +
                    (((mask & GROUP_UPDATE_FLAG_CUR_HP) != 0) ? 4 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_MAX_HP) != 0) ? 4 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_POWER_TYPE) != 0) ? 1 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_UNK6) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_MAX_POWER) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_CUR_POWER) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_LEVEL) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_ZONE) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_UNK11) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_POSITION) != 0) ? 3 * (2) : 0) +
                    (((mask & GROUP_UPDATE_FLAG_AURAS) != 0) ? (1 + 8 + 4 + playerAppList.size() * (4 + 1 + 4 + MAX_SPELL_EFFECTS * (4))) : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_GUID) != 0) ? 8 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_NAME) != 0) ? pet->GetName().size() : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_MODEL_ID) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_CUR_HP) != 0) ? 4 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_MAX_HP) != 0) ? 4 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_POWER_TYPE) != 0) ? 1 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_UNK_20) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_CUR_POWER) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_MAX_POWER) != 0) ? 2 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PET_AURAS) != 0) ? (1 + 8 + 4 + petAppList ? (petAppList->size() * (4 + 1 + 4 + MAX_SPELL_EFFECTS * (4))) : 0) : 0) +
                    (((mask & GROUP_UPDATE_FLAG_VEHICLE_SEAT) != 0) ? 4 : 0) +
                    (((mask & GROUP_UPDATE_FLAG_PHASE) != 0) ? (3 + phases.size() * (2)) : 0));

    data->WriteGuidMask(guid, 0, 5);

    data->WriteBit(IsFullUpdate);

    data->WriteGuidMask(guid, 1, 4);

    data->WriteBit(IsForEnemy);

    data->WriteGuidMask(guid, 6, 2, 7, 3);

    data->FlushBits();

    data->WriteGuidBytes(guid, 3, 2, 6, 7, 5);

    *data << uint32(mask);

    data->WriteGuidBytes(guid, 1, 4, 0);

    if ((mask & GROUP_UPDATE_FLAG_STATUS) != 0)
    {
        uint16 playerStatus = MEMBER_STATUS_ONLINE;
        if (player->IsPvP())
            playerStatus |= MEMBER_STATUS_PVP;

        if (!player->IsAlive())
        {
            if (player->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
                playerStatus |= MEMBER_STATUS_GHOST;
            else
                playerStatus |= MEMBER_STATUS_DEAD;
        }

        if (player->IsFFAPvP())
            playerStatus |= MEMBER_STATUS_PVP_FFA;

        if (player->IsAFK())
            playerStatus |= MEMBER_STATUS_AFK;

        if (player->IsDND())
            playerStatus |= MEMBER_STATUS_DND;

        buffer << uint16(playerStatus);
    }

    // unk
    /*if ((mask & GROUP_UPDATE_FLAG_UNK_2) != 0)
    {
        buffer << uint8(0);
        buffer << uint8(0);
    }*/

    if ((mask & GROUP_UPDATE_FLAG_CUR_HP) != 0)
        buffer << uint32(player->GetHealth());

    if ((mask & GROUP_UPDATE_FLAG_MAX_HP) != 0)
        buffer << uint32(player->GetMaxHealth());

    Powers powerType = player->GetPowerType();
    if ((mask & GROUP_UPDATE_FLAG_POWER_TYPE) != 0)
        buffer << uint8(powerType);

    //if ((mask & GROUP_UPDATE_FLAG_UNK6) != 0)
        //buffer << uint16(0);

    if ((mask & GROUP_UPDATE_FLAG_MAX_POWER) != 0)
        buffer << uint16(player->GetMaxPower(powerType));

    if ((mask & GROUP_UPDATE_FLAG_CUR_POWER) != 0)
        buffer << uint16(player->GetPower(powerType));

    if ((mask & GROUP_UPDATE_FLAG_LEVEL) != 0)
        buffer << uint16(player->GetLevel());

    if ((mask & GROUP_UPDATE_FLAG_ZONE) != 0)
        buffer << uint16(player->GetZoneId());

    //if ((mask & GROUP_UPDATE_FLAG_UNK11) != 0)
        //buffer << uint16(0);

    if ((mask & GROUP_UPDATE_FLAG_POSITION) != 0)
    {
        buffer << uint16(player->GetPositionX());
        buffer << uint16(player->GetPositionY());
        buffer << uint16(player->GetPositionZ());
    }

    if ((mask & GROUP_UPDATE_FLAG_AURAS) != 0)
    {
        uint64 playerAuraMask = player->GetAuraUpdateMaskForRaid();

        buffer << uint8(0);
        buffer << uint64(playerAuraMask);
        buffer << uint32(playerAppList.size());

        for (AuraApplication const* aurApp : playerAppList)
        {
            Aura const* base = aurApp->GetBase();

            uint8 slot = aurApp->GetSlot();
            if (slot >= MAX_AURAS_PER_GROUP_UPDATE)
                continue;

            if ((playerAuraMask & (uint64(1) << slot)) == 0)
                continue;

            buffer << uint32(base->GetId());
            buffer << uint8(aurApp->GetFlags());
            buffer << uint32(aurApp->GetEffectMask());

            if (aurApp->GetFlags() & AFLAG_SCALABLE)
            {
                std::vector<float> effectAmounts;
                for (AuraEffect const* aurEff : base->GetAuraEffects())
                    if (aurEff)
                        effectAmounts.push_back(aurEff->GetAmount());

                buffer << uint8(effectAmounts.size());

                for (float Amount : effectAmounts)
                    buffer << float(Amount);
            }
        }
    }

    if ((mask & GROUP_UPDATE_FLAG_PET_GUID) != 0)
        buffer << uint64(pet->GetGUID().GetRawValue());

    if ((mask & GROUP_UPDATE_FLAG_PET_NAME) != 0)
        buffer << pet->GetName();

    if ((mask & GROUP_UPDATE_FLAG_PET_MODEL_ID) != 0)
        buffer << uint16(pet->GetDisplayId());

    if ((mask & GROUP_UPDATE_FLAG_PET_CUR_HP) != 0)
        buffer << uint32(pet->GetHealth());

    if ((mask & GROUP_UPDATE_FLAG_PET_MAX_HP) != 0)
        buffer << uint32(pet->GetMaxHealth());

    if ((mask & GROUP_UPDATE_FLAG_PET_POWER_TYPE) != 0)
        buffer << uint8(pet->GetPowerType());

    //if ((mask & GROUP_UPDATE_FLAG_UNK_20) != 0)
        //buffer << uint16(0);

    if ((mask & GROUP_UPDATE_FLAG_PET_CUR_POWER) != 0)
        buffer << uint16(pet->GetPower(pet->GetPowerType()));

    if ((mask & GROUP_UPDATE_FLAG_PET_MAX_POWER) != 0)
        buffer << uint16(pet->GetMaxPower(pet->GetPowerType()));

    if ((mask & GROUP_UPDATE_FLAG_PET_AURAS) != 0)
    {
        uint64 petAuraMask = pet->GetAuraUpdateMaskForRaid();

        buffer << uint8(0);
        buffer << uint64(petAuraMask);
        buffer << uint32(petAppList->size());

        for (AuraApplication const* aurApp : *petAppList)
        {
            Aura const* base = aurApp->GetBase();

            uint8 slot = aurApp->GetSlot();
            if (slot >= MAX_AURAS_PER_GROUP_UPDATE)
                continue;

            if ((petAuraMask & (uint64(1) << slot)) == 0)
                continue;

            buffer << uint32(base->GetId());
            buffer << uint8(aurApp->GetFlags());
            buffer << uint32(aurApp->GetEffectMask());

            if (aurApp->GetFlags() & AFLAG_SCALABLE)
            {
                std::vector<float> effectAmounts;
                for (AuraEffect const* aurEff : base->GetAuraEffects())
                    if (aurEff)
                        effectAmounts.push_back(aurEff->GetAmount());

                buffer << uint8(effectAmounts.size());

                for (float Amount : effectAmounts)
                    buffer << float(Amount);
            }
        }
    }

    if ((mask & GROUP_UPDATE_FLAG_VEHICLE_SEAT) != 0)
    {
        if (Vehicle* veh = player->GetVehicle())
            buffer << uint32(player->GetTransportSeatID());
        else
            buffer << uint32(0);
    }

    if ((mask & GROUP_UPDATE_FLAG_PHASE) != 0)
    {
        buffer << uint32(0x08 | (!phases.empty() ? 0x10 : 0));

        buffer.WriteBits(phases.size(), 23);

        buffer.FlushBits();

        for (uint32 phaseID : phases)
            buffer << uint16(phaseID);
    }

    //if ((mask & GROUP_UPDATE_FLAG_UNK_25) != 0)
        //buffer << uint16(0);

    //if ((mask & GROUP_UPDATE_FLAG_UNK_26) != 0)
       // buffer << uint32(0);

    uint32 bufferSize = buffer.size();
    *data << uint32(bufferSize);

    data->append(buffer);
}

void WorldSession::HandleRequestPartyMemberStatsOpcode(WorldPacket& recvData)
{
    uint8 PartyIndex = 0;
    recvData >> PartyIndex;

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 4, 0, 1, 3, 6, 2, 5);
    recvData.ReadGuidBytes(guid, 3, 6, 5, 2, 1, 4, 0, 7);

    Player* player = ObjectAccessor::FindConnectedPlayer(guid);
    if (!player)
    {
        WorldPacket data(SMSG_GROUP_MEMBER_STATS, 1 + 8 + 1 + 4 + 4 + 2 * (1));

        data.WriteGuidMask(guid, 0, 5);

        data.WriteBit(false);

        data.WriteGuidMask(guid, 1, 4);

        data.WriteBit(false);

        data.WriteGuidMask(guid, 6, 2, 7, 3);

        data.WriteGuidBytes(guid, 3, 2, 6, 7, 5);

        data << uint32(GROUP_UPDATE_FLAG_STATUS);

        data.WriteGuidBytes(guid, 1, 4, 0);

        data << uint32(2);

        for (uint32 i = 0; i < 2; ++i)
            data << uint8(MEMBER_STATUS_OFFLINE);

        SendPacket(&data);

        return;
    }

    Pet* pet = player->GetPet();

    // full stats supdate
    player->SetGroupUpdateFlag(GROUP_UPDATE_FULL);

    for (uint32 i = 0; i < MAX_AURAS_PER_GROUP_UPDATE; ++i)
    {
        player->SetAuraUpdateMaskForRaid(i);
        if (pet)
            pet->SetAuraUpdateMaskForRaid(i);
    }

    WorldPacket data;
    player->GetSession()->BuildPartyMemberStatsChangedPacket(player, &data);
    SendPacket(&data);

    // reset update flags
    player->SetGroupUpdateFlag(GROUP_UPDATE_FLAG_NONE);
    player->ResetAuraUpdateMaskForRaid();
    if (pet)
        pet->ResetAuraUpdateMaskForRaid();
}

void WorldSession::HandleRequestRaidInfoOpcode(WorldPacket& /*recvData*/)
{
    // every time the player checks the character screen
    _player->SendRaidInfo();
}

void WorldSession::HandleOptOutOfLootOpcode(WorldPacket& recvData)
{
    bool passOnLoot = false;
    passOnLoot = recvData.ReadBit(); // 1 always pass, 0 do not pass

    // ignore if player not loaded
    if (!GetPlayer())                                        // needed because STATUS_AUTHED
    {
        if (passOnLoot)
            TC_LOG_DEBUG("network", "CMSG_OPT_OUT_OF_LOOT value<>0 for not-loaded character!");
        return;
    }

    GetPlayer()->SetPassOnGroupLoot(passOnLoot);
}

void WorldSession::HandleGroupInitiatePollRole(WorldPacket& recvData)
{
    uint8 Index = 0;
    recvData >> Index;

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    if (!group->IsLeader(GetPlayer()->GetGUID()) && !group->IsAssistant(GetPlayer()->GetGUID()) && !(group->GetGroupType() & GROUPTYPE_EVERYONE_ASSISTANT))
        return;

    SendRolePollInform(Index);
}

void WorldSession::SendRolePollInform(uint8 Index)
{
    ObjectGuid guid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_GROUP_ROLE_POLL_INFORM, 1 + 8 + 1);

    data.WriteGuidMask(guid, 5, 7, 3, 1, 2, 0, 4, 6);

    data.WriteGuidBytes(guid, 7);

    data << uint8(Index);

    data.WriteGuidBytes(guid, 6, 5, 0, 1, 4, 2, 3);

    GetPlayer()->GetGroup()->BroadcastPacket(&data, false, -1);
}
