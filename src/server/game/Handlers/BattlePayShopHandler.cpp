/*
 * Copyright (C) 2015-2017 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2013-2014 TimelessCore <http://timeless.sk/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePayShopMgr.h"
#include "BattlePayShop.h"
#include "WorldSession.h"
#include "ObjectMgr.h"
#include "DBCStores.h"
#include "Player.h"
#include "RecruitAFriendRewardsStore.h"
#include "ReferAFriend.h"
#include "CharacterCache.h"

void WorldSession::HandleBattlePayShopGetProductList(WorldPacket& /*recvData*/)
{
    SendBattlePayShopProductList();
}

void WorldSession::SendBattlePayShopProductList()
{
    if (!sBattlePayShopMgr->IsStoreEnabled())
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PAY_SHOP_GET_PRODUCT_LIST - Account %u is trying to get access to battle pay shop, but it is closed now!", GetAccountId());
        return;
    }

    bool IsOnCharSelectScreen = _player == nullptr;

    BattlePayShopProductMap productsStore;
    sBattlePayShopMgr->GetProductsStore(productsStore, IsOnCharSelectScreen);

    BattlePayShopGroupMap groupsStore;
    sBattlePayShopMgr->GetGroupsStore(groupsStore, IsOnCharSelectScreen);

    BattlePayShopEntryMap entriesStore;
    sBattlePayShopMgr->GetEntriesStore(entriesStore, IsOnCharSelectScreen);

    uint32 TitleDescriptionSize = 0;
    uint32 ProductDescriptionSize = 0;
    for (auto& itr : productsStore)
    {
        TitleDescriptionSize += (itr.second.Title.length() * 2);
        ProductDescriptionSize += itr.second.Description.length();
    }

    uint32 GroupNameSize = 0;
    for (auto& itr : groupsStore)
        GroupNameSize += itr.second.Name.length();

    uint32 result = BATTLE_PAY_SHOP_RESULT_OK;

    ByteBuffer groupData;
    ByteBuffer shopData;
    ByteBuffer productData;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_GET_PRODUCT_LIST, 8 + 4 + 4 + productsStore.size() * (7 + 1 + 4 + 4 + 4 + 4 + 4 + 4 + 4) +
                    entriesStore.size() * (1 + 4 + 4 + 4 + 4 + 4 + 1) + groupsStore.size() * (1 + 4 + 1 + 4 + 4) +
                    TitleDescriptionSize + ProductDescriptionSize + GroupNameSize);

    data.WriteBits(entriesStore.size(), 19);
    data.WriteBits(productsStore.size(), 19);

    for (auto& itr : productsStore)
    {
        BattlePayShopProduct const& product = itr.second;

        uint64 normalPrice = product.NormalPrice;
        uint64 currentProductPrice = product.CurrentPrice;

        uint32 productFlags = BATTLE_PAY_SHOP_PRODUCT_FLAG_SHOW_PRODUCT;

        bool IsAvailable = sBattlePayShopMgr->IsProductUnavailableToPlayer(this, product);
        bool ShowCurrencyInsteadOfPrices = (!normalPrice && !currentProductPrice);
        bool IsLoginProduct = product.Id == BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX;

        if (!IsAvailable || ShowCurrencyInsteadOfPrices || IsLoginProduct)
            productFlags |= BATTLE_PAY_SHOP_PRODUCT_FLAG_UNAVAILABLE;
        else
            productFlags |= BATTLE_PAY_SHOP_PRODUCT_FLAG_AVAILABLE;

        // show currency quantity instead
        if (ShowCurrencyInsteadOfPrices)
        {
            currentProductPrice = _battlePayShop->GetCurrencyQuantity();
            normalPrice = _battlePayShop->GetCurrencyQuantity();
        }

        bool HasBattlePayShopDisplayInfo = true; // // always true
        bool HasBattlePayShopDisplayItemInfo = false; // entry overide display data currently ignored
        bool HasBattlePetResult = false;
        bool HasUnk = false;

        bool HasAlreadyProduct = _battlePayShop->HasOwnedProduct(product.Id);

        data.WriteBits(product.ChoiceType, 2);

        data.WriteBits(1, 20); // one item per product

        data.WriteBit(HasUnk);
        data.WriteBit(HasAlreadyProduct);
        data.WriteBit(HasBattlePayShopDisplayItemInfo);

        ByteBuffer productDisplayItemData;
        if (HasBattlePayShopDisplayItemInfo)
        {
            data.WriteBits(0, 10); // productDisplayItem->Title.length()
            data.WriteBits(0, 13); // productDisplayItem->Description.length()
            data.WriteBit(0); // HasProductDisplayItemFileDataID
            data.WriteBit(0); // HasProductDisplayItemCreatureDisplayInfoID;
            data.WriteBit(0); // HasProductDisplayItemUnk
            data.WriteBit(0); // HasProductDisplayItemFlags
            data.WriteBits(0, 10); // productDisplayItem->Title.length()

            //if (HasProductDisplayItemCreatureDisplayInfoID)
                //productDisplayItemData << uint32(ProductDisplayItemCreatureDisplayInfoID);

            //if (HasProductDisplayItemUnk)
                //productDisplayItemData << uint32(ProductDisplayItemUnk);

            //if (HasProductDisplayItemFlags)
                //productDisplayItemData << uint32(ProductDisplayItemFlags);

            //productDisplayItemData.WriteString("productDisplayItem->Title");

            //if (HasProductDisplayItemFileDataID)
                //productDisplayItemData << uint32(ProductDisplayItemFileDataID);

            //productDisplayItemData.WriteString("productDisplayItem->Title");

            //productDisplayItemData.WriteString("productDisplayItem->Description");
        }

        data.WriteBit(HasBattlePetResult);

        if (HasBattlePetResult)
            data.WriteBits(0, 4); // PetResult

        data.WriteBit(HasBattlePayShopDisplayInfo);

        productData << uint8(BATTLE_PAY_SHOP_PRODUCT_TYPE_DEFAULT);

        if (!productDisplayItemData.empty())
            productData.append(productDisplayItemData);

        productData << uint32(product.ItemId);
        productData << uint32(product.Quantity);
        productData << uint32(product.Id);

        if (HasBattlePayShopDisplayInfo)
        {
            uint32 CreatureDisplayID = product.CreatureDisplayId;
            uint32 FileDataID = product.FileDataId;
            uint32 DisplayFlags = 0;
            uint32 Unk = 0;

            bool HasCreatureDisplayInfoID = CreatureDisplayID != 0;
            bool HasDisplayFlags = DisplayFlags != 0;
            bool HasFileDataID = FileDataID != 0;
            bool HasUnk = Unk != 0;

            data.WriteBit(HasCreatureDisplayInfoID);

            data.WriteBits(product.Title.length(), 10);
            data.WriteBits(product.Description.length(), 13);
            data.WriteBits(0, 10);

            data.WriteBit(HasUnk);
            data.WriteBit(HasDisplayFlags);
            data.WriteBit(HasFileDataID);

            if (HasCreatureDisplayInfoID)
                productData << uint32(CreatureDisplayID);

            if (HasFileDataID)
                productData << uint32(FileDataID);

            //productData.WriteString("");

            productData.WriteString(product.Title);

            if (HasDisplayFlags)
                productData << uint32(DisplayFlags);

            if (HasUnk)
                productData << uint32(Unk);

            productData.WriteString(product.Description);
        }

        productData << uint32(product.Id);
        productData << uint32(productFlags);
        productData << uint64(normalPrice);
        productData << uint64(currentProductPrice);
    }

    data.WriteBits(groupsStore.size(), 20);

    for (auto& itr : entriesStore)
    {
        BattlePayShopEntry const& entry = itr.second;

        // entry overide display data currently ignored
        bool HasBattlePayShopDisplayInfo = false;

        data.WriteBit(HasBattlePayShopDisplayInfo);

        if (HasBattlePayShopDisplayInfo)
        {
            data.WriteBits(0, 10); // productDisplay->Title.length()
            data.WriteBit(0); // HasProductDisplayFlags;
            data.WriteBits(0, 10); // productDisplay->Title.length()
            data.WriteBits(0, 13); // productDisplay->Description.length()
            data.WriteBit(0); // HasProductDisplayCreatureDisplayInfoID
            data.WriteBit(0); // HasProductDisplayUnk;
            data.WriteBit(0); // HasProductDisplayFileDataID

            //if (HasProductDisplayFlags)
                //shopData << uint32(ProductDisplayFlags);

            //shopData.WriteString(productDisplay->Title);

            //if (HasProductDisplayFileDataID)
                //shopData << uint32(HasProductDisplayFileDataID);

            //if (HasProductDisplayUnk)
                //shopData << uint32(ProductDisplayUnk);

            //if (HasProductDisplayCreatureDisplayInfoID)
                //shopData << uint32(ProductDisplayCreatureDisplayInfoID);

            //shopData.WriteString(productDisplay->Description);

            //shopData.WriteString(productDisplay->Title);
        }

        shopData << uint32(entry.Flags);
        shopData << uint8(entry.Banner);
        shopData << uint32(entry.Id);
        shopData << uint32(entry.Order);
        shopData << uint32(entry.GroupId);
        shopData << uint32(entry.ProductId);
    }

    for (auto& itr : groupsStore)
    {
        BattlePayShopGroup const& group = itr.second;

        data.WriteBits(group.Name.length(), 8);

        groupData << uint32(group.Order);

        groupData.WriteString(group.Name);

        groupData << uint8(group.Type);
        groupData << uint32(group.FileDataId);
        groupData << uint32(group.Id);
    }

    data.append(groupData);
    data.append(productData);
    data.append(shopData);

    data << uint32(sBattlePayShopMgr->GetStoreCurrency());
    data << uint32(result);

    SendPacket(&data);
}

void WorldSession::HandleBattlePayShopStartPurchase(WorldPacket& recvData)
{
    ObjectGuid targetGUID;
    uint32 ProductID = 0;
    uint32 ClientToken = 0;

    recvData >> ProductID;
    recvData >> ClientToken;

    recvData.ReadGuidMask(targetGUID, 2, 3, 5, 1, 4, 7, 0, 6);
    recvData.ReadGuidBytes(targetGUID, 5, 3, 7, 1, 4, 0, 6, 2);

    if (_player->GetGUID() != targetGUID)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PAY_SHOP_START_PURCHASE - Player (%s, account: %u) is trying to start purchasing, but player's guid does not match!",
            targetGUID.ToString().c_str(), GetAccountId());

        SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_OTHER, ClientToken, 0);
        return;
    }

    BattlePayShopProduct const* product = sBattlePayShopMgr->GetProduct(ProductID);
    if (!product)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PAY_SHOP_START_PURCHASE - Player (%s, account: %u) is trying to start purchasing, but product provided by id %u does not exist",
            targetGUID.ToString().c_str(), GetAccountId(), ProductID);

        SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_OTHER, ClientToken, 0);
        return;
    }

    uint64 PurchaseID = sBattlePayShopMgr->GeneratePurchaseID();

    uint32 Result = BATTLE_PAY_SHOP_RESULT_OK;

    _battlePayShop->AddPurchasedProduct(ClientToken, PurchaseID, product);

    TC_LOG_TRACE("battlepayshop.shop", "StartPurchase: Player (%s, account: %u) confirmed purchase (id: " UI64FMTD ") for product id %u and token %u",
        _player->GetGUID().ToString().c_str(), GetAccountId(), PurchaseID, ProductID, ClientToken);

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_START_PURCHASE, 8 + 4 + 4);

    data << uint64(PurchaseID);
    data << uint32(Result);
    data << uint32(ClientToken);

    SendPacket(&data);

    uint32 purchaseStatus = BATTLE_PAY_SHOP_PURCHASING_STATUS_CONFIRMED_PURCHASE;
    uint32 purchaseResult = BATTLE_PAY_SHOP_PURCHASING_RESULT_WAIT_ON_CONFIRMATION;
    SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, ProductID, PurchaseID);

    data.Initialize(SMSG_BATTLE_PAY_SHOP_CONFIRM_PURCHASE, 4 + 8 + 8);

    data << uint32(ClientToken);
    data << uint64(product->CurrentPrice);
    data << uint64(PurchaseID);

    SendPacket(&data);
}

void WorldSession::HandleBattlePayShopConfirmedPurchase(WorldPacket& recvData)
{
    uint32 ClientToken = 0;
    uint64 ProductPrice = 0;

    bool Confirmed = false;

    recvData >> ProductPrice;
    recvData >> ClientToken;

    Confirmed = recvData.ReadBit();

    uint32 productId = 0;
    uint64 purchaseId = 0;

    uint32 purchaseStatus = BATTLE_PAY_SHOP_PURCHASING_STATUS_CONFIRMED_PURCHASE;
    uint32 purchaseResult = BATTLE_PAY_SHOP_PURCHASING_RESULT_INSTANT_CONFIRMATION;

    if (_battlePayShop->HasOwnedProduct(productId))
    {
        TC_LOG_ERROR("battlepayshop.shop", "ConfirmPurchase: Player (%s, account: %u) already has product %u for purchaseId " UI64FMTD "",
            _player->GetGUID().ToString().c_str(), GetAccountId(), productId, purchaseId);

        SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);
        SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_OTHER, ClientToken, purchaseId);
        return;
    }

    BattlePayShopPurchasedProduct const* purchasedProduct = _battlePayShop->GetPurchasedProduct(ClientToken);
    if (!purchasedProduct)
    {
        TC_LOG_ERROR("battlepayshop.shop", "ConfirmPurchase: Player (%s, account: %u) does not have purchased product provided by token %u for purchaseId " UI64FMTD ".",
            _player->GetGUID().ToString().c_str(), GetAccountId(), ClientToken, purchaseId);

        SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);
        SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_OTHER, ClientToken, purchaseId);
        return;
    }

    purchaseId = purchasedProduct->PurchaseID;

    BattlePayShopProduct const* product = purchasedProduct->product;
    if (!product)
    {
        TC_LOG_ERROR("battlepayshop.shop", "ConfirmPurchase: Product provided by id %u does not exist.", productId);

        SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);
        SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_PURCHASE_DENIED, ClientToken, purchaseId);
        return;
    }

    productId = product->Id;

    if (Confirmed)
    {
        if (product->CurrentPrice != ProductPrice)
        {
            TC_LOG_ERROR("battlepayshop.shop", "ConfirmPurchase: Product's current price %u does not match the real price %u for product %u.",
                product->CurrentPrice, ProductPrice, productId);

            SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);
            SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_PAYMENT_FAILED, ClientToken, purchaseId);
            return;
        }

        if (product->CurrentPrice > _battlePayShop->GetCurrencyQuantity())
        {
            TC_LOG_ERROR("battlepayshop.shop", "ConfirmPurchase: Player (%s, account: %u) does not have enough account balance (%u) to purchase product %u.",
                _player->GetGUID().ToString().c_str(), GetAccountId(), _battlePayShop->GetCurrencyQuantity(), productId);

            SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);
            SendBattlePayShopAckFailed(BATTLE_PAY_SHOP_ERROR_INSUFFICIENT_BALANCE, ClientToken, purchaseId);
            return;
        }

        purchaseResult = BATTLE_PAY_SHOP_PURCHASING_RESULT_WAIT_ON_CONFIRMATION;
        purchaseStatus = BATTLE_PAY_SHOP_PURCHASING_STATUS_ENDED_PURCHASE;
    }

    if (!Confirmed)
    {
        TC_LOG_TRACE("battlepayshop.shop", "ConfirmPurchase: Player (%s, account: %u) cancelled purchase (id: " UI64FMTD ") for product id %u",
            _player->GetGUID().ToString().c_str(), GetAccountId(), purchaseId, productId);

        _battlePayShop->RemovePurchasedProduct(ClientToken);

        SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);

        return;
    }

    TC_LOG_TRACE("battlepayshop.shop", "ConfirmPurchase: Player (%s, account: %u) has bought product (id: %u) for %u balance.",
        _player->GetGUID().ToString().c_str(), GetAccountId(), productId, ProductPrice);

    _battlePayShop->ModifyCurrency(ProductPrice);
    _battlePayShop->AddOwnedProduct(productId, purchasedProduct);
    _battlePayShop->RemovePurchasedProduct(ClientToken);

    SendBattlePayShopProductList();

    SendBattlePayShopPurchaseUpdate(purchaseStatus, purchaseResult, productId, purchaseId);
}

void WorldSession::HandleBattlePayShopAckFailed(WorldPacket& recvData)
{
    uint32 ClientToken = 0;

    recvData >> ClientToken;

    _battlePayShop->RemovePurchasedProduct(ClientToken);
}

void WorldSession::HandleBattlePayShopCharBoost(WorldPacket& recvData)
{
    ObjectGuid characterGUID;
    ObjectGuid distributedID;

    bool hasCharInfo = false;

    uint32 unk = 0;
    recvData >> unk;

    recvData.ReadGuidMask(distributedID, 1);

    recvData.ReadGuidMask(characterGUID, 0);

    recvData.ReadGuidMask(distributedID, 5, 4);

    recvData.ReadGuidMask(characterGUID, 3);

    recvData.ReadGuidMask(distributedID, 6, 0);

    recvData.ReadGuidMask(characterGUID, 5);

    recvData.ReadGuidMask(distributedID, 3, 7);

    recvData.ReadGuidMask(characterGUID, 1);

    recvData.ReadGuidMask(distributedID, 2);

    recvData.ReadGuidMask(characterGUID, 2);

    hasCharInfo = !recvData.ReadBit();

    recvData.ReadGuidMask(characterGUID, 7, 4, 6);

    recvData.ReadGuidBytes(characterGUID, 2);

    recvData.ReadGuidBytes(distributedID, 0);

    recvData.ReadGuidBytes(characterGUID, 0, 7);

    recvData.ReadGuidBytes(distributedID, 7);

    recvData.ReadGuidBytes(characterGUID, 3);

    recvData.ReadGuidBytes(distributedID, 6, 4, 5);

    recvData.ReadGuidBytes(characterGUID, 1, 6, 4);

    recvData.ReadGuidBytes(distributedID, 1, 2, 3);

    recvData.ReadGuidBytes(characterGUID, 5);

    CharacterCacheEntry const* data = sCharacterCache->GetCharacterCacheByGuid(characterGUID);
    if (!data)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PAY_SHOP_CHAR_BOOST - CharacterInfo not found for character %s.", characterGUID.ToString().c_str());
        return;
    }

    // check for using cheating tools, boosting not own character
    if (GetAccountId() != data->AccountId)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PAY_SHOP_CHAR_BOOST - Account %u is trying to cheat!", GetAccountId());
        return;
    }

    if (hasCharInfo)
    {
        uint32 charInfo = 0;
        recvData >> charInfo;

        // Char Boost
        if (!distributedID.IsEmpty())
        {
            uint64 distributedId = distributedID.GetRawValue();

            if (BattlePayShopPurchasedProduct const* distributedProduct = _battlePayShop->GetDistributedProduct(distributedId))
            {
                uint32 teamId = (charInfo >> 24 & 0xFF); // 0 - horde, 1- alliance
                uint32 specialization = (charInfo & 0xFFFFFF);

                TeamId team = TeamId(!teamId);

                TC_LOG_TRACE("battlepayshop.charboost", "StartCharBoost: Player (%s, account: %u) started character boosting with provided team %u and specialization %u",
                    characterGUID.ToString().c_str(), GetAccountId(), uint32(team), specialization);

                _battlePayShop->RemoveDistributedProduct(distributedId);
                _battlePayShop->StartBoostingChar(characterGUID, team, TalentSpecialization(specialization));

                SendBattlePayShopDistributingUpdate(characterGUID, distributedId, distributedProduct, BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_HIDE_DISTRIBUTION);

                WorldPacket data(SMSG_BATTLE_PAY_SHOP_CHAR_BOOST_STARTED, 1 + 8);

                data.WriteGuidMask(characterGUID, 6, 2, 5, 4, 7, 0, 3, 1);
                data.WriteGuidBytes(characterGUID, 4, 1, 6, 0, 7, 5, 2, 3);

                SendPacket(&data);
            }
        }
        else if (sWorld->getBoolConfig(CONFIG_RECRUIT_A_FRIEND_REWARDS_ENABLED))// Raf Reward
        {
            uint32 productId = charInfo;

            RaFReward const* reward = sRecruitAFriendRewardsStore->GetRaFReward(productId);
            if (!reward)
            {
                TC_LOG_DEBUG("network", "CMSG_BATTLE_PAY_SHOP_CHAR_BOOST - RaFRewards: Trying to award product %u, but does not exist.", productId);
                return;
            }

            RaFData* raf = GetRaFData();
            if (!raf)
                return;

            GetPlayer()->AddItem(reward->ItemId, 1);

            WorldPacket data(SMSG_BATTLE_PAY_SHOP_DELIVERY_ENDED, 3 + 4 + 8);

            data.WriteBits(1, 22); // one item

            data << uint32(reward->ItemId);
            data << uint64(0);

            SendPacket(&data);

            raf->DecreaseRewardsCount();
            SendRaFRewards(raf->GetRewardsCount() > 0);
        }
    }
}

void WorldSession::SendBattlePayShopDistributionList()
{
    DistributedProductsMap const& distributedProducts = _battlePayShop->GetDistributedProducts();

    uint32 result = BATTLE_PAY_SHOP_RESULT_OK;

    ObjectGuid characterGUID = ObjectGuid::Empty;

    uint32 productType = BATTLE_PAY_SHOP_PRODUCT_TYPE_CHAR_BOOST;

    uint32 distributingResult = BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_SHOW_DISTRIBUTION;

    bool IsRevoked = false;
    bool HasBattlePayShopDisplayItemInfo = false;
    bool HasBattlePetResult = false;
    bool HasUnk = false;
    bool HasBattlePayProduct = true;
    bool HasBattlepayDisplayInfo = true;
    bool HasAlreadyProduct = false;

    uint32 productFlags = BATTLE_PAY_SHOP_PRODUCT_FLAG_SHOW_PRODUCT | BATTLE_PAY_SHOP_PRODUCT_FLAG_AVAILABLE;

    ByteBuffer distributionsData;

    uint32 DescriptionSize = 0;
    uint32 TitleSize = 0;
    for (auto const itr : distributedProducts)
    {
        BattlePayShopPurchasedProduct const& distributedProduct = itr.second;
        BattlePayShopProduct const* product = distributedProduct.product;

        TitleSize += product->Title.length();
        DescriptionSize += product->Description.length();
    }

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_GET_DISTRIBUTION_LIST, 4 + 3 + distributedProducts.size() * (2 * (1 + 8) + 7 + 4 + 4 + 4 + 4 + 8 +
                    (HasBattlepayDisplayInfo ? (4 + 4 + 4 + 4) : 0) + 8 + 1 + 4 + 4 + 8 + 4 + 4 + 4) + TitleSize + DescriptionSize);

    data << uint32(result);

    data.WriteBits(distributedProducts.size(), 19);

    for (auto const itr : distributedProducts)
    {
        uint64 distributeId = itr.first;
        ObjectGuid distributeID = ObjectGuid(distributeId);

        uint64 purchaseId = itr.second.PurchaseID;

        BattlePayShopPurchasedProduct const& distributedProduct = itr.second;
        BattlePayShopProduct const* product = distributedProduct.product;

        data.WriteGuidMask(distributeID, 6, 7, 5, 0);

        data.WriteGuidMask(characterGUID, 2);

        data.WriteGuidMask(distributeID, 4);

        data.WriteBit(IsRevoked);

        data.WriteGuidMask(distributeID, 2);

        data.WriteGuidMask(characterGUID, 7, 3);

        data.WriteGuidMask(distributeID, 3);

        data.WriteBit(HasBattlePayProduct);

        data.WriteGuidMask(characterGUID, 0);

        ByteBuffer battlePayProductData;
        if (HasBattlePayProduct)
        {
            data.WriteBits(1, 20); // one item

            data.WriteBit(HasUnk);
            data.WriteBit(HasBattlePayShopDisplayItemInfo);
            data.WriteBit(HasBattlePetResult);

            if (HasBattlePetResult)
                data.WriteBits(0, 4); // PetResult

            data.WriteBit(HasAlreadyProduct);

            data.WriteBits(product->ChoiceType, 2);

            data.WriteBit(HasBattlepayDisplayInfo);

            ByteBuffer displayData;
            if (HasBattlepayDisplayInfo)
            {
                uint32 CreatureDisplayID = product->CreatureDisplayId;
                uint32 FileDataID = product->FileDataId;
                uint32 DisplayFlags = 0;
                uint32 Unk = 0;

                bool HasCreatureDisplayInfoID = CreatureDisplayID != 0;
                bool HasDisplayFlags = DisplayFlags != 0;
                bool HasFileDataID = FileDataID != 0;
                bool HasUnk = Unk != 0;

                data.WriteBit(HasUnk);

                data.WriteBits(product->Title.length(), 10);

                data.WriteBit(HasFileDataID);

                data.WriteBits(product->Description.length(), 13);

                data.WriteBit(HasCreatureDisplayInfoID);

                data.WriteBits(0, 10);

                data.WriteBit(HasDisplayFlags);

                if (HasDisplayFlags)
                    displayData << uint32(DisplayFlags);

                //displayData.WriteString("");

                if (HasUnk)
                    displayData << uint32(Unk);

                if (HasCreatureDisplayInfoID)
                    displayData << uint32(CreatureDisplayID);

                displayData.WriteString(product->Title);

                if (HasFileDataID)
                    displayData << uint32(FileDataID);

                displayData.WriteString(product->Description);
            }

            battlePayProductData << uint32(product->Id);
            battlePayProductData << uint32(product->ItemId);
            battlePayProductData << uint32(product->Quantity);
            battlePayProductData << uint32(productFlags);

            if (!displayData.empty())
                battlePayProductData.append(displayData);

            battlePayProductData << uint8(productType);
            battlePayProductData << uint64(product->NormalPrice);
            battlePayProductData << uint32(product->Id);
            battlePayProductData << uint64(product->CurrentPrice);
        }

        data.WriteGuidMask(characterGUID, 1);

        data.WriteGuidMask(distributeID, 1);

        data.WriteGuidMask(characterGUID, 4, 6, 5);

        distributionsData.WriteGuidBytes(characterGUID, 6);

        distributionsData << uint64(purchaseId);

        if (!battlePayProductData.empty())
            distributionsData.append(battlePayProductData);

        distributionsData.WriteGuidBytes(characterGUID, 2);

        distributionsData.WriteGuidBytes(distributeID, 0, 2);

        distributionsData.WriteGuidBytes(characterGUID, 1);

        distributionsData << uint32(realmID);

        distributionsData.WriteGuidBytes(characterGUID, 5);

        distributionsData << uint32(distributingResult);

        distributionsData.WriteGuidBytes(distributeID, 1);

        distributionsData.WriteGuidBytes(characterGUID, 4);

        distributionsData << uint32(realmID);

        distributionsData.WriteGuidBytes(distributeID, 6, 5);

        distributionsData << uint32(product->Id);

        distributionsData.WriteGuidBytes(distributeID, 7, 4);

        distributionsData.WriteGuidBytes(characterGUID, 7, 0, 3);

        distributionsData.WriteGuidBytes(distributeID, 3);
    }

    if (!distributionsData.empty())
        data.append(distributionsData);
    else
        data.FlushBits();

    SendPacket(&data);
}

void WorldSession::SendBattlePayShopPurchaseList()
{
    uint32 result = BATTLE_PAY_SHOP_RESULT_OK;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_GET_PURCHASE_LIST, 3 + 4);

    data.WriteBits(0, 19);

    data << uint32(result);

    SendPacket(&data);
}

void WorldSession::SendBattlePayShopAckFailed(uint32 error, uint32 clientToken, uint64 purchaseId)
{
    uint32 Result = BATTLE_PAY_SHOP_RESULT_OK;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_ACK_FAILED, 4 + 4 + 4 + 8);

    data << uint32(Result);
    data << uint32(error);
    data << uint32(clientToken);
    data << uint64(purchaseId);

    SendPacket(&data);
}

void WorldSession::SendBattlePayShopPurchaseUpdate(uint32 purchaseStatus, uint32 purchaseResult, uint32 productId, uint64 purchaseId)
{
    std::string const& walletName = _battlePayShop->GetWalletName();

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_PURCHASE_UPDATE, 4 + 4 + 8 + 4 + 4 + walletName.length());

    data.WriteBits(1, 19); // one product

    data.WriteBits(walletName.length(), 8);

    data << uint32(purchaseResult);
    data << uint64(purchaseId);
    data << uint32(productId);
    data << uint32(purchaseStatus);

    data.WriteString(walletName);

    SendPacket(&data);
}

void WorldSession::SendBattlePayShopDistributingUpdate(ObjectGuid characterGUID, uint64 distributeId, BattlePayShopPurchasedProduct const* distributedProduct, uint32 distributingResult)
{
    ObjectGuid distributeID = ObjectGuid(distributeId);

    uint64 purchaseId = distributedProduct->PurchaseID;
    BattlePayShopProduct const* product = distributedProduct->product;

    uint32 productType = BATTLE_PAY_SHOP_PRODUCT_TYPE_CHAR_BOOST;

    bool IsRevoked = false;
    bool HasBattlePayShopDisplayItemInfo = false;
    bool HasBattlePetResult = false;
    bool HasUnk = false;
    bool HasBattlePayProduct = true;
    bool HasBattlepayDisplayInfo = true;
    bool HasAlreadyProduct = false;

    uint32 productFlags = BATTLE_PAY_SHOP_PRODUCT_FLAG_SHOW_PRODUCT | BATTLE_PAY_SHOP_PRODUCT_FLAG_AVAILABLE;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_DISTRIBUTION_UPDATE, 2 * (1 + 8) + 7 + 4 + 4 + 4 + 4 + 8 + (HasBattlepayDisplayInfo ? (4 + 4 + 4 + 4) : 0) +
                    8 + 1 + 4 + 4 + 8 + 4 + 4 + 4 + product->Title.length() + product->Description.length());

    data.WriteGuidMask(distributeID, 5, 0);

    data.WriteBit(HasBattlePayProduct);

    data.WriteGuidMask(distributeID, 1);

    data.WriteGuidMask(characterGUID, 4, 7, 0);

    data.WriteBit(IsRevoked);

    data.WriteGuidMask(characterGUID, 1, 2);

    ByteBuffer displayData;
    if (HasBattlePayProduct)
    {
        data.WriteBits(product->ChoiceType, 2);

        data.WriteBits(1, 20); // one item

        data.WriteBit(HasBattlePayShopDisplayItemInfo);
        data.WriteBit(HasUnk);
        data.WriteBit(HasAlreadyProduct);
        data.WriteBit(HasBattlePetResult);

        if (HasBattlePetResult)
            data.WriteBits(0, 4); // PetResult

        data.WriteBit(HasBattlepayDisplayInfo);

        if (HasBattlepayDisplayInfo)
        {
            uint32 CreatureDisplayID = product->CreatureDisplayId;
            uint32 FileDataID = product->FileDataId;
            uint32 DisplayFlags = 0;
            uint32 Unk = 0;

            bool HasCreatureDisplayInfoID = CreatureDisplayID != 0;
            bool HasDisplayFlags = DisplayFlags != 0;
            bool HasFileDataID = FileDataID != 0;
            bool HasUnk = Unk != 0;

            data.WriteBits(0, 10);
            data.WriteBit(HasCreatureDisplayInfoID);
            data.WriteBits(product->Title.length(), 10);
            data.WriteBit(HasUnk);
            data.WriteBit(HasDisplayFlags);
            data.WriteBits(product->Description.length(), 13);
            data.WriteBit(HasFileDataID);

            if (HasDisplayFlags)
                displayData << uint32(DisplayFlags);

            displayData.WriteString(product->Description);

            displayData.WriteString(product->Title);

            //displayData.WriteString("");

            if (HasUnk)
                displayData << uint32(Unk);

            if (HasFileDataID)
                displayData << uint32(FileDataID);

            if (HasCreatureDisplayInfoID)
                displayData << uint32(CreatureDisplayID);
        }
    }

    data.WriteGuidMask(distributeID, 7);

    data.WriteGuidMask(characterGUID, 6);

    data.WriteGuidMask(distributeID, 2);

    data.WriteGuidMask(characterGUID, 5);

    data.WriteGuidMask(distributeID, 3, 6);

    data.WriteGuidMask(characterGUID, 3);

    data.WriteGuidMask(distributeID, 4);

    if (HasBattlePayProduct)
    {
        data << uint32(product->Id);
        data << uint32(product->Quantity);
        data << uint32(product->ItemId);
        data << uint32(product->Id);
        data << uint64(product->CurrentPrice);

        if (!displayData.empty())
            data.append(displayData);

        data << uint64(product->NormalPrice);
        data << uint8(productType);
        data << uint32(productFlags);
    }

    data << uint32(product->Id);

    data.WriteGuidBytes(characterGUID, 4);

    data << uint64(purchaseId);

    data.WriteGuidBytes(characterGUID, 1, 5);

    data.WriteGuidBytes(distributeID, 2, 4, 1, 0);

    data << uint32(realmID);

    data.WriteGuidBytes(distributeID, 7);

    data.WriteGuidBytes(characterGUID, 0, 7);

    data << uint32(realmID);
    data << uint32(distributingResult);

    data.WriteGuidBytes(characterGUID, 6);

    data.WriteGuidBytes(distributeID, 5, 6, 3);

    data.WriteGuidBytes(characterGUID, 3, 2);

    SendPacket(&data);
}
