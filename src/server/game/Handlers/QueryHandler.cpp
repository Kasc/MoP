/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "Language.h"
#include "DatabaseEnv.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "World.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "NPCHandler.h"
#include "Pet.h"
#include "MapManager.h"
#include "Config.hpp"
#include "CharacterCache.h"

void WorldSession::SendNameQueryOpcode(ObjectGuid guid)
{
    CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(guid);

    Player* player = ObjectAccessor::FindConnectedPlayer(guid);

    bool IsDeleted = false;
    uint32 accountId = 0;
    ObjectGuid accountGuid;
    std::string Name;
    uint8 Race = RACE_NONE;
    uint8 Gender = GENDER_NONE;
    uint8 ClassID = CLASS_NONE;
    uint8 Level = 0;
    DeclinedNames declinedNames;

    ResponseCodes response = RESPONSE_FAILURE;

    if (characterInfo)
    {
        response = RESPONSE_SUCCESS;

        if (player)
        {
            ASSERT(player->GetGUID() == guid);

            accountId = player->GetSession()->GetAccountId();

            Name = player->GetName();
            Race = player->GetRace();
            Gender = player->GetGender();
            ClassID = player->GetClass();
            Level = player->GetLevel();

            declinedNames = player->GetDeclinedNames();
        }
        else
        {
            accountId = sCharacterCache->GetCharacterAccountIdByGuid(guid);
            Name = characterInfo->Name;
            Race = characterInfo->Race;
            Gender = characterInfo->Gender;
            ClassID = characterInfo->Class;
            Level = characterInfo->Level;
        }

        accountGuid = ObjectGuid::Create<HighGuid::Account>(accountId);
        IsDeleted = characterInfo->IsDeleted;
    }

    uint32 DeclinedNamesLength = 0;
    for (std::string const& Name : declinedNames)
        DeclinedNamesLength += Name.length();

    WorldPacket data(SMSG_NAME_QUERY_RESPONSE, 1 + 8 + 1 + (response != RESPONSE_FAILURE ? (4 + 4 + 1 + 1 + 1 + 1 + 2 * (1 + 8) +
                    MAX_DECLINED_NAME_CASES * (1) + 1 + Name.length() + DeclinedNamesLength) : 0));

    data.WriteGuidMask(guid, 3, 6, 7, 2, 5, 4, 0, 1);

    data.WriteGuidBytes(guid, 5, 4, 7, 6, 1, 2);

    data << uint8(response);

    if (response != RESPONSE_FAILURE)
    {
        data << uint32(realmID);
        data << uint32(accountId);
        data << uint8(ClassID);
        data << uint8(Race);
        data << uint8(Level);
        data << uint8(Gender);
    }

    data.WriteGuidBytes(guid, 0, 3);

    if (response == RESPONSE_FAILURE)
    {
        SendPacket(&data);
        return;
    }

    data.WriteGuidMask(accountGuid, 2, 7);

    data.WriteGuidMask(guid, 7, 2, 0);

    data.WriteBit(IsDeleted);

    data.WriteGuidMask(accountGuid, 4);

    data.WriteGuidMask(guid, 5);

    data.WriteGuidMask(accountGuid, 1, 3, 0);

    for (std::string const& Name : declinedNames)
        data.WriteBits(Name.size(), 7);

    data.WriteGuidMask(guid, 6, 3);

    data.WriteGuidMask(accountGuid, 5);

    data.WriteGuidMask(guid, 1, 4);

    data.WriteBits(Name.size(), 6);

    data.WriteGuidMask(accountGuid, 6);

    data.WriteGuidBytes(guid, 6, 0);

    data.WriteString(Name);

    data.WriteGuidBytes(accountGuid, 5, 2);

    data.WriteGuidBytes(guid, 3);

    data.WriteGuidBytes(accountGuid, 4, 3);

    data.WriteGuidBytes(guid, 4, 2);

    data.WriteGuidBytes(accountGuid, 7);

    for (std::string const& Name : declinedNames)
        data.WriteString(Name);

    data.WriteGuidBytes(accountGuid, 6);

    data.WriteGuidBytes(guid, 7, 1);

    data.WriteGuidBytes(accountGuid, 1);

    data.WriteGuidBytes(guid, 5);

    data.WriteGuidBytes(accountGuid, 0);

    SendPacket(&data);
}

void WorldSession::HandleNameQueryOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    bool HasVirtualRealm = false;
    bool HasNativeRealm = false;

    uint32 VirtualRealmID = 0;
    uint32 NativeRealID = 0;

    recvData.ReadGuidMask(guid, 4);

    HasVirtualRealm = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 6, 0, 7, 1);

    HasNativeRealm = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 5, 2, 3);

    recvData.ReadGuidBytes(guid, 7, 5, 1, 2, 6, 3, 0, 4);

    // virtual and native realm Addresses

    if (HasVirtualRealm)
        recvData >> VirtualRealmID;

    if (HasNativeRealm)
        recvData >> NativeRealID;

    SendNameQueryOpcode(guid);
}

void WorldSession::SendRealmNameQueryOpcode(uint32 realmId)
{
    std::string realmName = sObjectMgr->GetRealmName(realmID);
    std::string normalizedRealmName = sObjectMgr->GetNormalizedRealmName(realmID);

    bool found = !realmName.empty();

    WorldPacket data(SMSG_REALM_NAME_QUERY_RESPONSE, 1 + 4 + (found ? (3 + realmName.length() + normalizedRealmName.length()) : 0));

    data << uint8(!found);
    data << uint32(realmId);

    if (found)
    {
        data.WriteBits(realmName.length(), 8);

        data.WriteBit(realmId == realmID);

        data.WriteBits(normalizedRealmName.length(), 8);

        data.FlushBits();

        data.WriteString(realmName);
        data.WriteString(normalizedRealmName);
    }

    SendPacket(&data);
}

void WorldSession::HandleRealmNameQueryOpcode(WorldPacket& recvData)
{
    uint32 realmId = 0;

    recvData >> realmId;

    SendRealmNameQueryOpcode(realmId);
}

void WorldSession::HandleQueryTimeOpcode(WorldPacket & /*recvData*/)
{
    SendQueryTimeResponse();
}

void WorldSession::SendQueryTimeResponse()
{
    WorldPacket data(SMSG_QUERY_TIME_RESPONSE, 4 + 4);

    data << uint32(time(NULL));
    data << uint32(sWorld->GetNextDailyQuestsResetTime() - time(NULL));

    SendPacket(&data);
}

/// Only _static_ data is sent in this packet !!!
void WorldSession::HandleCreatureQueryOpcode(WorldPacket& recvData)
{
    uint32 entry;
    recvData >> entry;

    CreatureTemplate const* info = sObjectMgr->GetCreatureTemplate(entry);

    bool hasData = info != nullptr;

    std::string Name;
    std::string NameAlt;
    std::string SubName;
    std::string SubNameAlt;
    std::string IconName;

    uint32 NameLength = 0;
    uint32 NameAltLength = 0;
    uint32 SubNameLength = 0;
    uint32 SubNameAltLength = 0;
    uint32 IconNameLength = 0;

    CreatureQuestItemList const* items = nullptr;

    if (hasData)
    {
        Name = info->Name;
        if (!Name.empty())
            NameLength = Name.length() + 1;

        NameAlt = info->FemaleName;
        if (!NameAlt.empty())
            NameAltLength = NameAlt.length() + 1;

        SubName = info->SubName;
        if (!SubName.empty())
            SubNameLength = SubName.length() + 1;

        IconName = info->IconName;
        if (!IconName.empty())
            IconNameLength = IconName.length() + 1;

        TC_LOG_DEBUG("network", "WORLD: CMSG_CREATURE_QUERY '%s' - Entry: %u.", info->Name.c_str(), entry);

        items = sObjectMgr->GetCreatureQuestItemList(entry);
    }

    WorldPacket data(SMSG_CREATURE_QUERY_RESPONSE, 4 + 1 + (hasData ? (7 + MAX_CREATURE_NAMES * (3) + SubNameAltLength + 40 +
                    NameLength + NameAltLength + SubNameLength + 4 + 4 + IconNameLength + (items ? (items->size() * (4)) : 0) + 4 + 4 + 4) : 0));

    data << uint32(entry);

    data.WriteBit(hasData);

    if (hasData)
    {
        data.WriteBits(SubNameLength, 11);
        data.WriteBits(items ? items->size() : 0, 22);
        data.WriteBits(SubNameAltLength, 11);

        for (uint8 i = 0; i < MAX_CREATURE_NAMES; ++i)
        {
            if (i == 0)
            {
                data.WriteBits(NameLength, 11);
                data.WriteBits(NameAltLength, 11);
            }
            else
            {
                data.WriteBits(0, 11);
                data.WriteBits(0, 11);
            }
        }

        data.WriteBit(info->RacialLeader);

        data.WriteBits(IconNameLength, 6);

        data.FlushBits();

        if (SubNameAltLength > 0)
            data << SubNameAlt;

        data << uint32(info->KillCredit[0]);
        data << uint32(info->Modelid4);
        data << uint32(info->Modelid2);
        data << uint32(info->expansion);
        data << uint32(info->type);
        data << float(info->ModHealth);
        data << uint32(info->type_flags);
        data << uint32(info->type_flags2);
        data << uint32(info->rank);
        data << uint32(info->movementId);

        for (uint8 i = 0; i < MAX_CREATURE_NAMES; ++i)
        {
            if (i == 0)
            {
                if (NameAltLength > 0)
                    data << NameAlt;

                if (NameLength > 0)
                    data << Name;
            }
            else
            {
                // Not supported
            }
        }

        if (SubNameLength > 0)
            data << SubName;

        data << uint32(info->Modelid1);
        data << uint32(info->Modelid3);

        if (IconNameLength > 0)
            data << IconName;

        if (items)
            for (uint32 item : *items)
                data << uint32(item);

        data << uint32(info->KillCredit[1]);
        data << float(info->ModMana);
        data << uint32(info->family);
    }
    else
        TC_LOG_DEBUG("network", "WORLD: CMSG_CREATURE_QUERY - NO CREATURE INFO! (ENTRY: %u)", entry);

    SendPacket(&data);
}

/// Only _static_ data is sent in this packet !!!
void WorldSession::HandleGameObjectQueryOpcode(WorldPacket& recvData)
{
    uint32 entry;
    ObjectGuid guid;

    recvData >> entry;

    recvData.ReadGuidMask(guid, 5, 3, 6, 2, 7, 1, 0, 4);
    recvData.ReadGuidBytes(guid, 1, 5, 3, 4, 6, 2, 7, 0);

    const GameObjectTemplate* info = sObjectMgr->GetGameObjectTemplate(entry);

    ByteBuffer byteData;

    byteData << uint32(entry);

    if (info)
    {
        std::string Name[MAX_GAMEOBJECTS_NAMES];
        std::string IconName;
        std::string CastBarCaption;
        std::string UnkString;

        Name[0] = info->name;
        IconName = info->IconName;
        CastBarCaption = info->castBarCaption;
        CastBarCaption = info->unk1;

        uint32 Type = info->type;
        uint32 DisplayId = info->displayId;

        float Size = info->size;

        uint32 Data[MAX_GAMEOBJECT_DATA];
        for (uint8 i = 0; i < MAX_GAMEOBJECT_DATA; ++i)
            Data[i] = info->raw.data[i];

        GameObjectQuestItemList const* items = sObjectMgr->GetGameObjectQuestItemList(entry);
        uint32 ItemSize = items ? items->size() : 0;

        uint32 RequiredLevel = info->RequiredLevel;

        uint32 DataSize = sizeof(Type) + sizeof(DisplayId) + (Name->size() + (4 * 1)) + (IconName.size() + 1) + (CastBarCaption.size() + 1) + (UnkString.size() + 1) + sizeof(Data) + sizeof(Size) + sizeof(uint8) + (items ? (items->size() * sizeof(int32)) : 0) + sizeof(RequiredLevel);

        byteData << uint32(DataSize);

        byteData << uint32(Type);
        byteData << uint32(DisplayId);

        for (uint8 i = 0; i < MAX_GAMEOBJECTS_NAMES; ++i)
            byteData << Name[i];

        byteData << IconName;
        byteData << CastBarCaption;
        byteData << UnkString;

        for (uint8 i = 0; i < MAX_GAMEOBJECT_DATA; i++)
            byteData << Data[i];

        byteData << float(Size);

        byteData << uint8(ItemSize);

        if (items)
            for (uint32 item : *items)
                byteData << uint32(item);

        byteData << int32(RequiredLevel);
    }
    else
    {
        byteData << uint32(0);

        TC_LOG_DEBUG("network", "WORLD: CMSG_GAMEOBJECT_QUERY - Missing gameobject info for (%s, ENTRY: %u)",
            guid.ToString().c_str(), entry);
    }

    WorldPacket data(SMSG_GAMEOBJECT_QUERY_RESPONSE, 1 + byteData.size());

    data.WriteBit(info != NULL);

    data.append(byteData);

    SendPacket(&data);
}

void WorldSession::HandleCorpseQueryOpcode(WorldPacket& /*recvData*/)
{
    Player* player = GetPlayer();

    bool CorpseFound = false;

    ObjectGuid corpseGuid;

    uint32 corpseMapID = 0;
    uint32 mapID = 0;
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    if (player->HasCorpse())
    {
        CorpseFound = true;

        WorldLocation corpseLocation = player->GetCorpseLocation();
        corpseMapID = corpseLocation.GetMapId();
        mapID = corpseLocation.GetMapId();
        x = corpseLocation.GetPositionX();
        y = corpseLocation.GetPositionY();
        z = corpseLocation.GetPositionZ();

        // if corpse at different map
        if (mapID != player->GetMapId())
        {
            // search entrance map for proper show entrance
            if (MapEntry const* corpseMapEntry = sMapStore.LookupEntry(mapID))
            {
                if (corpseMapEntry->IsDungeon() && corpseMapEntry->CorpseMapID >= 0)
                {
                    // if corpse map have entrance
                    if (Map const* entranceMap = sMapMgr->CreateBaseMap(corpseMapEntry->CorpseMapID))
                    {
                        mapID = corpseMapEntry->CorpseMapID;
                        x = corpseMapEntry->CorpsePos.X;
                        y = corpseMapEntry->CorpsePos.Y;
                        z = entranceMap->GetHeight(x, y, MAX_HEIGHT, player->GetPhases(), player->GetTerrainSwaps());
                    }

                    // Get map for corpse
                    if (Map* corpseMap = sMapMgr->FindMap(corpseMapID, corpseLocation.GetInstanceId()))
                        if (Corpse* corpse = corpseMap->GetCorpseByPlayer(player->GetGUID()))
                            corpseGuid = corpse->GetGUID();
                }
            }
        }
        else if (Corpse* corpse = GetPlayer()->GetCorpse())
            corpseGuid = corpse->GetGUID();
    }

    WorldPacket data(SMSG_CORPSE_QUERY, 1 + 8 + 1 + 4 + 4 + 4 + 4 + 4);

    data.WriteGuidMask(corpseGuid, 0, 3, 2);

    data.WriteBit(CorpseFound);

    data.WriteGuidMask(corpseGuid, 5, 4, 1, 7, 6);

    data.WriteGuidBytes(corpseGuid, 5);

    data << float(z);

    data.WriteGuidBytes(corpseGuid, 1);

    data << int32(mapID);

    data.WriteGuidBytes(corpseGuid, 6, 4);

    data << float(x);

    data.WriteGuidBytes(corpseGuid, 3, 7, 2, 0);

    data << uint32(corpseMapID);
    data << float(y);

    SendPacket(&data);
}

void WorldSession::HandleNpcTextQueryOpcode(WorldPacket& recvData)
{
    uint32 textID = 0;

    ObjectGuid guid;

    recvData >> textID;

    recvData.ReadGuidMask(guid, 4, 5, 1, 7, 0, 2, 6, 3);
    recvData.ReadGuidBytes(guid, 4, 0, 2, 5, 1, 7, 3, 6);

    Creature* creature = ObjectAccessor::GetCreature(*GetPlayer(), guid);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "CMSG_NPC_TEXT_QUERY - creature with guid %s is invalid!", guid.ToString().c_str());
        return;
    }

    NpcText const* npcText = sObjectMgr->GetNpcText(textID);

    bool Allow = false;

    std::vector<std::pair<uint32, float>> npcTexts;
    if (npcText)
        for (uint8 i = 0; i < MAX_NPC_TEXT_OPTIONS; ++i)
        {
            npcTexts.push_back(std::make_pair(npcText->Data[i].BroadcastTextID, npcText->Data[i].Probability));
            if (!Allow && npcText->Data[i].BroadcastTextID)
                Allow = true;
        }

    WorldPacket data(SMSG_NPC_TEXT_UPDATE, 4 + 4 + (Allow ? (MAX_NPC_TEXT_OPTIONS * 2 * 4) : 0));

    data << uint32(textID);
    data << uint32(Allow ? (MAX_NPC_TEXT_OPTIONS * (sizeof(float) + sizeof(uint32))) : 0);

    if (Allow)
    {
        for (auto & it : npcTexts)
            data << float(it.second);

        for (auto & it : npcTexts)
            data << uint32(it.first);
    }

    data.WriteBit(Allow);

    data.FlushBits();

    SendPacket(&data);
}

/// Only _static_ data is sent in this packet !!!
void WorldSession::HandlePageTextQueryOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;
    uint32 pageTextID;

    recvData >> pageTextID;

    recvData.ReadGuidMask(guid, 2, 1, 3, 7, 6, 4, 0, 5);
    recvData.ReadGuidBytes(guid, 0, 6, 3, 5, 1, 7, 4, 2);

    uint32 pageID = pageTextID;
    while (pageID)
    {
        PageText const* pageText = sObjectMgr->GetPageText(pageID);

        bool Allow = pageText != nullptr;

        std::string Text = "Item page missing.";

        uint32 PageId = 0;
        uint32 NextPageId = 0;

        if (Allow)
        {
            Text = pageText->Text;
            PageId = pageID;
            NextPageId = pageText->NextPageID;
        }

        pageID = NextPageId;

        WorldPacket data(SMSG_PAGE_TEXT_QUERY_RESPONSE, 1 + 2 + 4 + 4 + Text.length() + 4);

        data.WriteBit(Allow);

        data.WriteBits(Text.length(), 12);

        data << uint32(NextPageId);
        data << uint32(PageId);

        data.WriteString(Text);

        data << uint32(pageTextID);

        SendPacket(&data);
    }
}

void WorldSession::HandleCorpseTransportQuery(WorldPacket& recvData)
{
    ObjectGuid transportGuid;

    recvData.ReadGuidMask(transportGuid, 7, 6, 3, 0, 4, 1, 5, 2);
    recvData.ReadGuidBytes(transportGuid, 1, 6, 0, 5, 3, 2, 4, 7);

    Position pos;

    Corpse* corpse = _player->GetCorpse();
    if (corpse && !corpse->GetTransportGUID().IsEmpty() && corpse->GetTransportGUID() == transportGuid)
        pos.Relocate(corpse->GetTransportPosition());

    WorldPacket data(SMSG_CORPSE_TRANSPORT_QUERY_RESPONSE, 4 + 4 + 4 + 4);

    data << float(pos.GetPositionY());
    data << float(pos.GetPositionX());
    data << float(pos.GetOrientation());
    data << float(pos.GetPositionZ());

    SendPacket(&data);
}

void WorldSession::HandleQuestNPCQuery(WorldPacket& recvData)
{
    std::vector<uint32> QuestIDs(50);
    for (uint8 i = 0; i < 50; ++i)
        recvData >> QuestIDs[i];

    uint32 count = 0;
    recvData >> count;

    std::map<uint32, std::vector<uint32>> quests;

    uint32 NPCsSize = 0;
    for (uint32 i = 0; i < count; ++i)
    {
        uint32 questId = QuestIDs[i];

        if (!sObjectMgr->GetQuestTemplate(questId))
        {
            TC_LOG_DEBUG("network", "WORLD: Unknown quest %u in CMSG_QUEST_NPC_QUERY by player %u", questId, m_GUIDLow);
            continue;
        }

        auto creatures = sObjectMgr->GetCreatureQuestInvolvedRelationReverseBounds(questId);
        for (auto it = creatures.first; it != creatures.second; ++it)
            quests[questId].push_back(it->second);

        auto gos = sObjectMgr->GetGOQuestInvolvedRelationReverseBounds(questId);
        for (auto it = gos.first; it != gos.second; ++it)
            quests[questId].push_back(it->second | 0x80000000); // GO mask

        NPCsSize += quests[questId].size();
    }

    ByteBuffer npcData;

    WorldPacket data(SMSG_QUEST_NPC_QUERY_RESPONSE, 3 + quests.size() * (3 + 4) + NPCsSize * (4));

    data.WriteBits(quests.size(), 21);

    for (auto & it : quests)
    {
        data.WriteBits(it.second.size(), 22);

        npcData << uint32(it.first);

        for (uint32 NpcID : it.second)
            npcData << uint32(NpcID);
    }

    data.append(npcData);

    SendPacket(&data);
}

struct QuestPOIBlobPoint
{
    int32 X = 0;
    int32 Y = 0;
};

struct QuestPOIBlobData
{
    int32 BlobIndex = 0;
    int32 ObjectiveIndex = 0;
    int32 QuestObjectiveID = 0;
    int32 MapID = 0;
    int32 WorldMapAreaID = 0;
    int32 Floor = 0;
    int32 Priority = 0;
    int32 Flags = 0;
    int32 WorldEffectID = 0;
    int32 PlayerConditionID = 0;
    std::vector<QuestPOIBlobPoint> QuestPOIBlobPointStats;
};

struct QuestPOIData
{
    int32 QuestID = 0;
    std::vector<QuestPOIBlobData> QuestPOIBlobDataStats;
};

void WorldSession::HandleQuestPOIQuery(WorldPacket& recvData)
{
    uint32 count = 0;
    count = recvData.ReadBits(22);

    if (count >= MAX_QUEST_LOG_SIZE)
    {
        recvData.rfinish();
        return;
    }

    // Read quest ids and add the in a unordered_set so we don't send POIs for the same quest multiple times
    std::unordered_set<int32> questIds;
    for (uint32 i = 0; i < count; ++i)
    {
        uint32 questId = 0;
        recvData >> questId;

        questIds.insert(questId);
    }


    std::vector<QuestPOIData> QuestPOIDataStats;

    uint32 POIBlobSize = 0;
    uint32 POIPointsSize = 0;

    for (uint32 QuestID : questIds)
    {
        bool questOk = false;

        uint16 questSlot = _player->FindQuestSlot(uint32(QuestID));

        if (questSlot != MAX_QUEST_LOG_SIZE)
            questOk = _player->GetQuestSlotQuestId(questSlot) == uint32(QuestID);

        if (questOk)
        {
            QuestPOIVector const* poiData = sObjectMgr->GetQuestPOIVector(QuestID);
            if (poiData)
            {
                QuestPOIData questPOIData;

                questPOIData.QuestID = QuestID;

                for (auto data = poiData->begin(); data != poiData->end(); ++data)
                {
                    QuestPOIBlobData questPOIBlobData;

                    questPOIBlobData.BlobIndex = data->BlobIndex;
                    questPOIBlobData.ObjectiveIndex = data->ObjectiveIndex;
                    questPOIBlobData.QuestObjectiveID = data->QuestObjectiveID;
                    questPOIBlobData.MapID = data->MapID;
                    questPOIBlobData.WorldMapAreaID = data->WorldMapAreaID;
                    questPOIBlobData.Floor = data->Floor;
                    questPOIBlobData.Priority = data->Priority;
                    questPOIBlobData.Flags = data->Flags;
                    questPOIBlobData.WorldEffectID = data->WorldEffectID;
                    questPOIBlobData.PlayerConditionID = data->PlayerConditionID;

                    for (auto points = data->points.begin(); points != data->points.end(); ++points)
                    {
                        QuestPOIBlobPoint questPOIBlobPoint;

                        questPOIBlobPoint.X = points->X;
                        questPOIBlobPoint.Y = points->Y;

                        TC_LOG_DEBUG("misc", "Quest: %i BlobIndex: %i X/Y: %i/%i", QuestID, data->BlobIndex, points->X, points->Y);

                        questPOIBlobData.QuestPOIBlobPointStats.push_back(questPOIBlobPoint);
                    }

                    POIPointsSize += questPOIBlobData.QuestPOIBlobPointStats.size();

                    questPOIData.QuestPOIBlobDataStats.push_back(questPOIBlobData);
                }

                POIBlobSize += questPOIData.QuestPOIBlobDataStats.size();

                QuestPOIDataStats.push_back(questPOIData);
            }
        }
    }

    ByteBuffer poiData;

    WorldPacket data(SMSG_QUEST_POI_QUERY_RESPONSE, 3 + 4 + QuestPOIDataStats.size() * (3 + 4 + 4) + POIBlobSize * (3 + 44) + POIPointsSize * (4 + 4));

    data.WriteBits(QuestPOIDataStats.size(), 20);

    for (QuestPOIData const& POI : QuestPOIDataStats)
    {
        data.WriteBits(POI.QuestPOIBlobDataStats.size(), 18);

        for (QuestPOIBlobData const& questPOIBlobData : POI.QuestPOIBlobDataStats)
        {
            data.WriteBits(questPOIBlobData.QuestPOIBlobPointStats.size(), 21);

            poiData << uint32(questPOIBlobData.Floor);

            for (QuestPOIBlobPoint const& questPOIBlobPoint : questPOIBlobData.QuestPOIBlobPointStats)
            {
                poiData << int32(questPOIBlobPoint.X);
                poiData << int32(questPOIBlobPoint.Y);
            }

            poiData << int32(questPOIBlobData.ObjectiveIndex);
            poiData << uint32(questPOIBlobData.BlobIndex);
            poiData << uint32(questPOIBlobData.QuestObjectiveID);
            poiData << uint32(questPOIBlobData.PlayerConditionID);
            poiData << uint32(questPOIBlobData.MapID);
            poiData << uint32(questPOIBlobData.QuestPOIBlobPointStats.size()); // 40
            poiData << uint32(questPOIBlobData.WorldMapAreaID);
            poiData << uint32(questPOIBlobData.WorldEffectID);
            poiData << uint32(questPOIBlobData.Flags);
            poiData << uint32(questPOIBlobData.Priority);
        }

        poiData << uint32(POI.QuestID);
        poiData << uint32(POI.QuestPOIBlobDataStats.size());
    }

    poiData << uint32(QuestPOIDataStats.size());

    data.append(poiData);

    SendPacket(&data);
}

void WorldSession::HandleDBQueryBulk(WorldPacket& recvData)
{
    uint32 type = 0;
    uint32 count = 0;

    recvData >> type;

    DB2StorageBase const* store = sDB2Manager->GetStorage(type);
    if (!store)
    {
        TC_LOG_ERROR("network", "CMSG_DB_QUERY_BULK: Received unknown hotfix type: %u", type);
        recvData.rfinish();
        return;
    }

    count = recvData.ReadBits(21);

    GuidVector guids(count);
    for (uint32 i = 0; i < count; ++i)
        recvData.ReadGuidMask(guids[i], 6, 3, 0, 1, 4, 5, 7, 2);

    uint32 entry = 0;
    for (uint32 i = 0; i < count; ++i)
    {
        recvData.ReadGuidBytes(guids[i], 1);

        recvData >> entry;

        recvData.ReadGuidBytes(guids[i], 0, 5, 6, 4, 7, 2, 3);

        if (!store->HasRecord(entry))
        {
            TC_LOG_ERROR("network", "CMSG_DB_QUERY_BULK: Entry %u does not exist in datastore: %u", entry, type);
            continue;
        }

        ByteBuffer record;
        store->WriteRecord(entry, GetSessionDbcLocale(), record);

        time_t timer = sDB2Manager->GetHotfixDate(entry, type);

        WorldPacket data(SMSG_DB_REPLY, 4 + 4 + 4 + 4 + record.size());

        data << uint32(entry);

        data.AppendPackedTime(timer);

        data << uint32(type);
        data << uint32(record.size());

        data.append(record);

        SendPacket(&data);

        TC_LOG_DEBUG("network", "SMSG_DB_REPLY: Sent hotfix entry: %u type: %u", entry, type);
    }
}
