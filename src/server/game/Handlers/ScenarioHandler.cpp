/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScenarioMgr.h"
#include "WorldSession.h"

void WorldSession::HandleScenarioQueryPOI(WorldPacket& recvData)
{
    size_t listSize = 0;
    listSize = recvData.ReadBits(22);

    // Read criteria tree ids and add the in a unordered_set so we don't send POIs for the same criteria tree multiple times
    std::unordered_set<int32> criteriaTreeIds(listSize);
    for (size_t i = 0; i < listSize; ++i)
    {
        int32 CriteriaTreeID = 0;
        recvData >> CriteriaTreeID;

        criteriaTreeIds.insert(CriteriaTreeID);
    }

    uint32 POICount = 0;
    uint32 POIPointsCount = 0;
    for (int32 criteriaTreeId : criteriaTreeIds)
    {
        ScenarioPOIVector const* poiVector = sScenarioMgr->GetScenarioPOIs(criteriaTreeId);
        if (poiVector)
            POICount += poiVector->size();

        if (poiVector)
            for (ScenarioPOI const& scenarioPOI : *poiVector)
                POIPointsCount += scenarioPOI.Points.size();
    }

    ByteBuffer pointsData;

    WorldPacket data(SMSG_SCENARIO_POIS, 3 + criteriaTreeIds.size() * (3 + 4) + POICount * (3 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4) + POIPointsCount * (4 + 4));

    data.WriteBits(criteriaTreeIds.size(), 21);

    for (int32 criteriaTreeId : criteriaTreeIds)
    {
        uint32 POICount = 0;

        ScenarioPOIVector const* poiVector = sScenarioMgr->GetScenarioPOIs(criteriaTreeId);
        if (poiVector)
            POICount = poiVector->size();

        data.WriteBits(POICount, 19);

        if (poiVector)
            for (ScenarioPOI const& scenarioPOI : *poiVector)
            {
                data.WriteBits(scenarioPOI.Points.size(), 21);

                pointsData << int32(scenarioPOI.BlobIndex);
                pointsData << int32(scenarioPOI.Floor);
                pointsData << int32(scenarioPOI.WorldEffectID);

                for (ScenarioPOIPoint const& scenarioPOIPoint : scenarioPOI.Points)
                {
                    pointsData << uint32(scenarioPOIPoint.Y);
                    pointsData << uint32(scenarioPOIPoint.X);
                }

                pointsData << int32(scenarioPOI.Flags);
                pointsData << int32(scenarioPOI.PlayerConditionID);
                pointsData << int32(scenarioPOI.MapID);
                pointsData << int32(scenarioPOI.Priority);
                pointsData << int32(scenarioPOI.WorldMapAreaID);
            }

        pointsData << int32(criteriaTreeId);
    }

    data.append(pointsData);

    SendPacket(&data);
}
