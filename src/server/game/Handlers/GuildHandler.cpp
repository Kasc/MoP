/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "World.h"
#include "ObjectMgr.h"
#include "GuildMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "Guild.h"
#include "GossipDef.h"
#include "SocialMgr.h"

void WorldSession::HandleGuildQueryOpcode(WorldPacket& recvData)
{
    ObjectGuid guildGuid;
    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 7, 3, 4);

    recvData.ReadGuidMask(guildGuid, 3, 4);

    recvData.ReadGuidMask(playerGuid, 2, 6);

    recvData.ReadGuidMask(guildGuid, 2, 5);

    recvData.ReadGuidMask(playerGuid, 1, 5);

    recvData.ReadGuidMask(guildGuid, 7);

    recvData.ReadGuidMask(playerGuid, 0);

    recvData.ReadGuidMask(guildGuid, 1, 6, 0);

    recvData.ReadGuidBytes(playerGuid, 7);

    recvData.ReadGuidBytes(guildGuid, 2, 4, 7);

    recvData.ReadGuidBytes(playerGuid, 6, 0);

    recvData.ReadGuidBytes(guildGuid, 6, 0, 3);

    recvData.ReadGuidBytes(playerGuid, 2);

    recvData.ReadGuidBytes(guildGuid, 5);

    recvData.ReadGuidBytes(playerGuid, 3);

    recvData.ReadGuidBytes(guildGuid, 1);

    recvData.ReadGuidBytes(playerGuid, 4, 1, 5);

    if (Guild* guild = sGuildMgr->GetGuildByGuid(guildGuid))
        if (guild->IsMember(playerGuid))
            guild->HandleQuery(this);
}

void WorldSession::HandleGuildInviteOpcode(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    std::string invitedName;

    nameLength = recvData.ReadBits(9);
    invitedName = recvData.ReadString(nameLength);

    if (normalizePlayerName(invitedName))
        if (Guild* guild = GetPlayer()->GetGuild())
            guild->HandleInviteMember(this, invitedName);
}

void WorldSession::HandleGuildRemoveOpcode(WorldPacket& recvData)
{
    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 7, 3, 4, 2, 5, 6, 1, 0);
    recvData.ReadGuidBytes(playerGuid, 0, 2, 5, 6, 7, 1, 4, 3);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleRemoveMember(this, playerGuid);
}

void WorldSession::HandleGuildAcceptOpcode(WorldPacket& /*recvData*/)
{
    if (!GetPlayer()->GetGuildId())
        if (Guild* guild = sGuildMgr->GetGuildById(GetPlayer()->GetGuildIdInvited()))
            guild->HandleAcceptMember(this);
}

void WorldSession::HandleGuildDeclineOpcode(WorldPacket& /*recvData*/)
{
    GetPlayer()->SetGuildIdInvited(0);
    GetPlayer()->SetInGuild(0);

    ObjectGuid inviterGuid = GetPlayer()->GetLastGuildInviterGUID();

    if (Player* inviter = ObjectAccessor::FindPlayer(inviterGuid))
        inviter->SendDeclineGuildInvitation(GetPlayer()->GetName());
}

void WorldSession::HandleGuildRosterOpcode(WorldPacket& recvData)
{
    recvData.rfinish();

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleRoster(this);
    else
        Guild::SendCommandResult(this, GUILD_COMMAND_ROSTER, ERR_GUILD_PLAYER_NOT_IN_GUILD);

    m_guildRosterTimer = 10 * IN_MILLISECONDS;
}

void WorldSession::HandleGuildPromoteOpcode(WorldPacket& recvData)
{
    ObjectGuid targetGuid;

    recvData.ReadGuidMask(targetGuid, 6, 0, 4, 3, 1, 7, 2, 5);
    recvData.ReadGuidBytes(targetGuid, 1, 7, 2, 5, 3, 4, 0, 6);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleUpdateMemberRank(this, targetGuid, false);
}

void WorldSession::HandleGuildDemoteOpcode(WorldPacket& recvData)
{
    ObjectGuid targetGuid;

    recvData.ReadGuidMask(targetGuid, 3, 6, 0, 2, 7, 5, 4, 1);
    recvData.ReadGuidBytes(targetGuid, 7, 4, 2, 5, 1, 3, 0, 6);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleUpdateMemberRank(this, targetGuid, true);
}

void WorldSession::HandleGuildAssignRankOpcode(WorldPacket& recvData)
{
    ObjectGuid targetGuid;

    uint32 rankId = 0;

    recvData >> rankId;

    recvData.ReadGuidMask(targetGuid, 2, 3, 1, 6, 0, 4, 7, 5);
    recvData.ReadGuidBytes(targetGuid, 7, 3, 2, 5, 6, 0, 4, 1);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetMemberRank(this, targetGuid, _player->GetGUID(), rankId);
}

void WorldSession::HandleGuildLeaveOpcode(WorldPacket& /*recvData*/)
{
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleLeaveMember(this);
}

void WorldSession::HandleGuildDisbandOpcode(WorldPacket& /*recvData*/)
{
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleDisband(this);
}

void WorldSession::HandleGuildMOTDOpcode(WorldPacket& recvData)
{
    uint32 motdLength = 0;
    std::string motd;

    motdLength = recvData.ReadBits(10);
    motd = recvData.ReadString(motdLength);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetMOTD(this, motd);
}

void WorldSession::HandleGuildSetNoteOpcode(WorldPacket& recvData)
{
    ObjectGuid playerGuid;

    uint32 noteLength = 0;

    bool IsPublic = false;

    std::string note;

    recvData.ReadGuidMask(playerGuid, 1);

    noteLength = recvData.ReadBits(8);

    recvData.ReadGuidMask(playerGuid, 4, 2);

    IsPublic = recvData.ReadBit(); // 0 == Officer, 1 == Public

    recvData.ReadGuidMask(playerGuid, 3, 5, 0, 6, 7);

    recvData.ReadGuidBytes(playerGuid, 5, 1, 6);

    note = recvData.ReadString(noteLength);

    recvData.ReadGuidBytes(playerGuid, 0, 7, 4, 3, 2);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetMemberNote(this, note, playerGuid, IsPublic);
}

void WorldSession::HandleGuildQueryRanksOpcode(WorldPacket& recvData)
{
    ObjectGuid guildGuid;

    recvData.ReadGuidMask(guildGuid, 0, 2, 5, 4, 3, 7, 6, 1);
    recvData.ReadGuidBytes(guildGuid, 6, 0, 1, 7, 3, 2, 5, 4);

    if (Guild* guild = sGuildMgr->GetGuildByGuid(guildGuid))
        if (guild->IsMember(_player->GetGUID()))
            guild->SendGuildRankInfo(this);
}

void WorldSession::HandleGuildAddRankOpcode(WorldPacket& recvData)
{
    uint32 rankId = 0;
    uint32 length = 0;

    std::string rankName;

    recvData >> rankId;

    length = recvData.ReadBits(7);
    rankName = recvData.ReadString(length);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleAddNewRank(this, rankName);
}

void WorldSession::HandleGuildDelRankOpcode(WorldPacket& recvData)
{
    uint32 rankId = 0;

    recvData >> rankId;

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleRemoveRank(this, rankId);
}

void WorldSession::HandleGuildChangeInfoTextOpcode(WorldPacket& recvData)
{
    uint32 length = 0;

    std::string info;

    length = recvData.ReadBits(11);
    info = recvData.ReadString(length);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetInfo(this, info);
}

void WorldSession::HandleSaveGuildEmblemOpcode(WorldPacket& recvData)
{
    ObjectGuid vendorGuid;

    EmblemInfo emblemInfo;
    emblemInfo.ReadPacket(recvData);

    recvData.ReadGuidMask(vendorGuid, 0, 7, 4, 6, 5, 1, 2, 3);
    recvData.ReadGuidBytes(vendorGuid, 6, 2, 7, 5, 0, 4, 1, 3);

    if (GetPlayer()->GetNPCIfCanInteractWith(vendorGuid, UNIT_NPC_FLAG_TABARDDESIGNER))
    {
        // Remove fake death
        if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
            GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

        if (!emblemInfo.ValidateEmblemColors())
        {
            Guild::SendSaveEmblemResult(this, ERR_GUILDEMBLEM_INVALID_TABARD_COLORS);
            return;
        }

        if (Guild* guild = GetPlayer()->GetGuild())
            guild->HandleSetEmblem(this, emblemInfo);
        else
            Guild::SendSaveEmblemResult(this, ERR_GUILDEMBLEM_NOGUILD); // "You are not part of a guild!";
    }
    else
        Guild::SendSaveEmblemResult(this, ERR_GUILDEMBLEM_INVALIDVENDOR); // "That's not an emblem vendor!"
}

void WorldSession::HandleGuildEventLogQueryOpcode(WorldPacket& /* recvData */)
{
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->SendEventLog(this);
}

void WorldSession::HandleGuildBankMoneyWithdrawn(WorldPacket& /* recvData */)
{
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->SendMoneyInfo(this);
}

void WorldSession::HandleGuildPermissions(WorldPacket& /* recvData */)
{
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->SendPermissions(this);
}

// Called when clicking on Guild bank gameobject
void WorldSession::HandleGuildBankerActivate(WorldPacket& recvData)
{
    ObjectGuid guid;

    bool sendAllSlots = false;

    recvData.ReadGuidMask(guid, 3);

    sendAllSlots = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 0, 7, 1, 5, 2, 6, 4);

    recvData.ReadGuidBytes(guid, 7, 1, 0, 6, 4, 2, 5, 3);

    GameObject const* const go = GetPlayer()->GetGameObjectIfCanInteractWith(guid, GAMEOBJECT_TYPE_GUILD_BANK);
    if (!go)
        return;

    Guild* const guild = GetPlayer()->GetGuild();
    if (!guild)
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_VIEW_TAB, ERR_GUILD_PLAYER_NOT_IN_GUILD);
        return;
    }

    guild->SendBankList(this, 0, sendAllSlots);
}

// Called when opening guild bank tab only (first one)
void WorldSession::HandleGuildBankQueryTab(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 tabId = 0;

    bool sendAllSlots = false;

    recvData >> tabId;

    recvData.ReadGuidMask(guid, 7, 3);

    sendAllSlots = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 0, 2, 4, 1, 6, 5);

    recvData.ReadGuidBytes(guid, 3, 7, 6, 4, 2, 5, 0, 1);

    if (GetPlayer()->GetGameObjectIfCanInteractWith(guid, GAMEOBJECT_TYPE_GUILD_BANK))
        if (Guild* guild = GetPlayer()->GetGuild())
            guild->SendBankList(this, tabId, sendAllSlots);
}

void WorldSession::HandleGuildBankDepositMoney(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint64 money = UI64LIT(0);

    recvData >> money;

    recvData.ReadGuidMask(guid, 2, 7, 6, 4, 0, 1, 5, 3);
    recvData.ReadGuidBytes(guid, 1, 4, 5, 0, 2, 7, 6, 3);

    if (GetPlayer()->GetGameObjectIfCanInteractWith(guid, GAMEOBJECT_TYPE_GUILD_BANK))
        if (money && GetPlayer()->HasEnoughMoney(money))
            if (Guild* guild = GetPlayer()->GetGuild())
                guild->HandleMemberDepositMoney(this, money);
}

void WorldSession::HandleGuildBankWithdrawMoney(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint64 money = UI64LIT(0);

    recvData >> money;

    recvData.ReadGuidMask(guid, 1, 3, 7, 6, 5, 0, 4, 2);
    recvData.ReadGuidBytes(guid, 0, 7, 4, 2, 1, 6, 3, 5);

    if (money && GetPlayer()->GetGameObjectIfCanInteractWith(guid, GAMEOBJECT_TYPE_GUILD_BANK))
        if (Guild* guild = GetPlayer()->GetGuild())
            guild->HandleMemberWithdrawMoney(this, money);
}

void WorldSession::HandleGuildBankSwapItems(WorldPacket& recvData)
{
    Guild* guild = GetPlayer()->GetGuild();
    if (!guild)
    {
        recvData.rfinish();                   // Prevent additional spam at rejected packet
        return;
    }

    ObjectGuid banker;

    uint32 stackCount = 0;
    uint32 itemId = 0;
    uint32 itemId2 = 0;
    uint32 bankItemCount = 0;

    uint8 bankSlot = 0;
    uint8 bankTab = 0;
    uint8 toSlot = 0;
    uint8 containerSlot = NULL_BAG;
    uint8 containerItemSlot = NULL_SLOT;
    uint8 bankSlot2 = 0;
    uint8 bankTab2 = 0;

    bool hasContainerSlot = false;
    bool hasContainerItemSlot = false;
    bool hasItemId2 = false;
    bool hasBankSlot2 = false;
    bool hasBankTab2 = false;
    bool hasBankItemCount = false;
    bool bankOnly = false;
    bool autoStore = false;

    recvData >> stackCount;
    recvData >> bankSlot;
    recvData >> toSlot;
    recvData >> itemId;
    recvData >> bankTab;

    recvData.ReadGuidMask(banker, 5);

    hasBankTab2 = !recvData.ReadBit();

    recvData.ReadGuidMask(banker, 1);

    hasContainerSlot = !recvData.ReadBit();
    autoStore = recvData.ReadBit();

    recvData.ReadGuidMask(banker, 0);

    hasItemId2 = !recvData.ReadBit();
    hasBankSlot2 = !recvData.ReadBit();

    recvData.ReadGuidMask(banker, 2);

    bankOnly = recvData.ReadBit();

    recvData.ReadGuidMask(banker, 4, 7, 3);

    hasContainerItemSlot = !recvData.ReadBit();

    recvData.ReadGuidMask(banker, 6);

    hasBankItemCount = !recvData.ReadBit();

    recvData.ReadGuidBytes(banker, 2, 6, 5, 4, 0, 3, 1, 7);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(banker, GAMEOBJECT_TYPE_GUILD_BANK))
    {
        recvData.rfinish();                   // Prevent additional spam at rejected packet
        return;
    }

    if (hasItemId2)
        recvData >> itemId2;                  // source item id

    if (hasContainerSlot)
        recvData >> containerSlot;

    if (hasContainerItemSlot)
        recvData >> containerItemSlot;

    if (hasBankSlot2)
        recvData >> bankSlot2;                // source bank slot

    if (hasBankItemCount)
        recvData >> bankItemCount;

    if (hasBankTab2)
        recvData >> bankTab2;                 // source bank tab

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(banker, GAMEOBJECT_TYPE_GUILD_BANK))
    {
        recvData.rfinish();                   // Prevent additional spam at rejected packet
        return;
    }

    if (bankOnly)
        guild->SwapItems(GetPlayer(), bankTab2, bankSlot2, bankTab, bankSlot, stackCount);
    else
    {
        if (!Player::IsInventoryPos(containerSlot, containerItemSlot) && !(containerSlot == NULL_BAG && containerItemSlot == NULL_SLOT))
            GetPlayer()->SendEquipError(EQUIP_ERR_INTERNAL_BAG_ERROR, NULL);
        else
            guild->SwapItemsWithInventory(GetPlayer(), toSlot != 0, bankTab, bankSlot, containerSlot, containerItemSlot, stackCount);
    }
}

void WorldSession::HandleGuildBankBuyTab(WorldPacket& recvData)
{
    uint8 tabId = 0;

    ObjectGuid guid;

    recvData >> tabId;

    recvData.ReadGuidMask(guid, 0, 1, 3, 7, 2, 6, 5, 4);
    recvData.ReadGuidBytes(guid, 1, 4, 6, 7, 3, 5, 2, 0);

    // Since Cata you can buy tabs from the guild constrol tab - no need to check for guid
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleBuyBankTab(this, tabId);
}

void WorldSession::HandleGuildBankUpdateTab(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint32 iconLen = 0;
    uint32 nameLen = 0;

    std::string name;
    std::string icon;

    uint8 tabId = 0;

    recvData >> tabId;

    recvData.ReadGuidMask(guid, 5);

    iconLen = recvData.ReadBits(9);

    recvData.ReadGuidMask(guid, 1, 4, 2, 7, 0, 6, 3);

    nameLen = recvData.ReadBits(7);

    recvData.ReadGuidBytes(guid, 7, 4);

    icon = recvData.ReadString(iconLen);

    recvData.ReadGuidBytes(guid, 5, 1, 0);

    name = recvData.ReadString(nameLen);

    recvData.ReadGuidBytes(guid, 2, 3, 6);

    if (!name.empty() && !icon.empty())
        if (GetPlayer()->GetGameObjectIfCanInteractWith(guid, GAMEOBJECT_TYPE_GUILD_BANK))
            if (Guild* guild = GetPlayer()->GetGuild())
                guild->HandleSetBankTabInfo(this, tabId, name, icon);
}

void WorldSession::HandleGuildBankLogQuery(WorldPacket& recvData)
{
    uint32 tabId = 0;

    recvData >> tabId;

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->SendBankLog(this, tabId);
}

void WorldSession::HandleQueryGuildBankTabText(WorldPacket &recvData)
{
    uint32 tabId = 0;

    recvData >> tabId;

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->SendBankTabText(this, tabId);
}

void WorldSession::HandleSetGuildBankTabText(WorldPacket& recvData)
{
    uint32 tabId = 0;
    uint32 textLen = 0;

    std::string text;

    recvData >> tabId;

    textLen = recvData.ReadBits(14);
    text = recvData.ReadString(textLen);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->SetBankTabText(tabId, text);
}

void WorldSession::HandleGuildQueryXPOpcode(WorldPacket& recvData)
{
    ObjectGuid guildGuid;

    recvData.ReadGuidMask(guildGuid, 5, 6, 0, 1, 3, 7, 4, 2);
    recvData.ReadGuidBytes(guildGuid, 4, 6, 3, 0, 7, 5, 2, 1);

    if (Guild* guild = sGuildMgr->GetGuildByGuid(guildGuid))
        if (guild->IsMember(_player->GetGUID()))
            guild->SendGuildXP(this);
}

void WorldSession::HandleGuildSetRankPermissionsOpcode(WorldPacket& recvData)
{
    Guild* guild = GetPlayer()->GetGuild();
    if (!guild)
    {
        recvData.rfinish();
        return;
    }

    uint32 oldRankId = 0;
    uint32 newRankId = 0;
    uint32 oldRights = 0;
    uint32 newRights = 0;
    uint32 moneyPerDay = 0;
    uint32 nameLength = 0;

    std::string rankName;

    recvData >> oldRankId;

    GuildBankRightsAndSlotsVec rightsAndSlots(GUILD_BANK_MAX_TABS);
    for (uint8 tabId = 0; tabId < GUILD_BANK_MAX_TABS; ++tabId)
    {
        uint32 bankRights = 0;
        uint32 slots = 0;

        recvData >> bankRights;
        recvData >> slots;

        rightsAndSlots[tabId] = GuildBankRightsAndSlots(tabId, uint8(bankRights), slots);
    }

    recvData >> moneyPerDay;
    recvData >> oldRights;
    recvData >> newRights;
    recvData >> newRankId;

    nameLength = recvData.ReadBits(7);
    rankName = recvData.ReadString(nameLength);

    guild->HandleSetRankInfo(this, newRankId, rankName, newRights, moneyPerDay, rightsAndSlots);
}

void WorldSession::HandleGuildRequestPartyState(WorldPacket& recvData)
{
    ObjectGuid guildGuid;

    recvData.ReadGuidMask(guildGuid, 1, 5, 7, 2, 6, 3, 0, 4);
    recvData.ReadGuidBytes(guildGuid, 2, 5, 4, 6, 1, 0, 7, 3);

    if (Guild* guild = sGuildMgr->GetGuildByGuid(guildGuid))
        guild->HandleGuildPartyRequest(this);
}

void WorldSession::HandleAutoDeclineGuildInvites(WorldPacket& recvData)
{
    bool enable = false;
    
    enable = recvData.ReadBit();

    GetPlayer()->ApplyModFlag(PLAYER_FLAGS, PLAYER_FLAGS_AUTO_DECLINE_GUILD, enable);
}

void WorldSession::HandleGuildRewardsQueryOpcode(WorldPacket& recvData)
{
    recvData.read_skip<uint32>(); // Unk

    if (sGuildMgr->GetGuildById(_player->GetGuildId()))
    {
        std::vector<GuildReward> const& rewards = sGuildMgr->GetGuildRewards();

        uint32 achievSize = 0;
        for (uint32 i = 0; i < rewards.size(); ++i)
            achievSize += rewards[i].AchievementsRequired.size();

        WorldPacket data(SMSG_GUILD_REWARDS_LIST, 3 + 4 + rewards.size() * (3 + 4 + 4 + 4 + 4 + 8) + achievSize * (4));

        data.WriteBits(rewards.size(), 19);

        for (uint32 i = 0; i < rewards.size(); i++)
            data.WriteBits(rewards[i].AchievementsRequired.size(), 22);

        data.FlushBits();

        for (uint32 i = 0; i < rewards.size(); i++)
        {
            for (uint32 j = 0; j < rewards[i].AchievementsRequired.size(); j++)
                data << uint32(rewards[i].AchievementsRequired[j]);

            data << int32(rewards[i].RaceMask);
            data << uint32(rewards[i].ItemID);
            data << uint32(0); // minGuildLevel
            data << uint32(rewards[i].MinGuildRep);
            data << uint64(rewards[i].Cost);
        }

        data << uint32(time(nullptr));

        SendPacket(&data);
    }
}

void WorldSession::HandleGuildQueryNewsOpcode(WorldPacket& recvData)
{
    ObjectGuid guildGUID;

    recvData.ReadGuidMask(guildGUID, 7, 4, 2, 6, 0, 5, 3, 1);
    recvData.ReadGuidBytes(guildGUID, 3, 2, 7, 0, 5, 4, 1, 6);

    if (Guild* guild = GetPlayer()->GetGuild())
        if (guild->GetGUID() == guildGUID)
            guild->SendNewsUpdate(this);
}

void WorldSession::HandleSwapRanks(WorldPacket& recvData)
{
    uint32 id = 0;
    bool up = false;

    recvData >> id;

    up = recvData.ReadBit();

    if (Player* player = GetPlayer())
        if (Guild* guild = player->GetGuild())
            guild->HandleSwapRanks(this, id, up);
}

void WorldSession::HandleGuildNewsUpdateStickyOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint32 newsId = 0;

    bool sticky = false;

    recvData >> newsId;

    recvData.ReadGuidMask(guid, 6, 0);

    sticky = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 2, 7, 5, 4, 3, 1);

    recvData.ReadGuidBytes(guid, 5, 4, 0, 1, 6, 2, 3, 7);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleNewsSetSticky(this, newsId, sticky);
}

void WorldSession::HandleGuildSetGuildMaster(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    std::string playerName;

    nameLength = recvData.ReadBits(9);
    playerName = recvData.ReadString(nameLength);

    normalizePlayerName(playerName);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetNewGuildMaster(this, playerName);
}

void WorldSession::HandleGuildSetAchievementTracking(WorldPacket& recvData)
{
    uint32 count = 0;

    count = recvData.ReadBits(22);

    std::set<uint32> achievementIds;
    
    for (uint32 i = 0; i < count; ++i)
    {
        uint32 achievementId = 0;

        recvData >> achievementId;

        achievementIds.insert(achievementId);
    }

    if (achievementIds.empty())
        return;
    
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetAchievementTracking(this, achievementIds);
}

void WorldSession::HandleGuildRequestChallengeUpdate(WorldPacket& recvData)
{
    WorldPacket data(SMSG_GUILD_CHALLENGE_UPDATED, 4 * 6 * 5);
    for (int i = 0; i < 6; i++)
        data << uint32(GuildChallengeWeeklyMaximum[i]);

    for (int i = 0; i < 6; i++)
        data << uint32(GuildChallengeGoldReward[i]);

    for (int i = 0; i < 6; i++)
        data << uint32(GuildChallengeMaxLevelGoldReward[i]);

    for (int i = 0; i < 6; i++)
        data << uint32(GuildChallengeXPReward[i]);

    for (int i = 0; i < 6; i++)
        data << uint32(0); // Progress - NYI
    SendPacket(&data);
}

void WorldSession::HandleGuildBankTabNote(WorldPacket& recvData)
{
    uint32 tabId = 0;
    uint32 noteLength = 0;

    std::string note;

    recvData >> tabId;

    noteLength = recvData.ReadBits(14);

    note = recvData.ReadString(noteLength);

    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleSetBankTabNote(this, tabId, note);
}

void WorldSession::HandleGuildAchievementMembersOpcode(WorldPacket& recvData)
{
    uint32 achievementId = 0;

    ObjectGuid guildGuid;
    ObjectGuid playerGuid;

    recvData >> achievementId;

    recvData.ReadGuidMask(guildGuid, 4);

    recvData.ReadGuidMask(playerGuid, 2);

    recvData.ReadGuidMask(guildGuid, 0);

    recvData.ReadGuidMask(playerGuid, 6, 0);

    recvData.ReadGuidMask(guildGuid, 5);

    recvData.ReadGuidMask(playerGuid, 7);

    recvData.ReadGuidMask(guildGuid, 6, 1);

    recvData.ReadGuidMask(playerGuid, 1, 5);

    recvData.ReadGuidMask(guildGuid, 3);

    recvData.ReadGuidMask(playerGuid, 4);

    recvData.ReadGuidMask(guildGuid, 2);

    recvData.ReadGuidMask(playerGuid, 3);

    recvData.ReadGuidMask(guildGuid, 7);

    recvData.ReadGuidBytes(guildGuid, 5, 1, 6, 3, 7);

    recvData.ReadGuidBytes(playerGuid, 1, 2, 7, 5, 0);

    recvData.ReadGuidBytes(guildGuid, 2);

    recvData.ReadGuidBytes(playerGuid, 4);

    recvData.ReadGuidBytes(guildGuid, 0);

    recvData.ReadGuidBytes(playerGuid, 6, 3);

    recvData.ReadGuidBytes(guildGuid, 4);

    if (Guild* guild = sGuildMgr->GetGuildByGuid(guildGuid))
    {
        if (guild->GetAchievementMgr().HasAchieved(achievementId))
        {
            CompletedAchievementData const* achievement = guild->GetAchievementMgr().GetCompletedDataForAchievement(achievementId);
            if (!achievement)
                return;

            if (achievement->DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
                return;

            ByteBuffer byteData;

            WorldPacket data(SMSG_ACHIEVEMENT_GUILD_MEMBERS, 1 + 8 + 4 + achievement->CompletingPlayers.size() * (1 + 8) + 4);

            data.WriteBits(achievement->CompletingPlayers.size(), 24);

            for (ObjectGuid guid : achievement->CompletingPlayers)
            {
                data.WriteGuidMask(guid, 6, 7, 3, 0, 1, 4, 5, 2);
                byteData.WriteGuidBytes(guid, 1, 7, 4, 6, 5, 0, 3, 2);
            }

            data.WriteGuidMask(guildGuid, 5, 6, 2, 7, 0, 4, 3, 1);

            data.FlushBits();

            data.WriteGuidBytes(guildGuid, 1, 5);

            data.append(byteData);

            data.WriteGuidBytes(guildGuid, 3);

            data << uint32(achievementId);

            data.WriteGuidBytes(guildGuid, 7, 4, 2, 6, 0);

            SendPacket(&data);
        }
    }
}

void WorldSession::HandleGuildAchievementProgressQuery(WorldPacket& recvData)
{
    uint32 achievementId = 0;

    recvData >> achievementId;

    if (Guild* guild = _player->GetGuild())
        guild->GetAchievementMgr().SendAchievementInfo(_player, achievementId);
}

void WorldSession::HandleGuildDethroneGuildMasterOpcode(WorldPacket& /*recvData*/)
{
    if (Guild* guild = GetPlayer()->GetGuild())
        guild->HandleDethroneGuildMaster(this);
}

void WorldSession::HandleGuildQueryRecipes(WorldPacket& recvData)
{
    ObjectGuid guildGUID;

    recvData.ReadGuidMask(guildGUID, 7, 4, 0, 3, 2, 5, 6, 1);
    recvData.ReadGuidBytes(guildGUID, 1, 0, 6, 5, 2, 4, 7, 3);

    Guild* guild = GetPlayer()->GetGuild();
    if (!guild)
        return;

    if (guildGUID != guild->GetGUID())
        return;
    
    guild->SendGuildKnowRecipes(this);
}

void WorldSession::HandleGuildQueryMemberRecipes(WorldPacket& recvData)
{
    uint32 SkillLineID = 0;

    ObjectGuid guildGUID;
    ObjectGuid memberGUID;

    recvData >> SkillLineID;

    recvData.ReadGuidMask(guildGUID, 6, 7);

    recvData.ReadGuidMask(memberGUID, 3);

    recvData.ReadGuidMask(guildGUID, 1, 2, 4);

    recvData.ReadGuidMask(memberGUID, 4, 1);

    recvData.ReadGuidMask(guildGUID, 0 ,3);

    recvData.ReadGuidMask(memberGUID, 5);

    recvData.ReadGuidMask(guildGUID, 5);

    recvData.ReadGuidMask(memberGUID, 0, 7, 6, 2);

    recvData.ReadGuidBytes(guildGUID, 6);

    recvData.ReadGuidBytes(memberGUID, 5, 4, 3, 2);

    recvData.ReadGuidBytes(guildGUID, 7);

    recvData.ReadGuidBytes(memberGUID, 6);

    recvData.ReadGuidBytes(guildGUID, 2);

    recvData.ReadGuidBytes(memberGUID, 0, 7);

    recvData.ReadGuidBytes(guildGUID, 1, 0, 5);

    recvData.ReadGuidBytes(memberGUID, 1);

    recvData.ReadGuidBytes(guildGUID, 4, 3);

    if (!SkillLineID)
        return;

    Guild* guild = GetPlayer()->GetGuild();
    if (!guild)
        return;

    if (guildGUID != guild->GetGUID())
        return;

    if (!guild->IsMember(memberGUID))
        return;

    guild->SendGuildMemberRecipes(this, memberGUID, SkillLineID);
}

void WorldSession::HandleGuildQueryMembersForRecipe(WorldPacket& recvData)
{
    ObjectGuid guildGUID;

    uint32 SkillSpellID = 0;
    uint32 SkillRank = 0;
    uint32 SkillLineID = 0;

    recvData >> SkillSpellID;
    recvData >> SkillRank;
    recvData >> SkillLineID;

    recvData.ReadGuidMask(guildGUID, 6, 7, 3, 2, 5, 1, 0, 4);
    recvData.ReadGuidBytes(guildGUID, 0, 3, 2, 5, 6, 7, 4, 1);

    Guild* guild = GetPlayer()->GetGuild();
    if (!guild)
        return;

    if (guildGUID != guild->GetGUID())
        return;

    guild->SendGuildMembersForRecipe(this, SkillSpellID, SkillLineID);
}

void WorldSession::HandleGuildRenameRequest(WorldPacket& recvData)
{
    uint32 lenght = 0;
    std::string newName;

    lenght = recvData.ReadBits(7);
    newName = recvData.ReadString(lenght);

    Player* player = GetPlayer();
    if (!player)
        return;

    Guild* guild = player->GetGuild();
    if (!guild)
        return;

    if (!guild->IsAllowedToChangeGuildName(player->GetGUID()))
    {
        WorldPacket data(SMSG_GUILD_CHANGE_NAME_RESULT, 1);

        data.WriteBit(false);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    std::string oldName = guild->GetName();

    if (oldName == newName || sObjectMgr->IsReservedName(newName) || !ObjectMgr::IsValidCharterName(newName))
    {
        WorldPacket data(SMSG_GUILD_CHANGE_NAME_RESULT, 1);

        data.WriteBit(false);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GUILD_FREE_NAME);

    stmt->setUInt32(0, guild->GetId());
    stmt->setString(1, newName);

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
        .WithPreparedCallback(std::bind(&WorldSession::HandleGuildRenameCallback, this, newName, std::placeholders::_1)));
}

void WorldSession::HandleGuildRenameCallback(std::string const& newName, PreparedQueryResult result)
{
    if (!result)
    {
        WorldPacket data(SMSG_GUILD_CHANGE_NAME_RESULT, 1);

        data.WriteBit(false);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    Field* fields = result->Fetch();

    uint32 guildID = fields[0].GetUInt32();

    Guild* guild = sGuildMgr->GetGuildById(guildID);
    if (!guild)
    {
        WorldPacket data(SMSG_GUILD_CHANGE_NAME_RESULT, 1);

        data.WriteBit(false);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    WorldPacket data(SMSG_GUILD_CHANGE_NAME_RESULT, 1);

    data.WriteBit(true);

    data.FlushBits();

    SendPacket(&data);

    guild->SetName(newName);
}
