/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "DatabaseEnv.h"
#include "Mail.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "World.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "Language.h"
#include "DBCStores.h"
#include "Item.h"
#include "AccountMgr.h"
#include "GuildMgr.h"
#include "CharacterCache.h"

void WorldSession::HandleSendMail(WorldPacket& recvData)
{
    ObjectGuid mailbox;
    uint64 money = UI64LIT(0);
    uint64 COD = UI64LIT(0);

    std::string receiverName;
    std::string subject;
    std::string body;

    uint32 bodyLength = 0;
    uint32 subjectLength = 0;
    uint32 receiverLength = 0;
    uint32 StationaryID = 0;
    uint32 PackageID = 0;
    uint8 itemCount = 0;

    recvData >> StationaryID;
    recvData >> PackageID;
    recvData >> COD;
    recvData >> money;

    recvData.ReadGuidMask(mailbox, 0, 6, 4, 1);

    bodyLength = recvData.ReadBits(11);

    recvData.ReadGuidMask(mailbox, 3);

    receiverLength = recvData.ReadBits(9);

    recvData.ReadGuidMask(mailbox, 7, 5);

    itemCount = recvData.ReadBits(5);               // attached items count

    if (itemCount > MAX_MAIL_ITEMS)                 // client limit
    {
        GetPlayer()->SendMailResult(0, MAIL_SEND, MAIL_ERR_TOO_MANY_ATTACHMENTS);
        recvData.rfinish();                         // set to end to avoid warnings spam
        return;
    }

    GuidVector itemGuids(itemCount);
    for (uint8 i = 0; i < itemCount; ++i)
        recvData.ReadGuidMask(itemGuids[i], 1, 7, 2, 5, 0, 6, 3, 4);

    subjectLength = recvData.ReadBits(9);

    recvData.ReadGuidMask(mailbox, 2);

    for (uint8 i = 0; i < itemCount; ++i)
    {
        // item slot in mail, not used
        uint8 ItemSlot = 0;
        recvData >> ItemSlot;

        recvData.ReadGuidBytes(itemGuids[i], 3, 0, 2, 1, 6, 5, 7, 4);
    }

    recvData.ReadGuidBytes(mailbox, 1);

    body = recvData.ReadString(bodyLength);

    recvData.ReadGuidBytes(mailbox, 0);

    subject = recvData.ReadString(subjectLength);

    recvData.ReadGuidBytes(mailbox, 2, 6, 5, 7, 3, 4);

    receiverName = recvData.ReadString(receiverLength);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    if (receiverName.empty())
        return;

    Player* player = _player;

    if (player->GetLevel() < sWorld->getIntConfig(CONFIG_MAIL_LEVEL_REQ))
    {
        SendNotification(GetTrinityString(LANG_MAIL_SENDER_REQ), sWorld->getIntConfig(CONFIG_MAIL_LEVEL_REQ));
        return;
    }

    ObjectGuid receiverGuid;
    if (normalizePlayerName(receiverName))
        receiverGuid = sCharacterCache->GetCharacterGuidByName(receiverName);

    if (!receiverGuid)
    {
        TC_LOG_DEBUG("network", "Player %u is sending mail to %s (GUID: not existed!) with subject %s "
            "and body %s includes %u items, " UI64FMTD " copper and " UI64FMTD " COD copper with StationaryID = %u, PackageID = %u",
            player->GetGUID().GetCounter(), receiverName.c_str(), subject.c_str(), body.c_str(),
            itemCount, money, COD, StationaryID, PackageID);
        player->SendMailResult(0, MAIL_SEND, MAIL_ERR_RECIPIENT_NOT_FOUND);
        return;
    }

    if (player->GetGUID() == receiverGuid)
    {
        player->SendMailResult(0, MAIL_SEND, MAIL_ERR_CANNOT_SEND_TO_SELF);
        return;
    }

    uint32 cost = itemCount ? 30 * itemCount : 30;  // price hardcoded in client

    uint64 reqmoney = cost + money;

    // Check for overflow
    if (reqmoney < money)
    {
        player->SendMailResult(0, MAIL_SEND, MAIL_ERR_NOT_ENOUGH_MONEY);
        return;
    }

    if (!player->HasEnoughMoney(reqmoney) && !player->IsGameMaster())
    {
        player->SendMailResult(0, MAIL_SEND, MAIL_ERR_NOT_ENOUGH_MONEY);
        return;
    }

    Player* receiver = ObjectAccessor::FindConnectedPlayer(receiverGuid);

    uint32 receiverTeam = 0;
    uint8 mailsCount = 0;                                  //do not allow to send to one player more than 100 mails
    uint8 receiverLevel = 0;
    uint32 receiverAccountId = 0;

    if (receiver)
    {
        receiverTeam = receiver->GetTeam();
        mailsCount = receiver->GetMailSize();
        receiverLevel = receiver->GetLevel();
        receiverAccountId = receiver->GetSession()->GetAccountId();
    }
    else
    {
        if (CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(receiverGuid))
        {
            receiverTeam = Player::TeamForRace(characterInfo->Race);
            receiverLevel = characterInfo->Level;
            receiverAccountId = characterInfo->AccountId;
        }

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MAIL_COUNT);
        stmt->setUInt32(0, receiverGuid.GetCounter());

        PreparedQueryResult result = CharacterDatabase.Query(stmt);
        if (result)
        {
            Field* fields = result->Fetch();
            mailsCount = fields[0].GetUInt64();
        }
    }

    // do not allow to have more than 100 mails in mailbox.. mails count is in opcode uint8!!! - so max can be 255..
    if (mailsCount > 100)
    {
        player->SendMailResult(0, MAIL_SEND, MAIL_ERR_RECIPIENT_CAP_REACHED);
        return;
    }

    // test the receiver's Faction... or all items are account bound
    bool accountBound = itemCount ? true : false;
    for (uint8 i = 0; i < itemCount; ++i)
    {
        if (Item* item = player->GetItemByGuid(itemGuids[i]))
        {
            ItemTemplate const* itemProto = item->GetTemplate();
            if (!itemProto || !(itemProto->GetFlags() & ITEM_PROTO_FLAG_IS_BOUND_TO_ACCOUNT))
            {
                accountBound = false;
                break;
            }
        }
    }

    if (!accountBound && player->GetTeam() != receiverTeam && !HasPermission(rbac::RBAC_PERM_TWO_SIDE_INTERACTION_MAIL))
    {
        player->SendMailResult(0, MAIL_SEND, MAIL_ERR_NOT_YOUR_TEAM);
        return;
    }

    if (receiverLevel < sWorld->getIntConfig(CONFIG_MAIL_LEVEL_REQ))
    {
        SendNotification(GetTrinityString(LANG_MAIL_RECEIVER_REQ), sWorld->getIntConfig(CONFIG_MAIL_LEVEL_REQ));
        return;
    }

    std::vector<Item*> items;

    for (uint8 i = 0; i < itemCount; ++i)
    {
        if (!itemGuids[i])
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_MAIL_ATTACHMENT_INVALID);
            return;
        }

        Item* item = player->GetItemByGuid(itemGuids[i]);

        // prevent sending bag with items (cheat: can be placed in bag after adding equipped empty bag to mail)
        if (!item)
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_MAIL_ATTACHMENT_INVALID);
            return;
        }

        if (!item->CanBeTraded(true))
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_EQUIP_ERROR, EQUIP_ERR_MAIL_BOUND_ITEM);
            return;
        }

        if (item->IsBoundAccountWide() && item->IsSoulBound() && player->GetSession()->GetAccountId() != receiverAccountId)
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_EQUIP_ERROR, EQUIP_ERR_NOT_SAME_ACCOUNT);
            return;
        }

        if (item->GetTemplate()->GetFlags() & ITEM_PROTO_FLAG_CONJURED || item->GetUInt32Value(ITEM_EXPIRATION))
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_EQUIP_ERROR, EQUIP_ERR_MAIL_BOUND_ITEM);
            return;
        }

        if (COD && item->HasFlag(ITEM_FLAGS, ITEM_FLAG_WRAPPED))
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_CANT_SEND_WRAPPED_COD);
            return;
        }

        if (item->IsNotEmptyBag())
        {
            player->SendMailResult(0, MAIL_SEND, MAIL_ERR_EQUIP_ERROR, EQUIP_ERR_DESTROY_NONEMPTY_BAG);
            return;
        }

        items.push_back(item);
    }

    player->SendMailResult(0, MAIL_SEND, MAIL_OK);

    player->ModifyMoney(-int64(reqmoney));
    player->UpdateCriteria(CRITERIA_TYPE_GOLD_SPENT_FOR_MAIL, cost);

    bool needItemDelay = false;

    MailDraft draft(subject, body);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    if (itemCount > 0 || money > 0)
    {
        bool log = HasPermission(rbac::RBAC_PERM_LOG_GM_TRADE);
        if (itemCount > 0)
        {
            for (Item* item : items)
            {
                if (log)
                {
                    sLog->outCommand(GetAccountId(), "GM %s (GUID: %u) (Account: %u) mail item: %s (Entry: %u Count: %u) "
                        "to: %s (%s) (Account: %u)", GetPlayerName().c_str(), GetGUIDLow(), GetAccountId(),
                        item->GetTemplate()->GetDefaultLocaleName(), item->GetEntry(), item->GetCount(),
                        receiverName.c_str(), receiverGuid.ToString().c_str(), receiverAccountId);
                }

                item->SetNotRefundable(GetPlayer()); // makes the item no longer refundable
                player->MoveItemFromInventory(item->GetBagSlot(), item->GetSlot(), true);

                item->DeleteFromInventoryDB(trans);     // deletes item from character's inventory
                item->SetOwnerGUID(receiverGuid);
                item->SetState(ITEM_CHANGED);
                item->SaveToDB(trans);                  // recursive and not have transaction guard into self, item not in inventory and can be save standalone

                draft.AddItem(item);
            }

            // if item send to character at another account, then apply item delivery delay
            needItemDelay = player->GetSession()->GetAccountId() != receiverAccountId;
        }

        if (log && money > 0)
        {
            sLog->outCommand(GetAccountId(), "GM %s (GUID: %u) (Account: %u) mail money: %u to: %s (%s) (Account: %u)",
                GetPlayerName().c_str(), GetGUIDLow(), GetAccountId(), money, receiverName.c_str(), receiverGuid.ToString().c_str(), receiverAccountId);
        }
    }

    // If theres is an item, there is a one hour delivery delay if sent to another account's character.
    uint32 deliver_delay = needItemDelay ? sWorld->getIntConfig(CONFIG_MAIL_DELIVERY_DELAY) : 0;

    // Mail sent between guild members arrives instantly if they have the guild perk "Guild Mail"
    if (Guild* guild = sGuildMgr->GetGuildById(player->GetGuildId()))
        if (guild->GetLevel() >= 17 && guild->IsMember(receiverGuid))
            deliver_delay = 0;

    // don't ask for COD if there are no items
    if (itemCount == 0)
        COD = 0;

    // will delete item or place to receiver mail list
    draft
        .AddMoney(money)
        .AddCOD(COD)
        .SendMailTo(trans, MailReceiver(receiver, receiverGuid.GetCounter()), MailSender(player), body.empty() ? MAIL_CHECK_MASK_COPIED : MAIL_CHECK_MASK_HAS_BODY, deliver_delay);

    player->SaveInventoryAndGoldToDB(trans);
    CharacterDatabase.CommitTransaction(trans);
}

//called when mail is read
void WorldSession::HandleMailMarkAsRead(WorldPacket& recvData)
{
    ObjectGuid mailbox;

    uint32 mailId = 0;

    bool BiReceipt = false;

    recvData >> mailId;

    recvData.ReadGuidMask(mailbox, 0, 2, 3);

    BiReceipt = recvData.ReadBit();

    recvData.ReadGuidMask(mailbox, 4, 6, 7, 1, 5);

    recvData.ReadGuidBytes(mailbox, 1, 7, 2, 5, 6, 3, 4, 0);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    Mail* mail = _player->GetMail(mailId);
    if (mail && mail->state != MAIL_STATE_DELETED)
    {
        if (_player->unReadMails)
            --_player->unReadMails;

        mail->checked |= MAIL_CHECK_MASK_READ;
        mail->state = MAIL_STATE_CHANGED;

        _player->m_mailsUpdated = true;
    }
}

//called when client deletes mail
void WorldSession::HandleMailDelete(WorldPacket& recvData)
{
    uint32 mailId = 0;
    uint32 mailTemplateId = 0;

    recvData >> mailId;
    recvData >> mailTemplateId;

    if (Mail* mail = _player->GetMail(mailId))
    {
        // delete shouldn't show up for COD mails
        if (mail->COD)
        {
            _player->SendMailResult(mailId, MAIL_DELETED, MAIL_ERR_INTERNAL_ERROR);
            return;
        }

        mail->state = MAIL_STATE_DELETED;
    }

    _player->m_mailsUpdated = true;
    _player->SendMailResult(mailId, MAIL_DELETED, MAIL_OK);
}

void WorldSession::HandleMailReturnToSender(WorldPacket& recvData)
{
    ObjectGuid mailbox;

    uint32 mailId = 0;

    recvData >> mailId;

    recvData.ReadGuidMask(mailbox, 2, 0, 4, 6, 3, 1, 7, 5);
    recvData.ReadGuidBytes(mailbox, 5, 6, 2, 0, 3, 1, 4, 7);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    Player* player = _player;
    Mail* m = player->GetMail(mailId);
    if (!m || m->state == MAIL_STATE_DELETED || m->deliver_time > time(NULL))
    {
        player->SendMailResult(mailId, MAIL_RETURNED_TO_SENDER, MAIL_ERR_INTERNAL_ERROR);
        return;
    }

    //we can return mail now, so firstly delete the old one
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_MAIL_BY_ID);
    stmt->setUInt32(0, mailId);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_MAIL_ITEM_BY_ID);
    stmt->setUInt32(0, mailId);
    trans->Append(stmt);

    player->RemoveMail(mailId);

    // only return mail if the player exists (and delete if not existing)
    if (m->messageType == MAIL_NORMAL && m->sender)
    {
        MailDraft draft(m->subject, m->body);
        if (m->mailTemplateId)
            draft = MailDraft(m->mailTemplateId, false);     // items already included

        if (m->HasItems())
        {
            for (MailItemInfoVec::iterator itr2 = m->items.begin(); itr2 != m->items.end(); ++itr2)
            {
                if (Item* const item = player->GetMItem(itr2->item_guid))
                    draft.AddItem(item);
                player->RemoveMItem(itr2->item_guid);
            }
        }

        draft.AddMoney(m->money).SendReturnToSender(GetAccountId(), m->receiver, m->sender, trans);
    }

    CharacterDatabase.CommitTransaction(trans);

    delete m;                                               //we can deallocate old mail
    player->SendMailResult(mailId, MAIL_RETURNED_TO_SENDER, MAIL_OK);
}

//called when player takes item attached in mail
void WorldSession::HandleMailTakeItem(WorldPacket& recvData)
{
    ObjectGuid mailbox;

    uint32 mailId = 0;
    uint32 itemId = 0;

    recvData >> mailId;
    recvData >> itemId;

    recvData.ReadGuidMask(mailbox, 6, 5, 2, 3, 0, 1, 4, 7);
    recvData.ReadGuidBytes(mailbox, 0, 1, 4, 2, 5, 6, 3, 7);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    Player* player = _player;

    Mail* m = player->GetMail(mailId);
    if (!m || m->state == MAIL_STATE_DELETED || m->deliver_time > time(NULL))
    {
        player->SendMailResult(mailId, MAIL_ITEM_TAKEN, MAIL_ERR_INTERNAL_ERROR);
        return;
    }

    // verify that the mail has the item to avoid cheaters taking COD items without paying
    if (std::find_if(m->items.begin(), m->items.end(), [itemId](MailItemInfo info){ return info.item_guid == itemId; }) == m->items.end())
    {
        player->SendMailResult(mailId, MAIL_ITEM_TAKEN, MAIL_ERR_INTERNAL_ERROR);
        return;
    }

    // prevent cheating with skip client money check
    if (!player->HasEnoughMoney(m->COD))
    {
        player->SendMailResult(mailId, MAIL_ITEM_TAKEN, MAIL_ERR_NOT_ENOUGH_MONEY);
        return;
    }

    Item* it = player->GetMItem(itemId);

    ItemPosCountVec dest;
    uint8 msg = _player->CanStoreItem(NULL_BAG, NULL_SLOT, dest, it, false);
    if (msg == EQUIP_ERR_OK)
    {
        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        m->RemoveItem(itemId);
        m->removedItems.push_back(itemId);

        if (m->COD > 0)                                     //if there is COD, take COD money from player and send them to sender by mail
        {
            ObjectGuid sender_guid = ObjectGuid::Create<HighGuid::Player>(m->sender);
            Player* receiver = ObjectAccessor::FindConnectedPlayer(sender_guid);

            uint32 sender_accId = 0;

            if (HasPermission(rbac::RBAC_PERM_LOG_GM_TRADE))
            {
                std::string sender_name;
                if (receiver)
                {
                    sender_accId = receiver->GetSession()->GetAccountId();
                    sender_name = receiver->GetName();
                }
                else
                {
                    // can be calculated early
                    sender_accId = sCharacterCache->GetCharacterAccountIdByGuid(sender_guid);

                    if (!sCharacterCache->GetCharacterNameByGuid(sender_guid, sender_name))
                        sender_name = sObjectMgr->GetTrinityStringForDBCLocale(LANG_UNKNOWN);
                }

                sLog->outCommand(GetAccountId(), "GM %s (Account: %u) receiver mail item: %s (Entry: %u Count: %u) and send COD money: " UI64FMTD " to player: %s (Account: %u)",
                    GetPlayerName().c_str(), GetAccountId(), it->GetTemplate()->GetDefaultLocaleName(), it->GetEntry(), it->GetCount(), m->COD, sender_name.c_str(), sender_accId);
            }
            else if (!receiver)
                sender_accId = sCharacterCache->GetCharacterAccountIdByGuid(sender_guid);

            // check player existence
            if (receiver || sender_accId)
            {
                MailDraft(m->subject, "")
                    .AddMoney(m->COD)
                    .SendMailTo(trans, MailReceiver(receiver, m->sender), MailSender(MAIL_NORMAL, m->receiver), MAIL_CHECK_MASK_COD_PAYMENT);
            }

            player->ModifyMoney(-int64(m->COD));
        }

        m->COD = 0;
        m->state = MAIL_STATE_CHANGED;
        player->m_mailsUpdated = true;
        player->RemoveMItem(it->GetGUID().GetCounter());

        uint32 count = it->GetCount();                      // save counts before store and possible merge with deleting
        it->SetState(ITEM_UNCHANGED);                       // need to set this state, otherwise item cannot be removed later, if neccessary
        player->MoveItemToInventory(dest, it, true);

        player->SaveInventoryAndGoldToDB(trans);
        player->_SaveMail(trans);
        CharacterDatabase.CommitTransaction(trans);

        player->SendMailResult(mailId, MAIL_ITEM_TAKEN, MAIL_OK, 0, itemId, count);
    }
    else
        player->SendMailResult(mailId, MAIL_ITEM_TAKEN, MAIL_ERR_EQUIP_ERROR, msg);
}

void WorldSession::HandleMailTakeMoney(WorldPacket& recvData)
{
    ObjectGuid mailbox;

    uint64 money = UI64LIT(0);
    uint32 mailId = 0;

    recvData >> mailId;
    recvData >> money;

    recvData.ReadGuidMask(mailbox, 7, 6, 3, 2, 4, 5, 0, 1);
    recvData.ReadGuidBytes(mailbox, 7, 1, 4, 0, 3, 2, 6, 5);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    Player* player = _player;

    Mail* m = player->GetMail(mailId);
    if ((!m || m->state == MAIL_STATE_DELETED || m->deliver_time > time(NULL)) ||
        (money > 0 && m->money != money))
    {
        player->SendMailResult(mailId, MAIL_MONEY_TAKEN, MAIL_ERR_INTERNAL_ERROR);
        return;
    }

    if (!player->ModifyMoney(m->money, false))
    {
        player->SendMailResult(mailId, MAIL_MONEY_TAKEN, MAIL_ERR_EQUIP_ERROR, EQUIP_ERR_TOO_MUCH_GOLD);
        return;
    }

    m->money = 0;
    m->state = MAIL_STATE_CHANGED;
    player->m_mailsUpdated = true;

    player->SendMailResult(mailId, MAIL_MONEY_TAKEN, MAIL_OK);

    // save money and mail to prevent cheating
    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    player->SaveGoldToDB(trans);
    player->_SaveMail(trans);
    CharacterDatabase.CommitTransaction(trans);
}

//called when player lists his received mails
void WorldSession::HandleGetMailList(WorldPacket& recvData)
{
    ObjectGuid mailbox;

    recvData.ReadGuidMask(mailbox, 6, 3, 7, 5, 4, 1, 2, 0);
    recvData.ReadGuidBytes(mailbox, 7, 1, 6, 5, 4, 2, 3, 0);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    Player* player = _player;

    //load players mails, and mailed items
    if (!player->m_mailsLoaded)
        player->_LoadMail();

    if (!player->GetMailSize())
        return;

    time_t cur_time = time(nullptr);

    uint32 mailsCount = 0;
    uint32 mailItemsCount = 0;
    uint32 dynamicSize = 0;
    uint32 subjectSize = 0;
    uint32 bodySize = 0;

    std::vector<Mail*> mails;
    std::unordered_map<uint32, std::vector<Item*>> mailItems;
    std::unordered_map<uint32, std::vector<uint32>> itemDynamics;

    for (PlayerMails::iterator itr = player->GetMailBegin(); itr != player->GetMailEnd(); ++itr)
    {
        Mail* mail = *itr;
        if (!mail)
            continue;

        // skip deleted or not delivered (deliver delay not expired) mails
        if (mail->state == MAIL_STATE_DELETED || cur_time < mail->deliver_time)
            continue;

        ++mailsCount;

        if (mails.size() >= 50)
            continue;

        // skip mail with more than MAX_MAIL_ITEMS items (should not occur)
        uint8 itemCount = mail->items.size();
        if (itemCount > MAX_MAIL_ITEMS)
            continue;

        for (uint8 i = 0; i < itemCount; i++)
        {
            Item* item = player->GetMItem(mail->items[i].item_guid);
            if (!item)
                continue;

            mailItems[mail->messageID].push_back(item);

            // Dynamic modifiers
            uint32 mask = item->GetUInt32Value(ITEM_MODIFIERS_MASK);

            itemDynamics[item->GetGUID().GetCounter()].push_back(mask);

            if (mask)
                for (uint8 i = 0; mask != 0; mask >>= 1, ++i)
                    if ((mask & 1) != 0)
                        itemDynamics[item->GetGUID().GetCounter()].push_back(item->GetModifier(ItemModifier(i)));

            dynamicSize += itemDynamics[item->GetGUID().GetCounter()].size();
        }

        mailItemsCount += mailItems[mail->messageID].size();

        subjectSize += mail->subject.length();
        bodySize += mail->body.length();

        mails.push_back(mail);
    }

    WorldPacket data(SMSG_MAIL_LIST_RESULT, 4 + 3 + mails.size() * (6 + 1 + 8 + 4 + 4 + 8 + 4 + 4 + 8 + 4 + 4 + 1 + 4 + 4 + 4)
                    + mailItemsCount * (1 + 4 + 4 + 4 + 4 + MAX_INSPECTED_ENCHANTMENT_SLOT * (4 + 4 + 4) + 4 + 4 + 4 + 4 + 1 + 4)
                    + dynamicSize * (4) + subjectSize + bodySize);

    ByteBuffer mailData;

    data << uint32(mailsCount);

    data.WriteBits(mails.size(), 18);

    for (Mail* mail : mails)
    {
        ObjectGuid guid = mail->messageType == MAIL_NORMAL ? ObjectGuid::Create<HighGuid::Player>(mail->sender) : ObjectGuid::Empty;

        uint32 RealmID1 = 0;
        uint32 RealmID2 = 0;
        uint32 PackageID = 0;

        bool SendGuid = !guid.IsEmpty();
        bool HasSender = mail->messageType != MAIL_NORMAL;
        bool HasRealmID1 = RealmID1 != 0;
        bool HasRealmID2 = RealmID2 != 0;

        data.WriteBit(HasSender);

        data.WriteBits(mail->subject.length(), 8);
        data.WriteBits(mail->body.length(), 13);

        data.WriteBit(HasRealmID1);
        data.WriteBit(HasRealmID2);

        data.WriteBits(mailItems[mail->messageID].size(), 17);

        data.WriteBit(SendGuid);

        if (SendGuid)
            data.WriteGuidMask(guid, 2, 6, 7, 0, 5, 3, 1, 4);

        for (uint32 i = 0; i < mailItems[mail->messageID].size(); ++i)
        {
            Item* item = mailItems[mail->messageID][i];

            data.WriteBit(!item->IsLocked());

            mailData << uint32(item->GetGUID().GetCounter());

            /* START OF DYNAMIC FIELDS PART */

            size_t dynamicSize = itemDynamics[item->GetGUID().GetCounter()].size();

            // all dynamic modifiers sent as uint32
            mailData << uint32(dynamicSize * sizeof(uint32));

            // handle all dynamic modifiers
            for (uint32 modifier : itemDynamics[item->GetGUID().GetCounter()])
                mailData << uint32(modifier);

            /* END OF DYNAMIC FIELDS PART */

            mailData << uint32(item->GetUInt32Value(ITEM_MAX_DURABILITY));
            mailData << uint32(item->GetItemSuffixFactor());

            for (uint8 j = 0; j < MAX_INSPECTED_ENCHANTMENT_SLOT; j++)
            {
                mailData << uint32(item->GetEnchantmentDuration((EnchantmentSlot)j));
                mailData << uint32(item->GetEnchantmentId((EnchantmentSlot)j));
                mailData << uint32(item->GetEnchantmentCharges((EnchantmentSlot)j));
            }

            mailData << int32(item->GetItemRandomPropertyId());
            mailData << uint32(item->GetSpellCharges());
            mailData << uint32(item->GetUInt32Value(ITEM_DURABILITY));
            mailData << uint32(item->GetCount());
            mailData << uint8(i);
            mailData << uint32(item->GetEntry());
        }

        mailData.WriteString(mail->body);

        mailData << uint32(mail->messageID);

        if (SendGuid)
            mailData.WriteGuidBytes(guid, 4, 0, 5, 3, 1, 7, 2, 6);

        mailData << uint32(mail->mailTemplateId);
        mailData << uint64(mail->COD);

        mailData.WriteString(mail->subject);

        mailData << uint32(mail->stationery);
        mailData << float(float(mail->expire_time - time(NULL)) / DAY);
        mailData << uint64(mail->money);
        mailData << uint32(mail->checked);

        if (HasSender)
            mailData << uint32(mail->sender);

        mailData << uint8(mail->messageType);
        mailData << uint32(PackageID);

        if (HasRealmID1)
            mailData << uint32(RealmID1);

        if (HasRealmID2)
            mailData << uint32(RealmID2);
    }

    data.append(mailData);

    SendPacket(&data);

    // recalculate m_nextMailDelivereTime and unReadMails
    _player->UpdateNextMailTimeAndUnreads();
}

//used when player copies mail body to his inventory
void WorldSession::HandleMailCreateTextItem(WorldPacket& recvData)
{
    ObjectGuid mailbox;

    uint32 mailId = 0;

    recvData >> mailId;

    recvData.ReadGuidMask(mailbox, 4, 1, 6, 2, 5, 3, 0, 7);
    recvData.ReadGuidBytes(mailbox, 6, 5, 4, 3, 0, 7, 2, 1);

    if (!GetPlayer()->GetGameObjectIfCanInteractWith(mailbox, GAMEOBJECT_TYPE_MAILBOX))
        return;

    Player* player = _player;

    Mail* m = player->GetMail(mailId);
    if (!m || (m->body.empty() && !m->mailTemplateId) || m->state == MAIL_STATE_DELETED || m->deliver_time > time(NULL) || (m->checked & MAIL_CHECK_MASK_COPIED))
    {
        player->SendMailResult(mailId, MAIL_MADE_PERMANENT, MAIL_ERR_INTERNAL_ERROR);
        return;
    }

    // This is not bag and then can be used new Item.
    Item* bodyItem = new Item();
    if (!bodyItem->Create(sObjectMgr->GetGenerator<HighGuid::Item>().Generate(), MAIL_BODY_ITEM_TEMPLATE, player->GetGUID()))
    {
        delete bodyItem;
        return;
    }

    // in mail template case we need create new item text
    if (m->mailTemplateId)
    {
        MailTemplateEntry const* mailTemplateEntry = sMailTemplateStore.LookupEntry(m->mailTemplateId);
        if (!mailTemplateEntry)
        {
            player->SendMailResult(mailId, MAIL_MADE_PERMANENT, MAIL_ERR_INTERNAL_ERROR);
            return;
        }

        bodyItem->SetText(mailTemplateEntry->Content);
    }
    else
        bodyItem->SetText(m->body);

    bodyItem->SetUInt32Value(ITEM_CREATOR, m->sender);
    bodyItem->SetFlag(ITEM_FLAGS, ITEM_FLAG_MAIL_TEXT_MASK);

    ItemPosCountVec dest;
    uint8 msg = _player->CanStoreItem(NULL_BAG, NULL_SLOT, dest, bodyItem, false);
    if (msg == EQUIP_ERR_OK)
    {
        m->checked = m->checked | MAIL_CHECK_MASK_COPIED;
        m->state = MAIL_STATE_CHANGED;
        player->m_mailsUpdated = true;

        player->StoreItem(dest, bodyItem, true);
        player->SendMailResult(mailId, MAIL_MADE_PERMANENT, MAIL_OK);
    }
    else
    {
        player->SendMailResult(mailId, MAIL_MADE_PERMANENT, MAIL_ERR_EQUIP_ERROR, msg);
        delete bodyItem;
    }
}

void WorldSession::HandleQueryNextMailTime(WorldPacket& /*recvData*/)
{
    if (!_player->m_mailsLoaded)
        _player->_LoadMail();

    std::vector<Mail*> mails;

    time_t now = time(nullptr);

    if (_player->unReadMails > 0)
    {
        std::set<uint32> sentSenders;

        for (PlayerMails::iterator itr = _player->GetMailBegin(); itr != _player->GetMailEnd(); ++itr)
        {
            Mail* m = (*itr);

            // must be not checked yet
            if (m->checked & MAIL_CHECK_MASK_READ)
                continue;

            // and already delivered
            if (now < m->deliver_time)
                continue;

            // only send each mail sender once
            if (sentSenders.count(m->sender))
                continue;

            sentSenders.insert(m->sender);

            // do not display more than 2 mails
            if (sentSenders.size() > 2)
                break;

            mails.push_back(m);
        }
    }

    uint32 NativeRealmID = 0;
    uint32 VirtualRealmID = 0;

    bool HasNativeRealmID = NativeRealmID != 0;
    bool HasVirtualRealmID = VirtualRealmID != 0;

    WorldPacket data(SMSG_QUERY_NEXT_MAIL_TIME);

    ByteBuffer mailData;

    data.WriteBits(mails.size(), 20);

    for (Mail* mail : mails)
    {
        ObjectGuid guid = mail->messageType == MAIL_NORMAL ? ObjectGuid::Create<HighGuid::Player>(mail->sender) : ObjectGuid::Empty;

        data.WriteGuidMask(guid, 3);

        data.WriteBit(HasNativeRealmID);

        data.WriteGuidMask(guid, 2);

        data.WriteBit(HasVirtualRealmID);

        data.WriteGuidMask(guid, 6, 1, 4, 0, 5, 7);

        mailData << uint32(mail->messageType != MAIL_NORMAL ? mail->sender : 0);

        mailData.WriteGuidBytes(guid, 5, 4, 6, 1);

        mailData << uint8(mail->messageType);

        mailData.WriteGuidBytes(guid, 0);

        mailData << float(mail->deliver_time - now);

        if (HasVirtualRealmID)
            mailData << uint32(VirtualRealmID);

        mailData << uint32(mail->stationery);

        mailData.WriteGuidBytes(guid, 3, 2);

        if (HasNativeRealmID)
            mailData << uint32(NativeRealmID);

        mailData.WriteGuidBytes(guid, 7);
    }

    data.FlushBits();

    data.append(mailData);

    data << float(mails.size() > 0 ? 0.0f : -DAY);

    SendPacket(&data);
}
