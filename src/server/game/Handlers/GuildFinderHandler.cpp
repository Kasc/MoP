/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "WorldSession.h"
#include "WorldPacket.h"
#include "Object.h"
#include "SharedDefines.h"
#include "GuildFinderMgr.h"
#include "GuildMgr.h"

void WorldSession::HandleGuildFinderAddRecruit(WorldPacket& recvData)
{
    if (sGuildFinderMgr->GetAllMembershipRequestsForPlayer(GetPlayer()->GetGUID()).size() == 10)
        return;

    ObjectGuid guid;

    uint32 classRoles = 0;
    uint32 availability = 0;
    uint32 guildInterests = 0;
    uint32 commentLength = 0;

    std::string comment;

    recvData >> classRoles;
    recvData >> availability;
    recvData >> guildInterests;

    recvData.ReadGuidMask(guid, 7, 5, 2, 6, 1, 0);

    commentLength = recvData.ReadBits(10);

    recvData.ReadGuidMask(guid, 3, 4);

    recvData.ReadGuidBytes(guid, 4, 0, 2);

    comment = recvData.ReadString(commentLength);

    recvData.ReadGuidBytes(guid, 6, 1, 5, 7, 3);

    if (!guid.IsGuild())
        return;

    if ((classRoles & GUILDFINDER_ALL_ROLES) == 0 || classRoles > GUILDFINDER_ALL_ROLES)
        return;

    if ((availability & AVAILABILITY_ALWAYS) == 0 || availability > AVAILABILITY_ALWAYS)
        return;

    if ((guildInterests & ALL_INTERESTS) == 0 || guildInterests > ALL_INTERESTS)
        return;

    MembershipRequest request = MembershipRequest(GetPlayer()->GetGUID(), guid, availability, classRoles, guildInterests, comment, time(nullptr));
    sGuildFinderMgr->AddMembershipRequest(guid, request);
}

void WorldSession::HandleGuildFinderBrowse(WorldPacket& recvData)
{
    uint32 classRoles = 0;
    uint32 availability = 0;
    uint32 guildInterests = 0;
    uint32 playerLevel = 0;

    recvData >> playerLevel;
    recvData >> availability;
    recvData >> classRoles;
    recvData >> guildInterests;

    if ((classRoles & GUILDFINDER_ALL_ROLES) == 0 || classRoles > GUILDFINDER_ALL_ROLES)
        return;

    if ((availability & AVAILABILITY_ALWAYS) == 0 || availability > AVAILABILITY_ALWAYS)
        return;

    if ((guildInterests & ALL_INTERESTS) == 0 || guildInterests > ALL_INTERESTS)
        return;

    if (playerLevel > sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL) || playerLevel < 1)
        return;

    uint8 level = ANY_FINDER_LEVEL;
    if (playerLevel == sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
        level |= MAX_FINDER_LEVEL;

    Player* player = GetPlayer();

    LFGuildPlayer settings(player->GetGUID(), classRoles, availability, guildInterests, level);
    LFGuildStore guildList = sGuildFinderMgr->GetGuildsMatchingSetting(settings, player->GetTeamId());

    uint32 guildCount = guildList.size();

    uint32 commentSize = 0;
    uint32 guildNameSize = 0;
    for (LFGuildStore::const_iterator itr = guildList.begin(); itr != guildList.end(); ++itr)
    {
        LFGuildSettings guildSettings = itr->second;
        Guild* guild = sGuildMgr->GetGuildById(itr->first);

        commentSize += guildSettings.GetComment().size();
        guildNameSize += guild->GetName().size();
    }

    ByteBuffer bufferData;

    WorldPacket data(SMSG_LF_GUILD_BROWSE_UPDATED, 3 + guildCount * (1 + 8 + 3 + 4 + 1 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 1 + 4 + 4 + 4 + 4) + commentSize + guildNameSize);

    data.WriteBits(guildCount, 18);

    for (LFGuildStore::const_iterator itr = guildList.begin(); itr != guildList.end(); ++itr)
    {
        LFGuildSettings guildSettings = itr->second;
        Guild* guild = sGuildMgr->GetGuildById(itr->first);

        ObjectGuid guildGUID = guild->GetGUID();

        data.WriteGuidMask(guildGUID, 6, 5, 4, 0, 1);

        data.WriteBits(guildSettings.GetComment().size(), 10);

        data.WriteGuidMask(guildGUID, 3);

        data.WriteBits(guild->GetName().size(), 7);

        data.WriteGuidMask(guildGUID, 7, 2);

        data.WriteGuidBytes(guildGUID, 3);

        bufferData << uint32(guild->GetEmblemInfo().GetColor());
        bufferData << uint8(sGuildFinderMgr->HasRequest(player->GetGUID(), guild->GetGUID()));

        bufferData.WriteGuidBytes(guildGUID, 0);

        bufferData << uint32(guild->GetAchievementMgr().GetAchievementPoints());

        bufferData.WriteGuidBytes(guildGUID, 2);

        bufferData << uint32(guildSettings.GetInterests());
        bufferData << uint32(guild->GetEmblemInfo().GetBackgroundColor());
        bufferData << uint32(guild->GetLevel());
        bufferData << uint32(guildSettings.GetAvailability());
        bufferData << uint32(guildSettings.GetClassRoles());

        bufferData.WriteGuidBytes(guildGUID, 5);

        bufferData << uint32(0); //Zero: always before guild name

        bufferData.WriteString(guild->GetName());

        bufferData << uint32(guild->GetEmblemInfo().GetBorderStyle());
        bufferData << uint8(0); //Unk

        bufferData.WriteGuidBytes(guildGUID, 7);

        bufferData << uint32(guild->GetEmblemInfo().GetColor());

        bufferData.WriteGuidBytes(guildGUID, 6);

        bufferData << uint32(0); // Unk

        bufferData.WriteString(guildSettings.GetComment());

        bufferData << uint32(guild->GetEmblemInfo().GetBorderColor());
        bufferData << uint32(guild->GetMembersCount());

        bufferData.WriteGuidBytes(guildGUID, 1, 4);
    }

    data.append(bufferData);

    player->SendDirectMessage(&data);
}

void WorldSession::HandleGuildFinderDeclineRecruit(WorldPacket& recvData)
{
    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 6, 7, 3, 1, 2, 0, 4, 5);
    recvData.ReadGuidBytes(playerGuid, 0, 7, 1, 6, 4, 3, 5, 2);

    if (!playerGuid.IsPlayer())
        return;

    Guild* guild = GetPlayer()->GetGuild();
    if (!guild)
        return;

    sGuildFinderMgr->RemoveMembershipRequest(playerGuid, guild->GetGUID());
}

void WorldSession::HandleGuildFinderGetApplications(WorldPacket& /*recvData*/)
{
    std::vector<MembershipRequest> applicatedGuilds = sGuildFinderMgr->GetAllMembershipRequestsForPlayer(GetPlayer()->GetGUID());
    uint32 applicationsCount = applicatedGuilds.size();

    ByteBuffer bufferData;
    
    uint32 commentSize = 0;
    uint32 guildNameSize = 0;
    for (std::vector<MembershipRequest>::const_iterator itr = applicatedGuilds.begin(); itr != applicatedGuilds.end(); ++itr)
    {
        MembershipRequest request = *itr;
        Guild* guild = sGuildMgr->GetGuildByGuid(itr->GetGuildGUID());

        commentSize += request.GetComment().size();
        guildNameSize += guild->GetName().size();
    }

    WorldPacket data(SMSG_LF_GUILD_MEMBERSHIP_LIST_UPDATED, 3 + 4 + applicationsCount * (1 + 8 + 3 + 4 + 4 + 4 + 4 + 4 + 4) + commentSize + guildNameSize);

    data.WriteBits(applicationsCount, 19);

    for (std::vector<MembershipRequest>::const_iterator itr = applicatedGuilds.begin(); itr != applicatedGuilds.end(); ++itr)
    {
        Guild* guild = sGuildMgr->GetGuildByGuid(itr->GetGuildGUID());

        LFGuildSettings guildSettings = sGuildFinderMgr->GetGuildSettings(itr->GetGuildGUID());

        MembershipRequest request = *itr;

        ObjectGuid guildGuid = guild->GetGUID();

        data.WriteGuidMask(guildGuid, 0, 4, 2, 7);

        data.WriteBits(guild->GetName().size(), 7);

        data.WriteGuidMask(guildGuid, 1, 3);

        data.WriteBits(request.GetComment().size(), 10);

        data.WriteGuidMask(guildGuid, 6, 5);

        bufferData << uint32(request.GetInterests());
        bufferData << uint32(realmID);

        bufferData.WriteString(guild->GetName());

        bufferData.WriteGuidBytes(guildGuid, 4);

        bufferData << uint32(request.GetClassRoles());

        bufferData.WriteGuidBytes(guildGuid, 6, 5);

        bufferData << uint32(time(nullptr) - request.GetSubmitTime()); // Time since application (seconds)

        bufferData.WriteGuidBytes(guildGuid, 1, 3, 0, 7, 2);

        bufferData << uint32(request.GetExpiryTime() - time(nullptr)); // Time left to application expiry (seconds)
        bufferData << uint32(request.GetAvailability());

        bufferData.WriteString(request.GetComment());
    }

    data.append(bufferData);

    data << uint32(10 - sGuildFinderMgr->CountRequestsFromPlayer(GetPlayer()->GetGUID())); // Applications count left

    GetPlayer()->SendDirectMessage(&data);
}

void WorldSession::HandleGuildFinderGetRecruits(WorldPacket& recvData)
{
    uint32 updateTimer = 0;
    recvData >> updateTimer;

    Player* player = GetPlayer();
    if (!player)
        return;

    Guild* guild = sGuildMgr->GetGuildById(player->GetGuildId());
    if (!guild)
        return;

    std::vector<MembershipRequest> recruitsList = sGuildFinderMgr->GetAllMembershipRequestsForGuild(guild->GetGUID());

    uint32 commentSize = 0;
    uint32 nameSize = 0;
    for (std::vector<MembershipRequest>::const_iterator itr = recruitsList.begin(); itr != recruitsList.end(); ++itr)
    {
        MembershipRequest request = *itr;

        commentSize += request.GetComment().size();
        nameSize += request.GetName().size();
    }

    ByteBuffer dataBuffer;

    WorldPacket data(SMSG_LF_GUILD_RECRUIT_LIST_UPDATED, 3 + 4 + recruitsList.size() * (1 + 8 + 2 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4) + commentSize + nameSize);

    data.WriteBits(recruitsList.size(), 18);

    for (std::vector<MembershipRequest>::const_iterator itr = recruitsList.begin(); itr != recruitsList.end(); ++itr)
    {
        MembershipRequest request = *itr;

        ObjectGuid playerGuid = request.GetPlayerGUID();

        data.WriteGuidMask(playerGuid, 7);

        data.WriteBits(request.GetName().size(), 6);

        data.WriteGuidMask(playerGuid, 1, 0, 2, 4, 3, 6);

        data.WriteBits(request.GetComment().size(), 10);

        data.WriteGuidMask(playerGuid, 5);

        dataBuffer.WriteGuidBytes(playerGuid, 2, 7, 1);

        dataBuffer.WriteString(request.GetComment());

        dataBuffer << uint32(time(nullptr) <= request.GetExpiryTime());

        dataBuffer.WriteGuidBytes(playerGuid, 0);

        dataBuffer << uint32(time(nullptr) - request.GetSubmitTime());
        dataBuffer << uint32(request.GetClass());
        dataBuffer << uint32(request.GetClassRoles());

        dataBuffer.WriteGuidBytes(playerGuid, 4);

        dataBuffer << uint32(request.GetExpiryTime() - time(nullptr));
        dataBuffer << uint32(request.GetLevel());

        dataBuffer.WriteGuidBytes(playerGuid, 3);

        dataBuffer.WriteString(request.GetName());

        dataBuffer << uint32(realmID);

        dataBuffer.WriteGuidBytes(playerGuid, 6, 5);

        dataBuffer << uint32(request.GetAvailability());
        dataBuffer << uint32(request.GetInterests());
    }

    data.append(dataBuffer);

    data << uint32(time(nullptr));

    player->SendDirectMessage(&data);
}

void WorldSession::HandleGuildFinderPostRequest(WorldPacket& /*recvData*/)
{
    Player* player = GetPlayer();

    Guild* guild = sGuildMgr->GetGuildById(player->GetGuildId());
    if (!guild)
        return;

    bool IsGuildMaster = true;
    if (guild->GetLeaderGUID() != player->GetGUID())
        IsGuildMaster = false;

    LFGuildSettings settings = sGuildFinderMgr->GetGuildSettings(guild->GetGUID());

    WorldPacket data(SMSG_LF_GUILD_POST_UPDATED, 1 + (IsGuildMaster ? (1 + settings.GetComment().size() + 4 + 4 + 4 + 4 + 4) : 0));

    data.WriteBit(IsGuildMaster);

    if (IsGuildMaster)
    {
        data.WriteBit(settings.IsListed());

        data.WriteBits(settings.GetComment().size(), 10);

        data.FlushBits();

        data.WriteString(settings.GetComment());

        data << uint32(settings.GetInterests());
        data << uint32(settings.GetClassRoles());
        data << uint32(settings.GetLevel());
        data << uint32(0); // Seconds remaining
        data << uint32(settings.GetAvailability());
    }
    else
        data.FlushBits();

    player->SendDirectMessage(&data);
}

void WorldSession::HandleGuildFinderRemoveRecruit(WorldPacket& recvData)
{
    ObjectGuid guildGuid;

    recvData.ReadGuidMask(guildGuid, 7, 5, 4, 1, 6, 3, 2, 0);
    recvData.ReadGuidBytes(guildGuid, 6, 3, 7, 1, 2, 0, 5, 4);

    if (!guildGuid.IsGuild())
        return;

    sGuildFinderMgr->RemoveMembershipRequest(GetPlayer()->GetGUID(), guildGuid);
}

// Sent any time a guild master sets an option in the interface and when listing / unlisting his guild
void WorldSession::HandleGuildFinderSetGuildPost(WorldPacket& recvData)
{
    uint32 classRoles = 0;
    uint32 availability = 0;
    uint32 guildInterests =  0;
    uint32 level = 0;

    uint16 length = 0;

    bool listed = false;

    std::string comment;

    recvData >> level;
    recvData >> availability;
    recvData >> classRoles;
    recvData >> guildInterests;

    // Level sent is zero if untouched, force to any (from interface). Idk why
    if (!level)
        level = ANY_FINDER_LEVEL;

    length = recvData.ReadBits(10);

    listed = recvData.ReadBit();

    comment = recvData.ReadString(length);

    if ((classRoles & GUILDFINDER_ALL_ROLES) == 0 || classRoles > GUILDFINDER_ALL_ROLES)
        return;

    if ((availability & AVAILABILITY_ALWAYS) == 0 || availability > AVAILABILITY_ALWAYS)
        return;

    if ((guildInterests & ALL_INTERESTS) == 0 || guildInterests > ALL_INTERESTS)
        return;

    if ((level & ALL_GUILDFINDER_LEVELS) == 0 || level > ALL_GUILDFINDER_LEVELS)
        return;

    Player* player = GetPlayer();

    Guild* guild = sGuildMgr->GetGuildById(player->GetGuildId());
    if (!guild)
        return;

    if (guild->GetLeaderGUID() != player->GetGUID())
        return;

    LFGuildSettings settings(listed, player->GetTeamId(), guild->GetGUID(), classRoles, availability, guildInterests, level, comment);
    sGuildFinderMgr->SetGuildSettings(guild->GetGUID(), settings);
}
