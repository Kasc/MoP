/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectMgr.h"
#include "Player.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"

#include "AuctionHouseMgr.h"
#include "CharacterCache.h"
#include "Log.h"
#include "Language.h"
#include "Opcodes.h"
#include "Util.h"
#include "AccountMgr.h"

void WorldSession::HandleAuctionHelloOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_HELLO");

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 1, 5, 2, 0, 3, 6, 4, 7);
    recvData.ReadGuidBytes(guid, 2, 7, 1, 3, 5, 0, 4, 6);

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_AUCTIONEER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionHelloOpcode - Unit (%s) not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    SendAuctionHello(guid, unit);
}

void WorldSession::SendAuctionHello(ObjectGuid guid, Creature* unit)
{
    if (GetPlayer()->GetLevel() < sWorld->getIntConfig(CONFIG_AUCTION_LEVEL_REQ))
    {
        SendNotification(GetTrinityString(LANG_AUCTION_REQ), sWorld->getIntConfig(CONFIG_AUCTION_LEVEL_REQ));
        return;
    }

    AuctionHouseEntry const* ahEntry = AuctionHouseMgr::GetAuctionHouseEntry(unit->GetFaction());
    if (!ahEntry)
        return;

    bool IsOpened = true;

    WorldPacket data(SMSG_AUCTION_HELLO, 1 + 8 + 1 + 4);

    data.WriteGuidMask(guid, 6, 7, 3);

    data.WriteBit(IsOpened);            // 1 - AH enabled, 0 - AH disabled

    data.WriteGuidMask(guid, 4, 2, 5, 0, 1);

    data.FlushBits();

    data.WriteGuidBytes(guid, 3);

    data << uint32(ahEntry->ID);

    data.WriteGuidBytes(guid, 4, 2, 7, 1, 0, 6, 5);

    SendPacket(&data);
}

void WorldSession::SendAuctionCommandResult(AuctionEntry const* auction, uint32 action, uint32 errorCode, uint32 inventoryError)
{
    ObjectGuid BidderGuid;
    uint32 AuctionID = 0;
    uint64 HigherBid = 0;
    uint64 OutBid = 0;

    if (auction)
    {
        AuctionID = auction->Id;
        BidderGuid = auction->bidder;
        if (auction->bid != auction->buyout)
        {
            HigherBid = auction->bid;
            OutBid = auction->GetAuctionOutBid();
        }
    }

    bool HasOutBid = OutBid > 0;
    bool HasHigherBid = HigherBid > 0;

    WorldPacket data(SMSG_AUCTION_COMMAND_RESULT, 1 + 8 + 2 + 4 + 4 + 4 + 4 + (HasHigherBid ? 8 : 0) + (HasOutBid ? 8 : 0));

    data.WriteBit(!HasHigherBid);
    data.WriteBit(!HasOutBid);
    data.WriteBit(BidderGuid.IsEmpty());

    data.WriteGuidMask(BidderGuid, 1, 4, 0, 6, 3, 5, 2, 7);

    data.FlushBits();

    data.WriteGuidBytes(BidderGuid, 3, 0, 7, 1, 4, 6, 5, 2);

    if (HasOutBid)
        data << uint64(OutBid);

    data << uint32(errorCode);
    data << uint32(action);
    data << uint32(AuctionID);

    if (HasHigherBid)
        data << uint64(HigherBid);

    data << uint32(inventoryError); // combined with ERR_AUCTION_INVENTORY (see InventoryResult enum)

    SendPacket(&data);
}

void WorldSession::SendAuctionWonNotification(AuctionEntry const* auction, ObjectGuid bidder)
{
    Item* item = sAuctionMgr->GetAItem(auction->item);
    if (!item)
        return;

    uint32 ItemID = item->GetEntry();
    uint32 SuffixFactor = item->GetItemSuffixFactor();
    int32 RandomProperty = item->GetItemRandomPropertyId();

    uint32 AuctionID = auction->Id;
    uint32 HouseID = auction->GetHouseId();

    WorldPacket data(SMSG_AUCTION_WON_NOTIFICATION, 1 + 8 + 4 + 4 + 4 + 4 + 4);

    data.WriteGuidMask(bidder, 5, 4, 7, 6, 0, 1, 2, 3);

    data.WriteGuidBytes(bidder, 7, 3);

    data << uint32(ItemID);

    data.WriteGuidBytes(bidder, 1, 2);

    data << uint32(HouseID);

    data.WriteGuidBytes(bidder, 0);

    data << uint32(SuffixFactor);

    data.WriteGuidBytes(bidder, 5, 4, 6);

    data << int32(RandomProperty);
    data << uint32(AuctionID);

    SendPacket(&data);
}

void WorldSession::SendAuctionOutBidNotification(AuctionEntry const* auction, ObjectGuid bidder, uint64 bidSum)
{
    Item* item = sAuctionMgr->GetAItem(auction->item);
    if (!item)
        return;

    uint32 ItemID = item->GetEntry();
    uint32 SuffixFactor = item->GetItemSuffixFactor();
    int32 RandomProperty = item->GetItemRandomPropertyId();

    uint32 AuctionID = auction->Id;
    uint32 HouseID = auction->GetHouseId();
    uint32 OutBid = auction->GetAuctionOutBid();

    WorldPacket data(SMSG_AUCTION_OUTBID_NOTIFICATION, 1 + 8 + 4 + 4 + 4 + 8 + 4 + 4 + 8);

    data << uint32(AuctionID);
    data << uint32(HouseID);
    data << int32(RandomProperty);
    data << uint64(bidSum);
    data << uint32(ItemID);
    data << uint32(SuffixFactor);
    data << uint64(OutBid);

    data.WriteGuidMask(bidder, 2, 5, 0, 1, 4, 6, 3, 7);
    data.WriteGuidBytes(bidder, 4, 7, 3, 0, 1, 6, 2, 5);

    SendPacket(&data);
}

void WorldSession::SendAuctionOwnerNotification(AuctionEntry const* auction, float mailDelay, bool sold, Item* item)
{
    uint32 ItemID = item->GetEntry();
    uint32 SuffixFactor = item->GetItemSuffixFactor();
    int32 RandomProperty = item->GetItemRandomPropertyId();

    uint32 AuctionID = auction->Id;
    uint32 HouseID = auction->GetHouseId();
    uint32 OutBid = auction->GetAuctionOutBid();

    WorldPacket data(SMSG_AUCTION_OWNER_NOTIFICATION, 4 + 4 + 4 + 4 + 4 + 8 + 1);

    data << uint32(SuffixFactor);
    data << uint32(AuctionID);
    data << uint32(ItemID);
    data << int32(RandomProperty);
    data << float(mailDelay);
    data << uint64(auction->bid);

    data.WriteBit(sold);

    data.FlushBits();

    SendPacket(&data);
}

void WorldSession::SendAuctionOwnerBidNotification(AuctionEntry const* auction, ObjectGuid bidder, uint64 bidSum)
{
    Item* item = sAuctionMgr->GetAItem(auction->item);
    if (!item)
        return;

    uint32 ItemID = item->GetEntry();
    uint32 SuffixFactor = item->GetItemSuffixFactor();
    int32 RandomProperty = item->GetItemRandomPropertyId();

    uint32 AuctionID = auction->Id;
    uint64 OutBid = auction->GetAuctionOutBid();

    WorldPacket data(SMSG_AUCTION_OWNER_BID_NOTIFICATION, 1 + 8 + 8 + 4 + 4 + 4 + 4 + 8);

    data << uint64(bidSum);
    data << int32(RandomProperty);
    data << uint32(SuffixFactor);
    data << uint32(ItemID);
    data << uint32(AuctionID);
    data << uint64(OutBid);

    data.WriteGuidMask(bidder, 3, 1, 5, 0, 7, 4, 2, 6);
    data.WriteGuidBytes(bidder, 5, 1, 0, 7, 2, 6, 3, 4);

    SendPacket(&data);
}

void WorldSession::HandleAuctionSellItem(WorldPacket& recvData)
{
    ObjectGuid auctioneer;
    uint64 bid = 0;
    uint64 buyout = 0;
    uint32 etime = 0;
    uint32 itemCount = 0;

    GuidVector itemGuids;
    std::vector<uint32> count;

    recvData >> buyout;
    recvData >> bid;
    recvData >> etime;

    if (!bid || !etime)
    {
        recvData.rfinish();
        return;
    }

    if (bid > MAX_MONEY_AMOUNT || buyout > MAX_MONEY_AMOUNT)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionSellItem - Player %s (GUID %u) attempted to sell item with higher price than max gold amount.", _player->GetName().c_str(), _player->GetGUID().GetCounter());
        SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_DATABASE_ERROR);
        recvData.rfinish();
        return;
    }

    recvData.ReadGuidMask(auctioneer, 3);

    itemCount = recvData.ReadBits(5);

    if (itemCount > MAX_AUCTION_ITEMS)
    {
        SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_DATABASE_ERROR);
        recvData.rfinish();
        return;
    }

    itemGuids.resize(itemCount);
    count.resize(itemCount);

    recvData.ReadGuidMask(auctioneer, 0);

    for (uint32 i = 0; i < itemCount; i++)
        recvData.ReadGuidMask(itemGuids[i], 4, 6, 2, 3, 5, 7, 1, 0);

    recvData.ReadGuidMask(auctioneer, 6, 2, 1, 4, 5, 7);

    recvData.FlushBits();

    for (uint32 i = 0; i < itemCount; i++)
    {
        recvData.ReadGuidBytes(itemGuids[i], 3, 1);

        recvData >> count[i];

        recvData.ReadGuidBytes(itemGuids[i], 6, 4, 5, 0, 2, 7);

        if (!itemGuids[i] || !count[i] || count[i] > 1000)
        {
            recvData.rfinish();
            return;
        }
    }

    recvData.ReadGuidBytes(auctioneer, 3, 7, 2, 5, 6, 1, 0, 4);

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionSellItem - Unit (%s) not found or you can't interact with him.", auctioneer.ToString().c_str());
        return;
    }

    AuctionHouseEntry const* auctionHouseEntry = AuctionHouseMgr::GetAuctionHouseEntry(creature->GetFaction());
    if (!auctionHouseEntry)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionSellItem - Unit (%s) has wrong faction.", auctioneer.ToString().c_str());
        return;
    }

    etime *= MINUTE;

    switch (etime)
    {
        case 1 * MIN_AUCTION_TIME:
        case 2 * MIN_AUCTION_TIME:
        case 4 * MIN_AUCTION_TIME:
            break;
        default:
            return;
    }

    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    std::vector<Item*> items(itemCount);

    uint32 finalCount = 0;
    uint32 itemEntry = 0;

    for (uint32 i = 0; i < itemCount; ++i)
    {
        Item* item = _player->GetItemByGuid(itemGuids[i]);

        if (!item)
        {
            SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_ITEM_NOT_FOUND);
            return;
        }

        if (itemEntry == 0)
            itemEntry = item->GetTemplate()->GetId();

        if (sAuctionMgr->GetAItem(item->GetGUID()) || !item->CanBeTraded() || item->IsNotEmptyBag() ||
            item->GetTemplate()->GetFlags() & ITEM_PROTO_FLAG_CONJURED || item->GetUInt32Value(ITEM_EXPIRATION) ||
            item->GetCount() < count[i] || itemEntry != item->GetTemplate()->GetId())
        {
            SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_DATABASE_ERROR);
            return;
        }

        items[i] = item;
        finalCount += count[i];
    }

    if (!finalCount || items.empty())
    {
        SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_DATABASE_ERROR);
        return;
    }

    for (uint32 i = 0; i < itemCount; ++i)
    {
        Item* item = items[i];

        if (item->GetMaxStackCount() < finalCount)
        {
            SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_DATABASE_ERROR);
            return;
        }
    }

    Item* item = items[0];

    uint32 auctionTime = uint32(etime * sWorld->getRate(RATE_AUCTION_TIME));
    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());

    uint64 deposit = sAuctionMgr->GetAuctionDeposit(auctionHouseEntry, etime, item, finalCount);
    if (!_player->HasEnoughMoney(deposit))
    {
        SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_NOT_ENOUGHT_MONEY);
        return;
    }

    AuctionEntry* AH = new AuctionEntry();

    if (sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_AUCTION))
        AH->houseId = AUCTIONHOUSE_NEUTRAL;
    else
    {
        CreatureData const* auctioneerData = sObjectMgr->GetCreatureData(creature->GetSpawnId());
        if (!auctioneerData)
        {
            TC_LOG_ERROR("misc", "Data for auctioneer not found (%s)", auctioneer.ToString().c_str());
            return;
        }

        CreatureTemplate const* auctioneerInfo = sObjectMgr->GetCreatureTemplate(auctioneerData->id);
        if (!auctioneerInfo)
        {
            TC_LOG_ERROR("misc", "Non existing auctioneer (%s)", auctioneer.ToString().c_str());
            return;
        }

        const AuctionHouseEntry* AHEntry = sAuctionMgr->GetAuctionHouseEntry(auctioneerInfo->faction);
        AH->houseId = AHEntry->ID;
    }

    // Required stack size of auction matches to current item stack size, just move item to auctionhouse
    if (itemCount == 1 && item->GetCount() == count[0])
    {
        if (HasPermission(rbac::RBAC_PERM_LOG_GM_TRADE))
        {
            sLog->outCommand(GetAccountId(), "GM %s (Account: %u) create auction: %s (Entry: %u Count: %u)",
                GetPlayerName().c_str(), GetAccountId(), item->GetTemplate()->GetDefaultLocaleName(), item->GetEntry(), item->GetCount());
        }

        AH->Id = sObjectMgr->GenerateAuctionID();
        AH->item = item->GetGUID();
        AH->itemEntry = item->GetEntry();
        AH->itemCount = item->GetCount();
        AH->owner = _player->GetGUID();
        AH->startbid = bid;
        AH->bidder = ObjectGuid::Empty;
        AH->bid = 0;
        AH->buyout = buyout;
        AH->expire_time = time(NULL) + auctionTime;
        AH->deposit = deposit;
        AH->etime = etime;
        AH->auctionHouseEntry = auctionHouseEntry;

        TC_LOG_INFO("network", "CMSG_AUCTION_SELL_ITEM: Player %s (guid %d) is selling item %s entry %u (guid %d) with count %u with initial bid %u with buyout %u and with time %u (in sec) in auctionhouse %u",
            _player->GetName().c_str(), _player->GetGUID().GetCounter(), item->GetTemplate()->GetDefaultLocaleName(), item->GetEntry(), item->GetGUID().GetCounter(), item->GetCount(), bid, buyout, auctionTime, AH->GetHouseId());

        // Add to pending auctions, or fail with insufficient funds error
        if (!sAuctionMgr->PendingAuctionAdd(_player, AH, item))
        {
            SendAuctionCommandResult(AH, AUCTION_SELL_ITEM, ERR_AUCTION_NOT_ENOUGHT_MONEY);
            return;
        }

        sAuctionMgr->AddAItem(item);
        auctionHouse->AddAuction(AH);
        _player->MoveItemFromInventory(item->GetBagSlot(), item->GetSlot(), true);

        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        item->DeleteFromInventoryDB(trans);
        item->SaveToDB(trans);

        AH->SaveToDB(trans);
        _player->SaveInventoryAndGoldToDB(trans);
        CharacterDatabase.CommitTransaction(trans);

        SendAuctionCommandResult(AH, AUCTION_SELL_ITEM, ERR_AUCTION_OK);

        GetPlayer()->UpdateCriteria(CRITERIA_TYPE_CREATE_AUCTION, 1);
    }
    else // Required stack size of auction does not match to current item stack size, clone item and set correct stack size
    {
        Item* newItem = item->CloneItem(finalCount, _player);
        if (!newItem)
        {
            TC_LOG_ERROR("network", "CMSG_AUCTION_SELL_ITEM: Could not create clone of item %u", item->GetEntry());
            SendAuctionCommandResult(NULL, AUCTION_SELL_ITEM, ERR_AUCTION_DATABASE_ERROR);
            delete AH;
            return;
        }

        if (HasPermission(rbac::RBAC_PERM_LOG_GM_TRADE))
        {
            sLog->outCommand(GetAccountId(), "GM %s (Account: %u) create auction: %s (Entry: %u Count: %u)",
                GetPlayerName().c_str(), GetAccountId(), newItem->GetTemplate()->GetDefaultLocaleName(), newItem->GetEntry(), newItem->GetCount());
        }

        AH->Id = sObjectMgr->GenerateAuctionID();
        AH->item = newItem->GetGUID();
        AH->itemEntry = newItem->GetEntry();
        AH->itemCount = newItem->GetCount();
        AH->owner = _player->GetGUID();
        AH->startbid = bid;
        AH->bidder = ObjectGuid::Empty;
        AH->bid = 0;
        AH->buyout = buyout;
        AH->expire_time = time(NULL) + auctionTime;
        AH->deposit = deposit;
        AH->etime = etime;
        AH->auctionHouseEntry = auctionHouseEntry;

        TC_LOG_INFO("network", "CMSG_AUCTION_SELL_ITEM: Player %s (guid %d) is selling item %s entry %u (guid %d) with count %u with initial bid %u with buyout %u and with time %u (in sec) in auctionhouse %u",
            _player->GetName().c_str(), _player->GetGUID().GetCounter(), newItem->GetTemplate()->GetDefaultLocaleName(), newItem->GetEntry(), newItem->GetGUID().GetCounter(), newItem->GetCount(), bid, buyout, auctionTime, AH->GetHouseId());

        // Add to pending auctions, or fail with insufficient funds error
        if (!sAuctionMgr->PendingAuctionAdd(_player, AH, newItem))
        {
            SendAuctionCommandResult(AH, AUCTION_SELL_ITEM, ERR_AUCTION_NOT_ENOUGHT_MONEY);
            return;
        }

        sAuctionMgr->AddAItem(newItem);
        auctionHouse->AddAuction(AH);
        for (uint32 j = 0; j < itemCount; ++j)
        {
            Item* item2 = items[j];

            // Item stack count equals required count, ready to delete item - cloned item will be used for auction
            if (item2->GetCount() == count[j])
            {
                _player->MoveItemFromInventory(item2->GetBagSlot(), item2->GetSlot(), true);

                SQLTransaction trans = CharacterDatabase.BeginTransaction();
                item2->DeleteFromInventoryDB(trans);
                item2->DeleteFromDB(trans);
                CharacterDatabase.CommitTransaction(trans);
                delete item2;
            }
            else // Item stack count is bigger than required count, update item stack count and save to database - cloned item will be used for auction
            {
                item2->SetCount(item2->GetCount() - count[j]);
                item2->SetState(ITEM_CHANGED, _player);
                _player->ItemRemovedQuestCheck(item2->GetEntry(), count[j]);
                item2->SendUpdateToPlayer(_player);

                SQLTransaction trans = CharacterDatabase.BeginTransaction();
                item2->SaveToDB(trans);
                CharacterDatabase.CommitTransaction(trans);
            }
        }

        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        newItem->SaveToDB(trans);
        AH->SaveToDB(trans);
        _player->SaveInventoryAndGoldToDB(trans);
        CharacterDatabase.CommitTransaction(trans);

        SendAuctionCommandResult(AH, AUCTION_SELL_ITEM, ERR_AUCTION_OK);

        GetPlayer()->UpdateCriteria(CRITERIA_TYPE_CREATE_AUCTION, 1);
    }
}

void WorldSession::HandleAuctionPlaceBid(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_PLACE_BID");

    ObjectGuid auctioneer;
    uint32 auctionId = 0;
    uint64 price = 0;

    recvData >> auctionId;
    recvData >> price;

    recvData.ReadGuidMask(auctioneer, 1, 6, 3, 7, 2, 4, 0, 5);
    recvData.ReadGuidBytes(auctioneer, 3, 2, 1, 4, 6, 5, 7, 0);

    // check for cheaters
    if (!auctionId || !price)
        return;

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionPlaceBid - %s not found or you can't interact with him.", auctioneer.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());
    if (!auctionHouse)
        return;

    AuctionEntry* auction = auctionHouse->GetAuction(auctionId);

    Player* player = GetPlayer();

    if (!auction || auction->owner == player->GetGUID())
    {
        //you cannot bid your own auction:
        SendAuctionCommandResult(NULL, AUCTION_PLACE_BID, ERR_AUCTION_BID_OWN);
        return;
    }

    // impossible have online own another character (use this for speedup check in case online owner)
    Player* auction_owner = ObjectAccessor::FindPlayer(auction->owner);
    if (!auction_owner && sCharacterCache->GetCharacterAccountIdByGuid(auction->owner) == player->GetSession()->GetAccountId())
    {
        //you cannot bid your another character auction:
        SendAuctionCommandResult(NULL, AUCTION_PLACE_BID, ERR_AUCTION_BID_OWN);
        return;
    }

    // cheating
    if (price <= auction->bid || price < auction->startbid)
        return;

    // price too low for next bid if not buyout
    if ((price < auction->buyout || auction->buyout == 0) &&
        price < auction->bid + auction->GetAuctionOutBid())
    {
        // client already test it but just in case ...
        SendAuctionCommandResult(auction, AUCTION_PLACE_BID, ERR_AUCTION_HIGHER_BID);
        return;
    }

    if (!player->HasEnoughMoney(price))
    {
        // client already test it but just in case ...
        SendAuctionCommandResult(auction, AUCTION_PLACE_BID, ERR_AUCTION_NOT_ENOUGHT_MONEY);
        return;
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    if (price < auction->buyout || auction->buyout == 0)
    {
        if (auction->bidder > 0)
        {
            if (auction->bidder == player->GetGUID().GetCounter())
                player->ModifyMoney(-int64(price - auction->bid));
            else
            {
                // mail to last bidder and return money
                sAuctionMgr->SendAuctionOutbiddedMail(auction, price, GetPlayer(), trans);
                player->ModifyMoney(-int64(price));
            }
        }
        else
            player->ModifyMoney(-int64(price));

        auction->bidder = player->GetGUID();
        auction->bid = price;
        GetPlayer()->UpdateCriteria(CRITERIA_TYPE_HIGHEST_AUCTION_BID, price);

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_AUCTION_BID);
        stmt->setUInt32(0, auction->bidder.GetCounter());
        stmt->setUInt64(1, auction->bid);
        stmt->setUInt32(2, auction->Id);
        trans->Append(stmt);

        SendAuctionCommandResult(auction, AUCTION_PLACE_BID, ERR_AUCTION_OK);

        if (auction_owner)
            auction_owner->GetSession()->SendAuctionOwnerBidNotification(auction, player->GetGUID(), price);
    }
    else
    {
        //buyout:
        if (player->GetGUID() == auction->bidder)
            player->ModifyMoney(-int64(auction->buyout - auction->bid));
        else
        {
            player->ModifyMoney(-int64(auction->buyout));

            //buyout for bidded auction ..
            if (!auction->bidder.IsEmpty())
                sAuctionMgr->SendAuctionOutbiddedMail(auction, auction->buyout, GetPlayer(), trans);
        }

        auction->bidder = player->GetGUID();
        auction->bid = auction->buyout;
        GetPlayer()->UpdateCriteria(CRITERIA_TYPE_HIGHEST_AUCTION_BID, auction->buyout);

        //- Mails must be under transaction control too to prevent data loss
        sAuctionMgr->SendAuctionSalePendingMail(auction, trans);
        sAuctionMgr->SendAuctionSuccessfulMail(auction, trans);
        sAuctionMgr->SendAuctionWonMail(auction, trans);

        SendAuctionCommandResult(auction, AUCTION_PLACE_BID, ERR_AUCTION_OK);

        auction->DeleteFromDB(trans);

        sAuctionMgr->RemoveAItem(auction->item);
        auctionHouse->RemoveAuction(auction);
    }

    player->SaveInventoryAndGoldToDB(trans);
    CharacterDatabase.CommitTransaction(trans);
}

void WorldSession::HandleAuctionRemoveItem(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_REMOVE_ITEM");

    ObjectGuid auctioneer;
    uint32 auctionId = 0;

    recvData >> auctionId;

    recvData.ReadGuidMask(auctioneer, 6, 4, 2, 3, 7, 5, 1, 0);
    recvData.ReadGuidBytes(auctioneer, 3, 2, 6, 5, 4, 7, 0, 1);

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionRemoveItem - %s not found or you can't interact with him.", auctioneer.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());
    if (!auctionHouse)
        return;

    AuctionEntry* auction = auctionHouse->GetAuction(auctionId);

    Player* player = GetPlayer();

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    if (auction && auction->owner == player->GetGUID())
    {
        Item* pItem = sAuctionMgr->GetAItem(auction->item);
        if (pItem)
        {
            // If we have a bidder, we have to send him the money he paid
            if (!auction->bidder.IsEmpty())
            {
                uint64 auctionCut = auction->GetAuctionCut();

                //player doesn't have enough money, maybe message needed
                if (!player->HasEnoughMoney(auctionCut))
                    return;

                sAuctionMgr->SendAuctionCancelledToBidderMail(auction, trans, pItem);
                player->ModifyMoney(-int64(auctionCut));
            }

            // item will deleted or added to received mail list
            MailDraft(auction->BuildAuctionMailSubject(AUCTION_CANCELED), AuctionEntry::BuildAuctionMailBody(ObjectGuid::Empty, 0, auction->buyout, auction->deposit, 0))
                .AddItem(pItem)
                .SendMailTo(trans, player, auction, MAIL_CHECK_MASK_COPIED);
        }
        else
        {
            TC_LOG_ERROR("network", "Auction id: %u got non existing item (item guid : %u)!", auction->Id, auction->item.ToString().c_str());
            SendAuctionCommandResult(NULL, AUCTION_CANCEL, ERR_AUCTION_DATABASE_ERROR);
            return;
        }
    }
    else
    {
        SendAuctionCommandResult(NULL, AUCTION_CANCEL, ERR_AUCTION_DATABASE_ERROR);
        //this code isn't possible ... maybe there should be assert
        TC_LOG_ERROR("entities.player.cheat", "CHEATER: %s tried to cancel auction (id: %u) of another player or auction is NULL", player->GetGUID().ToString().c_str(), auctionId);
        return;
    }

    //inform player, that auction is removed
    SendAuctionCommandResult(auction, AUCTION_CANCEL, ERR_AUCTION_OK);

    // Now remove the auction

    player->SaveInventoryAndGoldToDB(trans);
    auction->DeleteFromDB(trans);
    CharacterDatabase.CommitTransaction(trans);

    sAuctionMgr->RemoveAItem(auction->item);
    auctionHouse->RemoveAuction(auction);
}

void WorldSession::HandleAuctionListBidderItems(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_LIST_BIDDER_ITEMS");

    ObjectGuid auctioneer;
    uint32 listFrom = 0;
    uint32 outbiddedCount = 0;

    std::vector<uint32> outbiddedAuctionIds;

    recvData >> listFrom;

    recvData.ReadGuidMask(auctioneer, 3, 4, 1, 5, 6, 2);

    outbiddedCount = recvData.ReadBits(7);

    outbiddedAuctionIds.resize(outbiddedCount);

    recvData.ReadGuidMask(auctioneer, 7, 0);

    recvData.FlushBits();

    recvData.ReadGuidBytes(auctioneer, 3, 4, 1, 0, 2, 5);

    for (uint32 i = 0; i < outbiddedCount; i++)
        recvData >> outbiddedAuctionIds[i];

    recvData.ReadGuidBytes(auctioneer, 7, 6);

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionListBidderItems - %s not found or you can't interact with him.", auctioneer.ToString().c_str());
        recvData.rfinish();
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());
    if (!auctionHouse)
        return;

    ByteBuffer bidderListItemsData;

    uint32 totalcount = 0;
    uint32 count = 0;

    for (uint32 i = 0; i < outbiddedCount; i++)
        if (AuctionEntry* auction = auctionHouse->GetAuction(outbiddedAuctionIds[i]))
        {
            if (auction->BuildAuctionInfo(bidderListItemsData))
                ++count;

            ++totalcount;
        }

    auctionHouse->BuildListBidderItems(bidderListItemsData, GetPlayer(), count, totalcount);

    WorldPacket data(SMSG_AUCTION_BIDDER_LIST_RESULT, 4 + 4 + 4 + count * (4 + 4 + PROP_ENCHANTMENT_SLOT_0 * (4 + 4 + 4) + 4 + 4 + 4 + 4 + 4 + 4 +
                    8 + 8 + 8 + 8 + 4 + 8 + 8));

    data << uint32(count);

    data.append(bidderListItemsData);

    data << uint32(totalcount);
    data << uint32(sWorld->getIntConfig(CONFIG_AUCTION_SEARCH_DELAY));

    SendPacket(&data);
}

void WorldSession::HandleAuctionListOwnerItems(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_LIST_OWNER_ITEMS");

    uint32 listfrom = 0;
    ObjectGuid auctioneer;

    recvData >> listfrom;

    recvData.ReadGuidMask(auctioneer, 4, 5, 2, 1, 7, 0, 3, 6);
    recvData.ReadGuidBytes(auctioneer, 5, 7, 3, 6, 4, 2, 0, 1);

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionListOwnerItems - %s not found or you can't interact with him.", auctioneer.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());
    if (!auctionHouse)
        return;

    ByteBuffer ownerListItemsData;

    uint32 count = 0;
    uint32 totalcount = 0;

    auctionHouse->BuildListOwnerItems(ownerListItemsData, _player, count, totalcount);

    WorldPacket data(SMSG_AUCTION_OWNER_LIST_RESULT, 4 + 4 + 4 + count * (4 + 4 + PROP_ENCHANTMENT_SLOT_0 * (4 + 4 + 4) + 4 + 4 + 4 + 4 + 4 + 4 +
                    8 + 8 + 8 + 8 + 4 + 8 + 8));

    data << uint32(count);

    data.append(ownerListItemsData);

    data << uint32(totalcount);
    data << uint32(sWorld->getIntConfig(CONFIG_AUCTION_SEARCH_DELAY));

    SendPacket(&data);
}

void WorldSession::HandleAuctionListItems(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_LIST_ITEMS");

    ObjectGuid auctioneer;
    std::string searchString;
    uint8 levelMin = 0;
    uint8 levelMax = 0;
    uint8 searchStringLen;

    uint32 listFrom = 0;
    int32 auctionSlotId = 0;
    int32 auctionMainCategory = 0;
    int32 auctionSubCategory = 0;
    int32 quality = 0;
    int32 sortCount = 0;

    bool usableItems = false;
    bool exactMatch = false;

    recvData >> auctionSlotId;
    recvData >> listFrom;
    recvData >> auctionMainCategory;

    recvData.read_skip<uint8>(); // sort Type

    recvData >> levelMax;
    recvData >> levelMin;
    recvData >> quality;
    recvData >> auctionSubCategory;
    recvData >> sortCount;

    // this block looks like it uses some lame byte packing or similar...
    for (uint8 i = 0; i < sortCount; i++)
        recvData.read_skip<uint8>();                    // sorts currently unhandled

    recvData.ReadGuidMask(auctioneer, 3, 4, 5, 2);

    exactMatch = recvData.ReadBit();
    usableItems = recvData.ReadBit();

    recvData.ReadGuidMask(auctioneer, 7, 0);

    searchStringLen = recvData.ReadBits(8);

    recvData.ReadGuidMask(auctioneer, 1, 6);

    recvData.FlushBits();

    recvData.ReadGuidBytes(auctioneer, 6, 3, 4, 0, 7, 2);

    searchString = recvData.ReadString(searchStringLen);

    recvData.ReadGuidBytes(auctioneer, 1, 5);

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleAuctionListItems - %s not found or you can't interact with him.", auctioneer.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());
    if (!auctionHouse)
        return;

    TC_LOG_DEBUG("auctionHouse", "Auctionhouse search (%s) list from: %u, searchedname: %s, levelmin: %u, levelmax: %u, auctionSlotID: %u, auctionMainCategory: %u, auctionSubCategory: %u, quality: %u, usable: %u",
        auctioneer.ToString().c_str(), listFrom, searchString.c_str(), levelMin, levelMax, auctionSlotId, auctionMainCategory, auctionSubCategory, quality, usableItems);

    // converting string that we try to find to lower case
    std::wstring wsearchedname;
    if (!Utf8toWStr(searchString, wsearchedname))
        return;

    wstrToLower(wsearchedname);

    ByteBuffer auctionListItemsData;

    uint32 count = 0;
    uint32 totalcount = 0;

    auctionHouse->BuildListAuctionItems(auctionListItemsData, _player,
        wsearchedname, listFrom, levelMin, levelMax, usableItems,
        auctionSlotId, auctionMainCategory, auctionSubCategory, quality,
        count, totalcount);

    WorldPacket data(SMSG_AUCTION_LIST_RESULT, 4 + 4 + 4 + count * (4 + 4 + PROP_ENCHANTMENT_SLOT_0 * (4 + 4 + 4) + 4 + 4 + 4 + 4 + 4 + 4 +
                    8 + 8 + 8 + 8 + 4 + 8 + 8));

    data << uint32(count);

    data.append(auctionListItemsData);

    data << uint32(totalcount);
    data << uint32(sWorld->getIntConfig(CONFIG_AUCTION_SEARCH_DELAY));

    SendPacket(&data);
}

void WorldSession::HandleAuctionListPendingSales(WorldPacket& /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_AUCTION_LIST_PENDING_SALES");

    uint32 count = 0;

    WorldPacket data(SMSG_AUCTION_LIST_PENDING_SALES);
    data << uint32(count);
    SendPacket(&data);
}

void WorldSession::HandleReplicateItems(WorldPacket& recvData)
{
    ObjectGuid Auctioneer;

    uint32 ChangeNumberGlobal = 0;
    uint32 ChangeNumberCursor = 0;
    uint32 ChangeNumberTombstone = 0;
    uint32 Count = 0;

    recvData >> ChangeNumberGlobal;
    recvData >> ChangeNumberCursor;
    recvData >> ChangeNumberTombstone;
    recvData >> Count;

    recvData.ReadGuidMask(Auctioneer, 6, 0, 1, 5, 3, 2, 4, 7);
    recvData.ReadGuidBytes(Auctioneer, 7, 0, 6, 4, 1, 5, 3, 2);

    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(Auctioneer, UNIT_NPC_FLAG_AUCTIONEER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleReplicateItems - %s not found or you can't interact with him.", Auctioneer.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    AuctionHouseObject* auctionHouse = sAuctionMgr->GetAuctionsMap(creature->GetFaction());
    if (!auctionHouse)
        return;

    ByteBuffer auctionReplicateItemsData;

    auctionHouse->BuildReplicate(auctionReplicateItemsData, GetPlayer(), ChangeNumberGlobal, ChangeNumberCursor, ChangeNumberTombstone, Count);

    WorldPacket data(SMSG_AUCTION_REPLICATE_RESPONSE, 4 + 4 + 4 + 4 + 4 + 4 + Count * (4 + 4 + PROP_ENCHANTMENT_SLOT_0 * (4 + 4 + 4) + 4 + 4 + 4 + 4 + 4 + 4 +
        8 + 8 + 8 + 8 + 4 + 8 + 8));

    data << uint32(sWorld->getIntConfig(CONFIG_AUCTION_SEARCH_DELAY));
    data << uint32(0); // Result
    data << uint32(ChangeNumberCursor);
    data << uint32(ChangeNumberTombstone);
    data << uint32(ChangeNumberGlobal);
    data << uint32(Count);

    data.append(auctionReplicateItemsData);

    SendPacket(&data);
}
