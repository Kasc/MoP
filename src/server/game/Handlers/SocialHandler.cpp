/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CharacterCache.h"
#include "Common.h"
#include "Language.h"
#include "DatabaseEnv.h"
#include "WorldPacket.h"
#include "Opcodes.h"
#include "Log.h"
#include "Player.h"
#include "SocialMgr.h"
#include "DBCEnums.h"
#include "ScriptMgr.h"
#include "WorldSession.h"

void WorldSession::HandleContactListOpcode(WorldPacket& recvData)
{
    uint32 flags = 0;
    recvData >> flags;

    _player->GetSocial()->SendSocialList(_player, flags);
}

void WorldSession::HandleAddFriendOpcode(WorldPacket& recvData)
{
    std::string friendName;
    std::string friendNote;

    recvData >> friendName;
    recvData >> friendNote;

    if (!normalizePlayerName(friendName))
        return;

    TC_LOG_DEBUG("network", "WORLD: %s asked to add friend : '%s'",
        GetPlayer()->GetName().c_str(), friendName.c_str());

    FriendsResult friendResult = FRIEND_NOT_FOUND;
    ObjectGuid friendGuid = sCharacterCache->GetCharacterGuidByName(friendName);
    if (!friendGuid.IsEmpty())
    {
        if (CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(friendGuid))
        {
            uint32 team = Player::TeamForRace(characterInfo->Race);
            uint32 friendAccountId = characterInfo->AccountId;

            if (HasPermission(rbac::RBAC_PERM_ALLOW_GM_FRIEND) || AccountMgr::IsPlayerAccount(AccountMgr::GetSecurity(friendAccountId, realmID)))
            {
                if (friendGuid)
                {
                    if (friendGuid == GetPlayer()->GetGUID())
                        friendResult = FRIEND_SELF;
                    else if (GetPlayer()->GetTeam() != team && !HasPermission(rbac::RBAC_PERM_TWO_SIDE_ADD_FRIEND))
                        friendResult = FRIEND_ENEMY;
                    else if (GetPlayer()->GetSocial()->HasFriend(friendGuid))
                        friendResult = FRIEND_ALREADY;
                    else
                    {
                        Player* pFriend = ObjectAccessor::FindPlayer(friendGuid);
                        if (pFriend && pFriend->IsVisibleGloballyFor(GetPlayer()))
                            friendResult = FRIEND_ADDED_ONLINE;
                        else
                            friendResult = FRIEND_ADDED_OFFLINE;
                        if (GetPlayer()->GetSocial()->AddToSocialList(friendGuid, SOCIAL_FLAG_FRIEND))
                            GetPlayer()->GetSocial()->SetFriendNote(friendGuid, friendNote);
                        else
                            friendResult = FRIEND_LIST_FULL;
                    }
                }

                GetPlayer()->GetSocial()->SetFriendNote(friendGuid, friendNote);
            }
        }
    }

    sSocialMgr->SendFriendStatus(GetPlayer(), friendResult, friendGuid, false);
}

void WorldSession::HandleDelFriendOpcode(WorldPacket& recvData)
{
    uint64 FriendGUID = 0;

    recvData >> FriendGUID;

    _player->GetSocial()->RemoveFromSocialList(ObjectGuid(FriendGUID), false);

    sSocialMgr->SendFriendStatus(GetPlayer(), FRIEND_REMOVED, ObjectGuid(FriendGUID), false);
}

void WorldSession::HandleAddIgnoreOpcode(WorldPacket& recvData)
{
    std::string ignoreName;

    recvData >> ignoreName;

    if (!normalizePlayerName(ignoreName))
        return;

    TC_LOG_DEBUG("network", "WORLD: %s asked to Ignore: '%s'",
        GetPlayer()->GetName().c_str(), ignoreName.c_str());

    ObjectGuid ignoreGuid = sCharacterCache->GetCharacterGuidByName(ignoreName);
    FriendsResult ignoreResult = FRIEND_IGNORE_NOT_FOUND;
    if (!ignoreGuid.IsEmpty())
    {
        if (ignoreGuid == GetPlayer()->GetGUID())              //not add yourself
            ignoreResult = FRIEND_IGNORE_SELF;
        else if (GetPlayer()->GetSocial()->HasIgnore(ignoreGuid))
            ignoreResult = FRIEND_IGNORE_ALREADY;
        else
        {
            ignoreResult = FRIEND_IGNORE_ADDED;

            // ignore list full
            if (!GetPlayer()->GetSocial()->AddToSocialList(ignoreGuid, true))
                ignoreResult = FRIEND_IGNORE_FULL;
        }
    }

    sSocialMgr->SendFriendStatus(GetPlayer(), ignoreResult, ignoreGuid, false);
}

void WorldSession::HandleDelIgnoreOpcode(WorldPacket& recvData)
{
    uint64 IgnoreGUID = 0;

    recvData >> IgnoreGUID;

    _player->GetSocial()->RemoveFromSocialList(ObjectGuid(IgnoreGUID), true);

    sSocialMgr->SendFriendStatus(GetPlayer(), FRIEND_IGNORE_REMOVED, ObjectGuid(IgnoreGUID), false);
}

void WorldSession::HandleSetContactNotesOpcode(WorldPacket& recvData)
{
    uint64 guid = 0;

    std::string note;

    recvData >> guid;

    recvData >> note;

    _player->GetSocial()->SetFriendNote(ObjectGuid(guid), note);
}
