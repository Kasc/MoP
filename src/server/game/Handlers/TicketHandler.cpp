/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "zlib.h"
#include "Common.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Player.h"
#include "SupportMgr.h"
#include "Util.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"

void WorldSession::HandleGMTicketCreateOpcode(WorldPacket& recvData)
{
    // Don't accept tickets if the ticket queue is disabled. (Ticket UI is greyed out but not fully dependable)
    if (sSupportMgr->GetSupportSystemStatus() == GMTICKET_QUEUE_STATUS_DISABLED)
    {
        recvData.rfinish();
        return;
    }

    if (GetPlayer()->GetLevel() < sWorld->getIntConfig(CONFIG_TICKET_LEVEL_REQ))
    {
        SendNotification(GetTrinityString(LANG_TICKET_REQ), sWorld->getIntConfig(CONFIG_TICKET_LEVEL_REQ));
        recvData.rfinish();
        return;
    }

    GMTicketResponse response = GMTICKET_RESPONSE_CREATE_ERROR;
    GmTicket* ticket = sSupportMgr->GetGmTicketByPlayerGuid(GetPlayer()->GetGUID());

    if (ticket && ticket->IsCompleted())
        sSupportMgr->CloseTicket<GmTicket>(ticket->GetId(), GetPlayer()->GetGUID());

    // Player must not have ticket
    if (!ticket || ticket->IsClosed())
    {
        uint32 mapId = 0;
        uint32 chatLogLength = 0;
        uint32 messageLength = 0;

        uint8 Flags = 0;

        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
        
        bool needResponse = false;
        bool needMoreHelp = false;
        
        std::string message;
        std::string chatLog;

        recvData >> mapId;
        recvData >> z;
        recvData >> y;
        recvData >> Flags;
        recvData >> x;

        recvData >> chatLogLength;
        chatLog = recvData.ReadString(chatLogLength);

        needResponse = recvData.ReadBit();
        needMoreHelp = recvData.ReadBit();

        messageLength = recvData.ReadBits(11);

        message = recvData.ReadString(messageLength);

        ticket = new GmTicket(GetPlayer());

        G3D::Vector3 pos(x, y, z);
        ticket->SetPosition(mapId, pos);
        ticket->SetDescription(message);
        ticket->SetGmAction(needResponse, needMoreHelp);

        if (!chatLog.empty())
            ticket->SetChatLog(chatLog);

        sSupportMgr->AddTicket(ticket);
        sSupportMgr->UpdateLastChange();

        sWorld->SendGMText(LANG_COMMAND_TICKETNEW, GetPlayer()->GetName().c_str(), ticket->GetId());

        response = GMTICKET_RESPONSE_CREATE_SUCCESS;
    }

    sSupportMgr->SendGmTicketUpdate(this, response);
}

void WorldSession::HandleGMTicketUpdateOpcode(WorldPacket& recvData)
{
    uint32 messageLength = 0;

    std::string message;

    messageLength = recvData.ReadBits(11);
    message = recvData.ReadString(messageLength);

    GMTicketResponse response = GMTICKET_RESPONSE_UPDATE_ERROR;
    if (GmTicket* ticket = sSupportMgr->GetGmTicketByPlayerGuid(GetPlayer()->GetGUID()))
    {
        SQLTransaction trans = SQLTransaction(NULL);
        ticket->SetDescription(message);
        ticket->SaveToDB(trans);

        sWorld->SendGMText(LANG_COMMAND_TICKETUPDATED, GetPlayer()->GetName().c_str(), ticket->GetId());

        response = GMTICKET_RESPONSE_UPDATE_SUCCESS;
    }

    sSupportMgr->SendGmTicketUpdate(this, response);
}

void WorldSession::HandleGMTicketDeleteOpcode(WorldPacket& /*recvData*/)
{
    if (GmTicket* ticket = sSupportMgr->GetGmTicketByPlayerGuid(GetPlayer()->GetGUID()))
    {
        sSupportMgr->SendGmTicketUpdate(this, GMTICKET_RESPONSE_TICKET_DELETED);

        sWorld->SendGMText(LANG_COMMAND_TICKETPLAYERABANDON, GetPlayer()->GetName().c_str(), ticket->GetId());

        sSupportMgr->CloseTicket<GmTicket>(ticket->GetId(), GetPlayer()->GetGUID());
        sSupportMgr->SendGmTicket(this, NULL);
    }
}

void WorldSession::HandleGMTicketGetTicketOpcode(WorldPacket& /*recvData*/)
{
    SendQueryTimeResponse();

    if (GmTicket* ticket = sSupportMgr->GetGmTicketByPlayerGuid(GetPlayer()->GetGUID()))
    {
        if (ticket->IsCompleted())
            ticket->SendResponse(this);
        else
            sSupportMgr->SendGmTicket(this, ticket);
    }
    else
        sSupportMgr->SendGmTicket(this, NULL);
}

void WorldSession::HandleGMTicketGetWebTicketsOpcode(WorldPacket& /*recvData*/)
{
    // Not implemented
}

void WorldSession::HandleGMTicketSystemStatusOpcode(WorldPacket& /*recvData*/)
{
    // Note: This only disables the ticket UI at client side and is not fully reliable
    // Note: This disables the whole customer support UI after trying to send a ticket in disabled state (MessageBox: "GM Help Tickets are currently unavaiable.").
    // Note: UI remains disabled until the character relogs.

    WorldPacket data(SMSG_GM_TICKET_SYSTEM_STATUS, 4);

    data << uint32(sSupportMgr->GetSupportSystemStatus() ? GMTICKET_QUEUE_STATUS_ENABLED : GMTICKET_QUEUE_STATUS_DISABLED);

    SendPacket(&data);
}

void WorldSession::HandleGMResponseResolve(WorldPacket& /*recvData*/)
{
    if (GmTicket* ticket = sSupportMgr->GetGmTicketByPlayerGuid(GetPlayer()->GetGUID()))
    {
        bool showSurvey = true;
        if (Math::RollUnder(sWorld->getFloatConfig(CONFIG_CHANCE_OF_GM_SURVEY)))
            showSurvey = true;

        WorldPacket data(SMSG_GM_TICKET_RESOLVE_RESPONSE, 1);
        data.WriteBit(showSurvey);
        data.FlushBits();
        SendPacket(&data);

        sSupportMgr->SendGmTicketUpdate(this, GMTICKET_RESPONSE_TICKET_DELETED);

        sSupportMgr->CloseTicket<GmTicket>(ticket->GetId(), GetPlayer()->GetGUID());
        sSupportMgr->SendGmTicket(this, NULL);
    }
}

void WorldSession::HandleGMSurveySubmit(WorldPacket& recvData)
{
    uint32 SurveyID = 0;
    uint32 CommentLength = 0;
    uint32 QuestionsCount = 0;

    std::string Comment;

    recvData >> SurveyID;

    QuestionsCount = recvData.ReadBits(4);

    std::vector<uint32> QuestionAnswerCommentLengths(QuestionsCount);
    for (uint32 i = 0; i < QuestionsCount; ++i)
    {
        uint32 AnswerCommentLength = recvData.ReadBits(9);

        QuestionAnswerCommentLengths[i] = AnswerCommentLength;
    }

    CommentLength = recvData.ReadBits(11);

    Comment = recvData.ReadString(CommentLength);

    uint32 nextSurveyID = sSupportMgr->GetNextSurveyID();

    std::unordered_set<uint32> surveyIds;
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    for (uint32 i = 0; i < QuestionsCount; ++i)
    {
        uint32 QuestionID = 0;
        uint8 Answer = 0;

        std::string AnswerComment;

        recvData >> QuestionID;
        recvData >> Answer;

        AnswerComment = recvData.ReadString(QuestionAnswerCommentLengths[i]);

        if (!QuestionID)
            break;

        // make sure the same sub survey is not added to DB twice
        if (!surveyIds.insert(QuestionID).second)
            continue;

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GM_SUBSURVEY);
        stmt->setUInt32(0, nextSurveyID);
        stmt->setUInt32(1, QuestionID);     // ref to i'th GMSurveySurveys.dbc field (all fields in that dbc point to fields in GMSurveyQuestions.dbc)
        stmt->setUInt32(2, uint32(Answer)); // probably some sort of ref to GMSurveyAnswers.dbc
        stmt->setString(3, AnswerComment);  // comment ("Usage: GMSurveyAnswerSubmit(question, rank, comment)")
        trans->Append(stmt);
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GM_SURVEY);
    stmt->setUInt32(0, GetPlayer()->GetGUID().GetCounter());
    stmt->setUInt32(1, nextSurveyID);
    stmt->setUInt32(2, SurveyID);   // GMSurveyCurrentSurvey.dbc, column 1 (all 9) ref to GMSurveySurveys.dbc
    stmt->setString(3, Comment);

    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);
}

void WorldSession::HandleSupportTicketSubmitBug(WorldPacket& recvData)
{
    if (!sSupportMgr->GetFeedbackSystemStatus())
    {
        recvData.rfinish();
        return;
    }

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    float facing = 0.0f;

    int32 mapID = -1;
    uint32 noteLength = 0;

    std::string note;

    recvData >> y;
    recvData >> x;
    recvData >> z;
    recvData >> facing;
    recvData >> mapID;

    noteLength = recvData.ReadBits(10);

    note = recvData.ReadString(noteLength);

    Vector3 position(x, y, z);

    BugTicket* ticket = new BugTicket(GetPlayer());
    ticket->SetPosition(mapID, position);
    ticket->SetFacing(facing);
    ticket->SetNote(note);

    sSupportMgr->AddTicket(ticket);
}

void WorldSession::HandleSupportTicketSubmitSuggestion(WorldPacket& recvData)
{
    if (!sSupportMgr->GetFeedbackSystemStatus())
    {
        recvData.rfinish();
        return;
    }

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    float facing = 0.0f;

    int32 mapID = -1;
    uint32 noteLength = 0;

    std::string note;

    recvData >> z;
    recvData >> facing;
    recvData >> y;
    recvData >> mapID;
    recvData >> x;

    noteLength = recvData.ReadBits(10);

    note = recvData.ReadString(noteLength);

    Vector3 position(x, y, z);

    SuggestionTicket* ticket = new SuggestionTicket(GetPlayer());
    ticket->SetPosition(mapID, position);
    ticket->SetFacing(facing);
    ticket->SetNote(note);

    sSupportMgr->AddTicket(ticket);
}

void WorldSession::HandleSupportTicketSubmitComplaint(WorldPacket& recvData)
{
    if (!sSupportMgr->GetFeedbackSystemStatus())
    {
        recvData.rfinish();
        return;
    }

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    float facing = 0.0f;

    int32 mapID = -1;

    uint32 noteLength = 0;
    uint32 linesCount = 0;

    uint8 complaintType = 0;

    bool hasMailInfo = false;
    bool hasCalendarInfo = false;
    bool hasPetInfo = false;
    bool hasGuildInfo = false;
    bool hasReportLineIndex = false;

    ObjectGuid characterGUID;

    std::string note;

    SupportTicketChatLog chatLog;

    recvData >> y;
    recvData >> x;
    recvData >> z;
    recvData >> mapID;
    recvData >> facing;

    recvData.ReadGuidMask(characterGUID, 5);

    hasCalendarInfo = recvData.ReadBit();

    linesCount = recvData.ReadBits(21);

    recvData.ReadGuidMask(characterGUID, 1, 7);

    hasMailInfo = recvData.ReadBit();
    hasGuildInfo = recvData.ReadBit();
    hasPetInfo = recvData.ReadBit();

    recvData.ReadGuidMask(characterGUID, 0);

    std::vector<uint32> LinesTextLengths(linesCount);
    for (uint32 i = 0; i < linesCount; ++i)
    {
        uint32 TextLength = recvData.ReadBits(12);

        LinesTextLengths[i] = TextLength;
    }

    noteLength = recvData.ReadBits(10);

    complaintType = recvData.ReadBits(5);

    hasReportLineIndex = recvData.ReadBit();

    recvData.ReadGuidMask(characterGUID, 2, 6, 4, 3);

    ObjectGuid eventID;
    ObjectGuid inviteID;
    uint32 eventTitleLength = 0;
    if (hasCalendarInfo)
    {
        recvData.ReadGuidMask(eventID, 0, 4);

        recvData.ReadGuidMask(inviteID, 6);

        eventTitleLength = recvData.ReadBits(8);

        recvData.ReadGuidMask(eventID, 3);

        recvData.ReadGuidMask(inviteID, 1);

        recvData.ReadGuidMask(eventID, 1);

        recvData.ReadGuidMask(inviteID, 5, 4);

        recvData.ReadGuidMask(eventID, 7);

        recvData.ReadGuidMask(inviteID, 0, 2, 3);

        recvData.ReadGuidMask(eventID, 5, 2, 6);

        recvData.ReadGuidMask(inviteID, 7);
    }

    ObjectGuid petGuid;
    uint32 petNameLength = 0;
    if (hasPetInfo)
    {
        recvData.ReadGuidMask(petGuid, 4, 1, 3, 0);

        petNameLength = recvData.ReadBits(7);

        recvData.ReadGuidMask(petGuid, 6, 2, 7, 5);
    }

    ObjectGuid guildGuid;
    uint32 guildNameLength = 0;
    if (hasGuildInfo)
    {
        recvData.ReadGuidMask(guildGuid, 7, 4, 0, 5);

        guildNameLength = recvData.ReadBits(8);

        recvData.ReadGuidMask(guildGuid, 2, 1, 3, 6);
    }

    uint32 mailBodyLength = 0;
    uint32 mailSubjectLength = 0;
    if (hasMailInfo)
    {
        mailSubjectLength = recvData.ReadBits(9);
        mailBodyLength = recvData.ReadBits(13);
    }

    recvData.ReadGuidBytes(characterGUID, 7);

    for (uint32 i = 0; i < linesCount; ++i)
    {
        uint32 Timestamp = 0;

        std::string Text;

        recvData >> Timestamp;

        Text = recvData.ReadString(LinesTextLengths[i]);

        SupportTicketChatLine chatLine(Timestamp, Text);
        chatLog.Lines.push_back(std::move(chatLine));
    }

    recvData.ReadGuidBytes(characterGUID, 5, 3, 1, 4, 6, 0, 2);

    note = recvData.ReadString(noteLength);

    if (hasCalendarInfo)
    {
        std::string eventTitle;

        recvData.ReadGuidBytes(inviteID, 3, 1);

        recvData.ReadGuidBytes(eventID, 7, 3, 5);

        recvData.ReadGuidBytes(inviteID, 7);

        recvData.ReadGuidBytes(eventID, 0);

        recvData.ReadGuidBytes(inviteID, 5, 2);

        recvData.ReadGuidBytes(eventID, 2);

        recvData.ReadGuidBytes(inviteID, 6, 0);

        recvData.ReadGuidBytes(eventID, 4, 6);

        eventTitle = recvData.ReadString(eventTitleLength);

        recvData.ReadGuidBytes(inviteID, 4);

        recvData.ReadGuidBytes(eventID, 1);
    }

    if (hasGuildInfo)
    {
        std::string guildName;

        recvData.ReadGuidBytes(guildGuid, 0, 2, 4);

        guildName = recvData.ReadString(guildNameLength);

        recvData.ReadGuidBytes(guildGuid, 5, 1, 6, 3, 7);
    }

    if (hasPetInfo)
    {
        std::string petName;

        recvData.ReadGuidBytes(petGuid, 3, 0, 5, 6);

        petName = recvData.ReadString(petNameLength);

        recvData.ReadGuidBytes(petGuid, 2, 4, 1, 7);
    }

    if (hasMailInfo)
    {
        uint32 mailID = 0;

        std::string mailBody;
        std::string mailSubject;

        recvData >> mailID;

        mailBody = recvData.ReadString(mailBodyLength);
        mailSubject = recvData.ReadString(mailSubjectLength);
    }

    if (hasReportLineIndex)
    {
        uint32 reportIndex = 0;
        recvData >> reportIndex;

        chatLog.ReportLineIndex = reportIndex;
    }

    Vector3 position(x, y, z);

    ComplaintTicket* comp = new ComplaintTicket(GetPlayer());
    comp->SetPosition(mapID, position);
    comp->SetFacing(facing);
    comp->SetChatLog(chatLog);
    comp->SetTargetCharacterGuid(characterGUID);
    comp->SetComplaintType(GMSupportComplaintType(complaintType));
    comp->SetNote(note);

    sSupportMgr->AddTicket(comp);
}

void WorldSession::HandleSpamComplainOpcode(WorldPacket& recvData)
{
    ObjectGuid SpammerGUID;
    uint8 SpamType = 0;

    uint32 RealmAddress = 0;
    uint32 TimeSinceOffence = 0;

    bool hasRealmAddress = false;
    bool hasTimeSinceOffence = false;
    bool hasSpammerGuid = false;

    recvData >> SpamType;

    hasTimeSinceOffence = !recvData.ReadBit();
    hasRealmAddress = !recvData.ReadBit();
    hasSpammerGuid = !recvData.ReadBit();

    bool hasCommand = false;
    bool hasChannelID = false;
    uint32 messageLogLength = 0;
    if (SpamType == SUPPORT_SPAM_TYPE_CHAT)
    {
        hasCommand = !recvData.ReadBit();

        messageLogLength = recvData.ReadBits(12);

        hasChannelID = !recvData.ReadBit();
    }

    recvData.ReadGuidMask(SpammerGUID, 4, 5, 6, 7, 3, 1, 2, 0);

    bool hasMailID = false;
    if (SpamType == SUPPORT_SPAM_TYPE_MAIL)
        hasMailID = !recvData.ReadBit();

    bool hasEventGuid = false;
    bool hasInviteGuid = false;
    ObjectGuid eventGuid;
    ObjectGuid inviteGuid;
    if (SpamType == SUPPORT_SPAM_TYPE_CALENDAR)
    {
        hasEventGuid = !recvData.ReadBit();

        recvData.ReadGuidMask(eventGuid, 5, 6, 2, 4, 7, 0, 3, 1);

        hasInviteGuid = !recvData.ReadBit();

        recvData.ReadGuidMask(inviteGuid, 6, 0, 5, 4, 3, 1, 7, 2);
        
        recvData.ReadGuidBytes(inviteGuid, 6, 0, 7, 1, 2, 5, 4, 3);

        recvData.ReadGuidBytes(eventGuid, 5, 7, 0, 1, 2, 6, 3, 4);
    }

    recvData.ReadGuidBytes(SpammerGUID, 0, 1, 4, 3, 6, 5, 2, 7);

    if (SpamType == SUPPORT_SPAM_TYPE_CHAT)
    {
        uint32 channelID = 0;
        uint32 commandID = 0;

        std::string MessageLog;

        if (hasChannelID)
            recvData >> channelID;

        if (hasCommand)
            recvData >> commandID;

        MessageLog = recvData.ReadString(messageLogLength);
    }

    uint32 mailID = 0;
    if (SpamType == SUPPORT_SPAM_TYPE_MAIL)
        if (hasMailID)
            recvData >> mailID;

    if (hasTimeSinceOffence)
        recvData >> TimeSinceOffence;

    if (hasRealmAddress)
        recvData >> RealmAddress;

    // NOTE: all chat messages from this spammer automatically ignored by spam reporter until logout in case chat spam.
    // if it's mail spam - ALL mails from this spammer automatically removed by client

    uint8 Result = 0; //value 1 resets CGChat::m_complaintsSystemStatus in client. (unused ? )

    WorldPacket data(SMSG_SPAM_COMPLAIN_RESULT, 4 + 1);

    data << uint32(SpamType);
    data << uint8(Result);

    SendPacket(&data);
}
