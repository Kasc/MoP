/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "BattlePetMgr.h"
#include "BattlePetSpawnMgr.h"
#include "BattlePetQueue.h"

#include "Common.h"
#include "DB2Enums.h"
#include "DB2Stores.h"
#include "Log.h"
#include "WorldSession.h"
#include "WorldPacket.h"

// -------------------------------------------------------------------------------
// Battle Pet
// -------------------------------------------------------------------------------

#define BATTLE_PET_MAX_DECLINED_NAMES 5

void WorldSession::HandleBattlePetModifyName(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_BATTLE_PET_MODIFY_NAME");

    ObjectGuid battlePetGUID;

    uint8 nicknameLen = 0;
    std::string nickname;

    bool hasDeclinedNames = false;

    uint8 declinedNameLen[BATTLE_PET_MAX_DECLINED_NAMES] = { };
    std::string declinedNames[BATTLE_PET_MAX_DECLINED_NAMES];

    recvData.ReadGuidMask(battlePetGUID, 5, 7, 3, 0, 6);

    nicknameLen = recvData.ReadBits(7);

    recvData.ReadGuidMask(battlePetGUID, 2, 1);

    hasDeclinedNames = recvData.ReadBit();

    recvData.ReadGuidMask(battlePetGUID, 4);

    if (hasDeclinedNames)
        for (uint8 i = 0; i < BATTLE_PET_MAX_DECLINED_NAMES; i++)
            declinedNameLen[i] = recvData.ReadBits(7);

    recvData.ReadGuidBytes(battlePetGUID, 3, 0, 6, 1, 5, 2, 4, 7);

    nickname = recvData.ReadString(nicknameLen);

    if (hasDeclinedNames)
        for (uint8 i = 0; i < BATTLE_PET_MAX_DECLINED_NAMES; i++)
            declinedNames[i] = recvData.ReadString(declinedNameLen[i]);

    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
    if (!battlePet)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_MODIFY_NAME - Player %s tried to set the name for Battle Pet %s which it doesn't own!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    // check if nickname is a valid length
    if (nickname.size() > BATTLE_PET_MAX_NAME_LENGTH)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_MODIFY_NAME - Player %s tried to set the name for Battle Pet %s with an invalid length!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    battlePet->SetNickname(nickname);
    battlePet->SetTimestamp(uint32(time(nullptr)));

    // update current summoned battle pet timestamp, this makes the client request the name again
    if (TempSummon* summon = battlePetMgr->GetCurrentSummon())
        summon->SetUInt32Value(UNIT_BATTLE_PET_COMPANION_NAME_TIMESTAMP, battlePet->GetTimestamp());
}

void WorldSession::HandleBattlePetRelease(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_BATTLE_PET_DELETE");

    ObjectGuid battlePetGUID;

    recvData.ReadGuidMask(battlePetGUID, 0, 4, 7, 6, 1, 5, 2, 3);
    recvData.ReadGuidBytes(battlePetGUID, 6, 1, 7, 0, 4, 3, 5, 2);

    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
    if (!battlePet)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_DELETE - Player %s tried to release Battle Pet %s which it doesn't own!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    if (!sDB2Manager->HasBattlePetSpeciesFlag(battlePet->GetSpecies(), BATTLE_PET_FLAG_RELEASABLE))
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_DELETE - Player %s tried to release Battle Pet %s which isn't releasable!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    GetPlayer()->GetBattlePetMgr()->Delete(battlePet);
}

void WorldSession::HandleBattlePetSetBattleSlot(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_BATTLE_PET_SET_BATTLE_SLOT");

    uint8 slot = 0;
    ObjectGuid battlePetGUID;

    recvData >> slot;

    recvData.ReadGuidMask(battlePetGUID, 4, 6, 5, 7, 3, 1, 0, 2);
    recvData.ReadGuidBytes(battlePetGUID, 1, 3, 5, 0, 7, 6, 4, 2);

    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
    if (!battlePet)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_SET_BATTLE_SLOT - Player %s tried to add Battle Pet %s to loadout which it doesn't own!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    if (!battlePetMgr->HasLoadoutSlot(slot))
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_SET_BATTLE_SLOT - Player %s tried to add Battle Pet %s into slot %u which is locked!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    // this check is also done clientside
    if (sDB2Manager->HasBattlePetSpeciesFlag(battlePet->GetSpecies(), BATTLE_PET_FLAG_COMPANION))
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_SET_BATTLE_SLOT - Player %s tried to add a compainion Battle Pet %s into slot %u!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    // sever handles slot swapping, find source slot and replace it with the destination slot
    uint8 srcSlot = battlePetMgr->GetLoadoutSlotForBattlePet(battlePetGUID);
    if (srcSlot != BATTLE_PET_LOADOUT_SLOT_NONE)
        battlePetMgr->SetLoadoutSlot(srcSlot, battlePetMgr->GetLoadoutSlot(slot), true);

    battlePetMgr->SetLoadoutSlot(slot, battlePetGUID, true);
}

void WorldSession::HandleBattlePetSetFlags(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_BATTLE_PET_SET_FLAGS");

    ObjectGuid battlePetGUID;
    uint32 flag = 0;
    uint8 mode = 0;

    recvData >> flag;

    recvData.ReadGuidMask(battlePetGUID, 5, 2);

    mode = recvData.ReadBits(2);

    recvData.ReadGuidMask(battlePetGUID, 1, 4, 6, 3, 7, 0);

    recvData.ReadGuidBytes(battlePetGUID, 4, 0, 7, 3, 1, 6, 2, 5);

    // check if player owns the battle pet
    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
    if (!battlePet)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_SET_FLAGS - Player %s tried to set the flags for Battle Pet %s which it doesn't own!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    // list of flags the client can currently change
    if (flag != BATTLE_PET_JOURNAL_FLAG_FAVORITES
        && flag != BATTLE_PET_JOURNAL_FLAG_ABILITY_1
        && flag != BATTLE_PET_JOURNAL_FLAG_ABILITY_2
        && flag != BATTLE_PET_JOURNAL_FLAG_ABILITY_3)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_SET_FLAGS - Player %s tryed to set an invalid Battle Pet flag %u!",
            GetPlayer()->GetGUID().ToString().c_str(), flag);
        return;
    }

    // handle flag
    switch (mode)
    {
        case BATTLE_PET_FLAG_MODE_SET:
            battlePet->SetFlag(flag);
            break;
        case BATTLE_PET_FLAG_MODE_UNSET:
            battlePet->UnSetFlag(flag);
            break;
    }

    battlePet->InitializeAbilities(false);
}

void WorldSession::HandleBattlePetQueryName(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_QUERY_BATTLE_PET_NAME");

    ObjectGuid battlePetGUID;
    ObjectGuid petGuid;

    recvData.ReadGuidMask(petGuid, 2);

    recvData.ReadGuidMask(battlePetGUID, 6, 3);

    recvData.ReadGuidMask(petGuid, 3);

    recvData.ReadGuidMask(battlePetGUID, 7);

    recvData.ReadGuidMask(petGuid, 4, 1, 0);

    recvData.ReadGuidMask(battlePetGUID, 0);

    recvData.ReadGuidMask(petGuid, 7, 5, 6);

    recvData.ReadGuidMask(battlePetGUID, 1, 2, 5, 4);

    recvData.ReadGuidBytes(petGuid, 5);

    recvData.ReadGuidBytes(battlePetGUID, 1);

    recvData.ReadGuidBytes(petGuid, 0);

    recvData.ReadGuidBytes(battlePetGUID, 4);

    recvData.ReadGuidBytes(petGuid, 3);

    recvData.ReadGuidBytes(battlePetGUID, 3);

    recvData.ReadGuidBytes(petGuid, 1, 6);

    recvData.ReadGuidBytes(battlePetGUID, 6, 0, 2);

    recvData.ReadGuidBytes(petGuid, 7, 2);

    recvData.ReadGuidBytes(battlePetGUID, 7);

    recvData.ReadGuidBytes(petGuid, 4);

    recvData.ReadGuidBytes(battlePetGUID, 5);

    Creature* petUnit = ObjectAccessor::GetCreature(*GetPlayer(), petGuid);
    if (!petUnit)
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_QUERY_NAME - Player %s queried the name of Battle Pet %s which doesnt't exist in world!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    TempSummon* summmon = petUnit->ToTempSummon();
    if (!summmon)
        return;

    Unit* ownerUnit = summmon->GetSummoner();
    if (!ownerUnit)
        return;

    Player* player = ownerUnit->ToPlayer();
    if (!player)
        return;

    BattlePetMgr* battlePetMgr = player->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetMgr->GetCurrentSummonGUID());
    if (!battlePet)
        return;

    BattlePetSpeciesEntry const* speciesEntry = sBattlePetSpeciesStore.LookupEntry(battlePet->GetSpecies());
    if (!speciesEntry)
        return;

    bool Allow = true;
    bool HasDeclinedNames = false;

    WorldPacket data(SMSG_BATTLE_PET_QUERY_NAME_RESPONSE, 8 + 4 + 4 + 2 + BATTLE_PET_MAX_DECLINED_NAMES * (1) + battlePet->GetNickname().size());

    data << uint64(battlePetGUID.GetRawValue());
    data << uint32(battlePet->GetTimestamp());
    data << uint32(speciesEntry->NpcId);

    data.WriteBit(Allow);

    if (Allow)
    {
        data.WriteBits(battlePet->GetNickname().size(), 8);

        for (uint8 i = 0; i < BATTLE_PET_MAX_DECLINED_NAMES; i++)
            data.WriteBits(0, 7);

        data.WriteBit(HasDeclinedNames);
    }

    data.FlushBits();

    if (Allow)
    {
        // Todo 5.4.8: finished declined names
        //for (uint8 i = 0; i < BATTLE_PET_MAX_DECLINED_NAMES; i++)
            //data.WriteString(battlePet->GetNickname());

        data.WriteString(battlePet->GetNickname());
    }

    SendPacket(&data);
}

void WorldSession::HandleBattlePetSummonCompanion(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_SUMMON_BATTLEPET_COMPANION");

    ObjectGuid battlePetGUID;

    recvData.ReadGuidMask(battlePetGUID, 3, 2, 5, 0, 7, 1, 6, 4);

    recvData.ReadGuidBytes(battlePetGUID, 6, 7, 3, 5, 0, 4, 1, 2);

    // check if player owns the battle pet
    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
    if (!battlePet)
    {
        TC_LOG_DEBUG("network", "CMSG_SUMMON_BATTLE_PET_COMPANION - Player %s tried to summon battle pet companion %s which it doesn't own!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    BattlePetSpeciesEntry const* species = sBattlePetSpeciesStore.LookupEntry(battlePet->GetSpecies());
    if (!species)
        return;

    // make sure the battle pet is alive
    if (!battlePet->GetCurrentHealth())
    {
        TC_LOG_DEBUG("network", "CMSG_SUMMON_BATTLE_PET_COMPANION - Player %s tried to summon battle pet companion %s which is dead!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    // player wants to unsummon the current battle pet
    if (battlePetMgr->GetCurrentSummonGUID() == battlePetGUID)
        battlePetMgr->UnSummonCurrentBattlePet(false);
    else
    {
        // player wants to summon a new battle pet
        battlePetMgr->SetCurrentSummonGUID(battlePetGUID);
        GetPlayer()->CastSpell(GetPlayer(), species->SpellId, false);
    }
}

void WorldSession::HandleBattlePetCage(WorldPacket& recvData)
{
    ObjectGuid battlePetGUID;

    recvData.ReadGuidMask(battlePetGUID, 5, 1, 4, 3, 0, 6, 2, 7);
    recvData.ReadGuidBytes(battlePetGUID, 5, 4, 2, 7, 1, 3, 6, 0);

    // check if player owns the battle pet
    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
        return;

    BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
    if (!battlePet)
        return;

    if (sDB2Manager->HasBattlePetSpeciesFlag(battlePet->GetSpecies(), BATTLE_PET_FLAG_NOT_CAGEABLE))
    {
        TC_LOG_DEBUG("network", "CMSG_BATTLE_PET_CAGE - Player %s tried to cage Battle Pet %s which isn't cageable!",
            GetPlayer()->GetGUID().ToString().c_str(), battlePetGUID.ToString().c_str());
        return;
    }

    // Store New Item
    ItemPosCountVec dest;
    InventoryResult msg = GetPlayer()->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, PET_CAGE, 1);
    if (msg != EQUIP_ERR_OK)
    {
        GetPlayer()->SendEquipError(msg, NULL, NULL, PET_CAGE);
        return;
    }

    Item* item = GetPlayer()->StoreNewItem(dest, PET_CAGE, true, Item::GenerateItemRandomPropertyId(PET_CAGE));

    item->SetModifier(ITEM_MODIFIER_BATTLE_PET_SPECIES_ID, battlePet->GetSpecies());
    item->SetModifier(ITEM_MODIFIER_BATTLE_PET_BREED_DATA, (battlePet->GetBreed() | (battlePet->GetQuality() << 24)));
    item->SetModifier(ITEM_MODIFIER_BATTLE_PET_LEVEL, battlePet->GetLevel());
    item->SetModifier(ITEM_MODIFIER_BATTLE_PET_DISPLAY_ID, battlePet->GetDisplayId());

    item->SetState(ITEM_CHANGED, GetPlayer());

    GetPlayer()->SendNewItem(item, 1, false, true);

    // Delete battle pet from journal
    battlePetMgr->Delete(battlePet);
}

// -------------------------------------------------------------------------------
// Pet Battle
// -------------------------------------------------------------------------------

void WorldSession::HandlePetBattleInput(WorldPacket& recvData)
{
    bool HasAbilityID = false;
    bool HasNewFrontPet = false;
    bool HasBattleInterrupted = false;
    bool HasIgnoreAbandonPenalty = false;
    bool HasDebugFlags = false;
    bool HasRoundID = false;
    bool HasMoveType = false;

    uint8 MoveType = 0;
    uint8 NewFrontPet = 0;
    uint8 DebugFlags = 0;
    uint8 BattleInterrupted = 0;
    uint32 AbilityID = 0;
    uint32 RoundID = 0;

    HasAbilityID = !recvData.ReadBit();
    HasNewFrontPet = !recvData.ReadBit();
    HasBattleInterrupted = !recvData.ReadBit();
    HasIgnoreAbandonPenalty = recvData.ReadBit();
    HasDebugFlags = !recvData.ReadBit();
    HasRoundID = !recvData.ReadBit();
    HasMoveType = !recvData.ReadBit();

    if (HasMoveType)
        recvData >> MoveType;

    if (HasBattleInterrupted)
        recvData >> BattleInterrupted;

    if (HasRoundID)
        recvData >> RoundID;

    if (HasDebugFlags)
        recvData >> DebugFlags;

    if (HasAbilityID)
        recvData >> AbilityID;

    if (HasNewFrontPet)
        recvData >> NewFrontPet;

    // make sure player is in a pet battle
    PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(GetPlayer()->GetGUID());
    if (!petBattle)
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried to make a pet battle move while not in battle!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());
        return;
    }

    // make sure the pet battle is in progress
    if (petBattle->GetPhase() != PET_BATTLE_PHASE_IN_PROGRESS)
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried to make a move in a pet battle that isn't in progress!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());
        return;
    }

    if (HasAbilityID)
    {
        // make sure ability is valid
        if (!sBattlePetAbilityStore.LookupEntry(AbilityID))
        {
            TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried use an invalid battle pet ability %u!",
                GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), AbilityID);
            return;
        }
    }

    if (HasNewFrontPet)
    {
        // make sure active pet is valid
        if (NewFrontPet >= PET_BATTLE_MAX_TEAM_PETS)
        {
            TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried to switch to invalid front pet %u!",
                GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), NewFrontPet);
            return;
        }
    }

    PetBattleTeam* team = petBattle->GetTeam(GetPlayer()->GetGUID());
    ASSERT(team);

    // make sure players team hasn't already made a move this round
    if (team->IsReady())
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried use an invalid battle pet ability %u!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), AbilityID);
        return;
    }

    // make sure new active pet is valid
    BattlePet* battlePet = nullptr;
    if (HasNewFrontPet)
    {
        battlePet = team->BattlePets[NewFrontPet];
        if (!battlePet)
        {
            TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried to swap to invalid team battle pet %u!",
                GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), NewFrontPet);
            return;
        }
    }

    // make sure move type is valid
    if (MoveType >= PET_BATTLE_MOVE_TYPE_COUNT)
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried to do invalid move %u!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), MoveType);
        return;
    }

    if (MoveType == PET_BATTLE_MOVE_TYPE_REQUEST_LEAVE)
    {
        sPetBattleSystem->ForfeitBattle(petBattle, team);
        return;
    }

    team->SetPendingMove(MoveType, AbilityID, battlePet);
}

void WorldSession::HandlePetBattleRequestWild(WorldPacket& recvData)
{
    PetBattleRequest petBattleRequest;

    bool HasLocationResult = false;
    bool HasBattleFacing = false;

    float Orientation = 0.0f;

    ObjectGuid wildBattlePetGuid;

    for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; ++i)
    {
        recvData >> petBattleRequest.TeamPositions[i].m_positionX;
        recvData >> petBattleRequest.TeamPositions[i].m_positionZ;
        recvData >> petBattleRequest.TeamPositions[i].m_positionY;
    }

    recvData >> petBattleRequest.BattlePosition.m_positionZ;
    recvData >> petBattleRequest.BattlePosition.m_positionY;
    recvData >> petBattleRequest.BattlePosition.m_positionX;

    recvData.ReadGuidMask(wildBattlePetGuid, 0);

    HasBattleFacing = !recvData.ReadBit();

    recvData.ReadGuidMask(wildBattlePetGuid, 6, 3, 5, 2, 7, 1, 4);

    HasLocationResult = !recvData.ReadBit();

    recvData.ReadGuidBytes(wildBattlePetGuid, 3, 6, 5, 2, 7, 1, 0, 4);

    if (HasBattleFacing)
    {
        recvData >> Orientation;
        petBattleRequest.BattlePosition.SetOrientation(Orientation);
    }

    if (HasLocationResult)
        recvData >> petBattleRequest.LocationResult;

    // check if player is dead
    if (!GetPlayer()->IsAlive())
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a wild pet battle while dead!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_DEAD);
        return;
    }

    // check if player is in combat
    if (GetPlayer()->IsInCombat())
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a wild pet battle while in combat!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_ALREADY_IN_COMBAT);
        return;
    }

    // check if player isn't already in a battle
    if (sPetBattleSystem->GetPlayerPetBattle(GetPlayer()->GetGUID()))
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a new wild pet battle while still in an old pet battle!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_ALREADY_IN_PETBATTLE);
        return;
    }

    Creature* wildBattlePet = ObjectAccessor::GetCreatureOrPetOrVehicle(*GetPlayer(), wildBattlePetGuid);
    if (!wildBattlePet)
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_INVALID_TARGET);
        return;
    }

    // player must be able to interact with the creature
    if (!GetPlayer()->IsWithinDistInMap(wildBattlePet, INTERACTION_DISTANCE))
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_TOO_FAR);
        return;
    }

    if (!GetPlayer()->GetNPCIfCanInteractWith(wildBattlePetGuid, UNIT_NPC_FLAG_PETBATTLE))
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a wild pet battle but can't interact with opponent %s!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), wildBattlePet->GetGUID().ToString().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NOT_VALID_TARGET);
        return;
    }

    // check if creature is a wild battle pet
    if (!sBattlePetSpawnMgr->GetWildBattlePet(wildBattlePet->GetGUID()))
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %u(%s) tried to initiate a wild pet battle but creature %u isn't a wild battle pet!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), wildBattlePet->GetGUID().ToString().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_INVALID_TARGET);
        return;
    }

    if (GetPlayer()->IsFlying())
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NOT_DURING_FLYING);
        return;
    }

    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_FAILED);
        return;
    }

    // player needs to have at least one battle pet loadout slot populated
    if (!battlePetMgr->GetLoadoutSlot(0))
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NEED_AT_LEAST_1_PET_IN_SLOT);
        return;
    }

    // make sure all of the players loadout pets aren't dead
    bool allDead = true;
    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
        if (BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetMgr->GetLoadoutSlot(i)))
            if (battlePet->IsAlive())
            {
                allDead = false;
                break;
            }

    if (allDead)
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_PET_ALL_DEAD);
        return;
    }

    // make sure there is enough room for the pet battle
    for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; ++i)
    {
        if (_player->GetMap()->getStaticObjectHitPos(petBattleRequest.BattlePosition.GetPositionX(), petBattleRequest.BattlePosition.GetPositionY(),
            petBattleRequest.BattlePosition.GetPositionZ(), petBattleRequest.TeamPositions[i].GetPositionX(), petBattleRequest.TeamPositions[i].GetPositionY(),
            petBattleRequest.TeamPositions[i].GetPositionZ(),petBattleRequest.TeamPositions[i].m_positionX, petBattleRequest.TeamPositions[i].m_positionY,
            petBattleRequest.TeamPositions[i].m_positionZ, 0.0f, _player->GetTerrainSwaps()))
        {
            SendPetBattleRequestFailed(PET_BATTLE_REQUEST_GROUND_NOT_ENOUGH_SMOOTH);
            return;
        }
    }

    SendPetBattleFinalizeLocation(petBattleRequest);

    // setup player
    _player->SetFlag(UNIT_FLAGS, UNIT_FLAG_PACIFIED | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC);
    _player->SetFacingTo(_player->GetAngle(petBattleRequest.TeamPositions[PET_BATTLE_OPPONENT_TEAM].GetPositionX(), petBattleRequest.TeamPositions[PET_BATTLE_OPPONENT_TEAM].GetPositionY()));
    _player->SetControlled(true, UNIT_STATE_ROOT);
    _player->SetSelection(wildBattlePet->GetGUID());

    sBattlePetSpawnMgr->EnteredBattle(wildBattlePet);

    // create and start pet battle
    sPetBattleSystem->Create(GetPlayer(), wildBattlePet, PET_BATTLE_TYPE_PVE)->StartBattle();
}

void WorldSession::HandlePetBattleJoinQueue(WorldPacket& /*recvData*/)
{
    Player* player = GetPlayer();
    if (!player)
    {
        SetPetBattleQueueError(PET_BATTLE_QUEUE_ERROR_JOIN_FAILED);
        return;
    }

    if (player->InBattleground() || player->GetInstanceScript())
    {
        SetPetBattleQueueError(PET_BATTLE_QUEUE_ERROR_JOIN_FAILED);
        return;
    }

    if (sPetBattleQueueMgr->IsInPetBattleQueue(player->GetGUID()))
    {
        SetPetBattleQueueError(PET_BATTLE_QUEUE_ERROR_ALREADY_QUEUED);
        return;
    }

    if (player->GetRace() == RACE_PANDAREN_NEUTRAL)
    {
        SetPetBattleQueueError(PET_BATTLE_QUEUE_ERROR_NEUTRAL);
        return;
    }

    if (player->IsFlying())
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NOT_DURING_FLYING);
        return;
    }

    BattlePetMgr * battlePetMgr = player->GetBattlePetMgr();
    if (!battlePetMgr)
    {
        SetPetBattleQueueError(PET_BATTLE_QUEUE_ERROR_JOIN_FAILED);
        return;
    }

    PetBattleSlotErrorArray slotErrors;
    bool HasError = false;
    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; ++i)
    {
        PetBattleQueueSlotError error = sPetBattleQueueMgr->GetPetBattleQueueSlotError(battlePetMgr, i);
        if (error != PET_BATTLE_QUEUE_ERROR_SLOT_NONE)
            HasError = true;

        slotErrors[i] = error;
    }

    if (HasError)
    {
        SetPetBattleQueueSlotsError(slotErrors);
        return;
    }

    uint32 petLevel = 0;
    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
        if (ObjectGuid guid = battlePetMgr->GetLoadoutSlot(i))
            if (BattlePet* battlePet = battlePetMgr->GetBattlePet(guid))
                petLevel += battlePet->GetLevel();

    uint32 averageTime = sPetBattleQueueMgr->AddPlayerToPetBattleQueue(player->GetGUID(), petLevel);

    SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_QUEUED, 0, averageTime);
}

void WorldSession::HandlePetBattleRemoveFromQueue(WorldPacket& /*recvData*/)
{
    Player* player = GetPlayer();

    sPetBattleQueueMgr->RemovePlayerFromPetBattleQueue(player->GetGUID());

    SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_REMOVED);
}

void WorldSession::HandlePetBattleProposeMatchResult(WorldPacket& recvData)
{
    bool Accept = recvData.ReadBit();

    Player* player = GetPlayer();
    if (!player)
        return;

    PetBattleQueueInfo* challengerQueueInfo = sPetBattleQueueMgr->GetPetBattleQueueInfo(player->GetGUID());
    if (!challengerQueueInfo)
        return;

    Player* opponent = ObjectAccessor::FindPlayer(challengerQueueInfo->OpponentGUID);
    if (!opponent)
    {
        challengerQueueInfo->OpponentGUID.Clear();
        challengerQueueInfo->JoinTime = getMSTime();

        uint32 averageWaitTime = sPetBattleQueueMgr->GetAverageWaitTimer();

        SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_REQUEUED_REMOVED, 0, averageWaitTime);

        return;
    }

    PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(player->GetGUID());

    if (!Accept)
    {
        sPetBattleQueueMgr->RemovePlayerFromPetBattleQueue(player->GetGUID());

        if (petBattle)
        {
            if (PetBattleTeam* team = petBattle->GetTeam(player->GetGUID()))
                sPetBattleSystem->ForfeitBattle(petBattle, team);
        }
        else if (PetBattleQueueInfo* opponentQueueInfo = sPetBattleQueueMgr->GetPetBattleQueueInfo(challengerQueueInfo->OpponentGUID))
        {
            opponentQueueInfo->OpponentGUID.Clear();
            opponentQueueInfo->JoinTime = getMSTime();

            uint32 averageWaitTime = sPetBattleQueueMgr->GetAverageWaitTimer();

            opponent->GetSession()->SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_OPPONENT_DECLINED, 0, averageWaitTime);
        }

        SetPetBattleQueueStatus(PET_BATTLE_QUEUE_ERROR_PROPOSAL_DECLINED);
    }
    else
    {
        PetBattleRequest petBattleRequest = PetBattleRequest(challengerQueueInfo->BattlePosition, challengerQueueInfo->PlayerPosition);

        SendPetBattleFinalizeLocation(petBattleRequest);

        // setup player
        player->SetFlag(UNIT_FLAGS, UNIT_FLAG_PACIFIED | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC);
        player->SetVisible(false);

        if (!petBattle)
            sPetBattleSystem->Create(player, opponent, PET_BATTLE_TYPE_PVP_MATCHMAKING);

        player->TeleportTo(challengerQueueInfo->PlayerPosition);
    }
}

void WorldSession::HandlePetBattleReplaceFrontPet(WorldPacket& recvData)
{
    uint8 FrontPetSlot = 0;
    recvData >> FrontPetSlot;

    Player* player = GetPlayer();
    if (!player)
        return;

    PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(player->GetGUID());
    if (!petBattle)
        return;

    PetBattleTeam* team = petBattle->GetTeam(player->GetGUID());
    if (!team)
        return;

    // make sure players team hasn't already made a move this round
    if (team->IsReady())
        return;

    int8 PetIndex = -1;
    if (BattlePet* oldPet = team->GetActivePet())
        PetIndex = team->ConvertToLocalIndex(oldPet->GetGlobalIndex());

    if (PetIndex >= 0 && PetIndex == FrontPetSlot)
        return;

    // make sure new active pet is valid
    BattlePet* battlePet = battlePet = team->BattlePets[FrontPetSlot];
    if (!battlePet)
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_INPUT - Player %s (%s) tried to swap to invalid team battle pet %u!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str(), FrontPetSlot);
        return;
    }

    if (team->CanSwap(battlePet, true))
        petBattle->SwapActivePet(team, battlePet);

    team->IsReady(true);

    // if battle is in progress, set opponent team ready for the next round
    if (petBattle->GetRound())
        if (PetBattleTeam* opponentTeam = petBattle->Teams[!team->GetTeamIndex()])
            opponentTeam->SetPendingMove(PET_BATTLE_MOVE_TYPE_SWAP_OR_PASS, 0, opponentTeam->GetActivePet());

}

void WorldSession::HandlePetBattleRequestDuel(WorldPacket& recvData)
{
    PetBattleRequest petBattleRequest;

    bool HasLocationResult = false;
    bool HasBattleFacing = false;

    float Orientation = 0.0f;

    ObjectGuid opponentGuid;

    recvData >> petBattleRequest.BattlePosition.m_positionZ;

    for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; ++i)
    {
        recvData >> petBattleRequest.TeamPositions[i].m_positionZ;
        recvData >> petBattleRequest.TeamPositions[i].m_positionX;
        recvData >> petBattleRequest.TeamPositions[i].m_positionY;
    }

    recvData >> petBattleRequest.BattlePosition.m_positionY;
    recvData >> petBattleRequest.BattlePosition.m_positionX;

    recvData.ReadGuidMask(opponentGuid, 1);

    HasBattleFacing = !recvData.ReadBit();

    recvData.ReadGuidMask(opponentGuid, 2, 3);

    HasLocationResult = !recvData.ReadBit();

    recvData.ReadGuidMask(opponentGuid, 4, 0, 5, 7, 6);

    recvData.ReadGuidBytes(opponentGuid, 3, 0, 4, 2, 6, 5, 7, 1);

    if (HasBattleFacing)
    {
        recvData >> Orientation;
        petBattleRequest.BattlePosition.SetOrientation(Orientation);
    }

    if (HasLocationResult)
        recvData >> petBattleRequest.LocationResult;

    Player* opponent = ObjectAccessor::FindConnectedPlayer(opponentGuid);
    if (!opponent)
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_INVALID_TARGET);
        return;
    }

    // check if player is dead
    if (!opponent->IsAlive())
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a pvp pet battle while dead!",
            opponent->GetGUID().ToString().c_str(), opponent->GetName().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_DEAD);
        return;
    }

    // player must be able to interact with the opponent
    if (!GetPlayer()->IsWithinDistInMap(opponent, INTERACTION_DISTANCE))
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_TOO_FAR);
        return;
    }

    // check if player is in combat
    if (GetPlayer()->IsInCombat() || opponent->IsInCombat())
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a pvp pet battle while in combat!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_ALREADY_IN_COMBAT);
        return;
    }

    // check if player isn't already in a battle
    if (sPetBattleSystem->GetPlayerPetBattle(GetPlayer()->GetGUID()) || sPetBattleSystem->GetPlayerPetBattle(opponentGuid))
    {
        TC_LOG_DEBUG("network", "CMSG_PET_BATTLE_REQUEST_WILD - Player %s (%s) tried to initiate a new pvp pet battle while still in an old pet battle!",
            GetPlayer()->GetGUID().ToString().c_str(), GetPlayer()->GetName().c_str());

        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_ALREADY_IN_PETBATTLE);
        return;
    }

    AreaTableEntry const* areaEntry = sAreaStore.LookupEntry(GetPlayer()->GetAreaId());
    if (areaEntry && !(areaEntry->GetFlags() & AREA_FLAG_ALLOW_DUELS))
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NOT_HERE);
        return;
    }

    if (GetPlayer()->IsFlying() || opponent->IsFlying())
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NOT_DURING_FLYING);
        return;
    }

    BattlePetMgr* battlePetMgr = GetPlayer()->GetBattlePetMgr();
    if (!battlePetMgr)
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_FAILED);
        return;
    }

    // player needs to have at least one battle pet loadout slot populated
    if (!battlePetMgr->GetLoadoutSlot(0))
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_NEED_AT_LEAST_1_PET_IN_SLOT);
        return;
    }

    // make sure all of the players loadout pets aren't dead
    bool allDead = true;
    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
        if (BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetMgr->GetLoadoutSlot(i)))
            if (battlePet->IsAlive())
            {
                allDead = false;
                break;
            }

    if (allDead)
    {
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_PET_ALL_DEAD);
        return;
    }

    sPetBattleSystem->AddPetBattleDuelRequest(opponentGuid, petBattleRequest);

    WorldPacket data(SMSG_PET_BATTLE_DUEL_CHALLENGE, 1 + 8 + 1 + PET_BATTLE_MAX_TEAMS * (4 + 4 + 4) + (HasBattleFacing ? 4 : 0) + 4 + (HasLocationResult ? 4 : 0) + 4 + 4);

    ObjectGuid challengerGUID = GetPlayer()->GetGUID();

    data.WriteGuidMask(challengerGUID, 4);

    data.WriteBit(HasBattleFacing);

    data.WriteGuidMask(challengerGUID, 6, 0, 7, 1);

    data.WriteBit(HasLocationResult);

    data.WriteGuidMask(challengerGUID, 5, 2, 3);

    data.FlushBits();

    for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; ++i)
    {
        data << float(petBattleRequest.TeamPositions[i].GetPositionX());
        data << float(petBattleRequest.TeamPositions[i].GetPositionY());
        data << float(petBattleRequest.TeamPositions[i].GetPositionZ());
    }

    data.WriteGuidBytes(challengerGUID, 4, 0);

    if (!HasBattleFacing)
        data << float(petBattleRequest.BattlePosition.GetOrientation());

    data << float(petBattleRequest.BattlePosition.GetPositionZ());

    if (!HasLocationResult)
        data << uint32(petBattleRequest.LocationResult);

    data << float(petBattleRequest.BattlePosition.GetPositionY());

    data.WriteGuidBytes(challengerGUID, 1);

    data << float(petBattleRequest.BattlePosition.GetPositionX());

    data.WriteGuidBytes(challengerGUID, 6, 7, 3, 2, 5);

    opponent->SendDirectMessage(&data);
}

void WorldSession::HandlePetBattleRequestUpdate(WorldPacket& recvData)
{
    ObjectGuid TargetGUID;
    bool Canceled = false;

    recvData.ReadGuidMask(TargetGUID, 4, 6, 7, 3);

    Canceled = recvData.ReadBit();

    recvData.ReadGuidMask(TargetGUID, 0, 2, 1, 5);

    recvData.ReadGuidBytes(TargetGUID, 7, 1, 0, 5, 6, 2, 4, 3);

    Player* opponent = ObjectAccessor::FindConnectedPlayer(TargetGUID);
    if (!opponent)
    {
        sPetBattleSystem->RemovePetBattleDuelRequest(GetPlayer()->GetGUID());
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_INVALID_TARGET);
        return;
    }

    PetBattleRequest* request = sPetBattleSystem->GetPetBattleDuelRequest(GetPlayer()->GetGUID());

    if (Canceled || !request)
    {
        sPetBattleSystem->RemovePetBattleDuelRequest(GetPlayer()->GetGUID());
        SendPetBattleRequestFailed(PET_BATTLE_REQUEST_FAILED);
        opponent->GetSession()->SendPetBattleRequestFailed(PET_BATTLE_REQUEST_DECLINED);
        return;
    }

    SendPetBattleFinalizeLocation(*request);
    opponent->GetSession()->SendPetBattleFinalizeLocation(*request);

    _player->SetFlag(UNIT_FLAGS, UNIT_FLAG_PACIFIED | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC);
    opponent->SetFlag(UNIT_FLAGS, UNIT_FLAG_PACIFIED | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC);

    float playerOrient = request->TeamPositions[PET_BATTLE_OPPONENT_TEAM].GetAngle(&request->TeamPositions[PET_BATTLE_CHALLANGER_TEAM]);
    float opponentOrient = request->TeamPositions[PET_BATTLE_CHALLANGER_TEAM].GetAngle(&request->TeamPositions[PET_BATTLE_OPPONENT_TEAM]);

    _player->GetMotionMaster()->MovePoint(EVENT_DUEL_PET_BATTLE, request->TeamPositions[PET_BATTLE_OPPONENT_TEAM], playerOrient);
    opponent->GetMotionMaster()->MovePoint(EVENT_DUEL_PET_BATTLE, request->TeamPositions[PET_BATTLE_CHALLANGER_TEAM], opponentOrient);

    sPetBattleSystem->RemovePetBattleDuelRequest(GetPlayer()->GetGUID());

    // create pet battle
    sPetBattleSystem->Create(GetPlayer(), opponent, PET_BATTLE_TYPE_PVP_DUEL);
}

// -------------------------------------------------------------------------------

void WorldSession::SendPetBattleFinalizeLocation(PetBattleRequest& request)
{
    bool HasLocationResult = request.LocationResult == 21;
    bool HasBattleFacing = true;

    WorldPacket data(SMSG_PET_BATTLE_FINALIZE_LOCATION, 1 + 4 + 4 + PET_BATTLE_MAX_TEAMS * (4 + 4 + 4) + 4 + (HasLocationResult ? 4 : 0) + (HasBattleFacing ? 4 : 0));

    data << float(request.BattlePosition.GetPositionX());
    data << float(request.BattlePosition.GetPositionY());

    for (uint8 i = 0; i < PET_BATTLE_MAX_TEAMS; ++i)
    {
        data << float(request.TeamPositions[i].GetPositionY());
        data << float(request.TeamPositions[i].GetPositionZ());
        data << float(request.TeamPositions[i].GetPositionX());
    }

    data << float(request.BattlePosition.GetPositionZ());

    data.WriteBit(!HasLocationResult);
    data.WriteBit(!HasBattleFacing);

    data.FlushBits();

    if (HasBattleFacing)
        data << float(request.BattlePosition.GetOrientation());

    if (HasLocationResult)
        data << uint32(request.LocationResult);

    SendPacket(&data);
}

void WorldSession::SendPetBattleFirstRound(PetBattle* petBattle)
{
    bool HasNextPetBattleState = true; // always true

    uint32 CooldownCount = 0;
    uint32 PetDiedCount = 0;

    ByteBuffer effectData;

    uint32 effectSize = 0;
    uint32 effecttargetSize = 0;
    for (PetBattleEffect& effect : petBattle->Effects)
    {
        ++effectSize;
        effecttargetSize += effect.Targets.size();
    }

    WorldPacket data(SMSG_PET_BATTLE_FIRST_ROUND, petBattle->Teams.size() * (2 + 1 + 1) + 4 + 6 + (HasNextPetBattleState ? 1 : 0) + CooldownCount * (1 + 1 + 2 + 4 + 2 + 1) +
                    effectSize * (4 + 1 + 2 + 2 + 4 + 2 + 1 + 1) + effecttargetSize * (1 + 1 + 4 + 4 + 4 + 4) + PetDiedCount * (1));

    for (PetBattleTeam* team : petBattle->Teams)
    {
        data << uint16(team->GetNextRoundTimer());
        data << uint8(team->GetTrapStatus());
        data << uint8(team->GetInputStatusFlags());
    }

    data << uint32(petBattle->GetRound());

    data.WriteBit(!HasNextPetBattleState);

    data.WriteBits(PetDiedCount, 3);
    data.WriteBits(CooldownCount, 20);

    for (uint32 i = 0; i < CooldownCount; ++i)
        data.WriteBit(false);   // HasGlobalIndex

    data.WriteBits(petBattle->Effects.size(), 22);

    for (PetBattleEffect& effect : petBattle->Effects)
    {
        ByteBuffer effectTargetData;

        bool HasPetEffect = true;

        bool HasTurnInstance = effect.GetTurnInstance() != 0;
        bool HasAbilityEffectID = effect.GetAbilityEffect() != 0;
        bool HasSourceAuraInstanceID = effect.GetAuraInstance() != 0;
        bool HasStackDepth = effect.GetStackDepth() != 0;
        bool HasFlags = effect.GetFlags() != 0;
        bool HasSource = effect.GetSource() != PET_BATTLE_NULL_PET_INDEX;

        data.WriteBit(!HasSourceAuraInstanceID);
        data.WriteBit(!HasStackDepth);
        data.WriteBit(!HasFlags);
        data.WriteBit(!HasAbilityEffectID);
        data.WriteBit(!HasSource);

        data.WriteBits(effect.Targets.size(), 25);

        for (PetBattleEffectTarget& target : effect.Targets)
        {
            bool HasPetTarget = target.Target != PET_BATTLE_NULL_PET_INDEX;

            data.WriteBit(!HasPetTarget);

            data.WriteBits(target.Type, 3);

            if (target.Type == PET_BATTLE_EFFECT_TARGET_UPDATE_AURA)
            {
                bool HasCurrentRound = target.Aura.CurrentRound != 0;
                bool HasAuraInstanceID = target.Aura.AuraInstanceId != 0;
                bool HasRoundsRemaining = target.Aura.RoundsRemaining != 0;
                bool HasAuraAbilityID = target.Aura.AbilityId != 0;

                data.WriteBit(!HasCurrentRound);
                data.WriteBit(!HasAuraInstanceID);
                data.WriteBit(!HasRoundsRemaining);
                data.WriteBit(!HasAuraAbilityID);

                if (HasRoundsRemaining)
                    effectTargetData << int32(target.Aura.RoundsRemaining);

                if (HasCurrentRound)
                    effectTargetData << uint32(target.Aura.CurrentRound);

                if (HasAuraInstanceID)
                    effectTargetData << uint32(target.Aura.AuraInstanceId);

                if (HasAuraAbilityID)
                    effectTargetData << uint32(target.Aura.AbilityId);

                if (HasPetTarget)
                    effectTargetData << int8(target.Target);
            }
            else
            {
                if (HasPetTarget)
                    effectTargetData << int8(target.Target);

                switch (target.Type)
                {
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_STAT:
                    {
                        bool HasStatValue = target.StatValue != 0;

                        data.WriteBit(!HasStatValue);

                        if (HasStatValue)
                            effectTargetData << uint32(target.StatValue);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_STATE:
                    {
                        bool HasStateID = target.State.StateId != 0;
                        bool HasStateValue = target.State.Value != 0;

                        data.WriteBit(!HasStateID);
                        data.WriteBit(!HasStateValue);

                        if (HasStateValue)
                            effectTargetData << uint32(target.State.Value);

                        if (HasStateID)
                            effectTargetData << uint32(target.State.StateId);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_HEALTH:
                    {
                        bool HasHealth = target.Health != 0;

                        data.WriteBit(!HasHealth);

                        if (HasHealth)
                            effectTargetData << uint32(target.Health);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_ABILITY:
                    {
                        bool HasChangedAbilityID = target.Ability.ChangedAbilityId != 0;
                        bool HasCooldownRemaining = target.Ability.CooldownRemaining != 0;
                        bool HasLockdownRemaining = target.Ability.LockdownRemaining != 0;

                        data.WriteBit(!HasLockdownRemaining); // 44
                        data.WriteBit(!HasCooldownRemaining); // 48
                        data.WriteBit(!HasChangedAbilityID); // 52

                        if (HasChangedAbilityID)
                            effectTargetData << uint32(target.Ability.ChangedAbilityId);

                        if (HasLockdownRemaining)
                            effectTargetData << uint32(target.Ability.LockdownRemaining);

                        if (HasCooldownRemaining)
                            effectTargetData << uint32(target.Ability.CooldownRemaining);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_TRIGGER_ABILITY:
                    {
                        bool HasTriggerAbilityID = target.TriggerAbilityId != 0;

                        data.WriteBit(!HasTriggerAbilityID);

                        if (HasTriggerAbilityID)
                            effectTargetData << uint32(target.TriggerAbilityId);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_NPC_EMOTE:
                    {
                        bool HasBroadcastTextID = target.BroadcastTextId;

                        data.WriteBit(!HasBroadcastTextID);

                        if (HasBroadcastTextID)
                            effectTargetData << uint32(target.BroadcastTextId);

                        break;
                    }
                    default:
                        break;
                }
            }
        }

        data.WriteBit(!HasTurnInstance);
        data.WriteBit(!HasPetEffect);

        if (HasSource)
            effectData << int8(effect.GetSource());

        if (HasSourceAuraInstanceID)
            effectData << uint16(effect.GetAuraInstance());

        if (HasFlags)
            effectData << uint16(effect.GetFlags());

        if (HasAbilityEffectID)
            effectData << uint32(effect.GetAbilityEffect());

        effectData.append(effectTargetData);

        if (HasTurnInstance)
            effectData << uint16(effect.GetTurnInstance());

        if (HasPetEffect)
            effectData << uint8(effect.GetType());

        if (HasStackDepth)
            effectData << uint8(effect.GetStackDepth());
    }

    data.append(effectData);

    for (uint32 i = 0; i < CooldownCount; ++i)
    {
        data << uint8(0);   // AbilityIndex
        data << uint16(0);  // LockdownRemaining
        data << uint32(0);  // AbilityID
        data << uint16(0);  // CooldownRemaining
        data << uint8(0);  // Pboid
    }

    for (uint32 i = 0; i < PetDiedCount; ++i)
        data << uint8(0);   // PetDied

    if (HasNextPetBattleState)
        data << uint8(PET_BATTLE_ROUND_RESULT_NORMAL);

    SendPacket(&data);
}

void WorldSession::SendPetBattleInitialUpdate(PetBattle* petBattle, bool Inverse)
{
    ObjectGuid wildBattlePetGuid;
    if (petBattle->GetType() == PET_BATTLE_TYPE_PVE)
        if (Creature* wildBattlePet = petBattle->Teams[PET_BATTLE_OPPONENT_TEAM]->GetWildBattlePet())
            wildBattlePetGuid = wildBattlePet->GetGUID();

    bool IsPvP = petBattle->GetType() != PET_BATTLE_TYPE_PVE;
    bool CanAwardXP = petBattle->GetType() != PET_BATTLE_TYPE_PVP_DUEL;

    uint8 CurPetBattleState = PET_BATTLE_ROUND_RESULT_INITIALIZE;
    bool HasCurPetBattleState = CurPetBattleState != PET_BATTLE_ROUND_RESULT_NONE;

    uint8 ForfeitPenalty = 10;
    bool HasForfeitPenalty = ForfeitPenalty != 0;

    uint16 PvPMaxRoundTime = 30;
    bool HasPvPMaxRoundTime = PvPMaxRoundTime != 0;

    uint16 WatingForFrontPetsMaxSecs = 30;
    bool HasWatingForFrontPetsMaxSecs = WatingForFrontPetsMaxSecs != 0;

    uint32 CreatureID = 0;
    uint32 DisplayID = 0;
    bool HasCreatureID = CreatureID != 0;
    bool HasDisplayID = DisplayID != 0;

    ByteBuffer playerUpdateData;
    ByteBuffer enviromentUpdateData;

    uint32 battlePetCount = 0;
    uint32 battlePetStateCount = 0;
    uint32 battlePetAuraCount = 0;
    uint32 battlePetAbilityCount = 0;
    uint32 battlePetNicknameSize = 0;

    for (PetBattleTeam* team : petBattle->Teams)
    {
        for (BattlePet* battlePet : team->BattlePets)
        {
            ++battlePetCount;

            for (uint32 i = 0; i < BATTLE_PET_MAX_STATES; ++i)
                if (battlePet->States[i])
                    ++battlePetStateCount;

            battlePetAuraCount += battlePet->Auras.size();

            for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; ++i)
                if (battlePet->Abilities[i])
                    ++battlePetAbilityCount;

            battlePetNicknameSize += battlePet->GetNickname().size();
        }
    }

    WorldPacket data(SMSG_PET_BATTLE_INITIAL_UPDATE, 1 + 8 + 2 + (HasForfeitPenalty ? 1 : 0) + (HasCurPetBattleState ? 1 : 0) + (HasCreatureID ? 4 : 0) +
                    (HasPvPMaxRoundTime ? 2 : 0) + 4 + (HasWatingForFrontPetsMaxSecs ? 2 : 0) + (HasDisplayID ? 4 : 0) + petBattle->Teams.size() *
                    (1 + 8 + 1 + 4 + 4 + 1 + 1) + battlePetCount * (1 + 8 + 9 + 1 + 4 + 2 + 2 + 4 + 4 + 4 + 4 + 4 + 4 + 2 + 2 + 4) + battlePetStateCount *
                    (4 + 4) + battlePetAuraCount * (1 + 4 + 4 + 4 + 4 + 1) + battlePetAbilityCount * (1 + 4 + 1 + 1 + 2 + 2) + (3 * 6) + battlePetNicknameSize);

    // enviroment update (bits)
    for (uint8 i = 0; i < 3; ++i)
    {
        ByteBuffer auraUpdateData;

        uint32 AuraCount = 0;       // AuraCount
        uint32 StateCount = 0;      // StateCount

        data.WriteBits(StateCount, 21);
        data.WriteBits(AuraCount, 21);

        for (uint32 j = 0; j < AuraCount; ++j)
        {
            data.WriteBit(!false);  // CurrentRound
            data.WriteBit(!false);   // CasterPBOID
            data.WriteBit(!false);  // RoundsRemaining

            //if (RoundsRemaining)
                //auraUpdateData << uint32(0);        // RoundsRemaining

             //if (CurrentRound)
                //auraUpdateData << uint32(0);        // CurrentRound

            auraUpdateData << uint32(0);        // InstanceID

            //if (CasterPBOID)
                //auraUpdateData << uint8(0);        // CasterPBOID

            auraUpdateData << uint32(0);        // AbilityID
        }

        if (!auraUpdateData.empty())
            enviromentUpdateData.append(auraUpdateData);

        for (uint32 j = 0; j < StateCount; ++j)
        {
            enviromentUpdateData << uint16(0);      // StateID
            enviromentUpdateData << uint16(0);      // StateValue
        }
    }

    for (uint8 i = 0; i < petBattle->Teams.size(); ++i)
    {
        uint8 Index = Inverse ? !i : i;

        PetBattleTeam* team = petBattle->Teams[Index];
        if (!team)
            continue;

        ByteBuffer teamUpdateData;

        bool HasRoundTimeSec = false;
        bool HasFrontPet = true;
        bool HasTrapAbilityStatus = true;

        ObjectGuid characterGuid;
        uint32 TrapAbilityID = 0;

        if (Player* player = team->GetPlayerOwner())
        {
            characterGuid = player->GetGUID();
            TrapAbilityID = player->GetBattlePetMgr()->GetTrapAbility();
        }

        data.WriteBit(!HasTrapAbilityStatus);

        data.WriteBits(team->BattlePets.size(), 2);

        data.WriteGuidMask(characterGuid, 2);

        uint8 battlePetTeamIndex = 0;

        for (BattlePet* battlePet : team->BattlePets)
        {
            ByteBuffer abilityUpdateData;
            ByteBuffer auraUpdateData;

            uint32 BattlePetFlags = battlePet->GetFlags();
            bool HasBattlePetFlags = BattlePetFlags != 0;

            bool HasSlot = true;

            ObjectGuid battlePetGUID = battlePet->GetGUID();

            uint32 StateCount = 0;
            for (uint32 i = 0; i < BATTLE_PET_MAX_STATES; ++i)
                if (battlePet->States[i])
                    ++StateCount;

            data.WriteBits(battlePet->Auras.size(), 21);

            data.WriteGuidMask(battlePetGUID, 3);

            data.WriteBits(StateCount, 21);

            for (PetBattleAura* aura : battlePet->Auras)
            {
                bool HasCaster = true;
                bool HasCurrentRound = aura->GetTurn() != 0;
                bool HasRoundsRemaining = aura->GetDuration();

                data.WriteBit(!HasCaster);
                data.WriteBit(!HasCurrentRound);
                data.WriteBit(!HasRoundsRemaining);

                auraUpdateData << uint32(aura->GetAbility());

                if (HasCurrentRound)
                    auraUpdateData << int32(aura->GetTurn());

                if (HasRoundsRemaining)
                    auraUpdateData << int32(aura->GetDuration());

                auraUpdateData << uint32(aura->GetId());

                if (HasCaster)
                    auraUpdateData << uint8(aura->GetCasterIndex());
            }

            data.WriteGuidMask(battlePetGUID, 0);

            data.WriteBit(!HasBattlePetFlags);

            data.WriteGuidMask(battlePetGUID, 5, 1);

            uint8 abilityCount = 0;
            for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; ++i)
                if (battlePet->Abilities[i])
                    ++abilityCount;

            data.WriteBits(abilityCount, 20);

            data.WriteBit(!HasSlot);

            data.WriteBits(battlePet->GetNickname().size(), 7);

            data.WriteGuidMask(battlePetGUID, 2, 4);

            for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; ++i)
                if (battlePet->Abilities[i])
                {
                    uint8 GlobalIndex = battlePet->GetGlobalIndex();
                    bool HasGlobalIndex = true;

                    data.WriteBit(!HasGlobalIndex);

                    abilityUpdateData << uint32(battlePet->Abilities[i]->AbilityId);
                    abilityUpdateData << uint8(i);

                    if (HasGlobalIndex)
                        abilityUpdateData << uint8(GlobalIndex);

                    abilityUpdateData << uint16(battlePet->Abilities[i]->Lockdown);
                    abilityUpdateData << uint16(battlePet->Abilities[i]->Cooldown);
                }

            data.WriteGuidMask(battlePetGUID, 6, 7);

            if (HasSlot)
                teamUpdateData << uint8(battlePetTeamIndex++);

            if (!abilityUpdateData.empty())
                teamUpdateData.append(abilityUpdateData);

            teamUpdateData << uint32(battlePet->GetDisplayId());

            teamUpdateData.WriteGuidBytes(battlePetGUID, 4);

            teamUpdateData << uint16(battlePet->GetLevel());

            teamUpdateData.WriteGuidBytes(battlePetGUID, 7);

            teamUpdateData << uint16(battlePet->GetQuality());

            teamUpdateData.WriteGuidBytes(battlePetGUID, 6);

            teamUpdateData << uint32(battlePet->GetPower());

            for (uint32 i = 0; i < BATTLE_PET_MAX_STATES; ++i)
                if (battlePet->States[i])
                {
                    teamUpdateData << uint32(i);
                    teamUpdateData << uint32(battlePet->States[i]);
                }

            teamUpdateData.WriteGuidBytes(battlePetGUID, 0);

            teamUpdateData << uint32(battlePet->GetMaxHealth());

            if (!auraUpdateData.empty())
                teamUpdateData.append(auraUpdateData);

            teamUpdateData.WriteGuidBytes(battlePetGUID, 5, 2);

            teamUpdateData << uint32(battlePet->GetSpeed());
            teamUpdateData << uint32(battlePet->GetCurrentHealth());

            teamUpdateData.WriteGuidBytes(battlePetGUID, 3);

            teamUpdateData << uint32(0);        // CollarID

            teamUpdateData.WriteGuidBytes(battlePetGUID, 1);

            teamUpdateData << uint32(0);        // NpcTeamMemberID
            teamUpdateData << uint16(battlePet->GetXp());

            if (HasBattlePetFlags)
                teamUpdateData << uint16(BattlePetFlags);

            teamUpdateData.WriteString(battlePet->GetNickname());

            teamUpdateData << uint32(battlePet->GetSpecies());
        }

        data.WriteBit(!HasFrontPet);
        data.WriteBit(!HasRoundTimeSec);

        data.WriteGuidMask(characterGuid, 5, 3, 4, 6, 7, 0, 1);

        if (!teamUpdateData.empty())
            playerUpdateData.append(teamUpdateData);

        if (HasTrapAbilityStatus)
            playerUpdateData << uint32(team->GetTrapStatus());

        playerUpdateData << uint32(TrapAbilityID);

        playerUpdateData.WriteGuidBytes(characterGuid, 5, 7, 6, 1, 4, 0);

        if (HasFrontPet)
        {
            uint8 Index = 0;
            if (team->GetActivePet())
                Index = team->ConvertToLocalIndex(team->GetActivePet()->GetGlobalIndex());

            playerUpdateData << uint8(Index);
        }

        playerUpdateData << uint8(team->GetInputStatusFlags());

        //if (HasRoundTimeSec)
            //playerUpdateData << uint16(0);      // RoundTimeSecs

        playerUpdateData.WriteGuidBytes(characterGuid, 3, 2);
    }

    data.WriteBit(!HasForfeitPenalty);
    data.WriteBit(IsPvP);
    data.WriteBit(CanAwardXP);
    data.WriteBit(!HasCreatureID);
    data.WriteBit(!HasDisplayID);
    data.WriteBit(!HasWatingForFrontPetsMaxSecs);
    data.WriteBit(!wildBattlePetGuid.IsEmpty());

    data.WriteGuidMask(wildBattlePetGuid, 2, 4, 5, 1, 3, 6, 7, 0);

    data.WriteBit(!HasPvPMaxRoundTime);
    data.WriteBit(!HasCurPetBattleState);

    data.append(playerUpdateData);

    if (HasForfeitPenalty)
        data << uint8(ForfeitPenalty);

    if (!enviromentUpdateData.empty())
        data.append(enviromentUpdateData);

    if (HasCurPetBattleState)
        data << uint8(CurPetBattleState);

    data.WriteGuidBytes(wildBattlePetGuid, 5, 4, 3, 2, 7, 0, 1, 6);

    if (HasCreatureID)
        data << uint32(CreatureID);

    if (HasPvPMaxRoundTime)
        data << uint16(PvPMaxRoundTime);

    data << uint32(petBattle->GetRound());

    if (HasWatingForFrontPetsMaxSecs)
        data << uint16(WatingForFrontPetsMaxSecs);

    if (HasDisplayID)
        data << uint32(DisplayID);

    SendPacket(&data);
}

void WorldSession::SendPetBattleRequestFailed(uint8 reason)
{
    WorldPacket data(SMSG_PET_BATTLE_REQUEST_FAILED, 1 + 1);

    bool HasError = true;   // always true

    data.WriteBit(!HasError);

    data.FlushBits();

    if (HasError)
        data << uint8(reason);

    SendPacket(&data);
}

void WorldSession::SendPetBattleRoundResult(PetBattle* petBattle)
{
    // get players team
    PetBattleTeam* team = petBattle->Teams[PET_BATTLE_CHALLANGER_TEAM];
    if (petBattle->GetType() != PET_BATTLE_TYPE_PVE && petBattle->Teams[PET_BATTLE_OPPONENT_TEAM]->GetPlayerOwner() == GetPlayer())
        team = petBattle->Teams[PET_BATTLE_OPPONENT_TEAM];

    uint8 RoundResult = petBattle->GetRoundResult();
    bool HasRoundResult = RoundResult != PET_BATTLE_ROUND_RESULT_NONE;

    ByteBuffer effectData;
    ByteBuffer cooldownData;

    std::vector<uint8> PetDiedList;
    for (PetBattleTeam* team : petBattle->Teams)
        for (BattlePet* pet : team->BattlePets)
            if (!pet->IsAlive())
                PetDiedList.push_back(pet->GetGlobalIndex());

    uint32 abilitiesCount = 0;
    for (BattlePet* battlePet : team->BattlePets)
        for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
        {
            if (!battlePet->Abilities[i])
                continue;

            if (!battlePet->Abilities[i]->OnCooldown)
                continue;

            ++abilitiesCount;
        }

    uint32 effectSize = 0;
    uint32 effectTargetSize = 0;
    for (PetBattleEffect effect : petBattle->Effects)
    {
        ++effectSize;
        effectTargetSize += effect.Targets.size();
    }

    WorldPacket data(SMSG_PET_BATTLE_ROUND_RESULT, petBattle->Teams.size() * (1 + 1 + 2) + 4 + 6 + (HasRoundResult ? 1 : 0) + PetDiedList.size() * (1) +
                    effectSize * (4 + 2 + 2 + 4 + 1 + 2 + 1 + 1) + effectTargetSize * (1 + 1 + 4 + 4 + 4 + 4) + abilitiesCount * (1 + 2 + 2 + 4 + 1 + 1));

    for (PetBattleTeam* team : petBattle->Teams)
    {
        data << uint8(team->GetTrapStatus());
        data << uint8(team->GetInputStatusFlags());
        data << uint16(team->GetNextRoundTimer());
    }

    data << uint32(petBattle->GetRound());

    data.WriteBits(petBattle->Effects.size(), 22);

    for (PetBattleEffect effect : petBattle->Effects)
    {
        ByteBuffer effectTargetData;

        bool HasPetEffect = true;

        bool HasTurnInstance = effect.GetTurnInstance() != 0;
        bool HasAbilityEffectID = effect.GetAbilityEffect() != 0;
        bool HasSourceAuraInstanceID = effect.GetAuraInstance() != 0;
        bool HasStackDepth = effect.GetStackDepth() != 0;
        bool HasFlags = effect.GetFlags() != 0;
        bool HasSource = effect.GetSource() != PET_BATTLE_NULL_PET_INDEX;

        data.WriteBit(!HasAbilityEffectID);
        data.WriteBit(!HasPetEffect);

        data.WriteBits(effect.Targets.size(), 25);

        data.WriteBit(!HasSourceAuraInstanceID);

        for (PetBattleEffectTarget& target : effect.Targets)
        {
            bool HasPetTarget = target.Target != PET_BATTLE_NULL_PET_INDEX;

            data.WriteBits(target.Type, 3);

            if (HasPetTarget)
                effectTargetData << int8(target.Target);

            if (target.Type == PET_BATTLE_EFFECT_TARGET_UPDATE_HEALTH)
            {
                data.WriteBit(!HasPetTarget);

                bool HasHealth = target.Health != 0;

                data.WriteBit(!HasHealth);

                if (HasHealth)
                    effectTargetData << uint32(target.Health);
            }
            else
            {
                switch (target.Type)
                {
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_AURA:
                    {
                        bool HasCurrentRound = target.Aura.CurrentRound != 0;
                        bool HasAuraInstanceID = target.Aura.AuraInstanceId != 0;
                        bool HasRoundsRemaining = target.Aura.RoundsRemaining != 0;
                        bool HasAuraAbilityID = target.Aura.AbilityId != 0;

                        data.WriteBit(!HasAuraInstanceID);
                        data.WriteBit(!HasRoundsRemaining);
                        data.WriteBit(!HasAuraAbilityID);
                        data.WriteBit(!HasCurrentRound);

                        if (HasAuraInstanceID)
                            effectTargetData << uint32(target.Aura.AuraInstanceId);

                        if (HasRoundsRemaining)
                            effectTargetData << int32(target.Aura.RoundsRemaining);

                        if (HasCurrentRound)
                            effectTargetData << uint32(target.Aura.CurrentRound);

                        if (HasAuraAbilityID)
                            effectTargetData << uint32(target.Aura.AbilityId);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_NPC_EMOTE:
                    {
                        bool HasBroadcastTextID = target.BroadcastTextId;

                        data.WriteBit(!HasBroadcastTextID);

                        if (HasBroadcastTextID)
                            effectTargetData << uint32(target.BroadcastTextId);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_ABILITY:
                    {
                        bool HasChangedAbilityID = target.Ability.ChangedAbilityId != 0;
                        bool HasCooldownRemaining = target.Ability.CooldownRemaining != 0;
                        bool HasLockdownRemaining = target.Ability.LockdownRemaining != 0;

                        data.WriteBit(!HasChangedAbilityID);
                        data.WriteBit(!HasCooldownRemaining);
                        data.WriteBit(!HasLockdownRemaining);

                        if (HasChangedAbilityID)
                            effectTargetData << uint32(target.Ability.ChangedAbilityId);

                        if (HasCooldownRemaining)
                            effectTargetData << uint32(target.Ability.CooldownRemaining);

                        if (HasLockdownRemaining)
                            effectTargetData << uint32(target.Ability.LockdownRemaining);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_TRIGGER_ABILITY:
                    {
                        bool HasTriggerAbilityID = target.TriggerAbilityId != 0;

                        data.WriteBit(!HasTriggerAbilityID);

                        if (HasTriggerAbilityID)
                            effectTargetData << uint32(target.TriggerAbilityId);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_STATE:
                    {
                        bool HasStateID = target.State.StateId != 0;
                        bool HasStateValue = target.State.Value != 0;

                        data.WriteBit(!HasStateID);
                        data.WriteBit(!HasStateValue);

                        if (HasStateValue)
                            effectTargetData << uint32(target.State.Value);

                        if (HasStateID)
                            effectTargetData << uint32(target.State.StateId);

                        break;
                    }
                    case PET_BATTLE_EFFECT_TARGET_UPDATE_STAT:
                    {
                        bool HasStatValue = target.StatValue != 0;

                        data.WriteBit(!HasStatValue);

                        if (HasStatValue)
                            effectTargetData << uint32(target.StatValue);

                        break;
                    }
                    default:
                        break;
                }

                data.WriteBit(!HasPetTarget);
            }
        }

        data.WriteBit(!HasSource);
        data.WriteBit(!HasTurnInstance);
        data.WriteBit(!HasStackDepth);
        data.WriteBit(!HasFlags);

        if (HasFlags)
            effectData << uint16(effect.GetFlags());

        if (HasSourceAuraInstanceID)
            effectData << uint16(effect.GetAuraInstance());

        if (HasAbilityEffectID)
            effectData << uint32(effect.GetAbilityEffect());

        effectData.append(effectTargetData);

        if (HasPetEffect)
            effectData << uint8(effect.GetType());

        if (HasTurnInstance)
            effectData << uint16(effect.GetTurnInstance());

        if (HasSource)
            effectData << int8(effect.GetSource());

        if (HasStackDepth)
            effectData << uint8(effect.GetStackDepth());
    }

    data.WriteBit(!HasRoundResult);

    data.WriteBits(PetDiedList.size(), 3);
    data.WriteBits(abilitiesCount, 20);

    for (BattlePet* battlePet : team->BattlePets)
    {
        for (uint8 i = 0; i < BATTLE_PET_MAX_ABILITIES; i++)
        {
            BattlePetAbility* ability = battlePet->Abilities[i];
            if (!ability)
                continue;

            if (!ability->OnCooldown)
                continue;

            if (!ability->Cooldown)
                ability->OnCooldown = false;

            uint8 GlobalIndex = battlePet->GetGlobalIndex();

            bool HasGlobalIndex = true;

            data.WriteBit(!HasGlobalIndex);

            cooldownData << uint16(ability->Cooldown);
            cooldownData << uint16(ability->Lockdown);
            cooldownData << uint32(ability->AbilityId);

            cooldownData << uint8(i);

            if (HasGlobalIndex)
                cooldownData << uint8(GlobalIndex);
        }
    }

    data.append(cooldownData);
    data.append(effectData);

    for (uint8 Index : PetDiedList)
        data << uint8(Index);

    if (HasRoundResult)
        data << uint8(RoundResult);

    SendPacket(&data);
}

void WorldSession::SendPetBattleFinalRound(PetBattle* petBattle)
{
    bool IsPvPBattle = petBattle->GetType() != PET_BATTLE_TYPE_PVE;

    ByteBuffer battlePetData;

    uint32 battlePetCount = 0;
    for (PetBattleTeam* team : petBattle->Teams)
        battlePetCount += team->BattlePets.size();

    WorldPacket data(SMSG_PET_BATTLE_FINAL_ROUND, 3 + petBattle->Teams.size() * (1 + 4) + battlePetCount * (1 + 4 + 2 + 2 + 1 + 2 + 4));

    data.WriteBit(IsPvPBattle);

    for (PetBattleTeam* team : petBattle->Teams)
        data.WriteBit(petBattle->GetWinningTeam() == team);

    data.WriteBits(battlePetCount, 20);

    for (PetBattleTeam* team : petBattle->Teams)
    {
        for (BattlePet* battlePet : team->BattlePets)
        {
            // these are always true
            bool HasOldLevelData = true;
            bool HasLevelData = true;
            bool HasXPData = true;

            bool Caged = petBattle->GetCagedPet() == battlePet;
            bool Captured = Caged && petBattle->GetWinningTeam() != team;
            bool SeenAction = team->SeenAction.find(battlePet->GetGUID()) != team->SeenAction.end();
            bool AwardedXP = SeenAction && !IsPvPBattle  && petBattle->GetWinningTeam() == team && battlePet->GetLevel() != BATTLE_PET_MAX_LEVEL && battlePet->IsAlive();

            data.WriteBit(!HasLevelData);
            data.WriteBit(Caged);
            data.WriteBit(AwardedXP);
            data.WriteBit(!HasXPData);
            data.WriteBit(Captured);
            data.WriteBit(!HasOldLevelData);
            data.WriteBit(SeenAction);

            battlePetData << uint32(battlePet->GetCurrentHealth());

            if (HasOldLevelData)
                battlePetData << uint16(battlePet->GetOldLevel());

            if (HasXPData)
                battlePetData << uint16(battlePet->GetXp());

            battlePetData << uint8(battlePet->GetGlobalIndex());

            if (HasLevelData)
                battlePetData << uint16(battlePet->GetLevel());

            battlePetData << uint32(battlePet->GetMaxHealth());
        }
    }

    data.WriteBit(true); // IsAbandoned

    data.append(battlePetData);

    for (PetBattleTeam* team : petBattle->Teams)
        data << uint32(0);                          // NpcCreatureID

    SendPacket(&data);
}

void WorldSession::SendPetBattleFinished()
{
    WorldPacket data(SMSG_PET_BATTLE_FINISHED, 0);

    SendPacket(&data);
}

// -------------------------------------------------------------------------------

void WorldSession::SetPetBattleQueueStatus(uint32 Status, uint32 WaitTime, uint32 AverageWaitTime)
{
    bool HasAverageWaitTime = AverageWaitTime != 0;

    ObjectGuid guid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_PET_BATTLE_QUEUE_STATUS, 1 + 8 + 3 + 4 + 4 + 4 + 4 + 4 + 4);

    data.WriteGuidMask(guid, 7, 2, 6, 1);

    data.WriteBit(HasAverageWaitTime);

    data.WriteGuidMask(guid, 4);

    data.WriteBits(0, 22);      // always 0

    data.WriteGuidMask(guid, 0);

    data.WriteBit(true);

    data.WriteGuidMask(guid, 3, 5);

    data.WriteGuidBytes(guid, 2, 4);

    data << uint32(getMSTime());

    data.WriteGuidBytes(guid, 3);

    data << uint32(Status);

    data.WriteGuidBytes(guid, 6);

    data << uint32(WaitTime);

    data.WriteGuidBytes(guid, 1);

    data << uint32(0);      // Type

    data.WriteGuidBytes(guid, 5, 7);

    data << uint32(0);      // Id

    data.WriteGuidBytes(guid, 0);

    if (HasAverageWaitTime)
        data << uint32(AverageWaitTime);

    SendPacket(&data);
}

void WorldSession::SetPetBattleQueueError(uint32 Error)
{
    ObjectGuid guid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_PET_BATTLE_QUEUE_STATUS, 1 + 8 + 3 + 4 + 4 + 4 + 4);

    data.WriteGuidMask(guid, 7, 2, 6, 1);

    data.WriteBit(false);       // always false

    data.WriteGuidMask(guid, 4);

    data.WriteBits(0, 22);      // always 0

    data.WriteGuidMask(guid, 0);

    data.WriteBit(false);       // always false

    data.WriteGuidMask(guid, 3, 5);

    data.WriteGuidBytes(guid, 2, 4);

    data << uint32(getMSTime());

    data.WriteGuidBytes(guid, 3);

    data << uint32(Error);

    data.WriteGuidBytes(guid, 6);

    data.WriteGuidBytes(guid, 1);

    data << uint32(0);      // Type

    data.WriteGuidBytes(guid, 5, 7);

    data << uint32(0);      // Id

    data.WriteGuidBytes(guid, 0);

    SendPacket(&data);
}

void WorldSession::SetPetBattleQueueSlotsError(PetBattleSlotErrorArray SlotErrors)
{
    ObjectGuid guid = GetPlayer()->GetGUID();

    WorldPacket data(SMSG_PET_BATTLE_QUEUE_STATUS, 1 + 8 + 3 + 4 + 4 + 4 + 4 + (SlotErrors.size() * 4));

    data.WriteGuidMask(guid, 7, 2, 6, 1);

    data.WriteBit(false);       // always false

    data.WriteGuidMask(guid, 4);

    data.WriteBits(BATTLE_PET_MAX_LOADOUT_SLOTS, 22);

    data.WriteGuidMask(guid, 0);

    data.WriteBit(false);       // always false

    data.WriteGuidMask(guid, 3, 5);

    data.WriteGuidBytes(guid, 2, 4);

    data << uint32(getMSTime());

    data.WriteGuidBytes(guid, 3);

    data << uint32(PET_BATTLE_QUEUE_ERROR_SLOT);

    data.WriteGuidBytes(guid, 6);

    data.WriteGuidBytes(guid, 1);

    data << uint32(0);      // Type

    data.WriteGuidBytes(guid, 5, 7);

    data << uint32(0);      // Id

    data.WriteGuidBytes(guid, 0);

    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; ++i)
        data << uint32(SlotErrors[i]);

    SendPacket(&data);
}

void WorldSession::SetPetBattleQueueProposeMatch()
{
    WorldPacket data(SMSG_BATTLE_PET_QUEUE_PROPOSE_MATCH, 0);

    SendPacket(&data);
}
