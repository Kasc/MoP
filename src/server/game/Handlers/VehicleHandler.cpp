/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Vehicle.h"
#include "Player.h"
#include "Log.h"
#include "ObjectAccessor.h"
#include "MovementStructures.h"

void WorldSession::HandleDismissControlledVehicle(WorldPacket& recvData)
{
    Unit* mover = _player->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    MovementInfo mi;
    mi.ReadFromPacket(recvData);

    if (mi.guid != mover->GetGUID())
        return;

    mi.SanitizeFlags(mover);

    mover->SetMovementInfo(&mi);

    _player->ExitVehicle();
}

void WorldSession::HandleChangeSeatsOnControlledVehicle(WorldPacket& recvData)
{
    Unit* vehicle_base = GetPlayer()->GetVehicleBase();
    if (!vehicle_base)
    {
        recvData.rfinish();
        return;
    }

    VehicleSeatEntry const* seat = GetPlayer()->GetVehicle()->GetSeatForPassenger(GetPlayer());
    if (!seat->CanSwitchFromSeat())
    {
        TC_LOG_DEBUG("network", "HandleChangeSeatsOnControlledVehicle, Opcode: %u, Player %u tried to switch seats but current seatflags %u don't permit that.",
            recvData.GetOpcode(), GetPlayer()->GetGUID().GetCounter(), seat->Flags);

        recvData.rfinish();
        return;
    }

    switch (recvData.GetOpcode())
    {
        case CMSG_REQUEST_VEHICLE_PREV_SEAT:
            GetPlayer()->ChangeSeat(-1, false);
            break;
        case CMSG_REQUEST_VEHICLE_NEXT_SEAT:
            GetPlayer()->ChangeSeat(-1, true);
            break;
        case CMSG_CHANGE_SEATS_ON_CONTROLLED_VEHICLE:
        {
            static MovementStatusElements const accessoryGuid[] = // 5.4.8 18414
            {
                MSEExtraInt8,
                MSEHasGuidByte2,
                MSEHasGuidByte1,
                MSEHasGuidByte7,
                MSEHasGuidByte5,
                MSEHasGuidByte3,
                MSEHasGuidByte6,
                MSEHasGuidByte4,
                MSEHasGuidByte0,
                MSEGuidByte5,
                MSEGuidByte4,
                MSEGuidByte7,
                MSEGuidByte1,
                MSEGuidByte3,
                MSEGuidByte2,
                MSEGuidByte6,
                MSEGuidByte0
            };

            ExtraMovementStatusElement extra(accessoryGuid);

            MovementInfo movementInfo;
            movementInfo.ReadFromPacket(recvData, &extra);

            if (vehicle_base->GetGUID() != movementInfo.guid)
                return;

            movementInfo.SanitizeFlags(vehicle_base);

            vehicle_base->SetMovementInfo(&movementInfo);

            ObjectGuid accessory = extra.GetData().guid;
            int8 seatId = extra.GetData().byteData;

            if (!accessory)
                GetPlayer()->ChangeSeat(-1, seatId > 0); // prev/next
            else if (Unit* vehUnit = ObjectAccessor::GetUnit(*GetPlayer(), accessory))
            {
                if (Vehicle* vehicle = vehUnit->GetVehicleKit())
                    if (vehicle->HasEmptySeat(seatId))
                        vehUnit->HandleSpellClick(GetPlayer(), seatId);
            }
            break;
        }
        case CMSG_REQUEST_VEHICLE_SWITCH_SEAT:
        {
            int8 seatID = -1;
            ObjectGuid vehicleGuid;

            recvData >> seatID;

            recvData.ReadGuidMask(vehicleGuid, 7, 3, 0, 1, 6, 4, 5, 2);
            recvData.ReadGuidBytes(vehicleGuid, 5, 0, 2, 3, 7, 4, 6, 1);

            if (vehicle_base->GetGUID() == vehicleGuid)
                GetPlayer()->ChangeSeat(seatID);
            else if (Unit* vehUnit = ObjectAccessor::GetUnit(*GetPlayer(), vehicleGuid))
                if (Vehicle* vehicle = vehUnit->GetVehicleKit())
                    if (vehicle->HasEmptySeat(seatID))
                        vehUnit->HandleSpellClick(GetPlayer(), seatID);
            break;
        }
        default:
            break;
    }
}

void WorldSession::HandleEnterPlayerVehicle(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 5, 7, 3, 0, 2, 4, 6, 1);
    recvData.ReadGuidBytes(guid, 5, 3, 1, 2, 7, 0, 6, 4);

    if (Player* player = ObjectAccessor::GetPlayer(*_player, guid))
    {
        if (!player->GetVehicleKit())
            return;

        if (!player->IsInRaidWith(_player))
            return;

        if (!_player->IsWithinDistInMap(player, INTERACTION_DISTANCE))
            return;
        // Dont' allow players to enter player vehicle on arena
        if (!_player->FindMap() || _player->FindMap()->IsBattleArena())
            return;

        _player->EnterVehicle(player);
    }
}

void WorldSession::HandleEjectPassenger(WorldPacket& recvData)
{
    Vehicle* vehicle = _player->GetVehicleKit();
    if (!vehicle)
    {
        TC_LOG_DEBUG("network", "HandleEjectPassenger: %s is not in a vehicle!", GetPlayer()->GetGUID().ToString().c_str());

        recvData.rfinish();
        return;
    }

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 4, 2, 1, 6, 5, 0, 7, 3);
    recvData.ReadGuidBytes(guid, 2, 7, 0, 6, 4, 3, 5, 1);

    if (guid.IsUnit())
    {
        Unit* unit = ObjectAccessor::GetUnit(*_player, guid);
        if (!unit) // creatures can be ejected too from player mounts
        {
            TC_LOG_DEBUG("network", "%s tried to eject %s from vehicle, but the latter was not found in world!", GetPlayer()->GetGUID().ToString().c_str(), guid.ToString().c_str());
            return;
        }

        if (!unit->IsOnVehicle(vehicle->GetBase()))
        {
            TC_LOG_DEBUG("network", "%s tried to eject %s, but they are not in the same vehicle", GetPlayer()->GetGUID().ToString().c_str(), guid.ToString().c_str());
            return;
        }

        VehicleSeatEntry const* seat = vehicle->GetSeatForPassenger(unit);
        ASSERT(seat);
        if (seat->IsEjectable())
            unit->ExitVehicle();
        else
            TC_LOG_DEBUG("network", "Player %u attempted to eject %s from non-ejectable seat.", GetPlayer()->GetGUID().GetCounter(), guid.ToString().c_str());
    }
    else
        TC_LOG_DEBUG("network", "HandleEjectPassenger: %s tried to eject invalid %s ", GetPlayer()->GetGUID().ToString().c_str(), guid.ToString().c_str());
}

void WorldSession::HandleRequestVehicleExit(WorldPacket& /*recvData*/)
{
    if (Vehicle* vehicle = GetPlayer()->GetVehicle())
    {
        if (VehicleSeatEntry const* seat = vehicle->GetSeatForPassenger(GetPlayer()))
        {
            if (seat->CanEnterOrExit())
                GetPlayer()->ExitVehicle();
            else
                TC_LOG_DEBUG("network", "Player %u tried to exit vehicle, but seatflags %u (ID: %u) don't permit that.",
                GetPlayer()->GetGUID().GetCounter(), seat->ID, seat->Flags);
        }
    }
}

void WorldSession::HandleMoveSetVehicleRecAck(WorldPacket& recvData)
{
    Unit* mover = GetPlayer()->GetMover();
    if (!mover)
    {
        recvData.rfinish();
        return;
    }

    static MovementStatusElements const setVehicleRecId = MSEExtraInt32;
    ExtraMovementStatusElement extra(&setVehicleRecId);

    MovementInfo movementInfo;
    movementInfo.ReadFromPacket(recvData, &extra);

    HandleMovementAck(movementInfo, mover, MOVEMENT_ACK_VEHICLE_REC, SMSG_SET_VEHICLE_REC_ID, &extra);
}
