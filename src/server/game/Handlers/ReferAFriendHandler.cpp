/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "WorldSession.h"
#include "Player.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "RecruitAFriendRewardsStore.h"
#include "BattlePayShop.h"
#include "ReferAFriend.h"

void WorldSession::HandleRecruitAFriendOpcode(WorldPacket& recvData)
{
    if (!sWorld->getBoolConfig(CONFIG_RECRUIT_A_FRIEND_ENABLED))
        return;

    uint32 NameLength = 0;
    uint32 EmailLength = 0;
    uint32 TextLength = 0;

    std::string Name;
    std::string Email;
    std::string Text;

    EmailLength = recvData.ReadBits(9);
    NameLength = recvData.ReadBits(7);
    TextLength = recvData.ReadBits(10);

    Text = recvData.ReadString(TextLength);
    Email = recvData.ReadString(EmailLength);
    Name = recvData.ReadString(NameLength);

    // should never happen
    if (Email.empty())
    {
        TC_LOG_DEBUG("network", "CMSG_RECRUIT_A_FRIEND - Player (%s) is trying send mail to nobody!", GetPlayer()->GetGUID().ToString().c_str());
        SendRecruitAFriendError(ERR_RECRUIT_A_FRIEND_FAILED);
        return;
    }

    RaFData* rafData = GetRaFData();
    if (!rafData)
    {
        TC_LOG_DEBUG("network", "CMSG_RECRUIT_A_FRIEND - Player (%s) does not have valid RaF Data.", GetPlayer()->GetGUID().ToString().c_str());
        SendRecruitAFriendError(ERR_RECRUIT_A_FRIEND_FAILED);
        return;
    }

    if (rafData->GetRecruitsCount() >= sWorld->getIntConfig(CONFIG_MAX_RECRUIT_A_FRIEND_ACTIVE_COUNT))
    {
        TC_LOG_DEBUG("network", "CMSG_RECRUIT_A_FRIEND - Player (%s) exceeded max number of recruited friends.", GetPlayer()->GetGUID().ToString().c_str());
        SendRecruitAFriendError(ERR_RECRUIT_A_FRIEND_ACCOUNT_LIMIT);
        return;
    }

    if (rafData->GetInvitationsCount() >= sWorld->getIntConfig(CONFIG_MAX_RECRUIT_A_FRIEND_INVITATIONS_SENT))
    {
        TC_LOG_DEBUG("network", "CMSG_RECRUIT_A_FRIEND - Player (%s) exceeded max number of allowed invitations.", GetPlayer()->GetGUID().ToString().c_str());
        SendRecruitAFriendError(ERR_RECRUIT_A_FRIEND_ACCOUNT_LIMIT);
        return;
    }

    rafData->IncreaseInvitationsCount();

    // ToDo: web-side implementation
}

void WorldSession::HandleReferAFriendGrantLevelPropose(WorldPacket& recvData)
{
    if (!sWorld->getBoolConfig(CONFIG_REFER_A_FRIEND_ENABLED))
        return;

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 2, 1, 5, 3, 7, 4, 0, 6);
    recvData.ReadGuidBytes(guid, 1, 4, 2, 7, 5, 3, 6, 0);

    Player* target = ObjectAccessor::GetPlayer(*_player, guid);

    RaFData const* rafData = GetRaFData();

    // check cheating
    uint8 levels = _player->GetGrantableLevels();
    uint8 error = ERR_REFER_A_FRIEND_NONE;

    if (!target || !rafData)
        error = ERR_REFER_A_FRIEND_NO_TARGET;
    else if (levels == 0)
        error = ERR_REFER_A_FRIEND_INSUFFICIENT_GRANTABLE_LEVELS;
    else if (rafData->GetRecruiter() != target->GetSession()->GetAccountId())
        error = ERR_REFER_A_FRIEND_NOT_REFERRED_BY;
    else if (target->GetTeamId() != _player->GetTeamId())
        error = ERR_REFER_A_FRIEND_DIFFERENT_FACTION;
    else if (target->GetLevel() >= _player->GetLevel())
        error = ERR_REFER_A_FRIEND_TARGET_TOO_HIGH;
    else if (target->GetLevel() >= sWorld->getIntConfig(CONFIG_MAX_REFER_A_FRIEND_BONUS_PLAYER_LEVEL))
        error = ERR_REFER_A_FRIEND_GRANT_LEVEL_MAX_I;
    else if (target->GetGroup() != _player->GetGroup())
        error = ERR_REFER_A_FRIEND_NOT_IN_GROUP;
    else if (target->GetLevel() >= sDBCManager->GetMaxLevelForExpansion(target->GetSession()->Expansion()))
        error = ERR_REFER_A_FRIEND_INSUF_EXPAN_LVL;

    if (error > 0)
    {
        SendReferAFriendError(error, target);
        return;
    }

    ObjectGuid playerGUID = _player->GetGUID();

    WorldPacket data(SMSG_REFER_A_FRIEND_GRANT_LEVEL_PROPOSE, 1 + 8);

    data.WriteGuidMask(playerGUID, 6, 7, 2, 5, 3, 0, 1, 4);
    data.WriteGuidBytes(playerGUID, 2, 5, 6, 7, 1, 4, 3, 0);

    target->SendDirectMessage(&data);
}

void WorldSession::HandleReferAFriendGrantLevelAccept(WorldPacket& recvData)
{
    if (!sWorld->getBoolConfig(CONFIG_REFER_A_FRIEND_ENABLED))
        return;

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 2, 7, 5, 4, 3, 0, 1, 6);
    recvData.ReadGuidBytes(guid, 5, 3, 2, 7, 4, 1, 0, 6);

    Player* other = ObjectAccessor::GetPlayer(*_player, guid);
    if (!(other && other->GetSession()))
        return;

    if (GetAccountId() != other->GetRecruiterId())
        return;

    if (other->GetGrantableLevels())
    {
        other->SetGrantableLevels(other->GetGrantableLevels() - 1);
        _player->GiveLevel(_player->GetLevel() + 1);
    }
}

void WorldSession::SendReferAFriendError(uint32 error, Player* target)
{
    bool NotInGroupError = (error == ERR_REFER_A_FRIEND_NOT_IN_GROUP);

    WorldPacket data(SMSG_REFER_A_FRIEND_FAILURE, 4 + 1 + (NotInGroupError ? target->GetName().length() : 0));

    data << uint32(error);

    data.WriteBit(!NotInGroupError);

    if (NotInGroupError)
    {
        data.WriteBits(target->GetName().length(), 6);

        data.FlushBits();

        data.WriteString(target->GetName());
    }
    else
        data.FlushBits();

    SendPacket(&data);
}

void WorldSession::SendRecruitAFriendError(uint32 error)
{
    WorldPacket data(SMSG_RECRUIT_A_FRIEND_FAILURE, 1);

    data.WriteBits(error, 3);

    data.FlushBits();

    SendPacket(&data);
}

void WorldSession::SendRaFRewards(bool show)
{
    if (!sWorld->getBoolConfig(CONFIG_RECRUIT_A_FRIEND_REWARDS_ENABLED))
        return;

    RaFRewardsStore const& rafRewards = sRecruitAFriendRewardsStore->GetRaFRewards();

    uint32 result = BATTLE_PAY_SHOP_RESULT_OK;
    uint32 distributingResult = show ? BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_SHOW_DISTRIBUTION : BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_HIDE_DISTRIBUTION;

    ObjectGuid characterGUID = ObjectGuid::Empty;
    ObjectGuid distributeID = ObjectGuid::Empty;

    uint32 productType = BATTLE_PAY_SHOP_PRODUCT_TYPE_RAF_REWARD;

    bool IsRevoked = false;
    bool HasBattlePayShopDisplayItemInfo = true;
    bool HasBattlePetResult = false;
    bool HasUnk = false;
    bool HasBattlePayProduct = true;
    bool HasBattlepayDisplayInfo = false;
    bool HasAlreadyProduct = false;

    uint32 productFlags = BATTLE_PAY_SHOP_PRODUCT_FLAG_SHOW_PRODUCT | BATTLE_PAY_SHOP_PRODUCT_FLAG_AVAILABLE;
    uint32 productQuantity = 1;

    uint64 purchaseId = 0;
    uint32 ProductID = 300;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_DISTRIBUTION_UPDATE);

    data.WriteGuidMask(distributeID, 5, 0);

    data.WriteBit(HasBattlePayProduct);

    data.WriteGuidMask(distributeID, 1);

    data.WriteGuidMask(characterGUID, 4, 7, 0);

    data.WriteBit(IsRevoked);

    data.WriteGuidMask(characterGUID, 1, 2);

    ByteBuffer rewardData;
    if (HasBattlePayProduct)
    {
        data.WriteBits(BATTLE_PAY_SHOP_CHOISE_TYPE_NORMAL, 2);

        data.WriteBits(rafRewards.size(), 20);

        for (auto const& itr : rafRewards)
        {
            RaFReward const& reward = itr.second;

            data.WriteBit(HasBattlePayShopDisplayItemInfo);

            ByteBuffer displayData;
            if (HasBattlePayShopDisplayItemInfo)
            {
                uint32 DisplayID = reward.DisplayId;
                uint32 FileDataID = 0;
                uint32 DisplayFlags = 0;
                uint32 Unk = 0;

                bool HasDisplayInfoID = DisplayID != 0;
                bool HasDisplayFlags = DisplayFlags != 0;
                bool HasFileDataID = FileDataID != 0;
                bool HasUnk = Unk != 0;

                data.WriteBit(HasDisplayInfoID);

                data.WriteBits(0, 13);

                data.WriteBit(HasDisplayFlags);
                data.WriteBit(HasFileDataID);

                data.WriteBits(reward.Title.length(), 10);
                data.WriteBits(reward.Title2.length(), 10);

                data.WriteBit(HasUnk);

                if (HasDisplayInfoID)
                    displayData << uint32(DisplayID);

                if (HasFileDataID)
                    displayData << uint32(FileDataID);

                displayData.WriteString(reward.Title);

                if (HasDisplayFlags)
                    displayData << uint32(DisplayFlags);

                displayData.WriteString(reward.Title2);
                //displayData.WriteString(reward.Description);

                if (HasUnk)
                    displayData << uint32(Unk);
            }

            data.WriteBit(HasUnk);
            data.WriteBit(HasAlreadyProduct);
            data.WriteBit(HasBattlePetResult);

            if (HasBattlePetResult)
                data.WriteBits(0, 4); // PetResult

            rewardData << uint32(reward.Id);

            if (!displayData.empty())
                rewardData.append(displayData);

            rewardData << uint32(productQuantity);
            rewardData << uint32(reward.ItemId);
        }

        data.WriteBit(HasBattlepayDisplayInfo);
    }

    data.WriteGuidMask(distributeID, 7);

    data.WriteGuidMask(characterGUID, 6);

    data.WriteGuidMask(distributeID, 2);

    data.WriteGuidMask(characterGUID, 5);

    data.WriteGuidMask(distributeID, 3, 6);

    data.WriteGuidMask(characterGUID, 3);

    data.WriteGuidMask(distributeID, 4);

    if (HasBattlePayProduct)
    {
        if (!rewardData.empty())
            data.append(rewardData);

        data << uint32(ProductID);
        data << uint64(0); // NormalPrice
        data << uint64(0); // CurrentPrice
        data << uint8(productType);
        data << uint32(productFlags);
    }

    data << uint32(ProductID);

    data.WriteGuidBytes(characterGUID, 4);

    data << uint64(purchaseId);

    data.WriteGuidBytes(characterGUID, 1, 5);

    data.WriteGuidBytes(distributeID, 2, 4, 1, 0);

    data << uint32(realmID);

    data.WriteGuidBytes(distributeID, 7);

    data.WriteGuidBytes(characterGUID, 0, 7);

    data << uint32(realmID);
    data << uint32(distributingResult);

    data.WriteGuidBytes(characterGUID, 6);

    data.WriteGuidBytes(distributeID, 5, 6, 3);

    data.WriteGuidBytes(characterGUID, 3, 2);

    SendPacket(&data);
}
