/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "DatabaseEnv.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "MovementStructures.h"
#include "WaypointMovementGenerator.h"
#include "TaxiPathGraph.h"

void WorldSession::HandleEnableTaxiNodeOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 3, 1, 6, 0, 4, 7, 2, 5);
    recvData.ReadGuidBytes(guid, 1, 6, 3, 0, 4, 5, 7, 2);

    // cheating checks
    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleEnableTaxiNodeOpcode - %s not found.", guid.ToString().c_str());
        return;
    }

    SendLearnNewTaxiNode(unit);
}

void WorldSession::HandleTaxiNodeStatusQueryOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 4, 1, 3, 0, 5, 2, 6);
    recvData.ReadGuidBytes(guid, 7, 1, 5, 2, 4, 0, 6, 3);

    SendTaxiStatus(guid);
}

void WorldSession::SendTaxiStatus(ObjectGuid guid)
{
    // cheating checks
    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WorldSession::SendTaxiStatus - %s not found.", guid.ToString().c_str());
        return;
    }

    uint32 curloc = sObjectMgr->GetNearestTaxiNode(unit->GetPositionX(), unit->GetPositionY(), unit->GetPositionZ(), unit->GetMapId(), GetPlayer()->GetTeam());

    // not found nearest
    if (curloc == 0)
        return;

    TC_LOG_DEBUG("network", "WORLD: current location %u ", curloc);

    TaxiNodeStatus status = TAXISTATUS_NONE;
    if (unit->GetReactionTo(GetPlayer()) >= REP_NEUTRAL)
        status = GetPlayer()->m_taxi.IsTaximaskNodeKnown(curloc) ? TAXISTATUS_LEARNED : TAXISTATUS_UNLEARNED;
    else
        status = TAXISTATUS_NOT_ELIGIBLE;

    WorldPacket data(SMSG_TAXI_NODE_STATUS, 1 + 8 + 1);

    data.WriteGuidMask(guid, 6, 2, 7, 5, 4, 1);

    data.WriteBits(status, 2);

    data.WriteGuidMask(guid, 3, 0);

    data.FlushBits();

    data.WriteGuidBytes(guid, 0, 5, 2, 1, 4, 6, 7, 3);

    SendPacket(&data);
}

void WorldSession::HandleTaxiQueryAvailableNodes(WorldPacket& recvData)
{

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 1, 0, 4, 2, 5, 6, 3);
    recvData.ReadGuidBytes(guid, 0, 3, 7, 5, 2, 6, 4, 1);

    // cheating checks
    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_FLIGHTMASTER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleTaxiQueryAvailableNodes - %s not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    // unknown taxi node case
    if (SendLearnNewTaxiNode(unit))
        return;

    // known taxi node case
    SendTaxiMenu(unit);
}

void WorldSession::SendTaxiMenu(Creature* unit)
{
    // find current node
    uint32 curloc = sObjectMgr->GetNearestTaxiNode(unit->GetPositionX(), unit->GetPositionY(), unit->GetPositionZ(), unit->GetMapId(), GetPlayer()->GetTeam());
    if (!curloc)
        return;

    bool lastTaxiCheaterState = GetPlayer()->IsTaxiCheater();
    if (unit->GetEntry() == 29480)
        GetPlayer()->SetTaxiCheater(true); // Grimwing in Ebon Hold, special case. NOTE: Not perfect, Zul'Aman should not be included according to WoWhead, and I think taxicheat includes it.

    TC_LOG_DEBUG("network", "WORLD: CMSG_TAXI_NODE_STATUS_QUERY %u ", curloc);

    ObjectGuid guid = unit->GetGUID();

    bool ShowWindow = true;

    WorldPacket data(SMSG_TAXI_NODES_SHOW, 4 + (ShowWindow ? (1 + 8 + 4) : 0) + (MAX_TAXI_MASK_SIZE * 1));

    data.WriteBit(ShowWindow);

    if (ShowWindow)
        data.WriteGuidMask(guid, 3, 0, 4, 2, 1, 7, 6, 5);

    data.WriteBits(MAX_TAXI_MASK_SIZE, 24);

    data.FlushBits();

    if (ShowWindow)
    {
        data.WriteGuidBytes(guid, 0, 3);

        data << uint32(curloc);

        data.WriteGuidBytes(guid, 5, 2, 6, 1, 7, 4);
    }

    GetPlayer()->m_taxi.AppendTaximaskTo(data, GetPlayer()->IsTaxiCheater());

    SendPacket(&data);

    GetPlayer()->SetTaxiCheater(lastTaxiCheaterState);
}

void WorldSession::SendDoFlight(uint32 mountDisplayId, uint32 path, uint32 pathNode)
{
    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    while (GetPlayer()->GetMotionMaster()->GetCurrentMovementGeneratorType() == FLIGHT_MOTION_TYPE)
        GetPlayer()->GetMotionMaster()->MovementExpired(false);

    if (mountDisplayId)
        GetPlayer()->Mount(mountDisplayId);

    GetPlayer()->GetMotionMaster()->MoveTaxiFlight(path, pathNode);
}

bool WorldSession::SendLearnNewTaxiNode(Creature* unit)
{
    // find current node
    uint32 curloc = sObjectMgr->GetNearestTaxiNode(unit->GetPositionX(), unit->GetPositionY(), unit->GetPositionZ(), unit->GetMapId(), GetPlayer()->GetTeam());

    if (curloc == 0)
        return true;                                        // `true` send to avoid WorldSession::SendTaxiMenu call with one more curlock seartch with same false result.

    if (GetPlayer()->m_taxi.SetTaximaskNode(curloc))
    {
        WorldPacket msg(SMSG_TAXI_NEW_PATH, 0);
        SendPacket(&msg);

        ObjectGuid guid = unit->GetGUID();

        WorldPacket data(SMSG_TAXI_NODE_STATUS, 1 + 8 + 1);

        data.WriteGuidMask(guid, 6, 2, 7, 5, 4, 1);

        data.WriteBits(TAXISTATUS_LEARNED, 2);

        data.WriteGuidMask(guid, 3, 0);

        data.FlushBits();

        data.WriteGuidBytes(guid, 0, 5, 2, 1, 4, 6, 7, 3);

        SendPacket(&data);

        return true;
    }
    else
        return false;
}

void WorldSession::SendDiscoverNewTaxiNode(uint32 nodeid)
{
    if (GetPlayer()->m_taxi.SetTaximaskNode(nodeid))
    {
        WorldPacket msg(SMSG_TAXI_NEW_PATH, 0);
        SendPacket(&msg);
    }
}

void WorldSession::HandleActivateTaxiExpressOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint32 nodeCount = 0;

    recvData.ReadGuidMask(guid, 6, 7);

    nodeCount = recvData.ReadBits(22);

    recvData.ReadGuidMask(guid, 2, 0, 4, 3, 1, 5);

    recvData.ReadGuidBytes(guid, 2, 7, 1);

    std::vector<uint32> nodes;
    for (uint32 i = 0; i < nodeCount; i++)
    {
        uint32 node = 0;
        recvData >> node;

        if (!GetPlayer()->m_taxi.IsTaximaskNodeKnown(node) && !GetPlayer()->IsTaxiCheater())
        {
            SendActivateTaxiReply(ERR_TAXINOTVISITED);
            recvData.rfinish();
            return;
        }

        nodes.push_back(node);
    }

    if (nodes.empty())
    {
        recvData.rfinish();
        return;
    }

    recvData.ReadGuidBytes(guid, 0, 5, 3, 6, 4);

    Creature* npc = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_FLIGHTMASTER);
    if (!npc)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleActivateTaxiExpressOpcode - Unit (%s) not found or you can't interact with it.", guid.ToString().c_str());
        SendActivateTaxiReply(ERR_TAXITOOFARAWAY);
        return;
    }

    TC_LOG_DEBUG("network", "WORLD: Received CMSG_ACTIVATETAXIEXPRESS from %d to %d", nodes.front(), nodes.back());

    GetPlayer()->ActivateTaxiPathTo(nodes, npc);
}

void WorldSession::HandleActivateTaxiOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint32 FromNode = 0;
    uint32 ToNode = 0;

    recvData >> ToNode;
    recvData >> FromNode;

    recvData.ReadGuidMask(guid, 4, 0, 1, 2, 5, 6, 7, 3);
    recvData.ReadGuidBytes(guid, 1, 0, 6, 5, 2, 4, 3, 7);

    TC_LOG_DEBUG("network", "WORLD: Received CMSG_ACTIVATE_TAXI from %d to %d", FromNode, ToNode);

    Creature* npc = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_FLIGHTMASTER);
    if (!npc)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleActivateTaxiOpcode - %s not found or you can't interact with it.", guid.ToString().c_str());
        SendActivateTaxiReply(ERR_TAXITOOFARAWAY);
        return;
    }

    TaxiNodesEntry const* from = sTaxiNodesStore.LookupEntry(FromNode);
    TaxiNodesEntry const* to = sTaxiNodesStore.LookupEntry(ToNode);
    if (!to)
        return;

    if (!GetPlayer()->IsTaxiCheater())
    {
        if (!GetPlayer()->m_taxi.IsTaximaskNodeKnown(FromNode) || !GetPlayer()->m_taxi.IsTaximaskNodeKnown(ToNode))
        {
            SendActivateTaxiReply(ERR_TAXINOTVISITED);
            return;
        }
    }

    std::vector<uint32> nodes;
    sTaxiPathGraph->GetCompleteNodeRoute(from, to, GetPlayer(), nodes);
    GetPlayer()->ActivateTaxiPathTo(nodes, npc);
}

void WorldSession::SendActivateTaxiReply(ActivateTaxiReply reply)
{
    WorldPacket data(SMSG_TAXI_ACTIVATE_REPLY, 1);

    data.WriteBits(reply, 4);

    data.FlushBits();

    SendPacket(&data);
}

void WorldSession::HandleSetTaxiBenchmarkOpcode(WorldPacket& recvData)
{
    uint8 mode = 0;

    recvData >> mode;

    mode ? _player->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_TAXI_BENCHMARK) : _player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_TAXI_BENCHMARK);

    TC_LOG_DEBUG("network", "Client used \"/timetest %d\" command", mode);
}
