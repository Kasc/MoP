/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "WorldPacket.h"
#include "WorldSession.h"

#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "RatedInfo.h"
#include "RatedMgr.h"
#include "Chat.h"
#include "GameTime.h"
#include "Language.h"
#include "Log.h"
#include "Player.h"
#include "Object.h"
#include "Opcodes.h"
#include "DisableMgr.h"
#include "Group.h"
#include "ObjectGuid.h"
#include "Formulas.h"

void WorldSession::HandleBattlemasterHelloOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 4, 2, 0, 3, 5, 1, 7, 6);
    recvData.ReadGuidBytes(guid, 4, 1, 3, 2, 6, 7, 5, 0);

    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
        return;

    if (!unit->IsBattleMaster())
        return;

    unit->StopMoving();

    BattlegroundTypeId bgTypeId = sBattlegroundMgr->GetBattleMasterBG(unit->GetEntry());

    if (!_player->GetBGAccessByLevel(bgTypeId))
    {
        SendNotification(LANG_YOUR_BG_LEVEL_REQ_ERROR);
        return;
    }

    sBattlegroundMgr->BuildBattlegroundListPacket(guid, _player, bgTypeId);
}

void WorldSession::HandleBattlemasterJoinOpcode(WorldPacket& recvData)
{
    Group* grp = NULL;
    ObjectGuid guid;

    std::set<uint32> blacklistedSet;

    uint32 bgTypeId = 0;

    bool asGroup = false;
    bool isPremade = false;
    bool hasRoleMask = false;

    uint8 roleMask = 0;

    for (uint8 i = 0; i < 2; i++)
    {
        uint32 blacklistedId = 0;
        recvData >> blacklistedId;
        blacklistedSet.insert(blacklistedId);
    }

    recvData.ReadGuidMask(guid, 1, 7, 0, 3);

    asGroup = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 4);

    hasRoleMask = !recvData.ReadBit();

    recvData.ReadGuidMask(guid, 6, 2, 5);

    recvData.FlushBits();

    recvData.ReadGuidBytes(guid, 7, 2, 4, 5, 0, 6, 3, 1);

    if (hasRoleMask)
        recvData >> roleMask;

    bgTypeId = guid.GetCounter();

    if (!sBattlemasterListStore.LookupEntry(bgTypeId))
    {
        TC_LOG_ERROR("network", "Battleground: invalid bgtype (%u) received. possible cheater? player guid %u", bgTypeId, _player->GetGUID().GetCounter());
        return;
    }

    if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, bgTypeId, NULL))
    {
        ChatHandler(this).PSendSysMessage(LANG_BG_DISABLED);
        return;
    }

    BattlegroundTypeId bgTypeID = BattlegroundTypeId(bgTypeId);
    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeID, RATED_TYPE_NOT_RATED);

    BattlegroundQueueTypeId bgQueueTypeIdRandom = BattlegroundMgr::BGQueueTypeId(BATTLEGROUND_RB, RATED_TYPE_NOT_RATED);
    BattlegroundQueueTypeId bgQueueTypeIdRated = BattlegroundMgr::BGQueueTypeId(BATTLEGROUND_RATED_10_VS_10, RATED_TYPE_10v10);

    if (_player->InBattleground())
        return;

    Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeID);
    if (!bg)
        return;

    bool IsRandomBGQueue = bgQueueTypeId == BATTLEGROUND_QUEUE_RB;

    std::vector<BattlegroundQueueTypeId> battlegroundsByQueueType;
    if (IsRandomBGQueue)
    {
        for (uint8 i = BATTLEGROUND_QUEUE_AV; i < BATTLEGROUND_QUEUE_RB; ++i)
            battlegroundsByQueueType.push_back(BattlegroundQueueTypeId(i));
    }
    else
        battlegroundsByQueueType.push_back(bgQueueTypeId);

    GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_NONE;

    if (!asGroup)
    {
        if (GetPlayer()->IsUsingLfg())
            err = ERR_LFG_CANT_USE_BATTLEGROUND;

        if (!_player->CanJoinToBattleground(bg))
            err = ERR_GROUP_JOIN_BATTLEGROUND_DESERTERS;

        if (_player->InBattlegroundQueueForBattlegroundQueueType(bgQueueTypeIdRandom))
            err = ERR_IN_RANDOM_BG;

        if (_player->InBattlegroundQueue() && bgTypeId == BATTLEGROUND_RB)
            err = ERR_IN_NON_RANDOM_BG;

        if (_player->InBattlegroundQueueForBattlegroundQueueType(bgQueueTypeIdRated))
            err = ERR_BATTLEDGROUND_QUEUED_FOR_RATED;

        if (_player->InBattlegroundQueueForBattlegroundQueueType(bgQueueTypeId))
            err = ERR_BATTLEGROUND_DUPE_QUEUE;

        if (!_player->HasFreeBattlegroundQueueId())
            err = ERR_BATTLEGROUND_TOO_MANY_QUEUES;

        if (_player->IsLockedToRatedBG())
            err = ERR_BATTLEDGROUND_QUEUED_FOR_RATED;

        if (err != ERR_BATTLEGROUND_NONE)
        {
            sBattlegroundMgr->BuildStatusFailedPacket(bg, _player, 0, 0, err);
            return;
        }

        // check Freeze debuff
        if (_player->HasAura(9454))
            return;

        GroupQueueInfo* ginfo = NULL;
        uint32 avgTime = 0;

        // if we join rbg, it means: we join all queues!
        for (BattlegroundQueueTypeId bgQueueType : battlegroundsByQueueType)
        {
            BattlegroundTypeId bgTypeId = sBattlegroundMgr->BGTemplateId(bgQueueType);

            Battleground* bg2 = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeId);
            if (!bg2)
                continue;

            uint32 mapId = bg2->GetMapId();

            if (!blacklistedSet.empty() && blacklistedSet.count(mapId))
                continue;

            PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg2->GetMapId(), _player->GetLevel());
            if (!bracketEntry)
                continue;

            BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueType);
            ginfo = bgQueue.AddGroup(_player, nullptr, bgTypeId, bracketEntry, RATED_TYPE_NOT_RATED, false, isPremade, 0, 0, IsRandomBGQueue);

            if (!ginfo)
                continue;

            uint32 queueTimer = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
            if (!avgTime || queueTimer < avgTime)
                avgTime = queueTimer;

            sBattlegroundMgr->ScheduleQueueUpdate(0, RATED_TYPE_NOT_RATED, bgQueueType, bgTypeId, bracketEntry->GetBracketId());
        }

        if (hasRoleMask)
        {
            uint8 role = BG_ROLE_DPS;

            if ((roleMask & BG_ROLE_MASK_TANK) != 0)
                role = BG_ROLE_TANK;
            else if ((roleMask & BG_ROLE_MASK_HEALER) != 0)
                role = BG_ROLE_HEAL;

            ginfo->Role = role;
        }

        uint32 queueSlot = _player->AddBattlegroundQueueId(bgQueueTypeId);

        if (IsRandomBGQueue)
            _player->SetRandomBGForPlayer(BATTLEGROUND_RB);

        _player->SetOriginalBGForPlayer(bgTypeID);

        sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, _player, queueSlot, avgTime, ginfo->JoinTime, ginfo->ratedType, false);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player joined queue for bg queue type %u bg type %u: GUID %u, NAME %s",
                       bgQueueTypeId, bgTypeId, _player->GetGUID().GetCounter(), _player->GetName().c_str());
    }
    else
    {
        grp = _player->GetGroup();

        if (!grp)
            return;

        if (grp->GetLeaderGUID() != _player->GetGUID())
            return;

        ObjectGuid errorGuid;
        err = grp->CanJoinBattlegroundQueue(bg, bgQueueTypeId, 0, bg->GetMaxPlayersPerTeam(), false, 0, errorGuid);
        isPremade = (grp->GetMembersCount() >= bg->GetMinPlayersPerTeam());

        GroupQueueInfo* ginfo = NULL;
        uint32 avgTime = 0;

        if (!err)
        {
            TC_LOG_DEBUG("bg.battleground", "Battleground: the following players are joining as group:");

            // if we join rbg, it means: we join all queues!
            for (BattlegroundQueueTypeId bgQueueType : battlegroundsByQueueType)
            {
                BattlegroundTypeId bgTypeId = sBattlegroundMgr->BGTemplateId(bgQueueType);

                Battleground* bg2 = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeId);
                if (!bg2)
                    continue;

                uint32 mapId = bg2->GetMapId();

                if (!blacklistedSet.empty() && blacklistedSet.count(mapId))
                    continue;

                PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg2->GetMapId(), _player->GetLevel());
                if (!bracketEntry)
                    continue;

                BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueType);
                ginfo = bgQueue.AddGroup(_player, grp, bgTypeId, bracketEntry, RATED_TYPE_NOT_RATED, false, isPremade, 0, 0, IsRandomBGQueue);

                if (!ginfo)
                    continue;

                uint32 queueTimer = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
                if (!avgTime || queueTimer < avgTime)
                    avgTime = queueTimer;

                sBattlegroundMgr->ScheduleQueueUpdate(0, RATED_TYPE_NOT_RATED, bgQueueType, bgTypeId, bracketEntry->GetBracketId());
            }
        }

        for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* member = itr->GetSource();
            if (!member)
                continue;

            if (err)
            {
                sBattlegroundMgr->BuildStatusFailedPacket(bg, member, 0, 0, err, &errorGuid);
                continue;
            }

            uint32 queueSlot = member->AddBattlegroundQueueId(bgQueueTypeId);

            if (IsRandomBGQueue)
                member->SetRandomBGForPlayer(BATTLEGROUND_RB);

            member->SetOriginalBGForPlayer(bgTypeID);

            sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, member, queueSlot, avgTime, ginfo->JoinTime, ginfo->ratedType, true);

            TC_LOG_DEBUG("bg.battleground", "Battleground: player joined queue for bg queue type %u bg type %u: GUID %u, NAME %s",
                bgQueueTypeId, bgTypeId, member->GetGUID().GetCounter(), member->GetName().c_str());
        }

        TC_LOG_DEBUG("bg.battleground", "Battleground: group end");
    }
}

void WorldSession::HandleBattlemasterJoinRatedOpcode(WorldPacket & /*recvData*/)
{
    if (_player->InBattleground())
        return;

    //check existance
    Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_RATED_10_VS_10);
    if (!bg)
    {
        TC_LOG_DEBUG("network", "Battleground: template bg (10 vs 10) not found");
        return;
    }

    BattlegroundTypeId bgTypeID = bg->GetTypeID();

    if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, bgTypeID, NULL))
    {
        ChatHandler(this).PSendSysMessage(LANG_BG_DISABLED);
        return;
    }

    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeID, RATED_TYPE_10v10);

    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg->GetMapId(), _player->GetLevel());
    if (!bracketEntry)
        return;

    Group* grp = _player->GetGroup();
    if (!grp)
        return;

    if (grp->GetLeaderGUID() != _player->GetGUID())
        return;

    uint32 ratedSlot = RatedInfo::GetRatedSlotByType(RATED_TYPE_10v10);
    GroupRatedStats stats = grp->GetRatedStats(RATED_TYPE_10v10);

    uint32 TeamArenaRating = stats.averageRating;
    uint32 TeamMatchmakerRating = stats.averageMMR;

    if (TeamArenaRating <= 0)
        TeamArenaRating = 1;

    ObjectGuid errorGuid;
    GroupJoinBattlegroundResult err = grp->CanJoinBattlegroundQueue(bg, bgQueueTypeId, bg->GetMinPlayersPerTeam(), bg->GetMaxPlayersPerTeam(), true, ratedSlot, errorGuid);

    BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
    GroupQueueInfo* ginfo = NULL;
    uint32 avgTime = 0;

    if (!err)
    {
        TC_LOG_DEBUG("bg.battleground", "Battleground: the following players are joining as group:");
        ginfo = bgQueue.AddGroup(_player, grp, bgTypeID, bracketEntry, RATED_TYPE_10v10, true, true, TeamArenaRating, TeamMatchmakerRating, false);
        avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
    }

    for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member)
            continue;

        if (err)
        {
            sBattlegroundMgr->BuildStatusFailedPacket(bg, member, 0, RATED_TYPE_10v10, err, &errorGuid);
            continue;
        }

        uint32 queueSlot = member->AddBattlegroundQueueId(bgQueueTypeId);

        sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, member, queueSlot, avgTime, ginfo->JoinTime, ginfo->ratedType, true);

        member->SetOriginalBGForPlayer(bgTypeID);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player joined queue for bg queue type %u bg type %u: GUID %u, NAME %s",
            bgQueueTypeId, bgTypeID, member->GetGUID().GetCounter(), member->GetName().c_str());
    }

    sBattlegroundMgr->ScheduleQueueUpdate(TeamMatchmakerRating, RATED_TYPE_10v10, bgQueueTypeId, bgTypeID, bracketEntry->GetBracketId());
}

void WorldSession::HandlePVPLogDataOpcode(WorldPacket & /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_PVP_LOG_DATA Message");

    Battleground* bg = _player->GetBattleground();
    if (!bg)
        return;

    WorldPacket data;
    sBattlegroundMgr->BuildPvpLogDataPacket(&data, bg);
    SendPacket(&data);

    TC_LOG_DEBUG("network", "WORLD: Sent SMSG_PVP_LOG_DATA Message");
}

void WorldSession::HandleBattlegroundListOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_BATTLEGROUND_LIST Message");

    uint32 ListID;
    recvData >> ListID;

    BattlemasterListEntry const* bl = sBattlemasterListStore.LookupEntry(ListID);
    if (!bl)
    {
        TC_LOG_DEBUG("bg.battleground", "BattlegroundHandler: invalid ListID (%u) with player (Name: %s, GUID: %u) received.", ListID, _player->GetName().c_str(), _player->GetGUID().GetCounter());
        return;
    }

    sBattlegroundMgr->BuildBattlegroundListPacket(ObjectGuid::Empty, _player, BattlegroundTypeId(ListID));
}

void WorldSession::HandleBattlegroundPortOpcode(WorldPacket &recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_BATTLEGROUND_PORT Message");

    uint32 time = 0;
    uint32 queueSlot = 0;
    uint32 ratedtype = 0;

    bool accepted = false;

    ObjectGuid guid;

    accepted = recvData.ReadBit();

    recvData.FlushBits();

    recvData >> queueSlot;
    recvData >> ratedtype;
    recvData >> time;

    recvData.ReadGuidMask(guid, 6, 4, 2, 5, 0, 1, 7, 3);
    recvData.ReadGuidBytes(guid, 2, 5, 3, 0, 7, 4, 6, 1);

    if (!_player->InBattlegroundQueue())
    {
        TC_LOG_DEBUG("bg.battleground", "CMSG_BATTLEGROUND_PORT %s Slot: %u, RatedType: %u, Time: %u, Action: %u. Player not in queue!",
            GetPlayerInfo().c_str(), queueSlot, ratedtype, time, accepted);
        return;
    }

    if (_player->IsSuspendedQueueForBattlegroundQueueIndex(queueSlot))
    {
        TC_LOG_DEBUG("bg.battleground", "CMSG_BATTLEGROUND_PORT %s Slot: %u, RatedType: %u, Time: %u, Action: %u. Queue suspened!",
            GetPlayerInfo().c_str(), queueSlot, ratedtype, time, accepted);
        return;
    }

    BattlegroundQueueTypeId bgQueueTypeId = _player->GetBattlegroundQueueTypeId(queueSlot);
    if (bgQueueTypeId == BATTLEGROUND_QUEUE_NONE)
    {
        TC_LOG_DEBUG("bg.battleground", "CMSG_BATTLEGROUND_PORT %s Slot: %u, RatedType: %u, Time: %u, Action: %u. Invalid queueSlot!",
            GetPlayerInfo().c_str(), queueSlot, ratedtype, time, accepted);
        return;
    }

    BattlegroundQueueTypeId playerQueueType = bgQueueTypeId;

    GroupQueueInfo* ginfo = nullptr;
    Battleground* bg = nullptr;
    BattlegroundTypeId bgTypeId = BattlegroundMgr::BGTemplateId(bgQueueTypeId);

    bool IsRandomBGQueue = bgQueueTypeId == BATTLEGROUND_QUEUE_RB;

    std::vector<BattlegroundQueueTypeId> battlegroundsByQueueType;
    if (IsRandomBGQueue)
    {
        for (uint8 i = BATTLEGROUND_QUEUE_AV; i < BATTLEGROUND_QUEUE_RB; ++i)
            battlegroundsByQueueType.push_back(BattlegroundQueueTypeId(i));
    }
    else
        battlegroundsByQueueType.push_back(bgQueueTypeId);

    if (accepted)
    {
        for (BattlegroundQueueTypeId bgQueueType : battlegroundsByQueueType)
        {
            BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueType);
            ginfo = bgQueue.GetPlayerGroupInfoData(_player->GetGUID());
            if (!ginfo)
                continue;

            if (ginfo->IsInvitedToBGInstanceGUID)
            {
                playerQueueType = bgQueueType;
                if (!ratedtype)
                    bgTypeId = BattlegroundMgr::BGTemplateId(bgQueueType);
                else
                    bgTypeId = BATTLEGROUND_TYPE_NONE;
                break;
            }
        }

        if (!ginfo)
        {
            TC_LOG_DEBUG("bg.battleground", "CMSG_BATTLEGROUND_PORT %s Slot: %u, RatedType: %u, Time: %u, Action: %u. Player not in queue (No player Group Info)!",
                GetPlayerInfo().c_str(), queueSlot, ratedtype, time, accepted);
            return;
        }

        bg = sBattlegroundMgr->GetBattleground(ginfo->IsInvitedToBGInstanceGUID, bgTypeId);
    }
    else
        bg = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeId);

    if (!bg)
    {
        TC_LOG_DEBUG("bg.battleground", "CMSG_BATTLEGROUND_PORT %s Slot: %u, RatedType: %u, Time: %u, Action: %u. Cant find BG with type %u!",
            GetPlayerInfo().c_str(), queueSlot, ratedtype, time, accepted, bgTypeId);
        return;
    }

    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg->GetMapId(), _player->GetLevel());
    if (!bracketEntry)
        return;

    if (accepted)
    {
        if (ginfo->ratedType == 0)
        {
            if (!_player->CanJoinToBattleground(bg))
            {
                sBattlegroundMgr->BuildStatusFailedPacket(bg, _player, queueSlot, ratedtype, ERR_GROUP_JOIN_BATTLEGROUND_DESERTERS);
                accepted = false;
                TC_LOG_DEBUG("bg.battleground", "Player %s (%u) has a deserter debuff, do not port him to battleground!", _player->GetName().c_str(), _player->GetGUID().GetCounter());
            }

            if (_player->GetLevel() > bg->GetMaxLevel())
            {
                TC_LOG_DEBUG("network", "Player %s (%u) has level (%u) higher than maxlevel (%u) of battleground (%u)! Do not port him to battleground!",
                    _player->GetName().c_str(), _player->GetGUID().GetCounter(), _player->GetLevel(), bg->GetMaxLevel(), bg->GetTypeID());
                accepted = false;
            }
        }
    }

    TC_LOG_DEBUG("bg.battleground", "CMSG_BATTLEGROUND_PORT %s Slot: %u, RatedType: %u, Time: %u, Action: %u.",
        GetPlayerInfo().c_str(), queueSlot, ratedtype, time, accepted);

    if (accepted)
    {
        // check Freeze debuff
        if (_player->HasAura(9454))
            return;

        if (!_player->IsInvitedForBattlegroundQueueType(bgQueueTypeId))
            return;

        if (!_player->InBattleground())
            _player->SetBattlegroundEntryPoint();

        if (!_player->IsAlive())
        {
            _player->ResurrectPlayer(1.0f);
            _player->SpawnCorpseBones();
        }

        if (_player->IsInFlight())
        {
            _player->GetMotionMaster()->MovementExpired();
            _player->CleanupAfterTaxiFlight();
        }

        sBattlegroundMgr->BuildBattlegroundInProgressStatus(bg, _player, queueSlot, _player->GetBattlegroundQueueJoinTime(bgQueueTypeId), bg->GetElapsedTime(), bg->GetRatedType());

        if (Battleground* currentBg = _player->GetBattleground())
            currentBg->RemovePlayerAtLeave(_player->GetGUID(), false, true);

        _player->SetBattlegroundId(bg->GetInstanceID(), bgTypeId);
        _player->SetBGTeam(ginfo->team);

        sBattlegroundMgr->SendToBattleground(_player, ginfo->IsInvitedToBGInstanceGUID, bgTypeId);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player %s (%u) joined battle for bg %u, bgtype %u, queue type %u.", _player->GetName().c_str(), _player->GetGUID().GetCounter(), bg->GetInstanceID(), bg->GetTypeID(), bgQueueTypeId);
    }
    else
    {
        if (ginfo)
        {
            GetPlayer()->SuspendOtherQueues(false);

            if (ginfo->IsRated)
            {
                int16 matchmakerRatingChange, personalRatingChange;
                RatedInfo* rInfo = sRatedMgr->GetRatedInfo(_player->GetGUID());
                rInfo->UpdateStats(ginfo->ratedType, ginfo->OpponentsTeamMatchmakerRating, personalRatingChange, matchmakerRatingChange, false, true);
                TC_LOG_DEBUG("bg.battleground", "UPDATING memberLost's personal arena rating for %s by opponents rating: %u, because he has left queue!", _player->GetGUID().ToString().c_str(), ginfo->OpponentsTeamRating);
            }
        }

        sBattlegroundMgr->BuildBattlegroundNoneStatus(_player, queueSlot, time, RatedType(ratedtype));

        _player->RemoveBattlegroundQueueId(bgQueueTypeId);

        if (IsRandomBGQueue)
            _player->SetRandomBGForPlayer(BATTLEGROUND_TYPE_NONE);

        _player->SetOriginalBGForPlayer(BATTLEGROUND_TYPE_NONE);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player %s (%u) left queue for bgtype %u, queue type %u.", _player->GetName().c_str(), _player->GetGUID().GetCounter(), bg->GetTypeID(), bgQueueTypeId);
    }

    if (ginfo)
    {
        BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(playerQueueType);
        bgQueue.RemovePlayer(_player->GetGUID(), !accepted);
    }
    else
    {
        for (BattlegroundQueueTypeId bgQueueType : battlegroundsByQueueType)
        {
            BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueType);
            bgQueue.RemovePlayer(_player->GetGUID(), !accepted);
        }
    }
}

void WorldSession::HandleBattlegroundLeaveOpcode(WorldPacket& /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_BATTLEGROUND_LEAVE Message");

    if (_player->IsInCombat())
        if (Battleground* bg = _player->GetBattleground())
            if (bg->GetStatus() != STATUS_WAIT_LEAVE)
                return;

    _player->LeaveBattleground();
}

void WorldSession::HandleBattlegroundStatusOpcode(WorldPacket & /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: Recvd CMSG_BATTLEGROUND_STATUS Message");

    Battleground* bg = NULL;

    for (uint8 i = 0; i < PLAYER_MAX_BATTLEGROUND_QUEUES; ++i)
    {
        BattlegroundQueueTypeId bgQueueTypeId = _player->GetBattlegroundQueueTypeId(i);
        if (!bgQueueTypeId)
            continue;

        BattlegroundTypeId bgTypeId = BattlegroundMgr::BGTemplateId(bgQueueTypeId);
        RatedType ratedType = BattlegroundMgr::GetRatedTypeByQueue(bgQueueTypeId);

        if (bgTypeId == _player->GetBattlegroundTypeId())
        {
            bg = _player->GetBattleground();
            if (bg && bg->GetRatedType() == ratedType)
            {
                sBattlegroundMgr->BuildBattlegroundInProgressStatus(bg, GetPlayer(), i, _player->GetBattlegroundQueueJoinTime(bgQueueTypeId), bg->GetElapsedTime(), ratedType);
                continue;
            }
        }

        BattlegroundQueue& bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
        GroupQueueInfo* ginfo = bgQueue.GetPlayerGroupInfoData(_player->GetGUID());
        if (!ginfo)
            continue;
        if (ginfo->IsInvitedToBGInstanceGUID)
        {
            bg = sBattlegroundMgr->GetBattleground(ginfo->IsInvitedToBGInstanceGUID, bgTypeId);
            if (!bg)
                continue;

            if (GetPlayer()->IsSuspendedQueueForBattlegroundQueueIndex(i))
                continue;

            sBattlegroundMgr->BuildBattlegroundWaitJoinStatus(bg, GetPlayer(), i, getMSTimeDiff(GameTime::GetGameTimeMS(), ginfo->removeInviteTime), _player->GetBattlegroundQueueJoinTime(bgQueueTypeId), ratedType, ginfo->Role);
        }
        else
        {
            bg = sBattlegroundMgr->GetBattlegroundTemplate(bgTypeId);
            if (!bg)
                continue;

            PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg->GetMapId(), _player->GetLevel());
            if (!bracketEntry)
                continue;

            uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());

            sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, GetPlayer(), i, avgTime, _player->GetBattlegroundQueueJoinTime(bgQueueTypeId), ratedType, ginfo->players.size() > 1, GetPlayer()->IsSuspendedQueueForBattlegroundQueueIndex(i));
        }
    }
}

void WorldSession::HandleBattlemasterJoinArena(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: CMSG_BATTLEMASTER_JOIN_ARENA");

    uint8 arenaSlot;
    recvData >> arenaSlot;

    if (!RatedInfo::IsArenaSlot(arenaSlot))
        return;

    if (_player->InBattleground())
        return;

    uint32 TeamArenaRating = 0;
    uint32 TeamMatchmakerRating = 0;

    RatedType arenatype = RatedInfo::GetRatedTypeBySlot(arenaSlot);

    Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
    if (!bg)
    {
        TC_LOG_ERROR("network", "Battleground: template bg (all arenas) not found");
        return;
    }

    if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
    {
        ChatHandler(this).PSendSysMessage(LANG_ARENA_DISABLED);
        return;
    }

    BattlegroundTypeId bgTypeId = bg->GetTypeID();
    BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bg->GetMapId(), _player->GetLevel());
    if (!bracketEntry)
        return;

    Group* grp = _player->GetGroup();
    if (!grp)
        return;

    if (grp->GetLeaderGUID() != _player->GetGUID())
        return;

    GroupRatedStats stats = grp->GetRatedStats(arenatype);

    TeamArenaRating = stats.averageRating;
    TeamMatchmakerRating = stats.averageMMR;

    if (TeamArenaRating <= 0)
        TeamArenaRating = 1;

    BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);

    uint32 avgTime = 0;
    GroupQueueInfo* ginfo = NULL;

    ObjectGuid errorGuid;
    GroupJoinBattlegroundResult err = grp->CanJoinBattlegroundQueue(bg, bgQueueTypeId, arenatype, arenatype, true, arenaSlot, errorGuid);
    if (!err)
    {
        TC_LOG_DEBUG("bg.battleground", "Battleground: Group (GUID: %u), Leader (%s, GUID: %u) just queued arena (Average MMR: %u, Average Rating: %u, Type %u)",
            grp->GetGUID(), _player->GetName().c_str(), _player->GetGUID(), TeamMatchmakerRating, TeamArenaRating, arenatype);

        ginfo = bgQueue.AddGroup(_player, grp, bgTypeId, bracketEntry, arenatype, true, true, TeamArenaRating, TeamMatchmakerRating, false);
        avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
    }

    for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member)
            continue;

        if (err)
        {
            sBattlegroundMgr->BuildStatusFailedPacket(bg, member, 0, arenatype, err, &errorGuid);
            continue;
        }

        uint32 queueSlot = member->AddBattlegroundQueueId(bgQueueTypeId);

        sBattlegroundMgr->BuildBattlegroundWaitQueueStatus(bg, member, queueSlot, avgTime, ginfo->JoinTime, arenatype, true);

        member->SetOriginalBGForPlayer(bgTypeId);

        TC_LOG_DEBUG("bg.battleground", "Battleground: player joined queue for arena as group bg queue type %u bg type %u: GUID %u, NAME %s", bgQueueTypeId, bgTypeId, member->GetGUID().GetCounter(), member->GetName().c_str());
    }

    sBattlegroundMgr->ScheduleQueueUpdate(TeamMatchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());
}

void WorldSession::HandleReportPvPAFK(WorldPacket& recvData)
{
    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 3, 6, 1, 4, 5, 0, 7, 2);
    recvData.ReadGuidBytes(playerGuid, 3, 0, 4, 1, 7, 6, 2, 5);

    Player* reportedPlayer = ObjectAccessor::FindPlayer(playerGuid);
    if (!reportedPlayer)
    {
        TC_LOG_INFO("bg.reportpvpafk", "WorldSession::HandleReportPvPAFK: %s [IP: %s] reported %s", _player->GetName().c_str(), _player->GetSession()->GetRemoteAddress().c_str(), playerGuid.ToString().c_str());
        return;
    }

    TC_LOG_DEBUG("bg.battleground", "WorldSession::HandleReportPvPAFK: %s reported %s", _player->GetName().c_str(), reportedPlayer->GetName().c_str());

    reportedPlayer->ReportedAfkBy(_player);
}

void WorldSession::HandleRequestPvpOptions(WorldPacket& /*recvData*/)
{
    bool RatedBGsEnabled = true;
    bool RatedArenasEnabled = true;
    bool PugBGsEnabled = true;
    bool WargameBGsEnabled = true;
    bool WargameArenasEnabled = true;

    TC_LOG_DEBUG("network", "WORLD: CMSG_REQUEST_PVP_OPTIONS_ENABLED");

    WorldPacket data(SMSG_PVP_OPTIONS_ENABLED, 1);

    data.WriteBit(RatedBGsEnabled);
    data.WriteBit(WargameBGsEnabled);
    data.WriteBit(PugBGsEnabled);
    data.WriteBit(RatedArenasEnabled);
    data.WriteBit(WargameArenasEnabled);

    data.FlushBits();

    SendPacket(&data);
}

void WorldSession::HandleRequestPvpReward(WorldPacket& /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: CMSG_REQUEST_PVP_REWARDS");

    _player->SendPvpRewards();
}

void WorldSession::HandleRequestRatedInfo(WorldPacket& /*recvData*/)
{
    TC_LOG_DEBUG("network", "WORLD: CMSG_REQUEST_RATED_BG_STATS");

    RatedInfo* rInfo = sRatedMgr->GetRatedInfo(GetPlayer()->GetGUID());
    if (!rInfo)
        return;

    WorldPacket data(SMSG_BATTLEGROUND_RATED_INFO, MAX_RATED_SLOT * (4 + 4 + 4 + 4 + 4 + 4 + 4 + 4));

    for (uint8 i = 0; i < MAX_RATED_SLOT; ++i)
    {
        RatedType ratedType = RatedInfo::GetRatedTypeBySlot(i);

        const StatsBySlot* stats = rInfo->GetStatsBySlot(ratedType);
        ASSERT(stats && "Stats must be already initialized");

        data << uint32(stats->Rank);
        data << uint32(stats->WeekWins);
        data << uint32(stats->SeasonGames);
        data << uint32(stats->PersonalRating);
        data << uint32(stats->SeasonWins);
        data << uint32(stats->WeekBest);
        data << uint32(stats->WeekGames);
        data << uint32(stats->SeasonBest);
    }

    SendPacket(&data);
}

void WorldSession::HandleWargameRequest(WorldPacket& recvData)
{
    uint32 BattleTagID = 0;
    uint32 RealID = 0;

    recvData >> BattleTagID;
    recvData >> RealID;

    ObjectGuid bgGuid;
    ObjectGuid opponentGuid;

    recvData.ReadGuidMask(bgGuid, 5);

    recvData.ReadGuidMask(opponentGuid, 1);

    recvData.ReadGuidMask(bgGuid, 6);

    recvData.ReadGuidMask(opponentGuid, 0, 7, 4);

    recvData.ReadGuidMask(bgGuid, 7);

    recvData.ReadGuidMask(opponentGuid, 6);

    recvData.ReadGuidMask(bgGuid, 0);

    recvData.ReadGuidMask(opponentGuid, 3);

    recvData.ReadGuidMask(bgGuid, 2, 4);

    recvData.ReadGuidMask(opponentGuid, 2);

    recvData.ReadGuidMask(bgGuid, 3, 1);

    recvData.ReadGuidMask(opponentGuid, 5);

    recvData.ReadGuidBytes(bgGuid, 4);

    recvData.ReadGuidBytes(opponentGuid, 7, 2);

    recvData.ReadGuidBytes(bgGuid, 2);

    recvData.ReadGuidBytes(opponentGuid, 5);

    recvData.ReadGuidBytes(bgGuid, 3);

    recvData.ReadGuidBytes(opponentGuid, 3);

    recvData.ReadGuidBytes(bgGuid, 0);

    recvData.ReadGuidBytes(opponentGuid, 1);

    recvData.ReadGuidBytes(bgGuid, 1);

    recvData.ReadGuidBytes(opponentGuid, 4);

    recvData.ReadGuidBytes(bgGuid, 6);

    recvData.ReadGuidBytes(opponentGuid, 0, 6);

    recvData.ReadGuidBytes(bgGuid, 5, 7);

    Player* challenger = GetPlayer();
    Player* opponent = ObjectAccessor::FindConnectedPlayer(opponentGuid);
    if (!opponent)
    {
        sBattlegroundMgr->BuildStatusFailedPacket(NULL, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    if (challenger->InBattleground() || opponent->InBattleground())
    {
        sBattlegroundMgr->BuildStatusFailedPacket(NULL, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    Group* challengerGroup = challenger->GetGroup();
    Group* opponentGroup = opponent->GetGroup();
    if (!challengerGroup || !opponentGroup)
    {
        sBattlegroundMgr->BuildStatusFailedPacket(NULL, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    uint32 bgTypeId = (bgGuid.GetCounter() & uint32(0x0000FFFF));

    Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BattlegroundTypeId(bgTypeId));
    if (!bg)
    {
        sBattlegroundMgr->BuildStatusFailedPacket(NULL, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    BattlemasterListEntry const* battlemasterListEntry = sBattlemasterListStore.LookupEntry(bgTypeId);
    if (!battlemasterListEntry)
    {
        sBattlegroundMgr->BuildStatusFailedPacket(bg, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, bgTypeId, NULL))
    {
        ChatHandler(this).PSendSysMessage(LANG_BG_DISABLED);
        return;
    }

    // for arenas group sizes must be equal
    if (battlemasterListEntry->InstanceType == TYPE_ARENA)
        if (challengerGroup->GetMembersCount() != opponentGroup->GetMembersCount())
        {
            sBattlegroundMgr->BuildStatusFailedPacket(bg, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
            return;
        }

    // Send informative message to challenger
    SendWargameRequestMessage(bg->GetQueueId());

    // Send request to opponent
    opponent->GetSession()->SendWargameRequestToOpponent(challenger->GetGUID(), bg->GetQueueId(), bgTypeId);
}

void WorldSession::HandleWargameRequestResponse(WorldPacket& recvData)
{
    ObjectGuid battlegroundGuid;
    ObjectGuid challengerGuid;

    bool accept = false;

    recvData.ReadGuidMask(challengerGuid, 7, 3);

    recvData.ReadGuidMask(battlegroundGuid, 1);

    recvData.ReadGuidMask(challengerGuid, 4, 2);

    recvData.ReadGuidMask(battlegroundGuid, 3);

    recvData.ReadGuidMask(challengerGuid, 5);

    accept = recvData.ReadBit();

    recvData.ReadGuidMask(battlegroundGuid, 7, 6, 0, 2, 4);

    recvData.ReadGuidMask(challengerGuid, 0);

    recvData.ReadGuidMask(battlegroundGuid, 5);

    recvData.ReadGuidMask(challengerGuid, 6, 1);

    recvData.FlushBits();

    recvData.ReadGuidBytes(battlegroundGuid, 7);

    recvData.ReadGuidBytes(challengerGuid, 5);

    recvData.ReadGuidBytes(battlegroundGuid, 6);

    recvData.ReadGuidBytes(challengerGuid, 2, 1);

    recvData.ReadGuidBytes(battlegroundGuid, 1);

    recvData.ReadGuidBytes(challengerGuid, 0);

    recvData.ReadGuidBytes(battlegroundGuid, 2, 5);

    recvData.ReadGuidBytes(challengerGuid, 4);

    recvData.ReadGuidBytes(battlegroundGuid, 4, 3);

    recvData.ReadGuidBytes(challengerGuid, 3, 6, 7);

    recvData.ReadGuidBytes(battlegroundGuid, 0);

    uint32 bgTypeID = battlegroundGuid.GetCounter();

    Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BattlegroundTypeId(bgTypeID));
    if (!bg)
        return;

    Player* opponent = GetPlayer();
    Player* challenger = ObjectAccessor::FindConnectedPlayer(challengerGuid);
    if (!challenger || !opponent)
    {
        sBattlegroundMgr->BuildStatusFailedPacket(bg, opponent, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    if (!accept)
    {
        sBattlegroundMgr->BuildStatusFailedPacket(bg, challenger, 0, 0, ERR_WARGAME_REQUEST_FAILURE);
        return;
    }

    sBattlegroundMgr->CreateWarGameQueue(bg, challenger, opponent);
}

void WorldSession::SendWargameRequestMessage(ObjectGuid queueGuid)
{
    WorldPacket data(SMSG_WARGAME_REQUEST_SENT, 1 + 8);

    data.WriteGuidMask(queueGuid, 4, 3, 0, 2, 1, 6, 5, 7);
    data.WriteGuidBytes(queueGuid, 1, 4, 5, 6, 2, 0, 3, 7);

    SendPacket(&data);
}

void WorldSession::SendWargameRequestToOpponent(ObjectGuid guid, ObjectGuid queueGuid, uint32 bgTypeId)
{
    WorldPacket data(SMSG_WARGAME_CHECK_ENTRY, 2 * (1 + 8) + 4);

    data.WriteGuidMask(queueGuid, 6);

    data.WriteGuidMask(guid, 5);

    data.WriteGuidMask(queueGuid, 1);

    data.WriteGuidMask(guid, 1, 4);

    data.WriteGuidMask(queueGuid, 5);

    data.WriteGuidMask(guid, 6, 7);

    data.WriteGuidMask(queueGuid, 3);

    data.WriteGuidMask(guid, 3);

    data.WriteGuidMask(queueGuid, 2);

    data.WriteGuidMask(guid, 2, 0);

    data.WriteGuidMask(queueGuid, 0, 7, 4);

    data.FlushBits();

    data.WriteGuidBytes(queueGuid, 0);

    data.WriteGuidBytes(guid, 6, 0);

    data.WriteGuidBytes(queueGuid, 4);

    data.WriteGuidBytes(guid, 2);

    data << uint32(bgTypeId);

    data.WriteGuidBytes(guid, 1);

    data.WriteGuidBytes(queueGuid, 7);

    data.WriteGuidBytes(guid, 7);

    data.WriteGuidBytes(queueGuid, 1);

    data.WriteGuidBytes(guid, 3);

    data.WriteGuidBytes(queueGuid, 3, 2, 5, 6);

    data.WriteGuidBytes(guid, 4, 5);

    SendPacket(&data);
}

void WorldSession::HandleRequestConquestFormulaConstants(WorldPacket& /*recvData*/)
{
    WorldPacket data(SMSG_CONQUEST_FORMULA_CONSTANTS, 4 + 4 + 4 + 4 + 4);

    data << uint32(Trinity::Currency::PvpMinCPPerWeek);
    data << float(Trinity::Currency::PvpCPBaseCoefficient);
    data << float(Trinity::Currency::PvpCPExpCoefficient);
    data << uint32(Trinity::Currency::PvpMaxCPPerWeek);
    data << float(Trinity::Currency::PvpCPNumerator);

    SendPacket(&data);
}
