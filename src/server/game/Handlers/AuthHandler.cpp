/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Opcodes.h"
#include "WorldSession.h"
#include "WorldPacket.h"
#include "ObjectMgr.h"
#include "Config.hpp"

void WorldSession::SendAuthResponse(uint8 code, uint32 queuePos)
{
    ExpansionRequirementContainer const& classResult = sObjectMgr->GetClassExpansionRequirements();
    ExpansionRequirementContainer const& raceResult = sObjectMgr->GetRaceExpansionRequirements();

    if (classResult.empty() || raceResult.empty())
    {
        code = AUTH_FAILED;
        TC_LOG_ERROR("network", "Unable to retrieve class or race data.");
    }

    TC_LOG_DEBUG("network", "SMSG_AUTH_RESPONSE");

    std::vector<CharacterTemplate> Templates;
    if (HasPermission(rbac::RBAC_PERM_USE_CHARACTER_TEMPLATES))
        for (auto& templ : sObjectMgr->GetCharacterTemplates())
            Templates.emplace_back(templ.second);

    uint16 AlliancePlayerCount = 0;
    uint16 HordePlayerCount = 0;

    bool IsSuccess = code == AUTH_OK;
    bool IsQueued = queuePos > 0;

    bool HasAlliancePlayerCount = AlliancePlayerCount > 0;
    bool HasHordePlayerCount = HordePlayerCount > 0;

    bool IsExpansionTrial = false;
    bool IsVeteranTrial = false;
    bool ForceCharacterTemplate = false;

    bool HasForcedCharacterMigration = false;

    WorldPacket data(SMSG_AUTH_RESPONSE);

    ByteBuffer templateData;

    data.WriteBit(IsSuccess);

    if (IsSuccess)
    {
        data.WriteBits(1, 21);

        data.WriteBits(sObjectMgr->GetRealmName(realmID).length(), 8);
        data.WriteBits(sObjectMgr->GetNormalizedRealmName(realmID).length(), 8);

        data.WriteBit(true); // always true

        data.WriteBits(classResult.size(), 23);
        data.WriteBits(Templates.size(), 21);

        data.WriteBit(HasAlliancePlayerCount);
        data.WriteBit(ForceCharacterTemplate);
        data.WriteBit(IsVeteranTrial);
        data.WriteBit(HasHordePlayerCount);

        // Character Templates
        for (auto & itr : Templates)
        {
            CharacterTemplate const& _template = itr;

            data.WriteBits(_template.Classes.size(), 23);
            data.WriteBits(_template.Name.length(), 7);
            data.WriteBits(_template.Description.length(), 10);

            templateData.WriteString(_template.Description);
            templateData.WriteString(_template.Name);

            for (auto & iterator : _template.Classes)
            {
                CharcterTemplateClass const& classTemplate = iterator;

                templateData << uint8(classTemplate.ClassID);
                templateData << uint8(classTemplate.FactionGroup);
            }

            templateData << uint32(_template.TemplateSetId);
        }

        data.WriteBits(raceResult.size(), 23);
        data.WriteBit(IsExpansionTrial);
    }

    data.WriteBit(IsQueued);

    if (IsQueued)
        data.WriteBit(HasForcedCharacterMigration);

    data.FlushBits();

    if (IsQueued)
        data << uint32(queuePos);

    if (IsSuccess)
    {
        data << uint32(realmID);

        data.WriteString(sObjectMgr->GetRealmName(realmID));
        data.WriteString(sObjectMgr->GetNormalizedRealmName(realmID));

        for (auto race_ : raceResult)
        {
            data << race_.second;
            data << race_.first;
        }

        // Execute Character Templates
        data.append(templateData);

        for (auto class_ : classResult)
        {
            data << class_.second;
            data << class_.first;
        }

        data << uint32(0);

        if (HasHordePlayerCount)
            data << uint16(HordePlayerCount);

        data << uint8(Expansion());
        data << uint32(0);
        data << uint32(0);
        data << uint8(Expansion());

        if (HasAlliancePlayerCount)
            data << uint16(AlliancePlayerCount);

        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
    }

    data << uint8(code);

    SendPacket(&data);
}

void WorldSession::SendAuthWaitQueue(uint32 queuePos)
{
    if (queuePos)
    {
        bool HasFCM = false;

        WorldPacket data(SMSG_WAIT_QUEUE_UPDATE, 1 + 4);

        data.WriteBit(HasFCM);

        data << uint32(queuePos);

        SendPacket(&data);
    }
    else
    {
        WorldPacket data(SMSG_WAIT_QUEUE_FINISH, 0);
        SendPacket(&data);
    }
}

void WorldSession::SendClientCacheVersion(uint32 version)
{
    WorldPacket data(SMSG_CLIENTCACHE_VERSION, 4);
    data << uint32(version);
    SendPacket(&data);
}
