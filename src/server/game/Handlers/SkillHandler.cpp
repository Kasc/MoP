/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "DatabaseEnv.h"
#include "GossipDef.h"
#include "Log.h"
#include "ObjectAccessor.h"
#include "Opcodes.h"
#include "Player.h"
#include "Pet.h"
#include "WorldPacket.h"
#include "WorldSession.h"

void WorldSession::HandeSetTalentSpecialization(WorldPacket& recvData)
{
    uint32 specializationTabId = 0;

    recvData >> specializationTabId;

    Player* player = GetPlayer();

    if (specializationTabId >= MAX_SPECIALIZATIONS)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleSetSpecializationOpcode - specialization index %u out of range", specializationTabId);
        return;
    }

    ChrSpecializationEntry const* chrSpec = sDBCManager->GetChrSpecializationByIndexArray(player->GetClass(), specializationTabId);
    if (!chrSpec)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleSetSpecializationOpcode - specialization index %u not found", specializationTabId);
        return;
    }

    if (chrSpec->ClassID != player->GetClass())
    {
        TC_LOG_DEBUG("network", "WORLD: HandleSetSpecializationOpcode - specialization %u does not belong to class %u", chrSpec->ID, player->GetClass());
        return;
    }

    if (player->GetLevel() < MIN_SPECIALIZATION_LEVEL)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleSetSpecializationOpcode - player level too low for specializations");
        return;
    }

    player->LearnTalentSpecialization(chrSpec->ID);
}

void WorldSession::HandleLearnTalentOpcode(WorldPacket& recvData)
{
    uint32 talentCount = recvData.ReadBits(23);

    std::vector<uint16> talents(talentCount);
    for (uint32 i = 0; i < talentCount; ++i)
    {
        uint16 talentId = 0;
        recvData >> talentId;

        talents.push_back(talentId);
    }

    bool anythingLearned = false;

    TalentLearnResult result = TALENT_LEARN_OK;

    for (uint16 talentID : talents)
    {
        result = _player->LearnTalent(talentID);
        if (result == TALENT_LEARN_OK)
            anythingLearned = true;
    }

    if (anythingLearned)
        _player->SendTalentsInfoData();
    else
    {
        WorldPacket data(SMSG_LEARN_TALENT_FAILED, 4);

        data << uint32(result);

        SendPacket(&data);
    }
}

void WorldSession::HandleConfirmRespecWipe(WorldPacket& recvData)
{
    ObjectGuid respecMasterGUID;

    uint8 respecType = 0;

    recvData >> respecType;

    recvData.ReadGuidMask(respecMasterGUID, 7, 2, 6, 1, 4, 0, 3, 5);
    recvData.ReadGuidBytes(respecMasterGUID, 2, 4, 1, 3, 0, 5, 7, 6);

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(respecMasterGUID, UNIT_NPC_FLAG_TRAINER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleTalentWipeConfirmOpcode - %s not found or you can't interact with him.", respecMasterGUID.ToString().c_str());
        return;
    }

    if (!unit->CanResetTalents(_player))
        return;

    if (!_player->PlayerTalkClass->GetGossipMenu().HasMenuItemType(GOSSIP_OPTION_UNLEARNTALENTS))
        return;

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    if (!_player->ResetTalents())
        if (respecType == RESPEC_TYPE_TALENT)
        {
            WorldPacket data(SMSG_RESPEC_WIPE_CONFIRM, 1 + 1 + 4);    //you have not any talent
            data << uint8(0);
            data << uint8(0);
            data << uint32(0);
            SendPacket(&data);
            return;
        }

    _player->SendTalentsInfoData();
    unit->CastSpell(_player, 14867, true);                  //spell: "Untalent Visual Effect"
}

void WorldSession::HandleUnlearnSkillOpcode(WorldPacket& recvData)
{
    uint32 skillId = 0;

    recvData >> skillId;

    SkillRaceClassInfoEntry const* rcEntry = sDBCManager->GetSkillRaceClassInfo(skillId, GetPlayer()->GetRace(), GetPlayer()->GetClass());
    if (!rcEntry || !(rcEntry->Flags & SKILL_FLAG_UNLEARNABLE))
        return;

    GetPlayer()->SetSkill(skillId, 0, 0, 0);
}

void WorldSession::HandleArcheologyRequestHistory(WorldPacket& /*recvData*/)
{
    GetPlayer()->SendResearchHistory();
}
