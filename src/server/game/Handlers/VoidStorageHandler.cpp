/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "World.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "Player.h"
#include <list>
#include <vector>
#include <utility>

void WorldSession::SendVoidStorageTransferResult(VoidTransferError result)
{
    WorldPacket data(SMSG_VOID_TRANSFER_RESULT, 4);

    data << uint32(result);

    SendPacket(&data);
}

void WorldSession::HandleVoidStorageUnlock(WorldPacket& recvData)
{
    Player* player = GetPlayer();

    ObjectGuid npcGuid;

    recvData.ReadGuidMask(npcGuid, 3, 1, 5, 6, 4, 0, 7, 2);
    recvData.ReadGuidBytes(npcGuid, 4, 3, 6, 2, 1, 5, 7, 0);

    Creature* unit = player->GetNPCIfCanInteractWith(npcGuid, UNIT_NPC_FLAG_VAULTKEEPER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageUnlock - Unit (%s) not found or player can't interact with it.", npcGuid.ToString().c_str());
        return;
    }

    if (player->IsVoidStorageUnlocked())
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageUnlock - Player (GUID: %u, name: %s) tried to unlock void storage a 2nd time.", player->GetGUID().GetCounter(), player->GetName().c_str());
        return;
    }

    player->ModifyMoney(-int64(VOID_STORAGE_UNLOCK));
    player->UnlockVoidStorage();
}

void WorldSession::HandleVoidStorageQuery(WorldPacket& recvData)
{
    Player* player = GetPlayer();

    ObjectGuid npcGuid;

    recvData.ReadGuidMask(npcGuid, 1, 5, 6, 0, 7, 2, 3, 4);
    recvData.ReadGuidBytes(npcGuid, 1, 6, 4, 3, 7, 0, 2, 5);

    Creature* unit = player->GetNPCIfCanInteractWith(npcGuid, UNIT_NPC_FLAG_VAULTKEEPER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageQuery - Unit (%s) not found or player can't interact with it.", npcGuid.ToString().c_str());
        return;
    }

    if (!player->IsVoidStorageUnlocked())
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageQuery - Player (GUID: %u, name: %s) queried void storage without unlocking it.", player->GetGUID().GetCounter(), player->GetName().c_str());
        return;
    }

    VoidStorageItemMap const& storageItems = player->GetVoidStorageItemMap();

    std::vector<std::pair<uint8, VoidStorageItem>> storage;
    std::unordered_map<uint8, std::vector<uint32>> dynamicValues;

    uint32 dynamicValuesSize = 0;
    for (auto itr : storageItems)
    {
        uint8 slot = itr.first;
        VoidStorageItem const& voidItem = itr.second;

        if (voidItem.SaveStatus == VOID_STORAGE_ITEM_SAVE_STATUS_DELETE)
            continue;

        storage.emplace_back(slot, voidItem);

        // Dynamic modifiers
        Item const* item = voidItem.StorageItem;

        uint32 mask = item->GetUInt32Value(ITEM_MODIFIERS_MASK);

        dynamicValues[slot].push_back(mask);

        if (mask)
            for (uint8 i = 0; mask != 0; mask >>= 1, ++i)
                if ((mask & 1) != 0)
                    dynamicValues[slot].push_back(item->GetModifier(ItemModifier(i)));

        dynamicValuesSize += dynamicValues[slot].size();
    }

    WorldPacket data(SMSG_VOID_STORAGE_CONTENTS, 1 + storage.size() * (2 * (1 + 8) + 4 + 4 + 4 + 4 + 4) + dynamicValuesSize * (4));

    data.WriteBits(storage.size(), 7);

    ByteBuffer itemData;

    for (auto & itr : storage)
    {
        uint8 slot = itr.first;
        VoidStorageItem const& voidItem = itr.second;

        ObjectGuid itemId = voidItem.StorageItemId;

        Item const* item = voidItem.StorageItem;
        ObjectGuid creatorGuid = item->GetGuidValue(ITEM_CREATOR);

        data.WriteGuidMask(creatorGuid, 1, 3);

        data.WriteGuidMask(itemId, 1);

        data.WriteGuidMask(creatorGuid, 2);

        data.WriteGuidMask(itemId, 2);

        data.WriteGuidMask(creatorGuid, 5, 0);

        data.WriteGuidMask(itemId, 6, 5);

        data.WriteGuidMask(creatorGuid, 4);

        data.WriteGuidMask(itemId, 7, 3, 4, 0);

        data.WriteGuidMask(creatorGuid, 6, 7);

        itemData.WriteGuidBytes(creatorGuid, 4, 7);

        itemData.WriteGuidBytes(itemId, 6);

        itemData.WriteGuidBytes(creatorGuid, 6);

        itemData.WriteGuidBytes(itemId, 2);

        itemData << uint32(item->GetItemSuffixFactor());

        itemData.WriteGuidBytes(itemId, 7, 3);

        itemData.WriteGuidBytes(creatorGuid, 0);

        itemData << uint32(item->GetEntry());

        itemData.WriteGuidBytes(itemId, 0);

        itemData << int32(item->GetItemRandomPropertyId());

        itemData.WriteGuidBytes(creatorGuid, 2, 5, 3);

        itemData << uint32(slot);

        itemData.WriteGuidBytes(itemId, 5, 1);

        /* START OF DYNAMIC FIELDS PART */

        size_t dynamicSize = dynamicValues[slot].size();

        // all dynamic modifiers sent as uint32
        itemData << uint32(dynamicSize * sizeof(uint32));

        // handle all dynamic modifiers
        for (uint32 modifier : dynamicValues[slot])
            itemData << uint32(modifier);

        /* END OF DYNAMIC FIELDS PART */

        itemData.WriteGuidBytes(itemId, 4);

        itemData.WriteGuidBytes(creatorGuid, 1);
    }

    if (!itemData.empty())
        data.append(itemData);
    else
        data.FlushBits();

    SendPacket(&data);
}

void WorldSession::HandleVoidStorageTransfer(WorldPacket& recvData)
{
    Player* player = GetPlayer();

    ObjectGuid npcGuid;

    recvData.ReadGuidMask(npcGuid, 7, 4);

    uint32 countDeposit = recvData.ReadBits(24);

    if (countDeposit > VOID_STORAGE_MAX_DEPOSIT)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) wants to deposit more than 9 items (%u).", player->GetGUID().GetCounter(), player->GetName().c_str(), countDeposit);

        recvData.rfinish();
        return;
    }

    GuidVector depositGuids(countDeposit);

    for (uint32 i = 0; i < countDeposit; ++i)
        recvData.ReadGuidMask(depositGuids[i], 0, 3, 6, 5, 4, 2, 1, 7);

    uint32 countWithdraw = recvData.ReadBits(24);

    if (countWithdraw > VOID_STORAGE_MAX_WITHDRAW)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) wants to withdraw more than 9 items (%u).", player->GetGUID().GetCounter(), player->GetName().c_str(), countWithdraw);

        recvData.rfinish();
        return;
    }

    GuidVector withdrawGuids(countWithdraw);

    for (uint32 i = 0; i < countWithdraw; ++i)
        recvData.ReadGuidMask(withdrawGuids[i], 4, 0, 5, 7, 6, 1, 2, 3);

    recvData.ReadGuidMask(npcGuid, 6, 0, 3, 1, 2, 5);

    for (uint32 i = 0; i < countDeposit; ++i)
        recvData.ReadGuidBytes(depositGuids[i], 5, 6, 3, 4, 1, 7, 2, 0);

    recvData.ReadGuidBytes(npcGuid, 5);

    for (uint32 i = 0; i < countWithdraw; ++i)
        recvData.ReadGuidBytes(withdrawGuids[i], 0, 4, 1, 2, 6, 3, 7, 5);

    recvData.ReadGuidBytes(npcGuid, 1, 7, 4, 3, 2, 0, 6);

    Creature* unit = player->GetNPCIfCanInteractWith(npcGuid, UNIT_NPC_FLAG_VAULTKEEPER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Unit (%s) not found or player can't interact with it.", npcGuid.ToString().c_str());
        return;
    }

    if (!player->IsVoidStorageUnlocked())
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) queried void storage without unlocking it.", player->GetGUID().GetCounter(), player->GetName().c_str());
        return;
    }

    if (depositGuids.size() > player->GetNumOfVoidStorageFreeSlots())
    {
        SendVoidStorageTransferResult(VOID_TRANSFER_ERROR_FULL);
        return;
    }

    uint32 freeBagSlots = 0;
    if (withdrawGuids.size() != 0)
    {
        // make this a Player function
        for (uint8 i = INVENTORY_SLOT_BAG_START; i < INVENTORY_SLOT_BAG_END; i++)
            if (Bag* bag = player->GetBagByPos(i))
                freeBagSlots += bag->GetFreeSlots();

        for (uint8 i = INVENTORY_SLOT_ITEM_START; i < INVENTORY_SLOT_ITEM_END; i++)
            if (!player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
                ++freeBagSlots;
    }

    if (withdrawGuids.size() > freeBagSlots)
    {
        SendVoidStorageTransferResult(VOID_TRANSFER_ERROR_INVENTORY_FULL);
        return;
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    std::unordered_map<uint8, std::vector<uint32>> depositedDynamicValues;
    uint32 depositedDynamicValuesSize = 0;

    std::vector<std::pair<uint8, VoidStorageItem>> depositItems;
    for (ObjectGuid const& itemGuid : depositGuids)
    {
        Item* item = player->GetItemByGuid(itemGuid);
        if (!item)
        {
            TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) wants to deposit an invalid item (item guid: %s).", player->GetGUID().GetCounter(), player->GetName().c_str(), itemGuid.ToString().c_str());
            continue;
        }

        ObjectGuid voidItemGuid = ObjectGuid(sObjectMgr->GenerateVoidStorageItemId());

        VoidStorageItem voidItem;

        voidItem.StorageItemId = voidItemGuid;
        voidItem.StorageItem = item;
        voidItem.SaveStatus = VOID_STORAGE_ITEM_SAVE_STATUS_SAVE;

        uint8 slot = player->AddVoidStorageItem(voidItem);
        if (slot >= VOID_STORAGE_MAX_SLOT)
            continue;

        depositItems.emplace_back(slot, std::move(voidItem));

        // Dynamic modifiers
        uint32 mask = item->GetUInt32Value(ITEM_MODIFIERS_MASK);

        depositedDynamicValues[slot].push_back(mask);

        if (mask)
            for (uint8 i = 0; mask != 0; mask >>= 1, ++i)
                if ((mask & 1) != 0)
                    depositedDynamicValues[slot].push_back(item->GetModifier(ItemModifier(i)));

        depositedDynamicValuesSize += depositedDynamicValues[slot].size();

        player->MoveItemFromInventory(item->GetBagSlot(), item->GetSlot(), true);
        item->DeleteFromInventoryDB(trans);
        item->SaveToDB(trans);

        player->SaveVoidStorageItem(trans, slot);
    }

    int64 cost = depositItems.size() * VOID_STORAGE_STORE_ITEM;

    // SPELL_AURA_MOD_VOID_STORAGE_AND_TRANSMOGRIFY_COSTS
    AddPct(cost, player->GetTotalAuraModifier(SPELL_AURA_MOD_VOID_STORAGE_AND_TRANSMOGRIFY_COSTS));

    if (!player->HasEnoughMoney(cost))
    {
        SendVoidStorageTransferResult(VOID_TRANSFER_ERROR_NOT_ENOUGH_MONEY);
        return;
    }

    player->ModifyMoney(-cost);

    std::vector<VoidStorageItem const*> withdrawItems;
    for (ObjectGuid const& voidItemGuid : withdrawGuids)
    {
        uint8 slot = 0;
        VoidStorageItem const* voidItem = player->GetVoidStorageItem(voidItemGuid, slot);
        if (!voidItem)
        {
            TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) tried to withdraw an invalid item (id: %s)", player->GetGUID().GetCounter(), player->GetName().c_str(), voidItemGuid.ToString().c_str());
            continue;
        }

        Item* item = const_cast<Item*>(voidItem->StorageItem);
        if (!item)
        {
            TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) tried to withdraw an invalid item (id: %s)", player->GetGUID().GetCounter(), player->GetName().c_str(), voidItemGuid.ToString().c_str());
            continue;
        }

        ItemPosCountVec dest;
        InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, item->GetEntry(), item->GetCount());
        if (msg != EQUIP_ERR_OK)
        {
            SendVoidStorageTransferResult(VOID_TRANSFER_ERROR_INVENTORY_FULL);
            TC_LOG_DEBUG("network", "WORLD: HandleVoidStorageTransfer - Player (GUID: %u, name: %s) couldn't withdraw item id %s because inventory was full.", player->GetGUID().GetCounter(), player->GetName().c_str(), voidItemGuid.ToString().c_str());
            return;
        }

        withdrawItems.push_back(voidItem);

        player->DeleteVoidStorageItem(slot);

        item->SetState(ITEM_UNCHANGED);
        player->MoveItemToInventory(dest, item, true);
        player->SaveInventoryAndGoldToDB(trans);

        player->SaveVoidStorageItem(trans, slot);
    }

    CharacterDatabase.CommitTransaction(trans);

    ByteBuffer withdrawData;
    ByteBuffer depositData;

    WorldPacket data(SMSG_VOID_STORAGE_TRANSFER_CHANGES, 1 + withdrawItems.size() * (1 + 8) + depositItems.size() *
                    (2 * (1 + 8) + 4 + 4 + 4 + 4 + 4) + depositedDynamicValuesSize * (4));

    data.WriteBits(withdrawItems.size(), 4);

    for (VoidStorageItem const* voidItem : withdrawItems)
    {
        ObjectGuid itemId = voidItem->StorageItemId;

        data.WriteGuidMask(itemId, 1, 6, 7, 3, 2, 0, 4, 5);
        withdrawData.WriteGuidBytes(itemId, 7, 3, 1, 5, 4, 0, 6, 2);
    }

    data.WriteBits(depositItems.size(), 4);

    for (auto & itr : depositItems)
    {
        uint8 slot = itr.first;
        VoidStorageItem const& voidItem = itr.second;

        ObjectGuid itemId = voidItem.StorageItemId;

        Item const* item = voidItem.StorageItem;
        ObjectGuid creatorGuid = item->GetGuidValue(ITEM_CREATOR);

        data.WriteGuidMask(itemId, 0);

        data.WriteGuidMask(creatorGuid, 6, 4);

        data.WriteGuidMask(itemId, 3);

        data.WriteGuidMask(creatorGuid, 3);

        data.WriteGuidMask(itemId, 5, 7);

        data.WriteGuidMask(creatorGuid, 0, 5, 7);

        data.WriteGuidMask(itemId, 6, 4);

        data.WriteGuidMask(creatorGuid, 1);

        data.WriteGuidMask(itemId, 1);

        data.WriteGuidMask(creatorGuid, 2);

        data.WriteGuidMask(itemId, 2);

        depositData << uint32(slot);

        depositData.WriteGuidBytes(creatorGuid, 5);

        depositData << uint32(item->GetEntry());

        depositData.WriteGuidBytes(creatorGuid, 6, 3);

        depositData << uint32(item->GetItemSuffixFactor());

        depositData.WriteGuidBytes(creatorGuid, 2);

        depositData.WriteGuidBytes(itemId, 5);

        depositData << int32(item->GetItemRandomPropertyId());

        depositData.WriteGuidBytes(itemId, 3);

        depositData.WriteGuidBytes(creatorGuid, 7, 4, 1);

        depositData.WriteGuidBytes(itemId, 0, 4, 6);

        /* START OF DYNAMIC FIELDS PART */

        size_t dynamicSize = depositedDynamicValues[slot].size();

        // all dynamic modifiers sent as uint32
        depositData << uint32(dynamicSize * sizeof(uint32));

        // handle all dynamic modifiers
        for (uint32 modifier : depositedDynamicValues[slot])
            depositData << uint32(modifier);

        /* END OF DYNAMIC FIELDS PART */

        depositData.WriteGuidBytes(itemId, 1, 2);

        depositData.WriteGuidBytes(creatorGuid, 0);

        depositData.WriteGuidBytes(itemId, 7);
    }

    data.append(depositData);
    data.append(withdrawData);

    SendPacket(&data);

    SendVoidStorageTransferResult(VOID_TRANSFER_ERROR_NO_ERROR);
}

void WorldSession::HandleVoidSwapItem(WorldPacket& recvData)
{
    Player* player = GetPlayer();

    uint32 newSlot = 0;

    ObjectGuid npcGuid;
    ObjectGuid itemId;

    recvData >> newSlot;

    recvData.ReadGuidMask(npcGuid, 6);

    recvData.ReadGuidMask(itemId, 4, 7, 3, 2);

    recvData.ReadGuidMask(npcGuid, 4, 2);

    recvData.ReadGuidMask(itemId, 0, 1);

    recvData.ReadGuidMask(npcGuid, 7, 1);

    recvData.ReadGuidMask(itemId, 6);

    recvData.ReadGuidMask(npcGuid, 3, 5);

    recvData.ReadGuidMask(itemId, 5);

    recvData.ReadGuidMask(npcGuid, 0);

    recvData.ReadGuidBytes(npcGuid, 3, 5);

    recvData.ReadGuidBytes(itemId, 6);

    recvData.ReadGuidBytes(npcGuid, 4);

    recvData.ReadGuidBytes(itemId, 4);

    recvData.ReadGuidBytes(npcGuid, 0);

    recvData.ReadGuidBytes(itemId, 5, 7);

    recvData.ReadGuidBytes(npcGuid, 7, 2, 1);

    recvData.ReadGuidBytes(itemId, 1, 3);

    recvData.ReadGuidBytes(npcGuid, 6);

    recvData.ReadGuidBytes(itemId, 0, 2);

    Creature* unit = player->GetNPCIfCanInteractWith(npcGuid, UNIT_NPC_FLAG_VAULTKEEPER);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidSwapItem - Unit (%s) not found or player can't interact with it.", npcGuid.ToString().c_str());
        return;
    }

    if (!player->IsVoidStorageUnlocked())
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidSwapItem - Player (GUID: %u, name: %s) queried void storage without unlocking it.", player->GetGUID().GetCounter(), player->GetName().c_str());
        return;
    }

    uint8 oldSlot = 0;
    if (!player->GetVoidStorageItem(itemId, oldSlot))
    {
        TC_LOG_DEBUG("network", "WORLD: HandleVoidSwapItem - Player (GUID: %u, name: %s) requested swapping an invalid item (slot: %u, itemid: %s).", player->GetGUID().GetCounter(), player->GetName().c_str(), newSlot, itemId.ToString().c_str());
        return;
    }

    bool usedSrcSlot = player->GetVoidStorageItem(oldSlot) != nullptr; // should be always true
    bool usedDestSlot = player->GetVoidStorageItem(newSlot) != nullptr;

    ObjectGuid itemIdDest;
    if (usedDestSlot)
        itemIdDest = player->GetVoidStorageItem(newSlot)->StorageItemId;

    if (!player->SwapVoidStorageItem(oldSlot, newSlot))
    {
        SendVoidStorageTransferResult(VOID_TRANSFER_ERROR_INTERNAL_ERROR_1);
        return;
    }

    WorldPacket data(SMSG_VOID_ITEM_SWAP_RESPONSE, 1 + 2 * (1 + 8) + (usedDestSlot ? 4 : 0) + (usedSrcSlot ? 4 : 0));

    data.WriteBit(usedSrcSlot);

    data.WriteGuidMask(itemId, 4, 1, 6, 0, 3, 7, 2, 5);

    data.WriteBit(usedDestSlot);

    data.WriteGuidMask(itemIdDest, 6, 0, 3, 2, 1, 5, 7, 4);

    data.WriteBit(!usedDestSlot);
    data.WriteBit(!usedSrcSlot);

    data.FlushBits();

    data.WriteGuidBytes(itemIdDest, 3, 7, 2, 5, 0, 1, 4, 6);

    data.WriteGuidBytes(itemId, 0, 2, 7, 5, 6, 4, 3, 1);

    if (usedDestSlot)
        data << uint32(oldSlot);

    if (usedSrcSlot)
        data << uint32(newSlot);

    SendPacket(&data);
}
