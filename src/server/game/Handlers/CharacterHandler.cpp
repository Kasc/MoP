/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AccountMgr.h"
#include "Battleground.h"
#include "CalendarMgr.h"
#include "CharacterCache.h"
#include "Chat.h"
#include "Common.h"
#include "DatabaseEnv.h"
#include "GameTime.h"
#include "Group.h"
#include "Guild.h"
#include "GuildMgr.h"
#include "Language.h"
#include "LFGMgr.h"
#include "Log.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Pet.h"
#include "PlayerDump.h"
#include "Player.h"
#include "QueryCallback.h"
#include "RBAC.h"
#include "ReputationMgr.h"
#include "ScriptMgr.h"
#include "SharedDefines.h"
#include "SocialMgr.h"
#include "SystemConfig.h"
#include "Util.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Transport.h"
#include "BattlePayShop.h"
#include "GuildFinderMgr.h"

class CharLoginQueryHolder final : public SQLQueryHolder
{
    ObjectGuid m_guid;
    uint32 m_accountId;

public:
    CharLoginQueryHolder(ObjectGuid guid, uint32 accountId) : m_guid(guid), m_accountId(accountId) { }

    ObjectGuid GetGUID() const      { return m_guid; }
    uint32 GetAccountId() const     { return m_accountId; }
    bool Initialize();
};

class AccountLoginQueryHolder final : public SQLQueryHolder
{
    uint32 m_accountId;

public:
    AccountLoginQueryHolder(uint32 accountId) : m_accountId(accountId) { }

    uint32 GetAccountId() const       { return m_accountId; }
    bool Initialize();
};

bool CharLoginQueryHolder::Initialize()
{
    SetSize(MAX_CHAR_LOGIN_QUERY);

    bool res = true;
    uint32 lowGuid = m_guid.GetCounter();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_FROM, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GROUP_MEMBER);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_GROUP, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_INSTANCE);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_BOUND_INSTANCES, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_AURAS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_AURAS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_AURA_EFFECTS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_AURA_EFFECTS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_SPELL);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_SPELLS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_QUESTSTATUS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_QUEST_STATUS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_QUESTSTATUS_OBJECTIVES);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_QUEST_OBJECTIVE_STATUS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_DAILYQUESTSTATUS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_DAILY_QUEST_STATUS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_WEEKLYQUESTSTATUS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_WEEKLY_QUEST_STATUS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_MONTHLYQUESTSTATUS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_MONTHLY_QUEST_STATUS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_SEASONALQUESTSTATUS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_SEASONAL_QUEST_STATUS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_REPUTATION);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_REPUTATION, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_INVENTORY);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_INVENTORY, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_VOID_STORAGE);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_VOID_STORAGE, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_ACTIONS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_ACTIONS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_MAILCOUNT);
    stmt->setUInt32(0, lowGuid);
    stmt->setUInt64(1, uint64(time(NULL)));
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_MAIL_COUNT, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_MAILDATE);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_MAIL_DATE, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_SOCIALLIST);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_SOCIAL_LIST, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_HOMEBIND);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_HOME_BIND, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_SPELLCOOLDOWNS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_SPELL_COOLDOWNS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_SPELL_CHARGES);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_SPELL_CHARGES, stmt);

    if (sWorld->getBoolConfig(CONFIG_DECLINED_NAMES_USED))
    {
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_DECLINEDNAMES);
        stmt->setUInt32(0, lowGuid);
        res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_DECLINED_NAMES, stmt);
    }

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GUILD_MEMBER);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_GUILD, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_ACHIEVEMENTS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_CHAR_ACHIEVEMENTS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_CRITERIAPROGRESS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_CHAR_CRITERIA_PROGRESS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_EQUIPMENTSETS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_EQUIPMENT_SETS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_CUF_PROFILES);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_CUF_PROFILES, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_BGDATA);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_BG_DATA, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_GLYPHS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_GLYPHS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_TALENTS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_TALENTS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PLAYER_ACCOUNT_DATA);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_ACCOUNT_DATA, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_SKILLS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_SKILLS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_RANDOMBG);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_RANDOM_BG, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_WEEKENDBG);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_WEEKEND_BG, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_BANNED);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_BANNED, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_QUESTSTATUSREW);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_QUEST_STATUS_REW, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_ACCOUNT_INSTANCELOCKTIMES);
    stmt->setUInt32(0, m_accountId);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_INSTANCE_LOCK_TIMES, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PLAYER_CURRENCY);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_CURRENCY, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CORPSE_LOCATION);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_CORPSE_LOCATION, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_RESEARCH_HISTORY);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_RESEARCH_HISTORY, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_RESEARCH_PROJECTS);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_RESEARCH_PROJECTS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_RESEARCH_DIGSITES);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_RESEARCH_DIGSITES, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PET_CURRENT_PET);
    stmt->setUInt32(0, lowGuid);
    res &= SetPreparedQuery(CHAR_LOGIN_QUERY_LOAD_CURRENT_PET, stmt);

    return res;
}

bool AccountLoginQueryHolder::Initialize()
{
    SetSize(MAX_ACCOUNT_LOGIN_QUERY);

    bool res = true;

    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_SPELL);
    stmt->setUInt32(0, GetAccountId());
    res &= SetPreparedQuery(ACCOUNT_LOGIN_QUERY_LOAD_SPELLS, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_ACHIEVEMENTS);
    stmt->setUInt32(0, GetAccountId());
    res &= SetPreparedQuery(ACCOUNT_LOGIN_QUERY_LOAD_ACHIEVEMENTS, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_CRITERIA_PROGRESS);
    stmt->setUInt32(0, GetAccountId());
    res &= SetPreparedQuery(ACCOUNT_LOGIN_QUERY_LOAD_CRITERIA_PROGRESS, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_BATTLE_PETS);
    stmt->setUInt32(0, GetAccountId());
    res &= SetPreparedQuery(ACCOUNT_LOGIN_QUERY_LOAD_BATTLE_PETS, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_BATTLE_PET_SLOTS);
    stmt->setUInt32(0, GetAccountId());
    res &= SetPreparedQuery(ACCOUNT_LOGIN_QUERY_LOAD_BATTLE_PET_SLOTS, stmt);

    return res;
}

bool PetLoginQueryHolder::Initialize()
{
    SetSize(MAX_PET_LOGIN_QUERY);

    bool res = true;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PET_BY_ID);
    stmt->setUInt32(0, m_petId);
    res &= SetPreparedQuery(PET_LOGIN_QUERY_LOAD_ACTUAL_PET_DATA, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_AURA);
    stmt->setUInt32(0, m_petId);
    res &= SetPreparedQuery(PET_LOGIN_QUERY_LOAD_AURAS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_AURA_EFFECT);
    stmt->setUInt32(0, m_petId);
    res &= SetPreparedQuery(PET_LOGIN_QUERY_LOAD_AURA_EFFECTS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SPELL);
    stmt->setUInt32(0, m_petId);
    res &= SetPreparedQuery(PET_LOGIN_QUERY_LOAD_SPELLS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SPELL_COOLDOWN);
    stmt->setUInt32(0, m_petId);
    res &= SetPreparedQuery(PET_LOGIN_QUERY_LOAD_SPELL_COOLDOWNS, stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SPELL_CHARGES);
    stmt->setUInt32(0, m_petId);
    res &= SetPreparedQuery(PET_LOGIN_QUERY_LOAD_SPELL_CHARGES, stmt);

    return res;
}

void WorldSession::HandleCharEnum(PreparedQueryResult result)
{
    uint32 charCount = 0;
    ByteBuffer bitBuffer;
    ByteBuffer dataBuffer;

    if (result)
    {
        _legitCharacters.clear();

        charCount = uint32(result->GetRowCount());
        bitBuffer.reserve(24 * charCount / 8);
        dataBuffer.reserve(charCount * 381);

        bitBuffer.WriteBits(0, 21); // factionChangeRestrictions - raceId / mask loop
        bitBuffer.WriteBits(charCount, 16);

        do
        {
            uint32 guidLow = (*result)[0].GetUInt32();

            TC_LOG_INFO("network", "Loading char guid %u from account %u.", guidLow, GetAccountId());

            ObjectGuid boostingGUID;
            if (_battlePayShop)
                boostingGUID = _battlePayShop->GetBoostingGUID();

            Player::BuildEnumData(result, &dataBuffer, &bitBuffer, boostingGUID);

            ObjectGuid guid = ObjectGuid::Create<HighGuid::Player>(guidLow);

            // Do not allow banned characters to log in
            if (!(*result)[23].GetUInt32())
                _legitCharacters.insert(guid);

            if (!sCharacterCache->HasCharacterCacheEntry(guid)) // This can happen if characters are inserted into the database manually. Core hasn't loaded name data yet.
                sCharacterCache->AddCharacterCacheEntry(guid, GetAccountId(), (*result)[1].GetString(), (*result)[4].GetUInt8(), (*result)[2].GetUInt8(), (*result)[3].GetUInt8(), (*result)[10].GetUInt8());
        }
        while (result->NextRow());

        bitBuffer.WriteBit(1); // Sucess
        bitBuffer.FlushBits();
    }
    else
    {
        bitBuffer.WriteBits(0, 21);
        bitBuffer.WriteBits(0, 16);
        bitBuffer.WriteBit(1); // Success
        bitBuffer.FlushBits();
    }

    WorldPacket data(SMSG_CHAR_ENUM, 7 + bitBuffer.size() + dataBuffer.size());

    data.append(bitBuffer);

    if (charCount)
        data.append(dataBuffer);

    SendPacket(&data);
}

void WorldSession::HandleCharEnumOpcode(WorldPacket & /*recvData*/)
{
    // remove expired bans
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_EXPIRED_BANS);
    CharacterDatabase.Execute(stmt);

    /// get all the data necessary for loading all characters (along with their pets) on the account

    if (sWorld->getBoolConfig(CONFIG_DECLINED_NAMES_USED))
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_ENUM_DECLINED_NAME);
    else
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_ENUM);

    stmt->setUInt32(0, GetAccountId());

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt).WithPreparedCallback(std::bind(&WorldSession::HandleCharEnum, this, std::placeholders::_1)));
}

void WorldSession::HandleCharCreateOpcode(WorldPacket& recvData)
{
    uint8 hairStyle = 0;
    uint8 face = 0;
    uint8 facialHair = 0;
    uint8 hairColor = 0;
    uint8 race_ = 0;
    uint8 class_ = 0;
    uint8 skin = 0;
    uint8 gender = 0;
    uint8 outfitId = 0;

    uint32 charTemplateId = 0;
    uint32 nameLength = 0;

    bool hasCharacterTemplate = false;

    recvData >> outfitId;
    recvData >> hairStyle;
    recvData >> class_;
    recvData >> skin;
    recvData >> face;
    recvData >> race_;
    recvData >> facialHair;
    recvData >> gender;
    recvData >> hairColor;

    nameLength = recvData.ReadBits(6);

    hasCharacterTemplate = recvData.ReadBit();

    std::string name = recvData.ReadString(nameLength);

    if (hasCharacterTemplate)
        recvData >> charTemplateId;

    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_TEAMMASK))
    {
        if (uint32 mask = sWorld->getIntConfig(CONFIG_CHARACTER_CREATING_DISABLED))
        {
            bool disabled = false;

            uint32 team = Player::TeamForRace(race_);
            switch (team)
            {
            case ALLIANCE:
                disabled = mask & (1 << 0);
                break;
            case HORDE:
                disabled = mask & (1 << 1);
                break;
            }

            if (disabled)
            {
                WorldPacket data(SMSG_CHAR_CREATE, 1);
                data << uint8(CHAR_CREATE_DISABLED);
                SendPacket(&data);
                return;
            }
        }
    }

    ChrClassesEntry const* classEntry = sChrClassesStore.LookupEntry(class_);
    if (!classEntry)
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(CHAR_CREATE_FAILED);
        SendPacket(&data);
        TC_LOG_ERROR("network", "Class (%u) not found in DBC while creating new char for account (ID: %u): wrong DBC files or cheater?", class_, GetAccountId());
        return;
    }

    ChrRacesEntry const* raceEntry = sChrRacesStore.LookupEntry(race_);
    if (!raceEntry)
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(CHAR_CREATE_FAILED);
        SendPacket(&data);
        TC_LOG_ERROR("network", "Race (%u) not found in DBC while creating new char for account (ID: %u): wrong DBC files or cheater?", race_, GetAccountId());
        return;
    }

    // prevent character creating Expansion race without Expansion account
    uint8 raceExpansionRequirement = sObjectMgr->GetRaceExpansionRequirement(race_);
    if (raceExpansionRequirement > Expansion())
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(CHAR_CREATE_EXPANSION);
        SendPacket(&data);
        TC_LOG_ERROR("entities.player.cheat", "Expansion %u account:[%d] tried to Create character with expansion %u race (%u)", Expansion(), GetAccountId(), raceExpansionRequirement, race_);
        return;
    }

    // prevent character creating Expansion class without Expansion account
    uint8 classExpansionRequirement = sObjectMgr->GetClassExpansionRequirement(class_);
    if (classExpansionRequirement > Expansion())
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(CHAR_CREATE_EXPANSION_CLASS);
        SendPacket(&data);
        TC_LOG_ERROR("entities.player.cheat", "Expansion %u account:[%d] tried to Create character with expansion %u class (%u)", Expansion(), GetAccountId(), classExpansionRequirement, class_);
        return;
    }

    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_RACEMASK))
    {
        uint32 raceMaskDisabled = sWorld->getIntConfig(CONFIG_CHARACTER_CREATING_DISABLED_RACEMASK);
        if ((1 << (race_ - 1)) & raceMaskDisabled)
        {
            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_DISABLED);
            SendPacket(&data);
            return;
        }
    }

    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_CLASSMASK))
    {
        uint32 classMaskDisabled = sWorld->getIntConfig(CONFIG_CHARACTER_CREATING_DISABLED_CLASSMASK);
        if ((1 << (class_ - 1)) & classMaskDisabled)
        {
            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_DISABLED);
            SendPacket(&data);
            return;
        }
    }

    // prevent character creating with invalid name
    if (!normalizePlayerName(name))
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(CHAR_NAME_NO_NAME);
        SendPacket(&data);
        TC_LOG_ERROR("entities.player.cheat", "Account:[%d] but tried to Create character with empty [name] ", GetAccountId());
        return;
    }

    // check name limitations
    ResponseCodes res = ObjectMgr::CheckPlayerName(name, GetSessionDbcLocale(), true);
    if (res != CHAR_NAME_SUCCESS)
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(res);
        SendPacket(&data);
        return;
    }

    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_RESERVEDNAME) && sObjectMgr->IsReservedName(name))
    {
        WorldPacket data(SMSG_CHAR_CREATE, 1);
        data << uint8(CHAR_NAME_RESERVED);
        SendPacket(&data);
        return;
    }

    if (class_ == CLASS_DEATH_KNIGHT && !HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_HEROIC_CHARACTER))
    {
        // speedup check for heroic class disabled case
        uint32 heroic_free_slots = sWorld->getIntConfig(CONFIG_HEROIC_CHARACTERS_PER_REALM);
        if (heroic_free_slots == 0)
        {
            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_UNIQUE_CLASS_LIMIT);
            SendPacket(&data);
            return;
        }

        // speedup check for heroic class disabled case
        uint32 req_level_for_heroic = sWorld->getIntConfig(CONFIG_CHARACTER_CREATING_MIN_LEVEL_FOR_HEROIC_CHARACTER);
        if (req_level_for_heroic > sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
        {
            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_LEVEL_REQUIREMENT);
            SendPacket(&data);
            return;
        }
    }

    std::shared_ptr<CharacterCreateInfo> createInfo =
        std::make_shared<CharacterCreateInfo>(name, race_, class_, gender, skin, face, hairStyle, hairColor, facialHair, outfitId, charTemplateId, recvData);

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHECK_NAME);
    stmt->setString(0, createInfo->Name);

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
        .WithChainingPreparedCallback([this](QueryCallback& queryCallback, PreparedQueryResult result)
    {
        if (result)
        {
            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_NAME_IN_USE);
            SendPacket(&data);
            return;
        }

        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_SUM_REALM_CHARACTERS);
        stmt->setUInt32(0, GetAccountId());
        queryCallback.SetNextQuery(LoginDatabase.AsyncQuery(stmt));
    })
        .WithChainingPreparedCallback([this](QueryCallback& queryCallback, PreparedQueryResult result)
    {
        uint64 acctCharCount = 0;
        if (result)
        {
            Field* fields = result->Fetch();
            acctCharCount = uint64(fields[0].GetDouble());
        }

        if (acctCharCount >= sWorld->getIntConfig(CONFIG_CHARACTERS_PER_ACCOUNT))
        {
            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_ACCOUNT_LIMIT);
            SendPacket(&data);
            return;
        }

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_SUM_CHARS);
        stmt->setUInt32(0, GetAccountId());
        queryCallback.SetNextQuery(CharacterDatabase.AsyncQuery(stmt));
    })
        .WithChainingPreparedCallback([this, createInfo](QueryCallback& queryCallback, PreparedQueryResult result)
    {
        if (result)
        {
            Field* fields = result->Fetch();
            createInfo->CharCount = uint8(fields[0].GetUInt64()); // SQL's COUNT() returns uint64 but it will always be less than uint8.Max

            if (createInfo->CharCount >= sWorld->getIntConfig(CONFIG_CHARACTERS_PER_REALM))
            {
                WorldPacket data(SMSG_CHAR_CREATE, 1);
                data << uint8(CHAR_CREATE_SERVER_LIMIT);
                SendPacket(&data);
                return;
            }
        }

        bool allowTwoSideAccounts = !sWorld->IsPvPRealm() || HasPermission(rbac::RBAC_PERM_TWO_SIDE_CHARACTER_CREATION);
        uint32 skipCinematics = sWorld->getIntConfig(CONFIG_SKIP_CINEMATICS);

        std::function<void(PreparedQueryResult)> finalizeCharacterCreation = [this, createInfo](PreparedQueryResult result)
        {
            bool haveSameRace = false;
            uint32 heroicReqLevel = sWorld->getIntConfig(CONFIG_CHARACTER_CREATING_MIN_LEVEL_FOR_HEROIC_CHARACTER);
            bool hasHeroicReqLevel = (heroicReqLevel == 0);
            bool allowTwoSideAccounts = !sWorld->IsPvPRealm() || HasPermission(rbac::RBAC_PERM_TWO_SIDE_CHARACTER_CREATION);
            uint32 skipCinematics = sWorld->getIntConfig(CONFIG_SKIP_CINEMATICS);
            bool checkHeroicReqs = createInfo->Class == CLASS_DEATH_KNIGHT && !HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_HEROIC_CHARACTER);

            if (result)
            {
                uint32 team = Player::TeamForRace(createInfo->Race);
                uint32 freeHeroicSlots = sWorld->getIntConfig(CONFIG_HEROIC_CHARACTERS_PER_REALM);

                Field* field = result->Fetch();
                uint8 accRace = field[1].GetUInt8();

                if (checkHeroicReqs)
                {
                    uint8 accClass = field[2].GetUInt8();
                    if (accClass == CLASS_DEATH_KNIGHT)
                    {
                        if (freeHeroicSlots > 0)
                            --freeHeroicSlots;

                        if (freeHeroicSlots == 0)
                        {
                            WorldPacket data(SMSG_CHAR_CREATE, 1);
                            data << uint8(CHAR_CREATE_UNIQUE_CLASS_LIMIT);
                            SendPacket(&data);
                            return;
                        }
                    }

                    if (!hasHeroicReqLevel)
                    {
                        uint8 accLevel = field[0].GetUInt8();
                        if (accLevel >= heroicReqLevel)
                            hasHeroicReqLevel = true;
                    }
                }

                // need to check team only for first character
                /// @todo what to if account already has characters of both races?
                if (!allowTwoSideAccounts)
                {
                    uint32 accTeam = 0;
                    if (accRace > 0)
                        accTeam = Player::TeamForRace(accRace);

                    if (accTeam != team)
                    {
                        WorldPacket data(SMSG_CHAR_CREATE, 1);
                        data << uint8(CHAR_CREATE_PVP_TEAMS_VIOLATION);
                        SendPacket(&data);
                        return;
                    }
                }

                // search same race for cinematic or same class if need
                /// @todo check if cinematic already shown? (already logged in?; cinematic field)
                while ((skipCinematics == 1 && !haveSameRace) || createInfo->Class == CLASS_DEATH_KNIGHT)
                {
                    if (!result->NextRow())
                        break;

                    field = result->Fetch();
                    accRace = field[1].GetUInt8();

                    if (!haveSameRace)
                        haveSameRace = createInfo->Race == accRace;

                    if (checkHeroicReqs)
                    {
                        uint8 acc_class = field[2].GetUInt8();
                        if (acc_class == CLASS_DEATH_KNIGHT)
                        {
                            if (freeHeroicSlots > 0)
                                --freeHeroicSlots;

                            if (freeHeroicSlots == 0)
                            {
                                WorldPacket data(SMSG_CHAR_CREATE, 1);
                                data << uint8(CHAR_CREATE_UNIQUE_CLASS_LIMIT);
                                SendPacket(&data);
                                return;
                            }
                        }

                        if (!hasHeroicReqLevel)
                        {
                            uint8 acc_level = field[0].GetUInt8();
                            if (acc_level >= heroicReqLevel)
                                hasHeroicReqLevel = true;
                        }
                    }
                }
            }

            if (checkHeroicReqs && !hasHeroicReqLevel)
            {
                WorldPacket data(SMSG_CHAR_CREATE, 1);
                data << uint8(CHAR_CREATE_LEVEL_REQUIREMENT);
                SendPacket(&data);
                return;
            }

            if (createInfo->Data.rpos() < createInfo->Data.wpos())
            {
                uint8 unk = 0;
                createInfo->Data >> unk;
                TC_LOG_DEBUG("network", "Character creation %s (account %u) has unhandled tail data: [%u]", createInfo->Name.c_str(), GetAccountId(), unk);
            }

            Player newChar(this);
            newChar.GetMotionMaster()->Initialize();
            if (!newChar.Create(sObjectMgr->GetGenerator<HighGuid::Player>().Generate(), createInfo))
            {
                // Player not create (race/class/etc problem?)
                newChar.CleanupsBeforeDelete();

                WorldPacket data(SMSG_CHAR_CREATE, 1);
                data << uint8(CHAR_CREATE_ERROR);
                SendPacket(&data);
                return;
            }

            if ((haveSameRace && skipCinematics == 1) || skipCinematics == 2)
                newChar.SetCinematic(0);                          // not show intro

            newChar.SetAtLoginFlag(AT_LOGIN_FIRST);               // First login

            // Player created, save it now
            newChar.SaveToDB(true);
            createInfo->CharCount += 1;

            SQLTransaction trans = LoginDatabase.BeginTransaction();

            PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_REALM_CHARACTERS_BY_REALM);
            stmt->setUInt32(0, GetAccountId());
            stmt->setUInt32(1, realmID);
            trans->Append(stmt);

            stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_REALM_CHARACTERS);
            stmt->setUInt32(0, createInfo->CharCount);
            stmt->setUInt32(1, GetAccountId());
            stmt->setUInt32(2, realmID);
            trans->Append(stmt);

            LoginDatabase.CommitTransaction(trans);

            if (createInfo->TemplateId)
            {
                if (HasPermission(rbac::RBAC_PERM_USE_CHARACTER_TEMPLATES))
                {
                    if (CharacterTemplate const* charTemplate = sObjectMgr->GetCharacterTemplate(createInfo->TemplateId))
                    {
                        if (charTemplate->Level != 1)
                        {
                            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_LEVEL);
                            stmt->setUInt8(0, uint8(charTemplate->Level));
                            stmt->setUInt32(1, newChar.GetGUID().GetCounter());
                            CharacterDatabase.Execute(stmt);
                        }
                    }
                }
                else
                    TC_LOG_WARN("cheat", "Account: %u (IP: %s) tried to use a character template without given permission. Possible cheating attempt.", GetAccountId(), GetRemoteAddress().c_str());
            }

            WorldPacket data(SMSG_CHAR_CREATE, 1);
            data << uint8(CHAR_CREATE_SUCCESS);
            SendPacket(&data);

            TC_LOG_INFO("entities.player.character", "Account: %u (IP: %s) Create Character: %s %s", GetAccountId(), GetRemoteAddress().c_str(), createInfo->Name.c_str(), newChar.GetGUID().ToString().c_str());

            sScriptMgr->OnPlayerCreate(&newChar);

            sCharacterCache->AddCharacterCacheEntry(newChar.GetGUID(), GetAccountId(), newChar.GetName(), newChar.GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER), newChar.GetRace(), newChar.GetClass(), newChar.GetLevel());

            newChar.CleanupsBeforeDelete();
        };

        if (allowTwoSideAccounts && !skipCinematics && createInfo->Class != CLASS_DEATH_KNIGHT)
        {
            finalizeCharacterCreation(PreparedQueryResult(nullptr));
            return;
        }

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_CREATE_INFO);
        stmt->setUInt32(0, GetAccountId());
        stmt->setUInt32(1, (skipCinematics == 1 || createInfo->Class == CLASS_DEATH_KNIGHT) ? 10 : 1);
        queryCallback.WithPreparedCallback(std::move(finalizeCharacterCreation)).SetNextQuery(CharacterDatabase.AsyncQuery(stmt));
    }));
}

void WorldSession::HandleCharDeleteOpcode(WorldPacket& recvData)
{
    // Initiating
    uint32 initAccountId = GetAccountId();

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 1, 3, 2, 7, 4, 6, 0, 5);
    recvData.ReadGuidBytes(guid, 7, 1, 6, 0, 3, 4, 2, 5);

    // can't delete loaded character
    if (ObjectAccessor::FindPlayer(guid))
    {
        sScriptMgr->OnPlayerFailedDelete(guid, initAccountId);

        WorldPacket data(SMSG_CHAR_DELETE, 1);
        data << uint8(CHAR_DELETE_FAILED);
        SendPacket(&data);

        return;
    }

    CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(guid);
    if (!characterInfo)
    {
        sScriptMgr->OnPlayerFailedDelete(guid, initAccountId);

        WorldPacket data(SMSG_CHAR_DELETE, 1);
        data << uint8(CHAR_DELETE_FAILED);
        SendPacket(&data);

        return;
    }

    uint32 accountId = characterInfo->AccountId;

    // prevent deleting other players' characters using cheating tools
    if (accountId != initAccountId)
    {
        sScriptMgr->OnPlayerFailedDelete(guid, initAccountId);

        WorldPacket data(SMSG_CHAR_DELETE, 1);
        data << uint8(CHAR_DELETE_FAILED);
        SendPacket(&data);

        return;
    }

    if (_battlePayShop && _battlePayShop->GetBoostingGUID() == guid)
    {
        sScriptMgr->OnPlayerFailedDelete(guid, initAccountId);

        WorldPacket data(SMSG_CHAR_DELETE, 1);
        data << uint8(CHAR_DELETE_FAILED_LOCKED_BY_CHARACTER_UPGRADE);
        SendPacket(&data);

        return;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GUILD_MEMBER_RANK);
    stmt->setUInt32(0, guid.GetCounter());

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
        .WithChainingPreparedCallback([this, guid, accountId, characterInfo](QueryCallback& queryCallback, PreparedQueryResult result)
    {
        if (result)
        {
            Field* field = result->Fetch();

            uint8 rankId = field[0].GetUInt8();
            if (rankId == GR_GUILDMASTER)
            {
                sScriptMgr->OnPlayerFailedDelete(guid, accountId);

                WorldPacket data(SMSG_CHAR_DELETE, 1);
                data << uint8(CHAR_DELETE_FAILED_GUILD_LEADER);
                SendPacket(&data);

                return;
            }
        }

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MAIL_LIST_COUNT);
        stmt->setUInt32(0, guid.GetCounter());
        queryCallback.SetNextQuery(CharacterDatabase.AsyncQuery(stmt));
    })
        .WithChainingPreparedCallback([this, guid, accountId, characterInfo](QueryCallback& queryCallback, PreparedQueryResult result)
    {
        if (result)
        {
            Field* field = result->Fetch();

            uint32 countMail = field[0].GetUInt32();
            if (countMail > 0)
            {
                sScriptMgr->OnPlayerFailedDelete(guid, accountId);

                WorldPacket data(SMSG_CHAR_DELETE, 1);
                data << uint8(CHAR_DELETE_FAILED_HAS_HEIRLOOM_OR_MAIL);
                SendPacket(&data);

                return;
            }
        }

        std::function<void(PreparedQueryResult)> finalizeCharacterDeleting = [this, guid, accountId, characterInfo](PreparedQueryResult result)
        {
            if (result)
            {
                do
                {
                    Field* field = result->Fetch();

                    uint32 itemEntry = field[0].GetUInt32();

                    if (ItemSparseEntry const* itemSparse = sItemSparseStore.LookupEntry(itemEntry))
                        if (itemSparse->Quality == ITEM_QUALITY_HEIRLOOM)
                        {
                            sScriptMgr->OnPlayerFailedDelete(guid, accountId);

                            WorldPacket data(SMSG_CHAR_DELETE, 1);
                            data << uint8(CHAR_DELETE_FAILED_HAS_HEIRLOOM_OR_MAIL);
                            SendPacket(&data);

                            return;
                        }
                } while (result->NextRow());
            }

            std::string name = characterInfo->Name;
            uint8 level = characterInfo->Level;

            std::string IP_str = GetRemoteAddress();

            TC_LOG_INFO("entities.player.character", "Account: %d, IP: %s deleted character: %s, %s, Level: %u", accountId, IP_str.c_str(), name.c_str(), guid.ToString().c_str(), level);

            // To prevent hook failure, place hook before removing reference from DB
            // To prevent race conditioning, but as it also makes sense, we hand the accountId over for successful delete.
            sScriptMgr->OnPlayerDelete(guid, accountId);

            // Shouldn't interfere with character deletion though
            if (sLog->ShouldLog("entities.player.dump", LOG_LEVEL_INFO)) // optimize GetPlayerDump call
            {
                std::string dump;
                if (PlayerDumpWriter().GetDump(guid.GetCounter(), dump))
                    sLog->outCharDump(dump.c_str(), accountId, guid.GetCounter(), name.c_str());
            }

            sGuildFinderMgr->RemoveAllMembershipRequestsFromPlayer(guid);
            sCalendarMgr->RemoveAllPlayerEventsAndInvites(guid);

            Player::DeleteFromDB(guid, accountId);

            WorldPacket data(SMSG_CHAR_DELETE, 1);
            data << uint8(CHAR_DELETE_SUCCESS);
            SendPacket(&data);
        };

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_ITEMS_BY_OWNER);
        stmt->setUInt32(0, guid.GetCounter());
        queryCallback.WithPreparedCallback(std::move(finalizeCharacterDeleting)).SetNextQuery(CharacterDatabase.AsyncQuery(stmt));
    }));
}

void WorldSession::HandlePlayerLoginOpcode(WorldPacket& recvData)
{
    ObjectGuid playerGuid;
    float unk = 0;

    recvData >> unk;

    recvData.ReadGuidMask(playerGuid, 1, 4, 7, 3, 2, 6, 5, 0);
    recvData.ReadGuidBytes(playerGuid, 5, 1, 0, 6, 2, 4, 7, 3);

    if (PlayerLoading())
    {
        TC_LOG_ERROR("network", "Player is trying to login again while still loading character (%s), AccountId = %d", GetAccountId(), playerGuid.ToString().c_str());
        SendCharLoginError(CHAR_LOGIN_FAILED);
        return;
    }

    if (GetPlayer() != NULL)
    {
        TC_LOG_ERROR("network", "Player is trying to login, but there is already player bounded to account (%s), AccountId = %d", GetAccountId(), playerGuid.ToString().c_str());
        SendCharLoginError(CHAR_LOGIN_DUPLICATE_CHARACTER);
        return;
    }

    if (!IsLegitCharacterForAccount(playerGuid))
    {
        TC_LOG_ERROR("network", "Account (%u) can't login with that character (%s).", GetAccountId(), playerGuid.ToString().c_str());
        SendCharLoginError(CHAR_LOGIN_NO_CHARACTER);
        return;
    }

    if (_battlePayShop && _battlePayShop->GetBoostingGUID() == playerGuid)
    {
        TC_LOG_ERROR("network", "Account (%u) is trying to login with character (%s) that is boosting.", GetAccountId(), playerGuid.ToString().c_str());
        SendCharLoginError(CHAR_LOGIN_LOCKED_BY_CHARACTER_UPGRADE);
        return;
    }

    m_playerLoading = true;

    CharLoginQueryHolder* charHolder = new CharLoginQueryHolder(playerGuid, GetAccountId());
    AccountLoginQueryHolder* accountHolder = new AccountLoginQueryHolder(GetAccountId());

    if (!charHolder->Initialize() || !accountHolder->Initialize())
    {
        delete charHolder;
        delete accountHolder;
        m_playerLoading = false;
        SendCharLoginError(CHAR_LOGIN_FAILED);
        return;
    }

    _charLoginCallback = CharacterDatabase.DelayQueryHolder(charHolder);
    _accountLoginCallback = LoginDatabase.DelayQueryHolder(accountHolder);
}

void WorldSession::HandleLoadScreenOpcode(WorldPacket& recvData)
{
    uint32 mapID = 0;
    bool IsLoading = false;

    recvData >> mapID;
    IsLoading = recvData.ReadBit();

    // TODO: Do something with this packet
}

void WorldSession::HandlePlayerLogin(CharLoginQueryHolder* charHolder, AccountLoginQueryHolder* accountHolder)
{
    ObjectGuid playerGuid = charHolder->GetGUID();

    Player* pCurrChar = new Player(this);
    // for send server info and strings (config)
    ChatHandler chH = ChatHandler(pCurrChar->GetSession());

    // "GetAccountId() == db stored account id" checked in LoadFromDB (prevent login not own character using cheating tools)
    if (!pCurrChar->LoadFromDB(playerGuid, charHolder, accountHolder))
    {
        SetPlayer(NULL);
        KickPlayer();                                       // disconnect client, player no set to session and it will not deleted or saved at kick
        delete pCurrChar;                                   // delete it manually
        delete charHolder;                                  // delete all unprocessed queries
        delete accountHolder;
        m_playerLoading = false;
        return;
    }

    pCurrChar->GetMotionMaster()->Initialize();
    pCurrChar->SendDungeonDifficulty();

    WorldPacket data(SMSG_LOGIN_VERIFY_WORLD, 4 + 4 + 4 + 4 + 4);
    data << float(pCurrChar->GetPositionX());
    data << float(pCurrChar->GetOrientation());
    data << float(pCurrChar->GetPositionY());
    data << int32(pCurrChar->GetMapId());
    data << float(pCurrChar->GetPositionZ());
    SendPacket(&data);

    // load player specific part before send times
    LoadAccountData(charHolder->GetPreparedResult(CHAR_LOGIN_QUERY_LOAD_ACCOUNT_DATA), PER_CHARACTER_CACHE_MASK);
    SendAccountDataTimes(PER_CHARACTER_CACHE_MASK);

    pCurrChar->SendFeatureSystemStatus();

    // Send MOTD
    {
        data.Initialize(SMSG_MOTD, 50);                     // new in 2.0.1
        data.WriteBits(0, 4);

        uint32 linecount=0;
        std::string str_motd = sWorld->GetMotd();
        std::string::size_type pos, nextpos;
        ByteBuffer stringBuffer;

        pos = 0;
        while ((nextpos= str_motd.find('@', pos)) != std::string::npos)
        {
            if (nextpos != pos)
            {
                std::string string = str_motd.substr(pos, nextpos-pos);
                data.WriteBits(strlen(string.c_str()), 7);
                stringBuffer.WriteString(string);
                ++linecount;
            }
            pos = nextpos + 1;
        }

        if (pos<str_motd.length())
        {
            std::string string = str_motd.substr(pos);
            data.WriteBits(strlen(string.c_str()), 7);
            stringBuffer.WriteString(string);
            ++linecount;
        }

        data.PutBits(0, linecount, 4);
        data.FlushBits();
        data.append(stringBuffer);

        SendPacket(&data);

        // send server info
        if (sWorld->getIntConfig(CONFIG_ENABLE_SINFO_LOGIN) == 1)
            chH.PSendSysMessage(_FULLVERSION);
    }

    data.Initialize(SMSG_PVP_SEASON, 4 + 4);
    data << uint32(sWorld->getIntConfig(CONFIG_ARENA_SEASON_ID) - sWorld->getBoolConfig(CONFIG_ARENA_SEASON_IN_PROGRESS));      // Old season
    data << uint32(sWorld->getBoolConfig(CONFIG_ARENA_SEASON_IN_PROGRESS) ? sWorld->getIntConfig(CONFIG_ARENA_SEASON_ID) : 0);  // Current season
    SendPacket(&data);

    //QueryResult* result = CharacterDatabase.PQuery("SELECT guildid, rank FROM guild_member WHERE guid = '%u'", pCurrChar->GetGUID().GetCounter());
    if (PreparedQueryResult resultGuild = charHolder->GetPreparedResult(CHAR_LOGIN_QUERY_LOAD_GUILD))
    {
        Field* fields = resultGuild->Fetch();
        pCurrChar->SetInGuild(fields[0].GetUInt32());
        pCurrChar->SetRank(fields[1].GetUInt8());
        if (Guild* guild = sGuildMgr->GetGuildById(pCurrChar->GetGuildId()))
            pCurrChar->SetGuildLevel(guild->GetLevel());
    }
    else if (pCurrChar->GetGuildId())                        // clear guild related fields in case wrong data about non existed membership
    {
        pCurrChar->SetInGuild(0);
        pCurrChar->SetRank(0);
        pCurrChar->SetGuildLevel(0);
    }

    //SendTimezoneInformation();

    HotfixData const* hotfixes = sDB2Manager->GetHotfixData();

    data.Initialize(SMSG_HOTFIX_INFO, 4 + hotfixes->size() * (4 + 4 + 4));
    data.WriteBits(hotfixes->size(), 20);
    data.FlushBits();

    for (HotfixNotify const& hotfix : *hotfixes)
    {
        data << uint32(hotfix.Timestamp);
        data << uint32(hotfix.Entry);
        data << uint32(hotfix.TableHash);
    }

    SendPacket(&data);

    pCurrChar->SendInitialPacketsBeforeAddToMap();

    //Show cinematic at the first time that player login
    if (pCurrChar->GetCinematic() < 0)
    {
        if (ChrClassesEntry const* cEntry = sChrClassesStore.LookupEntry(pCurrChar->GetClass()))
        {
            if (cEntry->CinematicSequenceID)
                pCurrChar->SendCinematicStart(cEntry->CinematicSequenceID);
            else if (ChrRacesEntry const* rEntry = sChrRacesStore.LookupEntry(pCurrChar->GetRace()))
                pCurrChar->SendCinematicStart(rEntry->CinematicSequenceID);

            // send new char string if not empty
            if (!sWorld->GetNewCharString().empty())
                chH.PSendSysMessage("%s", sWorld->GetNewCharString().c_str());
        }
    }

    if (!pCurrChar->GetMap()->AddPlayerToMap(pCurrChar))
    {
        AreaTriggerStruct const* at = sObjectMgr->GetGoBackTrigger(pCurrChar->GetMapId());
        if (at)
            pCurrChar->TeleportTo(at->target_mapId, at->target_X, at->target_Y, at->target_Z, pCurrChar->GetOrientation());
        else
            pCurrChar->TeleportTo(pCurrChar->m_homebindMapId, pCurrChar->m_homebindX, pCurrChar->m_homebindY, pCurrChar->m_homebindZ, pCurrChar->GetOrientation());
    }

    ObjectAccessor::AddObject(pCurrChar);
    //TC_LOG_DEBUG("Player %s added to Map.", pCurrChar->GetName().c_str());

    if (pCurrChar->GetGuildId() != 0)
    {
        if (Guild* guild = sGuildMgr->GetGuildById(pCurrChar->GetGuildId()))
            guild->SendLoginInfo(this);
        else
        {
            // remove wrong guild data
            TC_LOG_ERROR("general", "Player %s (GUID: %u) marked as member of not existing guild (id: %u), removing guild membership for player.", pCurrChar->GetName().c_str(), pCurrChar->GetGUID().GetCounter(), pCurrChar->GetGuildId());
            pCurrChar->SetInGuild(0);
        }
    }

    // check if player is on transport and not added earlier to transport
    // e.g. in case of loading transport on grid load
    if (pCurrChar->GetTransportSpawnID() && !pCurrChar->GetTransport())
    {
        Transport* transport = nullptr;

        auto bounds = pCurrChar->GetMap()->GetGameObjectBySpawnIdStore().equal_range(pCurrChar->GetTransportSpawnID());
        if (bounds.first != bounds.second)
            transport = bounds.first->second->ToTransport();

        if (transport)
            transport->AddPassenger(pCurrChar);
        else
        {
            pCurrChar->SetTransport(nullptr);
            pCurrChar->ResetTransport();
            pCurrChar->SetTransportSpawnID(0);
        }
    }

    pCurrChar->SendInitialPacketsAfterAddToMap();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_ONLINE);
    stmt->setUInt32(0, pCurrChar->GetGUID().GetCounter());
    CharacterDatabase.Execute(stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_ACCOUNT_ONLINE);
    stmt->setUInt32(0, GetAccountId());
    LoginDatabase.Execute(stmt);

    pCurrChar->SetInGameTime(GameTime::GetGameTimeMS());

    // announce group about member online (must be after add to player list to receive announce to self)
    if (Group* group = pCurrChar->GetGroup())
    {
        group->SendUpdate();
        group->ResetMaxEnchantingLevel();
    }

    // friend status
    sSocialMgr->SendFriendStatus(pCurrChar, FRIEND_ONLINE, pCurrChar->GetGUID(), true);

    // Place character in world (and load zone) before some object loading
    pCurrChar->LoadCorpse(charHolder->GetPreparedResult(CHAR_LOGIN_QUERY_LOAD_CORPSE_LOCATION));

    // setting Ghost+speed if dead
    if (pCurrChar->m_deathState != ALIVE)
    {
        // not blizz like, we must correctly save and load player instead...
        if (pCurrChar->GetRace() == RACE_NIGHTELF)
            pCurrChar->CastSpell(pCurrChar, 20584, true, nullptr);// auras SPELL_AURA_INCREASE_SPEED(+speed in wisp form), SPELL_AURA_INCREASE_SWIM_SPEED(+swim speed in wisp form), SPELL_AURA_TRANSFORM (to wisp form)
        pCurrChar->CastSpell(pCurrChar, 8326, true, nullptr);     // auras SPELL_AURA_GHOST, SPELL_AURA_INCREASE_SPEED(why?), SPELL_AURA_INCREASE_SWIM_SPEED(why?)

        pCurrChar->SetWaterWalking(true);
    }

    pCurrChar->ContinueTaxiFlight();

    // reset for all pets before pet loading
    if (pCurrChar->HasAtLoginFlag(AT_LOGIN_RESET_PET_TALENTS))
    {
        // Delete all of the player's pet spells
        PreparedStatement* stmtSpells = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ALL_PET_SPELLS_BY_OWNER);
        stmtSpells->setUInt32(0, pCurrChar->GetGUID().GetCounter());
        CharacterDatabase.Execute(stmtSpells);

        // Then reset all of the player's pet specualizations
        PreparedStatement* stmtSpec = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ALL_PET_SPELLS_BY_OWNER);
        stmtSpec->setUInt32(0, pCurrChar->GetGUID().GetCounter());
        CharacterDatabase.Execute(stmtSpec);
    }

    // Load pet if any (if player not alive and in taxi flight or another then pet will remember as temporary unsummoned)
    LoadPet(charHolder->GetPreparedResult(CHAR_LOGIN_QUERY_LOAD_CURRENT_PET));

    // Set FFA PvP for non GM in non-rest mode
    if (sWorld->IsFFAPvPRealm() && !pCurrChar->IsGameMaster() && !pCurrChar->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_RESTING))
        pCurrChar->SetByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_PVP_FLAG, UNIT_BYTE2_FLAG_FFA_PVP);

    if (pCurrChar->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_CONTESTED_PVP))
        pCurrChar->SetContestedPvP();

    // Apply at_login requests
    if (pCurrChar->HasAtLoginFlag(AT_LOGIN_RESET_SPELLS))
    {
        pCurrChar->ResetSpells();
        SendNotification(LANG_RESET_SPELLS);
    }

    if (pCurrChar->HasAtLoginFlag(AT_LOGIN_RESET_TALENTS))
    {
        pCurrChar->ResetTalents(true);
        pCurrChar->ResetTalentSpecialization();
        pCurrChar->SendTalentsInfoData(); // original talents send already in to SendInitialPacketsBeforeAddToMap, resend reset state
        SendNotification(LANG_RESET_TALENTS);
    }

    bool firstLogin = pCurrChar->HasAtLoginFlag(AT_LOGIN_FIRST);
    if (firstLogin)
    {
        pCurrChar->RemoveAtLoginFlag(AT_LOGIN_FIRST);

        PlayerInfo const* info = sObjectMgr->GetPlayerInfo(pCurrChar->GetRace(), pCurrChar->GetClass());
        for (uint32 spellId : info->castSpells)
            pCurrChar->CastSpell(pCurrChar, spellId, true);

        // Start with all map areas explored if enabled
        if (sWorld->getBoolConfig(CONFIG_START_ALL_EXPLORED))
            for (uint8 i = 0; i < PLAYER_EXPLORED_ZONES_SIZE; ++i)
                pCurrChar->SetFlag(PLAYER_EXPLORED_ZONES + i, 0xFFFFFFFF);

        // Max relevant reputations if "StartAllReputation" is enabled
        if (sWorld->getBoolConfig(CONFIG_START_ALL_REP))
        {
            ReputationMgr& repMgr = pCurrChar->GetReputationMgr();
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 942), 42999, false); // Cenarion Expedition
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 935), 42999, false); // The Sha'tar
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 936), 42999, false); // Shattrath City
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1011), 42999, false); // Lower City
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 970), 42999, false); // Sporeggar
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 967), 42999, false); // The Violet Eye
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 989), 42999, false); // Keepers of Time
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 932), 42999, false); // The Aldor
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 934), 42999, false); // The Scryers
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1038), 42999, false); // Ogri'la
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1077), 42999, false); // Shattered Sun Offensive
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1106), 42999, false); // Argent Crusade
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1104), 42999, false); // Frenzyheart Tribe
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1090), 42999, false); // Kirin Tor
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1098), 42999, false); // Knights of the Ebon Blade
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1156), 42999, false); // The Ashen Verdict
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1073), 42999, false); // The Kalu'ak
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1105), 42999, false); // The Oracles
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1119), 42999, false); // The Sons of Hodir
            repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1091), 42999, false); // The Wyrmrest Accord

            // Factions depending on team, like cities and some more stuff
            switch (pCurrChar->GetTeam())
            {
                case ALLIANCE:
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  72), 42999, false); // Stormwind
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  47), 42999, false); // Ironforge
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  69), 42999, false); // Darnassus
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 930), 42999, false); // Exodar
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 730), 42999, false); // Stormpike Guard
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 978), 42999, false); // Kurenai
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  54), 42999, false); // Gnomeregan Exiles
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 946), 42999, false); // Honor Hold
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1037), 42999, false); // Alliance Vanguard
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1068), 42999, false); // Explorers' League
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1126), 42999, false); // The Frostborn
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1094), 42999, false); // The Silver Covenant
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1050), 42999, false); // Valiance Expedition
                    break;
                case HORDE:
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  76), 42999, false); // Orgrimmar
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  68), 42999, false); // Undercity
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(  81), 42999, false); // Thunder Bluff
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 911), 42999, false); // Silvermoon City
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 729), 42999, false); // Frostwolf Clan
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 941), 42999, false); // The Mag'har
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 530), 42999, false); // Darkspear Trolls
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry( 947), 42999, false); // Thrallmar
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1052), 42999, false); // Horde Expedition
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1067), 42999, false); // The Hand of Vengeance
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1124), 42999, false); // The Sunreavers
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1064), 42999, false); // The Taunka
                    repMgr.SetOneFactionReputation(sFactionStore.LookupEntry(1085), 42999, false); // Warsong Offensive
                    break;
                default:
                    break;
            }
            repMgr.SendState(nullptr);
        }
    }

    // Update character speed on login
    pCurrChar->UpdateSpeed(MOVE_RUN);
    pCurrChar->UpdateSpeed(MOVE_FLIGHT);
    pCurrChar->UpdateSpeed(MOVE_SWIM);

    // show time before shutdown if shutdown planned.
    if (sWorld->IsShuttingDown())
        sWorld->ShutdownMsg(true, pCurrChar);

    if (sWorld->getBoolConfig(CONFIG_ALL_TAXI_PATHS))
        pCurrChar->SetTaxiCheater(true);

    if (pCurrChar->IsGameMaster())
        SendNotification(LANG_GM_ON);

    std::string IP_str = GetRemoteAddress();
    TC_LOG_INFO("entities.player.character", "Account: %d (IP: %s) Login Character:[%s] (GUID: %u) Level: %d",
        GetAccountId(), IP_str.c_str(), pCurrChar->GetName().c_str(), pCurrChar->GetGUID().GetCounter(), pCurrChar->GetLevel());

    if (!pCurrChar->IsStandState() && !pCurrChar->HasUnitState(UNIT_STATE_STUNNED))
        pCurrChar->SetStandState(UNIT_STAND_STATE_STAND);

    m_playerLoading = false;

    // Handle Login-Achievements (should be handled after loading)
    _player->UpdateCriteria(CRITERIA_TYPE_ON_LOGIN);

    sScriptMgr->OnPlayerLogin(pCurrChar, firstLogin);

    delete charHolder;
    delete accountHolder;
}

void WorldSession::HandleFactionSetAtWar(WorldPacket& recvData)
{
    uint8 repId = 0;
    recvData >> repId;

    GetPlayer()->GetReputationMgr().SetAtWar(repId, true);
}

void WorldSession::HandleFactionUnSetAtWar(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_FACTION_UNSET_AT_WAR");

    uint8 repId = 0;
    recvData >> repId;

    GetPlayer()->GetReputationMgr().SetAtWar(repId, false);
}

void WorldSession::HandleTutorialFlag(WorldPacket& recvData)
{
    uint32 data = 0;
    recvData >> data;

    uint8 index = uint8(data / 32);
    if (index >= MAX_ACCOUNT_TUTORIAL_VALUES)
        return;

    uint32 value = (data % 32);

    uint32 flag = GetTutorialInt(index);
    flag |= (1 << value);
    SetTutorialInt(index, flag);
}

void WorldSession::HandleTutorialClear(WorldPacket& /*recvData*/)
{
    for (uint8 i = 0; i < MAX_ACCOUNT_TUTORIAL_VALUES; ++i)
        SetTutorialInt(i, 0xFFFFFFFF);
}

void WorldSession::HandleTutorialReset(WorldPacket& /*recvData*/)
{
    for (uint8 i = 0; i < MAX_ACCOUNT_TUTORIAL_VALUES; ++i)
        SetTutorialInt(i, 0x00000000);
}

void WorldSession::HandleSetWatchedFactionOpcode(WorldPacket& recvData)
{
    uint32 fact = 0;
    recvData >> fact;

    GetPlayer()->SetUInt32Value(PLAYER_WATCHED_FACTION_INDEX, fact);
}

void WorldSession::HandleSetFactionInactiveOpcode(WorldPacket& recvData)
{
    uint32 replistid = 0;
    bool inactive = false;

    recvData >> replistid;

    inactive = recvData.ReadBit();

    _player->GetReputationMgr().SetInactive(replistid, inactive);
}

void WorldSession::HandleRequestForcedReactionsOpcode(WorldPacket& /*recvData*/)
{
    _player->GetReputationMgr().SendForceReactions();
}

void WorldSession::HandleShowingHelmOpcode(WorldPacket& recvData)
{
    bool ShowHelm = false;
    ShowHelm = recvData.ReadBit();

    if (!ShowHelm)
        _player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_HIDE_HELM);
    else
        _player->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_HIDE_HELM);
}

void WorldSession::HandleShowingCloakOpcode(WorldPacket& recvData)
{
    bool ShowCloak = false;
    ShowCloak = recvData.ReadBit();

    if (!ShowCloak)
        _player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_HIDE_CLOAK);
    else
        _player->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_HIDE_CLOAK);
}

void WorldSession::HandleCharRenameOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;
    std::string newName;
    uint8 unk = 0;
    uint8 length = 0;

    recvData.ReadGuidMask(guid, 6, 3, 0);

    length = recvData.ReadBits(6);

    recvData.ReadGuidMask(guid, 1, 5, 7, 2, 4);

    unk = recvData.ReadBits(2);

    recvData.ReadGuidBytes(guid, 1, 6, 5);

    newName = recvData.ReadString(length);

    recvData.ReadGuidBytes(guid, 2, 4, 3, 7, 0);

    // prevent character rename to invalid name
    if (!normalizePlayerName(newName))
    {
        WorldPacket data(SMSG_CHAR_RENAME, 1 + 1);
        data << uint8(CHAR_NAME_NO_NAME);
        data.WriteBit(!false);
        data.WriteBit(false);
        data.FlushBits();
        SendPacket(&data);
        return;
    }

    ResponseCodes result = ObjectMgr::CheckPlayerName(newName, GetSessionDbcLocale(), true);
    if (result != CHAR_NAME_SUCCESS)
    {
        WorldPacket data(SMSG_CHAR_RENAME, 1 + 1 + newName.size());
        data << uint8(result);
        data.WriteBit(!true);
        data.WriteBits(newName.size(), 6);
        data.WriteBit(false);
        data.WriteString(newName);
        SendPacket(&data);
        return;
    }

    // check name limitations
    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_RESERVEDNAME) && sObjectMgr->IsReservedName(newName))
    {
        WorldPacket data(SMSG_CHAR_RENAME, 1 + 1 + newName.size());
        data << uint8(CHAR_NAME_RESERVED);
        data.WriteBit(!true);
        data.WriteBits(newName.size(), 6);
        data.WriteBit(false);
        data.WriteString(newName);
        SendPacket(&data);
        return;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_FREE_NAME);

    stmt->setUInt32(0, guid.GetCounter());
    stmt->setUInt32(1, GetAccountId());
    stmt->setUInt16(2, AT_LOGIN_RENAME);
    stmt->setUInt16(3, AT_LOGIN_RENAME);
    stmt->setString(4, newName);

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
        .WithPreparedCallback(std::bind(&WorldSession::HandleChangePlayerNameOpcodeCallBack, this, newName, std::placeholders::_1)));
}

void WorldSession::HandleChangePlayerNameOpcodeCallBack(std::string const& newName, PreparedQueryResult result)
{
    if (!result)
    {
        WorldPacket data(SMSG_CHAR_RENAME, 1 + 1);
        data << uint8(CHAR_CREATE_ERROR);
        data.WriteBit(!false);
        data.WriteBit(false);
        data.FlushBits();
        SendPacket(&data);
        return;
    }

    Field* fields = result->Fetch();

    uint32 guidLow      = fields[0].GetUInt32();
    std::string oldName = fields[1].GetString();

    ObjectGuid guid = ObjectGuid::Create<HighGuid::Player>(guidLow);

    // Update name and at_login flag in the db
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_NAME);
    stmt->setString(0, newName);
    stmt->setUInt16(1, AT_LOGIN_RENAME);
    stmt->setUInt32(2, guidLow);
    trans->Append(stmt);

    // Removed declined name from db
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_DECLINED_NAME);
    stmt->setUInt32(0, guidLow);
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);

    TC_LOG_INFO("entities.player.character", "Account: %d (IP: %s) Character:[%s] (%s) Changed name to: %s", GetAccountId(), GetRemoteAddress().c_str(), oldName.c_str(), guid.ToString().c_str(), newName.c_str());

    WorldPacket data(SMSG_CHAR_RENAME, 1 + 8 + 1 + 1 + newName.size());

    data << uint8(RESPONSE_SUCCESS);

    data.WriteBit(!true);

    data.WriteBits(newName.length(), 6);

    data.WriteBit(true);

    data.WriteGuidMask(guid, 6, 3, 4, 2, 0, 1, 7, 5);

    data.WriteGuidBytes(guid, 5, 0, 4, 2, 1, 3, 6, 7);

    data.WriteString(newName);

    SendPacket(&data);

    sCharacterCache->UpdateCharacterData(guid, newName);
}

void WorldSession::HandleSetPlayerDeclinedNames(WorldPacket& recvData)
{
    ObjectGuid guid;
    std::array<uint32, MAX_DECLINED_NAME_CASES> nameLength;

    recvData.ReadGuidMask(guid, 0, 2, 1, 7, 5, 6, 4, 3);

    for (uint32 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
        nameLength[i] = recvData.ReadBits(7);

    DeclinedNames declinedNames;

    for (uint32 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
        declinedNames[i] = recvData.ReadString(nameLength[i]);

    recvData.ReadGuidBytes(guid, 0, 7, 3, 6, 4, 2, 1, 5);

    for (uint32 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
    {
        if (!normalizePlayerName(declinedNames[i]))
        {
            WorldPacket data(SMSG_SET_PLAYER_DECLINED_NAMES_RESULT, 1 + 1 + 4);

            data.WriteBit(!false);

            data.WriteBits(0, 8);

            data.FlushBits();

            data << uint32(DECLINED_NAMES_RESULT_ERROR);

            SendPacket(&data);

            return;
        }
    }

    // not accept declined names for unsupported languages
    std::string name;
    if (!sCharacterCache->GetCharacterNameByGuid(guid, name))
    {
        WorldPacket data(SMSG_SET_PLAYER_DECLINED_NAMES_RESULT, 1 + 1 + 4);

        data.WriteBit(!false);

        data.WriteBits(0, 8);

        data.FlushBits();

        data << uint32(DECLINED_NAMES_RESULT_ERROR);

        SendPacket(&data);

        return;
    }

    std::wstring wname;
    if (!Utf8toWStr(name, wname))
    {
        WorldPacket data(SMSG_SET_PLAYER_DECLINED_NAMES_RESULT, 1 + 1 + 4);

        data.WriteBit(!false);

        data.WriteBits(0, 8);

        data.FlushBits();

        data << uint32(DECLINED_NAMES_RESULT_ERROR);

        SendPacket(&data);

        return;
    }

    if (!isCyrillicCharacter(wname[0]))                      // name already stored as only single alphabet using
    {
        WorldPacket data(SMSG_SET_PLAYER_DECLINED_NAMES_RESULT, 1 + 1 + 4);

        data.WriteBit(!false);

        data.WriteBits(0, 8);

        data.FlushBits();

        data << uint32(DECLINED_NAMES_RESULT_ERROR);

        SendPacket(&data);
        return;
    }

    if (!ObjectMgr::CheckDeclinedNames(wname, declinedNames))
    {
        WorldPacket data(SMSG_SET_PLAYER_DECLINED_NAMES_RESULT, 1 + 1 + 4);

        data.WriteBit(!false);

        data.WriteBits(0, 8);

        data.FlushBits();

        data << uint32(DECLINED_NAMES_RESULT_ERROR);

        SendPacket(&data);

        return;
    }

    for (int i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
        CharacterDatabase.EscapeString(declinedNames[i]);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_DECLINED_NAME);
    stmt->setUInt32(0, guid.GetCounter());
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_DECLINED_NAME);
    stmt->setUInt32(0, guid.GetCounter());

    for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; i++)
        stmt->setString(i + 1, declinedNames[i]);

    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);

    WorldPacket data(SMSG_SET_PLAYER_DECLINED_NAMES_RESULT, 1 + 8 + 1 + 4);

    data.WriteBit(!true);

    data.WriteGuidMask(guid, 2, 0, 3, 1, 4, 6, 5, 7);

    data.FlushBits();

    data.WriteGuidBytes(guid, 2, 7, 1, 0, 4, 3, 6, 5);

    data << uint32(DECLINED_NAMES_RESULT_SUCCESS);

    SendPacket(&data);
}

void WorldSession::HandleAlterAppearance(WorldPacket& recvData)
{
    uint32 HairStyle = 0;
    uint32 HairColor = 0;
    uint32 FacialHair = 0;
    uint32 SkinColor = 0;
    uint32 Face = 0;

    recvData >> SkinColor;
    recvData >> HairColor;
    recvData >> HairStyle;
    recvData >> FacialHair;
    recvData >> Face;

    BarberShopStyleEntry const* bs_hair = sBarberShopStyleStore.LookupEntry(HairStyle);
    if (!bs_hair || bs_hair->Type != 0 || bs_hair->Race != _player->GetRace() || bs_hair->Gender != _player->GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER))
        return;

    BarberShopStyleEntry const* bs_facialHair = sBarberShopStyleStore.LookupEntry(FacialHair);
    if (!bs_facialHair || bs_facialHair->Type != 2 || bs_facialHair->Race != _player->GetRace() || bs_facialHair->Gender != _player->GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER))
        return;

    BarberShopStyleEntry const* bs_skinColor = sBarberShopStyleStore.LookupEntry(SkinColor);
    if (bs_skinColor && (bs_skinColor->Type != 3 || bs_skinColor->Race != _player->GetRace() || bs_skinColor->Gender != _player->GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER)))
        return;

    BarberShopStyleEntry const* bs_face = sBarberShopStyleStore.LookupEntry(Face);
    if (bs_face && (bs_face->Type != 4 || bs_face->Race != _player->GetRace() || bs_face->Gender != _player->GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER)))
        return;

    if (!Player::ValidateAppearance(_player->GetRace(), _player->GetClass(), _player->GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER),
        bs_hair->Data,
        HairColor,
        bs_face ? bs_face->Data : _player->GetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_FACE_ID),
        bs_facialHair->Data,
        bs_skinColor ? bs_skinColor->Data : _player->GetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_SKIN_ID)))
        return;

    GameObject* go = _player->FindNearestGameObjectOfType(GAMEOBJECT_TYPE_BARBER_CHAIR, 5.0f);
    if (!go)
    {
        SendBarberShopResult(BARBER_SHOP_NOT_SITTING);
        return;
    }

    if (_player->GetStandState() != UNIT_STAND_STATE_SIT_LOW_CHAIR + go->GetGOInfo()->barberChair.chairheight)
    {
        SendBarberShopResult(BARBER_SHOP_NOT_SITTING);
        return;
    }

    uint32 cost = _player->GetBarberShopCost(bs_hair, HairColor, bs_facialHair, bs_skinColor);
    if (!_player->HasEnoughMoney((uint64)cost))
    {
        SendBarberShopResult(BARBER_SHOP_NOT_ENOUGH_MONEY);
        return;
    }

    SendBarberShopResult(BARBER_SHOP_SUCCESS);

    _player->ModifyMoney(-int64(cost));                     // it isn't free
    _player->UpdateCriteria(CRITERIA_TYPE_GOLD_SPENT_AT_BARBER, cost);

    _player->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_STYLE_ID, uint8(bs_hair->ID));
    _player->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_HAIR_COLOR_ID, uint8(HairColor));
    _player->SetByteValue(PLAYER_BYTES_2, PLAYER_BYTES_2_OFFSET_FACIAL_STYLE, uint8(bs_facialHair->ID));
    if (bs_skinColor)
        _player->SetByteValue(PLAYER_BYTES, PLAYER_BYTES_OFFSET_SKIN_ID, uint8(bs_skinColor->ID));

    _player->UpdateCriteria(CRITERIA_TYPE_VISIT_BARBER_SHOP, 1);

    _player->SetStandState(0);                              // stand up
}

void WorldSession::SendBarberShopResult(BarberShopResult result)
{
    WorldPacket data(SMSG_BARBER_SHOP_RESULT, 4);
    data << uint32(result);
    SendPacket(&data);
}

void WorldSession::HandleCharCustomize(WorldPacket& recvData)
{
    ObjectGuid guid;
    std::string newName;
    uint8 Length = 0;
    uint8 Gender = 0;
    uint8 Skin = 0;
    uint8 Face = 0;
    uint8 HairStyle = 0;
    uint8 HairColor = 0;
    uint8 FacialHair = 0;

    recvData >> HairStyle;
    recvData >> Gender;
    recvData >> Skin;
    recvData >> FacialHair;
    recvData >> Face;
    recvData >> HairColor;

    recvData.ReadGuidMask(guid, 2, 6, 1, 0, 7, 5);

    Length = recvData.ReadBits(6);

    recvData.ReadGuidMask(guid, 4, 3);

    recvData.ReadBits(2); // 00

    recvData.ReadGuidBytes(guid, 4);

    newName = recvData.ReadString(Length);

    recvData.ReadGuidBytes(guid, 0, 2, 6, 5, 3, 1, 7);

    if (!IsLegitCharacterForAccount(guid))
    {
        TC_LOG_ERROR("entities.player.cheat", "Account %u, IP: %s tried to customise %s, but it does not belong to their account!",
            GetAccountId(), GetRemoteAddress().c_str(), guid.ToString().c_str());
        KickPlayer();
        return;
    }

    std::shared_ptr<CharCustomizeInfo> customizeInfo = std::make_shared<CharCustomizeInfo>(guid, HairStyle, Face, Gender, HairColor, FacialHair, Skin, newName);

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHARACTER_AT_LOGIN);
    stmt->setUInt32(0, guid.GetCounter());

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
        .WithPreparedCallback(std::bind(&WorldSession::HandleCharCustomizeCallback, this, customizeInfo, std::placeholders::_1)));
}

void WorldSession::HandleCharCustomizeCallback(std::shared_ptr<CharCustomizeInfo> customizeInfo, PreparedQueryResult result)
{
    uint8 response = RESPONSE_SUCCESS;

    if (!result)
        response = uint8(CHAR_CREATE_ERROR);

    Field* fields = result->Fetch();

    uint8 plrRace = fields[0].GetUInt8();
    uint8 plrClass = fields[1].GetUInt8();
    uint8 plrGender = fields[2].GetUInt8();
    uint16 atLoginFlags = fields[3].GetUInt16();

    std::string oldname = customizeInfo->CharName;
    if (result)
        oldname = result->Fetch()[0].GetString();

    if (!response && !(atLoginFlags & AT_LOGIN_CUSTOMIZE))
        response = uint8(CHAR_CREATE_ERROR);

    // prevent character rename to invalid name
    if (!response &&  !normalizePlayerName(customizeInfo->CharName))
        response = uint8(CHAR_NAME_NO_NAME);

    // prevent character rename
    if (sWorld->getBoolConfig(CONFIG_PREVENT_RENAME_CUSTOMIZATION) && (customizeInfo->CharName != oldname))
        response = uint8(CHAR_NAME_FAILURE);

    ResponseCodes res = ObjectMgr::CheckPlayerName(customizeInfo->CharName, GetSessionDbcLocale(), true);
    if (!response &&  res != CHAR_NAME_SUCCESS)
        response = uint8(res);

    // check name limitations
    if (!response &&  !HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_RESERVEDNAME) && sObjectMgr->IsReservedName(customizeInfo->CharName))
        response = uint8(CHAR_NAME_RESERVED);

    // character with this name already exist
    if (ObjectGuid newGuid = sCharacterCache->GetCharacterGuidByName(customizeInfo->CharName))
        if (!response && newGuid != customizeInfo->CharGUID)
            response = uint8(CHAR_CREATE_NAME_IN_USE);

    if (!Player::ValidateAppearance(plrRace, plrClass, plrGender, customizeInfo->HairStyleID, customizeInfo->HairColorID, customizeInfo->FaceID, customizeInfo->FacialHairStyleID, customizeInfo->SkinID, true))
        response = uint8(CHAR_CREATE_ERROR);

    if (response == RESPONSE_SUCCESS)
    {
        Player::Customize(customizeInfo->CharGUID, customizeInfo->SexID, customizeInfo->SkinID, customizeInfo->FaceID,
            customizeInfo->HairStyleID, customizeInfo->HairColorID, customizeInfo->FacialHairStyleID);

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_NAME_AT_LOGIN);

        stmt->setString(0, customizeInfo->CharName);
        stmt->setUInt16(1, uint16(AT_LOGIN_CUSTOMIZE));
        stmt->setUInt32(2, customizeInfo->CharGUID.GetCounter());

        CharacterDatabase.Execute(stmt);

        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_DECLINED_NAME);

        stmt->setUInt32(0, customizeInfo->CharGUID.GetCounter());

        CharacterDatabase.Execute(stmt);

        sCharacterCache->UpdateCharacterData(customizeInfo->CharGUID, customizeInfo->CharName, &customizeInfo->SexID);
    }

    WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1 + (response == RESPONSE_SUCCESS ? (1 + 1 + 1 + 1 + 1 + 1 + 1 + customizeInfo->CharName.size()) : 0));

    data.WriteGuidMask(customizeInfo->CharGUID, 0, 7, 3, 2, 6, 5, 1, 4);

    data.WriteGuidBytes(customizeInfo->CharGUID, 1);

    data << uint8(response);

    if (response == RESPONSE_SUCCESS)
    {
        data << uint8(customizeInfo->SexID);
        data << uint8(customizeInfo->SkinID);
        data << uint8(customizeInfo->FaceID);
        data << uint8(customizeInfo->HairStyleID);
        data << uint8(customizeInfo->HairColorID);
        data << uint8(customizeInfo->FacialHairStyleID);
    }

    data.WriteGuidBytes(customizeInfo->CharGUID, 7, 3, 5, 2, 6, 0, 4);

    if (response == RESPONSE_SUCCESS)
    {
        data.WriteBits(customizeInfo->CharName.size(), 6);
        data.WriteString(customizeInfo->CharName);
    }

    SendPacket(&data);
}

void WorldSession::HandleEquipmentSetSave(WorldPacket& recvData)
{
    uint32 index = 0;
    recvData >> index;

    // client set slots amount
    if (index >= MAX_EQUIPMENT_SET_INDEX)
    {
        recvData.rfinish();
        return;
    }

    GuidVector itemGUID(EQUIPMENT_SLOT_END);

    for (uint32 i = 0; i < EQUIPMENT_SLOT_END; ++i)
        recvData.ReadGuidMask(itemGUID[i], 5, 0, 1, 4, 6, 3, 7, 2);

    ObjectGuid setGUID;

    recvData.ReadGuidMask(setGUID, 7, 1, 5, 2, 3, 0);

    uint32 setNameLen = 0;
    setNameLen = recvData.ReadBits(8);

    recvData.ReadGuidMask(setGUID, 6);

    uint32 iconNameLen = 0;
    iconNameLen = recvData.ReadBits(9);

    recvData.ReadGuidMask(setGUID, 4);

    recvData.FlushBits();

    recvData.ReadGuidBytes(setGUID, 4, 0);

    EquipmentSet eqSet;

    for (uint32 i = 0; i < EQUIPMENT_SLOT_END; ++i)
    {
        recvData.ReadGuidBytes(itemGUID[i], 1, 0, 7, 3, 6, 2, 4, 5);

        // equipment manager sends "1" (as raw GUID) for slots set to "ignore" (don't touch slot at equip set)
        if (itemGUID[i].GetRawValue() == 1)
        {
            // ignored slots saved as bit mask because we have no free special values for Items[i]
            eqSet.IgnoreMask |= 1 << i;
            continue;
        }

        Item* item = _player->GetItemByPos(INVENTORY_SLOT_BAG_0, i);

        if (!item && itemGUID[i])                               // cheating check 1
            return;

        if (item && item->GetGUID() != itemGUID[i])             // cheating check 2
            return;

        eqSet.Items[i] = itemGUID[i].GetCounter();
    }

    std::string iconName;
    iconName = recvData.ReadString(iconNameLen);

    recvData.ReadGuidBytes(setGUID, 7, 2);

    std::string setName;
    setName = recvData.ReadString(setNameLen);

    recvData.ReadGuidBytes(setGUID, 6, 1, 5, 3);

    eqSet.Guid      = setGUID;
    eqSet.Name      = setName;
    eqSet.IconName  = iconName;
    eqSet.state     = EQUIPMENT_SET_NEW;

    _player->SetEquipmentSet(index, eqSet);
}

void WorldSession::HandleEquipmentSetDelete(WorldPacket &recvData)
{
    ObjectGuid setGUID;

    recvData.ReadGuidMask(setGUID, 4, 2, 6, 0, 5, 1, 7, 3);
    recvData.ReadGuidBytes(setGUID, 2, 0, 1, 6, 3, 4, 5, 7);

    _player->DeleteEquipmentSet(setGUID);
}

void WorldSession::HandleEquipmentSetUse(WorldPacket& recvData)
{
    GuidVector itemGUID(EQUIPMENT_SLOT_END);

    for (uint32 i = 0; i < EQUIPMENT_SLOT_END; ++i)
    {
        uint8 srcbag = 0;
        uint8 srcslot = 0;
        recvData >> srcslot;
        recvData >> srcbag;

        recvData.ReadGuidMask(itemGUID[i], 3, 1, 7, 4, 5, 6, 0, 2);
    }

    uint8 unkCounter = 0;
    unkCounter = recvData.ReadBits(2);

    for (uint8 i = 0; i < unkCounter; i++)
    {
        recvData.ReadBit();
        recvData.ReadBit();
    }

    for (uint32 i = 0; i < EQUIPMENT_SLOT_END; ++i)
    {
        recvData.ReadGuidBytes(itemGUID[i], 4, 7, 0, 3, 2, 5, 1, 6);

        // check if item slot is set to "ignored" (raw value == 1), must not be unequipped then
        if (itemGUID[i].GetRawValue() == 1)
            continue;

        // Only equip weapons in combat
        if (_player->IsInCombat() && i != EQUIPMENT_SLOT_MAINHAND && i != EQUIPMENT_SLOT_OFFHAND)
            continue;

        Item* item = _player->GetItemByGuid(itemGUID[i]);

        uint16 dstpos = i | (INVENTORY_SLOT_BAG_0 << 8);

        if (!item)
        {
            Item* uItem = _player->GetItemByPos(INVENTORY_SLOT_BAG_0, i);
            if (!uItem)
                continue;

            ItemPosCountVec sDest;
            InventoryResult msg = _player->CanStoreItem(NULL_BAG, NULL_SLOT, sDest, uItem, false);
            if (msg == EQUIP_ERR_OK)
            {
                _player->RemoveItem(INVENTORY_SLOT_BAG_0, i, true);
                _player->StoreItem(sDest, uItem, true);
            }
            else
                _player->SendEquipError(msg, uItem, NULL);

            continue;
        }

        if (item->GetPos() == dstpos)
            continue;

        _player->SwapItem(item->GetPos(), dstpos);
    }

    for (uint8 i = 0; i < unkCounter; i++)
    {
        recvData.read_skip<uint8>();
        recvData.read_skip<uint8>();
    }

    WorldPacket data(SMSG_EQUIPMENT_SET_USE_RESULT, 1);

    // 4 - equipment swap failed - inventory is full
    data << uint8(0);

    SendPacket(&data);
}

void WorldSession::HandleCharFactionOrRaceChange(WorldPacket& recvData)
{
    uint8 newNameLenght = 0;
    uint8 gender = 0;
    uint8 skin = 0;
    uint8 face = 0;
    uint8 hairStyle = 0;
    uint8 hairColor = 0;
    uint8 facialHair = 0;
    uint8 race = 0;

    uint8 changeSkin = 0;
    uint8 changeFace = 0;
    uint8 changeFacialHair = 0;
    uint8 changeHairStyle = 0;
    uint8 changeHairColor = 0;

    bool IsFactionChanged = false;

    std::string newName;

    recvData >> gender;
    recvData >> race;

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 3, 2);

    changeSkin = recvData.ReadBit();
    changeFace = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 6);

    newNameLenght = recvData.ReadBits(6);

    changeFacialHair = recvData.ReadBit();
    changeHairStyle = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 4, 1, 0, 5);

    changeHairColor = recvData.ReadBit();
    IsFactionChanged = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 7);

    recvData.ReadGuidBytes(guid, 2, 1, 4, 5, 0);

    newName = recvData.ReadString(newNameLenght);

    recvData.ReadGuidBytes(guid, 6, 3, 7);

    if (changeHairColor)
        recvData >> hairColor;
    if (changeHairStyle)
        recvData >> hairStyle;
    if (changeSkin)
        recvData >> skin;
    if (changeFace)
        recvData >> face;
    if (changeFacialHair)
        recvData >> facialHair;

    if (!IsLegitCharacterForAccount(guid))
    {
        TC_LOG_ERROR("entities.player.cheat", "Account %u, IP: %s tried to factionchange character %s, but it does not belong to their account!",
            GetAccountId(), GetRemoteAddress().c_str(), guid.ToString().c_str());
        KickPlayer();
        return;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_RACE_OR_FACTION_CHANGE_INFOS);
    stmt->setUInt32(0, guid.GetCounter());

    std::shared_ptr<CharRaceOrFactionChangeInfo> raceOrFactionChangeInfo =
        std::make_shared<CharRaceOrFactionChangeInfo>(guid, race, hairStyle, face, gender, hairColor, facialHair, skin, newName, IsFactionChanged);

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt)
        .WithPreparedCallback(std::bind(&WorldSession::HandleCharRaceOrFactionChangeCallback, this, raceOrFactionChangeInfo, std::placeholders::_1)));
}

void WorldSession::HandleCharRaceOrFactionChangeCallback(std::shared_ptr<CharRaceOrFactionChangeInfo> raceOrFactionChangeInfo, PreparedQueryResult result)
{
    if (!result)
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 1);
        data.WriteBits(0, 8);
        data << uint8(CHAR_CREATE_ERROR);
        SendPacket(&data);
        return;
    }

    // get the players old (at this moment current) race
    CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(raceOrFactionChangeInfo->Guid);
    if (!characterInfo)
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 1);
        data.WriteBits(0, 8);
        data << uint8(CHAR_CREATE_ERROR);
        SendPacket(&data);
        return;
    }

    std::string oldName = characterInfo->Name;
    uint8 oldRace = characterInfo->Race;
    uint8 playerClass = characterInfo->Class;
    uint8 level = characterInfo->Level;

    if (!sObjectMgr->GetPlayerInfo(raceOrFactionChangeInfo->RaceID, playerClass))
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(CHAR_CREATE_ERROR);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    Field* fields = result->Fetch();
    uint32 at_loginFlags = fields[0].GetUInt16();
    char const* knownTitlesStr = fields[1].GetCString();
    uint32 used_loginFlag = raceOrFactionChangeInfo->FactionChange ? AT_LOGIN_CHANGE_FACTION : AT_LOGIN_CHANGE_RACE;

    if (!(at_loginFlags & used_loginFlag))
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(CHAR_CREATE_ERROR);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    uint32 newTeamId = Player::TeamForRace(raceOrFactionChangeInfo->RaceID);
    if (newTeamId == TEAM_OTHER)
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(CHAR_CREATE_RESTRICTED_RACECLASS);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    if (raceOrFactionChangeInfo->FactionChange == (Player::TeamForRace(oldRace) == newTeamId))
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(raceOrFactionChangeInfo->FactionChange ? CHAR_CREATE_CHARACTER_SWAP_FACTION : CHAR_CREATE_CHARACTER_RACE_ONLY);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_RACEMASK))
    {
        uint32 raceMaskDisabled = sWorld->getIntConfig(CONFIG_CHARACTER_CREATING_DISABLED_RACEMASK);
        if ((1 << (raceOrFactionChangeInfo->RaceID - 1)) & raceMaskDisabled)
        {
            WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

            data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

            data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

            data << uint8(CHAR_CREATE_ERROR);

            data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

            SendPacket(&data);
            return;
        }
    }

    // prevent character rename
    if (sWorld->getBoolConfig(CONFIG_PREVENT_RENAME_CUSTOMIZATION) && (raceOrFactionChangeInfo->Name != oldName))
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(CHAR_NAME_FAILURE);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    // prevent character rename to invalid name
    if (!normalizePlayerName(raceOrFactionChangeInfo->Name))
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(CHAR_NAME_NO_NAME);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    ResponseCodes res = ObjectMgr::CheckPlayerName(raceOrFactionChangeInfo->Name, GetSessionDbcLocale(), true);
    if (res != CHAR_NAME_SUCCESS)
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(res);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    // check name limitations
    if (!HasPermission(rbac::RBAC_PERM_SKIP_CHECK_CHARACTER_CREATION_RESERVEDNAME) && sObjectMgr->IsReservedName(raceOrFactionChangeInfo->Name))
    {
        WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

        data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

        data << uint8(CHAR_NAME_RESERVED);

        data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

        SendPacket(&data);
        return;
    }

    // character with this name already exist
    ObjectGuid newGuid = sCharacterCache->GetCharacterGuidByName(raceOrFactionChangeInfo->Name);
    if (!newGuid.IsEmpty())
    {
        if (newGuid != raceOrFactionChangeInfo->Guid)
        {
            WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

            data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

            data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

            data << uint8(CHAR_CREATE_NAME_IN_USE);

            data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

            SendPacket(&data);
            return;
        }
    }

    PreparedStatement* stmt = nullptr;
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    // resurrect the character in case he's dead
    Player::OfflineResurrect(raceOrFactionChangeInfo->Guid, trans);

    // Name Change and update atLogin flags
    CharacterDatabase.EscapeString(raceOrFactionChangeInfo->Name);

    Player::Customize(raceOrFactionChangeInfo->Guid, raceOrFactionChangeInfo->SexID, raceOrFactionChangeInfo->SkinID, raceOrFactionChangeInfo->FaceID,
        raceOrFactionChangeInfo->HairStyleID, raceOrFactionChangeInfo->HairColorID, raceOrFactionChangeInfo->FacialHairStyleID);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_FACTION_OR_RACE);
    stmt->setString(0, raceOrFactionChangeInfo->Name);
    stmt->setUInt8(1, raceOrFactionChangeInfo->RaceID);
    stmt->setUInt16(2, used_loginFlag);
    stmt->setUInt32(3, raceOrFactionChangeInfo->Guid.GetCounter());
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_DECLINED_NAME);
    stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
    trans->Append(stmt);

    sCharacterCache->UpdateCharacterData(raceOrFactionChangeInfo->Guid, raceOrFactionChangeInfo->Name, &raceOrFactionChangeInfo->SexID, &raceOrFactionChangeInfo->RaceID);

    if (oldRace != raceOrFactionChangeInfo->RaceID)
    {
        // delete all languages first
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_SKILL_LANGUAGES);
        stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
        trans->Append(stmt);

        // Now add them back
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_SKILL_LANGUAGE);
        stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());

        // Faction specific languages
        if (newTeamId == HORDE)
            stmt->setUInt16(1, 109);
        else
            stmt->setUInt16(1, 98);

        trans->Append(stmt);

        // Race specific languages
        if (raceOrFactionChangeInfo->RaceID != RACE_ORC && raceOrFactionChangeInfo->RaceID != RACE_HUMAN)
        {
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_SKILL_LANGUAGE);
            stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());

            switch (raceOrFactionChangeInfo->RaceID)
            {
                case RACE_DWARF:
                    stmt->setUInt16(1, 111);
                    break;
                case RACE_DRAENEI:
                    stmt->setUInt16(1, 759);
                    break;
                case RACE_GNOME:
                    stmt->setUInt16(1, 313);
                    break;
                case RACE_NIGHTELF:
                    stmt->setUInt16(1, 113);
                    break;
                case RACE_WORGEN:
                    stmt->setUInt16(1, 791);
                    break;
                case RACE_UNDEAD_PLAYER:
                    stmt->setUInt16(1, 673);
                    break;
                case RACE_TAUREN:
                    stmt->setUInt16(1, 115);
                    break;
                case RACE_TROLL:
                    stmt->setUInt16(1, 315);
                    break;
                case RACE_BLOODELF:
                    stmt->setUInt16(1, 137);
                    break;
                case RACE_GOBLIN:
                    stmt->setUInt16(1, 792);
                    break;
                case RACE_PANDAREN_NEUTRAL:
                    stmt->setUInt16(1, 905);
                    break;
            }

            trans->Append(stmt);
        }

        if (raceOrFactionChangeInfo->FactionChange)
        {
            // Delete all Flypaths
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_TAXI_PATH);
            stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
            trans->Append(stmt);

            if (level > 7)
            {
                // Update Taxi path
                // this doesn't seem to be 100% blizzlike... but it can't really be helped.
                std::ostringstream taximaskstream;
                TaxiMask const& factionMask = newTeamId == HORDE ? sDBCManager->GetTaxiNodesHordeMask() : sDBCManager->GetTaxiNodesAllianceMask();
                for (uint8 i = 0; i < MAX_TAXI_MASK_SIZE; ++i)
                {
                    // i = (315 - 1) / 8 = 39
                    // m = 1 << ((315 - 1) % 8) = 4
                    uint8 deathKnightExtraNode = playerClass != CLASS_DEATH_KNIGHT || i != 39 ? 0 : 4;
                    taximaskstream << uint32(factionMask[i] | deathKnightExtraNode) << ' ';
                }
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_TAXIMASK);
                stmt->setString(0, taximaskstream.str());
                stmt->setUInt32(1, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            if (!sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_GUILD))
            {
                // Reset guild
                if (Guild* guild = sGuildMgr->GetGuildById(characterInfo->GuildId))
                    guild->DeleteMember(raceOrFactionChangeInfo->Guid);
            }

            if (!HasPermission(rbac::RBAC_PERM_TWO_SIDE_ADD_FRIEND))
            {
                // Delete Friend List
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_SOCIAL_BY_GUID);
                stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_SOCIAL_BY_FRIEND);
                stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            // Reset homebind and position
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PLAYER_HOMEBIND);
            stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
            trans->Append(stmt);

            stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_PLAYER_HOMEBIND);
            stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());

            WorldLocation loc;
            uint16 zoneId = 0;
            if (newTeamId == ALLIANCE)
            {
                loc.WorldRelocate(0, -8867.68f, 673.373f, 97.9034f, 0.0f);
                zoneId = 1519;
            }
            else
            {
                loc.WorldRelocate(1, 1633.33f, -4439.11f, 15.7588f, 0.0f);
                zoneId = 1637;
            }

            stmt->setUInt16(1, loc.GetMapId());
            stmt->setUInt16(2, zoneId);
            stmt->setFloat(3, loc.GetPositionX());
            stmt->setFloat(4, loc.GetPositionY());
            stmt->setFloat(5, loc.GetPositionZ());
            trans->Append(stmt);

            Player::SavePositionInDB(loc, zoneId, raceOrFactionChangeInfo->Guid);

            // Achievement conversion
            for (std::map<uint32, uint32>::const_iterator it = sObjectMgr->FactionChangeAchievements.begin(); it != sObjectMgr->FactionChangeAchievements.end(); ++it)
            {
                uint32 achiev_alliance = it->first;
                uint32 achiev_horde = it->second;

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_ACHIEVEMENT_BY_ACHIEVEMENT);
                stmt->setUInt16(0, uint16(newTeamId == ALLIANCE ? achiev_alliance : achiev_horde));
                stmt->setUInt32(1, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_ACHIEVEMENT);
                stmt->setUInt16(0, uint16(newTeamId == ALLIANCE ? achiev_alliance : achiev_horde));
                stmt->setUInt16(1, uint16(newTeamId == ALLIANCE ? achiev_horde : achiev_alliance));
                stmt->setUInt32(2, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            // Item conversion
            for (std::map<uint32, uint32>::const_iterator it = sObjectMgr->FactionChangeItems.begin(); it != sObjectMgr->FactionChangeItems.end(); ++it)
            {
                uint32 item_alliance = it->first;
                uint32 item_horde = it->second;

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_INVENTORY_FACTION_CHANGE);
                stmt->setUInt32(0, (newTeamId == ALLIANCE ? item_alliance : item_horde));
                stmt->setUInt32(1, (newTeamId == ALLIANCE ? item_horde : item_alliance));
                stmt->setUInt32(2, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            // Delete all current quests
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_QUESTSTATUS);
            stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
            trans->Append(stmt);

            // Quest conversion
            for (std::map<uint32, uint32>::const_iterator it = sObjectMgr->FactionChangeQuests.begin(); it != sObjectMgr->FactionChangeQuests.end(); ++it)
            {
                uint32 quest_alliance = it->first;
                uint32 quest_horde = it->second;

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_QUESTSTATUS_REWARDED_BY_QUEST);
                stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
                stmt->setUInt32(1, (newTeamId == ALLIANCE ? quest_alliance : quest_horde));
                trans->Append(stmt);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_QUESTSTATUS_REWARDED_FACTION_CHANGE);
                stmt->setUInt32(0, (newTeamId == ALLIANCE ? quest_alliance : quest_horde));
                stmt->setUInt32(1, (newTeamId == ALLIANCE ? quest_horde : quest_alliance));
                stmt->setUInt32(2, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            // Mark all rewarded quests as "active" (will count for completed quests achievements)
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_QUESTSTATUS_REWARDED_ACTIVE);
            stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
            trans->Append(stmt);

            // Disable all old-faction specific quests
            {
                ObjectMgr::QuestMap const& questTemplates = sObjectMgr->GetQuestTemplates();
                for (ObjectMgr::QuestMap::const_iterator iter = questTemplates.begin(); iter != questTemplates.end(); ++iter)
                {
                    Quest const* quest = iter->second;
                    uint32 newRaceMask = (raceOrFactionChangeInfo->Guid.GetCounter() == ALLIANCE) ? RACEMASK_ALLIANCE : RACEMASK_HORDE;
                    if (!(quest->GetAllowableRaces() & newRaceMask))
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_QUESTSTATUS_REWARDED_ACTIVE_BY_QUEST);
                        stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
                        stmt->setUInt32(1, quest->GetQuestId());
                        trans->Append(stmt);
                    }
                }
            }

            // Spell conversion
            for (std::map<uint32, uint32>::const_iterator it = sObjectMgr->FactionChangeSpells.begin(); it != sObjectMgr->FactionChangeSpells.end(); ++it)
            {
                uint32 spell_alliance = it->first;
                uint32 spell_horde = it->second;

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_SPELL_BY_SPELL);
                stmt->setUInt32(0, (newTeamId == ALLIANCE ? spell_alliance : spell_horde));
                stmt->setUInt32(1, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_SPELL_FACTION_CHANGE);
                stmt->setUInt32(0, (newTeamId == ALLIANCE ? spell_alliance : spell_horde));
                stmt->setUInt32(1, (newTeamId == ALLIANCE ? spell_horde : spell_alliance));
                stmt->setUInt32(2, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            // Reputation conversion
            for (std::map<uint32, uint32>::const_iterator it = sObjectMgr->FactionChangeReputation.begin(); it != sObjectMgr->FactionChangeReputation.end(); ++it)
            {
                uint32 reputation_alliance = it->first;
                uint32 reputation_horde = it->second;
                uint32 newReputation = (newTeamId == ALLIANCE) ? reputation_alliance : reputation_horde;
                uint32 oldReputation = (newTeamId == ALLIANCE) ? reputation_horde : reputation_alliance;

                // select old standing set in db
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_REP_BY_FACTION);
                stmt->setUInt32(0, oldReputation);
                stmt->setUInt32(1, raceOrFactionChangeInfo->Guid.GetCounter());
                PreparedQueryResult result = CharacterDatabase.Query(stmt);

                if (!result)
                {
                    WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1);

                    data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

                    data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

                    data << uint8(CHAR_CREATE_ERROR);

                    data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

                    SendPacket(&data);
                    return;
                }

                Field* fields = result->Fetch();
                int32 oldDBRep = fields[0].GetInt32();
                FactionEntry const* factionEntry = sFactionStore.LookupEntry(oldReputation);

                // old base reputation
                int32 oldBaseRep = sObjectMgr->GetBaseReputationOf(factionEntry, oldRace, playerClass);

                // new base reputation
                int32 newBaseRep = sObjectMgr->GetBaseReputationOf(sFactionStore.LookupEntry(newReputation), raceOrFactionChangeInfo->RaceID, playerClass);

                // final reputation shouldnt change
                int32 FinalRep = oldDBRep + oldBaseRep;
                int32 newDBRep = FinalRep - newBaseRep;

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_REP_BY_FACTION);
                stmt->setUInt32(0, newReputation);
                stmt->setUInt32(1, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_REP_FACTION_CHANGE);
                stmt->setUInt16(0, uint16(newReputation));
                stmt->setInt32(1, newDBRep);
                stmt->setUInt16(2, uint16(oldReputation));
                stmt->setUInt32(3, raceOrFactionChangeInfo->Guid.GetCounter());
                trans->Append(stmt);
            }

            // Title conversion
            if (knownTitlesStr)
            {
                const uint32 ktcount = KNOWN_TITLES_SIZE * 2;
                uint32 knownTitles[ktcount];
                Tokenizer tokens(knownTitlesStr, ' ', ktcount);

                if (tokens.size() != ktcount)
                    return;

                for (uint32 index = 0; index < ktcount; ++index)
                    knownTitles[index] = atoul(tokens[index]);

                for (std::map<uint32, uint32>::const_iterator it = sObjectMgr->FactionChangeTitles.begin(); it != sObjectMgr->FactionChangeTitles.end(); ++it)
                {
                    uint32 title_alliance = it->first;
                    uint32 title_horde = it->second;

                    CharTitlesEntry const* atitleInfo = sCharTitlesStore.AssertEntry(title_alliance);
                    CharTitlesEntry const* htitleInfo = sCharTitlesStore.AssertEntry(title_horde);
                    // new team
                    if (newTeamId == ALLIANCE)
                    {
                        uint32 bitIndex = htitleInfo->MaskID;
                        uint32 index = bitIndex / 32;
                        uint32 old_flag = 1 << (bitIndex % 32);
                        uint32 new_flag = 1 << (atitleInfo->MaskID % 32);
                        if (knownTitles[index] & old_flag)
                        {
                            knownTitles[index] &= ~old_flag;
                            // use index of the new title
                            knownTitles[atitleInfo->MaskID / 32] |= new_flag;
                        }
                    }
                    else
                    {
                        uint32 bitIndex = atitleInfo->MaskID;
                        uint32 index = bitIndex / 32;
                        uint32 old_flag = 1 << (bitIndex % 32);
                        uint32 new_flag = 1 << (htitleInfo->MaskID % 32);
                        if (knownTitles[index] & old_flag)
                        {
                            knownTitles[index] &= ~old_flag;
                            // use index of the new title
                            knownTitles[htitleInfo->MaskID / 32] |= new_flag;
                        }
                    }

                    std::ostringstream ss;
                    for (uint32 index = 0; index < ktcount; ++index)
                        ss << knownTitles[index] << ' ';

                    stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_TITLES_FACTION_CHANGE);
                    stmt->setString(0, ss.str().c_str());
                    stmt->setUInt32(1, raceOrFactionChangeInfo->Guid.GetCounter());
                    trans->Append(stmt);

                    // unset any currently chosen title
                    stmt = CharacterDatabase.GetPreparedStatement(CHAR_RES_CHAR_TITLES_FACTION_CHANGE);
                    stmt->setUInt32(0, raceOrFactionChangeInfo->Guid.GetCounter());
                    trans->Append(stmt);
                }
            }
        }
    }

    CharacterDatabase.CommitTransaction(trans);

    std::string IP_str = GetRemoteAddress();
    TC_LOG_DEBUG("entities.player", "%s (IP: %s) changed race from %u to %u", GetPlayerInfo().c_str(), IP_str.c_str(), oldRace, raceOrFactionChangeInfo->RaceID);

    WorldPacket data(SMSG_CHAR_CUSTOMIZE, 1 + 8 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + raceOrFactionChangeInfo->Name.size());

    data.WriteGuidMask(raceOrFactionChangeInfo->Guid, 0, 7, 3, 2, 6, 5, 1, 4);

    data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 1);

    data << uint8(RESPONSE_SUCCESS);

    data << uint8(raceOrFactionChangeInfo->SexID);
    data << uint8(raceOrFactionChangeInfo->SkinID);
    data << uint8(raceOrFactionChangeInfo->FaceID);
    data << uint8(raceOrFactionChangeInfo->HairStyleID);
    data << uint8(raceOrFactionChangeInfo->HairColorID);
    data << uint8(raceOrFactionChangeInfo->FacialHairStyleID);

    data.WriteGuidBytes(raceOrFactionChangeInfo->Guid, 7, 3, 5, 2, 6, 0, 4);

    data.WriteBits(raceOrFactionChangeInfo->Name.size(), 6);
    data.WriteString(raceOrFactionChangeInfo->Name);

    SendPacket(&data);
}

void WorldSession::HandleRandomizeCharNameOpcode(WorldPacket& recvData)
{
    uint8 gender = 0;
    uint8 race = 0;

    recvData >> gender;
    recvData >> race;

    if (!Player::IsValidRace(race))
    {
        TC_LOG_DEBUG("general", "Invalid race (%u) sent by accountId: %u", race, GetAccountId());
        return;
    }

    if (!Player::IsValidGender(gender))
    {
        TC_LOG_DEBUG("general", "Invalid gender (%u) sent by accountId: %u", gender, GetAccountId());
        return;
    }

    std::string const* name = sDBCManager->GetRandomCharacterName(race, gender);

    WorldPacket data(SMSG_RANDOMIZE_CHAR_NAME, 1 + name->size());

    data.WriteBit(true);    // success

    data.WriteBits(name->size(), 6);

    data.FlushBits();

    data.WriteString(*name);

    SendPacket(&data);
}

void WorldSession::HandleReorderCharacters(WorldPacket& recvData)
{
    uint32 charactersCount = recvData.ReadBits(9);

    GuidVector guids(charactersCount);

    for (uint8 i = 0; i < charactersCount; ++i)
        recvData.ReadGuidMask(guids[i], 4, 2, 7, 6, 0, 5, 3, 1);

    uint8 position = 0;
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    for (uint8 i = 0; i < charactersCount; ++i)
    {
        recvData.ReadGuidBytes(guids[i], 1, 2, 7, 5, 4, 0, 3, 6);

        recvData >> position;

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_LIST_SLOT);
        stmt->setUInt8(0, position);
        stmt->setUInt32(1, guids[i].GetCounter());
        trans->Append(stmt);
    }

    CharacterDatabase.CommitTransaction(trans);
}

void WorldSession::HandleOpeningCinematic(WorldPacket& /*recvData*/)
{
    // Only players that has not yet gained any experience can use this
    if (_player->GetUInt32Value(PLAYER_XP))
        return;

    if (ChrClassesEntry const* classEntry = sChrClassesStore.LookupEntry(_player->GetClass()))
    {
        if (classEntry->CinematicSequenceID)
            _player->SendCinematicStart(classEntry->CinematicSequenceID);
        else if (ChrRacesEntry const* raceEntry = sChrRacesStore.LookupEntry(_player->GetRace()))
            _player->SendCinematicStart(raceEntry->CinematicSequenceID);
    }
}

void WorldSession::HandleLootSpecializationOpcode(WorldPacket& recvData)
{
    uint32 specialization;

    recvData >> specialization;

    GetPlayer()->SetLootSpecialization(specialization);
}

void WorldSession::SendCharLoginError(uint32 error)
{
    uint32 realError = error % CHAR_LOGIN_FAILED;

    m_playerRecentlyLogout = true;

    WorldPacket data(SMSG_CHARACTER_LOGIN_FAILED, 1);

    data << uint8(realError);

    SendPacket(&data);
}
