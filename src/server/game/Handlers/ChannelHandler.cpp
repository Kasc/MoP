/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectMgr.h"                                      // for normalizePlayerName
#include "ChannelMgr.h"
#include "Player.h"
#include "WorldSession.h"
#include "Channel.h"

static size_t const MAX_CHANNEL_NAME_STR = 0x31;
static size_t const MAX_CHANNEL_PASS_STR = 31;

void WorldSession::HandleChannelJoin(WorldPacket& recvData)
{
    bool Internal = false;
    bool enableVoice = false;

    uint32 channelId = 0;
    uint32 channelLength = 0;
    uint32 passLength = 0;

    std::string channelName;
    std::string password;

    recvData >> channelId;

    Internal = recvData.ReadBit(); // Joined by zone update

    channelLength = recvData.ReadBits(7);
    passLength = recvData.ReadBits(7);

    enableVoice = recvData.ReadBit();

    channelName = recvData.ReadString(channelLength);
    password = recvData.ReadString(passLength);

    AreaTableEntry const* zone = sAreaStore.LookupEntry(GetPlayer()->GetZoneId());

    if (channelId)
    {
        ChatChannelsEntry const* channel = sChatChannelsStore.LookupEntry(channelId);
        if (!channel)
            return;

        if (!zone || !GetPlayer()->CanJoinConstantChannelInZone(channel, zone))
            return;
    }

    if (channelName.empty())
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = cMgr->GetJoinChannel(channelId, channelName, zone))
            channel->JoinChannel(GetPlayer(), password);
}

void WorldSession::HandleChannelLeave(WorldPacket& recvData)
{
    uint32 channelId = 0;
    uint32 length = 0;

    std::string channelName;

    recvData >> channelId;

    length = recvData.ReadBits(7);

    channelName = recvData.ReadString(length);

    if (channelName.empty() && !channelId)
        return;

    AreaTableEntry const* zone = sAreaStore.LookupEntry(GetPlayer()->GetZoneId());
    if (channelId)
    {
        ChatChannelsEntry const* channel = sChatChannelsStore.LookupEntry(channelId);
        if (!channel)
            return;

        if (!zone || !GetPlayer()->CanJoinConstantChannelInZone(channel, zone))
            return;
    }

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
    {
        if (Channel* channel = cMgr->GetChannel(channelId, channelName, GetPlayer(), true, zone))
            channel->LeaveChannel(GetPlayer(), true);

        if (channelId)
            cMgr->LeftChannel(channelId, zone);
        else
            cMgr->LeftChannel(channelName);
    }
}

void WorldSession::HandleChannelList(WorldPacket& recvData)
{
    uint32 length = 0;
    std::string channelName;

    length = recvData.ReadBits(7);
    channelName = recvData.ReadString(length);

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->List(GetPlayer());
}

void WorldSession::HandleChannelPassword(WorldPacket& recvData)
{
    uint32 passLength = 0;
    uint32 channelLength = 0;

    passLength = recvData.ReadBits(7);
    channelLength = recvData.ReadBits(7);

    std::string password = recvData.ReadString(passLength);
    std::string channelName = recvData.ReadString(channelLength);

    if (password.length() > MAX_CHANNEL_PASS_STR)
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->Password(GetPlayer(), password);
}

void WorldSession::HandleChannelSetOwner(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    nameLength = recvData.ReadBits(9);
    channelLength = recvData.ReadBits(7);

    std::string channelName = recvData.ReadString(channelLength);
    std::string targetName = recvData.ReadString(nameLength);
    
    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->SetOwner(GetPlayer(), targetName);
}

void WorldSession::HandleChannelOwner(WorldPacket& recvData)
{
    uint32 length = 0;

    length = recvData.ReadBits(7);
    std::string channelName = recvData.ReadString(length);

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->SendWhoOwner(GetPlayer());
}

void WorldSession::HandleChannelModerator(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    channelLength = recvData.ReadBits(7);
    nameLength = recvData.ReadBits(9);

    std::string channelName = recvData.ReadString(channelLength);
    std::string targetName = recvData.ReadString(nameLength);

    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->SetModerator(GetPlayer(), targetName);
}

void WorldSession::HandleChannelUnmoderator(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    nameLength = recvData.ReadBits(9);
    channelLength = recvData.ReadBits(7);

    std::string channelName = recvData.ReadString(channelLength);
    std::string targetName = recvData.ReadString(nameLength);

    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->UnsetModerator(GetPlayer(), targetName);
}

void WorldSession::HandleChannelMute(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    channelLength = recvData.ReadBits(7);
    nameLength = recvData.ReadBits(9);

    std::string targetName = recvData.ReadString(nameLength);
    std::string channelName = recvData.ReadString(channelLength);
    
    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->SetMute(GetPlayer(), targetName);
}

void WorldSession::HandleChannelUnmute(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    nameLength = recvData.ReadBits(9);
    channelLength = recvData.ReadBits(7);

    std::string targetName = recvData.ReadString(nameLength);
    std::string channelName = recvData.ReadString(channelLength);

    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->UnsetMute(GetPlayer(), targetName);
}

void WorldSession::HandleChannelInvite(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    nameLength = recvData.ReadBits(9);
    channelLength = recvData.ReadBits(7);

    std::string channelName = recvData.ReadString(channelLength);
    std::string targetName = recvData.ReadString(nameLength);
    
    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->Invite(GetPlayer(), targetName);
}

void WorldSession::HandleChannelKick(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    channelLength = recvData.ReadBits(7);
    nameLength = recvData.ReadBits(9);

    std::string channelName = recvData.ReadString(channelLength);
    std::string targetName = recvData.ReadString(nameLength);

    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->Kick(GetPlayer(), targetName);
}

void WorldSession::HandleChannelBan(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    nameLength = recvData.ReadBits(9);
    channelLength = recvData.ReadBits(7);

    std::string targetName = recvData.ReadString(nameLength);
    std::string channelName = recvData.ReadString(channelLength);

    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->Ban(GetPlayer(), targetName);
}

void WorldSession::HandleChannelUnban(WorldPacket& recvData)
{
    uint32 nameLength = 0;
    uint32 channelLength = 0;

    nameLength = recvData.ReadBits(9);
    channelLength = recvData.ReadBits(7);

    std::string channelName = recvData.ReadString(channelLength);
    std::string targetName = recvData.ReadString(nameLength);

    if (!normalizePlayerName(targetName))
        return;

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->UnBan(GetPlayer(), targetName);
}

void WorldSession::HandleChannelAnnouncements(WorldPacket& recvData)
{
    uint32 length = 0;

    length = recvData.ReadBits(7);
    std::string channelName = recvData.ReadString(length);

    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = ChannelMgr::GetChannelForPlayerByNamePart(channelName, GetPlayer()))
            channel->Announce(GetPlayer());
}

void WorldSession::HandleChannelVoiceOnOpcode(WorldPacket& recvData)
{
    uint32 length = 0;

    length = recvData.ReadBits(7);
    std::string channelName = recvData.ReadString(length);

    /* uint64 playerGUID, groupGUID
    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = cMgr->GetChannel(channelName, GetPlayer()))
            channel->Voice(playerGUID, groupGUID);*/
}

void WorldSession::HandleChannelVoiceOffOpcode(WorldPacket& recvData)
{
    uint32 length = 0;

    length = recvData.ReadBits(8);
    std::string channelName = recvData.ReadString(length);

    /* uint64 playerGUID, groupGUID
    if (ChannelMgr* cMgr = ChannelMgr::ForTeam(GetPlayer()->GetTeam()))
        if (Channel* channel = cMgr->GetChannel(channelName, GetPlayer()))
            channel->DeVoice(playerGUID, groupGUID);*/
}

void WorldSession::HandleChannelSetActiveVoice(WorldPacket& recvData)
{
    recvData.read_skip<uint32>();
    recvData.read_skip<char*>();
}

void WorldSession::HandleChannelDeclineInvite(WorldPacket &recvData)
{
    // TODO: do something with packet
}