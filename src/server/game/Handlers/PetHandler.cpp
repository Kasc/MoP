/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "ObjectMgr.h"
#include "SpellMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "Spell.h"
#include "ObjectAccessor.h"
#include "CreatureAI.h"
#include "Util.h"
#include "Pet.h"
#include "World.h"
#include "Group.h"
#include "SpellHistory.h"
#include "SpellInfo.h"
#include "Player.h"

void WorldSession::HandleDismissCritter(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 4, 6, 7, 5, 1, 0, 2, 3);
    recvData.ReadGuidBytes(guid, 2, 4, 5, 0, 1, 7, 3, 6);

    Unit* pet = ObjectAccessor::GetCreatureOrPetOrVehicle(*_player, guid);
    if (!pet)
    {
        TC_LOG_DEBUG("entities.pet", "Vanitypet (%s) does not exist - player '%s' (guid: %u / account: %u) attempted to dismiss it (possibly lagged out)",
            guid.ToString().c_str(), GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().GetCounter(), GetAccountId());
        return;
    }

    if (_player->GetCritterGUID() == pet->GetGUID())
         if (pet->GetTypeId() == TYPEID_UNIT && pet->IsSummon())
             pet->ToTempSummon()->UnSummon();
}

void WorldSession::HandlePetAction(WorldPacket& recvData)
{
    ObjectGuid PetGUID;
    ObjectGuid TargetGUID;

    uint32 Action = 0;

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    recvData >> Action;

    // Position
    recvData >> y;
    recvData >> z;
    recvData >> x;

    recvData.ReadGuidMask(PetGUID, 1, 0, 6, 7, 5);

    recvData.ReadGuidMask(TargetGUID, 7);

    recvData.ReadGuidMask(PetGUID, 2, 3);

    recvData.ReadGuidMask(TargetGUID, 6, 3, 0, 2, 5);

    recvData.ReadGuidMask(PetGUID, 4);

    recvData.ReadGuidMask(TargetGUID, 4, 1);

    recvData.ReadGuidBytes(PetGUID, 7, 6, 1, 2, 5, 4);

    recvData.ReadGuidBytes(TargetGUID, 5);

    recvData.ReadGuidBytes(PetGUID, 3);

    recvData.ReadGuidBytes(TargetGUID, 0, 1, 7, 4, 6, 2, 3);

    recvData.ReadGuidBytes(PetGUID, 0);

    uint32 SpellID = UNIT_ACTION_BUTTON_ACTION(Action);
    uint8 Flag = UNIT_ACTION_BUTTON_TYPE(Action);

    // used also for charmed creature
    Unit* pet = ObjectAccessor::GetUnit(*_player, PetGUID);
    if (!pet)
    {
        TC_LOG_DEBUG("entities.pet", "HandlePetAction: Pet (%s) doesn't exist for player %s (%s)", PetGUID.ToString().c_str(), GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().ToString().c_str());
        return;
    }

    if (pet != GetPlayer()->GetFirstControlled())
    {
        TC_LOG_DEBUG("entities.pet", "HandlePetAction: Pet (%s) does not belong to player %s (%s)", PetGUID.ToString().c_str(), GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().ToString().c_str());
        return;
    }

    if (!pet->IsAlive())
    {
        SpellInfo const* spell = (Flag == ACT_ENABLED || Flag == ACT_PASSIVE) ? sSpellMgr->GetSpellInfo(SpellID) : NULL;
        if (!spell)
            return;
        if (!spell->HasAttribute(SPELL_ATTR0_CASTABLE_WHILE_DEAD))
            return;
    }

    /// @todo allow control charmed player?
    if (pet->GetTypeId() == TYPEID_PLAYER && !(Flag == ACT_COMMAND && SpellID == COMMAND_ATTACK))
        return;

    if (GetPlayer()->m_Controlled.size() == 1)
        HandlePetActionHelper(pet, PetGUID, SpellID, Flag, TargetGUID, x, y, z);
    else
    {
        //If a pet is dismissed, m_Controlled will change
        std::vector<Unit*> controlled;
        for (Unit::ControlList::iterator itr = GetPlayer()->m_Controlled.begin(); itr != GetPlayer()->m_Controlled.end(); ++itr)
            if ((*itr)->GetEntry() == pet->GetEntry() && (*itr)->IsAlive())
                controlled.push_back(*itr);

        for (std::vector<Unit*>::iterator itr = controlled.begin(); itr != controlled.end(); ++itr)
            HandlePetActionHelper(*itr, PetGUID, SpellID, Flag, TargetGUID, x, y, z);
    }
}

void WorldSession::HandlePetStopAttack(WorldPacket &recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 5, 1, 6, 0, 2, 4, 3);
    recvData.ReadGuidBytes(guid, 2, 5, 0, 4, 1, 7, 6, 3);

    Unit* pet = ObjectAccessor::GetCreatureOrPetOrVehicle(*_player, guid);
    if (!pet)
    {
        TC_LOG_DEBUG("entities.pet", "HandlePetStopAttack: Pet %s does not exist", guid.ToString().c_str());
        return;
    }

    if (pet != GetPlayer()->GetPet() && pet != GetPlayer()->GetCharm())
    {
        TC_LOG_DEBUG("entities.pet", "HandlePetStopAttack: %s isn't a pet or charmed creature of player %s",
            guid.ToString().c_str(), GetPlayer()->GetName().c_str());
        return;
    }

    if (!pet->IsAlive())
        return;

    pet->AttackStop();
    pet->ClearInPetCombat();
}

void WorldSession::HandlePetActionHelper(Unit* unit, ObjectGuid guid1, uint32 spellid, uint16 flag, ObjectGuid guid2, float x, float y, float z)
{
    CharmInfo* charmInfo = unit->GetCharmInfo();
    if (!charmInfo)
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetAction(petGuid: %s, tagGuid: %s, spellId: %u, flag: %u): object (entry: %u TypeId: %u) is considered pet-like but doesn't have a charminfo!",
            guid1.ToString().c_str(), guid2.ToString().c_str(), spellid, flag, unit->GetGUID().GetCounter(), unit->GetTypeId());
        return;
    }

    switch (flag)
    {
        case ACT_COMMAND:                                   //0x07
            switch (spellid)
            {
                case COMMAND_STAY:                          //flat=1792  //STAY
                    unit->StopMoving();
                    unit->GetMotionMaster()->Clear(false);
                    unit->GetMotionMaster()->MoveIdle();
                    charmInfo->SetCommandState(COMMAND_STAY);

                    charmInfo->SetIsCommandAttack(false);
                    charmInfo->SetIsAtStay(true);
                    charmInfo->SetIsCommandFollow(false);
                    charmInfo->SetIsFollowing(false);
                    charmInfo->SetIsReturning(false);
                    charmInfo->SaveStayPosition();
                    break;
                case COMMAND_FOLLOW:                        //spellid=1792  //FOLLOW
                    unit->AttackStop();
                    unit->InterruptNonMeleeSpells(false);
                    unit->ClearInPetCombat();
                    unit->GetMotionMaster()->MoveFollow(_player, PET_FOLLOW_DIST, unit->GetFollowAngle());
                    charmInfo->SetCommandState(COMMAND_FOLLOW);

                    charmInfo->SetIsCommandAttack(false);
                    charmInfo->SetIsAtStay(false);
                    charmInfo->SetIsReturning(true);
                    charmInfo->SetIsCommandFollow(true);
                    charmInfo->SetIsFollowing(false);
                    break;
                case COMMAND_ATTACK:                        //spellid=1792  //ATTACK
                {
                    // Can't attack if owner is pacified
                    if (_player->HasAuraType(SPELL_AURA_MOD_PACIFY))
                    {
                        //pet->SendPetCastFail(spellid, SPELL_FAILED_PACIFIED);
                        /// @todo Send proper error message to client
                        return;
                    }

                    // only place where pet can be player
                    Unit* TargetUnit = ObjectAccessor::GetUnit(*_player, guid2);
                    if (!TargetUnit)
                        return;

                    if (Unit* owner = unit->GetOwner())
                        if (!owner->IsValidAttackTarget(TargetUnit))
                            return;

                    unit->ClearUnitState(UNIT_STATE_FOLLOW);
                    // This is true if pet has no target or has target but targets differs.
                    if (unit->GetVictim() != TargetUnit || (unit->GetVictim() == TargetUnit && !unit->GetCharmInfo()->IsCommandAttack()))
                    {
                        if (unit->GetVictim())
                            unit->AttackStop();

                        if (unit->GetTypeId() != TYPEID_PLAYER && unit->ToCreature()->IsAIEnabled)
                        {
                            charmInfo->SetIsCommandAttack(true);
                            charmInfo->SetIsAtStay(false);
                            charmInfo->SetIsFollowing(false);
                            charmInfo->SetIsCommandFollow(false);
                            charmInfo->SetIsReturning(false);

                            unit->ToCreature()->AI()->AttackStart(TargetUnit);

                            //10% chance to play special pet attack talk, else growl
                            if (unit->IsPet() && ((Pet*)unit)->GetPetType() == SUMMON_PET && unit != TargetUnit && Math::Rand(0, 100) < 10)
                                unit->SendPetTalk((uint32)PET_TALK_ATTACK);
                            else
                            {
                                // 90% chance for pet and 100% chance for charmed creature
                                unit->SendPetAIReaction(guid1);
                            }
                        }
                        else                                // charmed player
                        {
                            charmInfo->SetIsCommandAttack(true);
                            charmInfo->SetIsAtStay(false);
                            charmInfo->SetIsFollowing(false);
                            charmInfo->SetIsCommandFollow(false);
                            charmInfo->SetIsReturning(false);

                            unit->Attack(TargetUnit, true);
                            unit->SendPetAIReaction(guid1);
                        }
                    }
                    break;
                }
                case COMMAND_ABANDON:                       // abandon (hunter pet) or dismiss (summoned pet)
                    if (unit->GetCharmerGUID() == GetPlayer()->GetGUID())
                        _player->StopCastingCharm();
                    else if (unit->GetOwnerGUID() == GetPlayer()->GetGUID())
                    {
                        ASSERT(unit->GetTypeId() == TYPEID_UNIT);
                        if (Pet* pet = unit->ToPet())
                        {
                            if (pet->GetPetType() == HUNTER_PET)
                                GetPlayer()->RemovePet(pet, PET_SLOT_DELETED, false, pet->IsStampeded());
                            else
                                //dismissing a summoned pet is like killing them (this prevents returning a soulshard...)
                                pet->SetDeathState(CORPSE);
                        }
                        else if (unit->HasUnitTypeMask(UNIT_MASK_MINION))
                        {
                            ((Minion*)unit)->UnSummon();
                        }
                    }
                    break;
                case COMMAND_MOVE_TO:
                    unit->StopMoving();
                    unit->GetMotionMaster()->Clear(false);
                    unit->GetMotionMaster()->MovePoint(0, x, y, z);
                    charmInfo->SetCommandState(COMMAND_MOVE_TO);

                    charmInfo->SetIsCommandAttack(false);
                    charmInfo->SetIsAtStay(true);
                    charmInfo->SetIsFollowing(false);
                    charmInfo->SetIsReturning(false);
                    charmInfo->SaveStayPosition();
                    break;
                default:
                    TC_LOG_DEBUG("entities.pet", "WORLD: unknown PET flag Action %i and spellid %i.", uint32(flag), spellid);
            }
            break;
        case ACT_REACTION:                                  // 0x6
            switch (spellid)
            {
                case REACT_PASSIVE:                         //passive
                    unit->AttackStop();
                    unit->ClearInPetCombat();
                    // no break;
                case REACT_DEFENSIVE:                       //recovery
                case REACT_AGGRESSIVE:                      //activete
                    if (unit->GetTypeId() == TYPEID_UNIT)
                        unit->ToCreature()->SetReactState(ReactStates(spellid));
                    break;
            }
            break;
        case ACT_DISABLED:                                  // 0x81    spell (disabled), ignore
        case ACT_PASSIVE:                                   // 0x01
        case ACT_ENABLED:                                   // 0xC1    spell
        {
            Unit* unit_target = NULL;

            if (guid2)
                unit_target = ObjectAccessor::GetUnit(*_player, guid2);

            // do not cast unknown spells
            SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellid);
            if (!spellInfo)
            {
                TC_LOG_DEBUG("entities.pet", "WORLD: unknown PET spell id %i", spellid);
                return;
            }

            for (SpellEffectInfo const* effect: spellInfo->GetEffects())
            {
                if (effect && (effect->TargetA.GetTarget() == TARGET_UNIT_SRC_AREA_ENEMY || effect->TargetA.GetTarget() == TARGET_UNIT_DEST_AREA_ENEMY || effect->TargetA.GetTarget() == TARGET_DEST_DYNOBJ_ENEMY))
                    return;
            }

            // do not cast not learned spells
            if (!unit->HasSpell(spellid) || spellInfo->IsPassive())
                return;

            //  Clear the flags as if owner clicked 'attack'. AI will reset them
            //  after AttackStart, even if spell failed
            if (unit->GetCharmInfo())
            {
                unit->GetCharmInfo()->SetIsAtStay(false);
                unit->GetCharmInfo()->SetIsCommandAttack(true);
                unit->GetCharmInfo()->SetIsReturning(false);
                unit->GetCharmInfo()->SetIsFollowing(false);
            }

            Spell* spell = new Spell(unit, spellInfo, TRIGGERED_NONE);

            SpellCastResult result = spell->CheckPetCast(unit_target);

            //auto turn to target unless possessed
            if (result == SPELL_FAILED_UNIT_NOT_INFRONT && !unit->IsPossessed() && !unit->IsVehicle())
            {
                if (unit_target)
                {
                    if (!unit->IsFocusing())
                        unit->SetInFront(unit_target);

                    if (Player* player = unit_target->ToPlayer())
                        unit->SendUpdateToPlayer(player);
                }
                else if (Unit* unit_target2 = spell->m_targets.GetUnitTarget())
                {
                    if (!unit->IsFocusing())
                        unit->SetInFront(unit_target2);

                    if (Player* player = unit_target2->ToPlayer())
                        unit->SendUpdateToPlayer(player);
                }

                if (Unit* powner = unit->GetCharmerOrOwner())
                    if (Player* player = powner->ToPlayer())
                        unit->SendUpdateToPlayer(player);

                result = SPELL_CAST_OK;
            }

            if (result == SPELL_CAST_OK)
            {
                unit_target = spell->m_targets.GetUnitTarget();

                //10% chance to play special pet attack talk, else growl
                //actually this only seems to happen on special spells, fire shield for imp, torment for voidwalker, but it's stupid to check every spell
                if (unit->IsPet() && (((Pet*)unit)->GetPetType() == SUMMON_PET) && (unit != unit_target) && (Math::Rand(0, 100) < 10))
                    unit->SendPetTalk((uint32)PET_TALK_SPECIAL_SPELL);
                else
                {
                    unit->SendPetAIReaction(guid1);
                }

                if (unit_target && !GetPlayer()->IsFriendlyTo(unit_target) && !unit->IsPossessed() && !unit->IsVehicle())
                {
                    // This is true if pet has no target or has target but targets differs.
                    if (unit->GetVictim() != unit_target)
                    {
                        unit->GetMotionMaster()->Clear();
                        if (unit->ToCreature()->IsAIEnabled)
                            unit->ToCreature()->AI()->AttackStart(unit_target);
                    }
                }

                spell->prepare(&(spell->m_targets));
            }
            else
            {
                if (unit->IsPossessed() || unit->IsVehicle()) /// @todo: confirm this check
                    Spell::SendCastResult(GetPlayer(), spellInfo, 0, result);
                else
                    spell->SendPetCastResult(result);

                if (!unit->GetSpellHistory()->HasCooldown(spellid))
                    unit->GetSpellHistory()->ResetCooldown(spellid, true);

                spell->finish(false);
                delete spell;

                // reset specific flags in case of spell fail. AI will reset other flags
                if (unit->GetCharmInfo())
                    unit->GetCharmInfo()->SetIsCommandAttack(false);
            }
            break;
        }
        default:
            TC_LOG_ERROR("entities.pet", "WORLD: unknown PET flag Action %i and spellid %i.", uint32(flag), spellid);
    }
}

void WorldSession::HandlePetNameQuery(WorldPacket& recvData)
{
    ObjectGuid petGuid;
    ObjectGuid petNumber;

    recvData.ReadGuidMask(petNumber, 0, 5);

    recvData.ReadGuidMask(petGuid, 1, 7);

    recvData.ReadGuidMask(petNumber, 7);

    recvData.ReadGuidMask(petGuid, 6, 4, 5, 0);

    recvData.ReadGuidMask(petNumber, 3, 6, 2);

    recvData.ReadGuidMask(petGuid, 3, 2);

    recvData.ReadGuidMask(petNumber, 1, 4);

    recvData.ReadGuidBytes(petNumber, 2, 1, 0, 7);

    recvData.ReadGuidBytes(petGuid, 5, 0);

    recvData.ReadGuidBytes(petNumber, 6);

    recvData.ReadGuidBytes(petGuid, 4);

    recvData.ReadGuidBytes(petNumber, 5);

    recvData.ReadGuidBytes(petGuid, 2, 6);

    recvData.ReadGuidBytes(petNumber, 3);

    recvData.ReadGuidBytes(petGuid, 3);

    recvData.ReadGuidBytes(petNumber, 4);

    recvData.ReadGuidBytes(petGuid, 1, 7);

    SendPetNameQuery(petGuid, petNumber);
}

void WorldSession::SendPetNameQuery(ObjectGuid petGuid, ObjectGuid petNumber)
{
    Creature* creature = ObjectAccessor::GetCreatureOrPetOrVehicle(*_player, petGuid);
    if (!creature)
    {
        WorldPacket data(SMSG_PET_NAME_QUERY_RESPONSE, 1 + 8);

        data.WriteBit(false);

        data.FlushBits();

        data << uint64(petNumber.GetRawValue());

        _player->SendDirectMessage(&data);

        return;
    }

    DeclinedNames declinedNames;

    uint32 DeclinedNamesSize = 0;
    if (Pet* pet = creature->ToPet())
    {
        declinedNames = pet->GetDeclinedNames();
        for (std::string const& Name : declinedNames)
            DeclinedNamesSize += Name.length();
    }

    bool HasDeclinedNames = DeclinedNamesSize > 0;

    WorldPacket data(SMSG_PET_NAME_QUERY_RESPONSE, 2 + MAX_DECLINED_NAME_CASES * (1) + DeclinedNamesSize + creature->GetName().length() + 4 + 8);

    data.WriteBit(true); // HasData

    for (std::string const& Name : declinedNames)
        data.WriteBits(Name.length(), 7);

    data.WriteBit(HasDeclinedNames);

    data.WriteBits(creature->GetName().size(), 8);

    data.FlushBits();

    for (std::string const& Name : declinedNames)
        data.WriteString(Name);

    data.WriteString(creature->GetName());

    data << uint32(creature->GetUInt32Value(UNIT_PET_NAME_TIMESTAMP));
    data << uint64(petNumber.GetRawValue());

    _player->SendDirectMessage(&data);
}

bool WorldSession::CheckStableMaster(ObjectGuid guid)
{
    // spell case or GM
    if (guid == GetPlayer()->GetGUID())
    {
        if (!GetPlayer()->IsGameMaster() && !GetPlayer()->HasAuraType(SPELL_AURA_OPEN_STABLE))
        {
            TC_LOG_DEBUG("entities.pet", "%s attempt open stable in cheating way.", guid.ToString().c_str());
            return false;
        }
    }
    // stable master case
    else
    {
        if (!GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_STABLEMASTER))
        {
            TC_LOG_DEBUG("entities.pet", "Stablemaster %s not found or you can't interact with him.", guid.ToString().c_str());
            return false;
        }
    }
    return true;
}

void WorldSession::HandlePetSetAction(WorldPacket& recvData)
{
    uint32 Position = 0;
    uint32 Action = 0;

    ObjectGuid petguid;

    recvData >> Position;
    recvData >> Action;

    recvData.ReadGuidMask(petguid, 1, 0, 5, 3, 2, 7, 6, 4);
    recvData.ReadGuidBytes(petguid, 5, 6, 7, 3, 2, 1, 4, 0);

    uint32 SpellID = UNIT_ACTION_BUTTON_ACTION(Action);
    uint8 ActState = UNIT_ACTION_BUTTON_TYPE(Action);

    //ignore invalid position
    if (Position >= MAX_UNIT_ACTION_BAR_INDEX)
        return;

    Unit* pet = ObjectAccessor::GetUnit(*_player, petguid);
    if (!pet || pet != _player->GetFirstControlled())
    {
        TC_LOG_DEBUG("entities.pet", "HandlePetSetAction: Unknown pet (%s) or pet owner (GUID: %u)", petguid.ToString().c_str(), _player->GetGUID().GetCounter());
        return;
    }

    CharmInfo* charmInfo = pet->GetCharmInfo();
    if (!charmInfo)
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetSetAction: object (GUID: %u TypeId: %u) is considered pet-like but doesn't have a charminfo!", pet->GetGUID().GetCounter(), pet->GetTypeId());
        return;
    }

    //if it's act for spell (en/disable/cast) and there is a spell given (0 = remove spell) which pet doesn't know, don't add
    if (!((ActState == ACT_ENABLED || ActState == ACT_DISABLED || ActState == ACT_PASSIVE) && SpellID && !pet->HasSpell(SpellID)))
    {
        if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SpellID))
        {
            //sign for autocast
            if (ActState == ACT_ENABLED)
            {
                if (pet->GetTypeId() == TYPEID_UNIT && pet->IsPet())
                    ((Pet*)pet)->ToggleAutocast(spellInfo, true);
                else
                    for (Unit::ControlList::iterator itr = GetPlayer()->m_Controlled.begin(); itr != GetPlayer()->m_Controlled.end(); ++itr)
                        if ((*itr)->GetEntry() == pet->GetEntry())
                            (*itr)->GetCharmInfo()->ToggleCreatureAutocast(spellInfo, true);
            }
            //sign for no/turn off autocast
            else if (ActState == ACT_DISABLED)
            {
                if (pet->GetTypeId() == TYPEID_UNIT && pet->IsPet())
                    ((Pet*)pet)->ToggleAutocast(spellInfo, false);
                else
                    for (Unit::ControlList::iterator itr = GetPlayer()->m_Controlled.begin(); itr != GetPlayer()->m_Controlled.end(); ++itr)
                        if ((*itr)->GetEntry() == pet->GetEntry())
                            (*itr)->GetCharmInfo()->ToggleCreatureAutocast(spellInfo, false);
            }
        }

        charmInfo->SetActionBar(Position, SpellID, ActiveStates(ActState));
    }
}

void WorldSession::HandlePetRename(WorldPacket& recvData)
{
    DeclinedNames declinedNames;

    std::array<uint32, MAX_DECLINED_NAME_CASES> DeclinedNamesLength;

    uint32 PetNumber = 0;

    std::string Name;

    bool HasName = false;
    bool HasDeclinedNames = false;

    recvData >> PetNumber;

    HasName = !recvData.ReadBit();
    HasDeclinedNames = recvData.ReadBit();

    if (HasDeclinedNames)
        for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
            DeclinedNamesLength[i] = recvData.ReadBits(7);

    if (HasName)
    {
        uint32 NameLength = recvData.ReadBits(8);
        Name = recvData.ReadString(NameLength);
    }

    if (HasDeclinedNames)
        for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
            declinedNames[i] = recvData.ReadBits(DeclinedNamesLength[i]);

    Pet* pet = _player->GetPet();
    if (!pet || pet->GetPetType() != HUNTER_PET ||
        !pet->HasByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_RENAME_STATE, UNIT_CAN_BE_RENAMED) ||
        !pet->GetCharmInfo())
        return;

    PetNameInvalidReason res = ObjectMgr::CheckPetName(Name);
    if (res != PET_NAME_SUCCESS)
    {
        SendPetNameInvalid(res, Name, declinedNames, HasDeclinedNames, PetNumber);
        return;
    }

    if (sObjectMgr->IsReservedName(Name))
    {
        SendPetNameInvalid(PET_NAME_RESERVED, Name, declinedNames, HasDeclinedNames, PetNumber);
        return;
    }

    pet->SetName(Name);

    if (_player->GetGroup())
        _player->SetGroupUpdateFlag(GROUP_UPDATE_FLAG_PET_NAME);

    pet->RemoveByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_RENAME_STATE, UNIT_CAN_BE_RENAMED);

    if (HasDeclinedNames)
    {
        std::wstring wname;
        if (!Utf8toWStr(Name, wname))
            return;

        if (!ObjectMgr::CheckDeclinedNames(wname, declinedNames))
        {
            SendPetNameInvalid(PET_NAME_DECLENSION_DOESNT_MATCH_BASE_NAME, Name, declinedNames, HasDeclinedNames, PetNumber);
            return;
        }
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    if (HasDeclinedNames)
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_PET_DECLINEDNAME);
        stmt->setUInt32(0, pet->GetCharmInfo()->GetPetNumber());
        trans->Append(stmt);

        stmt = CharacterDatabase.GetPreparedStatement(CHAR_ADD_CHAR_PET_DECLINEDNAME);
        stmt->setUInt32(0, _player->GetGUID().GetCounter());

        for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; i++)
            stmt->setString(i + 1, declinedNames[i]);

        trans->Append(stmt);
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_PET_NAME);
    stmt->setString(0, Name);
    stmt->setUInt32(1, _player->GetGUID().GetCounter());
    stmt->setUInt32(2, pet->GetCharmInfo()->GetPetNumber());
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);

    pet->SetUInt32Value(UNIT_PET_NAME_TIMESTAMP, uint32(time(NULL))); // cast can't be helped
}

void WorldSession::HandlePetAbandon(WorldPacket& recvData)
{
    ObjectGuid petGUID;

    recvData.ReadGuidMask(petGUID, 7, 3, 4, 2, 5, 6, 1, 0);
    recvData.ReadGuidBytes(petGUID, 0, 2, 5, 6, 7, 1, 4, 3);

    // pet/charmed
    Creature* creature = ObjectAccessor::GetCreatureOrPetOrVehicle(*GetPlayer(), petGUID);
    if (creature)
    {
        if (Pet* pet = creature->ToPet())
            _player->RemovePet(pet, PET_SLOT_DELETED, false, pet->IsStampeded());
        else if (creature->GetGUID() == _player->GetCharmGUID())
            _player->StopCastingCharm();
    }
}

void WorldSession::HandlePetSpellAutocastOpcode(WorldPacket& recvData)
{
    ObjectGuid petGUID;

    uint32 spellID = 0;

    bool enabled = false;

    recvData >> spellID;

    recvData.ReadGuidMask(petGUID, 0, 4, 2, 6, 1, 5, 3, 7);

    enabled = recvData.ReadBit();

    recvData.ReadGuidBytes(petGUID, 5, 0, 4, 1, 7, 2, 3, 6);

    if (!_player->GetGuardianPet() && !_player->GetCharm())
        return;

    Creature* creature = ObjectAccessor::GetCreatureOrPetOrVehicle(*_player, petGUID);
    if (!creature)
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetSpellAutocastOpcode: Pet %s not found.", petGUID.ToString().c_str());
        return;
    }

    if (creature != _player->GetGuardianPet() && creature != _player->GetCharm())
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetSpellAutocastOpcode: %s isn't pet of player %s (%s).",
            petGUID.ToString().c_str(), GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().ToString().c_str());
        return;
    }

    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellID);
    if (!spellInfo)
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetSpellAutocastOpcode: Unknown spell id %u used by %s.", spellID, petGUID.ToString().c_str());
        return;
    }

    // do not add not learned spells/ passive spells
    if (!creature->HasSpell(spellID) || !spellInfo->IsAutocastable())
        return;

    CharmInfo* charmInfo = creature->GetCharmInfo();
    if (!charmInfo)
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetSpellAutocastOpcode: object (%s) is considered pet-like but doesn't have a charminfo!", creature->GetGUID().ToString().c_str());
        return;
    }

    if (Pet* pet = creature->ToPet())
        pet->ToggleAutocast(spellInfo, enabled);
    else
        charmInfo->ToggleCreatureAutocast(spellInfo, enabled);

    charmInfo->SetSpellAutocast(spellInfo, enabled);
}

void WorldSession::HandlePetCastSpellOpcode(WorldPacket& recvData)
{
    ObjectGuid petGuid;

    uint8 castCount = 0;
    uint8 castFlags = 0;

    uint32 spellId = 0;
    uint32 glyphIndex = 0;
    uint32 targetMask = 0;
    uint32 targetStringLength = 0;

    float elevation = 0.0f;
    float missileSpeed = 0.0f;

    ObjectGuid targetGuid;
    ObjectGuid itemTargetGuid;
    ObjectGuid destTransportGuid;
    ObjectGuid srcTransportGuid;

    Position srcPos;
    Position destPos;

    std::string targetString;

    // Movement data
    MovementInfo movementInfo;

    ObjectGuid movementTransportGuid;
    ObjectGuid movementGuid;

    bool hasTransport = false;
    bool hasTransportTime2 = false;
    bool hasTransportTime3 = false;
    bool hasFallData = false;
    bool hasFallDirection = false;
    bool hasTimestamp = false;
    bool hasSplineElevation = false;
    bool hasPitch = false;
    bool hasOrientation = false;
    bool hasMoveTime = false;

    uint32 RemoveForcesCount = 0;

    bool hasDestLocation = recvData.ReadBit();

    recvData.ReadGuidMask(petGuid, 7);

    bool hasMissileSpeed = !recvData.ReadBit();
    bool hasSrcLocation = recvData.ReadBit();

    recvData.ReadGuidMask(petGuid, 1);

    uint8 SpellArchaeologyCount = recvData.ReadBits(2);
    std::vector<SpellArchaeology> SpellArchaeologyProjects(SpellArchaeologyCount);

    bool hasTargetMask = !recvData.ReadBit();

    recvData.ReadGuidMask(petGuid, 4);

    recvData.ReadBit(); // Fake bit

    recvData.ReadGuidMask(petGuid, 6);

    bool hasTargetString = !recvData.ReadBit();

    recvData.ReadBit(); // Fake Bit

    bool hasMovement = recvData.ReadBit();
    bool hasCastFlags = !recvData.ReadBit();
    bool hasSpellId = !recvData.ReadBit();

    recvData.ReadGuidMask(petGuid, 0, 5, 2);

    for (uint8 i = 0; i < SpellArchaeologyCount; ++i)
        SpellArchaeologyProjects[i].Type = recvData.ReadBits(2);

    recvData.ReadGuidMask(petGuid, 3);

    bool hasGlyphIndex = !recvData.ReadBit();
    bool hasCastCount = !recvData.ReadBit();
    bool hasElevation = !recvData.ReadBit();

    if (hasMovement)
    {
        hasOrientation = !recvData.ReadBit();
        hasSplineElevation = !recvData.ReadBit();

        bool HeightChangeFailed = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 5, 7);

        bool hasMovementFlags2 = !recvData.ReadBit();
        hasTimestamp = !recvData.ReadBit();
        hasFallData = recvData.ReadBit();
        bool hasMovementFlags = !recvData.ReadBit();
        hasMoveTime = !recvData.ReadBit();

        if (hasMovementFlags)
            movementInfo.flags = recvData.ReadBits(30);

        bool RemoteTimeValid = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 6);

        hasTransport = recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 0);

        RemoveForcesCount = recvData.ReadBits(22);

        if (hasTransport)
        {
            hasTransportTime2 = recvData.ReadBit();
            hasTransportTime3 = recvData.ReadBit();

            recvData.ReadGuidMask(movementTransportGuid, 5, 6, 4, 0, 1, 2, 7, 3);
        }

        recvData.ReadGuidMask(movementGuid, 1);

        if (hasMovementFlags2)
            movementInfo.flags2 = recvData.ReadBits(13);

        recvData.ReadGuidMask(movementGuid, 3, 2);

        bool hasSpline = !recvData.ReadBit();

        hasPitch = !recvData.ReadBit();

        recvData.ReadGuidMask(movementGuid, 4);

        if (hasFallData)
            hasFallDirection = recvData.ReadBit();
    }

    if (hasDestLocation)
        recvData.ReadGuidMask(destTransportGuid, 2, 0, 1, 4, 5, 6, 3, 7);

    if (hasCastFlags)
        castFlags = recvData.ReadBits(5);

    recvData.ReadGuidMask(targetGuid, 2, 4, 7, 0, 6, 1, 5, 3);

    if (hasTargetMask)
        targetMask = recvData.ReadBits(20);

    if (hasTargetString)
        targetStringLength = recvData.ReadBits(7);

    if (hasSrcLocation)
        recvData.ReadGuidMask(srcTransportGuid, 2, 0, 3, 1, 6, 7, 4, 5);

    recvData.ReadGuidMask(itemTargetGuid, 6, 0, 3, 4, 2, 1, 5, 7);

    recvData.ReadGuidBytes(petGuid, 2, 6, 3);

    for (uint8 i = 0; i < SpellArchaeologyCount; ++i)
    {
        recvData >> SpellArchaeologyProjects[i].ID;
        recvData >> SpellArchaeologyProjects[i].Quantity;
    }

    recvData.ReadGuidBytes(petGuid, 1, 7, 0, 4, 5);

    if (hasDestLocation)
    {
        float x, y, z;
        recvData.ReadGuidBytes(destTransportGuid, 4, 1, 7);

        recvData >> z;
        recvData >> y;

        recvData.ReadGuidBytes(destTransportGuid, 6, 3);

        recvData >> x;

        recvData.ReadGuidBytes(destTransportGuid, 2, 5, 0);

        destPos.Relocate(x, y, z);
    }

    if (hasMovement)
    {
        if (hasPitch)
            movementInfo.pitch = G3D::wrap(recvData.read<float>(), float(-M_PI), float(M_PI));

        if (hasTransport)
        {
            if (hasTransportTime3)
                recvData >> movementInfo.transport.vehicleId;

            if (hasTransportTime2)
                recvData >> movementInfo.transport.prevTime;

            recvData >> movementInfo.transport.seat;

            movementInfo.transport.pos.SetOrientation(recvData.read<float>());

            recvData >> movementInfo.transport.pos.m_positionZ;

            recvData.ReadGuidBytes(movementTransportGuid, 2);

            recvData >> movementInfo.transport.time;

            recvData.ReadGuidBytes(movementTransportGuid, 3);

            recvData >> movementInfo.transport.pos.m_positionX;

            recvData.ReadGuidBytes(movementTransportGuid, 6, 5, 7, 0);

            recvData >> movementInfo.transport.pos.m_positionY;

            recvData.ReadGuidBytes(movementTransportGuid, 4, 1);
        }

        if (hasMoveTime)
        {
            uint32 UnkTimer = 0;
            recvData >> UnkTimer;
        }

        for (uint32 i = 0; i != RemoveForcesCount; i++)
        {
            uint32 RemoveForcesIDs = 0;
            recvData >> RemoveForcesIDs;
            movementInfo.forcedMovement.RemovedForcedMovements.push_back(RemoveForcesIDs);
        }

        recvData.ReadGuidBytes(movementGuid, 3);

        if (hasOrientation)
            movementInfo.pos.SetOrientation(recvData.read<float>());

        recvData.ReadGuidBytes(movementGuid, 5);

        if (hasFallData)
        {
            recvData >> movementInfo.fall.zspeed;

            if (hasFallDirection)
            {
                recvData >> movementInfo.fall.cosAngle;
                recvData >> movementInfo.fall.xyspeed;
                recvData >> movementInfo.fall.sinAngle;
            }

            recvData >> movementInfo.fall.fallTime;
        }

        if (hasTimestamp)
            recvData >> movementInfo.time;

        recvData.ReadGuidBytes(movementGuid, 6);

        recvData >> movementInfo.pos.m_positionX;

        recvData.ReadGuidBytes(movementGuid, 1);

        recvData >> movementInfo.pos.m_positionZ;

        recvData.ReadGuidBytes(movementGuid, 2, 7, 0);

        recvData >> movementInfo.pos.m_positionY;

        recvData.ReadGuidBytes(movementGuid, 4);

        if (hasSplineElevation)
            recvData >> movementInfo.splineElevation;
    }

    if (hasSrcLocation)
    {
        float x, y, z;

        recvData.ReadGuidBytes(srcTransportGuid, 3, 4, 2, 1, 0, 7);

        recvData >> z;

        recvData.ReadGuidBytes(srcTransportGuid, 6, 5);

        recvData >> x;
        recvData >> y;

        srcPos.Relocate(x, y, z);
    }

    if (hasMissileSpeed)
        recvData >> missileSpeed;

    recvData.ReadGuidBytes(targetGuid, 1, 2, 5, 7, 4, 6, 3, 0);

    recvData.ReadGuidBytes(itemTargetGuid, 1, 5, 7, 3, 0, 2, 4, 6);

    if (hasElevation)
        recvData >> elevation;

    if (hasCastCount)
        recvData >> castCount;

    if (hasTargetString)
        targetString = recvData.ReadString(targetStringLength);

    if (hasGlyphIndex)
        recvData >> glyphIndex;

    if (hasSpellId)
        recvData >> spellId;

    // This opcode is also sent from charmed and possessed units (players and creatures)
    if (!_player->GetGuardianPet() && !_player->GetCharm())
        return;

    Unit* caster = ObjectAccessor::GetUnit(*_player, petGuid);
    if (!caster)
    {
        TC_LOG_DEBUG("entities.pet", "WorldSession::HandlePetCastSpellOpcode: Caster %s not found.", petGuid.ToString().c_str());
        return;
    }

    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId);
    if (!spellInfo)
    {
        TC_LOG_DEBUG("entities.pet", "WORLD: unknown PET spell id %u", spellId);
        return;
    }

    bool triggered = false;
    AuraEffect* triggeredByAura = caster->GetTriggeredByClientAura(spellId);
    if (triggeredByAura)
    {
        triggered = true;
        castCount = 0;
    }

    // do not cast not learned spells
    if ((!caster->HasSpell(spellId) && !triggered) || spellInfo->IsPassive())
        return;

    // client provided targets
    SpellCastTargets targets(caster, targetMask, targetGuid, itemTargetGuid, srcTransportGuid, destTransportGuid, srcPos, destPos, elevation, missileSpeed, targetString);

    caster->ClearUnitState(UNIT_STATE_FOLLOW);

    Spell* spell = new Spell(caster, spellInfo, triggered ? TRIGGERED_FULL_MASK : TRIGGERED_NONE, caster->GetGUID());

    spell->m_cast_count = castCount;                    // probably pending spell cast
    spell->m_targets = targets;

    spell->m_SpellArchaeologyProjects = SpellArchaeologyProjects;

    SpellCastResult result = triggered ? SPELL_CAST_OK : spell->CheckPetCast(nullptr);

    if (result == SPELL_CAST_OK)
    {
        if (Creature* creature = caster->ToCreature())
        {
            if (Pet* pet = creature->ToPet())
            {
                // 10% chance to play special pet attack talk, else growl
                // actually this only seems to happen on special spells, fire shield for imp, torment for voidwalker, but it's stupid to check every spell
                if (pet->GetPetType() == SUMMON_PET && (Math::Rand(0, 100) < 10))
                    pet->SendPetTalk(PET_TALK_SPECIAL_SPELL);
                else
                    pet->SendPetAIReaction(petGuid);
            }
        }

        spell->prepare(&(spell->m_targets), triggeredByAura);
    }
    else
    {
        spell->SendPetCastResult(result);

        if (!caster->GetSpellHistory()->HasCooldown(spellId) && !triggered)
            caster->GetSpellHistory()->ResetCooldown(spellId, true);

        spell->finish(false);
        delete spell;
    }
}

void WorldSession::SendPetNameInvalid(uint32 error, const std::string& name, DeclinedNames const& declinedNames, bool HasDeclinedNames, uint32 petNumber)
{
    WorldPacket data(SMSG_PET_NAME_INVALID);

    data.WriteBit(1); // HasData

    data.WriteBits(name.length(), 8);

    data.WriteBit(HasDeclinedNames);

    if (HasDeclinedNames)
    {
        for (std::string const& Name : declinedNames)
            data.WriteBits(Name.size(), 7);

        data.FlushBits();

        for (std::string const& Name : declinedNames)
            data.WriteString(Name);
    }
    else
        data.FlushBits();

    data.WriteString(name);

    data << uint8(error);
    data << uint32(petNumber);

    SendPacket(&data);
}

void WorldSession::HandleRequestPetInfoOpcode(WorldPacket& /*recvData */)
{
    /*
    TC_LOG_DEBUG("entities.pet", "WORLD: CMSG_REQUEST_PET_INFO");
    recvData.hexlike();
    */
}

void WorldSession::HandleRequestStabledPets(WorldPacket& recvData)
{
    ObjectGuid stableMasterGUID;

    recvData.ReadGuidMask(stableMasterGUID, 0, 5, 1, 3, 6, 7, 2, 4);
    recvData.ReadGuidBytes(stableMasterGUID, 0, 5, 7, 1, 2, 3, 4, 6);

    if (!CheckStableMaster(stableMasterGUID))
        return;

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    // remove mounts this fix bug where getting pet from stable while mounted deletes pet.
    if (GetPlayer()->IsMounted())
        GetPlayer()->RemoveAurasByType(SPELL_AURA_MOUNTED);

    SendOpenStable(stableMasterGUID);
}

void WorldSession::SendOpenStable(ObjectGuid stableMasterGUID)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SLOTS_DETAIL);

    stmt->setUInt32(0, _player->GetGUID().GetCounter());
    stmt->setUInt8(1, PET_SLOT_HUNTER_FIRST);
    stmt->setUInt8(2, PET_SLOT_STABLE_LAST);

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt).WithPreparedCallback(std::bind(&WorldSession::SendStablePetCallback, this, stableMasterGUID, std::placeholders::_1)));
}

void WorldSession::SendStablePetCallback(ObjectGuid guid, PreparedQueryResult result)
{
    if (!GetPlayer())
        return;

    std::vector<PetStableData> petStableVector;

    uint32 NameSize = 0;
    if (result)
    {
        do
        {
            Field* fields = result->Fetch();

            PetStableData pet;

            pet.PetNumber = fields[1].GetUInt32();
            pet.PetEntry = fields[2].GetUInt32();
            pet.PetLevel = uint32(fields[3].GetUInt16());
            pet.PetName = fields[4].GetString();
            pet.PetDisplayID = fields[5].GetUInt32();

            uint32 slot = fields[0].GetUInt8();

            pet.PetSlot = slot;
            pet.PetStableState = slot > PET_SLOT_HUNTER_LAST ? PET_STABLE_INACTIVE : PET_STABLE_ACTIVE;

            NameSize += pet.PetName.length();

            petStableVector.push_back(std::move(pet));
        }
        while (result->NextRow());
    }

    ByteBuffer petData;

    WorldPacket data(SMSG_PET_STABLE_LIST, 1 + 8 + 3 + petStableVector.size() * (1 + 4 + 4 + 1 + 4 + 4 + 4) + NameSize);

    data.WriteGuidMask(guid, 3, 0, 4, 7, 2, 1, 6, 5);

    data.WriteBits(petStableVector.size(), 19);

    for (PetStableData const& stablePet : petStableVector)
    {
        data.WriteBits(stablePet.PetName.length(), 8);

        petData << uint32(stablePet.PetEntry);
        petData << uint32(stablePet.PetLevel);
        petData << uint8(stablePet.PetStableState);
        petData << uint32(stablePet.PetDisplayID);

        petData.WriteString(stablePet.PetName);

        petData << uint32(stablePet.PetNumber);
        petData << uint32(stablePet.PetSlot);
    }

    data.append(petData);

    data.WriteGuidBytes(guid, 3, 5, 7, 2, 0, 4, 1, 6);

    SendPacket(&data);
}

void WorldSession::SendStableResult(uint8 res)
{
    WorldPacket data(SMSG_STABLE_RESULT, 1);

    data << uint8(res);

    SendPacket(&data);
}

void WorldSession::HandleSetPetSlot(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 petSlot = 0;
    uint32 petNumber = 0;

    recvData >> petNumber;
    recvData >> petSlot;

    recvData.ReadGuidMask(guid, 5, 7, 3, 2, 6, 1, 0, 4);
    recvData.ReadGuidBytes(guid, 0, 3, 2, 6, 5, 7, 4, 1);

    if (!GetPlayer()->IsAlive())
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    if (!CheckStableMaster(guid))
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    if (petSlot > MAX_PET_STABLES)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    Pet* pet = GetPlayer()->GetPet();

    // If we move the pet already summoned...
    if (pet)
    {
        CharmInfo * charm = pet->GetCharmInfo();

        if (charm && charm->GetPetNumber() == petNumber)
            _player->RemovePet(pet, PET_SLOT_ACTUAL_PET_SLOT, pet->IsStampeded());

        // If we move to the pet already summoned...
        if (GetPlayer()->GetCurrentPetSlot() == petSlot)
            GetPlayer()->RemovePet(pet, PET_SLOT_ACTUAL_PET_SLOT, pet->IsStampeded());
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SLOT_BY_ID);

    stmt->setUInt32(0, GetPlayer()->GetGUID().GetCounter());
    stmt->setUInt32(1, petNumber);

    _queryProcessor.AddQuery(CharacterDatabase.AsyncQuery(stmt).WithPreparedCallback(std::bind(&WorldSession::HandleSetPetSlotCallback, this, petSlot, std::placeholders::_1)));
}

void WorldSession::HandleSetPetSlotCallback(uint8 newSlot, PreparedQueryResult result)
{
    if (!GetPlayer())
        return;

    if (!result)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    Field* fields = result->Fetch();

    uint8 oldSlot       = fields[0].GetUInt8();
    uint32 petEntry     = fields[1].GetUInt32();
    uint32 petNumber    = fields[2].GetUInt32();

    if (!petEntry)
    {
        SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(petEntry);
    if (!creatureInfo || !creatureInfo->IsTameable(_player->CanTameExoticPets()))
    {
        // If we try to stable exotic pets.
        if (creatureInfo && creatureInfo->IsTameable(true))
            SendStableResult(STABLE_ERR_EXOTIC);
        else
            SendStableResult(STABLE_ERR_STABLE);
        return;
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_PET_DATA_OWNER);
    stmt->setUInt8(0, newSlot);
    stmt->setUInt8(1, oldSlot);
    stmt->setUInt32(2, GetPlayer()->GetGUID().GetCounter());
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_PET_DATA_OWNER_ID);
    stmt->setUInt8(0, oldSlot);
    stmt->setUInt8(1, newSlot);
    stmt->setUInt32(2, GetPlayer()->GetGUID().GetCounter());
    stmt->setUInt32(3, petNumber);
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);

    if (newSlot != PET_SLOT_OTHER_PET)
    {
        // We need to remove and add the new pet to a different slot
        // GetPlayer()->setPetSlotUsed((PetSlot)slot, false);
        _player->SetPetSlotInUse((PetSlot)oldSlot, false);
        _player->SetPetSlotInUse((PetSlot)newSlot, true);

        StableResultCode code = STABLE_SUCCESS_UNSTABLE;
        if (oldSlot < PET_SLOT_STABLE_FIRST && newSlot >= PET_SLOT_STABLE_FIRST)
            code = STABLE_SUCCESS_STABLE;

        SendStableResult(code);
    }
    else
        SendStableResult(STABLE_ERR_STABLE);
}

void WorldSession::HandeLearnPetSpecializationGroup(WorldPacket& recvData)
{
    uint32 specGroupIndex = 0;

    ObjectGuid petGUID;

    recvData >> specGroupIndex;

    recvData.ReadGuidMask(petGUID, 5, 7, 3, 0, 6, 4, 1, 2);
    recvData.ReadGuidBytes(petGUID, 7, 5, 4, 3, 0, 2, 6, 1);

    if (!_player->IsInWorld())
        return;

    Pet* pet = ObjectAccessor::GetPet(*_player, petGUID);

    if (!pet || !pet->IsPet() || ((Pet*)pet)->GetPetType() != HUNTER_PET ||
        pet->GetOwnerGUID() != _player->GetGUID() || !pet->GetCharmInfo())
        return;

    if (specGroupIndex >= MAX_SPECIALIZATIONS)
    {
        TC_LOG_DEBUG("entities.pet", "WORLD: HandlePetSetSpecializationOpcode - specialization index %u out of range", specGroupIndex);
        return;
    }

    ChrSpecializationEntry const* petSpec = sDBCManager->GetChrSpecializationByIndexArray(0, specGroupIndex);
    if (!petSpec)
    {
        TC_LOG_DEBUG("entities.pet", "WORLD: HandlePetSetSpecializationOpcode - specialization index %u not found", specGroupIndex);
        return;
    }

    if (_player->GetLevel() < MIN_SPECIALIZATION_LEVEL)
    {
        TC_LOG_DEBUG("entities.pet", "WORLD: HandlePetSetSpecializationOpcode - player level too low for specializations");
        return;
    }

    pet->SetSpecialization(petSpec->ID);
}

void WorldSession::LoadPet(PreparedQueryResult result)
{
    if (result)
    {
        Field* fields = result->Fetch();

        uint32 petID    = fields[0].GetUInt32();
        PetType petType = PetType(fields[1].GetUInt8());

        PetLoginQueryHolder* petHolder = new PetLoginQueryHolder(petID, petType);

        if (!petHolder->Initialize())
        {
            delete petHolder;
            return;
        }

        _petLoginCallback = CharacterDatabase.DelayQueryHolder(petHolder);
    }
}

void WorldSession::HandleLoadPet(PetLoginQueryHolder* petHolder)
{
    Player* player = GetPlayer();
    if (player && player->IsInWorld())
    {
        Pet* pet = new Pet(player, petHolder->GetPetType());
        if (!pet->LoadPetFromDB(petHolder))
            delete pet;
    }

    delete petHolder;
}
