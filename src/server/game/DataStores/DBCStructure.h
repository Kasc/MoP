/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_DBCSTRUCTURE_H
#define TRINITY_DBCSTRUCTURE_H

#include "Common.h"
#include "DBCEnums.h"
#include "Define.h"
#include "Util.h"

// Structures using to access raw DBC data and required packing to portability

// GCC have alternative #pragma pack(N) syntax and old gcc version not support pack(push, N), also any gcc version not support it at some platform
#if defined(__GNUC__)
#pragma pack(1)
#else
#pragma pack(push, 1)
#endif

enum DBCDefines
{
    MAX_GROUP_AREA_IDS              = 6,

    MAX_OUTFIT_ITEMS                = 24,

    MAX_MASTERY_SPELLS              = 2,

    MAX_CREATURE_SPELL_DATA_SLOT    = 4,

    MAX_FACTION_RELATIONS           = 4,

    MAX_HOLIDAY_DURATIONS           = 10,
    MAX_HOLIDAY_DATES               = 26,
    MAX_HOLIDAY_FLAGS               = 10,

    MAX_ITEM_RANDOM_PROPERTIES      = 5,
    MAX_ITEM_SET_ITEMS              = 17,
    MAX_ITEM_SET_SPELLS             = 8,

    MAX_LOCK_CASE                   = 8,

    MAX_MOUNT_CAPABILITIES          = 24,

    MAX_OVERRIDE_SPELL              = 10,

    MAX_SKILL_STEP                  = 16,

    MAX_SPELL_EFFECTS               = 32,
    MAX_SPELL_TOTEMS                = 2,

    MAX_SHAPESHIFT_SPELLS           = 8,

    MAX_ITEM_ENCHANTMENT_EFFECTS    = 3,

    MAX_TALENT_TIERS                = 6,
    MAX_TALENT_COLUMNS              = 3,

    MAX_VEHICLE_SEATS               = 8,

    MAX_TAXI_MASK_SIZE              = 162,

    MAX_WORLD_MAP_OVERLAY_AREA_IDX  = 4,

    MAX_TITLE_PVP_HK_RANKS          = 28,
    MAX_PVP_HK_RANKS                = 14
};

#define MAX_EFFECT_MASK 0xFFFFFFFF

struct AchievementEntry
{
    uint32      ID;                                         // 0
    int32       Faction;                                    // 1 -1=all, 0=horde, 1=alliance
    int32       MapID;                                      // 2 -1=none
    uint32      Supercedes;                                 // 3 its Achievement parent (can`t start while parent uncomplete, use its Criteria if don`t have own, use its progress on begin)
    char*       Title;                                      // 4
    char*       Description;                                // 5
    uint32      Category;                                   // 6
    uint32      Points;                                     // 7 reward points
    uint32      UIOrder;                                    // 8
    uint32      Flags;                                      // 9
    uint32      IconID;                                     // 10 icon (from SpellIcon.dbc)
    char*       Reward;                                     // 11
    uint32      MinimumCriteria;                            // 12 - need this count of completed criterias (own or referenced achievement criterias)
    uint32      SharesCriteria;                             // 13 - referenced achievement (counting of all completed criterias)
    uint32      CriteriaTree;                               // 14
};

struct AchievementCategory
{
    uint32      ID;
    int32       ParentID;
    char*       CategoryName;
    uint32      OrderIndex;
};

struct AnimKitEntry
{
    uint32      ID;                                         // 0
    //uint32    OneShotDuration;                            // 1
    //uint32    OneShotStopAnimKitID;                       // 2
};

struct AreaTableEntry
{
    uint32      ID;                                         // 0
    uint32      MapID;                                      // 1
    uint32      ParentAreaID;                               // 2 if 0 then it's zone, else it's zone id of this area
    uint32      AreaBit;                                    // 3, main index
    uint32      Flags;                                      // 4,
    //unit32    Unk1                                        // 5,
    //uint32    SoundProviderPref;                          // 6,
    //uint32    SoundProviderPrefUnderwater;                // 7,
    //uint32    AmbienceID;                                 // 8,
    //uint32    ZoneMusic;                                  // 9,
    //char*     ZoneName;                                   // 10 - Internal name
    //uint32    IntroSound;                                 // 11
    uint32      ExplorationLevel;                           // 12
    char*       AreaName;                                   // 13 - In-game name
    uint32      FactionGroupMask;                           // 14
    uint32      LiquidTypeID[4];                            // 15-18
    float       MaxDepth;                                   // 19
    //float     AmbientMultiplier;                          // 20
    //uint32    MountFlags;                                 // 21
    //uint32    UWIntroMusic;                               // 22
    //uint32    UWZoneMusic;                                // 23
    //uint32    UWAmbience;                                 // 24
    //uint32    WorldPvPID;                                 // 25 World_PVP_Area.dbc
    //uint32    WindSettingsID;                             // 26
    //uint32    PvPCombatWorldStateID;                      // 27
    uint32      WildBattlePetLevelMin;                      // 28
    uint32      WildBattlePetLevelMax;                      // 29

    // helpers
    uint32 GetFlags() const
    {
        return Flags;
    }

    bool IsSanctuary() const
    {
        if (MapID == 609)
            return true;

        return (Flags & AREA_FLAG_SANCTUARY);
    }

    bool IsFlyable() const
    {
        if (Flags & AREA_FLAG_OUTLAND)
        {
            if (!(Flags & AREA_FLAG_NO_FLY_ZONE))
                return true;
        }

        return false;
    }
};

struct AreaGroupEntry
{
    uint32      ID;                                         // 0
    uint32      AreaId[MAX_GROUP_AREA_IDS];                 // 1-6
    uint32      NextGroup;                                  // 7 index of next group
};

struct AreaTriggerEntry
{
    uint32          ID;                                     // 0
    uint32          MapID;                                  // 1
    DBCPosition3D   Pos;                                    // 2-4
    //uint32        PhaseUseFlags                           // 5
    //uint32        PhaseID                                 // 6
    //uint32        PhaseGroupID                            // 7
    float           Radius;                                 // 8
    float           BoxLength;                              // 9
    float           BoxWidth;                               // 10
    float           BoxHeight;                              // 11
    float           BoxYaw;                                 // 12
    //uint32        ShapeType                               // 13
    //uint32        ShapeID                                 // 14
    //uint32        AreaTriggerActionSetID                  // 15
};

struct ArmorLocationEntry
{
    uint32    ID;                                           // 0
    float     Modifier[5];                                  // 1-5 multiplier for armor types (cloth...plate, no armor?)
};

struct AuctionHouseEntry
{
    uint32      ID;                                         // 0
    uint32      FactionID;                                  // 1 id of faction.dbc for player factions associated with city
    uint32      DepositRate;                                // 2 1/3 from real
    uint32      ConsignmentRate;                            // 3
    char*       Name;                                       // 4
};

struct BankBagSlotPricesEntry
{
    uint32      ID;
    uint32      Price;
};

struct BannedAddOnsEntry
{
    uint32      ID;                                         // 0
    //uint32    NameMD5[4];                                 // 1
    //uint32    VersionMD5[4];                              // 2
    //uint32    LastModified;                               // 3
    //uint32    Flags;                                      // 4
};

struct BarberShopStyleEntry
{
    uint32      ID;                                         // 0
    uint32      Type;                                       // 1 value 0 -> hair, value 2 -> facialhair
    char*       DisplayName;                                // 2
    char*       Description;                                // 3
    float       CostModifier;                               // 4
    uint32      Race;                                       // 5
    uint32      Gender;                                     // 6
    uint32      Data;                                       // 7 (real ID to hair/facial hair)
};

struct BattlemasterListEntry
{
    uint32      ID;                                         // 0
    int32       MapID[16];                                  // 1-16 mapid
    uint32      InstanceType;                               // 17 map type (3 - BG, 4 - arena)
    //uint32    GroupsAllowed;                              // 18 (0 or 1)
    char*       Name;                                       // 19
    uint32      MaxGroupSize;                               // 20 maxGroupSize, used for checking if queue as group
    uint32      HolidayWorldState;                          // 21 new 3.1
    uint32      MinLevel;                                   // 22, min level (sync with PvPDifficulty.dbc content)
    uint32      MaxLevel;                                   // 23, max level (sync with PvPDifficulty.dbc content)
    //uint32    RatedPlayers;                               // 24 4.0.1
    //uint32    MinPlayers;                                 // 25 - 4.0.6.13596
    //uint32    MaxPlayers;                                 // 26 4.0.1
    //uint32    Flags;                                      // 27 4.0.3, value 2 for Rated Battlegrounds
    //uint32    IconFileDataID;                             // 28
    //char*     GameType;                                   // 29
};

struct CharSectionsEntry
{
    //uint32    ID;
    uint32      Race;
    uint32      Gender;
    uint32      GenType;
    //char*     TexturePath[3];
    uint32      Flags;
    uint32      Type;
    uint32      Color;
};

struct CharStartOutfitEntry
{
    uint32      ID;                                         // 0
    uint8       Race;                                       // 1
    uint8       Class;                                      // 2
    uint8       Gender;                                     // 3
    uint8       OutfitId;                                   // 4
    int32       ItemID[MAX_OUTFIT_ITEMS];                   // 5-28
    int32       ItemDisplayId[MAX_OUTFIT_ITEMS];            // 29-52 not required at server side
    int32       ItemInventorySlot[MAX_OUTFIT_ITEMS];        // 53-76 not required at server side
    uint32      PetDisplayId;                               // 77 Pet Model ID for starting pet
    uint32      PetFamilyEntry;                             // 78 Pet Family Entry for starting pet

    // helpers
    uint32 GetItemId(uint8 index) const
    {
        return ItemID[index];
    }
};

struct CharTitlesEntry
{
    uint32      ID;                                         // 0, title ids, for example in Quest::GetCharTitleId()
    //uint32    ConditionID;                                // 1
    char*       NameMale;                                   // 2 m_name_lang
    char*       NameFemale;                                 // 3 m_name1_lang
    uint32      MaskID;                                     // 4 m_mask_ID used in PLAYER_CHOSEN_TITLE and 1<<index in PLAYER__FIELD_KNOWN_TITLES
    //uint32    Flags;                                      // 5
};

struct ChatChannelsEntry
{
    uint32      ID;                                         // 0
    uint32      Flags;                                      // 1
    //uint32    FactionGroup                                // 2
    char*       Name;                                       // 3
    //char*     Shortcut;                                   // 4

    // helpers
    uint32 GetFlags() const
    {
        return Flags;
    }
};

struct ChrClassesEntry
{
    uint32      ID;                                         // 0
    uint32      PowerType;                                  // 1
    //char*     PetNameToken                                // 2
    char*       Name;                                       // 3
    //char*     NameFemale;                                 // 4
    //char*     NameMale;                                   // 5
    //char*     Filename;                                   // 6
    uint32      SpellClassSet;                              // 7
    //uint32    Flags;                                      // 8
    uint32      CinematicSequenceID;                        // 9
    uint32      AttackPowerPerStrength;                     // 10 Attack Power bonus per point of strength
    uint32      AttackPowerPerAgility;                      // 11 Attack Power bonus per point of agility
    uint32      RangedAttackPowerPerAgility;                // 12 Ranged Attack Power bonus per point of agility
    uint32      DefaultSpec;                                // 13
    //uint32    CreateScreenFileDataID;                     // 14
    //uint32    SelectScreenFileDataID;                     // 15
    //uint32    LowResScreenFileDataID;                     // 16
    //uint32    IconFileDataID;                             // 17
};

struct ChrRacesEntry
{
    uint32      ID;                                         // 0
    uint32      Flags;                                      // 1
    uint32      FactionID;                                  // 2 faction template id
    //uint32    ExplorationSoundID;                         // 3
    uint32      MaleDisplayID;                              // 4
    uint32      FemaleDisplayID;                            // 5
    //char*     ClientPrefix;                               // 6
    uint32      TeamID;                                     // 7 (42-Neutral 7-Alliance 1-Horde)
    //uint32    CreatureType;                               // 8
    //uint32    ResSicknessSpellID;                         // 9
    //uint32    SplashSoundID;                              // 10
    //char*     ClientFileString;                           // 11
    uint32      CinematicSequenceID;                        // 12
    //uint32    TeamID2;                                    // 13 m_alliance (0 alliance, 1 horde, 2 neutral)
    char*       Name;                                       // 14
    //char*     NameFemale;                                 // 15
    //char*     NameMale;                                   // 16
    //char*     FacialHairCustomization[2];                 // 17-18
    //char*     HairCustomization;                          // 19
    //uint32    RaceRelated;                                // 20
    //uint32    UnalteredVisualRaceID;                      // 21
    //uint32    UAMaleCreatureSoundDataID;                  // 22
    //uint32    UAFemaleCreatureSoundDataID;                // 23
    //uint32    CharComponentTextureLayoutID;               // 24
    //uint32    DefaultClassID;                             // 25
    //uint32    CreateScreenFileDataID;                     // 26
    //uint32    SelectScreenFileDataID;                     // 27
    //float     MaleCustomizeOffset[3];                     // 28-30
    //float     FemaleCustomizeOffset[3];                   // 31-33
    //uint32    NeutralRaceID;                              // 34
    //uint32    LowResScreenFileDataID;                     // 35
};

struct ChrPowerTypesEntry
{
    uint32      ID;                                         // 0
    uint32      ClassID;                                    // 1
    uint32      PowerType;                                  // 2
};

struct ChrSpecializationEntry
{
    uint32      ID;                                         // 0 Specialization ID
    //char*     BackgroundFile;                             // 1
    uint32      ClassID;                                    // 2
    uint32      MasterySpellID[MAX_MASTERY_SPELLS];         // 3-4
    uint32      OrderIndex;                                 // 5
    uint32      PetTalentType;                              // 6
    uint32      Role;                                       // 7 (0 - Tank, 1 - Healer, 2 - DPS)
    uint32      SpellIconID;                                // 8
    uint32      RaidBuffs;                                  // 9
    uint32      Flags;                                      // 10
    //char*     Name;                                       // 11
    //char*     Name2;                                      // 12 Same as name_lang?
    //char*     Description;                                // 13
};

struct CinematicCameraEntry
{
    uint32          ID;                                     // 0
    char*           Model;                                  // 1    Model filename (translate .mdx to .m2)
    uint32          SoundID;                                // 2    Sound ID       (voiceover for cinematic)
    DBCPosition3D   Origin;                                 // 3-5  Position in map used for basis for M2 co-ordinates
    float           OriginFacing;                           // 4    Orientation in map used for basis for M2 co-ordinates
};

struct CinematicSequencesEntry
{
    uint32      ID;                                         // 0
    uint32      SoundID;                                    // 1
    uint32      Camera[8];                                  // 2-9
};

struct CreatureDisplayInfoEntry
{
    uint32      ID;                                         // 0
    uint32      ModelID;                                    // 1
    uint32      SoundID;                                    // 2
    uint32      ExtendedDisplayInfoID;                      // 3
    float       CreatureModelScale;                         // 4
    uint32      CreatureModelAlpha;                         // 5
    char*       TextureVariation[3];                        // 6-8
    char*       PortraitTextureName;                        // 9
    int32       SizeClass;                                  // 10
    uint32      BloodID;                                    // 11
    uint32      NPCSoundID;                                 // 12
    uint32      ParticleColorID;                            // 13
    uint32      CreatureGeosetData;                         // 14
    uint32      ObjectEffectPackageID;                      // 15
    uint32      AnimReplacementSetID;                       // 16
    uint32      Flags;                                      // 17
    int32       Gender;                                     // 18
    uint32      StateSpellVisualKitID;                      // 19
};

struct CreatureDisplayInfoExtraEntry
{
    uint32      ID;                                         // 0
    uint32      DisplayRaceID;                              // 1
    uint32      DisplayGenderID;                            // 2
    //uint32    SkinColor;                                  // 3
    //uint32    FaceType;                                   // 4
    //uint32    HairType;                                   // 5
    //uint32    HairStyle;                                  // 6
    //uint32    FacialHair;                                 // 7
    //uint32    HelmDisplayId;                              // 8
    //uint32    ShoulderDisplayId;                          // 9
    //uint32    ShirtDisplayId;                             // 10
    //uint32    ChestDisplayId;                             // 11
    //uint32    BeltDisplayId;                              // 12
    //uint32    LegsDisplayId;                              // 13
    //uint32    BootsDisplayId;                             // 14
    //uint32    WristDisplayId;                             // 15
    //uint32    GlovesDisplayId                             // 16
    //uint32    TabardDisplayId;                            // 17
    //uint32    CloakDisplayId;                             // 18
    //uint32    CanEquip;                                   // 19
    //char*     Texture;                                    // 20
};

struct CreatureFamilyEntry
{
    uint32      ID;                                         // 0
    float       MinScale;                                   // 1
    uint32      MinScaleLevel;                              // 2
    float       MaxScale;                                   // 3
    uint32      MaxScaleLevel;                              // 4
    uint32      SkillLine[2];                               // 5-6
    uint32      PetFoodMask;                                // 7
    uint32      PetTalentType;                              // 8
    //uint32    CategoryEnumID;                             // 9
    char*       Name;                                       // 10
    //char*     IconFile;                                   // 11
};

struct CreatureModelDataEntry
{
    uint32          ID;                                     // 0
    uint32          Flags;                                  // 1
    char*           ModelPath;                              // 2
    //uint32        SizeClass;                              // 3
    //float         ModelScale;                             // 4
    //uint32        BloodID;                                // 5
    //uint32        FootprintTextureID;                     // 6
    //float         FootprintTextureLength;                 // 7
    //float         FootprintTextureWidth;                  // 8
    //float         FootprintParticleScale;                 // 9
    //uint32        FoleyMaterialID;                        // 10
    //uint32        FootstepShakeSize;                      // 11
    //uint32        DeathThudShakeSize;                     // 12
    //uint32        SoundID;                                // 13
    //float         CollisionWidth;                         // 14
    float           CollisionHeight;                        // 15
    float           MountHeight;                            // 16
    //DBCPosition3D GeoBoxMin;                              // 17-19
    //DBCPosition3D GeoBoxMax;                              // 20-22
    //float         WorldEffectScale;                       // 23
    //float         AttachedEffectScale;                    // 24
    //float         MissileCollisionRadius;                 // 25
    //float         MissileCollisionPush;                   // 26
    //float         MissileCollisionRaise;                  // 27
    //float         OverrideLootEffectScale;                // 28
    //float         OverrideNameScale;                      // 29
    //float         OverrideSelectionRadius;                // 30
    //float         TamedPetBaseScale;                      // 31
    //uint32        CreatureGeosetDataID;                   // 32
    //float         HoverHeight;                            // 33
};

struct CreatureSpellDataEntry
{
    uint32      ID;                                         // 0        m_ID
    uint32      SpellID[MAX_CREATURE_SPELL_DATA_SLOT];      // 1-4      m_spells[4]
    //uint32    Availability[MAX_CREATURE_SPELL_DATA_SLOT]; // 4-7      m_availability[4]
};

struct CreatureTypeEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      Flags;                                      // 2 no exp? critters, non-combat pets, gas cloud.
};

struct CriteriaEntry
{
    uint32 ID;                                                  // 0
    uint32 Type;                                                // 1
    union
    {
        uint32 ID;
        // CRITERIA_TYPE_KILL_CREATURE          = 0
        // CRITERIA_TYPE_KILLED_BY_CREATURE     = 20
        uint32 CreatureID;

        // CRITERIA_TYPE_WIN_BG                 = 1
        // CRITERIA_TYPE_COMPLETE_BATTLEGROUND  = 15
        // CRITERIA_TYPE_DEATH_AT_MAP           = 16
        // CRITERIA_TYPE_WIN_ARENA              = 32
        // CRITERIA_TYPE_PLAY_ARENA             = 33
        // CRITERIA_TYPE_CHALLENGE_MODE         = 71
        uint32 MapID;

        // CRITERIA_TYPE_REACH_SKILL_LEVEL      = 7
        // CRITERIA_TYPE_LEARN_SKILL_LEVEL      = 40
        // CRITERIA_TYPE_LEARN_SKILLLINE_SPELLS = 75
        // CRITERIA_TYPE_LEARN_SKILL_LINE       = 112
        uint32 SkillID;

        // CRITERIA_TYPE_COMPLETE_ACHIEVEMENT   = 8
        uint32 AchievementID;

        // CRITERIA_TYPE_COMPLETE_QUESTS_IN_ZONE = 11
        uint32 ZoneID;

        // CRITERIA_TYPE_CURRENCY = 12
        uint32 CurrencyID;

        // CRITERIA_TYPE_DEATH_IN_DUNGEON       = 18
        // CRITERIA_TYPE_COMPLETE_RAID          = 19
        uint32 GroupSize;

        // CRITERIA_TYPE_UPDATED_CRITERIA_ID    = 21
        uint32 CriteriaID;

        // CRITERIA_TYPE_DEATHS_FROM            = 26
        uint32 DamageType;

        // CRITERIA_TYPE_COMPLETE_QUEST         = 27
        uint32 QuestID;

        // CRITERIA_TYPE_BE_SPELL_TARGET        = 28
        // CRITERIA_TYPE_BE_SPELL_TARGET2       = 69
        // CRITERIA_TYPE_CAST_SPELL             = 29
        // CRITERIA_TYPE_CAST_SPELL2            = 110
        // CRITERIA_TYPE_LEARN_SPELL            = 34
        uint32 SpellID;

        // CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE
        uint32 ObjectiveId;

        // CRITERIA_TYPE_HONORABLE_KILL_AT_AREA = 31
        uint32 AreaID;

        // CRITERIA_TYPE_OWN_ITEM               = 36
        // CRITERIA_TYPE_USE_ITEM               = 41
        // CRITERIA_TYPE_LOOT_ITEM              = 42
        // CRITERIA_TYPE_EQUIP_ITEM             = 57
        // CRITERIA_TYPE_OWN_TOY                = 185
        uint32 ItemID;

        // CRITERIA_TYPE_HIGHEST_TEAM_RATING    = 38
        // CRITERIA_TYPE_HIGHEST_PERSONAL_RATING = 39
        uint32 TeamType;

        // CRITERIA_TYPE_EXPLORE_AREA           = 43
        uint32 WorldMapOverlayID;

        // CRITERIA_TYPE_OWN_PVP_HK_RANK        = 44
        uint32 PvPHKRank;

        // CRITERIA_TYPE_GAIN_REPUTATION        = 46
        uint32 FactionID;

        // CRITERIA_TYPE_EQUIP_EPIC_ITEM        = 49
        uint32 ItemSlot;

        // CRITERIA_TYPE_ROLL_NEED_ON_LOOT      = 50
        // CRITERIA_TYPE_ROLL_GREED_ON_LOOT     = 51
        uint32 RollValue;

        // CRITERIA_TYPE_HK_CLASS               = 52
        uint32 ClassID;

        // CRITERIA_TYPE_HK_RACE                = 53
        uint32 RaceID;

        // CRITERIA_TYPE_DO_EMOTE               = 54
        uint32 EmoteID;

        // CRITERIA_TYPE_USE_GAMEOBJECT         = 68
        // CRITERIA_TYPE_FISH_IN_GAMEOBJECT     = 72
        uint32 GameObjectID;

        // CRITERIA_TYPE_HIGHEST_STAT           = 97
        uint32 StatType;

        // CRITERIA_TYPE_HIGHEST_SPELLPOWER     = 98
        uint32 SpellSchool;

        // CRITERIA_TYPE_LOOT_TYPE              = 109
        uint32 LootType;

        // CRITERIA_TYPE_OBTAIN_BATTLE_PET      = 96
        uint32 BattlePetID;

        // CRITERIA_TYPE_COMPLETE_SCENARIO      = 152
        uint32 ScenarioID;

        // CRITERIA_TYPE_LEVEL_BATTLE_PET       = 160
        uint32 BattlePetLevel;

    } Asset;                                                    // 2
    uint32 StartEvent;                                          // 3
    uint32 StartAsset;                                          // 4
    uint32 StartTimer;                                          // 5
    uint32 FailEvent;                                           // 6
    int32 FailAsset;                                            // 7
    uint32 ModifierTreeId;                                      // 8
    uint32 Flags;                                               // 9
    uint32 EligibilityWorldStateID;                             // 10
    uint32 EligibilityWorldStateValue;                          // 11
};

struct CriteriaTreeEntry
{
    uint32      ID;                                         // 0
    uint32      CriteriaID;                                 // 1
    uint32      CriteriaCount;                              // 2
    uint32      OrderIndex;                                 // 3
    uint32      Operator;                                   // 4
    uint32      ParentID;                                   // 5
    uint32      Flags;                                      // 6
    char*       Name;                                       // 7
};

struct ModifierTreeEntry
{
    uint32      ID;                                         // 0
    uint32      Type;                                       // 1
    uint32      Asset[2];                                   // 2-3
    uint32      Operator;                                   // 4
    uint32      Amount;                                     // 5
    uint32      Parent;                                     // 6
};

struct CurrencyTypesEntry
{
    uint32      ID;                                         // 0
    uint32      CategoryID;                                 // 1
    char*       Name;                                       // 2
    char*       InventoryIcon[2];                           // 3-4
    uint32      SpellWeight;                                // 5
    uint32      SpellCategory;                              // 6
    uint32      MaxQty;                                     // 7
    uint32      MaxEarnablePerWeek;                         // 8
    uint32      Flags;                                      // 9
    uint32      Quality;                                    // 10
    char*       Description;                                // 11

    // helpers
    uint32 GetFlags() const
    {
        return Flags;
    }
};

struct DestructibleModelDataEntry
{
    uint32      ID;                                         // 0
    struct
    {
        uint32  DisplayID;                                  // 1
        uint32  ImpactEffectDoodadSet;                      // 2
        uint32  AmbientDoodadSet;                           // 3
        uint32  NameSet;                                    // 4
    } StateDamaged;
    struct
    {
        uint32  DisplayID;                                  // 5
        uint32  DestructionDoodadSet;                       // 6
        uint32  ImpactEffectDoodadSet;                      // 7
        uint32  AmbientDoodadSet;                           // 8
        uint32  NameSet;                                    // 9
    } StateDestroyed;
    struct
    {
        uint32  DisplayID;                                  // 10
        uint32  DestructionDoodadSet;                       // 11
        uint32  ImpactEffectDoodadSet;                      // 12
        uint32  AmbientDoodadSet;                           // 13
        uint32  NameSet;                                    // 14
    } StateRebuilding;
    struct
    {
        uint32  DisplayID;                                  // 15
        uint32  InitDoodadSet;                              // 16
        uint32  AmbientDoodadSet;                           // 17
        uint32  NameSet;                                    // 18
    } StateSmoke;
    uint32      EjectDirection;                             // 19
    uint32      RepairGroundFx;                             // 20
    uint32      DoNotHighlight;                             // 21
    uint32      HealEffect;                                 // 22
    uint32      HealEffectSpeed;                            // 23
};

struct DifficultyEntry
{
    uint32      ID;                                         // 0
    uint32      FallbackDifficultyID;                       // 1
    uint32      InstanceType;                               // 2
    uint32      MinPlayers;                                 // 3
    uint32      MaxPlayers;                                 // 4
    int32       OldEnumValue;                               // 5
    uint32      Flags;                                      // 6
    uint32      ToggleDifficultyID;                         // 7
    //uint32    GroupSizeHealthCurveID;                     // 8
    //uint32    GroupSizeDmgCurveID;                        // 9
    //uint32    GroupSizeSpellPointsCurveID;                // 10
    //char const* Name;                                     // 11
};

struct DungeonEncounterEntry
{
    uint32      ID;                                         // 0
    uint32      MapID;                                      // 1
    uint32      DifficultyID;                               // 2
    uint32      OrderIndex;                                 // 3
    //uint32    Bit;                                        // 4
    char*       Name;                                       // 5
    //uint32    CreatureDisplayID;                          // 6
    //uint32    Flags;                                      // 7
    //uint32    Unk;                                        // 8 Flags2?
};

struct DurabilityCostsEntry
{
    uint32      ID;                                         // 0
    uint32      WeaponSubClassCost[21];                     // 1-22
    uint32      ArmorSubClassCost[8];                       // 23-30
};

struct DurabilityQualityEntry
{
    uint32      Id;                                         // 0
    float       QualityMod;                                 // 1
};

struct EmotesEntry
{
    uint32      ID;                                         // 0
    //char*     EmoteSlashCommand;                          // 1
    //uint32    AnimID;                                     // 2 ref to animationData
    uint32      EmoteFlags;                                 // 3 bitmask, may be unit_flags
    uint32      EmoteSpecProc;                              // 4 Can be 0, 1 or 2 (determine how emote are shown)
    uint32      EmoteSpecProcParam;                         // 5 uncomfirmed, may be enum UnitStandStateType
    //uint32    EmoteSoundID;                               // 6 ref to soundEntries
    //uint32    SpellVisualKitID                            // 7
};

struct EmotesTextEntry
{
    uint32      ID;                                         // 0
    //char*     Name;                                       // 1
    uint32      EmoteID;                                    // 2
    //uint32    EmoteText[16];                              // 3-18
};

struct EmotesTextSoundEntry
{
    uint32 Id;                                              // 0
    uint32 EmotesTextId;                                    // 1
    uint32 RaceId;                                          // 2
    uint32 SexId;                                           // 3, 0 male / 1 female
    uint32 SoundId;                                         // 4
};

struct FactionEntry
{
    uint32      ID;                                         // 0
    int32       ReputationIndex;                            // 1
    uint32      ReputationRaceMask[4];                      // 2-5
    uint32      ReputationClassMask[4];                     // 6-9
    int32       ReputationBase[4];                          // 10-13
    uint32      ReputationFlags[4];                         // 14-17
    uint32      ParentFactionID;                            // 18
    float       ParentFactionModIn;                         // 19 Faction gains incoming rep * ParentFactionModIn
    float       ParentFactionModOut;                        // 20 Faction outputs rep * ParentFactionModOut as spillover reputation
    uint32      ParentFactionCapIn;                         // 21 The highest rank the faction will profit from incoming spillover
    //uint32    ParentFactionCapOut;                        // 22
    char*       Name;                                       // 23
    //char*     Description;                                // 24
    uint32      Expansion;                                  // 25
    //uint32    Flags;                                      // 26
    //uint32    FriendshipRepID;                            // 27

    // helpers
    bool CanHaveReputation() const
    {
        return ReputationIndex >= 0;
    }
};

struct FactionTemplateEntry
{
    uint32      ID;                                         // 0
    uint32      Faction;                                    // 1
    uint32      Flags;                                      // 2
    uint32      Mask;                                       // 3 m_factionGroup
    uint32      FriendMask;                                 // 4 m_friendGroup
    uint32      EnemyMask;                                  // 5 m_enemyGroup
    uint32      Enemies[MAX_FACTION_RELATIONS];             // 6
    uint32      Friends[MAX_FACTION_RELATIONS];             // 10
    //-------------------------------------------------------  end structure

    // helpers
    bool IsFriendlyTo(FactionTemplateEntry const& entry) const
    {
        if (ID == entry.ID)
            return true;
        if (entry.Faction)
        {
            for (int i = 0; i < MAX_FACTION_RELATIONS; ++i)
                if (Enemies[i] == entry.Faction)
                    return false;
            for (int i = 0; i < MAX_FACTION_RELATIONS; ++i)
                if (Friends[i] == entry.Faction)
                    return true;
        }
        return (FriendMask & entry.Mask) || (Mask & entry.FriendMask);
    }
    bool IsHostileTo(FactionTemplateEntry const& entry) const
    {
        if (ID == entry.ID)
            return false;
        if (entry.Faction)
        {
            for (int i = 0; i < MAX_FACTION_RELATIONS; ++i)
                if (Enemies[i] == entry.Faction)
                    return true;
            for (int i = 0; i < MAX_FACTION_RELATIONS; ++i)
                if (Friends[i] == entry.Faction)
                    return false;
        }
        return (EnemyMask & entry.Mask) != 0;
    }
    bool IsHostileToPlayers() const { return (EnemyMask & FACTION_MASK_PLAYER) != 0; }
    bool IsNeutralToAll() const
    {
        for (int i = 0; i < MAX_FACTION_RELATIONS; ++i)
            if (Enemies[i] != 0)
                return false;
        return EnemyMask == 0 && FriendMask == 0;
    }
    bool IsContestedGuardFaction() const { return (Flags & FACTION_TEMPLATE_FLAG_CONTESTED_GUARD) != 0; }
};

struct FileDataEntry
{
    uint32      ID;                                         // 0
    char*       FileName;                                   // 1
    char*       FilePath;                                   // 2
};

struct GameObjectDisplayInfoEntry
{
    uint32          ID;                                     // 0
    char*           FileName;                               // 1
    //uint32        Sound[10];                              // 2-11
    DBCPosition3D   GeoBoxMin;                              // 12-14
    DBCPosition3D   GeoBoxMax;                              // 15-17
    //uint32        ObjectEffectPackageID;                  // 18
    //float         OverrideLootEffectScale;                // 19
    //float         OverrideNameScale;                      // 20
};

struct GameTablesEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      NumRows;                                    // 2
    uint32      NumColumns;                                 // 3
};

struct GemPropertiesEntry
{
    uint32      ID;                                         // 0
    uint32      EnchantID;                                  // 1
    //uint32    MaxCountInv;                                // 2
    //uint32    MaxCountItem;                               // 3
    uint32      Type;                                       // 4
    uint32      MinItemLevel;                               // 5
};

struct GlyphPropertiesEntry
{
    uint32      ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      Type;                                       // 2
    uint32      SpellIconID;                                // 3 GlyphIconId (SpellIcon.dbc)
};

struct GlyphSlotEntry
{
    uint32      ID;                                         // 0
    uint32      Type;                                       // 1
    uint32      Tooltip;                                    // 2
};

struct GtBarberShopCostBaseEntry
{
    //uint32    Level;
    float       Cost;
};

struct GtBattlePetTypeDamageModEntry
{
    //uint32    Level;
    float Modifier;
};

struct GtBattlePetXpEntry
{
    //uint32    Level;
    float Value;
};

struct GtCombatRatingsEntry
{
    //uint32    Level;
    float       Ratio;
};

struct GtChanceToMeleeCritBaseEntry
{
    //uint32    Level;
    float       Base;
};

struct GtChanceToMeleeCritEntry
{
    //uint32    Level;
    float       Ratio;
};

struct GtChanceToSpellCritBaseEntry
{
    float       Base;
};

struct GtNPCManaCostScalerEntry
{
    float       Ratio;
};

struct GtChanceToSpellCritEntry
{
    float       Ratio;
};

struct GtItemSocketCostPerLevelEntry
{
    float       Ratio;
};

struct GtOCTClassCombatRatingScalarEntry
{
    float       Ratio;
};

struct GtOCTHpPerStaminaEntry
{
    float       Ratio;
};

struct GtRegenHPPerSptEntry
{
    float       Ratio;
};

struct GtRegenMPPerSptEntry
{
    float       Ratio;
};

struct GtSpellScalingEntry
{
    float       Value;
};

struct GtOCTBaseHPByClassEntry
{
    float       Ratio;
};

struct GtOCTBaseMPByClassEntry
{
    float       Ratio;
};

struct GuildPerkSpellsEntry
{
    uint32      ID;                                         // 0
    uint32      GuildLevel;                                 // 1
    uint32      SpellID;                                    // 2
};

struct HolidaysEntry
{
    uint32      ID;                                         // 0
    uint32      Duration[MAX_HOLIDAY_DURATIONS];            // 1-10
    uint32      Date[MAX_HOLIDAY_DATES];                    // 11-36 (dates in unix time starting at January, 1, 2000)
    uint32      Region;                                     // 37
    uint32      Looping;                                    // 38
    uint32      CalendarFlags[MAX_HOLIDAY_FLAGS];           // 39-48
    uint32      HolidayNameID;                              // 49 HolidayNames.dbc
    uint32      HolidayDescriptionID;                       // 50 HolidayDescriptions.dbc
    char*       TextureFilename;                            // 51
    uint32      Priority;                                   // 52
    int32       CalendarFilterType;                         // 53
    uint32      Flags;                                      // 54 (0 = Darkmoon Faire, Fishing Contest and Wotlk Launch, rest is 1)
};

struct GuildColorBackgroundEntry
{
    uint32      ID;
    //uint8     Red;
    //uint8     Green;
    //uint8     Blue;
};

struct GuildColorBorderEntry
{
    uint32      ID;
    //uint8     Red;
    //uint8     Green;
    //uint8     Blue;
};

struct GuildColorEmblemEntry
{
    uint32      ID;
    //uint8     Red;
    //uint8     Green;
    //uint8     Blue;
};

struct ImportPriceArmorEntry
{
    uint32      ID;                                         // 0
    float       ClothFactor;                                // 1
    float       LeatherFactor;                              // 2
    float       MailFactor;                                 // 3
    float       PlateFactor;                                // 4
};

struct ImportPriceQualityEntry
{
    uint32      ID;                                         // 0
    float       Factor;                                     // 1
};

struct ImportPriceShieldEntry
{
    uint32      ID;                                         // 0
    float       Factor;                                     // 1
};

struct ImportPriceWeaponEntry
{
    uint32      ID;                                         // 0
    float       Factor;                                     // 1
};

struct ItemPriceBaseEntry
{
    uint32      ID;                                         // 0
    uint32      ItemLevel;                                  // 1 Item level (1 - 1000)
    float       ArmorFactor;                                // 2 Price factor for armor
    float       WeaponFactor;                               // 3 Price factor for weapons
};

struct ItemReforgeEntry
{
    uint32      ID;
    uint32      SourceStat;
    float       SourceMultiplier;
    uint32      FinalStat;
    float       FinalMultiplier;
};

struct ItemDamageEntry
{
    uint32      ID;                                         // 0 item level
    float       DPS[7];                                     // 1-7 multiplier for item quality
    uint32      ItemLevel;                                  // 8 item level
};

struct ItemArmorQualityEntry
{
    uint32      ID;                                         // 0 item level
    float       QualityMod[7];                              // 1-7 multiplier for item quality
    uint32      ItemLevel;                                  // 8 item level
};

struct ItemArmorShieldEntry
{
    uint32      ID;                                         // 0 item level
    uint32      ItemLevel;                                  // 1 item level
    float       Quality[7];                                 // 2-8 multiplier for item quality
};

struct ItemArmorTotalEntry
{
    uint32      ID;                                         // 0 item level
    uint32      ItemLevel;                                  // 1 item level
    float       Value[4];                                   // 2-5 multiplier for armor types (cloth...plate)
};

struct ItemClassEntry
{
    uint32      Class;                                      // 1 item class id
    //uint32    IsWeapon;                                   // 2 1 for weapon, 0 for everything else
    float       PriceFactor;                                // 3 used to calculate certain prices
    //char*     Name;                                       // 4 class name
};

struct ItemBagFamilyEntry
{
    uint32      ID;                                         // 0
    //char*     name;                                       // 1
};

struct ItemDisenchantLootEntry
{
    uint32      ID;                                         // 0
    uint32      ItemClass;                                  // 1
    int32       ItemSubClass;                               // 2
    uint32      ItemQuality;                                // 3
    uint32      MinItemLevel;                               // 4
    uint32      MaxItemLevel;                               // 5
    uint32      RequiredDisenchantSkill;                    // 6
};

struct ItemDisplayInfoEntry
{
    uint32      ID;                                         // 0
    char*       LeftModel;                                  // 1
    char*       RightModel;                                 // 2
    char*       LeftModelTexture;                           // 3
    char*       RightModelTexture;                          // 4
    char*       Icon1;                                      // 5
    char*       Icon2;                                      // 6
    uint32      GeosetGroup1;                               // 7
    uint32      GeosetGroup2;                               // 8
    uint32      GeosetGroup3;                               // 9
    uint32      Flags;                                      // 10
    uint32      SpellVisualID;                              // 11
    uint32      GroupSoundIndex;                            // 12
    uint32      MaleHelmetGeosetVis1;                       // 13
    uint32      FemaleHelmetGeosetVis2;                     // 14
    char*       UpperArmTexture;                            // 15
    char*       LowerArmTexture;                            // 16
    char*       HandsTexture;                               // 17
    char*       UpperTorsoTexture;                          // 18
    char*       LowerTorsoTexture;                          // 19
    char*       UpperLegTexture;                            // 20
    char*       LowerLegTexture;                            // 21
    char*       FootTexture;                                // 22
    char*       AccessoryTexture;                           // 23
    uint32      ItemVisual;                                 // 24
    uint32      ParticleColorID;                            // 25
};

struct ItemLimitCategoryEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      Quantity;                                   // 2
    uint32      Flags;                                      // 3

    uint32 GetMaxCount() const
    {
        return Quantity;
    }
};

struct ItemRandomPropertiesEntry
{
    uint32      ID;                                         // 0
    char*       InternalName;                               // 1
    uint32      Enchantment[MAX_ITEM_RANDOM_PROPERTIES];    // 2-6
    char*       Name;                                       // 7
};

struct ItemRandomSuffixEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    char*       InternalName;                               // 2
    uint32      Enchantment[MAX_ITEM_RANDOM_PROPERTIES];    // 3-7
    uint32      AllocationPct[MAX_ITEM_RANDOM_PROPERTIES];  // 8-12
};

struct ItemSetEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      ItemID[MAX_ITEM_SET_ITEMS];                 // 2-18
    uint32      SpellID[MAX_ITEM_SET_SPELLS];               // 19-26
    uint32      TriggerSpellID[MAX_ITEM_SET_SPELLS];        // 27-34
    uint32      RequiredSkill;                              // 35
    uint32      RequiredSkillRank;                          // 36
};

struct ItemSpecEntry
{
    uint32      ID;                                         // 0
    uint32      MinLevel;                                   // 1
    uint32      MaxLevel;                                   // 2
    uint32      ItemType;                                   // 3
    uint32      PrimaryStat;                                // 4
    uint32      SecondaryStat;                              // 5
    uint32      SpecID;                                     // 6
};

struct ItemSpecOverrideEntry
{
    uint32      ID;                                         // 0
    uint32      ItemID;                                     // 1
    uint32      SpecID;                                     // 2
};

struct LFGDungeonEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      MinLevel;                                   // 2
    uint32      MaxLevel;                                   // 3
    uint32      TargetLevel;                                // 4
    //uint32    TargetLevelMin;                             // 5
    //uint32    TargetLevelMax;                             // 6
    int32       MapID;                                      // 7
    uint32      DifficultyID;                               // 8
    uint32      Flags;                                      // 9
    uint32      Type;                                       // 10
    int32       Faction;                                    // 11
    //char*     TextureFileName;                            // 12
    uint32      Expansion;                                  // 13
    //uint32    OrderIndex;                                 // 14
    uint32      GroupID;                                    // 15
    //char*     Description;                                // 16
    //uint32    RandomID;                                   // 17
    //uint32    CountTank;                                  // 18
    //uint32    CountHealer;                                // 19
    //uint32    CountDamage;                                // 20
    //uint32    MinCountTank;                               // 21
    //uint32    MinCountHealer;                             // 22
    //uint32    MinCountDamage;                             // 23
    uint32      ScenarioID;                                 // 24
    uint32      SubType;                                    // 25
    //uint32    BonusReputationAmount;                      // 26
    //uint32    MentorCharLevel;                            // 27
    //uint32    MentorItemLevel;                            // 28

    // Helpers
    uint32 Entry() const { return ID + (Type << 24); }
};

struct LightEntry
{
    uint32          ID;                                     // 0
    uint32          MapID;                                  // 1
    DBCPosition3D   Pos;                                    // 2-4
    //float         FalloffStart;                           // 5
    //float         FalloffEnd;                             // 6
    //uint32        LightParamsID[8];                       // 7-14
};

struct LiquidTypeEntry
{
    uint32      ID;                                         // 0
    //char*     Name;                                       // 1
    //uint32    Flags;                                      // 2
    uint32      Type;                                       // 3 m_soundBank
    //uint32    SoundID;                                    // 4
    uint32      SpellID;                                    // 5
    //float     MaxDarkenDepth;                             // 6
    //float     FogDarkenIntensity;                         // 7
    //float     AmbDarkenIntensity;                         // 8
    //float     DirDarkenIntensity;                         // 9
    //uint32    LightID;                                    // 10
    //float     ParticleScale;                              // 11
    //uint32    ParticleMovement;                           // 12
    //uint32    ParticleTexSlots;                           // 13
    //uint32    MaterialID;                                 // 14
    //char*     Texture[6];                                 // 15-20
    //uint32    Color[2];                                   // 21-22
    //float     Float[18];                                  // 23-40
    //uint32    Int[4];                                     // 41-44
};

struct LockEntry
{
    uint32      ID;                                         // 0
    uint32      Type[MAX_LOCK_CASE];                        // 1-8
    uint32      Index[MAX_LOCK_CASE];                       // 9-16
    uint32      Skill[MAX_LOCK_CASE];                       // 17-24
    //uint32    Action[MAX_LOCK_CASE];                      // 25-32
};

struct PhaseEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      flag;                                       // 2
};

struct PhaseGroupEntry
{
    uint32      ID;
    uint32      PhaseId;
    uint32      GroupId;
};

struct MailTemplateEntry
{
    uint32      ID;                                         // 0
    //char*     Subject;                                    // 1
    char*       Content;                                    // 2
};

struct MapEntry
{
    uint32          ID;                                     // 0
    //char*         Directory;                              // 1
    uint32          InstanceType;                           // 2
    uint32          Flags;                                  // 3
    //uint32        MapType;                                // 4
    char*           MapName;                                // 5
    uint32          AreaTableID;                            // 6
    //char*         MapDescription0;                        // 7 Horde
    //char*         MapDescription1;                        // 8 Alliance
    uint32          LoadingScreenID;                        // 9 LoadingScreens.dbc
    //float         MinimapIconScale;                       // 10
    int32           CorpseMapID;                            // 11 map_id of entrance map in ghost mode (continent always and in most cases = normal entrance)
    DBCPosition2D   CorpsePos;                              // 12 entrance coordinates in ghost mode  (in most cases = normal entrance)
    //uint32        TimeOfDayOverride;                      // 14
    uint32          ExpansionID;                            // 15
    uint32          RaidOffset;                             // 16
    uint32          MaxPlayers;                             // 17
    int32           ParentMapID;                            // 18 related to phasing

    // Helpers
    uint32 Expansion() const { return ExpansionID; }

    bool IsInstance() const { return InstanceType == MAP_INSTANCE; }
    bool IsRaid() const { return InstanceType == MAP_RAID; }
    bool IsBattleground() const { return InstanceType == MAP_BATTLEGROUND; }
    bool IsBattleArena() const { return InstanceType == MAP_ARENA; }
    bool IsWorldMap() const { return InstanceType == MAP_COMMON; }
    bool IsScenario() const { return InstanceType == MAP_SCENARIO; }

    bool IsNonRaidDungeon() const { return IsInstance() || IsScenario(); }
    bool IsDungeon() const { return IsNonRaidDungeon() || IsRaid(); }
    bool IsBattlegroundOrArena() const { return IsBattleground() || IsBattleArena(); }
    bool Instanceable() const { return IsDungeon() || IsBattlegroundOrArena(); }

    bool GetEntrancePos(int32 &mapid, float &x, float &y) const
    {
        if (CorpseMapID < 0)
            return false;
        mapid = CorpseMapID;
        x = CorpsePos.X;
        y = CorpsePos.Y;
        return true;
    }

    bool IsContinent() const
    {
        return ID == 0 || ID == 1 || ID == 530 || ID == 571 || ID == 870;
    }

    bool IsDynamicDifficultyMap() const { return (Flags & MAP_FLAG_CAN_TOGGLE_DIFFICULTY) != 0; }
};

struct MapDifficultyEntry
{
    //uint32    ID;                                         // 0
    uint32      MapID;                                      // 1
    uint32      DifficultyID;                               // 2 (for arenas: arena slot)
    char*       Message;                                    // 3 m_message_lang (text showed when transfer to map failed)
    uint32      RaidDuration;                               // 4 m_raidDuration in secs, 0 if no fixed reset time
    uint32      MaxPlayers;                                 // 5 m_maxPlayers some heroic versions have 0 when expected same amount as in normal version
    uint32      LockID;                                     // 6

    bool HasMessage() const { return Message[0] != '\0'; }
};

struct MountCapabilityEntry
{
    uint32      ID;                                         // 0
    uint32      Flags;                                      // 1
    uint32      RequiredRidingSkill;                        // 2
    uint32      RequiredArea;                               // 3
    uint32      RequiredAura;                               // 4
    uint32      RequiredSpell;                              // 5
    uint32      SpeedModSpell;                              // 6
    int32       RequiredMap;                                // 7
};

struct MountTypeEntry
{
    uint32      ID;
    uint32      MountCapability[MAX_MOUNT_CAPABILITIES];
};

struct MovieEntry
{
    uint32      ID;                                         // 0 index
    //uint32    Volume;                                     // 1
    //uint32    KeyID;                                      // 2
    //uint32    AudioFileDataID;                            // 3
    //uint32    SubtitleFileDataID;                         // 4
};

struct NameGenEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    uint32      Race;                                       // 2
    uint32      Gender;                                     // 3
};

struct OverrideSpellDataEntry
{
    uint32      ID;                                         // 0
    uint32      SpellID[MAX_OVERRIDE_SPELL];                // 1-10
    uint32      Flags;                                      // 11
    int32       PlayerActionbarFileDataID;                  // 12
};

struct PlayerConditionEntry
{
    uint32      ID;                                         // 0
    uint32      Flags;                                      // 1
    uint32      MinLevel;                                   // 2
    uint32      MaxLevel;                                   // 3
    uint32      RaceMask;                                   // 4
    uint32      ClassMask;                                  // 5
    int32       Gender;                                     // 6
    int32       NativeGender;                               // 7
    uint32      SkillID[4];                                 // 8-11
    int32       MinSkill[4];                                // 12-15
    int32       MaxSkill[4];                                // 16-19
    uint32      SkillLogic;                                 // 20
    uint32      LanguageID;                                 // 21
    uint32      MinLanguage;                                // 22
    uint32      MaxLanguage;                                // 23
    uint32      MinFactionID[3];                            // 24-26
    uint32      MaxFactionID;                               // 27
    uint32      MinReputation[3];                           // 28-30
    uint32      MaxReputation;                              // 31
    uint32      ReputationLogic;                            // 32
    uint32      MinPVPRank;                                 // 33
    uint32      MaxPVPRank;                                 // 34
    uint32      PvpMedal;                                   // 35
    uint32      PrevQuestLogic;                             // 36
    uint32      PrevQuestID[4];                             // 37-40
    uint32      CurrQuestLogic;                             // 41
    uint32      CurrQuestID[4];                             // 42-45
    uint32      CurrentCompletedQuestLogic;                 // 46
    uint32      CurrentCompletedQuestID[4];                 // 47-50
    uint32      SpellLogic;                                 // 51
    uint32      SpellID[4];                                 // 52-55
    uint32      ItemLogic;                                  // 56
    uint32      ItemID[4];                                  // 57-60
    uint32      ItemCount[4];                               // 61-64
    uint32      ItemFlags;                                  // 65
    uint32      Explored[2];                                // 66-67
    uint32      Time[2];                                    // 68-69
    uint32      AuraSpellLogic;                             // 70
    uint32      AuraSpellID[4];                             // 71-74
    uint32      WorldStateExpressionID;                     // 75
    uint32      WeatherID;                                  // 76
    uint32      PartyStatus;                                // 77
    uint32      LifetimeMaxPVPRank;                         // 78
    uint32      AchievementLogic;                           // 79
    uint32      Achievement[4];                             // 80-83
    uint32      LfgLogic;                                   // 84
    uint32      LfgStatus[4];                               // 85-88
    uint32      LfgCompare[4];                              // 89-92
    uint32      LfgValue[4];                                // 93-96
    uint32      AreaLogic;                                  // 97
    uint32      AreaID[4];                                  // 98-101
    uint32      CurrencyLogic;                              // 102
    uint32      CurrencyID[4];                              // 103-106
    uint32      CurrencyCount[4];                           // 107-110
    uint32      QuestKillID;                                // 111
    uint32      QuestKillLogic;                             // 112
    uint32      QuestKillMonster[4];                        // 113-116
    int32       MinExpansionLevel;                          // 117
    int32       MaxExpansionLevel;                          // 118
    int32       MinExpansionTier;                           // 119
    int32       MaxExpansionTier;                           // 120
    uint32      MinGuildLevel;                              // 121
    uint32      MaxGuildLevel;                              // 122
    uint32      PhaseUseFlags;                              // 123
    uint32      PhaseID;                                    // 124
    uint32      PhaseGroupID;                               // 125
    uint32      MinAvgItemLevel;                            // 126
    uint32      MaxAvgItemLevel;                            // 127
    uint32      MinAvgEquippedItemLevel;                    // 128
    uint32      MaxAvgEquippedItemLevel;                    // 139
    int32       ChrSpecializationIndex;                     // 130
    int32       ChrSpecializationRole;                      // 131
    char*       FailureDescription;                         // 132
    int32       PowerType;                                  // 133
    int32       PowerTypeComp;                              // 134
    int32       PowerTypeValue;                             // 135
};

struct PowerDisplayEntry
{
    uint32      ID;                                         // 0
    uint32      PowerType;                                  // 1
    //char*     GlobalStringBaseTag;                        // 2
    //uint8     Red;                                        // 3
    //uint8     Green;                                      // 4
    //uint8     Blue;                                       // 5
};

struct PvPDifficultyEntry
{
    //uint32    ID;                                         // 0
    uint32      MapID;                                      // 1
    uint32      BracketID;                                  // 2 m_rangeIndex
    uint32      MinLevel;                                   // 3
    uint32      MaxLevel;                                   // 4
    uint32      Difficulty;                                 // 5

    // helpers
    BattlegroundBracketId GetBracketId() const { return BattlegroundBracketId(BracketID); }
};

struct QuestMoneyRewardEntry
{
    uint32      Level;                                      // 0
    uint32      Money[10];                                  // 1
};

struct QuestSortEntry
{
    uint32      ID;                                         // 0
    char*       SortName;                                   // 1
};

struct QuestV2Entry
{
    uint32      ID;                                         // 0
    uint32      UniqueBitFlag;                              // 1
};

struct QuestXPEntry
{
    uint32      ID;                                         // 0
    uint32      XP[10];                                     // 1
};

struct QuestFactionRewEntry
{
    uint32      ID;                                         // 0
    int32       QuestRewFactionValue[10];                   // 1-10
};

struct QuestPOIPointEntry
{
    //uint32        ID;                                     // 0
    int32           POIPosX;                                // 1
    int32           POIPosY;                                // 2
    uint32          BlobID;                                 // 3
};

struct RandomPropertiesPointsEntry
{
    uint32      ID;                                         // 0
    uint32      EpicPropertiesPoints[5];                    // 1-5
    uint32      RarePropertiesPoints[5];                    // 6-10
    uint32      UncommonPropertiesPoints[5];                // 11-15
};

struct ResearchBranchEntry
{
    uint32      ID;                                         // 0
    //char*     BranchName;                                 // 1
    //uint32    ResearchFieldID;                            // 2 research field (from ResearchField.dbc)
    //uint32    FragmentCurrencyID;                         // 3
    //char*     Icon;                                       // 4
    //uint32    KeystoneItemID;                             // 5
};

struct ResearchProjectEntry
{
    uint32      ID;                                         // 0
    //char*     ProjectName;                                // 1
    //char*     ProjectDescription;                         // 2
    uint32      Rarity;                                     // 3 0-common, 1-rare
    uint32      ResearchBranchID;                           // 4 branch id (from ResearchBranch.dbc)
    //uint32    SpellID;                                    // 5
    //uint32    KeystoneCount;                              // 6
    //char*     ProjectIcon;                                // 7
    uint32      RequiredFragmentCount;                      // 8
};

struct ResearchSiteEntry
{
    uint32      ID;                                         // 0
    uint32      MapID;                                      // 1
    uint32      QuestPOIBlobID;                             // 2 blob id (from QuestPOIBlob.dbc)
    //char*     SiteName;                                   // 3
    //uint32    IconID;                                     // 4
};

struct ScalingStatDistributionEntry
{
    uint32      ID;                                         // 0
    int32       StatMod[10];                                // 1-10
    uint32      Modifier[10];                               // 11-20
    uint32      MinLevel;                                   // 21
    uint32      MaxLevel;                                   // 22
};

struct ScalingStatValuesEntry
{
    uint32      ID;                                         // 0
    uint32      Level;                                      // 1
    uint32      DPSMod[7];                                  // 2-8 DPS mod for level
    uint32      SpellPower;                                 // 9 spell power for level
    uint32      StatMultiplier[5];                          // 10-11 Multiplier for ScalingStatDistribution
    uint32      Armor[8][4];                                // 12-47 Armor for level
    uint32      CloakArmor;                                 // 48 armor for cloak
    uint32      Unk;                                        // 49

    uint32 GetStatMultiplier(uint32 inventoryType) const;
    uint32 GetArmor(uint32 inventoryType, uint32 armorType) const;
    uint32 GetDPSAndDamageMultiplier(uint32 subClass, bool isCasterWeapon, float* damageMultiplier) const;
};

struct ScenarioEntry
{
    uint32      ID;
    char*       Name;
    uint32      Flags;
};

struct ScenarioStepEntry
{
    uint32      ID;
    uint32      CriteriaTreeID;
    uint32      ScenarioID;
    uint32      Step;
    char*       Description;
    char*       Name;
    uint32      BonusRequiredStepID;        // Bonus step can only be completed if scenario is in the step specified in this field

    // Helpers
    bool IsBonusObjective() const
    {
        return BonusRequiredStepID > 0;
    }
};

struct SkillLineEntry
{
    uint32      ID;                                         // 0        m_ID
    int32       CategoryID;                                 // 1        m_categoryID
    char*       DisplayName;                                // 2        m_displayName_lang
    //char*     Description;                                // 3        m_description_lang
    uint32      SpellIconID;                                // 4        m_spellIconID
    //char*     AlternateVerb;                              // 5        m_alternateVerb_lang
    uint32      CanLink;                                    // 6        m_canLink (prof. with recipes)
    //uint32    ParentSkillLineID;                          // 7
    //uint32    Flags;                                      // 8
};

struct SkillLineAbilityEntry
{
    uint32      ID;                                         // 0        m_ID
    uint32      SkillLine;                                  // 1        m_skillLine
    uint32      SpellID;                                    // 2        m_spell
    uint32      RaceMask;                                   // 3        m_raceMask
    uint32      ClassMask;                                  // 4        m_classMask
    uint32      MinSkillLineRank;                           // 5        m_minSkillLineRank
    uint32      SupercedesSpell;                            // 6        m_supercededBySpell
    uint32      AquireMethod;                               // 7        m_acquireMethod
    uint32      TrivialSkillLineRankHigh;                   // 8        m_trivialSkillLineRankHigh
    uint32      TrivialSkillLineRankLow;                    // 9        m_trivialSkillLineRankLow
    uint32      NumSkillUps;                                // 10
    uint32      UniqueBit;                                  // 11
    uint32      TradeSkillCategoryID;                       // 12
};

struct SkillRaceClassInfoEntry
{
    //uint32    ID;                                         // 0      m_ID
    uint32      SkillId;                                    // 1      m_skillID
    int32       RaceMask;                                   // 2      m_raceMask
    int32       ClassMask;                                  // 3      m_classMask
    uint32      Flags;                                      // 4      m_flags
    uint32      Availability;                               // 5
    uint32      MinLevel;                                   // 6
    uint32      SkillTierID;                                // 7

    // helpers
    uint32 GetFlags() const
    {
        return Flags;
    }
};

struct SkillTiersEntry
{
    uint32      Id;                                         // 0
    uint32      MaxSkill[MAX_SKILL_STEP];                   // 1-16
};

struct SoundEntriesEntry
{
    uint32      ID;                                         // 0
    uint32      SoundType;                                  // 1
    char*       Name;                                       // 2
    uint32      FileDataID[10];                             // 3-12
    uint32      Freq[10];                                   // 13-22
    float       VolumeFloat;                                // 23
    uint32      Flags;                                      // 24
    float       MinDistance;                                // 25
    float       DistanceCutoff;                             // 26
    uint32      EAXDef;                                     // 27
    uint32      SoundEntriesAdvancedID;                     // 28
    float       VolumeVariationPlus;                        // 29
    float       VolumeVariationMinus;                       // 30
    float       PitchVariationPlus;                         // 31
    float       PitchVariationMinus;                        // 32
    float       PitchAdjust;                                // 33
    uint32      DialogType;                                 // 34
};

struct SpecializationSpellsEntry
{
    uint32      Id;                                         // 0
    uint32      SpecID;                                     // 1
    uint32      SpellId;                                    // 2
    uint32      OverridesSpellID;                           // 3
    //char*     Description;                                // 5
};

struct SpellEffectEntry
{
    uint32      Id;                                         // 0
    uint32      EffectDifficulty;                           // 1
    uint32      Effect;                                     // 2
    float       EffectAmplitude;                            // 3
    uint32      EffectAura;                                 // 4
    uint32      EffectAuraPeriod;                           // 5
    int32       EffectBasePoints;                           // 6
    float       EffectBonusCoefficient;                     // 7
    float       EffectChainAmplitude;                       // 8
    uint32      EffectChainTargets;                         // 9
    uint32      EffectDieSides;                             // 10
    uint32      EffectItemType;                             // 11
    uint32      EffectMechanic;                             // 12
    int32       EffectMiscValue;                            // 13
    int32       EffectMiscValueB;                           // 14
    float       EffectPointsPerResource;                    // 15
    uint32      EffectRadiusIndex;                          // 16
    uint32      EffectRadiusMaxIndex;                       // 17
    float       EffectRealPointsPerLevel;                   // 18
    flag128     EffectSpellClassMask;                       // 19-22
    uint32      EffectTriggerSpell;                         // 23
    float       EffectPosFacing;                            // 24
    uint32      ImplicitTarget[2];                          // 25-26
    uint32      SpellID;                                    // 27
    uint32      EffectIndex;                                // 28
    uint32      EffectAttributes;                           // 29
};

struct SpellAuraOptionsEntry
{
    uint32      ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    uint32      CumulativeAura;                             // 3
    uint32      ProcChance;                                 // 4
    uint32      ProcCharges;                                // 5
    uint32      ProcTypeMask;                               // 6
    uint32      ProcCategoryRecovery;                       // 7
    uint32      SpellProcsPerMinuteID;                      // 8
};

struct SpellAuraRestrictionsEntry
{
    uint32      ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    uint32      CasterAuraState;                            // 3
    uint32      TargetAuraState;                            // 4
    uint32      ExcludeCasterAuraState;                     // 5
    uint32      ExcludeTargetAuraState;                     // 6
    uint32      CasterAuraSpell;                            // 7
    uint32      TargetAuraSpell;                            // 8
    uint32      ExcludeCasterAuraSpell;                     // 9
    uint32      ExcludeTargetAuraSpell;                     // 10
};

struct SpellCastingRequirementsEntry
{
    uint32      ID;                                         // 0
    uint32      FacingCasterFlags;                          // 1
    uint32      MinFactionID;                               // 1
    uint32      MinReputation;                              // 3
    uint32      RequiredAreasID;                            // 4
    uint32      RequiredAuraVision;                         // 5
    uint32      RequiresSpellFocus;                         // 6
};

struct SpellTotemsEntry
{
    uint32      ID;                                         // 0        m_ID
    uint32      TotemCategory[MAX_SPELL_TOTEMS];            // 1        m_requiredTotemCategoryID
    uint32      Totem[MAX_SPELL_TOTEMS];                    // 2        m_totem
};

struct SpellEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1
    //char*     NameSubtext;                                // 2
    //char*     Description;                                // 3
    //char*     AuraDescription;                            // 4
    uint32      RuneCostID;                                 // 5
    uint32      SpellMissileID;                             // 6
    uint32      DescriptionVariablesID;                     // 7
    float       BonusCoefficientFromAP;                     // 8
    uint32      ScalingID;                                  // 9
    uint32      AuraOptionsID;                              // 10
    uint32      AuraRestrictionsID;                         // 11
    uint32      CastingRequirementsID;                      // 12
    uint32      CategoriesID;                               // 13
    uint32      ClassOptionsID;                             // 14
    uint32      CooldownsID;                                // 15
    uint32      EquippedItemsID;                            // 16
    uint32      InterruptsID;                               // 17
    uint32      LevelsID;                                   // 18
    uint32      ReagentsID;                                 // 19
    uint32      ShapeshiftID;                               // 20
    uint32      TargetRestrictionsID;                       // 21
    uint32      TotemsID;                                   // 22
    uint32      RequiredProjectID;                          // 23
    uint32      MiscID;                                     // 24
};

struct SpellLearnSpellEntry
{
    uint32      ID;                                         // 0
    uint32      LearnSpellID;                               // 1
    uint32      SpellID;                                    // 2
    uint32      OverridesSpellID;                           // 3
};

struct SpellMiscEntry
{
    uint32      ID;                                         // 0        m_ID
    //uint32    SpellId                                     // 1
    //uint32    Unk                                         // 2
    uint32      Attributes;                                 // 3        m_attribute
    uint32      AttributesEx;                               // 4        m_attributesEx
    uint32      AttributesEx2;                              // 5        m_attributesExB
    uint32      AttributesEx3;                              // 6        m_attributesExC
    uint32      AttributesEx4;                              // 7        m_attributesExD
    uint32      AttributesEx5;                              // 8        m_attributesExE
    uint32      AttributesEx6;                              // 9        m_attributesExF
    uint32      AttributesEx7;                              // 10       m_attributesExG
    uint32      AttributesEx8;                              // 11       m_attributesExH
    uint32      AttributesEx9;                              // 12       m_attributesExI
    uint32      AttributesEx10;                             // 13       m_attributesExJ
    uint32      AttributesEx11;                             // 14       m_attributesExK
    uint32      AttributesEx12;                             // 15       m_attributesExL
    uint32      AttributesEx13;                             // 16       m_attributesExM
    uint32      CastingTimeIndex;                           // 17       m_castingTimeIndex
    uint32      DurationIndex;                              // 18       m_durationIndex
    uint32      RangeIndex;                                 // 19       m_rangeIndex
    float       Speed;                                      // 20       m_speed
    uint32      SpellVisual[2];                             // 21-22    m_spellVisualID
    uint32      SpellIconID;                                // 23       m_spellIconID
    uint32      ActiveIconID;                               // 24       m_activeIconID
    uint32      SchoolMask;                                 // 25       m_schoolMask
};

struct SpellEffectScalingEntry
{
    uint32      ID;                                         // 0
    float       Coefficient;                                // 1
    float       Variance;                                   // 2
    float       ResourceCoefficient;                        // 3
    //float     UnkMultiplier                               // 4
    uint32      SpellEffectId;                              // 5
};

struct SpellCategoriesEntry
{
    uint32      ID;                                         // 0        m_ID
    uint32      SpellID;                                    // 1        m_spellId
    uint32      DifficultyID;                               // 2        m_difficulty
    uint32      Category;                                   // 3        m_category
    uint32      DmgClass;                                   // 4        m_defenseType
    uint32      Dispel;                                     // 5        m_dispelType
    uint32      Mechanic;                                   // 6        m_mechanic
    uint32      PreventionType;                             // 7        m_preventionType
    uint32      StartRecoveryCategory;                      // 8        m_startRecoveryCategory
    uint32      ChargeCategory;                             // 9        m_chargeCategory
};

struct SpellCastTimesEntry
{
    uint32      ID;                                         // 0
    int32       CastTime;                                   // 1
    int32       CastTimePerLevel;                           // 2
    int32       MinCastTime;                                // 3
};

struct SpellCategoryEntry
{
    uint32      ID;                                         // 0
    uint32      Flags;                                      // 1
    //uint8     UsesPerWeek;                                // 2
    //char*     Name;                                       // 3
    int32       MaxCharges;                                 // 4
    int32       ChargeRecoveryTime;                         // 5

    // helpers
    uint32 GetFlags() const
    {
        return Flags;
    }
};

struct SpellFocusObjectEntry
{
    uint32      ID;                                         // 0
    //char*     Name;                                       // 1 m_name_lang
};

struct SpellProcsPerMinuteEntry
{
    uint32 ID;
    float BaseProcRate;
    uint32 Flags;
};

struct SpellProcsPerMinuteModEntry
{
    uint32 ID;
    uint32 Type;
    uint32 Param;
    float Coeff;
    uint32 SpellProcsPerMinuteID;
};

struct SpellRadiusEntry
{
    uint32      ID;                                         // 0
    float       Radius;                                     // 1
    float       RadiusPerLevel;                             // 2
    float       RadiusMin;                                  // 3
    float       RadiusMax;                                  // 4
};

struct SpellRangeEntry
{
    uint32      ID;                                         // 0
    float       MinRangeHostile;                            // 1
    float       MinRangeFriend;                             // 2
    float       MaxRangeHostile;                            // 3
    float       MaxRangeFriend;                             // 4
    uint32      Flags;                                      // 5
    char*       DisplayName;                                // 6
    char*       DisplayNameShort;                           // 7
};

struct SpellEquippedItemsEntry
{
    //uint32    ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    int32       EquippedItemClass;                          // 3       m_equippedItemClass (value)
    int32       EquippedItemInventoryTypeMask;              // 4       m_equippedItemInvTypes (mask)
    int32       EquippedItemSubClassMask;                   // 5       m_equippedItemSubclass (mask)
};

struct SpellCooldownsEntry
{
    //uint32    ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    uint32      CategoryRecoveryTime;                       // 3
    uint32      RecoveryTime;                               // 4
    uint32      StartRecoveryTime;                          // 5
};

struct SpellClassOptionsEntry
{
    uint32      ID;                                         // 0
    uint32      ModalNextSpell;                             // 1
    flag128     SpellFamilyFlags;                           // 2-5
    uint32      SpellFamilyName;                            // 6
};

struct SpellInterruptsEntry
{
    //uint32    ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    uint32      AuraInterruptFlags[2];                      // 3-4
    uint32      ChannelInterruptFlags[2];                   // 5-6
    uint32      InterruptFlags;                             // 7
};

struct SpellKeyboundOverrideEntry
{
    uint32      ID;                                         // 0
    char*       Function;                                   // 1
};

struct SpellLevelsEntry
{
    //uint32    ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    uint32      BaseLevel;                                  // 3
    uint32      MaxLevel;                                   // 4
    uint32      SpellLevel;                                 // 5
};

struct SpellPowerEntry
{
    uint32      ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    uint32      PowerType;                                  // 3
    int32       ManaCost;                                   // 4
    uint32      ManaCostPerlevel;                           // 5
    uint32      ManaCostPerSecond;                          // 6
    uint32      ManaCostAdditional;                         // 7    Spell uses [ManaCost, ManaCost+ManaCostAdditional] power - affects tooltip parsing as multiplier on SpellEffectEntry::EffectPointsPerResource
                                                            //      only SPELL_EFFECT_WEAPON_DAMAGE_NOSCHOOL, SPELL_EFFECT_WEAPON_PERCENT_DAMAGE, SPELL_EFFECT_WEAPON_DAMAGE, SPELL_EFFECT_NORMALIZED_WEAPON_DMG
    uint32      PowerDisplayId;                             // 8
    float       ManaCostPercentage;                         // 9
    float       ManaCostPercentagePerSecond;                // 10
    uint32      RequiredAura;                               // 11
    float       HealthCostPercentage;                       // 12
};

struct SpellRuneCostEntry
{
    uint32      ID;                                         // 0
    uint32      RuneCost[4];                                // 1-4 (0=blood, 1=unholy, 2=frost, 3=death)
    uint32      RunicPower;                                 // 5

    bool NoRuneCost() const { return RuneCost[0] == 0 && RuneCost[1] == 0 && RuneCost[2] == 0 && RuneCost[3] == 0; }
    bool NoRunicPowerGain() const { return RunicPower == 0; }
};

struct SpellShapeshiftFormEntry
{
    uint32      ID;                                         // 0
    //uint32    BonusActionBar;                             // 1
    //char*     Name;                                       // 2
    uint32      Flags;                                      // 3
    int32       CreatureType;                               // 4 <=0 humanoid, other normal creature types
    //uint32    AttackIconID;                               // 5 unused, related to next field
    uint32      CombatRoundTime;                            // 6
    uint32      CreatureDisplayID[4];                       // 7-10 (0 - Alliance, 1 - Horde)
    uint32      PresetSpellID[MAX_SHAPESHIFT_SPELLS];       // 11-18 spells which appear in the bar after shapeshifting
    //uint32    MountTypeID;                                // 19
    //uint32    ExitSoundEntriesID;                         // 20
};

struct SpellShapeshiftEntry
{
    uint32      ID;                                         // 0
    uint32      ShapeshiftExclude[2];                       // 1-2
    uint32      ShapeshiftMask[2];                          // 3-4
    //uint32    StanceBarOrder;                             // 5
};

struct SpellTargetRestrictionsEntry
{
    uint32      ID;                                         // 0
    uint32      SpellID;                                    // 1
    uint32      DifficultyID;                               // 2
    float       ConeAngle;                                  // 3
    float       Width;                                      // 4
    uint32      MaxAffectedTargets;                         // 5
    uint32      MaxTargetLevel;                             // 6
    uint32      TargetCreatureType;                         // 7
    uint32      Targets;                                    // 8
};

struct SpellScalingEntry
{
    uint32      ID;                                         // 0
    int32       CastTimeMin;                                // 1
    int32       CastTimeMax;                                // 2
    uint32      CastTimeMaxLevel;                           // 3
    int32       ScalingClass;                               // 4
    float       NerfFactor;                                 // 5
    uint32      NerfMaxLevel;                               // 6
    uint32      MaxScalingLevel;                            // 7
    uint32      ScalesFromItemLevel;                        // 8
};

struct SpellDurationEntry
{
    uint32      ID;
    int32       Duration[3];
};

struct SpellItemEnchantmentEntry
{
    uint32      ID;                                         // 0
    uint32      Charges;                                    // 1
    uint32      Effect[MAX_ITEM_ENCHANTMENT_EFFECTS];       // 2-4
    uint32      EffectPointsMin[MAX_ITEM_ENCHANTMENT_EFFECTS]; // 5-7
    uint32      EffectSpellID[MAX_ITEM_ENCHANTMENT_EFFECTS]; // 8-10
    char*       Name;                                       // 11
    uint32      ItemVisual;                                 // 12
    uint32      Flags;                                      // 13
    uint32      SRCItemID;                                  // 14
    uint32      ConditionID;                                // 15
    uint32      RequiredSkillID;                            // 16
    uint32      RequiredSkillRank;                          // 17
    uint32      MinLevel;                                   // 18
    uint32      MaxLevel;                                   // 19
    uint32      ItemLevel;                                  // 20
    int32       ScalingClass;                               // 21
    int32       ScalingClassRestricted;                     // 22
    float       EffectScalingPoints[MAX_ITEM_ENCHANTMENT_EFFECTS]; //23-25

    // helpers
    uint32 GetRequiredSkill() const
    {
        return RequiredSkillID;
    }

    uint32 GetRequiredSkillValue() const
    {
        return RequiredSkillRank;
    }
};

struct SpellItemEnchantmentConditionEntry
{
    uint32      ID;                                         // 0
    uint8       LTOperandType[5];                           // 1-5
    uint32      LTOperand[5];                               // 6-10
    uint8       Operator[5];                                // 11-15
    uint8       RTOperandType[5];                           // 16-20
    uint32      RTOperand[5];                               // 21-25
    uint8       Logic[5];                                   // 26-30
};

struct SummonPropertiesEntry
{
    uint32      Id;                                         // 0
    uint32      Category;                                   // 1, 0 - can't be controlled?, 1 - something guardian?, 2 - pet?, 3 - something controllable?, 4 - taxi/mount?
    uint32      Faction;                                    // 2, 14 rows > 0
    uint32      Type;                                       // 3, see enum
    int32       Slot;                                       // 4, 0-6
    uint32      Flags;                                      // 5
};

struct TalentEntry
{
    uint32      ID;                                         // 0
    uint32      SpecID;                                     // 1 0 - any specialization
    uint32      TierID;                                     // 2 0-5
    uint32      ColumnIndex;                                // 3 0-2
    uint32      SpellID;                                    // 4
    uint32      Flags;                                      // 5 All 0
    uint32      CategoryMask[2];                            // 6-7 All 0
    uint32      ClassID;                                    // 8
    uint32      OverridesSpellID;                           // 9 spellid that is replaced by talent
    //char*     Description                                 // 10
};

struct TaxiNodesEntry
{
    uint32          ID;                                     // 0
    uint32          MapID;                                  // 1
    DBCPosition3D   Pos;                                    // 2-4
    char*           Name;                                   // 5
    uint32          MountCreatureID[2];                     // 6-7
    uint32          ConditionID;                            // 8
    uint32          Flags;                                  // 9
    DBCPosition2D   MapOffset;                              // 10-11
};

struct TaxiPathEntry
{
    uint32      ID;                                         // 0
    uint32      From;                                       // 1
    uint32      To;                                         // 2
    uint32      Cost;                                       // 3
};

struct TaxiPathNodeEntry
{
                                                            // 0        m_ID
    uint32          Path;                                   // 1        m_PathID
    uint32          Index;                                  // 2        m_NodeIndex
    uint32          MapID;                                  // 3        m_ContinentID
    DBCPosition3D   Loc;                                    // 4-6      m_Loc
    uint32          ActionFlag;                             // 7        m_flags
    uint32          Delay;                                  // 8        m_delay
    uint32          ArrivalEventID;                         // 9        m_arrivalEventID
    uint32          DepartureEventID;                       // 10       m_departureEventID
};

struct TotemCategoryEntry
{
    uint32      ID;                                         // 0
    char*       Name;                                       // 1        m_name_lang
    uint32      categoryType;                               // 2        m_totemCategoryType (one for specialization)
    uint32      categoryMask;                               // 3        m_totemCategoryMask (compatibility mask for same type: different for totems, compatible from high to low for rods)
};

struct UnitPowerBarEntry
{
    uint32      ID;                                         // 0
    uint32      MinPower;                                   // 1
    uint32      MaxPower;                                   // 2
    uint32      StartPower;                                 // 3
    uint32      CenterPower;                                // 4
    float       RegenerationPeace;                          // 5
    float       RegenerationCombat;                         // 6
    uint32      BarType;                                    // 7
    uint32      FileDataID[6];                              // 8-13
    uint32      Color[6];                                   // 14-19
    uint32      Flags;                                      // 20
    char*       Name;                                       // 21
    char*       Cost;                                       // 22
    char*       OutOfError;                                 // 23
    char*       ToolTip;                                    // 24
    float       StartInset;                                 // 25
    float       EndInset;                                   // 26
};

struct TransportAnimationEntry
{
    uint32          ID;                                     // 0
    uint32          TransportID;                            // 1
    uint32          TimeIndex;                              // 2
    DBCPosition3D   Pos;                                    // 3-5
    uint32          SequenceID;                             // 6
};

struct TransportRotationEntry
{
    uint32      ID;                                         // 0
    uint32      TransportID;                                // 1
    uint32      TimeIndex;                                  // 2
    float       X;                                          // 3
    float       Y;                                          // 4
    float       Z;                                          // 5
    float       W;                                          // 6
};

struct VehicleEntry
{
    uint32      ID;                                         // 0
    uint32      Flags;                                      // 1
    uint32      FlagsB;                                     // 2
    float       TurnSpeed;                                  // 3
    float       PitchSpeed;                                 // 4
    float       PitchMin;                                   // 5
    float       PitchMax;                                   // 6
    uint32      SeatID[MAX_VEHICLE_SEATS];                  // 7-14
    float       MouseLookOffsetPitch;                       // 15
    float       CameraFadeDistScalarMin;                    // 16
    float       CameraFadeDistScalarMax;                    // 17
    float       CameraPitchOffset;                          // 18
    float       FacingLimitRight;                           // 19
    float       FacingLimitLeft;                            // 20
    float       MsslTrgtTurnLingering;                      // 21
    float       MsslTrgtPitchLingering;                     // 22
    float       MsslTrgtMouseLingering;                     // 23
    float       MsslTrgtEndOpacity;                         // 24
    float       MsslTrgtArcSpeed;                           // 25
    float       MsslTrgtArcRepeat;                          // 26
    float       MsslTrgtArcWidth;                           // 27
    float       MsslTrgtImpactRadius[2];                    // 28-29
    //char*     MsslTrgtArcTexture;                         // 30
    //char*     MsslTrgtImpactTexture;                      // 31
    //char*     MsslTrgtImpactModel;                        // 32-33
    float       CameraYawOffset;                            // 34
    uint32      UILocomotionType;                           // 35
    float       MsslTrgtImpactTexRadius;                    // 36
    uint32      VehicleUIIndicatorID;                       // 37
    uint32      PowerDisplayID[3];                          // 38-40
};

struct VehicleSeatEntry
{
    uint32          ID;                                     // 0
    uint32          Flags;                                  // 1
    int32           AttachmentID;                           // 2
    DBCPosition3D   AttachmentOffset;                       // 3-5
    float           EnterPreDelay;                          // 6
    float           EnterSpeed;                             // 7
    float           EnterGravity;                           // 8
    float           EnterMinDuration;                       // 9
    float           EnterMaxDuration;                       // 10
    float           EnterMinArcHeight;                      // 11
    float           EnterMaxArcHeight;                      // 12
    int32           EnterAnimStart;                         // 13
    int32           EnterAnimLoop;                          // 14
    int32           RideAnimStart;                          // 15
    int32           RideAnimLoop;                           // 16
    int32           RideUpperAnimStart;                     // 17
    int32           RideUpperAnimLoop;                      // 18
    float           ExitPreDelay;                           // 19
    float           ExitSpeed;                              // 20
    float           ExitGravity;                            // 21
    float           ExitMinDuration;                        // 22
    float           ExitMaxDuration;                        // 23
    float           ExitMinArcHeight;                       // 24
    float           ExitMaxArcHeight;                       // 25
    int32           ExitAnimStart;                          // 26
    int32           ExitAnimLoop;                           // 27
    int32           ExitAnimEnd;                            // 28
    float           PassengerYaw;                           // 29
    float           PassengerPitch;                         // 30
    float           PassengerRoll;                          // 31
    int32           PassengerAttachmentID;                  // 32
    int32           VehicleEnterAnim;                       // 33
    int32           VehicleExitAnim;                        // 34
    int32           VehicleRideAnimLoop;                    // 35
    int32           VehicleEnterAnimBone;                   // 36
    int32           VehicleExitAnimBone;                    // 37
    int32           VehicleRideAnimLoopBone;                // 38
    float           VehicleEnterAnimDelay;                  // 39
    float           VehicleExitAnimDelay;                   // 40
    uint32          VehicleAbilityDisplay;                  // 41
    uint32          EnterUISoundID;                         // 42
    uint32          ExitUISoundID;                          // 43
    uint32          FlagsB;                                 // 44
    float           CameraEnteringDelay;                    // 45
    float           CameraEnteringDuration;                 // 46
    float           CameraExitingDelay;                     // 47
    float           CameraExitingDuration;                  // 48
    DBCPosition3D   CameraOffset;                           // 49-51
    float           CameraPosChaseRate;                     // 52
    float           CameraFacingChaseRate;                  // 53
    float           CameraEnteringZoom;                     // 54
    float           CameraSeatZoomMin;                      // 55
    float           CameraSeatZoomMax;                      // 56
    uint32          EnterAnimKitID;                         // 57
    uint32          RideAnimKitID;                          // 58
    uint32          ExitAnimKitID;                          // 59
    uint32          VehicleEnterAnimKitID;                  // 60
    uint32          VehicleRideAnimKitID;                   // 61
    uint32          VehicleExitAnimKitID;                   // 62
    uint32          CameraModeID;                           // 63
    uint32          FlagsC;                                 // 64
    uint32          UISkinFileDataID;                       // 65

    bool CanEnterOrExit() const
    {
        return ((Flags & VEHICLE_SEAT_FLAG_CAN_ENTER_OR_EXIT) != 0 ||
            //If it has anmation for enter/ride, means it can be entered/exited by logic
            (Flags & (VEHICLE_SEAT_FLAG_HAS_LOWER_ANIM_FOR_ENTER | VEHICLE_SEAT_FLAG_HAS_LOWER_ANIM_FOR_RIDE)) != 0);
    }
    bool CanSwitchFromSeat() const { return (Flags & VEHICLE_SEAT_FLAG_CAN_SWITCH) != 0; }
    bool CanControl() const { return (Flags & VEHICLE_SEAT_FLAG_CAN_CONTROL) != 0; }
    bool IsUsableByOverride() const {
        return (Flags & (VEHICLE_SEAT_FLAG_UNCONTROLLED | VEHICLE_SEAT_FLAG_UNK18)
            || (FlagsB & (VEHICLE_SEAT_FLAG_B_USABLE_FORCED | VEHICLE_SEAT_FLAG_B_USABLE_FORCED_2 |
                VEHICLE_SEAT_FLAG_B_USABLE_FORCED_3 | VEHICLE_SEAT_FLAG_B_USABLE_FORCED_4)));
    }
    bool IsEjectable() const { return (FlagsB & VEHICLE_SEAT_FLAG_B_EJECTABLE) != 0; }
};

struct WMOAreaTableEntry
{
    uint32      ID;                                         // 0 index
    int32       WMOID;                                      // 1 used in root WMO
    int32       NameSet;                                    // 2 used in adt file
    int32       WMOGroupID;                                 // 3 used in group WMO
    //uint32    SoundProviderPref;                          // 4
    //uint32    SoundProviderPrefUnderwater;                // 5
    //uint32    AmbienceID;                                 // 6
    //uint32    ZoneMusic;                                  // 7
    //uint32    IntroSound;                                 // 8
    uint32      Flags;                                      // 9 used for indoor/outdoor determination
    uint32      AreaTableID;                                // 10 link to AreaTableEntry.ID
    //char*     AreaName;                                   // 11 m_AreaName
    //uint32    UWIntroSound;                               // 12
    //uint32    UWZoneMusic;                                // 13
    //uint32    UWAmbience;                                 // 14
};

struct WorldMapAreaEntry
{
    //uint32    ID;                                         // 0
    uint32      MapID;                                      // 1
    uint32      AreaID;                                     // 2 index (continent 0 areas ignored)
    //char*     AreaName                                    // 3
    float       LocLeft;                                    // 4
    float       LocRight;                                   // 5
    float       LocTop;                                     // 6
    float       LocBottom;                                  // 7
    int32       DisplayMapID;                               // 8 -1 (map_id have correct map) other: virtual map where zone show (map_id - where zone in fact internally)
    //int32     DefaultDungeonFloor;                        // 9 pointer to DungeonMap.dbc (owerride loc coordinates)
    //uint32    ParentWorldMapID;                           // 10
    //uint32    Flags;                                      // 11
    //uint32    LevelRangeMin;                              // 12 Minimum recommended level displayed on world map
    //uint32    LevelRangeMax;                              // 13 Maximum recommended level displayed on world map
};

struct WorldMapOverlayEntry
{
    uint32      ID;                                         // 0
    uint32      MapAreaID;                                  // 1 idx in WorldMapArea.dbc
    uint32      AreaID[MAX_WORLD_MAP_OVERLAY_AREA_IDX];     // 2-5
    char*       TextureName;                                // 6
    uint32      TextureWidth;                               // 7
    uint32      TextureHeight;                              // 8
    uint32      OffsetX;                                    // 9
    uint32      OffsetY;                                    // 10
    uint32      HitRectTop;                                 // 11
    uint32      HitRectLeft;                                // 12
    uint32      HitRectBottom;                              // 13
    uint32      HitRectRight;                               // 14
    uint32      PlayerConditionID;                          // 15
};

struct WorldMapTransformsEntry
{
    //uint32        ID;                                     // 0
    uint32          MapID;                                  // 1
    DBCPosition3D   RegionMin;                              // 2-4
    DBCPosition3D   RegionMax;                              // 5-7
    uint32          NewMapID;                               // 8
    DBCPosition2D   RegionOffset;                           // 9-10
    //uint32        NewDungeonMapID;                        // 11
    //uint32        Flags;                                  // 12
    //uint32        NewAreaID;                              // 13
    float           RegionScale;                            // 14
};

struct WorldSafeLocsEntry
{
    uint32          ID;                                     // 0
    uint32          MapID;                                  // 1
    DBCPosition3D   Loc;                                    // 2-4
    float           Facing;                                 // 5 values are in degrees
    //char*         AreaName;                               // 6
};

// GCC have alternative #pragma pack() syntax and old gcc version not support pack(pop), also any gcc version not support it at some platform
#if defined(__GNUC__)
#pragma pack()
#else
#pragma pack(pop)
#endif

#endif
