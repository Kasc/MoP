/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePet.h"
#include "DB2Stores.h"
#include "DB2Format.h"
#include "Common.h"
#include "Log.h"
#include "World.h"

DB2Storage<BattlePetAbilityEntry>           sBattlePetAbilityStore("BattlePetAbility.db2", BattlePetAbilityFormat, HOTFIX_SEL_BATTLE_PET_ABILITY);
DB2Storage<BattlePetAbilityEffectEntry>     sBattlePetAbilityEffectStore("BattlePetAbilityEffect.db2", BattlePetAbilityEffectFormat, HOTFIX_SEL_BATTLE_PET_ABILITY_EFFECT);
DB2Storage<BattlePetAbilityStateEntry>      sBattlePetAbilityStateStore("BattlePetAbilityState.db2", BattlePetAbilityStateFormat, HOTFIX_SEL_BATTLE_PET_ABILITY_STATE);
DB2Storage<BattlePetAbilityTurnEntry>       sBattlePetAbilityTurnStore("BattlePetAbilityTurn.db2", BattlePetAbilityTurnFormat, HOTFIX_SEL_BATTLE_PET_ABILITY_TURN);
DB2Storage<BattlePetBreedQualityEntry>      sBattlePetBreedQualityStore("BattlePetBreedQuality.db2", BattlePetBreedQualityFormat, HOTFIX_SEL_BATTLE_PET_BREED_QUALITY);
DB2Storage<BattlePetBreedStateEntry>        sBattlePetBreedStateStore("BattlePetBreedState.db2", BattlePetBreedStateFormat, HOTFIX_SEL_BATTLE_PET_BREED_STATE);
DB2Storage<BattlePetEffectPropertiesEntry>  sBattlePetEffectPropertiesStore("BattlePetEffectProperties.db2", BattlePetEffectPropertiesFormat, HOTFIX_SEL_BATTLE_PET_EFFECT_PROPERTIES);
DB2Storage<BattlePetSpeciesEntry>           sBattlePetSpeciesStore("BattlePetSpecies.db2", BattlePetSpeciesFormat, HOTFIX_SEL_BATTLE_PET_SPECIES);
DB2Storage<BattlePetSpeciesStateEntry>      sBattlePetSpeciesStateStore("BattlePetSpeciesState.db2", BattlePetSpeciesStateFormat, HOTFIX_SEL_BATTLE_PET_SPECIES_STATE);
DB2Storage<BattlePetSpeciesXAbilityEntry>   sBattlePetSpeciesXAbilityStore("BattlePetSpeciesXAbility.db2", BattlePetSpeciesXAbilityFormat, HOTFIX_SEL_BATTLE_PET_SPECIES_XABILITY);
DB2Storage<BattlePetStateEntry>             sBattlePetStateStore("BattlePetState.db2", BattlePetStateFormat, HOTFIX_SEL_BATTLE_PET_STATE);
DB2Storage<BattlePetVisualEntry>            sBattlePetVisualStore("BattlePetVisual.db2", BattlePetVisualFormat, HOTFIX_SEL_BATTLE_PET_VISUAL);
DB2Storage<BroadcastTextEntry>              sBroadcastTextStore("BroadcastText.db2", BroadcastTextFormat, HOTFIX_SEL_BROADCAST_TEXT);
DB2Storage<CreatureEntry>                   sCreatureStore("Creature.db2", CreatureFormat, HOTFIX_SEL_CREATURE);
DB2Storage<CreatureDifficultyEntry>         sCreatureDifficultyStore("CreatureDifficulty.db2", CreatureDifficultyFormat, HOTFIX_SEL_CREATURE_DIFFICULTY);
DB2Storage<CurveEntry>                      sCurveStore("Curve.db2", CurveFormat, HOTFIX_SEL_CURVE);
DB2Storage<CurvePointEntry>                 sCurvePointStore("CurvePoint.db2", CurvePointFormat, HOTFIX_SEL_CURVE_POINT);
DB2Storage<DeviceBlacklistEntry>            sDeviceBlacklistStore("DeviceBlacklist.db2", DeviceBlacklistFormat, HOTFIX_SEL_DEVICE_BLACKLIST);
DB2Storage<DriverBlacklistEntry>            sDriverBlacklistStore("DriverBlacklist.db2", DriverBlacklistFormat, HOTFIX_SEL_DRIVER_BLACKLIST);
DB2Storage<GameObjectEntry>                 sGameObjectsStore("GameObjects.db2", GameObjectsFormat, HOTFIX_SEL_GAMEOBJECTS);
DB2Storage<ItemEntry>                       sItemStore("Item.db2", ItemFormat, HOTFIX_SEL_ITEM);
DB2Storage<ItemCurrencyCostEntry>           sItemCurrencyCostStore("ItemCurrencyCost.db2", ItemCurrencyCostFormat, HOTFIX_SEL_ITEM_CURRENCY_COST);
DB2Storage<ItemExtendedCostEntry>           sItemExtendedCostStore("ItemExtendedCost.db2", ItemExtendedCostFormat, HOTFIX_SEL_ITEM_EXTENDED_COST);
DB2Storage<ItemSparseEntry>                 sItemSparseStore("Item-sparse.db2", ItemSparseFormat, HOTFIX_SEL_ITEM_SPARSE);
DB2Storage<ItemToBattlePetEntry>            sItemToBattlePetStore("ItemToBattlePet.db2", ItemToBattlePetFormat, HOTFIX_SEL_ITEM_BATTLE_PET);
DB2Storage<ItemToMountSpellEntry>           sItemToMountSpellStore("ItemToMountSpell.db2", ItemToMountSpellFormat, HOTFIX_SEL_ITEM_MOUNT_SPELL);
DB2Storage<ItemUpgradeEntry>                sItemUpgradeStore("ItemUpgrade.db2", ItemUpgradeFormat, HOTFIX_SEL_ITEM_UPGRADE);
DB2Storage<KeyChainEntry>                   sKeyChainStore("KeyChain.db2", KeyChainFormat, HOTFIX_SEL_KEY_CHAIN);
DB2Storage<LocaleEntry>                     sLocaleStore("Locale.db2", LocaleFormat, HOTFIX_SEL_LOCALE);
DB2Storage<LocationEntry>                   sLocationStore("Location.db2", LocationFormat, HOTFIX_SEL_LOCATION);
DB2Storage<MapChallengeModeEntry>           sMapChallengeModeStore("MapChallengeMode.db2", MapChallengeModeFormat, HOTFIX_SEL_MAP_CHALLENGE_MODE);
DB2Storage<MarketingPromotionsXLocaleEntry> sMarketingPromotionsXLocaleStore("MarketingPromotionsXLocale.db2", MarketingPromotionsXLocaleFormat, HOTFIX_SEL_MARKETING_PROMOTIONS_XLOCALE);
DB2Storage<PathEntry>                       sPathStore("Path.db2", PathFormat, HOTFIX_SEL_PATH);
DB2Storage<PathNodeEntry>                   sPathNodeStore("PathNode.db2", PathNodeFormat, HOTFIX_SEL_PATH_NODE);
DB2Storage<PathNodePropertyEntry>           sPathNodePropertyStore("PathNodeProperty.db2", PathNodePropertyFormat, HOTFIX_SEL_PATH_NODE_PROPERTY);
DB2Storage<PathPropertyEntry>               sPathPropertyStore("PathProperty.db2", PathPropertyFormat, HOTFIX_SEL_PATH_PROPERTY);
DB2Storage<QuestPackageItemEntry>           sQuestPackageItemStore("QuestPackageItem.db2", QuestPackageItemFormat, HOTFIX_SEL_QUEST_PACKAGE_ITEM);
DB2Storage<RulesetItemUpgradeEntry>         sRulesetItemUpgradeStore("RulesetItemUpgrade.db2", RulesetItemUpgradeFormat, HOTFIX_SEL_RULESET_ITEM_UPGRADE);
DB2Storage<RulesetRaidLootUpgradeEntry>     sRulesetRaidLootUpgradeStore("RulesetRaidLootUpgrade.db2", RulesetRaidLootUpgradeFormat, HOTFIX_SEL_RULESET_RAID_LOOT_UPGRADE);
DB2Storage<SceneScriptEntry>                sSceneScriptStore("SceneScript.db2", SceneScriptFormat, HOTFIX_SEL_SCENE_SCRIPT);
DB2Storage<SceneScriptPackageEntry>         sSceneScriptPackageStore("SceneScriptPackage.db2", SceneScriptPackageFormat, HOTFIX_SEL_SCENE_SCRIPT_PACKAGE);
DB2Storage<SceneScriptPackageMemberEntry>   sSceneScriptPackageMemberStore("SceneScriptPackageMember.db2", SceneScriptPackageMemberFormat, HOTFIX_SEL_SCENE_SCRIPT_PACKAGE_MEMBER);
DB2Storage<SpellEffectCameraShakesEntry>    sSpellEffectCameraShakesStore("SpellEffectCameraShakes.db2", SpellEffectCameraShakesFormat, HOTFIX_SEL_SPELL_EFFECT_CAMERA_SHAKES);
DB2Storage<SpellMissileEntry>               sSpellMissileStore("SpellMissile.db2", SpellMissileFormat, HOTFIX_SEL_SPELL_MISSILE);
DB2Storage<SpellMissileMotionEntry>         sSpellMissileMotionStore("SpellMissileMotion.db2", SpellMissileMotionFormat, HOTFIX_SEL_SPELL_MISSILE_MOTION);
DB2Storage<SpellReagentsEntry>              sSpellReagentsStore("SpellReagents.db2", SpellReagentsFormat, HOTFIX_SEL_SPELL_REAGENTS);
DB2Storage<SpellVisualEntry>                sSpellVisualStore("SpellVisual.db2", SpellVisualFormat, HOTFIX_SEL_SPELL_VISUAL);
DB2Storage<SpellVisualEffectNameEntry>      sSpellVisualEffectNameStore("SpellVisualEffectName.db2", SpellVisualEffectNameFormat, HOTFIX_SEL_SPELL_VISUAL_EFFECT_NAME);
DB2Storage<SpellVisualKitEntry>             sSpellVisualKitStore("SpellVisualKit.db2", SpellVisualKitFormat, HOTFIX_SEL_SPELL_VISUAL_KIT);
DB2Storage<SpellVisualKitAreaModelEntry>    sSpellVisualKitAreaModelStore("SpellVisualKitAreaModel.db2", SpellVisualKitAreaModelFormat, HOTFIX_SEL_SPELL_VISUAL_KIT_AREA_MODEL);
DB2Storage<SpellVisualKitModelAttachEntry>  sSpellVisualKitModelAttachStore("SpellVisualKitModelAttach.db2", SpellVisualKitModelAttachFormat, HOTFIX_SEL_SPELL_VISUAL_KIT_MODEL_ATTACH);
DB2Storage<SpellVisualMissileEntry>         sSpellVisualMissileStore("SpellVisualMissile.db2", SpellVisualMissileFormat, HOTFIX_SEL_SPELL_VISUAL_MISSILE);
DB2Storage<VignetteEntry>                   sVignetteStore("Vignette.db2", VignetteFormat, HOTFIX_SEL_VIGNETTE);

DB2Manager::~DB2Manager() { }

DB2Manager::DB2Manager()
{
    DB2FilesCount = 0;
}

template<class T>
void DB2Manager::LoadDB2(uint32& availableDb2Locales, DB2StoreProblemList& errlist, DB2Storage<T>* storage, std::string const& db2_path)
{
    // compatibility format and C++ structure sizes
    ASSERT(DB2FileLoader::GetFormatRecordSize(storage->GetFormat()) == sizeof(T) && "Size set by format string not equal size of C++ structure");

    ++DB2FilesCount;

    if (storage->Load(db2_path, uint32(sWorld->GetDefaultDbcLocale())))
    {
        storage->LoadFromDB();

        for (uint32 i = 0; i < TOTAL_LOCALES; ++i)
        {
            if (uint32(sWorld->GetDefaultDbcLocale()) == i)
                continue;

            if (availableDb2Locales & (1 << i))
                if (!storage->LoadStringsFrom((db2_path + localeNames[i] + '/'), i))
                    availableDb2Locales &= ~(1 << i);             // mark as not available for speedup next checks
        }
    }
    else
    {
        // sort problematic db2 to (1) non compatible and (2) nonexistent
        if (FILE* f = fopen((db2_path + storage->GetFileName()).c_str(), "rb"))
        {
            std::ostringstream stream;
            stream << storage->GetFileName() << " exists, and has " << storage->GetFieldCount() << " field(s) (expected " << strlen(storage->GetFormat())
                << "). Extracted file might be from wrong client version.";
            std::string buf = stream.str();
            errlist.push_back(buf);
            fclose(f);
        }
        else
            errlist.push_back(storage->GetFileName());
    }

    _stores[storage->GetHash()] = storage;
}

void DB2Manager::LoadStores(std::string const& dataPath)
{
    uint32 oldMSTime = getMSTime();

    std::string db2Path = dataPath + "dbc/";

    DB2StoreProblemList bad_db2_files;
    uint32 availableDb2Locales = 0xFF;

    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetAbilityStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetAbilityEffectStore,     db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetAbilityStateStore,      db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetAbilityTurnStore,       db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetBreedQualityStore,      db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetBreedStateStore,        db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetEffectPropertiesStore,  db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetSpeciesStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetSpeciesStateStore,      db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetSpeciesXAbilityStore,   db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetStateStore,             db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBattlePetVisualStore,            db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sBroadcastTextStore,              db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sCreatureStore,                   db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sCreatureDifficultyStore,         db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sCurveStore,                      db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sCurvePointStore,                 db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sDeviceBlacklistStore,            db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sDriverBlacklistStore,            db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sGameObjectsStore,                db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemStore,                       db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemCurrencyCostStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemExtendedCostStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemSparseStore,                 db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemToBattlePetStore,            db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemToMountSpellStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sItemUpgradeStore,                db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sKeyChainStore,                   db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sLocaleStore,                     db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sLocationStore,                   db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sMapChallengeModeStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sMarketingPromotionsXLocaleStore, db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sPathStore,                       db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sPathNodeStore,                   db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sPathNodePropertyStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sPathPropertyStore,               db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sQuestPackageItemStore,           db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sRulesetItemUpgradeStore,         db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sRulesetRaidLootUpgradeStore,     db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSceneScriptStore,                db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSceneScriptPackageStore,         db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSceneScriptPackageMemberStore,   db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellEffectCameraShakesStore,    db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellMissileStore,               db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellMissileMotionStore,         db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellReagentsStore,              db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellVisualStore,                db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellVisualEffectNameStore,      db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellVisualKitStore,             db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellVisualKitAreaModelStore,    db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellVisualKitModelAttachStore,  db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sSpellVisualMissileStore,         db2Path);
    LoadDB2(availableDb2Locales, bad_db2_files, &sVignetteStore,                   db2Path);

    for (BattlePetSpeciesEntry const* speciesEntry : sBattlePetSpeciesStore)
    {
        _battlePetSpellXSpeciesStore[speciesEntry->SpellId] = speciesEntry->Id;
        _battlePetNPCSpeciesStore[speciesEntry->NpcId] = speciesEntry;
    }

    for (QuestPackageItemEntry const* questPackageItem : sQuestPackageItemStore)
    {
        if (questPackageItem->FilterType != QUEST_PACKAGE_FILTER_UNMATCHED)
            _questPackages[questPackageItem->QuestPackageId].first.push_back(questPackageItem);
        else
            _questPackages[questPackageItem->QuestPackageId].second.push_back(questPackageItem);
    }

    for (RulesetItemUpgradeEntry const* rulesetItemUpgrade : sRulesetItemUpgradeStore)
        if (rulesetItemUpgrade->RulesetID == 1) // xref to Cfg_Regions.db2, RulesetID column. 1 is the value for EU region we send hardcoded in SMSG_INITIAL_SETUP, possible other value for RulesetID is 2 in korean region
            _rulesetItemUpgrade[rulesetItemUpgrade->ItemEntry] = rulesetItemUpgrade->ItemUpgradeId;

    for (CurvePointEntry const* curvePoint : sCurvePointStore)
        if (sCurveStore.LookupEntry(curvePoint->CurveID))
            _curvePoints[curvePoint->CurveID].push_back(curvePoint);

    for (auto itr = _curvePoints.begin(); itr != _curvePoints.end(); ++itr)
        std::sort(itr->second.begin(), itr->second.end(), [](CurvePointEntry const* point1, CurvePointEntry const* point2) { return point1->OrderIndex < point2->OrderIndex; });

    // error checks
    if (bad_db2_files.size() >= DB2FilesCount)
    {
        TC_LOG_ERROR("misc", "\nIncorrect DataDir value in worldserver.conf or ALL required *.db2 files (%d) not found by path: %sdb2", DB2FilesCount, dataPath.c_str());
        exit(1);
    }
    else if (!bad_db2_files.empty())
    {
        std::string str;
        for (std::list<std::string>::iterator i = bad_db2_files.begin(); i != bad_db2_files.end(); ++i)
            str += *i + "\n";

        TC_LOG_ERROR("misc", "\nSome required *.db2 files (%u from %d) not found or not compatible:\n%s", (uint32)bad_db2_files.size(), DB2FilesCount, str.c_str());
        exit(1);
    }

    // Check loaded DB2 files proper version
    if (!sBattlePetAbilityStore.LookupEntry(1238)           // last battle pet ability added in 5.4.8 (18414)
        || !sBattlePetSpeciesStore.LookupEntry(1386)        // last battle pet species added in 5.4.8 (18414)
        || !sBattlePetStateStore.LookupEntry(176)           // last battle pet state added in 5.4.8 (18414)
        || !sItemToBattlePetStore.LookupEntry(109014)       // last battle pet item added in 5.4.8 (18414)
        || !sBroadcastTextStore.LookupEntry(77161)          // last broadcast text added in 5.4.8 (18414)
        || !sItemStore.LookupEntry(112353)                  // last item added in 5.4.8 (18414)
        || !sItemExtendedCostStore.LookupEntry(5280)        // last item extended cost added in 5.4.8 (18414)
        || !sQuestPackageItemStore.LookupEntry(2256)        // last quest package item in 5.4.8 (18414)
        || !sSceneScriptStore.LookupEntry(11156))           // last scene script added in 5.4.8 (18414)
    {
        TC_LOG_ERROR("misc", "You have _outdated_ DB2 files, Please extract correct db2 files from client 5.4.8 18414.");
        exit(1);
    }

    TC_LOG_INFO("server.loading", ">> Initialized %d DB2 data stores in %u ms", DB2FilesCount, GetMSTimeDiffToNow(oldMSTime));
}

DB2StorageBase const* DB2Manager::GetStorage(uint32 type) const
{
    StorageMap::const_iterator itr = _stores.find(type);
    if (itr != _stores.end())
        return itr->second;

    return nullptr;
}

void DB2Manager::LoadHotfixData()
{
    uint32 oldMSTime = getMSTime();

    QueryResult result = HotfixDatabase.Query("SELECT TableHash, RecordID, `Timestamp`, Deleted FROM hotfix_data");

    if (!result)
    {
        TC_LOG_INFO("misc", ">> Loaded 0 hotfix info entries.");
        return;
    }

    uint32 count = 0;

    _hotfixData.reserve(result->GetRowCount());

    do
    {
        Field* fields = result->Fetch();

        HotfixNotify info;
        info.TableHash = fields[0].GetUInt32();
        info.Entry = fields[1].GetUInt32();
        info.Timestamp = fields[2].GetUInt32();
        _hotfixData.push_back(info);

        if (fields[3].GetBool())
        {
            auto itr = _stores.find(info.TableHash);
            if (itr != _stores.end())
                itr->second->EraseRecord(info.Entry);
        }

        ++count;
    } while (result->NextRow());

    TC_LOG_INFO("misc", ">> Loaded %u hotfix info entries in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

time_t DB2Manager::GetHotfixDate(uint32 entry, uint32 type) const
{
    time_t ret = 0;
    for (HotfixNotify const& hotfix : _hotfixData)
        if (hotfix.Entry == entry && hotfix.TableHash == type)
            if (time_t(hotfix.Timestamp) > ret)
                ret = time_t(hotfix.Timestamp);

    return ret ? ret : time(NULL);
}

char const* DB2Manager::GetBroadcastTextValue(BroadcastTextEntry const* broadcastText, LocaleConstant locale /*= DEFAULT_LOCALE*/, uint8 gender /*= GENDER_MALE*/, bool forceGender /*= false*/)
{
    if (gender == GENDER_FEMALE && (forceGender || broadcastText->FemaleText->Str[DEFAULT_LOCALE][0] != '\0'))
    {
        if (broadcastText->FemaleText->Str[locale][0] != '\0')
            return broadcastText->FemaleText->Str[locale];

        return broadcastText->FemaleText->Str[DEFAULT_LOCALE];
    }

    if (broadcastText->MaleText->Str[locale][0] != '\0')
        return broadcastText->MaleText->Str[locale];

    return broadcastText->MaleText->Str[DEFAULT_LOCALE];
}

bool DB2Manager::HasBattlePetSpeciesFlag(uint32 species, uint32 flag) const
{
    BattlePetSpeciesEntry const* speciesEntry = sBattlePetSpeciesStore.LookupEntry(species);
    if (!speciesEntry)
        return false;

    return (speciesEntry->Flags & flag) != 0;
}

uint32 DB2Manager::GetBattlePetSummonSpell(uint32 species) const
{
    auto speciesEntry = sBattlePetSpeciesStore.LookupEntry(species);
    if (!speciesEntry)
        return 0;

    return speciesEntry->SpellId;
}

uint16 DB2Manager::GetBattlePetSpeciesFromSpell(uint32 spellId) const
{
    auto spellSpeciesEntry = _battlePetSpellXSpeciesStore.find(spellId);
    if (spellSpeciesEntry == _battlePetSpellXSpeciesStore.end())
        return 0;

    return spellSpeciesEntry->second;
}

BattlePetSpeciesEntry const* DB2Manager::GetBattlePetSpeciesIdFromNpcEntry(uint32 entry) const
{
    auto speciesId = _battlePetNPCSpeciesStore.find(entry);
    if (speciesId == _battlePetNPCSpeciesStore.end())
        return 0;

    return speciesId->second;
}

std::vector<QuestPackageItemEntry const*> const* DB2Manager::GetQuestPackageItems(uint32 questPackageID) const
{
    auto itr = _questPackages.find(questPackageID);
    if (itr != _questPackages.end())
        return &itr->second.first;

    return nullptr;
}

std::vector<QuestPackageItemEntry const*> const* DB2Manager::GetQuestPackageItemsFallback(uint32 questPackageID) const
{
    auto itr = _questPackages.find(questPackageID);
    if (itr != _questPackages.end())
        return &itr->second.second;

    return nullptr;
}

uint32 DB2Manager::GetRulesetItemUpgrade(uint32 itemId) const
{
    auto itr = _rulesetItemUpgrade.find(itemId);
    if (itr != _rulesetItemUpgrade.end())
        return itr->second;

    return 0;
}

static CurveInterpolationMode DetermineCurveType(CurveEntry const* curve, std::vector<CurvePointEntry const*> const& points)
{
    switch (curve->Type)
    {
        case 1:
            return points.size() < 4 ? CurveInterpolationMode::Cosine : CurveInterpolationMode::CatmullRom;
        case 2:
        {
            switch (points.size())
            {
                case 1:
                    return CurveInterpolationMode::Constant;
                case 2:
                    return CurveInterpolationMode::Linear;
                case 3:
                    return CurveInterpolationMode::Bezier3;
                case 4:
                    return CurveInterpolationMode::Bezier4;
                default:
                    break;
            }
            return CurveInterpolationMode::Bezier;
        }
        case 3:
            return CurveInterpolationMode::Cosine;
        default:
            break;
    }

    return points.size() != 1 ? CurveInterpolationMode::Linear : CurveInterpolationMode::Constant;
}

float DB2Manager::GetCurveValueAt(uint32 curveId, float x) const
{
    auto itr = _curvePoints.find(curveId);
    if (itr == _curvePoints.end())
        return 0.0f;

    CurveEntry const* curve = sCurveStore.AssertEntry(curveId);
    std::vector<CurvePointEntry const*> const& points = itr->second;
    if (points.empty())
        return 0.0f;

    switch (DetermineCurveType(curve, points))
    {
        case CurveInterpolationMode::Linear:
        {
            std::size_t pointIndex = 0;
            while (pointIndex < points.size() && points[pointIndex]->X <= x)
                ++pointIndex;
            if (!pointIndex)
                return points[0]->Y;
            if (pointIndex >= points.size())
                return points.back()->Y;
            float xDiff = points[pointIndex]->X - points[pointIndex - 1]->X;
            if (xDiff == 0.0)
                return points[pointIndex]->Y;
            return (((x - points[pointIndex - 1]->X) / xDiff) * (points[pointIndex]->Y - points[pointIndex - 1]->Y)) + points[pointIndex - 1]->Y;
        }
        case CurveInterpolationMode::Cosine:
        {
            std::size_t pointIndex = 0;
            while (pointIndex < points.size() && points[pointIndex]->X <= x)
                ++pointIndex;
            if (!pointIndex)
                return points[0]->Y;
            if (pointIndex >= points.size())
                return points.back()->Y;
            float xDiff = points[pointIndex]->X - points[pointIndex - 1]->X;
            if (xDiff == 0.0)
                return points[pointIndex]->Y;
            return ((points[pointIndex]->Y - points[pointIndex - 1]->Y) * (1.0f - std::cos((x - points[pointIndex - 1]->X) / xDiff * float(M_PI))) * 0.5f) + points[pointIndex - 1]->Y;
        }
        case CurveInterpolationMode::CatmullRom:
        {
            std::size_t pointIndex = 1;
            while (pointIndex < points.size() && points[pointIndex]->X <= x)
                ++pointIndex;
            if (pointIndex == 1)
                return points[1]->Y;
            if (pointIndex >= points.size() - 1)
                return points[points.size() - 2]->Y;
            float xDiff = points[pointIndex]->X - points[pointIndex - 1]->X;
            if (xDiff == 0.0)
                return points[pointIndex]->Y;

            float mu = (x - points[pointIndex - 1]->X) / xDiff;
            float a0 = -0.5f * points[pointIndex - 2]->Y + 1.5f * points[pointIndex - 1]->Y - 1.5f * points[pointIndex]->Y + 0.5f * points[pointIndex + 1]->Y;
            float a1 = points[pointIndex - 2]->Y - 2.5f * points[pointIndex - 1]->Y + 2.0f * points[pointIndex]->Y - 0.5f * points[pointIndex + 1]->Y;
            float a2 = -0.5f * points[pointIndex - 2]->Y + 0.5f * points[pointIndex]->Y;
            float a3 = points[pointIndex - 1]->Y;

            return a0 * mu * mu * mu + a1 * mu * mu + a2 * mu + a3;
        }
        case CurveInterpolationMode::Bezier3:
        {
            float xDiff = points[2]->X - points[0]->X;
            if (xDiff == 0.0)
                return points[1]->Y;
            float mu = (x - points[0]->X) / xDiff;
            return ((1.0f - mu) * (1.0f - mu) * points[0]->Y) + (1.0f - mu) * 2.0f * mu * points[1]->Y + mu * mu * points[2]->Y;
        }
        case CurveInterpolationMode::Bezier4:
        {
            float xDiff = points[3]->X - points[0]->X;
            if (xDiff == 0.0)
                return points[1]->Y;
            float mu = (x - points[0]->X) / xDiff;
            return (1.0f - mu) * (1.0f - mu) * (1.0f - mu) * points[0]->Y
                + 3.0f * mu * (1.0f - mu) * (1.0f - mu) * points[1]->Y
                + 3.0f * mu * mu * (1.0f - mu) * points[2]->Y
                + mu * mu * mu * points[3]->Y;
        }
        case CurveInterpolationMode::Bezier:
        {
            float xDiff = points.back()->X - points[0]->X;
            if (xDiff == 0.0f)
                return points.back()->Y;

            std::vector<float> tmp(points.size());
            for (std::size_t i = 0; i < points.size(); ++i)
                tmp[i] = points[i]->Y;

            float mu = (x - points[0]->X) / xDiff;
            int32 i = int32(points.size()) - 1;
            while (i > 0)
            {
                for (int32 k = 0; k < i; ++k)
                {
                    float val = tmp[k] + mu * (tmp[k + 1] - tmp[k]);
                    tmp[k] = val;
                }
                --i;
            }
            return tmp[0];
        }
        case CurveInterpolationMode::Constant:
            return points[0]->Y;
        default:
            break;
    }

    return 0.0f;
}
