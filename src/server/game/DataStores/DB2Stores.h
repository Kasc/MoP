/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_DB2STORES_H
#define TRINITY_DB2STORES_H

#include "DB2Store.h"
#include "DB2Structure.h"
#include <string>
#include "Singleton/Singleton.hpp"

enum class CurveInterpolationMode
{
    Linear      = 0,
    Cosine      = 1,
    CatmullRom  = 2,
    Bezier3     = 3,
    Bezier4     = 4,
    Bezier      = 5,
    Constant    = 6
};

extern DB2Storage<BattlePetAbilityEntry>            sBattlePetAbilityStore;
extern DB2Storage<BattlePetAbilityEffectEntry>      sBattlePetAbilityEffectStore;
extern DB2Storage<BattlePetAbilityStateEntry>       sBattlePetAbilityStateStore;
extern DB2Storage<BattlePetAbilityTurnEntry>        sBattlePetAbilityTurnStore;
extern DB2Storage<BattlePetBreedQualityEntry>       sBattlePetBreedQualityStore;
extern DB2Storage<BattlePetBreedStateEntry>         sBattlePetBreedStateStore;
extern DB2Storage<BattlePetEffectPropertiesEntry>   sBattlePetEffectPropertiesStore;
extern DB2Storage<BattlePetSpeciesEntry>            sBattlePetSpeciesStore;
extern DB2Storage<BattlePetSpeciesStateEntry>       sBattlePetSpeciesStateStore;
extern DB2Storage<BattlePetSpeciesXAbilityEntry>    sBattlePetSpeciesXAbilityStore;
extern DB2Storage<BattlePetStateEntry>              sBattlePetStateStore;
extern DB2Storage<BattlePetVisualEntry>             sBattlePetVisualStore;
extern DB2Storage<BroadcastTextEntry>               sBroadcastTextStore;
extern DB2Storage<CreatureEntry>                    sCreatureStore;
extern DB2Storage<CreatureDifficultyEntry>          CreatureDifficultyStore;
extern DB2Storage<CurveEntry>                       sCurveStore;
extern DB2Storage<CurvePointEntry>                  sCurvePointStore;
extern DB2Storage<DeviceBlacklistEntry>             sDeviceBlacklistStore;
extern DB2Storage<DriverBlacklistEntry>             sDriverBlacklistStore;
extern DB2Storage<GameObjectEntry>                  sGameObjectsStore;
extern DB2Storage<ItemEntry>                        sItemStore;
extern DB2Storage<ItemCurrencyCostEntry>            sItemCurrencyCostStore;
extern DB2Storage<ItemExtendedCostEntry>            sItemExtendedCostStore;
extern DB2Storage<ItemSparseEntry>                  sItemSparseStore;
extern DB2Storage<ItemToBattlePetEntry>             sItemToBattlePetStore;
extern DB2Storage<ItemToMountSpellEntry>            sItemToMountSpellStore;
extern DB2Storage<ItemUpgradeEntry>                 sItemUpgradeStore;
extern DB2Storage<KeyChainEntry>                    sKeyChainStore;
extern DB2Storage<LocaleEntry>                      sLocaleStore;
extern DB2Storage<LocationEntry>                    sLocationStore;
extern DB2Storage<MapChallengeModeEntry>            sMapChallengeModeStore;
extern DB2Storage<MarketingPromotionsXLocaleEntry>  sMarketingPromotionsXLocaleStore;
extern DB2Storage<PathEntry>                        sPathStore;
extern DB2Storage<PathNodeEntry>                    sPathNodeStore;
extern DB2Storage<PathNodePropertyEntry>            sPathNodePropertyStore;
extern DB2Storage<PathPropertyEntry>                sPathPropertyStore;
extern DB2Storage<QuestPackageItemEntry>            sQuestPackageItemStore;
extern DB2Storage<RulesetItemUpgradeEntry>          sRulesetItemUpgradeStore;
extern DB2Storage<RulesetRaidLootUpgradeEntry>      sRulesetRaidLootUpgradeStore;
extern DB2Storage<SceneScriptEntry>                 sSceneScriptStore;
extern DB2Storage<SceneScriptPackageEntry>          sSceneScriptPackageStore;
extern DB2Storage<SceneScriptPackageMemberEntry>    sSceneScriptPackageMemberStore;
extern DB2Storage<SpellEffectCameraShakesEntry>     sSpellEffectCameraShakesStore;
extern DB2Storage<SpellMissileEntry>                sSpellMissileStore;
extern DB2Storage<SpellMissileMotionEntry>          sSpellMissileMotionStore;
extern DB2Storage<SpellReagentsEntry>               sSpellReagentsStore;
extern DB2Storage<SpellVisualEntry>                 sSpellVisualStore;
extern DB2Storage<SpellVisualEffectNameEntry>       sSpellVisualEffectNameStore;
extern DB2Storage<SpellVisualKitEntry>              sSpellVisualKitStore;
extern DB2Storage<SpellVisualKitAreaModelEntry>     sSpellVisualKitAreaModelStore;
extern DB2Storage<SpellVisualKitModelAttachEntry>   sSpellVisualKitModelAttachStore;
extern DB2Storage<SpellVisualMissileEntry>          sSpellVisualMissileStore;
extern DB2Storage<VignetteEntry>                    VignetteStore;

struct HotfixNotify
{
    uint32 TableHash;
    uint32 Timestamp;
    uint32 Entry;
};

typedef std::vector<HotfixNotify> HotfixData;

typedef std::unordered_map<uint32, uint16> BattlePetSpellStore;
typedef std::unordered_map<uint32, BattlePetSpeciesEntry const*> BattlePetNPCStore;

typedef std::unordered_map<uint32, std::pair<std::vector<QuestPackageItemEntry const*>, std::vector<QuestPackageItemEntry const*>>> QuestPackageItemContainer;

typedef std::unordered_map<uint32, uint32> RulesetItemUpgradeContainer;

typedef std::unordered_map<uint32, std::vector<CurvePointEntry const*>> CurvePointsContainer;

class DB2Manager
{
    friend class Tod::Singleton<DB2Manager>;
    DB2Manager();
    ~DB2Manager();

private:
    uint32 DB2FilesCount;

    typedef std::map<uint32 /*hash*/, DB2StorageBase*> StorageMap;
    typedef std::list<std::string> DB2StoreProblemList;

    StorageMap _stores;
    HotfixData _hotfixData;

    BattlePetSpellStore _battlePetSpellXSpeciesStore;
    BattlePetNPCStore _battlePetNPCSpeciesStore;

    QuestPackageItemContainer _questPackages;

    RulesetItemUpgradeContainer _rulesetItemUpgrade;

    CurvePointsContainer _curvePoints;

public:
    void LoadStores(std::string const& dataPath);
    DB2StorageBase const* GetStorage(uint32 type) const;

    template<class T>
    void LoadDB2(uint32& availableDb2Locales, DB2StoreProblemList& errlist, DB2Storage<T>* storage, std::string const& db2_path);

    void LoadHotfixData();
    HotfixData const* GetHotfixData() const { return &_hotfixData; }
    time_t GetHotfixDate(uint32 entry, uint32 type) const;

    bool HasBattlePetSpeciesFlag(uint32 species, uint32 flag) const;
    uint32 GetBattlePetSummonSpell(uint32 species) const;
    uint16 GetBattlePetSpeciesFromSpell(uint32 spellId) const;
    BattlePetSpeciesEntry const* GetBattlePetSpeciesIdFromNpcEntry(uint32 entry) const;

    char const* GetBroadcastTextValue(BroadcastTextEntry const* broadcastText, LocaleConstant locale = DEFAULT_LOCALE, uint8 gender = GENDER_MALE, bool forceGender = false);

    std::vector<QuestPackageItemEntry const*> const* GetQuestPackageItems(uint32 questPackageID) const;
    std::vector<QuestPackageItemEntry const*> const* GetQuestPackageItemsFallback(uint32 questPackageID) const;

    uint32 GetRulesetItemUpgrade(uint32 itemId) const;

    float GetCurveValueAt(uint32 curveId, float x) const;
};

#define sDB2Manager Tod::Singleton<DB2Manager>::GetSingleton()

#endif
