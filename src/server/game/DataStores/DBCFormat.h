/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_DBCSFRM_H
#define TRINITY_DBCSFRM_H

// x - skip<uint32>, X - skip<uint8>, s - char*, f - float, i - uint32, b - uint8, d - index (not included)
// n - index (included), l - uint64, p - field present in sql dbc, a - field absent in sql dbc

char const AchievementFormat[]                      = "niiissiiiiisiii";
//const std::string CustomAchievementFormat         = "pppaaaapapaappa";
//const std::string CustomAchievementIndex          = "ID";
char const AchievementCategoryFormat[]              = "nisi";
char const AnimKitFormat[]                          = "nxx";
char const AreaTableEntryFormat[]                   = "niiiixxxxxxxisiiiiifxxxxxxxxii";
char const AreaGroupEntryFormat[]                   = "niiiiiii";
char const AreaTriggerEntryFormat[]                 = "nifffxxxfffffxxx";
char const ArmorLocationFormat[]                    = "nfffff";
char const AuctionHouseEntryFormat[]                = "niiis";
char const BankBagSlotPricesEntryFormat[]           = "ni";
char const BannedAddOnsFormat[]                     = "nxxxxxxxxxx";
char const BarberShopStyleEntryFormat[]             = "nissfiii";
char const BattlemasterListEntryFormat[]            = "niiiiiiiiiiiiiiiiixsiiiixxxxxx";
char const CharSectionsFormat[]                     = "diiixxxiii";
char const CharStartOutfitEntryFormat[]             = "nbbbbiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
char const CharTitlesEntryFormat[]                  = "nxssix";
char const ChatChannelsEntryFormat[]                = "nixsx";
char const ChrClassesEntryFormat[]                  = "nixsxxxixiiiiixxxx";
char const ChrRacesEntryFormat[]                    = "niixiixixxxxixsxxxxxxxxxxxxxxxxxxxxx";
char const ChrClassesXPowerTypesFormat[]            = "nii";
char const CinematicSequencesEntryFormat[]          = "niiiiiiiii";
char const CreatureDisplayInfoFormat[]              = "niiifissssiiiiiiiiii";
char const CreatureDisplayInfoExtraFormat[]         = "niixxxxxxxxxxxxxxxxxx";
char const CreatureModelDataFormat[]                = "nisxxxxxxxxxxxxffxxxxxxxxxxxxxxxxx";
char const CreatureFamilyFormat[]                   = "nfifiiiiixsx";
char const CreatureSpellDataFormat[]                = "niiiixxxx";
char const CreatureTypeFormat[]                     = "nsi";
char const CurrencyTypesFormat[]                    = "nisssiiiiiis";
char const CriteriaFormat[]                         = "niiiiiiiiiii";
char const CriteriaTreeFormat[]                     = "niiiiiis";
char const ChrSpecializationFormat[]                = "nxiiiiiiiiixxx";
char const CinematicCameraEntryFormat[]             = "nsiffff";
char const DestructibleModelDataFormat[]            = "niiiiiiiiiiiiiiiiiiiiiii";
char const DifficultyFormat[]                       = "niiiiiiixxxx";
char const DungeonEncounterFormat[]                 = "niiixsxxx";
char const DurabilityCostsFormat[]                  = "niiiiiiiiiiiiiiiiiiiiiiiiiiiii";
char const DurabilityQualityFormat[]                = "nf";
char const EmotesEntryFormat[]                      = "nxxiiixx";
char const EmotesTextEntryFormat[]                  = "nxixxxxxxxxxxxxxxxx";
char const EmotesTextSoundEntryFormat[]             = "niiii";
char const FactionEntryFormat[]                     = "niiiiiiiiiiiiiiiiiiffixsxixx";
char const FactionTemplateEntryFormat[]             = "niiiiiiiiiiiii";
char const FileDataFormat[]                         = "nss";
char const GameObjectDisplayInfoFormat[]            = "nsxxxxxxxxxxffffffxxx";
char const GameTablesFmt[]                          = "nsii";
char const GemPropertiesEntryFormat[]               = "nixxii";
char const GlyphPropertiesFormat[]                  = "niii";
char const GlyphSlotFormat[]                        = "nii";
char const GtBarberShopCostBaseFormat[]             = "xf";
char const GtBattlePetTypeDamageModFormat[]         = "xf";
char const GtBattlePetXpFormat[]                    = "xf";
char const GtCombatRatingsFormat[]                  = "xf";
char const GtOCTHpPerStaminaFormat[]                = "df";
char const GtChanceToMeleeCritBaseFormat[]          = "xf";
char const GtChanceToMeleeCritFormat[]              = "xf";
char const GtChanceToSpellCritBaseFormat[]          = "xf";
char const GtChanceToSpellCritFormat[]              = "xf";
char const GtItemSocketCostPerLevelFormat[]         = "xf";
char const GtNPCManaCostScalerFormat[]              = "xf";
char const GtOCTClassCombatRatingScalarFormat[]     = "df";
char const GtRegenMPPerSptFormat[]                  = "xf";
char const GtSpellScalingFormat[]                   = "df";
char const GtOCTBaseHPByClassFormat[]               = "df";
char const GtOCTBaseMPByClassFormat[]               = "df";
char const GuildPerkSpellsFormat[]                  = "nii";
char const GuildColorBackgroundFormat[]             = "nXXX";
char const GuildColorBorderFormat[]                 = "nXXX";
char const GuildColorEmblemFormat[]                 = "nXXX";
char const HolidaysFormat[]                         = "niiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiisiii";
char const ImportPriceArmorFormat[]                 = "nffff";
char const ImportPriceQualityFormat[]               = "nf";
char const ImportPriceShieldFormat[]                = "nf";
char const ImportPriceWeaponFormat[]                = "nf";
char const ItemPriceBaseFormat[]                    = "niff";
char const ItemReforgeFormat[]                      = "nifif";
char const ItemBagFamilyFormat[]                    = "nx";
char const ItemArmorQualityFormat[]                 = "nfffffffi";
char const ItemArmorShieldFormat[]                  = "nifffffff";
char const ItemArmorTotalFormat[]                   = "niffff";
char const ItemClassFormat[]                        = "difx";
char const ItemDamageFormat[]                       = "nfffffffi";
char const ItemDisenchantLootFormat[]               = "niiiiii";
char const ItemDisplayInfoFormat[]                  = "nssssssiiiiiiiisssssssssii";
char const ItemLimitCategoryEntryFormat[]           = "nsii";
char const ItemRandomPropertiesFormat[]             = "nsiiiiis";
char const ItemRandomSuffixFormat[]                 = "nssiiiiiiiiii";
char const ItemSetEntryFormat[]                     = "nsiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
char const ItemSpecEntryFormat[]                    = "niiiiii";
char const ItemSpecOverrideEntryFormat[]            = "nii";
char const LFGDungeonEntryFormat[]                  = "nsiiixxiiiiixixixxxxxxxxiixxx";
char const LightEntryFormat[]                       = "nifffxxxxxxxxxx";
char const LiquidTypeFormat[]                       = "nxxixixxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
char const LockEntryFormat[]                        = "niiiiiiiiiiiiiiiiiiiiiiiixxxxxxxx";
char const PhaseEntryFormat[]                       = "nsi";
char const PhaseGroupFormat[]                       = "nii";
char const MailTemplateEntryFormat[]                = "nxs";
char const MapEntryFormat[]                         = "nxiixsixxixiffxiiii";
char const MapDifficultyEntryFormat[]               = "diisiii";
char const ModifierTreeFormat[]                     = "niiiiii";
char const MovieEntryFormat[]                       = "nxxxx";
char const MountCapabilityFormat[]                  = "niiiiiii";
char const MountTypeFormat[]                        = "niiiiiiiiiiiiiiiiiiiiiiii";
char const NameGenFormat[]                          = "nsii";
char const NumTalentsAtLevelFormat[]                = "df";
char const OverrideSpellDataFormat[]                = "niiiiiiiiiiii";
char const QuestFactionRewardFormat[]               = "niiiiiiiiii";
char const QuestMoneyRewardFormat[]                 = "niiiiiiiiii";
char const QuestSortEntryFormat[]                   = "ns";
char const QuestV2Format[]                          = "ni";
char const QuestXPFormat[]                          = "niiiiiiiiii";
char const QuestPOIPointFormat[]                    = "diii";
char const PlayerConditionFormat[]                  = "niiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiisiii";
char const PowerDisplayFormat[]                     = "nixxxx";
char const PvPDifficultyFormat[]                    = "diiiii";
char const RandomPropertiesPointsFormat[]           = "niiiiiiiiiiiiiii";
char const ResearchBranchFormat[]                   = "nxxxxx";
char const ResearchProjectFormat[]                  = "nxxiixxxi";
char const ResearchSiteFormat[]                     = "niixx";
char const ScalingStatDistributionFormat[]          = "niiiiiiiiiiiiiiiiiiiiii";
char const ScalingStatValuesFormat[]                = "iniiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
char const ScenarioFormat[]                         = "nsi";
char const ScenarioStepFormat[]                     = "niiissi";
char const SkillLineFormat[]                        = "nisxixixx";
char const SkillLineAbilityFormat[]                 = "niiiiiiiiiiii";
char const SkillRaceClassInfoFormat[]               = "diiiiiii";
char const SkillTiersFormat[]                       = "niiiiiiiiiiiiiiii";
char const SoundEntriesFormat[]                     = "nisiiiiiiiiiiiiiiiiiiiififfiifffffi";
char const SpecializationSpellsFormat[]             = "niiix";
char const SpellCastTimeFormat[]                    = "niii";
char const SpellCategoriesEntryFormat[]             = "niiiiiiiii";
char const SpellCategoryFormat[]                    = "niXxii";
char const SpellDurationFormat[]                    = "niii";
char const SpellEffectEntryFormat[]                 = "iiifiiiffiiiiiifiifiiiiifiiiii";
const std::string CustomSpellEffectEntryFormat      = "appppppppppppppppppaaaapppppp";
const std::string CustomSpellEffectEntryIndex       = "";
char const SpellEntryFormat[]                       = "nsxxxiiifiiiiiiiiiiiiiiii";
const std::string CustomSpellEntryFormat            = "paaaaaaaaaaaaaaaaaaaaaaapa";
const std::string CustomSpellEntryIndex             = "Id";
char const SpellLearnSpellEntryFormat[]             = "niii";
char const SpellMiscEntryFormat[]                   = "nxxiiiiiiiiiiiiiiiiifiiiii";
const std::string CustomSpellMiscEntryFormat        = "paappppppppppppppppppaaaap";
const std::string CustomSpellMiscEntryIndex         = "Id";
char const SpellEffectScalingFormat[]               = "nfffxi";
char const SpellFocusObjectFormat[]                 = "nx";
char const SpellItemEnchantmentFormat[]             = "niiiiiiiiiisiiiiiiiiiiifff";
char const SpellItemEnchantmentConditionFormat[]    = "nbbbbbiiiiibbbbbbbbbbiiiiibbbbb";
char const SpellProcsPerMinuteFormat[]              = "nfi";
char const SpellProcsPerMinuteModFormat[]           = "niifi";
char const SpellRadiusFormat[]                      = "nffff";
char const SpellRangeFormat[]                       = "nffffiss";
char const SpellScalingEntryFormat[]                = "niiiifiii";
char const SpellTotemsEntryFormat[]                 = "niiii";
char const SpellTargetRestrictionsEntryFormat[]     = "niiffiiii";
char const SpellPowerEntryFormat[]                  = "niiiiiiiiffif";
char const SpellInterruptsEntryFormat[]             = "diiiiiii";
char const SpellEquippedItemsEntryFormat[]          = "diiiii";
char const SpellAuraOptionsEntryFormat[]            = "niiiiiiii";
char const SpellAuraRestrictionsEntryFormat[]       = "niiiiiiiiii";
char const SpellCastingRequirementsEntryFormat[]    = "niiiiii";
char const SpellClassOptionsEntryFormat[]           = "niiiiii";
char const SpellCooldownsEntryFormat[]              = "diiiii";
char const SpellKeyboundOverrideEntryFormat[]       = "ns";
char const SpellLevelsEntryFormat[]                 = "diiiii";
char const SpellRuneCostFormat[]                    = "niiiii";
char const SpellShapeshiftEntryFormat[]             = "niiiix";
char const SpellShapeshiftFormFormat[]              = "nxxiixiiiiiiiiiiiiixx";
char const SummonPropertiesFormat[]                 = "niiiii";
char const TalentEntryFormat[]                      = "niiiiiiiiix";
char const TaxiNodesEntryFormat[]                   = "nifffsiiiiff";
char const TaxiPathEntryFormat[]                    = "niii";
char const TaxiPathNodeEntryFormat[]                = "diiifffiiii";
char const TotemCategoryEntryFormat[]               = "nsii";
char const UnitPowerBarFormat[]                     = "niiiiffiiiiiiiiiiiiiissssff";
char const TransportAnimationFormat[]               = "niifffi";
char const TransportRotationFormat[]                = "niiffff";
char const VehicleEntryFormat[]                     = "niiffffiiiiiiiifffffffffffffffxxxxfifiiii";
char const VehicleSeatEntryFormat[]                 = "niiffffffffffiiiiiifffffffiiifffiiiiiiiffiiiiffffffffffffiiiiiiiii";
char const WMOAreaTableEntryFormat[]                = "niiixxxxxiixxxx";
char const WorldMapAreaEntryFormat[]                = "xinxffffixxxxx";
char const WorldMapOverlayEntryFormat[]             = "niiiiisiiiiiiiii";
char const WorldMapTransformsFormat[]               = "diffffffiffxxxf";
char const WorldSafeLocsEntryFormat[]               = "niffffx";

#endif
