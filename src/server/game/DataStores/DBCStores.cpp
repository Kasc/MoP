/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "DBCStores.h"
#include "Log.h"
#include "SharedDefines.h"
#include "SpellMgr.h"
#include "SpellInfo.h"
#include "TransportMgr.h"
#include "DBCFormat.h"
#include "ItemTemplate.h"
#include "Timer.h"
#include "ObjectDefines.h"

#include <map>

DBCStorage <AreaTableEntry>                     sAreaStore(AreaTableEntryFormat);
DBCStorage <AreaGroupEntry>                     sAreaGroupStore(AreaGroupEntryFormat);
DBCStorage <AchievementEntry>                   sAchievementStore(AchievementFormat);
DBCStorage <AchievementCategory>                sAchievementCategoryStore(AchievementCategoryFormat);
DBCStorage <AnimKitEntry>                       sAnimKitStore(AnimKitFormat);
DBCStorage <AreaTriggerEntry>                   sAreaTriggerStore(AreaTriggerEntryFormat);
DBCStorage <ArmorLocationEntry>                 sArmorLocationStore(ArmorLocationFormat);
DBCStorage <AuctionHouseEntry>                  sAuctionHouseStore(AuctionHouseEntryFormat);
DBCStorage <BankBagSlotPricesEntry>             sBankBagSlotPricesStore(BankBagSlotPricesEntryFormat);
DBCStorage <BannedAddOnsEntry>                  sBannedAddOnsStore(BannedAddOnsFormat);
DBCStorage <BattlemasterListEntry>              sBattlemasterListStore(BattlemasterListEntryFormat);
DBCStorage <BarberShopStyleEntry>               sBarberShopStyleStore(BarberShopStyleEntryFormat);
DBCStorage <CharSectionsEntry>                  sCharSectionsStore(CharSectionsFormat);
DBCStorage <CharStartOutfitEntry>               sCharStartOutfitStore(CharStartOutfitEntryFormat);
DBCStorage <CharTitlesEntry>                    sCharTitlesStore(CharTitlesEntryFormat);
DBCStorage <ChatChannelsEntry>                  sChatChannelsStore(ChatChannelsEntryFormat);
DBCStorage <ChrClassesEntry>                    sChrClassesStore(ChrClassesEntryFormat);
DBCStorage <ChrRacesEntry>                      sChrRacesStore(ChrRacesEntryFormat);
DBCStorage <ChrPowerTypesEntry>                 sChrPowerTypesStore(ChrClassesXPowerTypesFormat);
DBCStorage <ChrSpecializationEntry>             sChrSpecializationStore(ChrSpecializationFormat);
DBCStorage <CinematicCameraEntry>               sCinematicCameraStore(CinematicCameraEntryFormat);
DBCStorage <CinematicSequencesEntry>            sCinematicSequencesStore(CinematicSequencesEntryFormat);
DBCStorage <CreatureDisplayInfoEntry>           sCreatureDisplayInfoStore(CreatureDisplayInfoFormat);
DBCStorage <CreatureDisplayInfoExtraEntry>      sCreatureDisplayInfoExtraStore(CreatureDisplayInfoExtraFormat);
DBCStorage <CreatureFamilyEntry>                sCreatureFamilyStore(CreatureFamilyFormat);
DBCStorage <CreatureModelDataEntry>             sCreatureModelDataStore(CreatureModelDataFormat);
DBCStorage <CreatureSpellDataEntry>             sCreatureSpellDataStore(CreatureSpellDataFormat);
DBCStorage <CreatureTypeEntry>                  sCreatureTypeStore(CreatureTypeFormat);
DBCStorage <CurrencyTypesEntry>                 sCurrencyTypesStore(CurrencyTypesFormat);
DBCStorage <CriteriaEntry>                      sCriteriaStore(CriteriaFormat);
DBCStorage <CriteriaTreeEntry>                  sCriteriaTreeStore(CriteriaTreeFormat);
DBCStorage <DestructibleModelDataEntry>         sDestructibleModelDataStore(DestructibleModelDataFormat);
DBCStorage <DifficultyEntry>                    sDifficultyStore(DifficultyFormat);
DBCStorage <DungeonEncounterEntry>              sDungeonEncounterStore(DungeonEncounterFormat);
DBCStorage <DurabilityQualityEntry>             sDurabilityQualityStore(DurabilityQualityFormat);
DBCStorage <DurabilityCostsEntry>               sDurabilityCostsStore(DurabilityCostsFormat);
DBCStorage <EmotesEntry>                        sEmotesStore(EmotesEntryFormat);
DBCStorage <EmotesTextSoundEntry>               sEmotesTextSoundStore(EmotesTextSoundEntryFormat);
DBCStorage <EmotesTextEntry>                    sEmotesTextStore(EmotesTextEntryFormat);
DBCStorage <FactionEntry>                       sFactionStore(FactionEntryFormat);
DBCStorage <FactionTemplateEntry>               sFactionTemplateStore(FactionTemplateEntryFormat);
DBCStorage <FileDataEntry>                      sFileDataStore(FileDataFormat);
DBCStorage <GameObjectDisplayInfoEntry>         sGameObjectDisplayInfoStore(GameObjectDisplayInfoFormat);
DBCStorage <GameTablesEntry>                    sGameTablesStore(GameTablesFmt);
DBCStorage <GemPropertiesEntry>                 sGemPropertiesStore(GemPropertiesEntryFormat);
DBCStorage <GlyphPropertiesEntry>               sGlyphPropertiesStore(GlyphPropertiesFormat);
DBCStorage <GlyphSlotEntry>                     sGlyphSlotStore(GlyphSlotFormat);
DBCStorage <GuildPerkSpellsEntry>               sGuildPerkSpellsStore(GuildPerkSpellsFormat);
DBCStorage <GuildColorBackgroundEntry>          sGuildColorBackgroundStore(GuildColorBackgroundFormat);
DBCStorage <GuildColorBorderEntry>              sGuildColorBorderStore(GuildColorBorderFormat);
DBCStorage <GuildColorEmblemEntry>              sGuildColorEmblemStore(GuildColorEmblemFormat);
DBCStorage <HolidaysEntry>                      sHolidaysStore(HolidaysFormat);
DBCStorage <ImportPriceArmorEntry>              sImportPriceArmorStore(ImportPriceArmorFormat);
DBCStorage <ImportPriceQualityEntry>            sImportPriceQualityStore(ImportPriceQualityFormat);
DBCStorage <ImportPriceShieldEntry>             sImportPriceShieldStore(ImportPriceShieldFormat);
DBCStorage <ImportPriceWeaponEntry>             sImportPriceWeaponStore(ImportPriceWeaponFormat);
DBCStorage <ItemPriceBaseEntry>                 sItemPriceBaseStore(ItemPriceBaseFormat);
DBCStorage <ItemReforgeEntry>                   sItemReforgeStore(ItemReforgeFormat);
DBCStorage <ItemArmorQualityEntry>              sItemArmorQualityStore(ItemArmorQualityFormat);
DBCStorage <ItemArmorShieldEntry>               sItemArmorShieldStore(ItemArmorShieldFormat);
DBCStorage <ItemArmorTotalEntry>                sItemArmorTotalStore(ItemArmorTotalFormat);
DBCStorage <ItemClassEntry>                     sItemClassStore(ItemClassFormat);
DBCStorage <ItemBagFamilyEntry>                 sItemBagFamilyStore(ItemBagFamilyFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageAmmoStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageOneHandStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageOneHandCasterStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageRangedStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageThrownStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageTwoHandStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageTwoHandCasterStore(ItemDamageFormat);
DBCStorage <ItemDamageEntry>                    sItemDamageWandStore(ItemDamageFormat);
DBCStorage <ItemDisenchantLootEntry>            sItemDisenchantLootStore(ItemDisenchantLootFormat);
DBCStorage <ItemDisplayInfoEntry>               sItemDisplayInfoStore(ItemDisplayInfoFormat);
DBCStorage <ItemLimitCategoryEntry>             sItemLimitCategoryStore(ItemLimitCategoryEntryFormat);
DBCStorage <ItemRandomPropertiesEntry>          sItemRandomPropertiesStore(ItemRandomPropertiesFormat);
DBCStorage <ItemRandomSuffixEntry>              sItemRandomSuffixStore(ItemRandomSuffixFormat);
DBCStorage <ItemSetEntry>                       sItemSetStore(ItemSetEntryFormat);
DBCStorage <ItemSpecOverrideEntry>              sItemSpecOverrideStore(ItemSpecOverrideEntryFormat);
DBCStorage <ItemSpecEntry>                      sItemSpecStore(ItemSpecEntryFormat);
DBCStorage <LFGDungeonEntry>                    sLFGDungeonStore(LFGDungeonEntryFormat);
DBCStorage <LightEntry>                         sLightStore(LightEntryFormat);
DBCStorage <LiquidTypeEntry>                    sLiquidTypeStore(LiquidTypeFormat);
DBCStorage <LockEntry>                          sLockStore(LockEntryFormat);
DBCStorage <MailTemplateEntry>                  sMailTemplateStore(MailTemplateEntryFormat);
DBCStorage <MapEntry>                           sMapStore(MapEntryFormat);
DBCStorage <MapDifficultyEntry>                 sMapDifficultyStore(MapDifficultyEntryFormat);
DBCStorage <ModifierTreeEntry>                  sModifierTreeStore(ModifierTreeFormat);
DBCStorage <MovieEntry>                         sMovieStore(MovieEntryFormat);
DBCStorage <MountCapabilityEntry>               sMountCapabilityStore(MountCapabilityFormat);
DBCStorage <MountTypeEntry>                     sMountTypeStore(MountTypeFormat);
DBCStorage <NameGenEntry>                       sNameGenStore(NameGenFormat);
DBCStorage <OverrideSpellDataEntry>             sOverrideSpellDataStore(OverrideSpellDataFormat);
DBCStorage <PlayerConditionEntry>               sPlayerConditionStore(PlayerConditionFormat);
DBCStorage <PhaseEntry>                         sPhaseStore(PhaseEntryFormat);
DBCStorage <PhaseGroupEntry>                    sPhaseGroupStore(PhaseGroupFormat);
DBCStorage <PowerDisplayEntry>                  sPowerDisplayStore(PowerDisplayFormat);
DBCStorage <PvPDifficultyEntry>                 sPvPDifficultyStore(PvPDifficultyFormat);
DBCStorage <QuestMoneyRewardEntry>              sQuestMoneyRewardStore(QuestMoneyRewardFormat);
DBCStorage <QuestSortEntry>                     sQuestSortStore(QuestSortEntryFormat);
DBCStorage <QuestV2Entry>                       sQuestV2Store(QuestV2Format);
DBCStorage <QuestXPEntry>                       sQuestXPStore(QuestXPFormat);
DBCStorage <QuestFactionRewEntry>               sQuestFactionRewardStore(QuestFactionRewardFormat);
DBCStorage <QuestPOIPointEntry>                 sQuestPOIPointStore(QuestPOIPointFormat);
DBCStorage <RandomPropertiesPointsEntry>        sRandomPropertiesPointsStore(RandomPropertiesPointsFormat);
DBCStorage <ResearchBranchEntry>                sResearchBranchStore(ResearchBranchFormat);
DBCStorage <ResearchProjectEntry>               sResearchProjectStore(ResearchProjectFormat);
DBCStorage <ResearchSiteEntry>                  sResearchSiteStore(ResearchSiteFormat);
DBCStorage <ScalingStatDistributionEntry>       sScalingStatDistributionStore(ScalingStatDistributionFormat);
DBCStorage <ScalingStatValuesEntry>             sScalingStatValuesStore(ScalingStatValuesFormat);
DBCStorage <ScenarioEntry>                      sScenarioStore(ScenarioFormat);
DBCStorage <ScenarioStepEntry>                  sScenarioStepStore(ScenarioStepFormat);
DBCStorage <SkillLineEntry>                     sSkillLineStore(SkillLineFormat);
DBCStorage <SkillLineAbilityEntry>              sSkillLineAbilityStore(SkillLineAbilityFormat);
DBCStorage <SkillRaceClassInfoEntry>            sSkillRaceClassInfoStore(SkillRaceClassInfoFormat);
DBCStorage <SkillTiersEntry>                    sSkillTiersStore(SkillTiersFormat);
DBCStorage <SoundEntriesEntry>                  sSoundEntriesStore(SoundEntriesFormat);
DBCStorage <SpecializationSpellsEntry>          sSpecializationSpellsStore(SpecializationSpellsFormat);
DBCStorage <SpellItemEnchantmentEntry>          sSpellItemEnchantmentStore(SpellItemEnchantmentFormat);
DBCStorage <SpellItemEnchantmentConditionEntry> sSpellItemEnchantmentConditionStore(SpellItemEnchantmentConditionFormat);
DBCStorage <SpellEntry>                         sSpellStore(SpellEntryFormat);
DBCStorage <SpellMiscEntry>                     sSpellMiscStore(SpellMiscEntryFormat);
DBCStorage <SpellEffectScalingEntry>            sSpellEffectScalingStore(SpellEffectScalingFormat);
DBCStorage <SpellScalingEntry>                  sSpellScalingStore(SpellScalingEntryFormat);
DBCStorage <SpellTotemsEntry>                   sSpellTotemsStore(SpellTotemsEntryFormat);
DBCStorage <SpellTargetRestrictionsEntry>       sSpellTargetRestrictionsStore(SpellTargetRestrictionsEntryFormat);
DBCStorage <SpellLearnSpellEntry>               sSpellLearnSpellStore(SpellLearnSpellEntryFormat);
DBCStorage <SpellPowerEntry>                    sSpellPowerStore(SpellPowerEntryFormat);
DBCStorage <SpellKeyboundOverrideEntry>         sSpellKeyboundOverrideStore(SpellKeyboundOverrideEntryFormat);
DBCStorage <SpellLevelsEntry>                   sSpellLevelsStore(SpellLevelsEntryFormat);
DBCStorage <SpellInterruptsEntry>               sSpellInterruptsStore(SpellInterruptsEntryFormat);
DBCStorage <SpellEquippedItemsEntry>            sSpellEquippedItemsStore(SpellEquippedItemsEntryFormat);
DBCStorage <SpellClassOptionsEntry>             sSpellClassOptionsStore(SpellClassOptionsEntryFormat);
DBCStorage <SpellCooldownsEntry>                sSpellCooldownsStore(SpellCooldownsEntryFormat);
DBCStorage <SpellAuraOptionsEntry>              sSpellAuraOptionsStore(SpellAuraOptionsEntryFormat);
DBCStorage <SpellAuraRestrictionsEntry>         sSpellAuraRestrictionsStore(SpellAuraRestrictionsEntryFormat);
DBCStorage <SpellCastingRequirementsEntry>      sSpellCastingRequirementsStore(SpellCastingRequirementsEntryFormat);
DBCStorage <SpellCastTimesEntry>                sSpellCastTimesStore(SpellCastTimeFormat);
DBCStorage <SpellCategoriesEntry>               sSpellCategoriesStore(SpellCategoriesEntryFormat);
DBCStorage <SpellCategoryEntry>                 sSpellCategoryStore(SpellCategoryFormat);
DBCStorage <SpellEffectEntry>                   sSpellEffectStore(SpellEffectEntryFormat);
DBCStorage <SpellDurationEntry>                 sSpellDurationStore(SpellDurationFormat);
DBCStorage <SpellFocusObjectEntry>              sSpellFocusObjectStore(SpellFocusObjectFormat);
DBCStorage <SpellProcsPerMinuteEntry>           sSpellProcsPerMinuteStore(SpellProcsPerMinuteFormat);
DBCStorage <SpellProcsPerMinuteModEntry>        sSpellProcsPerMinuteModStore(SpellProcsPerMinuteModFormat);
DBCStorage <SpellRadiusEntry>                   sSpellRadiusStore(SpellRadiusFormat);
DBCStorage <SpellRangeEntry>                    sSpellRangeStore(SpellRangeFormat);
DBCStorage <SpellRuneCostEntry>                 sSpellRuneCostStore(SpellRuneCostFormat);
DBCStorage <SpellShapeshiftEntry>               sSpellShapeshiftStore(SpellShapeshiftEntryFormat);
DBCStorage <SpellShapeshiftFormEntry>           sSpellShapeshiftFormStore(SpellShapeshiftFormFormat);
DBCStorage <SummonPropertiesEntry>              sSummonPropertiesStore(SummonPropertiesFormat);
DBCStorage <TalentEntry>                        sTalentStore(TalentEntryFormat);
DBCStorage <TaxiNodesEntry>                     sTaxiNodesStore(TaxiNodesEntryFormat);
DBCStorage <TaxiPathEntry>                      sTaxiPathStore(TaxiPathEntryFormat);
DBCStorage <TaxiPathNodeEntry>                  sTaxiPathNodeStore(TaxiPathNodeEntryFormat);
DBCStorage <TotemCategoryEntry>                 sTotemCategoryStore(TotemCategoryEntryFormat);
DBCStorage <TransportAnimationEntry>            sTransportAnimationStore(TransportAnimationFormat);
DBCStorage <TransportRotationEntry>             sTransportRotationStore(TransportRotationFormat);
DBCStorage <UnitPowerBarEntry>                  sUnitPowerBarStore(UnitPowerBarFormat);
DBCStorage <VehicleEntry>                       sVehicleStore(VehicleEntryFormat);
DBCStorage <VehicleSeatEntry>                   sVehicleSeatStore(VehicleSeatEntryFormat);
DBCStorage <WMOAreaTableEntry>                  sWMOAreaTableStore(WMOAreaTableEntryFormat);
DBCStorage <WorldMapAreaEntry>                  sWorldMapAreaStore(WorldMapAreaEntryFormat);
DBCStorage <WorldMapOverlayEntry>               sWorldMapOverlayStore(WorldMapOverlayEntryFormat);
DBCStorage <WorldMapTransformsEntry>            sWorldMapTransformsStore(WorldMapTransformsFormat);
DBCStorage <WorldSafeLocsEntry>                 sWorldSafeLocsStore(WorldSafeLocsEntryFormat);

GameTable <GtBarberShopCostBaseEntry>           sGtBarberShopCostBaseStore(GtBarberShopCostBaseFormat);
GameTable <GtBattlePetTypeDamageModEntry>       sGtBattlePetTypeDamageModStore(GtBattlePetTypeDamageModFormat);
GameTable <GtBattlePetXpEntry>                  sGtBattlePetXpStore(GtBattlePetXpFormat);
GameTable <GtCombatRatingsEntry>                sGtCombatRatingsStore(GtCombatRatingsFormat);
GameTable <GtChanceToMeleeCritBaseEntry>        sGtChanceToMeleeCritBaseStore(GtChanceToMeleeCritBaseFormat);
GameTable <GtChanceToMeleeCritEntry>            sGtChanceToMeleeCritStore(GtChanceToMeleeCritFormat);
GameTable <GtChanceToSpellCritBaseEntry>        sGtChanceToSpellCritBaseStore(GtChanceToSpellCritBaseFormat);
GameTable <GtChanceToSpellCritEntry>            sGtChanceToSpellCritStore(GtChanceToSpellCritFormat);
GameTable <GtItemSocketCostPerLevelEntry>       sGtItemSocketCostPerLevelStore(GtItemSocketCostPerLevelFormat);
GameTable <GtNPCManaCostScalerEntry>            sGtNPCManaCostScalerStore(GtNPCManaCostScalerFormat);
GameTable <GtOCTClassCombatRatingScalarEntry>   sGtOCTClassCombatRatingScalarStore(GtOCTClassCombatRatingScalarFormat);
GameTable <GtOCTHpPerStaminaEntry>              sGtOCTHpPerStaminaStore(GtOCTHpPerStaminaFormat);
GameTable <GtRegenMPPerSptEntry>                sGtRegenMPPerSptStore(GtRegenMPPerSptFormat);
GameTable <GtSpellScalingEntry>                 sGtSpellScalingStore(GtSpellScalingFormat);
GameTable <GtOCTBaseHPByClassEntry>             sGtOCTBaseHPByClassStore(GtOCTBaseHPByClassFormat);
GameTable <GtOCTBaseMPByClassEntry>             sGtOCTBaseMPByClassStore(GtOCTBaseMPByClassFormat);

static bool LoadDBC_assert_print(uint32 fsize, uint32 rsize, const std::string& filename)
{
    TC_LOG_ERROR("misc", "Size of '%s' set by format string (%u) not equal size of C++ structure (%u).", filename.c_str(), fsize, rsize);

    // ASSERT must fail after function call
    return false;
}

DBCManager::~DBCManager() { }

DBCManager::DBCManager()
{
    DBCFileCount = 0;
}

template<class T>
void DBCManager::LoadDBC(uint32& availableDbcLocales, DBCStoreProblemList& errors, DBCStorage<T>& storage, std::string const& dbcPath, std::string const& filename, std::string const* customFormat, std::string const* customIndexName)
{
    ASSERT(DBCFileLoader::GetFormatRecordSize(storage.GetFormat()) == sizeof(T) || LoadDBC_assert_print(DBCFileLoader::GetFormatRecordSize(storage.GetFormat()), sizeof(T), filename));

    ++DBCFileCount;
    std::string dbcFilename = dbcPath + filename;
    SqlDbc * sql = NULL;
    if (customFormat)
        sql = new SqlDbc(&filename, customFormat, customIndexName, storage.GetFormat());

    if (storage.Load(dbcFilename.c_str(), sql))
    {
        for (uint8 i = 0; i < TOTAL_LOCALES; ++i)
        {
            if (!(availableDbcLocales & (1 << i)))
                continue;

            std::string localizedName(dbcPath);
            localizedName.append(localeNames[i]);
            localizedName.push_back('/');
            localizedName.append(filename);

            if (!storage.LoadStringsFrom(localizedName.c_str()))
                availableDbcLocales &= ~(1<<i);             // mark as not available for speedup next checks
        }
    }
    else
    {
        // sort problematic dbc to (1) non compatible and (2) non-existed
        if (FILE* f = fopen(dbcFilename.c_str(), "rb"))
        {
            std::ostringstream stream;
            stream << dbcFilename << " exists, and has " << storage.GetFieldCount() << " field(s) (expected " << strlen(storage.GetFormat()) << "). Extracted file might be from wrong client version or a database-update has been forgotten.";
            std::string buf = stream.str();
            errors.push_back(buf);
            fclose(f);
        }
        else
            errors.push_back(dbcFilename);
    }

    delete sql;
}

template<class T>
void DBCManager::LoadGameTable(DBCStoreProblemList& errors, std::string const& tableName, GameTable<T>& storage, std::string const& dbcPath, std::string const& filename)
{
    // compatibility format and C++ structure sizes
    ASSERT(DBCFileLoader::GetFormatRecordSize(storage.GetFormat()) == sizeof(T) || LoadDBC_assert_print(DBCFileLoader::GetFormatRecordSize(storage.GetFormat()), sizeof(T), filename));

    ++DBCFileCount;
    std::string dbcFilename = dbcPath + filename;

    if (storage.Load(dbcFilename.c_str()))
    {
        bool found = false;
        // Find table definition in GameTables.dbc
        for (GameTablesEntry const* gt : sGameTablesStore)
            if (tableName == gt->Name)
            {
                found = true;
                storage.SetGameTableEntry(gt);
                break;
            }

        ASSERT(found && "Game table definition not found in GameTables.dbc");
    }
    else
    {
        // sort problematic dbc to (1) non compatible and (2) non-existed
        if (FILE* f = fopen(dbcFilename.c_str(), "rb"))
        {
            std::ostringstream stream;
            stream << dbcFilename << " exists, and has " << storage.GetFieldCount() << " field(s) (expected " << strlen(storage.GetFormat()) << "). Extracted file might be from wrong client version or a database-update has been forgotten.";
            std::string buf = stream.str();
            errors.push_back(buf);
            fclose(f);
        }
        else
            errors.push_back(dbcFilename);
    }
}

void DBCManager::LoadDBCStores(const std::string& dataPath)
{
    uint32 oldMSTime = getMSTime();

    std::string dbcPath = dataPath + "dbc/";

    DBCStoreProblemList bad_dbc_files;
    uint32 availableDbcLocales = 0xFFFFFFFF;

    LoadDBC(availableDbcLocales, bad_dbc_files, sAreaStore,                             dbcPath, "AreaTable.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sAchievementStore,                      dbcPath, "Achievement.dbc"/*, &CustomAchievementFormat, &CustomAchievementIndex*/); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sAchievementCategoryStore,              dbcPath, "Achievement_Category.dbc"         ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sAnimKitStore,                          dbcPath, "AnimKit.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sAreaTriggerStore,                      dbcPath, "AreaTrigger.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sAreaGroupStore,                        dbcPath, "AreaGroup.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sAuctionHouseStore,                     dbcPath, "AuctionHouse.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sArmorLocationStore,                    dbcPath, "ArmorLocation.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sBankBagSlotPricesStore,                dbcPath, "BankBagSlotPrices.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sBannedAddOnsStore,                     dbcPath, "BannedAddOns.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sBattlemasterListStore,                 dbcPath, "BattleMasterList.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sBarberShopStyleStore,                  dbcPath, "BarberShopStyle.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCharSectionsStore,                     dbcPath, "CharSections.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCharStartOutfitStore,                  dbcPath, "CharStartOutfit.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCharTitlesStore,                       dbcPath, "CharTitles.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sChatChannelsStore,                     dbcPath, "ChatChannels.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sChrClassesStore,                       dbcPath, "ChrClasses.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sChrRacesStore,                         dbcPath, "ChrRaces.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sChrPowerTypesStore,                    dbcPath, "ChrClassesXPowerTypes.dbc"        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCinematicCameraStore,                  dbcPath, "CinematicCamera.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCinematicSequencesStore,               dbcPath, "CinematicSequences.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCreatureDisplayInfoStore,              dbcPath, "CreatureDisplayInfo.dbc"          ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCreatureDisplayInfoExtraStore,         dbcPath, "CreatureDisplayInfoExtra.dbc"     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCreatureFamilyStore,                   dbcPath, "CreatureFamily.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCreatureModelDataStore,                dbcPath, "CreatureModelData.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCreatureSpellDataStore,                dbcPath, "CreatureSpellData.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCreatureTypeStore,                     dbcPath, "CreatureType.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCurrencyTypesStore,                    dbcPath, "CurrencyTypes.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCriteriaStore,                         dbcPath, "Criteria.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sCriteriaTreeStore,                     dbcPath, "CriteriaTree.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sDestructibleModelDataStore,            dbcPath, "DestructibleModelData.dbc"        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sDifficultyStore,                       dbcPath, "Difficulty.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sDungeonEncounterStore,                 dbcPath, "DungeonEncounter.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sDurabilityCostsStore,                  dbcPath, "DurabilityCosts.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sDurabilityQualityStore,                dbcPath, "DurabilityQuality.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sEmotesStore,                           dbcPath, "Emotes.dbc"                       ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sEmotesTextStore,                       dbcPath, "EmotesText.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sEmotesTextSoundStore,                  dbcPath, "EmotesTextSound.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sFactionStore,                          dbcPath, "Faction.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sFactionTemplateStore,                  dbcPath, "FactionTemplate.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sFileDataStore,                         dbcPath, "FileData.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGameObjectDisplayInfoStore,            dbcPath, "GameObjectDisplayInfo.dbc"        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGameTablesStore,                       dbcPath, "GameTables.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGemPropertiesStore,                    dbcPath, "GemProperties.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGlyphPropertiesStore,                  dbcPath, "GlyphProperties.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGlyphSlotStore,                        dbcPath, "GlyphSlot.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGuildPerkSpellsStore,                  dbcPath, "GuildPerkSpells.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGuildColorBackgroundStore,             dbcPath, "GuildColorBackground.dbc"         ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGuildColorBorderStore,                 dbcPath, "GuildColorBorder.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sGuildColorEmblemStore,                 dbcPath, "GuildColorEmblem.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sHolidaysStore,                         dbcPath, "Holidays.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sImportPriceArmorStore,                 dbcPath, "ImportPriceArmor.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sImportPriceQualityStore,               dbcPath, "ImportPriceQuality.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sImportPriceShieldStore,                dbcPath, "ImportPriceShield.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sImportPriceWeaponStore,                dbcPath, "ImportPriceWeapon.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemPriceBaseStore,                    dbcPath, "ItemPriceBase.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemReforgeStore,                      dbcPath, "ItemReforge.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemBagFamilyStore,                    dbcPath, "ItemBagFamily.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemClassStore,                        dbcPath, "ItemClass.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemLimitCategoryStore,                dbcPath, "ItemLimitCategory.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemRandomPropertiesStore,             dbcPath, "ItemRandomProperties.dbc"         ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemRandomSuffixStore,                 dbcPath, "ItemRandomSuffix.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemSetStore,                          dbcPath, "ItemSet.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemSpecStore,                         dbcPath, "ItemSpec.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemSpecOverrideStore,                 dbcPath, "ItemSpecOverride.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemArmorQualityStore,                 dbcPath, "ItemArmorQuality.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemArmorShieldStore,                  dbcPath, "ItemArmorShield.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemArmorTotalStore,                   dbcPath, "ItemArmorTotal.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageAmmoStore,                   dbcPath, "ItemDamageAmmo.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageOneHandStore,                dbcPath, "ItemDamageOneHand.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageOneHandCasterStore,          dbcPath, "ItemDamageOneHandCaster.dbc"      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageRangedStore,                 dbcPath, "ItemDamageRanged.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageThrownStore,                 dbcPath, "ItemDamageThrown.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageTwoHandStore,                dbcPath, "ItemDamageTwoHand.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageTwoHandCasterStore,          dbcPath, "ItemDamageTwoHandCaster.dbc"      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDamageWandStore,                   dbcPath, "ItemDamageWand.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDisenchantLootStore,               dbcPath, "ItemDisenchantLoot.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sItemDisplayInfoStore,                  dbcPath, "ItemDisplayInfo.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sLFGDungeonStore,                       dbcPath, "LfgDungeons.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sLightStore,                            dbcPath, "Light.dbc"                        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sLiquidTypeStore,                       dbcPath, "LiquidType.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sLockStore,                             dbcPath, "Lock.dbc"                         ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sMailTemplateStore,                     dbcPath, "MailTemplate.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sMapStore,                              dbcPath, "Map.dbc"                          ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sMapDifficultyStore,                    dbcPath, "MapDifficulty.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sMountCapabilityStore,                  dbcPath, "MountCapability.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sMountTypeStore,                        dbcPath, "MountType.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sNameGenStore,                          dbcPath, "NameGen.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sModifierTreeStore,                     dbcPath, "ModifierTree.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sMovieStore,                            dbcPath, "Movie.dbc"                        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sOverrideSpellDataStore,                dbcPath, "OverrideSpellData.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sPlayerConditionStore,                  dbcPath, "PlayerCondition.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sPowerDisplayStore,                     dbcPath, "PowerDisplay.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sPhaseStore,                            dbcPath, "Phase.dbc"                        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sPhaseGroupStore,                       dbcPath, "PhaseXPhaseGroup.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sPvPDifficultyStore,                    dbcPath, "PvpDifficulty.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sQuestMoneyRewardStore,                 dbcPath, "QuestMoneyReward.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sQuestXPStore,                          dbcPath, "QuestXP.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sQuestFactionRewardStore,               dbcPath, "QuestFactionReward.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sQuestPOIPointStore,                    dbcPath, "QuestPOIPoint.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sQuestSortStore,                        dbcPath, "QuestSort.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sQuestV2Store,                          dbcPath, "QuestV2.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sRandomPropertiesPointsStore,           dbcPath, "RandPropPoints.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sResearchBranchStore,                   dbcPath, "ResearchBranch.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sResearchProjectStore,                  dbcPath, "ResearchProject.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sResearchSiteStore,                     dbcPath, "ResearchSite.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sScalingStatDistributionStore,          dbcPath, "ScalingStatDistribution.dbc"      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sScalingStatValuesStore,                dbcPath, "ScalingStatValues.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sScenarioStore,                         dbcPath, "Scenario.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sScenarioStepStore,                     dbcPath, "ScenarioStep.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSkillLineStore,                        dbcPath, "SkillLine.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSkillLineAbilityStore,                 dbcPath, "SkillLineAbility.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSkillRaceClassInfoStore,               dbcPath, "SkillRaceClassInfo.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSkillTiersStore,                       dbcPath, "SkillTiers.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSoundEntriesStore,                     dbcPath, "SoundEntries.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellStore,                            dbcPath, "Spell.dbc", &CustomSpellEntryFormat, &CustomSpellEntryIndex); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellCategoriesStore,                  dbcPath, "SpellCategories.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellCategoryStore,                    dbcPath, "SpellCategory.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellScalingStore,                     dbcPath, "SpellScaling.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellTotemsStore,                      dbcPath, "SpellTotems.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellTargetRestrictionsStore,          dbcPath, "SpellTargetRestrictions.dbc"      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellPowerStore,                       dbcPath, "SpellPower.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellKeyboundOverrideStore,            dbcPath, "SpellKeyboundOverride.dbc"        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellLevelsStore,                      dbcPath, "SpellLevels.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellInterruptsStore,                  dbcPath, "SpellInterrupts.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellEquippedItemsStore,               dbcPath, "SpellEquippedItems.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellClassOptionsStore,                dbcPath, "SpellClassOptions.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellCooldownsStore,                   dbcPath, "SpellCooldowns.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellAuraOptionsStore,                 dbcPath, "SpellAuraOptions.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellAuraRestrictionsStore,            dbcPath, "SpellAuraRestrictions.dbc"        ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellCastingRequirementsStore,         dbcPath, "SpellCastingRequirements.dbc"     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellEffectStore,                      dbcPath, "SpellEffect.dbc", &CustomSpellEffectEntryFormat, &CustomSpellEffectEntryIndex); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellCastTimesStore,                   dbcPath, "SpellCastTimes.dbc"               ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellDurationStore,                    dbcPath, "SpellDuration.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellFocusObjectStore,                 dbcPath, "SpellFocusObject.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellItemEnchantmentStore,             dbcPath, "SpellItemEnchantment.dbc"         ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellMiscStore,                        dbcPath, "SpellMisc.dbc", &CustomSpellMiscEntryFormat, &CustomSpellMiscEntryIndex); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellEffectScalingStore,               dbcPath, "SpellEffectScaling.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellItemEnchantmentConditionStore,    dbcPath, "SpellItemEnchantmentCondition.dbc"); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellProcsPerMinuteStore,              dbcPath, "SpellProcsPerMinute.dbc"          ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellProcsPerMinuteModStore,           dbcPath, "SpellProcsPerMinuteMod.dbc"       ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellRadiusStore,                      dbcPath, "SpellRadius.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellRangeStore,                       dbcPath, "SpellRange.dbc"                   ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellRuneCostStore,                    dbcPath, "SpellRuneCost.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellShapeshiftStore,                  dbcPath, "SpellShapeshift.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpellShapeshiftFormStore,              dbcPath, "SpellShapeshiftForm.dbc"          ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSummonPropertiesStore,                 dbcPath, "SummonProperties.dbc"             ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTalentStore,                           dbcPath, "Talent.dbc"                       ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sChrSpecializationStore,                dbcPath, "ChrSpecialization.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sSpecializationSpellsStore,             dbcPath, "SpecializationSpells.dbc"         ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTaxiNodesStore,                        dbcPath, "TaxiNodes.dbc"                    ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTaxiPathStore,                         dbcPath, "TaxiPath.dbc"                     ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTaxiPathNodeStore,                     dbcPath, "TaxiPathNode.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTotemCategoryStore,                    dbcPath, "TotemCategory.dbc"                ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTransportAnimationStore,               dbcPath, "TransportAnimation.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sTransportRotationStore,                dbcPath, "TransportRotation.dbc"            ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sUnitPowerBarStore,                     dbcPath, "UnitPowerBar.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sVehicleStore,                          dbcPath, "Vehicle.dbc"                      ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sVehicleSeatStore,                      dbcPath, "VehicleSeat.dbc"                  ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sWMOAreaTableStore,                     dbcPath, "WMOAreaTable.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sWorldMapAreaStore,                     dbcPath, "WorldMapArea.dbc"                 ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sWorldMapOverlayStore,                  dbcPath, "WorldMapOverlay.dbc"              ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sWorldMapTransformsStore,               dbcPath, "WorldMapTransforms.dbc"           ); // 5.4.8 18414
    LoadDBC(availableDbcLocales, bad_dbc_files, sWorldSafeLocsStore,                    dbcPath, "WorldSafeLocs.dbc"                ); // 5.4.8 18414

    LoadGameTable(bad_dbc_files, "BarberShopCostBase",          sGtBarberShopCostBaseStore,         dbcPath, "gtBarberShopCostBase.dbc"         ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "BattlePetTypeDamageMod",      sGtBattlePetTypeDamageModStore,     dbcPath, "gtBattlePetTypeDamageMod.dbc"     ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "BattlePetXP",                 sGtBattlePetXpStore,                dbcPath, "gtBattlePetXP.dbc"                ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "CombatRatings",               sGtCombatRatingsStore,              dbcPath, "gtCombatRatings.dbc"              ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "ChanceToMeleeCritBase",       sGtChanceToMeleeCritBaseStore,      dbcPath, "gtChanceToMeleeCritBase.dbc"      ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "ChanceToMeleeCrit",           sGtChanceToMeleeCritStore,          dbcPath, "gtChanceToMeleeCrit.dbc"          ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "ChanceToSpellCritBase",       sGtChanceToSpellCritBaseStore,      dbcPath, "gtChanceToSpellCritBase.dbc"      ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "ChanceToSpellCrit",           sGtChanceToSpellCritStore,          dbcPath, "gtChanceToSpellCrit.dbc"          ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "NPCManaCostScaler",           sGtNPCManaCostScalerStore,          dbcPath, "gtNPCManaCostScaler.dbc"          ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "OCTClassCombatRatingScalar",  sGtOCTClassCombatRatingScalarStore, dbcPath, "gtOCTClassCombatRatingScalar.dbc" ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "OCTHPPerStamina",             sGtOCTHpPerStaminaStore,            dbcPath, "gtOCTHpPerStamina.dbc"            ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "RegenMPPerSpt",               sGtRegenMPPerSptStore,              dbcPath, "gtRegenMPPerSpt.dbc"              ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "SpellScaling",                sGtSpellScalingStore,               dbcPath, "gtSpellScaling.dbc"               ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "OCTBaseHPByClass",            sGtOCTBaseHPByClassStore,           dbcPath, "gtOCTBaseHPByClass.dbc"           ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "OCTBaseMPByClass",            sGtOCTBaseMPByClassStore,           dbcPath, "gtOCTBaseMPByClass.dbc"           ); // 5.4.8 18414
    LoadGameTable(bad_dbc_files, "ItemSocketCostPerLevel",      sGtItemSocketCostPerLevelStore,     dbcPath, "gtItemSocketCostPerLevel.dbc"     ); // 5.4.8 18414

    for (CharSectionsEntry const* entry : sCharSectionsStore)
        if (entry->Race && ((1 << (entry->Race - 1)) & RACEMASK_ALL_PLAYABLE) != 0) //ignore Nonplayable races
            sCharSectionMap.insert({ entry->GenType | (entry->Gender << 8) | (entry->Race << 16), entry });

    for (CharStartOutfitEntry const* outfit : sCharStartOutfitStore)
        sCharStartOutfitMap[outfit->Race | (outfit->Class << 8) | (outfit->Gender << 16)] = outfit;

    for (uint32 i = 0; i < MAX_CLASSES; ++i)
        for (uint32 j = 0; j < MAX_POWERS; ++j)
            PowersByClass[i][j] = MAX_POWERS;

    for (ChrPowerTypesEntry const* power : sChrPowerTypesStore)
    {
        uint32 index = 0;
        for (uint32 j = 0; j < MAX_POWERS; ++j)
            if (PowersByClass[power->ClassID][j] != MAX_POWERS)
                ++index;

        PowersByClass[power->ClassID][power->PowerType] = index;
    }

    for (EmotesTextSoundEntry const* entry : sEmotesTextSoundStore)
        sEmotesTextSoundMap[EmotesTextSoundKey(entry->EmotesTextId, entry->RaceId, entry->SexId)] = entry;

    for (uint32 i = 0; i < sFactionStore.GetNumRows(); ++i)
    {
        FactionEntry const* faction = sFactionStore.LookupEntry(i);
        if (faction && faction->ParentFactionID)
        {
            SimpleFactionsList &flist = sFactionTeamMap[faction->ParentFactionID];
            flist.push_back(i);
        }
    }

    for (GameObjectDisplayInfoEntry const* info : sGameObjectDisplayInfoStore)
    {
        if (info->GeoBoxMax.X < info->GeoBoxMin.X)
            std::swap(*(float*)(&info->GeoBoxMax.X), *(float*)(&info->GeoBoxMin.X));
        if (info->GeoBoxMax.Y < info->GeoBoxMin.Y)
            std::swap(*(float*)(&info->GeoBoxMax.Y), *(float*)(&info->GeoBoxMin.Y));
        if (info->GeoBoxMax.Z < info->GeoBoxMin.Z)
            std::swap(*(float*)(&info->GeoBoxMax.Z), *(float*)(&info->GeoBoxMin.Z));
    }

    for (ItemSpecOverrideEntry const* entry : sItemSpecOverrideStore)
        sItemSpecOverridesStore[entry->ItemID].push_back(entry);

    for (MapDifficultyEntry const* entry : sMapDifficultyStore)
        sMapDifficultyMap[entry->MapID].push_back(entry);

    //map 0 is missing from MapDifficulty.dbc use this till its ported to sql
    sMapDifficultyMap[0].push_back(sMapDifficultyMap[1][0]);

    // Most scenarios are missing from MapDifficulty.dbc - fill data manually from LFGDungeons.dbc
    // Hacky!
    {
        std::set<std::pair<uint32, uint8>> LFGDungeonsForScenarios;
        for (LFGDungeonEntry const* dungeon : sLFGDungeonStore)
            if (dungeon->SubType == LFG_TYPE_SCENARIO)
            {
                if (sMapDifficultyMap.find(dungeon->MapID) != sMapDifficultyMap.end())
                    continue;

                LFGDungeonsForScenarios.insert(std::make_pair(dungeon->MapID, dungeon->DifficultyID));
            }

        for (auto & dungeon : LFGDungeonsForScenarios)
        {
            uint32 MapID = dungeon.first;
            uint8 Index = dungeon.second == DIFFICULTY_HC_SCENARIO ? 0 : 1;

            // map 1024 (PandaFishingVillage Scenario) as benchmark (already existed in sMapDifficultyMap)
            // Index 0 - heroic difficulty
            // Index 1 - normal difficulty

            sMapDifficultyMap[MapID].push_back(sMapDifficultyMap[1024][Index]);
        }
    }

    for (NameGenEntry const* entry : sNameGenStore)
        sGenNameVectoArraysMap[entry->Race].stringVectorArray[entry->Gender].push_back(std::string(entry->Name));

    for (PhaseGroupEntry const* group : sPhaseGroupStore)
        if (PhaseEntry const* phase = sPhaseStore.LookupEntry(group->PhaseId))
            sPhasesByGroup[group->GroupId].insert(phase->ID);

    for (SkillRaceClassInfoEntry const* entry : sSkillRaceClassInfoStore)
        if (sSkillLineStore.LookupEntry(entry->SkillId))
            SkillRaceClassInfoBySkill.emplace(entry->SkillId, entry);

    for (SpellPowerEntry const* power : sSpellPowerStore)
    {
        if (uint32 difficultyId = power->DifficultyID)
            _spellPowerDifficulties[power->SpellID][difficultyId].push_back(power);
        else
            _spellPowers[power->SpellID].push_back(power);
    }

    for (SpellEffectScalingEntry const* spellEffectScaling : sSpellEffectScalingStore)
        sSpellEffectScallingByEffectId[spellEffectScaling->SpellEffectId] = spellEffectScaling;

    for (SpellProcsPerMinuteModEntry const* ppmMod : sSpellProcsPerMinuteModStore)
        _spellProcsPerMinuteMods[ppmMod->SpellProcsPerMinuteID].push_back(ppmMod);

    for (TalentEntry const* talentInfo : sTalentStore)
    {
        if (talentInfo->ClassID < MAX_CLASSES && talentInfo->TierID < MAX_TALENT_TIERS && talentInfo->ColumnIndex < MAX_TALENT_COLUMNS)
            sTalentByPos[talentInfo->ClassID][talentInfo->TierID][talentInfo->ColumnIndex].push_back(talentInfo);
        else
            TC_LOG_ERROR("server.loading", "Value of class (found: %u, max allowed %u) or (found: %u, max allowed %u) tier or column (found: %u, max allowed %u) is invalid.",
                talentInfo->ClassID, MAX_CLASSES, talentInfo->TierID, MAX_TALENT_TIERS, talentInfo->ColumnIndex, MAX_TALENT_COLUMNS);
    }

    memset(sChrSpecializationByIndexStore, 0, sizeof(sChrSpecializationByIndexStore));
    for (ChrSpecializationEntry const* chrSpec : sChrSpecializationStore)
        sChrSpecializationByIndexStore[chrSpec->ClassID][chrSpec->OrderIndex] = chrSpec;

    for (SpecializationSpellsEntry const* specSpells : sSpecializationSpellsStore)
        sSpecializationSpellsBySpecStore[specSpells->SpecID].push_back(specSpells);

    for (TaxiPathEntry const* entry : sTaxiPathStore)
        sTaxiPathSetBySource[entry->From][entry->To] = TaxiPathBySourceAndDestination(entry->ID, entry->Cost);

    uint32 pathCount = sTaxiPathStore.GetNumRows();

    std::vector<uint32> pathLength;
    pathLength.resize(pathCount);                           // 0 and some other indexes not used
    for (TaxiPathNodeEntry const* entry : sTaxiPathNodeStore)
        if (pathLength[entry->Path] < entry->Index + 1)
            pathLength[entry->Path] = entry->Index + 1;

    // Set path length
    sTaxiPathNodesByPath.resize(pathCount);                 // 0 and some other indexes not used
    for (uint32 i = 1; i < sTaxiPathNodesByPath.size(); ++i)
        sTaxiPathNodesByPath[i].resize(pathLength[i]);

    // fill data
    for (TaxiPathNodeEntry const* entry : sTaxiPathNodeStore)
        sTaxiPathNodesByPath[entry->Path][entry->Index] = entry;

    // Initialize global taxinodes mask
    // include existed nodes that have at least single not spell base (scripted) path
    {
        sTaxiNodesMask.fill(0);
        sOldContinentsNodesMask.fill(0);
        sHordeTaxiNodesMask.fill(0);
        sAllianceTaxiNodesMask.fill(0);

        for (uint32 i = 1; i < sTaxiNodesStore.GetNumRows(); ++i)
        {
            TaxiNodesEntry const* node = sTaxiNodesStore.LookupEntry(i);
            if (!node)
                continue;

            if (!(node->Flags & (TAXI_NODE_FLAG_ALLIANCE | TAXI_NODE_FLAG_HORDE)))
                continue;

            // valid taxi network node
            uint8  field = (uint8)((i - 1) / 8);
            uint32 submask = 1 << ((i - 1) % 8);

            sTaxiNodesMask[field] |= submask;
            if (node->Flags & TAXI_NODE_FLAG_HORDE)
                sHordeTaxiNodesMask[field] |= submask;
            if (node->Flags & TAXI_NODE_FLAG_ALLIANCE)
                sAllianceTaxiNodesMask[field] |= submask;

            uint32 nodeMap;
            DeterminaAlternateMapPosition(node->MapID, node->Pos.X, node->Pos.Y, node->Pos.Z, &nodeMap);
            if (nodeMap < 2)
                sOldContinentsNodesMask[field] |= submask;
        }
    }

    for (TransportAnimationEntry const* anim : sTransportAnimationStore)
        sTransportMgr->AddPathNodeToTransport(anim->TransportID, anim->TimeIndex, anim);

    for (TransportRotationEntry const* rot : sTransportRotationStore)
        sTransportMgr->AddPathRotationToTransport(rot->TransportID, rot->TimeIndex, rot);

    for (WMOAreaTableEntry const* entry : sWMOAreaTableStore)
        sWMOAreaInfoByTripple.insert(WMOAreaInfoByTripple::value_type(WMOAreaTableTripple(entry->WMOID, entry->NameSet, entry->WMOGroupID), entry));

    for (ResearchSiteEntry const* siteEntry : sResearchSiteStore)
        for (QuestPOIPointEntry const* pointEntry : sQuestPOIPointStore)
            if (siteEntry->QuestPOIBlobID == pointEntry->BlobID)
                sDigsitePOIPolygons[siteEntry->ID].push_back(std::make_pair(pointEntry->POIPosX, pointEntry->POIPosY));

    for (SkillLineAbilityEntry const* ability : sSkillLineAbilityStore)
        SkillLineAbilities[ability->SpellID] = ability;

    for (MapEntry const* mapEntry : sMapStore)
        if (mapEntry->ParentMapID != -1)
            sParentMaps[mapEntry->ParentMapID].push_back(mapEntry->ID);

    // error checks
    if (bad_dbc_files.size() >= DBCFileCount)
    {
        TC_LOG_ERROR("misc", "Incorrect DataDir value in worldserver.conf or ALL required *.dbc files (%d) not found by path: %sdbc", DBCFileCount, dataPath.c_str());
        exit(1);
    }
    else if (!bad_dbc_files.empty())
    {
        std::string str;
        for (DBCStoreProblemList::iterator i = bad_dbc_files.begin(); i != bad_dbc_files.end(); ++i)
            str += *i + "\n";

        TC_LOG_ERROR("misc", "Some required *.dbc files (%u from %d) not found or not compatible:\n%s", (uint32)bad_dbc_files.size(), DBCFileCount, str.c_str());
        exit(1);
    }

    // Check loaded DBC files proper version
    if (!sAreaStore.LookupEntry(6863)          ||     // last area added in 5.4.8 (18414)
        !sCharTitlesStore.LookupEntry(389)     ||     // last char title added in 5.4.8 (18414)
        !sGemPropertiesStore.LookupEntry(2467) ||     // last gem property added in 5.4.8 (18414)
        !sMapStore.LookupEntry(1173)           ||     // last map added in 5.4.8 (18414)
        !sSpellStore.LookupEntry(163227)       )      // last spell added in 5.4.8 (18414)
    {
        TC_LOG_ERROR("misc", "You have _outdated_ DBC files. Please extract correct dbc files from client 5.4.8 18414.");
        exit(1);
    }

    TC_LOG_INFO("server.loading", ">> Initialized %d DBC data stores in %u ms", DBCFileCount, GetMSTimeDiffToNow(oldMSTime));
}

const std::string* DBCManager::GetRandomCharacterName(uint8 race, uint8 gender)
{
    uint32 size = sGenNameVectoArraysMap[race].stringVectorArray[gender].size();
    uint32 randPos = Math::Rand(0, size-1);

    return &sGenNameVectoArraysMap[race].stringVectorArray[gender][randPos];
}

SimpleFactionsList const* DBCManager::GetFactionTeamList(uint32 faction)
{
    FactionTeamMap::const_iterator itr = sFactionTeamMap.find(faction);
    if (itr != sFactionTeamMap.end())
        return &itr->second;

    return NULL;
}

char const* DBCManager::GetPetName(uint32 petfamily, uint32 /*dbclang*/)
{
    if (!petfamily)
        return NULL;
    CreatureFamilyEntry const* pet_family = sCreatureFamilyStore.LookupEntry(petfamily);
    if (!pet_family)
        return NULL;
    return pet_family->Name ? pet_family->Name : NULL;
}

WMOAreaTableEntry const* DBCManager::GetWMOAreaTableEntryByTripple(int32 rootid, int32 adtid, int32 groupid)
{
    WMOAreaInfoByTripple::iterator i = sWMOAreaInfoByTripple.find(WMOAreaTableTripple(rootid, adtid, groupid));
        if (i == sWMOAreaInfoByTripple.end())
            return NULL;
        return i->second;
}

char const* DBCManager::GetRaceName(uint8 race, uint8 /*locale*/)
{
    ChrRacesEntry const* raceEntry = sChrRacesStore.LookupEntry(race);
    return raceEntry ? raceEntry->Name : NULL;
}

char const* DBCManager::GetClassName(uint8 class_, uint8 /*locale*/)
{
    ChrClassesEntry const* classEntry = sChrClassesStore.LookupEntry(class_);
    return classEntry ? classEntry->Name : NULL;
}

uint32 DBCManager::GetVirtualMapForMapAndZone(uint32 mapid, uint32 zoneId)
{
    if (mapid != 530 && mapid != 571 && mapid != 732)   // speed for most cases
        return mapid;

    if (WorldMapAreaEntry const* wma = sWorldMapAreaStore.LookupEntry(zoneId))
        return wma->DisplayMapID >= 0 ? wma->DisplayMapID : wma->MapID;

    return mapid;
}

uint32 DBCManager::GetMaxLevelForExpansion(uint32 expansion)
{
    switch (expansion)
    {
        case CONTENT_1_60:
            return 60;
        case CONTENT_61_70:
            return 70;
        case CONTENT_71_80:
            return 80;
        case CONTENT_81_85:
            return 85;
        case CONTENT_86_90:
            return 90;
        default:
            break;
    }
    return 0;
}

uint32 DBCManager::GetExpansionForLevel(uint8 level)
{
    if (level <= 60)
        return CONTENT_1_60;
    else if (level <= 70)
        return CONTENT_61_70;
    else if (level <= 80)
        return CONTENT_71_80;
    else if (level <= 85)
        return CONTENT_81_85;
    else
        return CONTENT_86_90;
}

/*
Used only for calculate xp gain by content lvl.
Calculation on Gilneas and group maps of LostIslands calculated as CONTENT_1_60.
*/
ContentLevels DBCManager::GetContentLevelsForMapAndZone(uint32 mapid, uint32 zoneId)
{
    mapid = sDBCManager->GetVirtualMapForMapAndZone(mapid, zoneId);
    if (mapid < 2)
        return CONTENT_1_60;

    MapEntry const* mapEntry = sMapStore.LookupEntry(mapid);
    if (!mapEntry)
        return CONTENT_1_60;

    // no need enum all maps from phasing
    if (mapEntry->ParentMapID >= 0)
        mapid = mapEntry->ParentMapID;

    switch (mapid)
    {
        case 648:   //LostIslands
        case 654:   //Gilneas
            return CONTENT_1_60;
        default:
            return ContentLevels(mapEntry->Expansion());
    }
}

bool DBCManager::IsTotemCategoryCompatibleWith(uint32 itemTotemCategoryId, uint32 requiredTotemCategoryId)
{
    if (requiredTotemCategoryId == 0)
        return true;
    if (itemTotemCategoryId == 0)
        return false;

    TotemCategoryEntry const* itemEntry = sTotemCategoryStore.LookupEntry(itemTotemCategoryId);
    if (!itemEntry)
        return false;
    TotemCategoryEntry const* reqEntry = sTotemCategoryStore.LookupEntry(requiredTotemCategoryId);
    if (!reqEntry)
        return false;

    if (itemEntry->categoryType != reqEntry->categoryType)
        return false;

    return (itemEntry->categoryMask & reqEntry->categoryMask) == reqEntry->categoryMask;
}

void DBCManager::Zone2MapCoordinates(float& x, float& y, uint32 zone)
{
    WorldMapAreaEntry const* maEntry = sWorldMapAreaStore.LookupEntry(zone);

    // if not listed then map coordinates (instance)
    if (!maEntry)
        return;

    std::swap(x, y);                                         // at client map coords swapped
    x = x*((maEntry->LocBottom - maEntry->LocTop) / 100) + maEntry->LocTop;
    y = y*((maEntry->LocRight - maEntry->LocLeft) / 100) + maEntry->LocLeft;      // client y coord from top to down
}

void DBCManager::Map2ZoneCoordinates(float& x, float& y, uint32 zone)
{
    WorldMapAreaEntry const* maEntry = sWorldMapAreaStore.LookupEntry(zone);

    // if not listed then map coordinates (instance)
    if (!maEntry)
        return;

    x = (x - maEntry->LocTop) / ((maEntry->LocBottom - maEntry->LocTop) / 100);
    y = (y - maEntry->LocLeft) / ((maEntry->LocRight - maEntry->LocLeft) / 100);    // client y coord from top to down
    std::swap(x, y);                                         // client have map coords swapped
}

MapDifficultyEntry const* DBCManager::GetDefaultMapDifficulty(uint32 mapId, Difficulty* difficulty /*= nullptr*/)
{
    auto itr = sMapDifficultyMap.find(mapId);
    if (itr == sMapDifficultyMap.end())
        return nullptr;

    if (itr->second.empty())
        return nullptr;

    for (auto& p : itr->second)
    {
        DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(p->DifficultyID);
        if (!difficultyEntry)
            continue;

        if (difficultyEntry->Flags & DIFFICULTY_FLAG_DEFAULT)
        {
            if (difficulty)
                *difficulty = Difficulty(p->DifficultyID);

            return p;
        }
    }

    if (difficulty)
        *difficulty = Difficulty((*itr->second.begin())->DifficultyID);

    return *(itr->second.begin());
}

MapDifficultyEntry const* DBCManager::GetMapDifficultyData(uint32 mapId, Difficulty difficulty)
{
    auto itr = sMapDifficultyMap.find(mapId);
    if (itr != sMapDifficultyMap.end())
        for (MapDifficultyEntry const* mapDiff : itr->second)
            if (mapDiff->DifficultyID == difficulty)
                return mapDiff;

    return nullptr;
}

MapDifficultyEntry const* DBCManager::GetDownscaledMapDifficultyData(uint32 mapId, Difficulty &difficulty)
{
    DifficultyEntry const* diffEntry = sDifficultyStore.LookupEntry(difficulty);
    if (!diffEntry)
        return GetDefaultMapDifficulty(mapId, &difficulty);

    uint32 tmpDiff = difficulty;
    MapDifficultyEntry const* mapDiff = sDBCManager->GetMapDifficultyData(mapId, Difficulty(tmpDiff));
    while (!mapDiff)
    {
        tmpDiff = diffEntry->FallbackDifficultyID;
        diffEntry = sDifficultyStore.LookupEntry(tmpDiff);
        if (!diffEntry)
            return GetDefaultMapDifficulty(mapId, &difficulty);

        // pull new data
        mapDiff = sDBCManager->GetMapDifficultyData(mapId, Difficulty(tmpDiff)); // we are 10 normal or 25 normal
    }

    difficulty = Difficulty(tmpDiff);
    return mapDiff;
}

PvPDifficultyEntry const* DBCManager::GetBattlegroundBracketByLevel(uint32 mapid, uint32 level)
{
    PvPDifficultyEntry const* maxEntry = NULL;              // used for level > max listed level case
    for (PvPDifficultyEntry const* entry : sPvPDifficultyStore)
    {
        // skip unrelated and too-high brackets
        if (entry->MapID != mapid || entry->MinLevel > level)
            continue;

        // exactly fit
        if (entry->MaxLevel >= level)
            return entry;

        // remember for possible out-of-range case (search higher from existed)
        if (!maxEntry || maxEntry->MaxLevel < entry->MaxLevel)
            maxEntry = entry;
    }

    return maxEntry;
}

PvPDifficultyEntry const* DBCManager::GetBattlegroundBracketById(uint32 mapid, BattlegroundBracketId id)
{
    for (PvPDifficultyEntry const* entry : sPvPDifficultyStore)
        if (entry->MapID == mapid && entry->GetBracketId() == id)
            return entry;

    return NULL;
}

std::vector<SpecializationSpellsEntry const*> const* DBCManager::GetSpecializationSpells(uint32 specId)
{
    auto itr = sSpecializationSpellsBySpecStore.find(specId);
    if (itr != sSpecializationSpellsBySpecStore.end())
        return &itr->second;

    return nullptr;
}

CharSectionsEntry const* DBCManager::GetCharSectionEntry(uint8 race, CharSectionType genType, uint8 gender, uint8 type, uint8 color)
{
    std::pair<CharSectionsMap::const_iterator, CharSectionsMap::const_iterator> eqr = sCharSectionMap.equal_range(uint32(genType) | uint32(gender << 8) | uint32(race << 16));
    for (CharSectionsMap::const_iterator itr = eqr.first; itr != eqr.second; ++itr)
        if (itr->second->Type == type && itr->second->Color == color)
            return itr->second;

    return NULL;
}

CharStartOutfitEntry const* DBCManager::GetCharStartOutfitEntry(uint8 race, uint8 class_, uint8 gender)
{
    std::map<uint32, CharStartOutfitEntry const*>::const_iterator itr = sCharStartOutfitMap.find(race | (class_ << 8) | (gender << 16));
    if (itr == sCharStartOutfitMap.end())
        return NULL;

    return itr->second;
}

uint32 DBCManager::GetPowerIndexByClass(uint32 powerType, uint32 classId)
{
    return PowersByClass[classId][powerType];
}

/// Returns LFGDungeonEntry for a specific map and difficulty. Will return first found entry if multiple dungeons use the same map (such as Scarlet Monastery)
LFGDungeonEntry const* DBCManager::GetLFGDungeon(uint32 mapId, Difficulty difficulty)
{
    for (LFGDungeonEntry const* dungeon : sLFGDungeonStore)
        if (dungeon->MapID == int32(mapId) && Difficulty(dungeon->DifficultyID) == difficulty)
            return dungeon;

    return NULL;
}

uint32 DBCManager::GetDefaultMapLight(uint32 mapId)
{
    for (LightEntry const* light : sLightStore)
        if (light->MapID == mapId && light->Pos.X == 0.0f && light->Pos.Y == 0.0f && light->Pos.Z == 0.0f)
            return light->ID;

    return 0;
}

std::set<uint32> const& DBCManager::GetPhasesForGroup(uint32 group)
{
    return sPhasesByGroup[group];
}

SkillRaceClassInfoEntry const* DBCManager::GetSkillRaceClassInfo(uint32 skill, uint8 race, uint8 class_)
{
    SkillRaceClassInfoBounds bounds = SkillRaceClassInfoBySkill.equal_range(skill);
    for (SkillRaceClassInfoMap::iterator itr = bounds.first; itr != bounds.second; ++itr)
    {
        if (itr->second->RaceMask && !(itr->second->RaceMask & (1 << (race - 1))))
            continue;
        if (itr->second->ClassMask && !(itr->second->ClassMask & (1 << (class_ - 1))))
            continue;

        return itr->second;
    }

    return NULL;
}

uint32 DBCManager::GetQuestUniqueBitFlag(uint32 questId)
{
    QuestV2Entry const* v2 = sQuestV2Store.LookupEntry(questId);
    if (!v2)
        return 0;

    return v2->UniqueBitFlag;
}

std::vector<ItemSpecOverrideEntry const*> const* DBCManager::GetItemSpecOverrides(uint32 itemId)
{
    auto itr = sItemSpecOverridesStore.find(itemId);
    if (itr != sItemSpecOverridesStore.end())
        return &itr->second;

    return nullptr;
}

std::vector<SpellPowerEntry const*> DBCManager::GetSpellPowers(uint32 spellId, Difficulty difficulty /*= DIFFICULTY_NONE*/, bool* hasDifficultyPowers /*= nullptr*/)
{
    std::vector<SpellPowerEntry const*> powers;

    auto difficultyItr = _spellPowerDifficulties.find(spellId);
    if (difficultyItr != _spellPowerDifficulties.end())
    {
        if (hasDifficultyPowers)
            *hasDifficultyPowers = true;

        DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(difficulty);
        while (difficultyEntry)
        {
            auto powerDifficultyItr = difficultyItr->second.find(difficultyEntry->ID);
            if (powerDifficultyItr != difficultyItr->second.end())
                for (SpellPowerEntry const* difficultyPower : powerDifficultyItr->second)
                    powers.push_back(difficultyPower);

            difficultyEntry = sDifficultyStore.LookupEntry(difficultyEntry->FallbackDifficultyID);
        }
    }

    auto itr = _spellPowers.find(spellId);
    if (itr != _spellPowers.end())
        for (SpellPowerEntry const* power : itr->second)
            powers.push_back(power);

    return powers;
}

void DBCManager::DeterminaAlternateMapPosition(uint32 mapId, float x, float y, float z, uint32* newMapId /*= nullptr*/, DBCPosition2D* newPos /*= nullptr*/)
{
    ASSERT(newMapId || newPos);
    WorldMapTransformsEntry const* transformation = nullptr;
    for (WorldMapTransformsEntry const* transform : sWorldMapTransformsStore)
    {
        if (transform->MapID != mapId)
            continue;

        if (transform->RegionMin.X > x || transform->RegionMax.X < x)
            continue;
        if (transform->RegionMin.Y > y || transform->RegionMax.Y < y)
            continue;
        if (transform->RegionMin.Z > z || transform->RegionMax.Z < z)
            continue;

        transformation = transform;
        break;
    }

    if (!transformation)
    {
        if (newMapId)
            *newMapId = mapId;

        if (newPos)
        {
            newPos->X = x;
            newPos->Y = y;
        }
        return;
    }

    if (newMapId)
        *newMapId = transformation->NewMapID;

    if (!newPos)
        return;

    if (transformation->RegionScale > 0.0f && transformation->RegionScale < 1.0f)
    {
        x = (x - transformation->RegionMin.X) * transformation->RegionScale + transformation->RegionMin.X;
        y = (y - transformation->RegionMin.Y) * transformation->RegionScale + transformation->RegionMin.Y;
    }

    newPos->X = x + transformation->RegionOffset.X;
    newPos->Y = y + transformation->RegionOffset.Y;
}

uint32 ScalingStatValuesEntry::GetStatMultiplier(uint32 inventoryType) const
{
    if (inventoryType < MAX_INVTYPE)
    {
        switch (inventoryType)
        {
            case INVTYPE_NON_EQUIP:
            case INVTYPE_BODY:
            case INVTYPE_BAG:
            case INVTYPE_TABARD:
            case INVTYPE_AMMO:
            case INVTYPE_QUIVER:
                return 0;
            case INVTYPE_HEAD:
            case INVTYPE_CHEST:
            case INVTYPE_LEGS:
            case INVTYPE_2HWEAPON:
            case INVTYPE_ROBE:
                return StatMultiplier[0];
            case INVTYPE_SHOULDERS:
            case INVTYPE_WAIST:
            case INVTYPE_FEET:
            case INVTYPE_HANDS:
            case INVTYPE_TRINKET:
                return StatMultiplier[1];
            case INVTYPE_NECK:
            case INVTYPE_WRISTS:
            case INVTYPE_FINGER:
            case INVTYPE_SHIELD:
            case INVTYPE_CLOAK:
            case INVTYPE_HOLDABLE:
                return StatMultiplier[2];
            case INVTYPE_RANGED:
            case INVTYPE_THROWN:
            case INVTYPE_RANGEDRIGHT:
            case INVTYPE_RELIC:
                return StatMultiplier[3];
            case INVTYPE_WEAPON:
            case INVTYPE_WEAPONMAINHAND:
            case INVTYPE_WEAPONOFFHAND:
                return StatMultiplier[4];
            default:
                break;
        }
    }
    return 0;
}

uint32 ScalingStatValuesEntry::GetArmor(uint32 inventoryType, uint32 armorType) const
{
    if (inventoryType <= INVTYPE_ROBE && armorType < 4)
    {
        switch (inventoryType)
        {
            case INVTYPE_NON_EQUIP:
            case INVTYPE_NECK:
            case INVTYPE_BODY:
            case INVTYPE_FINGER:
            case INVTYPE_TRINKET:
            case INVTYPE_WEAPON:
            case INVTYPE_SHIELD:
            case INVTYPE_RANGED:
            case INVTYPE_2HWEAPON:
            case INVTYPE_BAG:
            case INVTYPE_TABARD:
                break;
            case INVTYPE_SHOULDERS:
                return Armor[0][armorType];
            case INVTYPE_CHEST:
            case INVTYPE_ROBE:
                return Armor[1][armorType];
            case INVTYPE_HEAD:
                return Armor[2][armorType];
            case INVTYPE_LEGS:
                return Armor[3][armorType];
            case INVTYPE_FEET:
                return Armor[4][armorType];
            case INVTYPE_WAIST:
                return Armor[5][armorType];
            case INVTYPE_HANDS:
                return Armor[6][armorType];
            case INVTYPE_WRISTS:
                return Armor[7][armorType];
            case INVTYPE_CLOAK:
                return CloakArmor;
            default:
                break;
        }
    }
    return 0;
}

uint32 ScalingStatValuesEntry::GetDPSAndDamageMultiplier(uint32 subClass, bool isCasterWeapon, float* damageMultiplier) const
{
    if (!isCasterWeapon)
    {
        switch (subClass)
        {
            case ITEM_SUBCLASS_WEAPON_AXE:
            case ITEM_SUBCLASS_WEAPON_MACE:
            case ITEM_SUBCLASS_WEAPON_SWORD:
            case ITEM_SUBCLASS_WEAPON_DAGGER:
            case ITEM_SUBCLASS_WEAPON_THROWN:
                *damageMultiplier = 0.3f;
                return DPSMod[0];
            case ITEM_SUBCLASS_WEAPON_AXE2:
            case ITEM_SUBCLASS_WEAPON_MACE2:
            case ITEM_SUBCLASS_WEAPON_POLEARM:
            case ITEM_SUBCLASS_WEAPON_SWORD2:
            case ITEM_SUBCLASS_WEAPON_STAFF:
            case ITEM_SUBCLASS_WEAPON_FISHING_POLE:
                *damageMultiplier = 0.2f;
                return DPSMod[1];
            case ITEM_SUBCLASS_WEAPON_BOW:
            case ITEM_SUBCLASS_WEAPON_GUN:
            case ITEM_SUBCLASS_WEAPON_CROSSBOW:
                *damageMultiplier = 0.3f;
                return DPSMod[4];
            case ITEM_SUBCLASS_WEAPON_Obsolete:
            case ITEM_SUBCLASS_WEAPON_EXOTIC:
            case ITEM_SUBCLASS_WEAPON_EXOTIC2:
            case ITEM_SUBCLASS_WEAPON_FIST_WEAPON:
            case ITEM_SUBCLASS_WEAPON_MISCELLANEOUS:
            case ITEM_SUBCLASS_WEAPON_SPEAR:
            case ITEM_SUBCLASS_WEAPON_WAND:
                break;
        }
    }
    else
    {
        if (subClass <= ITEM_SUBCLASS_WEAPON_WAND)
        {
            uint32 mask = 1 << subClass;
            // two-handed weapons
            if (mask & 0x562)
            {
                *damageMultiplier = 0.2f;
                return DPSMod[3];
            }

            if (mask & (1 << ITEM_SUBCLASS_WEAPON_WAND))
            {
                *damageMultiplier = 0.3f;
                return DPSMod[5];
            }
        }
        *damageMultiplier = 0.3f;
        return DPSMod[2];
    }
    return 0;
}

SpellEffectScalingEntry const* DBCManager::GetSpellEffectScallingEntry(uint32 effectId) const
{
    SpellEffectScallingByEffectId::const_iterator itr = sSpellEffectScallingByEffectId.find(effectId);
    if (itr != sSpellEffectScallingByEffectId.end())
        return itr->second;

    return nullptr;
}

TaxiPathSetForSource const* DBCManager::GetTaxiPathSetForSource(uint32 source) const
{
    TaxiPathSetBySource::const_iterator itr = sTaxiPathSetBySource.find(source);
    if (itr != sTaxiPathSetBySource.end())
        return &itr->second;
    return nullptr;
}

DigsitePOIPolygon const* DBCManager::GetDigsitePOIPolygon(uint32 digsiteId) const
{
    DigsitePOIPolygonContainer::const_iterator itr = sDigsitePOIPolygons.find(digsiteId);
    if (itr != sDigsitePOIPolygons.end())
        return &itr->second;

    return nullptr;
}

uint32 GetLiquidFlags(uint32 liquidType)
{
    if (LiquidTypeEntry const* liq = sLiquidTypeStore.LookupEntry(liquidType))
        return 1 << liq->Type;

    return 0;
}

EmotesTextSoundEntry const* DBCManager::FindTextSoundEmoteFor(uint32 emote, uint32 race, uint32 gender)
{
    auto itr = sEmotesTextSoundMap.find(EmotesTextSoundKey(emote, race, gender));
    return itr != sEmotesTextSoundMap.end() ? itr->second : nullptr;
}

std::vector<SpellProcsPerMinuteModEntry const*> DBCManager::GetSpellProcsPerMinuteMods(uint32 spellprocsPerMinuteId) const
{
    auto itr = _spellProcsPerMinuteMods.find(spellprocsPerMinuteId);
    if (itr != _spellProcsPerMinuteMods.end())
        return itr->second;

    return std::vector<SpellProcsPerMinuteModEntry const*>();
}

SkillLineAbilityEntry const* DBCManager::GetSkillLineAbilityBySpell(uint32 spellId)
{
    auto itr = SkillLineAbilities.find(spellId);
    if (itr != SkillLineAbilities.end())
        return itr->second;

    return nullptr;
}

ParentMapsVector DBCManager::GetParentMapsForMapID(uint32 mapID) const
{
    auto itr = sParentMaps.find(mapID);
    if (itr != sParentMaps.end())
        return itr->second;

    return ParentMapsVector();
}
