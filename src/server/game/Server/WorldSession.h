/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/// \addtogroup u2w
/// @{
/// \file

#ifndef __WORLDSESSION_H
#define __WORLDSESSION_H

#include "Common.h"
#include "SharedDefines.h"
#include "AddonMgr.h"
#include "DatabaseEnv.h"
#include "World.h"
#include "Opcodes.h"
#include "WorldPacket.h"
#include "Cryptography/BigNumber.h"
#include "Opcodes.h"
#include "AccountMgr.h"
#include "Object.h"
#include "QueryCallbackProcessor.h"

class Battlefield;
class BlackMarketEntry;
class Creature;
class GameObject;
class InstanceSave;
class Item;
class CharLoginQueryHolder;
class AccountLoginQueryHolder;
class PetLoginQueryHolder;
class AuthSessionQueryHolder;
class Object;
class Player;
class Quest;
class SpellCastTargets;
class Unit;
class Warden;
class WorldPacket;
class WorldSocket;
class PetBattle;
class BattlePayShop;
class RaFData;

struct AreaTableEntry;
struct AuctionEntry;
struct BlackMarketTemplate;
struct ItemTemplate;
struct Petition;
struct TradeStatusInfo;
struct Loot;
struct LfgJoinResultData;
struct LfgPlayerBoot;
struct LfgProposal;
struct LfgQueueStatusData;
struct LfgPlayerRewardData;
struct LfgRoleCheck;
struct LfgUpdateData;
struct PetBattleRequest;
struct BattlePayShopPurchasedProduct;

typedef std::array<std::string, MAX_DECLINED_NAME_CASES> DeclinedNames;

typedef std::array<uint32, BATTLE_PET_MAX_LOADOUT_SLOTS> PetBattleSlotErrorArray;

namespace rbac
{
class RBACData;
}

enum AccountDataType
{
    GLOBAL_CONFIG_CACHE             = 0,                    // 0x01 g
    PER_CHARACTER_CONFIG_CACHE      = 1,                    // 0x02 p
    GLOBAL_BINDINGS_CACHE           = 2,                    // 0x04 g
    PER_CHARACTER_BINDINGS_CACHE    = 3,                    // 0x08 p
    GLOBAL_MACROS_CACHE             = 4,                    // 0x10 g
    PER_CHARACTER_MACROS_CACHE      = 5,                    // 0x20 p
    PER_CHARACTER_LAYOUT_CACHE      = 6,                    // 0x40 p
    PER_CHARACTER_CHAT_CACHE        = 7                     // 0x80 p
};

#define NUM_ACCOUNT_DATA_TYPES        8

#define GLOBAL_CACHE_MASK           0x15
#define PER_CHARACTER_CACHE_MASK    0xEA

#define REGISTERED_ADDON_PREFIX_SOFTCAP 64

enum Tutorials
{
    TUTORIAL_TALENT           = 0,
    TUTORIAL_SPEC             = 1,
    TUTORIAL_GLYPH            = 2,
    TUTORIAL_SPELLBOOK        = 3,
    TUTORIAL_PROFESSIONS      = 4,
    TUTORIAL_CORE_ABILITITES  = 5,
    TUTORIAL_PET_JOURNAL      = 6,
    TUTORIAL_WHAT_HAS_CHANGED = 7
};

#define MAX_ACCOUNT_TUTORIAL_VALUES 8

struct AccountData
{
    AccountData() : Time(0), Data("") { }

    time_t Time;
    std::string Data;
};

enum PartyOperation
{
    PARTY_OP_INVITE     = 0,
    PARTY_OP_UNINVITE   = 1,
    PARTY_OP_LEAVE      = 2,
    PARTY_OP_SWAP       = 4
};

enum BFLeaveReason
{
    BF_LEAVE_REASON_CLOSE               = 1,
    //BF_LEAVE_REASON_UNK1              = 2, (not used)
    //BF_LEAVE_REASON_UNK2              = 4, (not used)
    BF_LEAVE_REASON_EXITED              = 8,
    BF_LEAVE_REASON_LOW_LEVEL           = 10,
    BF_LEAVE_REASON_NOT_WHILE_IN_RAID   = 15,
    BF_LEAVE_REASON_DESERTER            = 16
};

enum ChangeDynamicDifficultyResult
{
    DIFF_CHANGE_FAIL_RAID_RECENTLY_IN_COMBAT                    =  0, // "Raid was in combat recently and may not change difficulty again for %s.", where %s is time. Send the time in seconds as a uint32
    DIFF_CHANGE_FAIL_EVENT_IN_PROGRESS                          =  1, // "Raid difficulty cannot be changed at this time. An event is in progress."
    DIFF_CHANGE_FAIL_ENCOUNTER_IN_PROGRESS                      =  2, // "Raid difficulty cannot be changed at this time. An encounter is in progress."
    DIFF_CHANGE_FAIL_SOMEONE_IN_COMBAT                          =  3, // "Raid difficulty cannot be changed at this time. A player is in combat."
    DIFF_CHANGE_FAIL_SOMEONE_BUSY                               =  4, // "Raid difficulty cannot be changed at this time. A player is busy."
    DIFF_CHANGE_SET_CHANGE_TIME                                 =  5, // Sets the time(in secs, uint32 sent) until the raid can't change difficulty
    DIFF_CHANGE_FAIL_CHANGE_STARTED                             =  6, // "A raid difficulty change is currently in progress."
    DIFF_CHANGE_SHOW_AREA_TRIGGER_TEXT                          =  7, // Shows the area trigger text in the chat box. Send MapDifficultyID as a uint32
    DIFF_CHANGE_FAIL_SOMEONE_LOCKED                             =  8, // "Raid difficulty cannot be changed. %s is already locked to a different Heroic instance." where %s is the name of the player. Send a packed guid of the locked player
    DIFF_CHANGE_FAIL_ALREADY_HEROIC                             =  9, // "Your heroic instance is already in running and in use by another party"
    DIFF_CHANGE_FAIL_IN_LFR                                     = 10, // "Using Raid Finder to enter this instance disables dynamic difficulty selection"
    DIFF_CHANGE_SUCCESS                                         = 11, // This is sent where the change is successful. Send the mapID(uint32) and the DifficultyID(uint32)
};

enum ChatRestrictionType
{
    ERR_CHAT_RESTRICTED = 0,
    ERR_CHAT_THROTTLED  = 1,
    ERR_USER_SQUELCHED  = 2,
    ERR_YELL_RESTRICTED = 3
};

enum BarberShopResult
{
    BARBER_SHOP_SUCCESS          = 0,
    BARBER_SHOP_NOT_ENOUGH_MONEY = 1,
    BARBER_SHOP_NOT_SITTING      = 2
    /*BARBER_SHOP_NOT_ENOUGH_MONEY = 3*/
};

enum DeclinedNameResult
{
    DECLINED_NAMES_RESULT_SUCCESS = 0,
    DECLINED_NAMES_RESULT_ERROR   = 1
};

enum TutorialsFlag : uint8
{
    TUTORIALS_FLAG_NONE                           = 0x00,
    TUTORIALS_FLAG_CHANGED                        = 0x01,
    TUTORIALS_FLAG_LOADED_FROM_DB                 = 0x02
};

#define DB2_REPLY_BROADCAST             35137211
#define DB2_REPLY_SPARSE                2442913102
#define DB2_REPLY_ITEM                  1344507586

enum AuthSessionQueryIndex
{
    AUTH_SESSION_QUERY_LOAD_BATTLE_PAY_SHOP,
    AUTH_SESSION_QUERY_LOAD_BATTLE_PAY_SHOP_PRODUCTS,

    AUTH_SESSION_QUERY_LOAD_RAF_DATA,
    AUTH_SESSION_QUERY_LOAD_RAF_RECRUITS_COUNT,

    MAX_AUTH_SESSION_QUERY
};

struct WhoListData
{
    WhoListData(Player* _player, std::string _guildName, std::string _playerName, uint8 _class, uint8 race, int8 _gender, uint8 _level, int32 _zoneID) :
                player(_player), GuildName(_guildName), PlayerName(_playerName), Class(_class), Race(race), Gender(_gender), Level(_level), ZoneID(_zoneID) { }

    Player* player;
    std::string GuildName;
    std::string PlayerName;
    uint8 Class;
    uint8 Race;
    int8 Gender;
    uint8 Level;
    int32 ZoneID;
};

//class to deal with packet processing
//allows to determine if next packet is safe to be processed
class PacketFilter
{
public:
    explicit PacketFilter(WorldSession* pSession) : m_pSession(pSession) { }
    virtual ~PacketFilter() { }

    virtual bool Process(WorldPacket* /*packet*/) { return true; }
    virtual bool ProcessLogout() const { return true; }
    static uint16 DropHighBytes(uint16 opcode) { return opcode & NUM_OPCODE_HANDLERS; }

protected:
    WorldSession* const m_pSession;

private:
    PacketFilter(PacketFilter const& right) = delete;
    PacketFilter& operator=(PacketFilter const& right) = delete;
};
//process only thread-safe packets in Map::Update()
class MapSessionFilter : public PacketFilter
{
public:
    explicit MapSessionFilter(WorldSession* pSession) : PacketFilter(pSession) { }
    ~MapSessionFilter() { }

    virtual bool Process(WorldPacket* packet) override;
    //in Map::Update() we do not process player logout!
    virtual bool ProcessLogout() const override { return false; }
};

//class used to filer only thread-unsafe packets from queue
//in order to update only be used in World::UpdateSessions()
class WorldSessionFilter : public PacketFilter
{
public:
    explicit WorldSessionFilter(WorldSession* pSession) : PacketFilter(pSession) { }
    ~WorldSessionFilter() { }

    virtual bool Process(WorldPacket* packet) override;
};

// Proxy structure to contain data passed to callback function,
// only to prevent bloating the parameter list
struct CharacterCreateInfo
{
    CharacterCreateInfo(std::string const& name, uint8 race, uint8 cclass, uint8 gender, uint8 skin, uint8 face, uint8 hairStyle, uint8 hairColor, uint8 facialHair, uint8 outfitId, uint32 templateId,
        WorldPacket& data) : Name(name), Race(race), Class(cclass), Gender(gender), Skin(skin), Face(face), HairStyle(hairStyle), HairColor(hairColor), FacialHair(facialHair),
        OutfitId(outfitId), Data(data), CharCount(0), TemplateId(templateId) { }

    /// User specified variables
    std::string Name;
    uint8 Race;
    uint8 Class;
    uint8 Gender;
    uint8 Skin;
    uint8 Face;
    uint8 HairStyle;
    uint8 HairColor;
    uint8 FacialHair;
    uint8 OutfitId;
    WorldPacket Data;

    uint32 TemplateId;

    /// Server side data
    uint8 CharCount;
};

struct CharCustomizeInfo
{
    CharCustomizeInfo(ObjectGuid guid, uint8 hairStyle, uint8 face, uint8 gender, uint8 hairColor, uint8 facialHairStyle, uint8 skin, std::string name) :
        CharGUID(guid), HairStyleID(hairStyle), FaceID(face), SexID(gender), HairColorID(hairColor), FacialHairStyleID(facialHairStyle),
        SkinID(skin), CharName(name) { }

    uint8 HairStyleID;
    uint8 FaceID;
    uint8 SexID;
    uint8 HairColorID;
    uint8 FacialHairStyleID;
    uint8 SkinID;

    ObjectGuid CharGUID;

    std::string CharName;
};

struct CharRaceOrFactionChangeInfo
{
    CharRaceOrFactionChangeInfo(ObjectGuid guid, uint8 raceId, uint8 hairStyle, uint8 face, uint8 gender, uint8 hairColor, uint8 facialHairStyle, uint8 skin, std::string name, bool factionChange) :
        Guid(guid), RaceID(raceId), HairStyleID(hairStyle), FaceID(face), SexID(gender), HairColorID(hairColor), FacialHairStyleID(facialHairStyle),
        SkinID(skin), Name(name), FactionChange(factionChange) { }

    uint8 HairColorID;
    uint8 RaceID;
    uint8 SexID;
    uint8 SkinID;
    uint8 FacialHairStyleID;
    uint8 FaceID;
    uint8 HairStyleID;

    ObjectGuid Guid;

    bool FactionChange;

    std::string Name;
};

struct PacketCounter
{
    time_t lastReceiveTime;
    uint32 amountCounter;
};

/// Player session in the World
class WorldSession
{
    public:
        WorldSession(uint32 id, WorldSocket* sock, AccountTypes sec, uint8 expansion, time_t mute_time, LocaleConstant locale);
        ~WorldSession();

        bool PlayerLoading() const { return m_playerLoading; }
        bool PlayerLogout() const { return m_playerLogout; }
        bool PlayerLogoutWithSave() const { return m_playerLogout && m_playerSave; }
        bool PlayerRecentlyLoggedOut() const { return m_playerRecentlyLogout; }
        bool PlayerDisconnected() const { return !m_Socket; }

        void ReadAddonsInfo(WorldPacket& recvData);
        void SendAddonsInfo();

        void SendTimezoneInformation();
        void SendDisplayPromotion();
        void SendGlueScreen();
        void SendDanceStudioCreate();

        bool IsAddonRegistered(const std::string& prefix) const;

        void SendPacket(WorldPacket const* packet, bool forced = false);
        void SendNotification(const char *format, ...) ATTR_PRINTF(2, 3);
        void SendNotification(uint32 string_id, ...);
        void SendPetNameInvalid(uint32 error, std::string const& name, DeclinedNames const& declinedNames, bool HasDeclinedNames, uint32 petNumber);
        void SendGroupResult(PartyOperation operation, std::string const& member, PartyResult res, uint32 val = 0);
        void SendSetPhaseShift(std::set<uint32> const& phaseIds, std::set<uint32> const& terrainswaps, std::set<uint32> const& worldMapAreaSwaps);
        void SendQueryTimeResponse();
        void SendGroupInviteNotification(const std::string& inviterName, uint32 ProposedRoles, bool inGroup);

        void SendAuthResponse(uint8 code, uint32 queuePos);
        void SendAuthWaitQueue(uint32 queuePos);
        void SendClientCacheVersion(uint32 version);

        void InitializeSession();
        void InitializeSessionCallback(AuthSessionQueryHolder* holder);

        rbac::RBACData* GetRBACData();
        bool HasPermission(uint32 permissionId);
        void LoadPermissions();
        void InvalidateRBACData(); // Used to force LoadPermissions at next HasPermission check

        AccountTypes GetSecurity() const { return _security; }
        uint32 GetAccountId() const { return _accountId; }
        Player* GetPlayer() const { return _player; }
        std::string const& GetPlayerName() const;
        std::string GetPlayerInfo() const;

        uint32 GetGUIDLow() const;
        void SetSecurity(AccountTypes security) { _security = security; }
        std::string const& GetRemoteAddress() { return m_Address; }
        void SetPlayer(Player* player);
        uint8 Expansion() const { return m_expansion; }

        void InitWarden(BigNumber* k, std::string const& os);

        /// Session in auth.queue currently
        void SetInQueue(bool state) { m_inQueue = state; }

        /// Is the user engaged in a log out process?
        bool isLogingOut() const { return _logoutTime || m_playerLogout; }

        /// Engage the logout process for the user
        void LogoutRequest(time_t requestTime)
        {
            _logoutTime = requestTime;
        }

        /// Is logout cooldown expired?
        bool ShouldLogOut(time_t currTime) const
        {
            return (_logoutTime > 0 && currTime >= _logoutTime + 20);
        }

        void LogoutPlayer(bool save);
        void KickPlayer();

        void QueuePacket(WorldPacket* new_packet);
        bool Update(uint32 diff, PacketFilter& updater);

        void SendNameQueryOpcode(ObjectGuid guid);

        void SendRealmNameQueryOpcode(uint32 realmId);

        void SendTrainerList(Creature* npc, uint32 trainerId);
        void SendListInventory(ObjectGuid guid);
        void SendShowBank(ObjectGuid guid);
        void SendShowMailBox(ObjectGuid guid);
        void SendTabardVendorActivate(ObjectGuid guid);
        void SendSpiritResurrect();
        void SendBindPoint(Creature* npc);

        void SendTradeStatus(TradeStatusInfo& status);
        void SendUpdateTrade(bool trader_data = true);
        void SendCancelTrade();

        void SendPetitionQueryOpcode(ObjectGuid petitionguid);

        // Pet
        void SendPetNameQuery(ObjectGuid guid, ObjectGuid petNumber);
        void SendOpenStable(ObjectGuid guid);
        void SendStableResult(uint8 res);
        bool CheckStableMaster(ObjectGuid guid);
        void SendStablePetCallback(ObjectGuid guid, PreparedQueryResult result);

        // Account Data
        AccountData* GetAccountData(AccountDataType type) { return &m_accountData[type]; }
        void SetAccountData(AccountDataType type, time_t tm, std::string const& data);
        void SendAccountDataTimes(uint32 mask);
        void LoadGlobalAccountData();
        void LoadAccountData(PreparedQueryResult result, uint32 mask);

        void LoadTutorialsData();
        void SendTutorialsData();
        void SaveTutorialsData(SQLTransaction& trans);
        uint32 GetTutorialInt(uint8 index) const { return m_Tutorials[index]; }
        void SetTutorialInt(uint8 index, uint32 value)
        {
            if (m_Tutorials[index] != value)
            {
                m_Tutorials[index] = value;
                m_TutorialsChanged |= TUTORIALS_FLAG_CHANGED;
            }
        }

        //auction
        void SendAuctionHello(ObjectGuid guid, Creature* unit);
        void SendAuctionCommandResult(AuctionEntry const* auction, uint32 Action, uint32 ErrorCode, uint32 inventoryError = 0);
        void SendAuctionWonNotification(AuctionEntry const* auction, ObjectGuid bidder);
        void SendAuctionOutBidNotification(AuctionEntry const* auction, ObjectGuid bidder, uint64 bidSum);
        void SendAuctionOwnerNotification(AuctionEntry const* auction, float mailDelay, bool sold, Item* item);
        void SendAuctionOwnerBidNotification(AuctionEntry const* auction, ObjectGuid bidder, uint64 bidSum);

        //Item Enchantment
        void SendEnchantmentLog(ObjectGuid targetGUID, ObjectGuid casterGUID, ObjectGuid itemGUID, uint32 itemId, uint32 enchantId, uint32 slot);
        void SendItemEnchantTimeUpdate(ObjectGuid Playerguid, ObjectGuid Itemguid, uint32 slot, uint32 Duration);

        //Taxi
        void SendTaxiStatus(ObjectGuid guid);
        void SendTaxiMenu(Creature* unit);
        void SendDoFlight(uint32 mountDisplayId, uint32 path, uint32 pathNode = 0);
        bool SendLearnNewTaxiNode(Creature* unit);
        void SendDiscoverNewTaxiNode(uint32 nodeid);

        // Guild
        void SendPetitionShowList(ObjectGuid guid);

        void BuildPartyMemberStatsChangedPacket(Player* player, WorldPacket* data);

        void DoLootRelease(Loot* loot);

        // Account mute time
        time_t m_muteTime;

        // Locales
        LocaleConstant GetSessionDbcLocale() const { return m_sessionDbcLocale; }
        LocaleConstant GetSessionDbLocaleIndex() const { return m_sessionDbLocaleIndex; }
        char const* GetTrinityString(uint32 entry) const;

        uint32 GetLatency() const { return m_latency; }
        void SetLatency(uint32 latency) { m_latency = latency; }
        void ResetClientTimeDelay() { m_clientTimeDelay = 0; }

        ACE_Atomic_Op<ACE_Thread_Mutex, time_t> m_timeOutTime;
        void UpdateTimeOutTime(uint32 diff)
        {
            if (time_t(diff) > m_timeOutTime.value())
                m_timeOutTime = 0;
            else
                m_timeOutTime -= diff;
        }
        void ResetTimeOutTime(bool onlyActive)
        {
            if (GetPlayer())
                m_timeOutTime = int32(sWorld->getIntConfig(CONFIG_SOCKET_TIMEOUTTIME_ACTIVE));
            else if (!onlyActive)
                m_timeOutTime = int32(sWorld->getIntConfig(CONFIG_SOCKET_TIMEOUTTIME));
        }

        bool IsConnectionIdle() const { return (m_timeOutTime <= 0 && !m_inQueue); }

        void UpdateGuildRosterTimer(uint32 diff);
        bool HasGuildRosterTimer() const { return m_guildRosterTimer > 0; }

        bool HandleMovementInfo(MovementInfo& movementInfo, Unit* mover, Opcodes receivedOpcode);
        bool HandleMovementAck(MovementInfo& movementInfo, Unit* mover, uint32 AckIndex, Opcodes dataOpcode = SMSG_PLAYER_MOVE, ExtraMovementStatusElement* extras = NULL);
        bool CanMovementBeProcessed(uint16 opcode);

        void InitBattlePayShop(uint32 accountID, std::string const& accountName);
        BattlePayShop const* GetBattlePayShop() const { return _battlePayShop; }
        BattlePayShop* GetBattlePayShop() { return _battlePayShop; }

        void SendCharLoginError(uint32 error);

        void InitReferAFriendSystem(uint32 accountID);
        RaFData const* GetRaFData() const { return _rafData; }
        RaFData* GetRaFData() { return _rafData; }

    public:                                                 // opcodes handlers

        void Handle_NULL(WorldPacket& recvData);          // not used
        void Handle_EarlyProccess(WorldPacket& recvData); // just mark packets processed in WorldSocket::OnRead
        void Handle_Deprecated(WorldPacket& recvData);    // never used anymore by client

        void HandleCharEnumOpcode(WorldPacket& recvData);
        void HandleCharDeleteOpcode(WorldPacket& recvData);
        void HandleCharCreateOpcode(WorldPacket& recvData);
        void HandlePlayerLoginOpcode(WorldPacket& recvData);
        void HandleLoadScreenOpcode(WorldPacket& recvData);
        void HandleCharEnum(PreparedQueryResult result);
        void HandlePlayerLogin(CharLoginQueryHolder* charHolder, AccountLoginQueryHolder* accountHolder);
        void HandleCharFactionOrRaceChange(WorldPacket& recvData);
        void HandleCharRaceOrFactionChangeCallback(std::shared_ptr<CharRaceOrFactionChangeInfo> raceOrFactionChangeInfo, PreparedQueryResult result);
        void HandleRandomizeCharNameOpcode(WorldPacket& recvData);
        void HandleReorderCharacters(WorldPacket& recvData);
        void HandleOpeningCinematic(WorldPacket& recvData);

        void HandleSendTimezoneInformation(WorldPacket& recvData);

        // played time
        void HandlePlayedTime(WorldPacket& recvData);

        // cemetery/graveyard related
        void HandleReturnToGraveyard(WorldPacket& recvData);
        void HandleRequestCemeteryList(WorldPacket& recvData);
        void HandleSetPreferedCemetery(WorldPacket& recvData);

        // Currency
        void HandleSetCurrencyFlags(WorldPacket& recvData);

        // new inspect
        void HandleInspectOpcode(WorldPacket& recvData);
        void HandleInspectHonorStatsOpcode(WorldPacket& recvData);
        void HandleInspectRatedBGStatsOpcode(WorldPacket& recvData);

        void HandleMountSpecialAnimOpcode(WorldPacket& recvdata);

        // character view
        void HandleShowingHelmOpcode(WorldPacket& recvData);
        void HandleShowingCloakOpcode(WorldPacket& recvData);

        // repair
        void HandleRepairItemOpcode(WorldPacket& recvData);

        void HandleMoveKnockBackAck(WorldPacket& recvData);
        void HandleSuspendTokenResponse(WorldPacket& recvData);
        void HandleMoveTeleportAck(WorldPacket& recvData);
        void HandleForceSpeedChangeAck(WorldPacket& recvData);
        void HandleSetCollisionHeightAck(WorldPacket& recvData);
        void HandleMoveRootAckOpcode(WorldPacket& recvData);
        void HandleFallAckOpcode(WorldPacket& recvData);
        void HandleGravityAckOpcode(WorldPacket& recvData);
        void HandleHoverAckOpcode(WorldPacket& recvData);
        void HandleTransitionAckOpcode(WorldPacket& recvData);
        void HandleTurnWhileFallingAckOpcode(WorldPacket& recvData);
        void HandleWaterWalkAckOpcode(WorldPacket& recvData);
        void HandleApplyMovementForceAckOpcode(WorldPacket& recvData);
        void HandleRemoveMovementForceAckOpcode(WorldPacket& recvData);
        void HandleRepopRequestOpcode(WorldPacket& recvData);
        void HandleAutostoreLootItemOpcode(WorldPacket& recvData);
        void HandleLootMoneyOpcode(WorldPacket& recvData);
        void HandleLootOpcode(WorldPacket& recvData);
        void HandleLootReleaseOpcode(WorldPacket& recvData);
        void HandleLootMasterGiveOpcode(WorldPacket& recvData);
        void HandleLootMasterAskForRoll(WorldPacket& recvData);
        void HandleWhoOpcode(WorldPacket& recvData);
        void HandleLogoutRequestOpcode(WorldPacket& recvData);
        void HandleLogoutCancelOpcode(WorldPacket& recvData);

        // GM Ticket opcodes
        void HandleGMTicketCreateOpcode(WorldPacket& recvData);
        void HandleGMTicketUpdateOpcode(WorldPacket& recvData);
        void HandleGMTicketDeleteOpcode(WorldPacket& recvData);
        void HandleGMTicketGetTicketOpcode(WorldPacket& recvData);
        void HandleGMTicketGetWebTicketsOpcode(WorldPacket& recvData);
        void HandleGMTicketSystemStatusOpcode(WorldPacket& recvData);
        void HandleGMSurveySubmit(WorldPacket& recvData);
        void HandleGMResponseResolve(WorldPacket& recvData);
        void HandleSupportTicketSubmitBug(WorldPacket& recvData);
        void HandleSupportTicketSubmitSuggestion(WorldPacket& recvData);
        void HandleSupportTicketSubmitComplaint(WorldPacket& recvData);

        void HandleTogglePvP(WorldPacket& recvData);
        void HandleSetPvP(WorldPacket& recvData);

        void HandleSetSelectionOpcode(WorldPacket& recvData);
        void HandleStandStateChangeOpcode(WorldPacket& recvData);
        void HandleEmoteOpcode(WorldPacket& recvData);
        void HandleContactListOpcode(WorldPacket& recvData);
        void HandleAddFriendOpcode(WorldPacket& recvData);
        void HandleAddFriendOpcodeCallBack(std::string const& friendNote, PreparedQueryResult result);
        void HandleDelFriendOpcode(WorldPacket& recvData);
        void HandleAddIgnoreOpcode(WorldPacket& recvData);
        void HandleAddIgnoreOpcodeCallBack(PreparedQueryResult result);
        void HandleDelIgnoreOpcode(WorldPacket& recvData);
        void HandleSetContactNotesOpcode(WorldPacket& recvData);

        void HandleAreaTriggerOpcode(WorldPacket& recvData);

        void HandleFactionSetAtWar(WorldPacket& recvData);
        void HandleFactionUnSetAtWar(WorldPacket& recvData);
        void HandleSetWatchedFactionOpcode(WorldPacket& recvData);
        void HandleSetFactionInactiveOpcode(WorldPacket& recvData);
        void HandleRequestForcedReactionsOpcode(WorldPacket& recvData);

        void HandleUpdateAccountData(WorldPacket& recvData);
        void HandleRequestAccountData(WorldPacket& recvData);
        void HandleSetActionButtonOpcode(WorldPacket& recvData);
        void HandleSetAdvancedCombatLogging(WorldPacket& recvData);

        void HandleGameObjectUseOpcode(WorldPacket& recData);
        void HandleGameobjectReportUse(WorldPacket& recvData);

        void HandleNameQueryOpcode(WorldPacket& recvData);
        void HandleRealmNameQueryOpcode(WorldPacket& recvData);

        void HandleQueryTimeOpcode(WorldPacket& recvData);

        void HandleCreatureQueryOpcode(WorldPacket& recvData);

        void HandleGameObjectQueryOpcode(WorldPacket& recvData);

        void HandleDBQueryBulk(WorldPacket& recvData);

        void HandleMoveWorldportAckOpcode(WorldPacket& recvData);
        void HandleMoveWorldportAckOpcode();                // for server-side calls

        void HandleMovementOpcodes(WorldPacket& recvData);
        void HandleKeyboundOverride(WorldPacket& recvData);

        void HandleSetActiveMoverOpcode(WorldPacket& recvData);
        void HandleDismissControlledVehicle(WorldPacket& recvData);
        void HandleRequestVehicleExit(WorldPacket& recvData);
        void HandleChangeSeatsOnControlledVehicle(WorldPacket& recvData);
        void HandleMoveSetVehicleRecAck(WorldPacket& recvData);

        void HandleMoveTimeSkippedOpcode(WorldPacket& recvData);

        void HandleRequestRaidInfoOpcode(WorldPacket& recvData);

        void HandleBattlegroundStatusOpcode(WorldPacket& recvData);

        void HandleGroupRequestJoinUpdatesOpcode(WorldPacket & recvData);
        void HandleGroupInviteOpcode(WorldPacket& recvData);
        //void HandleGroupCancelOpcode(WorldPacket& recvData);
        void HandleGroupInviteResponseOpcode(WorldPacket& recvData);
        //void HandleGroupUninviteOpcode(WorldPacket& recvData);
        void HandleGroupUninviteGuidOpcode(WorldPacket& recvData);
        void HandleGroupSetLeaderOpcode(WorldPacket& recvData);
        void HandleGroupSetRolesOpcode(WorldPacket& recvData);
        void HandleGroupDisbandOpcode(WorldPacket& recvData);
        void HandleOptOutOfLootOpcode(WorldPacket& recvData);
        void HandleLootMethodOpcode(WorldPacket& recvData);
        void HandleLootSpecializationOpcode(WorldPacket& recvData);
        void HandleLootRoll(WorldPacket& recvData);
        void HandleRequestPartyMemberStatsOpcode(WorldPacket& recvData);
        void HandleRaidTargetUpdateOpcode(WorldPacket& recvData);
        void HandleRaidReadyCheckOpcode(WorldPacket& recvData);
        void HandleRaidReadyCheckConfirmOpcode(WorldPacket& recvData);
        void HandleGroupRaidConvertOpcode(WorldPacket& recvData);
        void HandleGroupChangeSubGroupOpcode(WorldPacket& recvData);
        void HandleGroupSwapSubGroupOpcode(WorldPacket& recvData);
        void HandleGroupAssistantLeaderOpcode(WorldPacket& recvData);
        void HandleSetEveryoneAssistantOpcode(WorldPacket& recvData);
        void HandleGroupAssignmentOpcode(WorldPacket& recvData);
        void HandleRaidMarkerClear(WorldPacket& recvData);
        void HandleGroupInitiatePollRole(WorldPacket& recvData);
        void SendRolePollInform(uint8 index);

        void HandlePetitionBuyOpcode(WorldPacket& recvData);
        void HandlePetitionShowSignOpcode(WorldPacket& recvData);
        void SendPetitionSigns(Petition const* petition, Player* sendTo);
        void HandlePetitionQueryOpcode(WorldPacket& recvData);
        void HandlePetitionRenameOpcode(WorldPacket& recvData);
        void HandlePetitionSignOpcode(WorldPacket& recvData);
        void HandlePetitionDeclineOpcode(WorldPacket& recvData);
        void HandleOfferPetitionOpcode(WorldPacket& recvData);
        void HandleTurnInPetitionOpcode(WorldPacket& recvData);
        void SendPetitionSignResults(ObjectGuid petitionGuid, ObjectGuid playerGuid, int8 result);

        void HandleGuildQueryOpcode(WorldPacket& recvData);
        void HandleGuildInviteOpcode(WorldPacket& recvData);
        void HandleGuildRemoveOpcode(WorldPacket& recvData);
        void HandleGuildDethroneGuildMasterOpcode(WorldPacket& recvData);
        void HandleGuildAcceptOpcode(WorldPacket& recvData);
        void HandleGuildDeclineOpcode(WorldPacket& recvData);
        void HandleGuildEventLogQueryOpcode(WorldPacket& recvData);
        void HandleGuildRosterOpcode(WorldPacket& recvData);
        void HandleGuildRewardsQueryOpcode(WorldPacket& recvData);
        void HandleGuildPromoteOpcode(WorldPacket& recvData);
        void HandleGuildDemoteOpcode(WorldPacket& recvData);
        void HandleGuildAssignRankOpcode(WorldPacket& recvData);
        void HandleGuildLeaveOpcode(WorldPacket& recvData);
        void HandleGuildDisbandOpcode(WorldPacket& recvData);
        void HandleGuildSetAchievementTracking(WorldPacket& recvData);
        void HandleGuildSetGuildMaster(WorldPacket& recvData);
        void HandleGuildMOTDOpcode(WorldPacket& recvData);
        void HandleGuildNewsUpdateStickyOpcode(WorldPacket& recvData);
        void HandleGuildSetNoteOpcode(WorldPacket& recvData);
        void HandleGuildQueryRanksOpcode(WorldPacket& recvData);
        void HandleGuildQueryNewsOpcode(WorldPacket& recvData);
        void HandleSwapRanks(WorldPacket& recvData);
        void HandleGuildSetRankPermissionsOpcode(WorldPacket& recvData);
        void HandleGuildAddRankOpcode(WorldPacket& recvData);
        void HandleGuildDelRankOpcode(WorldPacket& recvData);
        void HandleGuildChangeInfoTextOpcode(WorldPacket& recvData);
        void HandleSaveGuildEmblemOpcode(WorldPacket& recvData);
        void HandleGuildRequestPartyState(WorldPacket& recvData);
        void HandleGuildRequestChallengeUpdate(WorldPacket& recvData);
        void HandleAutoDeclineGuildInvites(WorldPacket& recvData);
        void HandleGuildAchievementMembersOpcode(WorldPacket& recvData);
        void HandleGuildQueryRecipes(WorldPacket& recvData);
        void HandleGuildQueryMemberRecipes(WorldPacket& recvData);
        void HandleGuildQueryMembersForRecipe(WorldPacket& recvData);

        void HandleGuildRenameRequest(WorldPacket& recvData);
        void HandleGuildRenameCallback(std::string const& newName, PreparedQueryResult result);

        void HandleGuildFinderAddRecruit(WorldPacket& recvData);
        void HandleGuildFinderBrowse(WorldPacket& recvData);
        void HandleGuildFinderDeclineRecruit(WorldPacket& recvData);
        void HandleGuildFinderGetApplications(WorldPacket& recvData);
        void HandleGuildFinderGetRecruits(WorldPacket& recvData);
        void HandleGuildFinderPostRequest(WorldPacket& recvData);
        void HandleGuildFinderRemoveRecruit(WorldPacket& recvData);
        void HandleGuildFinderSetGuildPost(WorldPacket& recvData);

        void HandleEnableTaxiNodeOpcode(WorldPacket& recvData);
        void HandleTaxiNodeStatusQueryOpcode(WorldPacket& recvData);
        void HandleTaxiQueryAvailableNodes(WorldPacket& recvData);
        void HandleActivateTaxiOpcode(WorldPacket& recvData);
        void HandleActivateTaxiExpressOpcode(WorldPacket& recvData);
        void HandleMoveSplineDoneOpcode(WorldPacket& recvData);
        void SendActivateTaxiReply(ActivateTaxiReply reply);

        void HandleTabardActivateOpcode(WorldPacket& recvData);
        void HandleBankerActivateOpcode(WorldPacket& recvData);
        void HandleBuyBankSlotOpcode(WorldPacket& recvData);
        void HandleTrainerListOpcode(WorldPacket& recvData);
        void HandleTrainerBuySpellOpcode(WorldPacket& recvData);
        void HandlePetitionShowListOpcode(WorldPacket& recvData);
        void HandleGossipHelloOpcode(WorldPacket& recvData);
        void HandleGossipSelectOptionOpcode(WorldPacket& recvData);
        void HandleSpiritHealerActivateOpcode(WorldPacket& recvData);
        void HandleNpcTextQueryOpcode(WorldPacket& recvData);
        void HandleBinderActivateOpcode(WorldPacket& recvData);

        void HandleRequestStabledPets(WorldPacket& recvData);
        void HandleSetPetSlot(WorldPacket& recvData);
        void HandleSetPetSlotCallback(uint8 newSlot, PreparedQueryResult result);

        void LoadPet(PreparedQueryResult result);
        void HandleLoadPet(PetLoginQueryHolder* petHolder);

        void HandleDuelProposedOpcode(WorldPacket& recvData);
        void HandleDuelResponseOpcode(WorldPacket& recvData);

        void HandleAcceptTradeOpcode(WorldPacket& recvData);
        void HandleBeginTradeOpcode(WorldPacket& recvData);
        void HandleBusyTradeOpcode(WorldPacket& recvData);
        void HandleCancelTradeOpcode(WorldPacket& recvData);
        void HandleClearTradeItemOpcode(WorldPacket& recvData);
        void HandleIgnoreTradeOpcode(WorldPacket& recvData);
        void HandleInitiateTradeOpcode(WorldPacket& recvData);
        void HandleSetTradeGoldOpcode(WorldPacket& recvData);
        void HandleSetTradeItemOpcode(WorldPacket& recvData);
        void HandleUnacceptTradeOpcode(WorldPacket& recvData);

        void HandleShowTradeSkillOpcode(WorldPacket& recvData);

        void HandleAuctionHelloOpcode(WorldPacket& recvData);
        void HandleAuctionListItems(WorldPacket& recvData);
        void HandleAuctionListBidderItems(WorldPacket& recvData);
        void HandleAuctionSellItem(WorldPacket& recvData);
        void HandleAuctionRemoveItem(WorldPacket& recvData);
        void HandleAuctionListOwnerItems(WorldPacket& recvData);
        void HandleAuctionPlaceBid(WorldPacket& recvData);
        void HandleAuctionListPendingSales(WorldPacket& recvData);
        void HandleReplicateItems(WorldPacket& recvData);

        void HandleGetMailList(WorldPacket& recvData);
        void HandleSendMail(WorldPacket& recvData);
        void HandleMailTakeMoney(WorldPacket& recvData);
        void HandleMailTakeItem(WorldPacket& recvData);
        void HandleMailMarkAsRead(WorldPacket& recvData);
        void HandleMailReturnToSender(WorldPacket& recvData);
        void HandleMailDelete(WorldPacket& recvData);
        void HandleItemTextQuery(WorldPacket& recvData);
        void HandleMailCreateTextItem(WorldPacket& recvData);
        void HandleQueryNextMailTime(WorldPacket& recvData);
        void HandleCancelChanneling(WorldPacket& recvData);

        void HandleSplitItemOpcode(WorldPacket& recvData);
        void HandleSwapInvItemOpcode(WorldPacket& recvData);
        void HandleDestroyItemOpcode(WorldPacket& recvData);
        void HandleAutoEquipItemOpcode(WorldPacket& recvData);
        void HandleSellItemOpcode(WorldPacket& recvData);
        void HandleBuyItemOpcode(WorldPacket& recvData);
        void HandleListInventoryOpcode(WorldPacket& recvData);
        void HandleAutoStoreBagItemOpcode(WorldPacket& recvData);
        void HandleReadItem(WorldPacket& recvData);
        void HandleAutoEquipItemSlotOpcode(WorldPacket& recvData);
        void HandleSwapItem(WorldPacket& recvData);
        void HandleBuybackItem(WorldPacket& recvData);
        void HandleAutoBankItemOpcode(WorldPacket& recvData);
        void HandleAutoStoreBankItemOpcode(WorldPacket& recvData);
        void HandleWrapItemOpcode(WorldPacket& recvData);

        void HandleAttackSwingOpcode(WorldPacket& recvData);
        void HandleAttackStopOpcode(WorldPacket& recvData);
        void HandleSetSheathedOpcode(WorldPacket& recvData);

        void HandleUseItemOpcode(WorldPacket& recvData);
        void HandleOpenItemOpcode(WorldPacket& recvData);
        void HandleOpenWrappedItemCallback(uint16 pos, ObjectGuid itemGuid, PreparedQueryResult result);
        void HandleCastSpellOpcode(WorldPacket& recvData);
        void HandleCancelCastOpcode(WorldPacket& recvData);
        void HandleCancelAuraOpcode(WorldPacket& recvData);
        void HandleCancelMountAuraOpcode(WorldPacket& recvData);
        void HandleCancelAutoRepeatSpellOpcode(WorldPacket& recvData);

        void HandleLearnTalentOpcode(WorldPacket& recvData);
        void HandleConfirmRespecWipe(WorldPacket& recvData);
        void HandleUnlearnSkillOpcode(WorldPacket& recvData);
        void HandleArcheologyRequestHistory(WorldPacket& recvData);

        void HandleQuestGiverStatusQueryOpcode(WorldPacket& recvData);
        void HandleQuestGiverStatusMultipleQuery(WorldPacket& recvData);
        void HandleQuestGiverHelloOpcode(WorldPacket& recvData);
        void HandleQuestGiverAcceptQuestOpcode(WorldPacket& recvData);
        void HandleQuestGiverQueryQuestOpcode(WorldPacket& recvData);
        void HandleQuestGiverChooseRewardOpcode(WorldPacket& recvData);
        void HandleQuestGiverRequestRewardOpcode(WorldPacket& recvData);
        void HandleQuestQueryOpcode(WorldPacket& recvData);
        void HandleQuestLogRemoveQuest(WorldPacket& recvData);
        void HandleQuestConfirmAccept(WorldPacket& recvData);
        void HandleQuestGiverCompleteQuest(WorldPacket& recvData);
        void HandleQuestPushToParty(WorldPacket& recvData);
        void HandleQuestPushResult(WorldPacket& recvData);

        void HandleMessagechatOpcode(WorldPacket& recvData);
        void HandleAddonMessagechatOpcode(WorldPacket& recvData);
        void SendPlayerNotFoundNotice(std::string const& name);
        void SendPlayerAmbiguousNotice(std::string const& name);
        void SendChatRestrictedNotice(ChatRestrictionType restriction);
        void HandleTextEmoteOpcode(WorldPacket& recvData);
        void HandleChatIgnoredOpcode(WorldPacket& recvData);

        void HandleUnregisterAddonPrefixesOpcode(WorldPacket& recvData);
        void HandleAddonRegisteredPrefixesOpcode(WorldPacket& recvData);

        void HandleReclaimCorpseOpcode(WorldPacket& recvData);
        void HandleCorpseQueryOpcode(WorldPacket& recvData);
        void HandleCorpseTransportQuery(WorldPacket& recvData);
        void HandleResurrectResponseOpcode(WorldPacket& recvData);
        void HandleSummonResponseOpcode(WorldPacket& recvData);

        void HandleChannelJoin(WorldPacket& recvData);
        void HandleChannelLeave(WorldPacket& recvData);
        void HandleChannelList(WorldPacket& recvData);
        void HandleChannelPassword(WorldPacket& recvData);
        void HandleChannelSetOwner(WorldPacket& recvData);
        void HandleChannelOwner(WorldPacket& recvData);
        void HandleChannelModerator(WorldPacket& recvData);
        void HandleChannelUnmoderator(WorldPacket& recvData);
        void HandleChannelMute(WorldPacket& recvData);
        void HandleChannelUnmute(WorldPacket& recvData);
        void HandleChannelInvite(WorldPacket& recvData);
        void HandleChannelKick(WorldPacket& recvData);
        void HandleChannelBan(WorldPacket& recvData);
        void HandleChannelUnban(WorldPacket& recvData);
        void HandleChannelAnnouncements(WorldPacket& recvData);
        void HandleChannelDeclineInvite(WorldPacket& recvData);

        void HandleCompleteCinematic(WorldPacket& recvData);
        void HandleNextCinematicCamera(WorldPacket& recvData);

        void HandleCompleteMovie(WorldPacket& recvData);

        void HandlePageTextQueryOpcode(WorldPacket& recvData);

        void HandleTutorialFlag (WorldPacket& recvData);
        void HandleTutorialClear(WorldPacket& recvData);
        void HandleTutorialReset(WorldPacket& recvData);

        //Pet
        void HandlePetAction(WorldPacket& recvData);
        void HandlePetStopAttack(WorldPacket& recvData);
        void HandlePetActionHelper(Unit* pet, ObjectGuid guid1, uint32 spellid, uint16 flag, ObjectGuid guid2, float x, float y, float z);
        void HandlePetNameQuery(WorldPacket& recvData);
        void HandlePetSetAction(WorldPacket& recvData);
        void HandlePetAbandon(WorldPacket& recvData);
        void HandlePetRename(WorldPacket& recvData);
        void HandlePetSpellAutocastOpcode(WorldPacket& recvData);
        void HandlePetCastSpellOpcode(WorldPacket& recvData);
        void HandeLearnPetSpecializationGroup(WorldPacket& recvData);

        void HandleSetActionBarToggles(WorldPacket& recvData);

        void HandleCharRenameOpcode(WorldPacket& recvData);
        void HandleChangePlayerNameOpcodeCallBack(std::string const& newName, PreparedQueryResult result);
        void HandleSetPlayerDeclinedNames(WorldPacket& recvData);
        void HandeSetTalentSpecialization(WorldPacket& recvData);

        void HandleTotemDestroyed(WorldPacket& recvData);
        void HandleDismissCritter(WorldPacket& recvData);

        //Battleground
        void HandleBattlemasterHelloOpcode(WorldPacket& recvData);
        void HandleBattlemasterJoinOpcode(WorldPacket& recvData);
        void HandleBattlemasterJoinRatedOpcode(WorldPacket& recvData);
        void HandlePVPLogDataOpcode(WorldPacket& recvData);
        void HandleBattlegroundPortOpcode(WorldPacket& recvData);
        void HandleBattlegroundListOpcode(WorldPacket& recvData);
        void HandleBattlegroundLeaveOpcode(WorldPacket& recvData);
        void HandleBattlemasterJoinArena(WorldPacket& recvData);
        void HandleReportPvPAFK(WorldPacket& recvData);
        void HandleRequestRatedInfo(WorldPacket& recvData);
        void HandleRequestPvpOptions(WorldPacket& recvData);
        void HandleRequestPvpReward(WorldPacket& recvData);
        void HandleRequestConquestFormulaConstants(WorldPacket& recvData);

        // WarGames
        void HandleWargameRequest(WorldPacket& recvData);
        void HandleWargameRequestResponse(WorldPacket& recvData);
        void SendWargameRequestMessage(ObjectGuid queueGuid);
        void SendWargameRequestToOpponent(ObjectGuid guid, ObjectGuid queueGuid, uint32 bgTypeId);

        void HandleWardenDataOpcode(WorldPacket& recvData);
        void HandleWorldTeleportOpcode(WorldPacket& recvData);
        void HandleMinimapPingOpcode(WorldPacket& recvData);
        void HandleRandomRollOpcode(WorldPacket& recvData);
        void HandleFarSightOpcode(WorldPacket& recvData);
        void HandleSetDungeonDifficultyOpcode(WorldPacket& recvData);
        void HandleSetRaidDifficultyOpcode(WorldPacket& recvData);
        void HandleChangePlayerDifficulty(WorldPacket& recvData);
        void HandleMoveSetCanFlyAckOpcode(WorldPacket& recvData);
        void HandleTimeSyncResp(WorldPacket& recvData);
        void HandleWhoisOpcode(WorldPacket& recvData);
        void HandleResetInstancesOpcode(WorldPacket& recvData);
        void HandleHearthAndResurrect(WorldPacket& recvData);
        void HandleInstanceLockResponse(WorldPacket& recvData);

        // Battlefield
        void SendBfInvitePlayerToWar(ObjectGuid guid, uint32 zoneId, uint32 time);
        void SendBfDeclineInviteToWar(ObjectGuid guid, bool OutsideWarZone);
        void SendBfInvitePlayerToQueue(ObjectGuid guid, int8 battleState);
        void SendBfQueueInviteResponse(ObjectGuid guid, uint32 zoneId, int8 battleStatus, bool canQueue = true, bool loggingIn = false);
        void SendBfEntered(ObjectGuid guid, bool relocated, bool onOffense);
        void SendBfLeaveMessage(ObjectGuid guid, int8 battleState, bool relocated, BFLeaveReason reason = BF_LEAVE_REASON_EXITED);
        void HandleBfQueueInviteResponse(WorldPacket& recvData);
        void HandleBfEntryInviteResponse(WorldPacket& recvData);
        void HandleBfExitRequest(WorldPacket& recvData);
        void HandleBfQueueRequest(WorldPacket& recvData);

        // Looking for Dungeon/Raid
        void HandleLfgSetCommentOpcode(WorldPacket& recvData);
        void HandleLfgGetLockInfoOpcode(WorldPacket& recvData);
        void SendLfgPlayerLockInfo();
        void SendLfgPartyLockInfo();
        void HandleLfgJoinOpcode(WorldPacket& recvData);
        void HandleLfgLeaveOpcode(WorldPacket& recvData);
        void HandleLfgSetRolesOpcode(WorldPacket& recvData);
        void HandleLfgProposalResultOpcode(WorldPacket& recvData);
        void HandleLfgSetBootVoteOpcode(WorldPacket& recvData);
        void HandleLfgTeleportOpcode(WorldPacket& recvData);
        void HandleLfgGetStatus(WorldPacket& recvData);
        void HandleSetLFGBonusReputationOpcode(WorldPacket& recvData);
        void HandleLFGGetJoinStatus(WorldPacket& recvData);

        void SendLfgUpdateStatus(LfgUpdateData const& updateData, bool party);
        void SendLfgRoleChosen(ObjectGuid guid, uint8 roles);
        void SendLfgRoleCheckUpdate(LfgRoleCheck const& pRoleCheck);
        void SendLfgLfrList(bool update);
        void SendLfgJoinResult(LfgJoinResultData const& joinData);
        void SendLfgQueueStatus(LfgQueueStatusData const& queueData);
        void SendLfgPlayerReward(LfgPlayerRewardData const& lfgPlayerRewardData);
        void SendLfgBootProposalUpdate(LfgPlayerBoot const& boot);
        void SendLfgUpdateProposal(LfgProposal const& proposal);
        void SendLfgDisabled();
        void SendLfgOfferContinue(uint32 dungeonEntry);
        void SendLfgTeleportError(uint8 err);

        void HandleAreaSpiritHealerQueryOpcode(WorldPacket& recvData);
        void HandleAreaSpiritHealerQueueOpcode(WorldPacket& recvData);
        void HandleSelfResOpcode(WorldPacket& recvData);
        void HandleSpamComplainOpcode(WorldPacket& recvData);
        void HandleRequestPetInfoOpcode(WorldPacket& recvData);

        // Socket gem
        void HandleSocketOpcode(WorldPacket& recvData);

        void HandleCancelTempEnchantmentOpcode(WorldPacket& recvData);

        void HandleItemRefundInfoRequest(WorldPacket& recvData);
        void HandleItemRefund(WorldPacket& recvData);

        void HandleChannelVoiceOnOpcode(WorldPacket& recvData);
        void HandleChannelVoiceOffOpcode(WorldPacket& recvData);
        void HandleVoiceSessionEnableOpcode(WorldPacket& recvData);
        void HandleChannelSetActiveVoice(WorldPacket& recvData);
        void HandleSetTaxiBenchmarkOpcode(WorldPacket& recvData);

        // Guild Bank
        void HandleGuildPermissions(WorldPacket& recvData);
        void HandleGuildBankMoneyWithdrawn(WorldPacket& recvData);
        void HandleGuildBankerActivate(WorldPacket& recvData);
        void HandleGuildBankQueryTab(WorldPacket& recvData);
        void HandleGuildBankLogQuery(WorldPacket& recvData);
        void HandleGuildBankDepositMoney(WorldPacket& recvData);
        void HandleGuildBankWithdrawMoney(WorldPacket& recvData);
        void HandleGuildBankSwapItems(WorldPacket& recvData);
        void HandleGuildBankTabNote(WorldPacket& recvData);

        void HandleGuildBankUpdateTab(WorldPacket& recvData);
        void HandleGuildBankBuyTab(WorldPacket& recvData);
        void HandleQueryGuildBankTabText(WorldPacket& recvData);
        void HandleSetGuildBankTabText(WorldPacket& recvData);
        void HandleGuildQueryXPOpcode(WorldPacket& recvData);

        // Recruit A Friend
        void HandleRecruitAFriendOpcode(WorldPacket& recvData);
        void SendRecruitAFriendError(uint32 error);
        void SendRaFRewards(bool show);

        // Refer A Friend
        void HandleReferAFriendGrantLevelPropose(WorldPacket& recvData);
        void HandleReferAFriendGrantLevelAccept(WorldPacket& recvData);
        void SendReferAFriendError(uint32 error, Player* target);

        // Calendar
        void HandleCalendarGetCalendar(WorldPacket& recvData);
        void HandleCalendarGetEvent(WorldPacket& recvData);
        void HandleCalendarGuildFilter(WorldPacket& recvData);
        void HandleCalendarAddEvent(WorldPacket& recvData);
        void HandleCalendarUpdateEvent(WorldPacket& recvData);
        void HandleCalendarRemoveEvent(WorldPacket& recvData);
        void HandleCalendarCopyEvent(WorldPacket& recvData);
        void HandleCalendarEventInvite(WorldPacket& recvData);
        void HandleCalendarEventRsvp(WorldPacket& recvData);
        void HandleCalendarEventRemoveInvite(WorldPacket& recvData);
        void HandleCalendarEventStatus(WorldPacket& recvData);
        void HandleCalendarEventModeratorStatus(WorldPacket& recvData);
        void HandleCalendarComplain(WorldPacket& recvData);
        void HandleCalendarGetNumPending(WorldPacket& recvData);
        void HandleCalendarEventSignup(WorldPacket& recvData);

        void SendCalendarRaidLockout(InstanceSave const* save, bool add);
        void SendCalendarRaidLockoutUpdated(InstanceSave const* save);
        void HandleSetSavedInstanceExtend(WorldPacket& recvData);

        // Void Storage
        void HandleVoidStorageUnlock(WorldPacket& recvData);
        void HandleVoidStorageQuery(WorldPacket& recvData);
        void HandleVoidStorageTransfer(WorldPacket& recvData);
        void HandleVoidSwapItem(WorldPacket& recvData);
        void SendVoidStorageTransferResult(VoidTransferError result);

        // Transmogrification
        void HandleTransmogrifyItems(WorldPacket& recvData);

        // Reforge
        void HandleReforgeItemOpcode(WorldPacket& recvData);
        void SendReforgeResult(bool success);

        // BlackMarket
        void HandleBlackMarketOpen(WorldPacket& recvData);
        void HandleBlackMarketRequestItems(WorldPacket& recvData);
        void HandleBlackMarketBidOnItem(WorldPacket& recvData);

        void SendBlackMarketOpenResult(ObjectGuid guid, Creature* auctioneer);
        void SendBlackMarketBidOnItemResult(int32 result, int32 marketId, uint32 item);
        void SendBlackMarketWonNotification(BlackMarketEntry const* entry, Item const* item);
        void SendBlackMarketOutbidNotification(BlackMarketTemplate const* templ);

        // Item Upgrade
        void SendItemUpgradeResult(bool success);
        void HandleUpgradeItem(WorldPacket& recvData);

        // Miscellaneous
        void HandleSpellClick(WorldPacket& recvData);
        void HandleMirrorImageDataRequest(WorldPacket& recvData);
        void HandleAlterAppearance(WorldPacket& recvData);
        void SendBarberShopResult(BarberShopResult result);
        void HandleCharCustomize(WorldPacket& recvData);
        void HandleCharCustomizeCallback(std::shared_ptr<CharCustomizeInfo> customizeInfo, PreparedQueryResult result);

        void HandleQueryInspectAchievements(WorldPacket& recvData);
        void HandleGuildAchievementProgressQuery(WorldPacket& recvData);
        void HandleEquipmentSetSave(WorldPacket& recvData);
        void HandleEquipmentSetDelete(WorldPacket& recvData);
        void HandleEquipmentSetUse(WorldPacket& recvData);
        void HandleWorldStateUITimerUpdate(WorldPacket& recvData);
        void HandleReadyForAccountDataTimes(WorldPacket& recvData);
        void HandleQuestNPCQuery(WorldPacket& recvData);
        void HandleQuestPOIQuery(WorldPacket& recvData);
        void HandleEjectPassenger(WorldPacket& recvData);
        void HandleEnterPlayerVehicle(WorldPacket& recvData);
        void HandleUpdateProjectilePosition(WorldPacket& recvData);
        void HandleUpdateMissileTrajectory(WorldPacket& recvData);
        void HandleViolenceLevel(WorldPacket& recvData);
        void HandleObjectUpdateFailedOpcode(WorldPacket& recvData);
        void HandleObjectUpdateRescuedOpcode(WorldPacket& recvData);
        void HandleSelectFactionOpcode(WorldPacket& recvData);
        void HandleRequestCategoryCooldowns(WorldPacket& recvData);
        void HandleQueryCountdownTimer(WorldPacket& recvData);

        int32 HandleEnableNagleAlgorithm();

        // Compact Unit Frames (4.x)
        void HandleSaveCUFProfiles(WorldPacket& recvData);
        void SendLoadCUFProfiles();

        // Battle Pet
        void HandleBattlePetModifyName(WorldPacket& recvData);
        void HandleBattlePetRelease(WorldPacket& recvData);
        void HandleBattlePetSetBattleSlot(WorldPacket& recvData);
        void HandleBattlePetSetFlags(WorldPacket& recvData);
        void HandleBattlePetQueryName(WorldPacket& recvData);
        void HandleBattlePetSummonCompanion(WorldPacket& recvData);
        void HandleBattlePetCage(WorldPacket& recvData);

        // Pet Battle
        void HandlePetBattleInput(WorldPacket& recvData);
        void HandlePetBattleRequestWild(WorldPacket& recvData);
        void HandlePetBattleJoinQueue(WorldPacket& recvData);
        void HandlePetBattleRemoveFromQueue(WorldPacket& recvData);
        void HandlePetBattleProposeMatchResult(WorldPacket& recvData);
        void HandlePetBattleRequestDuel(WorldPacket& recvData);
        void HandlePetBattleRequestUpdate(WorldPacket& recvData);
        void HandlePetBattleReplaceFrontPet(WorldPacket& recvData);

        void SendPetBattleFinalizeLocation(PetBattleRequest& request);
        void SendPetBattleFirstRound(PetBattle* petBattle);
        void SendPetBattleInitialUpdate(PetBattle* petBattle, bool Inverse = false);
        void SendPetBattleRequestFailed(uint8 reason);
        void SendPetBattleRoundResult(PetBattle* petBattle);
        void SendPetBattleFinalRound(PetBattle* petBattle);
        void SendPetBattleFinished();

        void SetPetBattleQueueStatus(uint32 Status, uint32 WaitTime = 0, uint32 AverageWaitTime = 0);
        void SetPetBattleQueueError(uint32 Error);
        void SetPetBattleQueueSlotsError(PetBattleSlotErrorArray SlotErrors);
        void SetPetBattleQueueProposeMatch();

        // Titles
        void HandleSetTitleOpcode(WorldPacket& recvData);
        void SendTitleEarned(uint32 TitleIndex);
        void SendTitleLost(uint32 TitleIndex);

        // Battle Pay Shop
        void HandleBattlePayShopGetProductList(WorldPacket& recvData);
        void HandleBattlePayShopStartPurchase(WorldPacket& recvData);
        void HandleBattlePayShopCharBoost(WorldPacket& recvData);
        void HandleBattlePayShopConfirmedPurchase(WorldPacket& recvData);
        void HandleBattlePayShopAckFailed(WorldPacket& recvData);

        void SendBattlePayShopProductList();
        void SendBattlePayShopDistributionList();
        void SendBattlePayShopPurchaseList();
        void SendBattlePayShopAckFailed(uint32 error, uint32 clientToken, uint64 purchaseId);
        void SendBattlePayShopPurchaseUpdate(uint32 purchaseStatus, uint32 purchaseResult, uint32 productId, uint64 purchaseId);
        void SendBattlePayShopDistributingUpdate(ObjectGuid characterGUID, uint64 distributeId, BattlePayShopPurchasedProduct const* distributedProduct, uint32 distributingResult);

        // Scenes
        void HandleSceneTriggerEvent(WorldPacket& recvPacket);
        void HandleScenePlaybackComplete(WorldPacket& recvPacket);
        void HandleScenePlaybackCanceled(WorldPacket& recvPacket);

        // Scenarios
        void HandleScenarioQueryPOI(WorldPacket& recvPacket);

    public:
        QueryCallbackProcessor& GetQueryProcessor() { return _queryProcessor; }

    private:
        void ProcessQueryCallbacks();

        QueryResultHolderFuture     _authSessionCallback;
        QueryResultHolderFuture     _charLoginCallback;
        QueryResultHolderFuture     _accountLoginCallback;
        QueryResultHolderFuture     _petLoginCallback;

        QueryCallbackProcessor _queryProcessor;

    friend class World;
    protected:
        class DosProtection
        {
            friend class World;
            public:
                DosProtection(WorldSession* s) : Session(s), _policy((Policy)sWorld->getIntConfig(CONFIG_PACKET_SPOOF_POLICY)) { }
                bool EvaluateOpcode(WorldPacket& p, time_t time) const;
            protected:
                enum Policy
                {
                    POLICY_LOG,
                    POLICY_KICK,
                    POLICY_BAN,
                };

                uint32 GetMaxPacketCounterAllowed(uint16 opcode) const;

                WorldSession* Session;

            private:
                Policy _policy;
                typedef std::unordered_map<uint16, PacketCounter> PacketThrottlingMap;
                // mark this member as "mutable" so it can be modified even in const functions
                mutable PacketThrottlingMap _PacketThrottlingMap;

                DosProtection(DosProtection const& right) = delete;
                DosProtection& operator=(DosProtection const& right) = delete;
        } AntiDOS;

    private:
        // private trade methods
        void moveItems(Item* myItems[], Item* hisItems[]);

        bool CanUseBank(ObjectGuid bankerGUID = ObjectGuid::Empty) const;

        // logging helper
        void LogUnexpectedOpcode(WorldPacket* packet, const char* status, const char *reason);
        void LogUnprocessedTail(WorldPacket* packet);

        // EnumData helpers
        bool IsLegitCharacterForAccount(ObjectGuid lowGUID)
        {
            return _legitCharacters.find(lowGUID) != _legitCharacters.end();
        }

        // this stores the GUIDs of the characters who can login
        // characters who failed on Player::BuildEnumData shouldn't login
        GuidSet _legitCharacters;

        uint32 m_GUIDLow;                                   // set logined or recently logout player (while m_playerRecentlyLogout set)
        Player* _player;
        WorldSocket* m_Socket;
        std::string m_Address;                              // Current Remote Address
     // std::string m_LAddress;                             // Last Attempted Remote Adress - we can not set attempted ip for a non-existing session!

        AccountTypes _security;
        uint32 _accountId;
        uint8 m_expansion;

        typedef std::vector<AddonInfo> AddonsList;

        // Warden
        Warden* _warden;                                    // Remains NULL if Warden system is not enabled by config

        time_t _logoutTime;
        bool m_inQueue;                                     // session wait in auth.queue
        bool m_playerLoading;                               // code processed in LoginPlayer
        bool m_playerLogout;                                // code processed in LogoutPlayer
        bool m_playerRecentlyLogout;
        bool m_playerSave;
        LocaleConstant m_sessionDbcLocale;
        LocaleConstant m_sessionDbLocaleIndex;
        uint32 m_latency;
        uint32 m_clientTimeDelay;
        AccountData m_accountData[NUM_ACCOUNT_DATA_TYPES];
        uint32 m_Tutorials[MAX_ACCOUNT_TUTORIAL_VALUES];
        uint8  m_TutorialsChanged;
        AddonsList m_addonsList;
        std::vector<std::string> _registeredAddonPrefixes;
        bool _filterAddonMessages;
        LockedQueue<WorldPacket*> _recvQueue;
        rbac::RBACData* _RBACData;
        uint32 expireTime;
        bool forceExit;
        ObjectGuid m_currentBankerGUID;

        int32 m_guildRosterTimer;

        BattlePayShop* _battlePayShop;

        RaFData* _rafData;

        WorldSession(WorldSession const& right) = delete;
        WorldSession& operator=(WorldSession const& right) = delete;
};
#endif
/// @}
