/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <ace/Message_Block.h>
#include <ace/OS_NS_string.h>
#include <ace/OS_NS_unistd.h>
#include <ace/os_include/arpa/os_inet.h>
#include <ace/os_include/netinet/os_tcp.h>
#include <ace/os_include/sys/os_types.h>
#include <ace/os_include/sys/os_socket.h>
#include <ace/OS_NS_string.h>
#include <ace/Reactor.h>
#include <ace/Auto_Ptr.h>

#include "WorldSocket.h"
#include "Common.h"
#include "Player.h"
#include "Util.h"
#include "World.h"
#include "WorldPacket.h"
#include "SharedDefines.h"
#include "ByteBuffer.h"
#include "Opcodes.h"
#include "DatabaseEnv.h"
#include "BigNumber.h"
#include "SHA1.h"
#include "WorldSession.h"
#include "WorldSocketMgr.h"
#include "Log.h"
#include "PacketLog.h"
#include "ScriptMgr.h"
#include "AccountMgr.h"

#include "zlib.h"

#if defined(__GNUC__)
#pragma pack(1)
#else
#pragma pack(push, 1)
#endif

struct ServerPktHeader
{
    ServerPktHeader(uint32 size, uint32 cmd, AuthCrypt* _authCrypt) : size(size)
    {
        if (_authCrypt->IsInitialized())
        {
            uint32 data = (size << 13) | (cmd & MAX_OPCODE);
            memcpy(&header[0], &data, 4);
            _authCrypt->EncryptSend((uint8*)&header[0], getHeaderLength());
        }
        else
        {
            // Dynamic header size is not needed anymore, we are using not encrypted part for only the first few packets
            memcpy(&header[0], &size, 2);
            memcpy(&header[2], &cmd, 2);
        }
    }

    uint8 getHeaderLength()
    {
        return 4;
    }

    const uint32 size;
    uint8 header[4];
};

struct AuthClientPktHeader
{
    uint16 size;
    uint32 cmd;
};

struct WorldClientPktHeader
{
    uint16 size;
    uint16 cmd;
};

#if defined(__GNUC__)
#pragma pack()
#else
#pragma pack(pop)
#endif

WorldSocket::WorldSocket (void): WorldHandler(),
m_LastPingTime(ACE_Time_Value::zero), m_OverSpeedPings(0), m_Session(0),
m_RecvWPct(0), m_RecvPct(), m_Header(sizeof(AuthClientPktHeader)),
m_WorldHeader(sizeof(WorldClientPktHeader)), m_OutBuffer(0),
m_OutBufferSize(65536), m_OutActive(false), m_zstream(),
m_Seed( Math::Rand( 0u, std::numeric_limits< uint32 >::max() ))
{
    reference_counting_policy().value (ACE_Event_Handler::Reference_Counting_Policy::ENABLED);

    msg_queue()->high_water_mark(8 * 1024 * 1024);
    msg_queue()->low_water_mark(8 * 1024 * 1024);

    m_zstream = new z_stream_s();
    m_zstream->zalloc = (alloc_func)0;
    m_zstream->zfree = (free_func)0;
    m_zstream->opaque = (voidpf)0;

    int z_res = deflateInit(m_zstream, sWorld->getIntConfig(CONFIG_COMPRESSION));
    if (z_res != Z_OK)
    {
        TC_LOG_ERROR("network", "WorldSocket: Can't initialize packet compression deflateInit failed with code %d", z_res);
        ASSERT(z_res == Z_OK);
    }
}

WorldSocket::~WorldSocket (void)
{
    delete m_RecvWPct;

    if (m_OutBuffer)
        m_OutBuffer->release();

    int z_res = deflateEnd(m_zstream);
    if (z_res != Z_OK && z_res != Z_DATA_ERROR)
    {
        TC_LOG_ERROR("network", "WorldSocket: Can't close packet compression stream. deflateEnd failed with code %d", z_res);
        return;
    }

    closing_ = true;

    peer().close();
}

bool WorldSocket::IsClosed (void) const
{
    return closing_;
}

void WorldSocket::CloseSocket (void)
{
    {
        ACE_GUARD (LockType, Guard, m_OutBufferLock);

        if (closing_)
            return;

        closing_ = true;
        peer().close_writer();
    }

    {
        ACE_GUARD (LockType, Guard, m_SessionLock);

        m_Session = NULL;
    }
}

const std::string& WorldSocket::GetRemoteAddress (void) const
{
    return m_Address;
}

int WorldSocket::SendPacket(WorldPacket const& pct)
{
    ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, -1);

    if (closing_)
        return -1;

    // Dump outgoing packet
    if (sPacketLog->CanLogPacket())
        sPacketLog->LogPacket(pct, SERVER_TO_CLIENT);

    // @todo 5.4.8: find SMSG_COMPRESSED_PACKET opcode number
    /*
    WorldPacket compressed;
    size_t size = pct.size();

    if (size >= 45 && pct.GetOpcode() != MSG_VERIFY_CONNECTIVITY && pct.GetOpcode() != SMSG_MOTD)
    {
        size += sizeof(uint32);

        size_t reserved_size = deflateBound(m_zstream, size) + 12;
        compressed.resize(reserved_size);
        compressed.put<uint32>(0, size);

        ByteBuffer buff(size);
        {
            buff.append((uint32)pct.GetOpcode());
            buff.append(pct.contents(), pct.size());
        }

        uint32 adler_1 = (uint32)adler32(2552748273u, (const Bytef*)buff.contents(), buff.size());

        compressed.put<uint32>(4, adler_1);

        m_zstream->next_in = (Bytef*)buff.contents();
        m_zstream->avail_in = buff.size();

        m_zstream->next_out = (Bytef*)(compressed.contents() + 12);
        m_zstream->avail_out = reserved_size - 12;

        int z_res = deflate(m_zstream, Z_SYNC_FLUSH);

        size_t totalOut = m_zstream->next_out - compressed.contents();
        TC_LOG_ERROR("network", "totalOut = %u, reserved_size = %u, m_zstream->avail_out = %u", (uint32)totalOut, (uint32)reserved_size, (uint32)m_zstream->avail_out);
        ASSERT(totalOut == reserved_size - m_zstream->avail_out);

        uint32 adler_2 = (uint32)adler32(2552748273u, (const Bytef*)(compressed.contents() + 12), totalOut - 12);

        compressed.put<uint32>(8, adler_2);

        if (z_res != Z_OK)
            TC_LOG_ERROR("network", "Can't compress packet (zlib: deflate) Error code: %i (%s)", z_res, zError(z_res));
        else if (m_zstream->avail_in != 0)
            TC_LOG_ERROR("network", "Can't compress packet (zlib: deflate not greedy)");
        else
        {
            compressed.resize(totalOut + 12);
            compressed.SetOpcode(SMSG_COMPRESSED_PACKET);
            pct = compressed;
            size = pct.size();
        }

        m_zstream->next_in = NULL;
        m_zstream->next_out = NULL;
        m_zstream->avail_in = 0;
        m_zstream->avail_out = 0;
    }
    */

    uint16 opcodeNumber = serverOpcodeTable[pct.GetOpcode()]->OpcodeNumber;

    ServerPktHeader header(!m_Crypt.IsInitialized() ? pct.size() + 2 : pct.size(), opcodeNumber, &m_Crypt);

    if (m_OutBuffer->space() >= pct.size() + header.getHeaderLength() && msg_queue()->is_empty())
    {
        // Put the packet on the buffer.
        if (m_OutBuffer->copy((char*) header.header, header.getHeaderLength()) == -1)
            ACE_ASSERT (false);

        if (!pct.empty())
            if (m_OutBuffer->copy((char*) pct.contents(), pct.size()) == -1)
                ACE_ASSERT (false);
    }
    else
    {
        // Enqueue the packet.
        ACE_Message_Block* mb;

        ACE_NEW_RETURN(mb, ACE_Message_Block(pct.size() + header.getHeaderLength()), -1);

        mb->copy((char*) header.header, header.getHeaderLength());

        if (!pct.empty())
            mb->copy((const char*)pct.contents(), pct.size());

        if (msg_queue()->enqueue_tail(mb, (ACE_Time_Value*)&ACE_Time_Value::zero) == -1)
        {
            TC_LOG_ERROR("network", "WorldSocket::SendPacket enqueue_tail failed");
            mb->release();
            return -1;
        }
    }

    return 0;
}

long WorldSocket::AddReference (void)
{
    return static_cast<long> (add_reference());
}

long WorldSocket::RemoveReference (void)
{
    return static_cast<long> (remove_reference());
}

int WorldSocket::open (void *a)
{
    ACE_UNUSED_ARG (a);

    // Prevent double call to this func.
    if (m_OutBuffer)
        return -1;

    // This will also prevent the socket from being Updated
    // while we are initializing it.
    m_OutActive = true;

    // Hook for the manager.
    if (sWorldSocketMgr->OnSocketOpen(this) == -1)
        return -1;

    // Allocate the buffer.
    ACE_NEW_RETURN (m_OutBuffer, ACE_Message_Block (m_OutBufferSize), -1);

    // Store peer address.
    ACE_INET_Addr remote_addr;

    if (peer().get_remote_addr(remote_addr) == -1)
    {
        TC_LOG_ERROR("network", "WorldSocket::open: peer().get_remote_addr errno = %s", ACE_OS::strerror (errno));
        return -1;
    }

    m_Address = remote_addr.get_host_addr();

    // not an opcode. this packet sends raw string WORLD OF WARCRAFT CONNECTION - SERVER TO CLIENT"
    // because of our implementation, bytes "WO" become the opcode
    WorldPacket packet(MSG_VERIFY_CONNECTIVITY);
    packet << std::string("RLD OF WARCRAFT CONNECTION - SERVER TO CLIENT");

    if (SendPacket(packet) == -1)
        return -1;

    // Register with ACE Reactor
    if (reactor()->register_handler(this, ACE_Event_Handler::READ_MASK | ACE_Event_Handler::WRITE_MASK) == -1)
    {
        TC_LOG_ERROR("network", "WorldSocket::open: unable to register client handler errno = %s", ACE_OS::strerror (errno));
        return -1;
    }

    // reactor takes care of the socket from now on
    remove_reference();

    return 0;
}

int WorldSocket::close(u_long)
{
    shutdown();

    closing_ = true;

    remove_reference();

    return 0;
}

int WorldSocket::handle_input(ACE_HANDLE)
{
    if (closing_)
        return -1;

    switch (handle_input_missing_data())
    {
        case -1 :
        {
            if ((errno == EWOULDBLOCK) ||
                (errno == EAGAIN))
            {
                return Update();                           // interesting line, isn't it ?
            }

            TC_LOG_DEBUG("network", "WorldSocket::handle_input: Peer error closing connection errno = %s", ACE_OS::strerror (errno));

            errno = ECONNRESET;
            return -1;
        }
        case 0:
        {
            TC_LOG_DEBUG("network", "WorldSocket::handle_input: Peer has closed connection");

            errno = ECONNRESET;
            return -1;
        }
        case 1:
            return 1;
        default:
            return Update();                               // another interesting line ;)
    }

    ACE_NOTREACHED(return -1);
}

int WorldSocket::handle_output(ACE_HANDLE)
{
    ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, -1);

    if (closing_)
        return -1;

    size_t send_len = m_OutBuffer->length();

    if (send_len == 0)
        return handle_output_queue(Guard);

#ifdef MSG_NOSIGNAL
    ssize_t n = peer().send (m_OutBuffer->rd_ptr(), send_len, MSG_NOSIGNAL);
#else
    ssize_t n = peer().send (m_OutBuffer->rd_ptr(), send_len);
#endif // MSG_NOSIGNAL

    if (n == 0)
        return -1;
    else if (n == -1)
    {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
            return schedule_wakeup_output (Guard);

        return -1;
    }
    else if (n < (ssize_t)send_len) //now n > 0
    {
        m_OutBuffer->rd_ptr (static_cast<size_t> (n));

        // move the data to the base of the buffer
        m_OutBuffer->crunch();

        return schedule_wakeup_output (Guard);
    }
    else //now n == send_len
    {
        m_OutBuffer->reset();

        return handle_output_queue (Guard);
    }

    ACE_NOTREACHED (return 0);
}

int WorldSocket::handle_output_queue(GuardType& g)
{
    if (msg_queue()->is_empty())
        return cancel_wakeup_output(g);

    ACE_Message_Block* mblk;

    if (msg_queue()->dequeue_head(mblk, (ACE_Time_Value*)&ACE_Time_Value::zero) == -1)
    {
        TC_LOG_ERROR("network", "WorldSocket::handle_output_queue dequeue_head");
        return -1;
    }

    const size_t send_len = mblk->length();

#ifdef MSG_NOSIGNAL
    ssize_t n = peer().send(mblk->rd_ptr(), send_len, MSG_NOSIGNAL);
#else
    ssize_t n = peer().send(mblk->rd_ptr(), send_len);
#endif // MSG_NOSIGNAL

    if (n == 0)
    {
        mblk->release();

        return -1;
    }
    else if (n == -1)
    {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
        {
            msg_queue()->enqueue_head(mblk, (ACE_Time_Value*) &ACE_Time_Value::zero);
            return schedule_wakeup_output (g);
        }

        mblk->release();
        return -1;
    }
    else if (n < (ssize_t)send_len) //now n > 0
    {
        mblk->rd_ptr(static_cast<size_t> (n));

        if (msg_queue()->enqueue_head(mblk, (ACE_Time_Value*) &ACE_Time_Value::zero) == -1)
        {
            TC_LOG_ERROR("network", "WorldSocket::handle_output_queue enqueue_head");
            mblk->release();
            return -1;
        }

        return schedule_wakeup_output (g);
    }
    else //now n == send_len
    {
        mblk->release();

        return msg_queue()->is_empty() ? cancel_wakeup_output(g) : ACE_Event_Handler::WRITE_MASK;
    }

    ACE_NOTREACHED(return -1);
}

int WorldSocket::handle_close(ACE_HANDLE h, ACE_Reactor_Mask)
{
    // Critical section
    {
        ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, -1);

        closing_ = true;

        if (h == ACE_INVALID_HANDLE)
            peer().close_writer();
    }

    // Critical section
    {
        ACE_GUARD_RETURN (LockType, Guard, m_SessionLock, -1);

        m_Session = NULL;
    }

    reactor()->remove_handler(this, ACE_Event_Handler::DONT_CALL | ACE_Event_Handler::ALL_EVENTS_MASK);
    return 0;
}

int WorldSocket::Update (void)
{
    if (closing_)
        return -1;

    if (m_OutActive)
        return 0;

    {
        ACE_GUARD_RETURN (LockType, Guard, m_OutBufferLock, 0);
        if (m_OutBuffer->length() == 0 && msg_queue()->is_empty())
            return 0;
    }

    int ret;
    do
        ret = handle_output(get_handle());
    while (ret > 0);

    return ret;
}

int WorldSocket::handle_input_header (void)
{
    ACE_ASSERT(m_RecvWPct == NULL);


    if (m_Crypt.IsInitialized())
    {
        ACE_ASSERT(m_WorldHeader.length() == sizeof(WorldClientPktHeader));
        uint8* uintHeader = (uint8*)m_WorldHeader.rd_ptr();
        m_Crypt.DecryptRecv(uintHeader, sizeof(WorldClientPktHeader));
        WorldClientPktHeader& header = *(WorldClientPktHeader*)uintHeader;

        uint32 value = *(uint32*)uintHeader;
        header.cmd = value & 0x1FFF;
        header.size = ((value & ~(uint32)0x1FFF) >> 13);

        if (header.size > 10236)
        {
            Player* _player = m_Session ? m_Session->GetPlayer() : NULL;
            TC_LOG_ERROR("network", "WorldSocket::handle_input_header(): client (account: %u, char [GUID: %u, name: %s]) sent malformed packet (size: %d, cmd: %d)",
                m_Session ? m_Session->GetAccountId() : 0,
                _player ? _player->GetGUID().GetCounter() : 0,
                _player ? _player->GetName().c_str() : "<none>",
                header.size, header.cmd);

            errno = EINVAL;
            return -1;
        }

        uint16 opcodeNumber = PacketFilter::DropHighBytes(header.cmd);
        ACE_NEW_RETURN(m_RecvWPct, WorldPacket(clientOpcodeTable.GetOpcodeByNumber(opcodeNumber), header.size), -1);
        m_RecvWPct->SetReceivedOpcode(opcodeNumber);

        if (header.size > 0)
        {
            m_RecvWPct->resize(header.size);
            m_RecvPct.base ((char*) m_RecvWPct->contents(), m_RecvWPct->size());
        }
        else
            ACE_ASSERT(m_RecvPct.space() == 0);
    }
    else
    {
        ACE_ASSERT(m_Header.length() == sizeof(AuthClientPktHeader));
        uint8* uintHeader = (uint8*)m_Header.rd_ptr();
        AuthClientPktHeader& header = *((AuthClientPktHeader*)uintHeader);

        if ((header.size < 4) || (header.size > 10240))
        {
            Player* _player = m_Session ? m_Session->GetPlayer() : NULL;
            TC_LOG_ERROR("network", "WorldSocket::handle_input_header(): client (account: %u, char [GUID: %u, name: %s]) sent malformed packet (size: %d, cmd: %d)",
                m_Session ? m_Session->GetAccountId() : 0,
                _player ? _player->GetGUID().GetCounter() : 0,
                _player ? _player->GetName().c_str() : "<none>",
                header.size, header.cmd);

            errno = EINVAL;
            return -1;
        }

        header.size -= 4;

        uint16 opcodeNumber = PacketFilter::DropHighBytes(header.cmd);
        ACE_NEW_RETURN(m_RecvWPct, WorldPacket(clientOpcodeTable.GetOpcodeByNumber(opcodeNumber), header.size), -1);
        m_RecvWPct->SetReceivedOpcode(opcodeNumber);

        if (header.size > 0)
        {
            m_RecvWPct->resize(header.size);
            m_RecvPct.base ((char*) m_RecvWPct->contents(), m_RecvWPct->size());
        }
        else
            ACE_ASSERT(m_RecvPct.space() == 0);
    }

    return 0;
}
int WorldSocket::handle_input_payload (void)
{
    // set errno properly here on error !!!
    // now have a header and payload

    if (m_Crypt.IsInitialized())
    {
        ACE_ASSERT (m_RecvPct.space() == 0);
        ACE_ASSERT (m_WorldHeader.space() == 0);
        ACE_ASSERT (m_RecvWPct != NULL);

        const int ret = ProcessIncoming (m_RecvWPct);

        m_RecvPct.base (NULL, 0);
        m_RecvPct.reset();
        m_RecvWPct = NULL;

        m_WorldHeader.reset();

        if (ret == -1)
            errno = EINVAL;

        return ret;
    }
    else
    {
        ACE_ASSERT(m_RecvPct.space() == 0);
        ACE_ASSERT(m_Header.space() == 0);
        ACE_ASSERT(m_RecvWPct != NULL);

        const int ret = ProcessIncoming(m_RecvWPct);

        m_RecvPct.base(NULL, 0);
        m_RecvPct.reset();
        m_RecvWPct = NULL;

        m_Header.reset();

        if (ret == -1)
            errno = EINVAL;

        return ret;
    }
}

int WorldSocket::handle_input_missing_data (void)
{
    char buf [4096];

    ACE_Data_Block db (sizeof (buf),
                        ACE_Message_Block::MB_DATA,
                        buf,
                        0,
                        0,
                        ACE_Message_Block::DONT_DELETE,
                        0);

    ACE_Message_Block message_block(&db,
                                    ACE_Message_Block::DONT_DELETE,
                                    0);

    const size_t recv_size = message_block.space();

    const ssize_t n = peer().recv (message_block.wr_ptr(),
                                          recv_size);

    if (n <= 0)
        return int(n);

    message_block.wr_ptr (n);

    while (message_block.length() > 0)
    {
        if (m_Crypt.IsInitialized())
        {
            if (m_WorldHeader.space() > 0)
            {
                //need to receive the header
                const size_t to_header = (message_block.length() > m_WorldHeader.space() ? m_WorldHeader.space() : message_block.length());
                m_WorldHeader.copy (message_block.rd_ptr(), to_header);
                message_block.rd_ptr (to_header);

                if (m_WorldHeader.space() > 0)
                {
                    // Couldn't receive the whole header this time.
                    ACE_ASSERT (message_block.length() == 0);
                    errno = EWOULDBLOCK;
                    return -1;
                }

                // We just received nice new header
                if (handle_input_header() == -1)
                {
                    ACE_ASSERT ((errno != EWOULDBLOCK) && (errno != EAGAIN));
                    return -1;
                }
            }
        }
        else
        {
            if (m_Header.space() > 0)
            {
                //need to receive the header
                const size_t to_header = (message_block.length() > m_Header.space() ? m_Header.space() : message_block.length());
                m_Header.copy (message_block.rd_ptr(), to_header);
                message_block.rd_ptr (to_header);

                if (m_Header.space() > 0)
                {
                    // Couldn't receive the whole header this time.
                    ACE_ASSERT (message_block.length() == 0);
                    errno = EWOULDBLOCK;
                    return -1;
                }

                // We just received nice new header
                if (handle_input_header() == -1)
                {
                    ACE_ASSERT ((errno != EWOULDBLOCK) && (errno != EAGAIN));
                    return -1;
                }
            }
        }

        // Its possible on some error situations that this happens
        // for example on closing when epoll receives more chunked data and stuff
        // hope this is not hack, as proper m_RecvWPct is asserted around
        if (!m_RecvWPct)
        {
            TC_LOG_ERROR("network", "Forcing close on input m_RecvWPct = NULL");
            errno = EINVAL;
            return -1;
        }

        // We have full read header, now check the data payload
        if (m_RecvPct.space() > 0)
        {
            //need more data in the payload
            const size_t to_data = (message_block.length() > m_RecvPct.space() ? m_RecvPct.space() : message_block.length());
            m_RecvPct.copy (message_block.rd_ptr(), to_data);
            message_block.rd_ptr (to_data);

            if (m_RecvPct.space() > 0)
            {
                // Couldn't receive the whole data this time.
                ACE_ASSERT (message_block.length() == 0);
                errno = EWOULDBLOCK;
                return -1;
            }
        }

        //just received fresh new payload
        if (handle_input_payload() == -1)
        {
            ACE_ASSERT ((errno != EWOULDBLOCK) && (errno != EAGAIN));
            return -1;
        }
    }

    return size_t(n) == recv_size ? 1 : 2;
}

int WorldSocket::cancel_wakeup_output(GuardType& g)
{
    if (!m_OutActive)
        return 0;

    m_OutActive = false;

    g.release();

    if (reactor()->cancel_wakeup
        (this, ACE_Event_Handler::WRITE_MASK) == -1)
    {
        // would be good to store errno from reactor with errno guard
        TC_LOG_ERROR("network", "WorldSocket::cancel_wakeup_output");
        return -1;
    }

    return 0;
}

int WorldSocket::schedule_wakeup_output(GuardType& g)
{
    if (m_OutActive)
        return 0;

    m_OutActive = true;

    g.release();

    if (reactor()->schedule_wakeup
        (this, ACE_Event_Handler::WRITE_MASK) == -1)
    {
        TC_LOG_ERROR("network", "WorldSocket::schedule_wakeup_output");
        return -1;
    }

    return 0;
}

int WorldSocket::ProcessIncoming(WorldPacket* new_pct)
{
    ACE_ASSERT (new_pct);

    // manage memory ;)
    ACE_Auto_Ptr<WorldPacket> aptr(new_pct);

    Opcodes opcode = new_pct->GetOpcode();

    if (closing_)
        return -1;

    // Dump received packet.
    if (sPacketLog->CanLogPacket())
        sPacketLog->LogPacket(*new_pct, CLIENT_TO_SERVER);

    if (m_Session)
        TC_LOG_TRACE("network.opcode", "C->S: %s %s", m_Session->GetPlayerInfo().c_str(), GetOpcodeNameForLogging(opcode, false).c_str());

    try
    {
        switch (opcode)
        {
            case CMSG_PING:
            {
                try
                {
                    return HandlePing(*new_pct);
                }
                catch (ByteBufferPositionException const&)
                {
                }
                TC_LOG_ERROR("network", "WorldSocket::ReadDataHandler(): client %s sent malformed CMSG_PING", m_Session->GetPlayerInfo().c_str());
                return -1;
            }
            case CMSG_AUTH_SESSION:
            {
                if (m_Session)
                {
                    TC_LOG_ERROR("network", "WorldSocket::ProcessIncoming: received duplicate CMSG_AUTH_SESSION from %s", m_Session->GetPlayerInfo().c_str());
                    return -1;
                }

                try
                {
                    sScriptMgr->OnPacketReceive(this, WorldPacket(*new_pct));
                    return HandleAuthSession(*new_pct);
                }
                catch (ByteBufferPositionException const&)
                {
                }
                TC_LOG_ERROR("network", "WorldSocket::ReadDataHandler(): client %s sent malformed CMSG_AUTH_SESSION", m_Session->GetPlayerInfo().c_str());
                return -1;
            }
            //case CMSG_KEEP_ALIVE:
            //  sessionGuard.lock();
            //  sScriptMgr->OnPacketReceive(this, WorldPacket(*new_pct));
            //  if (_worldSession)
            //  _worldSession->ResetTimeOutTime(true);
            //  return 0;
            case CMSG_LOG_DISCONNECT:
                new_pct->rfinish(); // contains uint32 disconnectReason;
                sScriptMgr->OnPacketReceive(this, WorldPacket(*new_pct));
                return 0;
            // not an opcode, client sends string "WORLD OF WARCRAFT CONNECTION - CLIENT TO SERVER" without opcode
            // first 4 bytes become the opcode (2 dropped)
            case MSG_VERIFY_CONNECTIVITY:
            {
                sScriptMgr->OnPacketReceive(this, WorldPacket(*new_pct));
                std::string str;
                *new_pct >> str;
                if (str != "D OF WARCRAFT CONNECTION - CLIENT TO SERVER")
                    return -1;
                return HandleSendAuthSession();
            }
            /*case CMSG_ENABLE_NAGLE:
            {
                TC_LOG_DEBUG("network", "%s", opcodeName.c_str());
                sScriptMgr->OnPacketReceive(this, WorldPacket(*new_pct));
                return m_Session ? m_Session->HandleEnableNagleAlgorithm() : -1;
            }*/
            default:
            {
                ACE_GUARD_RETURN(LockType, Guard, m_SessionLock, -1);
                if (!m_Session)
                {
                    TC_LOG_ERROR("network.opcode", "ProcessIncoming: Client not authed opcode = %u", uint32(opcode));
                    return -1;
                }

                // prevent invalid memory access/crash with custom opcodes
                if (opcode >= NUM_OPCODES)
                    return 0;

                OpcodeHandler const* handler = clientOpcodeTable[opcode];
                if (!handler || handler->Status == STATUS_UNHANDLED)
                {
                    TC_LOG_ERROR("network.opcode", "No defined handler for opcode %s sent by %s",
                        GetOpcodeNameForLogging(new_pct->GetOpcode(), false, new_pct->GetReceivedOpcode()).c_str(), m_Session->GetPlayerInfo().c_str());
                    return 0;
                }

                // Our Idle timer will reset on any non PING opcodes on login screen, allowing us to catch people idling.
                m_Session->ResetTimeOutTime(false);

                // OK, give the packet to WorldSession
                aptr.release();
                // WARNING here we call it with locks held.
                // Its possible to cause deadlock if QueuePacket calls back
                m_Session->QueuePacket(new_pct);
                return 0;
            }
        }
    }
    catch (ByteBufferException &)
    {
        //char message[2056];
        //sprintf(message, "ByteBufferException occured while parsing a packet %s", opcodeName.c_str());
        TC_LOG_ERROR("network", "WorldSocket::ProcessIncoming ByteBufferException occured while parsing an instant handled packet %s from client %s, accountid=%i. Disconnected client.",
                       GetOpcodeNameForLogging(new_pct->GetOpcode(), false, new_pct->GetReceivedOpcode()).c_str(), GetRemoteAddress().c_str(), m_Session ? int32(m_Session->GetAccountId()) : -1);
        //if (Player* player = m_Session->GetPlayer())
                //player->TextEmote(message);
        new_pct->hexlike();
        return -1;
    }
    ACE_NOTREACHED (return 0);
}

int WorldSocket::HandleSendAuthSession()
{
    BigNumber seed1;
    BigNumber seed2;

    seed1.SetRand(16 * 8);
    seed2.SetRand(16 * 8);

    uint32 DosZeroBits = 1;

    uint32 DosChallenge[8];
    memcpy(&DosChallenge[0], seed1.AsByteArray(16).get(), 16);
    memcpy(&DosChallenge[4], seed2.AsByteArray(16).get(), 16);

    WorldPacket packet(SMSG_AUTH_CHALLENGE, 8 * 4 + 1 + 4 + 2);

    packet << uint16(0); // WTF is that?!

    packet.append(DosChallenge, 8);

    packet << uint8(DosZeroBits);
    packet << uint32(m_Seed);

    return SendPacket(packet);
}

int WorldSocket::HandleAuthSession(WorldPacket& recvData)
{
    SHA1Hash Sha;
    BigNumber K;
    WorldPacket AddonsData;

    uint8 Digest[SHA_DIGEST_LENGTH];
    memset(Digest, 0, SHA_DIGEST_LENGTH);

    uint64 DosResponse = 0;

    uint32 ClientSeed = 0;
    uint32 ID = 0;
    uint32 AddonSize = 0;
    uint32 RegionID = 0;
    uint32 RealmIndex = 0;
    uint32 BattlegroupID = 0;

    int32 LoginServerID = -1;

    uint16 ClientBuild = 0;

    uint8 Security = 0;
    uint8 LoginServerType = 0;  // Auth type used - 0 GRUNT, 1 battle.net
    uint8 BuildType = 0;

    LocaleConstant Locale = LOCALE_enUS;
    std::string Account;

    bool UseIPv6 = false;

    recvData >> LoginServerID;
    recvData >> RegionID;

    recvData >> Digest[18];
    recvData >> Digest[14];
    recvData >> Digest[3];
    recvData >> Digest[4];
    recvData >> Digest[0];

    recvData >> RealmIndex;

    recvData >> Digest[11];

    recvData >> ClientSeed;

    recvData >> Digest[19];

    recvData >> LoginServerType;
    recvData >> BuildType;

    recvData >> Digest[2];
    recvData >> Digest[9];
    recvData >> Digest[12];

    recvData >> DosResponse;
    recvData >> BattlegroupID;

    recvData >> Digest[16];
    recvData >> Digest[5];
    recvData >> Digest[6];
    recvData >> Digest[8];

    recvData >> ClientBuild;

    recvData >> Digest[17];
    recvData >> Digest[7];
    recvData >> Digest[13];
    recvData >> Digest[15];
    recvData >> Digest[1];
    recvData >> Digest[10];

    recvData >> AddonSize;

    if (AddonSize)
    {
        AddonsData.resize(AddonSize);
        recvData.read((uint8*)AddonsData.contents(), AddonSize);
    }

    UseIPv6 = recvData.ReadBit();

    uint32 accountNameLength = recvData.ReadBits(11);

    recvData.FlushBits();

    Account = recvData.ReadString(accountNameLength);

    // Get the account information from the realmd database
    //         0           1        2       3          4         5       6  7
    // SELECT id, sessionkey, last_ip, locked, expansion, mutetime, locale, os FROM account WHERE username = ?
    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_INFO_BY_NAME);

    stmt->setString(0, Account);

    PreparedQueryResult result = LoginDatabase.Query(stmt);

    // Stop if the account is not found
    if (!result)
    {
        // We can not log here, as we do not know the account. Thus, no accountId.
        SendAuthResponseError(AUTH_UNKNOWN_ACCOUNT);
        TC_LOG_ERROR("network", "WorldSocket::HandleAuthSession: Sent Auth Response (unknown account).");
        return -1;
    }

    Field* fields = result->Fetch();

    uint8 expansion = fields[4].GetUInt8();
    uint32 world_expansion = sWorld->getIntConfig(CONFIG_EXPANSION);
    if (expansion > world_expansion)
        expansion = world_expansion;

    // For hook purposes, we get Remoteaddress at this point.
    std::string address = GetRemoteAddress();

    // As we don't know if attempted login process by ip works, we update last_attempt_ip right away
    stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_LAST_ATTEMPT_IP);
    stmt->setString(0, address);
    stmt->setString(1, Account);
    LoginDatabase.Execute(stmt);
    // This also allows to check for possible "hack" attempts on account

    // id has to be fetched at this point, so that first actual account response that fails can be logged
    ID = fields[0].GetUInt32();

    K.SetHexStr(fields[1].GetCString());

    // even if auth credentials are bad, try using the session key we have - client cannot read auth response error without it
    m_Crypt.Init(&K);

    // First reject the connection if packet contains invalid data or realm state doesn't allow logging in
    if (sWorld->IsClosed())
    {
        SendAuthResponseError(AUTH_REJECT);
        TC_LOG_ERROR("network", "WorldSocket::HandleAuthSession: World closed, denying client (%s).", address.c_str());
        return -1;
    }

    if (RealmIndex != realmID)
    {
        SendAuthResponseError(REALM_LIST_REALM_NOT_FOUND);
        TC_LOG_ERROR("network", "WorldSocket::HandleAuthSession: Sent Auth Response (bad realm).");
        return -1;
    }

    std::string os = fields[7].GetString();

    // Must be done before WorldSession is created
    if (sWorld->getBoolConfig(CONFIG_WARDEN_ENABLED) && os != "Win" && os != "OSX")
    {
        SendAuthResponseError(AUTH_REJECT);
        TC_LOG_ERROR("network", "WorldSocket::HandleAuthSession: Client %s attempted to log in using invalid client OS (%s).", address.c_str(), os.c_str());
        return -1;
    }

    // Check that Key and account name are the same on client and server
    uint32 t = 0;

    Sha.UpdateData(Account);
    Sha.UpdateData((uint8*)&t, 4);
    Sha.UpdateData((uint8*)&ClientSeed, 4);
    Sha.UpdateData((uint8*)&m_Seed, 4);
    Sha.UpdateBigNumbers(&K, NULL);
    Sha.Finalize();

    if (memcmp(Sha.GetDigest(), Digest, SHA_DIGEST_LENGTH) != 0)
    {
        SendAuthResponseError(AUTH_FAILED);
        TC_LOG_ERROR("network", "WorldSocket::HandleAuthSession: Authentication failed for account: %u ('%s') address: %s", ID, Account.c_str(), address.c_str());
        return -1;
    }

    ///- Re-check ip locking (same check as in realmd).
    if (fields[3].GetUInt8() == 1) // if ip is locked
    {
        if (strcmp(fields[2].GetCString(), address.c_str()) != 0)
        {
            SendAuthResponseError(AUTH_FAILED);
            TC_LOG_DEBUG("network", "WorldSocket::HandleAuthSession: Sent Auth Response (Account IP differs. Original IP: %s, new IP: %s).", fields[2].GetCString(), address.c_str());
            // We could log on hook only instead of an additional db log, however action logger is config based. Better keep DB logging as well
            sScriptMgr->OnFailedAccountLogin(ID);
            return -1;
        }
    }

    int64 mutetime = fields[5].GetInt64();
    //! Negative mutetime indicates amount of seconds to be muted effective on next login - which is now.
    if (mutetime < 0)
    {
        mutetime = time(NULL) + llabs(mutetime);

        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_MUTE_TIME_LOGIN);

        stmt->setInt64(0, mutetime);
        stmt->setUInt32(1, ID);

        LoginDatabase.Execute(stmt);
    }

    Locale = LocaleConstant(fields[6].GetUInt8());
    if (Locale >= TOTAL_LOCALES)
        Locale = LOCALE_enUS;

    // Checks gmlevel per Realm
    stmt = LoginDatabase.GetPreparedStatement(LOGIN_GET_GMLEVEL_BY_REALMID);

    stmt->setUInt32(0, ID);
    stmt->setInt32(1, int32(realmID));

    result = LoginDatabase.Query(stmt);

    if (!result)
        Security = 0;
    else
    {
        fields = result->Fetch();
        Security = fields[0].GetUInt8();
    }

    // Re-check account ban (same check as in realmd)
    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_BANS);

    stmt->setUInt32(0, ID);
    stmt->setString(1, address);

    PreparedQueryResult banresult = LoginDatabase.Query(stmt);

    if (banresult) // if account banned
    {
        SendAuthResponseError(AUTH_BANNED);
        TC_LOG_ERROR("network", "WorldSocket::HandleAuthSession: Sent Auth Response (Account banned).");
        sScriptMgr->OnFailedAccountLogin(ID);
        return -1;
    }

    // Check locked state for server
    AccountTypes allowedAccountType = sWorld->GetPlayerSecurityLimit();
    TC_LOG_DEBUG("network", "Allowed Level: %u Player Level %u", allowedAccountType, AccountTypes(Security));
    if (allowedAccountType > SEC_PLAYER && AccountTypes(Security) < allowedAccountType)
    {
        SendAuthResponseError(AUTH_UNAVAILABLE);
        TC_LOG_INFO("network", "WorldSocket::HandleAuthSession: User tries to login but his security level is not enough");
        sScriptMgr->OnFailedAccountLogin(ID);
        return -1;
    }

    TC_LOG_DEBUG("network", "WorldSocket::HandleAuthSession: Client '%s' authenticated successfully from %s.",
        Account.c_str(),
        address.c_str());

    // Update the last_ip in the database as it was successful for login
    stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_LAST_IP);

    stmt->setString(0, address);
    stmt->setString(1, Account);

    LoginDatabase.Execute(stmt);

    // NOTE ATM the socket is single-threaded, have this in mind ...
    ACE_NEW_RETURN(m_Session, WorldSession(ID, this, AccountTypes(Security), expansion, mutetime, Locale), -1);

    m_Session->LoadGlobalAccountData();
    m_Session->LoadTutorialsData();
    m_Session->ReadAddonsInfo(AddonsData);
    m_Session->LoadPermissions();

    // At this point, we can safely hook a successful login
    sScriptMgr->OnAccountLogin(ID);

    // Initialize Warden system only if it is enabled by config
    if (sWorld->getBoolConfig(CONFIG_WARDEN_ENABLED))
        m_Session->InitWarden(&K, os);

    // Initialize BattlePayShop
    if (sWorld->getBoolConfig(CONFIG_BATTLE_PAY_SHOP_ENABLED))
        m_Session->InitBattlePayShop(ID, Account);

    // Initialize RaF System
    if (sWorld->getBoolConfig(CONFIG_REFER_A_FRIEND_ENABLED))
        m_Session->InitReferAFriendSystem(ID);

    // Sleep this Network thread for
    uint32 sleepTime = sWorld->getIntConfig(CONFIG_SESSION_ADD_DELAY);
    ACE_OS::sleep(ACE_Time_Value(0, sleepTime));

    sWorld->AddSession(m_Session);
    return 0;
}

int WorldSocket::HandlePing(WorldPacket& recvData)
{
    uint32 ping;
    uint32 latency;

    // Get the ping packet content
    recvData >> latency;
    recvData >> ping;

    if (m_LastPingTime == ACE_Time_Value::zero)
        m_LastPingTime = ACE_OS::gettimeofday(); // for 1st ping
    else
    {
        ACE_Time_Value cur_time = ACE_OS::gettimeofday();
        ACE_Time_Value diff_time (cur_time);
        diff_time -= m_LastPingTime;
        m_LastPingTime = cur_time;

        if (diff_time < ACE_Time_Value (27))
        {
            ++m_OverSpeedPings;

            uint32 max_count = sWorld->getIntConfig (CONFIG_MAX_OVERSPEED_PINGS);

            if (max_count && m_OverSpeedPings > max_count)
            {
                ACE_GUARD_RETURN (LockType, Guard, m_SessionLock, -1);

                if (m_Session && !m_Session->HasPermission(rbac::RBAC_PERM_SKIP_CHECK_OVERSPEED_PING))
                {
                    TC_LOG_ERROR("network", "WorldSocket::HandlePing: %s kicked for over-speed pings (address: %s)",
                        m_Session->GetPlayerInfo().c_str(), GetRemoteAddress().c_str());

                    return -1;
                }
            }
        }
        else
            m_OverSpeedPings = 0;
    }

    // critical section
    {
        ACE_GUARD_RETURN (LockType, Guard, m_SessionLock, -1);

        if (m_Session)
        {
            m_Session->SetLatency (latency);
            m_Session->ResetClientTimeDelay();
        }
        else
        {
            TC_LOG_ERROR("network", "WorldSocket::HandlePing: peer sent CMSG_PING, "
                            "but is not authenticated or got recently kicked, "
                            " address = %s",
                            GetRemoteAddress().c_str());
             return -1;
        }
    }

    WorldPacket packet(SMSG_PONG, 4);

    packet << uint32(ping);

    return SendPacket(packet);
}

void WorldSocket::SendAuthResponseError(uint8 code)
{
    bool IsSuccess = false;
    bool IsQueued = false;

    WorldPacket data(SMSG_AUTH_RESPONSE, 1 + 1);

    data.WriteBit(IsSuccess);
    data.WriteBit(IsQueued);

    data.FlushBits();

    data << uint8(code);

    SendPacket(data);
}
