/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_AUCTION_HOUSE_MGR_H
#define SF_AUCTION_HOUSE_MGR_H

#include "Common.h"
#include "DatabaseEnv.h"
#include "DBCStructure.h"
#include "Singleton/Singleton.hpp"
#include "ObjectGuid.h"
#include <set>

class Item;
class Player;
class WorldPacket;

#define MIN_AUCTION_TIME    (12*HOUR)
#define MAX_AUCTION_ITEMS    32

enum AuctionError
{
    ERR_AUCTION_OK                  = 0,
    ERR_AUCTION_INVENTORY           = 1,
    ERR_AUCTION_DATABASE_ERROR      = 2,
    ERR_AUCTION_NOT_ENOUGHT_MONEY   = 3,
    ERR_AUCTION_ITEM_NOT_FOUND      = 4,
    ERR_AUCTION_HIGHER_BID          = 5,
    ERR_AUCTION_BID_INCREMENT       = 7,
    ERR_AUCTION_BID_OWN             = 10,
    ERR_AUCTION_RESTRICTED_ACCOUNT  = 13
};

enum AuctionAction
{
    AUCTION_SELL_ITEM               = 0,
    AUCTION_CANCEL                  = 1,
    AUCTION_PLACE_BID               = 2
};

enum MailAuctionAnswers
{
    AUCTION_OUTBIDDED               = 0,
    AUCTION_WON                     = 1,
    AUCTION_SUCCESSFUL              = 2,
    AUCTION_EXPIRED                 = 3,
    AUCTION_CANCELLED_TO_BIDDER     = 4,
    AUCTION_CANCELED                = 5,
    AUCTION_SALE_PENDING            = 6
};

enum AuctionHouses
{
    AUCTIONHOUSE_ALLIANCE       = 2,
    AUCTIONHOUSE_HORDE          = 6,
    AUCTIONHOUSE_NEUTRAL        = 7
};

struct AuctionEntry
{
    uint32 Id;
    uint8 houseId;
    uint32 auctioneerEntry;
    ObjectGuid item;
    uint32 itemEntry;
    uint32 itemCount;
    ObjectGuid owner;
    uint64 startbid;
    uint64 bid;
    uint64 buyout;
    time_t expire_time;
    ObjectGuid bidder;
    uint32 etime;
    uint64 deposit;
    AuctionHouseEntry const* auctionHouseEntry;             // in AuctionHouse.dbc

    // helpers
    uint8 GetHouseId() const { return houseId; }
    uint64 GetAuctionCut() const;
    uint64 GetAuctionOutBid() const;
    bool BuildAuctionInfo(ByteBuffer & data, Item* sourceItem = nullptr) const;
    void DeleteFromDB(SQLTransaction& trans) const;
    void SaveToDB(SQLTransaction& trans) const;
    bool LoadFromDB(Field* fields);
    bool LoadFromFieldList(Field* fields);
    std::string BuildAuctionMailSubject(MailAuctionAnswers response) const;
    static std::string BuildAuctionMailBody(ObjectGuid guid, uint64 bid, uint64 buyout, uint64 deposit, uint64 cut);
};

//this class is used as auctionhouse instance
class AuctionHouseObject
{
public:
    ~AuctionHouseObject()
    {
        for (AuctionEntryMap::iterator itr = AuctionsMap.begin(); itr != AuctionsMap.end(); ++itr)
            delete itr->second;
    }

    typedef std::map<uint32, AuctionEntry*> AuctionEntryMap;

    struct PlayerGetAllThrottleData
    {
        uint32 Global;
        uint32 Cursor;
        uint32 Tombstone;
        time_t NextAllowedReplication;

        bool IsReplicationInProgress() const { return Cursor != Tombstone && Global != 0; }
    };

    typedef std::unordered_map<ObjectGuid, PlayerGetAllThrottleData> PlayerGetAllThrottleMap;

    uint32 GetCount() const { return AuctionsMap.size(); }

    AuctionEntryMap::iterator GetAuctionsBegin() { return AuctionsMap.begin(); }
    AuctionEntryMap::iterator GetAuctionsEnd() { return AuctionsMap.end(); }

    AuctionEntry* GetAuction(uint32 id) const
    {
        AuctionEntryMap::const_iterator itr = AuctionsMap.find(id);
        return itr != AuctionsMap.end() ? itr->second : NULL;
    }

    void AddAuction(AuctionEntry* auction);
    bool RemoveAuction(AuctionEntry* auction);

    void Update();

    void BuildListBidderItems(ByteBuffer& data, Player* player, uint32& count, uint32& totalcount);
    void BuildListOwnerItems(ByteBuffer& data, Player* player, uint32& count, uint32& totalcount);
    void BuildListAuctionItems(ByteBuffer& data, Player* player,
    std::wstring const& searchedname, uint32 listfrom, uint8 levelmin, uint8 levelmax, uint8 usable,
    uint32 inventoryType, uint32 itemClass, uint32 itemSubClass, uint32 quality,
    uint32& count, uint32& totalcount);
    void BuildReplicate(ByteBuffer& data, Player* player, uint32& global, uint32& cursor, uint32& tombstone, uint32 count);

private:
    AuctionEntryMap AuctionsMap;

    // Map of throttled players for GetAll, and throttle expiry time
    // Stored here, rather than player object to maintain persistence after logout
    PlayerGetAllThrottleMap GetAllThrottleMap;
};

class AuctionHouseMgr
{
    friend class Tod::Singleton<AuctionHouseMgr>;

    AuctionHouseMgr();
    ~AuctionHouseMgr();

    public:

        typedef std::unordered_map<ObjectGuid, Item*> ItemMap;
        typedef std::vector<AuctionEntry*> PlayerAuctions;
        typedef std::pair<PlayerAuctions*, uint32> AuctionPair;

        AuctionHouseObject* GetAuctionsMap(uint32 factionTemplateId);
        AuctionHouseObject* GetAuctionsMapByHouseId(uint8 auctionHouseId);

        Item* GetAItem(ObjectGuid guid)
        {
            ItemMap::const_iterator itr = mAitems.find(guid);
            if (itr != mAitems.end())
                return itr->second;

            return NULL;
        }

        //auction messages
        void SendAuctionWonMail(AuctionEntry* auction, SQLTransaction& trans);
        void SendAuctionSalePendingMail(AuctionEntry* auction, SQLTransaction& trans);
        void SendAuctionSuccessfulMail(AuctionEntry* auction, SQLTransaction& trans);
        void SendAuctionExpiredMail(AuctionEntry* auction, SQLTransaction& trans);
        void SendAuctionOutbiddedMail(AuctionEntry* auction, uint64 newPrice, Player* newBidder, SQLTransaction& trans);
        void SendAuctionCancelledToBidderMail(AuctionEntry* auction, SQLTransaction& trans, Item* item);

        static uint64 GetAuctionDeposit(AuctionHouseEntry const* entry, uint32 time, Item* pItem, uint32 count);
        static AuctionHouseEntry const* GetAuctionHouseEntry(uint32 factionTemplateId);
        static AuctionHouseEntry const* GetAuctionHouseEntryFromHouse(uint8 houseId);

    public:

        // Used primarily at server start to avoid loading a list of expired auctions
        void DeleteExpiredAuctionsAtStartup();

        //load first auction items, because of check if item exists, when loading
        void LoadAuctionItems();
        void LoadAuctions();

        void AddAItem(Item* it);
        bool RemoveAItem(ObjectGuid guid, bool deleteItem = false, SQLTransaction* trans = nullptr);
        bool PendingAuctionAdd(Player* player, AuctionEntry* aEntry, Item* item);
        uint32 PendingAuctionCount(const Player* player) const;
        void PendingAuctionProcess(Player* player);
        void UpdatePendingAuctions();
        void Update();

    private:

        AuctionHouseObject mHordeAuctions;
        AuctionHouseObject mAllianceAuctions;
        AuctionHouseObject mNeutralAuctions;

        std::map<ObjectGuid, AuctionPair> pendingAuctionMap;

        ItemMap mAitems;
};

#define sAuctionMgr Tod::Singleton<AuctionHouseMgr>::GetSingleton()
#endif
