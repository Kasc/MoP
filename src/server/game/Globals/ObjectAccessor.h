/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_OBJECTACCESSOR_H
#define TRINITY_OBJECTACCESSOR_H

#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>

#include "Define.h"
#include <unordered_map>

#include "UpdateData.h"

#include "GridDefines.h"
#include "Object.h"

#include <set>

class Creature;
class Corpse;
class Unit;
class GameObject;
class DynamicObject;
class WorldObject;
class Vehicle;
class Map;
class WorldRunnable;
class Transport;

template <class T>
class HashMapHolder
{
    //Non instanceable only static
    HashMapHolder() { }

public:
    static_assert(std::is_same<Player, T>::value
        || std::is_same<MotionTransport, T>::value,
        "Only Player and MotionTransport can be registered in global HashMapHolder");

    typedef std::unordered_map<ObjectGuid, T*> MapType;

    static void Insert(T* o);

    static void Remove(T* o);

    static T* Find(ObjectGuid guid);

    static MapType& GetContainer();

    static boost::shared_mutex* GetLock();
};


namespace ObjectAccessor
{
    // these functions return objects only if in map of specified object
    WorldObject* GetWorldObject(WorldObject const&, ObjectGuid const&);
    Object* GetObjectByTypeMask(WorldObject const&, ObjectGuid const&, uint32 typemask);
    Corpse* GetCorpse(WorldObject const& u, ObjectGuid const& guid);
    GameObject* GetGameObject(WorldObject const& u, ObjectGuid const& guid);
    Transport* GetTransportOnMap(WorldObject const& u, ObjectGuid const& guid);
    MotionTransport* GetMotionTransportOnMap(WorldObject const& u, ObjectGuid const& guid);
    MotionTransport* GetMotionTransport(ObjectGuid const& guid);
    DynamicObject* GetDynamicObject(WorldObject const& u, ObjectGuid const& guid);
    AreaTrigger* GetAreaTrigger(WorldObject const& u, ObjectGuid const& guid);
    Unit* GetUnit(WorldObject const&, ObjectGuid const& guid);
    Creature* GetCreature(WorldObject const& u, ObjectGuid const& guid);
    Pet* GetPet(WorldObject const&, ObjectGuid const& guid);
    Player* GetPlayer(Map const*, ObjectGuid const& guid);
    Player* GetPlayer(WorldObject const&, ObjectGuid const& guid);
    Creature* GetCreatureOrPetOrVehicle(WorldObject const&, ObjectGuid const&);

    // these functions return objects if found in whole world
    // ACCESS LIKE THAT IS NOT THREAD SAFE
    Player* FindPlayer(ObjectGuid const&);
    Player* FindPlayerByName(std::string const& name);
    Player* FindPlayerByLowGUID(uint32 lowguid);

    // this returns Player even if he is not in world, for example teleporting
    Player* FindConnectedPlayer(ObjectGuid const&);
    Player* FindConnectedPlayerByName(std::string const& name);

    // when using this, you must use the hashmapholder's lock
    HashMapHolder<Player>::MapType const& GetPlayers();

    template<class T>
    void AddObject(T* object)
    {
        HashMapHolder<T>::Insert(object);
    }

    template<class T>
    void RemoveObject(T* object)
    {
        HashMapHolder<T>::Remove(object);
    }

    template<>
    void AddObject(Player* player);

    template<>
    void RemoveObject(Player* player);

    void SaveAllPlayers();
};

#endif
