/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "MovementPacketBuilder.h"
#include "MoveSpline.h"
#include "WorldPacket.h"
#include "Object.h"
#include "Unit.h"
#include "Transport.h"

MonsterMoveType PacketBuilder::GetMonsterMoveType(MoveSpline const& moveSpline)
{
    switch (moveSpline.splineflags & MoveSplineFlag::Mask_Final_Facing)
    {
        case MoveSplineFlag::Final_Target:
            return MonsterMoveFacingTarget;
        case MoveSplineFlag::Final_Angle:
            return MonsterMoveFacingAngle;
        case MoveSplineFlag::Final_Point:
            return MonsterMoveFacingPoint;
        default:
            return MonsterMoveNormal;
    }
}

void PacketBuilder::WriteStopMovement(Vector3 const& pos, uint32 splineId, ByteBuffer& data, Unit* unit)
{
    ObjectGuid guid = unit->GetGUID();
    ObjectGuid transport = unit->GetTransportGUID();

    Vector3 transportPos;
    if (Transport* transport = unit->GetTransport())
    {
        transportPos = pos;
        transport->CalculatePassengerOffset(transportPos.x, transportPos.y, transportPos.z, nullptr);
    }

    int8 SeatdId = unit->GetTransportSeatID();
    bool ShowSeatId = SeatdId != -1;

    bool HasParabolicSpeed = false;
    bool HasParabolic = false;
    bool HasAnimation = false;

    uint8 VehicleExitVoluntaryId = 0;
    bool HasVehicleExitVoluntaryId = VehicleExitVoluntaryId != 0;

    uint8 Mode = SplineBase::EvaluationMode::ModesEnd;
    bool HasMode = Mode != SplineBase::EvaluationMode::ModesEnd;

    uint32 MonsterSplineFilterKey = 0;
    bool HasMonsterSplineFilter = MonsterSplineFilterKey != 0;

    bool CrzTeleport = false;

    data << float(pos.z);
    data << float(pos.x);

    data << uint32(splineId);

    data << float(pos.y);
    data << float(transportPos.y);
    data << float(transportPos.z);
    data << float(transportPos.x);

    data.WriteBit(!HasParabolicSpeed);

    data.WriteGuidMask(guid, 0);

    data.WriteBits(MonsterMoveStop, 3);

    data.WriteBit(true); // HasTimePassed
    data.WriteBit(!HasAnimation);
    data.WriteBit(!ShowSeatId);

    data.WriteBits(0, 20);

    data.WriteBit(true); // HasFlags

    data.WriteGuidMask(guid, 3);

    data.WriteBit(!HasVehicleExitVoluntaryId);
    data.WriteBit(!HasParabolic);
    data.WriteBit(!HasMode);
    data.WriteBit(true); // HasDuration

    data.WriteGuidMask(guid, 7, 4);

    data.WriteBit(!HasAnimation);

    data.WriteGuidMask(guid, 5);

    data.WriteBits(0, 22);

    data.WriteGuidMask(guid, 6);

    data.WriteBit(transport.IsEmpty());

    data.WriteGuidMask(transport, 7, 1, 3, 0, 6, 4, 5, 2);

    data.WriteBit(HasMonsterSplineFilter);

    data.WriteBit(CrzTeleport);

    data.WriteGuidMask(guid, 2, 1);

    data.FlushBits();

    data.WriteGuidBytes(guid, 1);

    data.WriteGuidBytes(transport, 6, 4, 1, 7, 0, 3, 5, 2);

    data.WriteGuidBytes(guid, 5, 3, 6, 0);

    if (ShowSeatId)
        data << int8(SeatdId);

    data.WriteGuidBytes(guid, 7, 2, 4);
}

void PacketBuilder::WriteMonsterMove(const MoveSpline& moveSpline, WorldPacket& data, Unit* unit)
{
    ObjectGuid guid = unit->GetGUID();
    ObjectGuid transport = unit->GetTransportGUID();
    MonsterMoveType type = GetMonsterMoveType(moveSpline);

    HoverPathTransform transform(unit);

    Vector3 const& firstPoint = transform(moveSpline.spline.getPoint(moveSpline.spline.first()));

    Vector3 transportPos;
    if (Transport* transport = unit->GetTransport())
    {
        transportPos = firstPoint;
        transport->CalculatePassengerOffset(transportPos.x, transportPos.y, transportPos.z, nullptr);
    }

    int8 SeatdId = unit->GetTransportSeatID();
    bool ShowSeatId = SeatdId != -1;

    bool HasParabolicSpeed = moveSpline.splineflags.parabolic && moveSpline.effect_start_time < moveSpline.Duration();
    bool HasParabolic = moveSpline.splineflags.parabolic;
    bool HasAnimation = moveSpline.splineflags.animation;

    uint8 VehicleExitVoluntaryId = 0;
    bool HasVehicleExitVoluntaryId = VehicleExitVoluntaryId != 0;

    uint8 Mode = moveSpline.spline.mode();
    bool HasMode = Mode != SplineBase::EvaluationMode::ModesEnd;

    uint32 MonsterSplineFilterKey = 0;
    bool HasMonsterSplineFilter = false;

    bool CrzTeleport = false;

    std::vector<Vector3> const& array = moveSpline.spline.getPoints();

    std::vector<Vector3> splinePoints;
    for (uint32 i = 0; i < array.size(); ++i)
    {
        Vector3 point = array[i];
        splinePoints.push_back(transform(point));
    }

    std::vector<Vector3> Points;
    std::vector<Vector3> PackedDeltas;
    if (moveSpline.splineflags & ::MoveSplineFlag::UncompressedPath)
    {
        if (!moveSpline.splineflags.cyclic)
        {
            uint32 count = splinePoints.size() - 3;
            for (uint32 i = 0; i < count; ++i)
                Points.push_back(splinePoints[i + 2]);
        }
        else
        {
            uint32 count = splinePoints.size() - 3;

            Points.push_back(splinePoints[1]);

            for (uint32 i = 0; i < count; ++i)
                Points.push_back(splinePoints[i + 1]);
        }
    }
    else
    {
        uint32 lastIdx = splinePoints.size() - 3;
        Vector3 const* realPath = &splinePoints[1];

        Points.push_back(realPath[lastIdx]);

        if (lastIdx > 1)
        {
            Vector3 middle = (realPath[0] + realPath[lastIdx]) / 2.f;

            // first and last points already appended
            for (uint32 i = 1; i < lastIdx; ++i)
                PackedDeltas.push_back(middle - realPath[i]);
        }
    }

    data << float(firstPoint.z);
    data << float(firstPoint.x);

    data << uint32(moveSpline.GetId());

    data << float(firstPoint.y);
    data << float(transportPos.y);
    data << float(transportPos.z);
    data << float(transportPos.x);

    data.WriteBit(!HasParabolicSpeed);

    data.WriteGuidMask(guid, 0);

    data.WriteBits(type, 3);

    if (type == MonsterMoveFacingTarget)
    {
        ObjectGuid targetGuid = moveSpline.facing.target;
        data.WriteGuidMask(targetGuid, 6, 4, 3, 0, 5, 7, 1, 2);
    }

    data.WriteBit(!moveSpline.timePassed());
    data.WriteBit(!HasAnimation);
    data.WriteBit(!ShowSeatId);

    data.WriteBits(Points.size(),  20);

    data.WriteBit(!moveSpline.splineflags.raw());

    data.WriteGuidMask(guid, 3);

    data.WriteBit(!HasVehicleExitVoluntaryId);
    data.WriteBit(!HasParabolic);
    data.WriteBit(!HasMode);
    data.WriteBit(!moveSpline.Duration());

    data.WriteGuidMask(guid, 7, 4);

    data.WriteBit(!HasAnimation);

    data.WriteGuidMask(guid, 5);

    data.WriteBits(PackedDeltas.size(), 22);

    data.WriteGuidMask(guid, 6);

    data.WriteBit(transport.IsEmpty());

    data.WriteGuidMask(transport, 7, 1, 3, 0, 6, 4, 5, 2);

    data.WriteBit(HasMonsterSplineFilter);

    if (HasMonsterSplineFilter)
    {
        uint32 FilterFlags = 0;

        data.WriteBits(FilterFlags, 2);
        data.WriteBits(MonsterSplineFilterKey, 22);
    }

    data.WriteBit(CrzTeleport);

    data.WriteGuidMask(guid, 2, 1);

    data.FlushBits();

    for (Vector3 const& pos : PackedDeltas)
        data.appendPackXYZ(pos.x, pos.y, pos.z);

    data.WriteGuidBytes(guid, 1);

    data.WriteGuidBytes(transport, 6, 4, 1, 7, 0, 3, 5, 2);

    for (Vector3 const& pos : Points)
        data << pos.y << pos.x << pos.z;

    if (HasAnimation)
        data << int32(moveSpline.effect_start_time);

    if (type == MonsterMoveFacingTarget)
    {
        ObjectGuid targetGuid = moveSpline.facing.target;
        data.WriteGuidBytes(targetGuid, 5, 7, 0, 4, 3, 2, 6, 1);
    }

    data.WriteGuidBytes(guid, 5);

    if (HasParabolicSpeed)
        data << float(moveSpline.vertical_acceleration);

    if (HasMonsterSplineFilter)
    {
        for (uint32 i = 0; i < MonsterSplineFilterKey; i++)
        {
            uint16 MonsterSplineFilterId = 0;
            uint16 MonsterSplineFilterSpeed = 0;

            data << uint16(MonsterSplineFilterId);
            data << uint16(MonsterSplineFilterSpeed);
        }

        float BaseSpeed = 1.0f;
        float DistToPrevFilterKey = 1.0f;

        uint16 StartOffset = 0;
        uint16 AddedToStart = 0;

        data << float(DistToPrevFilterKey);
        data << uint16(StartOffset);
        data << uint16(AddedToStart);
        data << float(DistToPrevFilterKey);
    }

    if (moveSpline.timePassed())
        data << int32(moveSpline.timePassed());

    if (type == MonsterMoveFacingAngle)
        data << float(moveSpline.facing.angle);

    data.WriteGuidBytes(guid, 3);

    if (moveSpline.splineflags.raw())
        data << uint32(moveSpline.splineflags.raw());

    if (HasAnimation)
        data << uint8(moveSpline.splineflags.getAnimationId());

    data.WriteGuidBytes(guid, 6);

    if (HasMode)
        data << uint8(Mode);

    if (type == MonsterMoveFacingPoint)
    {
        Vector3 facing = transform(Vector3(moveSpline.facing.f.x, moveSpline.facing.f.y, moveSpline.facing.f.z));
        data << facing.x << facing.y << facing.z;
    }

    data.WriteGuidBytes(guid, 0);

    if (ShowSeatId)
        data << int8(SeatdId);

    if (HasVehicleExitVoluntaryId)
        data << uint8(VehicleExitVoluntaryId);

    data.WriteGuidBytes(guid, 7, 2);

    if (HasParabolic)
        data << int32(moveSpline.effect_start_time);

    data.WriteGuidBytes(guid, 4);

    if (moveSpline.Duration())
        data << int32(moveSpline.Duration());
}

void PacketBuilder::WriteCreateBits(MoveSpline const& moveSpline, ByteBuffer& data)
{
    bool hasSplineMove = data.WriteBit(!moveSpline.Finalized() && !moveSpline.splineIsFacingOnly);

    if (!hasSplineMove)
        return;

    data.WriteBit(moveSpline.splineflags & (MoveSplineFlag::Parabolic | MoveSplineFlag::Animation));
    data.WriteBit((moveSpline.splineflags & MoveSplineFlag::Parabolic) && moveSpline.effect_start_time < moveSpline.Duration());
    data.WriteBit(0); // HasSplineFilterKey
    data.WriteBits(moveSpline.getPath().size(), 20);
    data.WriteBits(moveSpline.spline.mode(), 2);
    data.WriteBits(moveSpline.splineflags.raw(), 25);

    /*
    if (HasSplineFilterKey)
    {
        data.WriteBits(FilterKeysCount, 21);
        data.WriteBits(FilterFlags, 2);
    }
    */
}

void PacketBuilder::WriteCreateData(Unit const* unit, MoveSpline const& moveSpline, ByteBuffer& data)
{
    HoverPathTransform transform(unit);

    if (!moveSpline.Finalized() && !moveSpline.splineIsFacingOnly)
    {
        MoveSplineFlag const& splineFlags = moveSpline.splineflags;
        MonsterMoveType type = GetMonsterMoveType(moveSpline);

        data << moveSpline.timePassed();
        data << float(1.f);                             // splineInfo.duration_mod_next; added in 3.1

        /*
        if (HasSplineFilterKey)
        {
            for (uint8 i = 0; i < FilterKeysCount; ++i)
            {
                data << float(In);
                data << float(Out);
            }
        }
        */

        data << float(1.f);                             // splineInfo.duration_mod; added in 3.1

        uint32 nodes = moveSpline.getPath().size();
        Vector3 const* realPath = &moveSpline.getPath()[0];

        for (uint32 i = 0; i < nodes; ++i)
        {
            Vector3 point = realPath[i];
            Vector3 transformed = transform(point);

            data << float(transformed.x);
            data << float(transformed.z);
            data << float(transformed.y);
        }

        if (splineFlags & (MoveSplineFlag::Parabolic | MoveSplineFlag::Animation))
            data << moveSpline.effect_start_time;       // added in 3.1

        data << uint8(type);

        if (type == MonsterMoveFacingAngle)
            data << float(moveSpline.facing.angle);

        if (type == MonsterMoveFacingPoint)
        {
            Vector3 facing = transform(G3D::Vector3(moveSpline.facing.f.x, moveSpline.facing.f.y, moveSpline.facing.f.z));
            data << facing.x << facing.z << facing.y;
        }

        if ((splineFlags & MoveSplineFlag::Parabolic) && moveSpline.effect_start_time < moveSpline.Duration())
            data << float(moveSpline.vertical_acceleration);   // added in 3.1

        //    NYI block here
        data << moveSpline.Duration();
    }

    Vector3 destination = moveSpline.isCyclic() ? Vector3::zero() : moveSpline.FinalDestination();
    Vector3 dest = transform(destination);

    data << float(dest.x);
    data << float(dest.z);

    data << moveSpline.GetId();

    data << float(dest.y);
}

void PacketBuilder::WriteFacingTargetPart(MoveSpline const& moveSpline, ByteBuffer& data)
{
    if (GetMonsterMoveType(moveSpline) == MonsterMoveFacingTarget && !moveSpline.Finalized())
    {
        ObjectGuid facingGuid = moveSpline.facing.target;

        data.WriteGuidMask(facingGuid, 4, 7, 0, 5, 1, 2, 3, 6);
        data.WriteGuidBytes(facingGuid, 4, 2, 0, 5, 6, 3, 1, 7);
    }
}
