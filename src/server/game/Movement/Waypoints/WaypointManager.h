/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_WAYPOINTMANAGER_H
#define TRINITY_WAYPOINTMANAGER_H

#include <vector>

#include "Singleton/Singleton.hpp"

enum WaypointMoveType
{
    WAYPOINT_MOVE_TYPE_NONE     = -1,
    WAYPOINT_MOVE_TYPE_WALK     = 0,
    WAYPOINT_MOVE_TYPE_RUN      = 1,
    WAYPOINT_MOVE_TYPE_LAND     = 2,
    WAYPOINT_MOVE_TYPE_TAKEOFF  = 3,

    WAYPOINT_MOVE_TYPE_MAXIMUM
};

struct WaypointData
{
    WaypointData() : ID(0), x(0.0f), y(0.0f), z(0.0f), orientation(0.0f), Delay(-1), EventID(0), EventChance(0), MoveType(WAYPOINT_MOVE_TYPE_NONE) { }

    uint32 ID;
    float x;
    float y;
    float z;
    float orientation;
    int32 Delay;
    uint32 EventID;
    uint8 EventChance;
    int8 MoveType;
};

typedef std::vector<WaypointData>                WaypointPath;
typedef std::unordered_map<uint32, WaypointPath> WaypointPathContainer;

class WaypointMgr
{
        friend class Tod::Singleton<WaypointMgr>;

    public:
        // Attempts to reload a single path from database
        void ReloadPath(uint32 id);

        // Loads all paths from database, should only run on startup
        void Load();

        // Returns the path from a given id
        WaypointPath const* GetPath(uint32 id) const
        {
            WaypointPathContainer::const_iterator itr = _waypointStore.find(id);
            if (itr != _waypointStore.end())
                return &itr->second;

            return nullptr;
        }

    private:
        // Only allow instantiation from ACE_Singleton
        WaypointMgr();
        ~WaypointMgr();

        WaypointPathContainer _waypointStore;
};

#define sWaypointMgr Tod::Singleton<WaypointMgr>::GetSingleton()

#endif
