/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//Basic headers
#include "WaypointMovementGenerator.h"
//Extended headers
#include "ObjectMgr.h"
#include "World.h"
#include "Transport.h"
//Flightmaster grid preloading
#include "MapManager.h"
//Creature-specific headers
#include "Creature.h"
#include "CreatureAI.h"
#include "CreatureGroups.h"
//Player-specific
#include "Player.h"
#include "MoveSplineInit.h"
#include "MoveSpline.h"
#include "SpellAuraEffects.h"

void WaypointMovementGenerator<Creature>::DoInitialize(Creature* creature)
{
    if (LoadedFromDB)
    {
        if (!PathID)
            PathID = creature->GetWaypointPath();

        i_path = sWaypointMgr->GetPath(PathID);
    }

    if (!i_path)
    {
        // No path id found for entry
        TC_LOG_ERROR("sql.sql", "WaypointMovementGenerator::LoadPath: creature %s (Entry: %u GUID: %u DB GUID: %u) doesn't have waypoint path id: %u", creature->GetName().c_str(), creature->GetEntry(), creature->GetGUID().GetCounter(), creature->GetSpawnId(), PathID);
        return;
    }

    creature->AddUnitState(UNIT_STATE_ROAMING);

    if (creature->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        _reinitialized = true;
        return;
    }

    if (!Stopped())
        StartMoveNow(creature);
}

void WaypointMovementGenerator<Creature>::DoFinalize(Creature* creature)
{
    creature->ClearUnitState(UNIT_STATE_ROAMING | UNIT_STATE_ROAMING_MOVE);
    creature->SetWalk(false);
}

void WaypointMovementGenerator<Creature>::DoReset(Creature* creature)
{
    creature->AddUnitState(UNIT_STATE_ROAMING);

    if (creature->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        _reinitialized = true;
        return;
    }

    if (!Stopped())
        StartMoveNow(creature);
}

void WaypointMovementGenerator<Creature>::OnArrived(Creature* creature, uint32 node)
{
    if (!i_path || i_path->empty())
        return;

    if (IsArrivalDone)
        return;

    WaypointData const& waypoint = i_path->at(node);
    if (waypoint.Delay != 0)
    {
        creature->ClearUnitState(UNIT_STATE_ROAMING_MOVE);

        // waypoint.Delay = -1 -> means infinite stop - will resume in script
        Stop(creature, waypoint.Delay < 0 ? 2 * HOUR * IN_MILLISECONDS : waypoint.Delay, true);
    }

    if (waypoint.EventID && Math::Rand(0, 99) < waypoint.EventChance)
    {
        TC_LOG_DEBUG("maps.script", "Creature movement start script %u at point %u for %s.", waypoint.EventID, node, creature->GetGUID().ToString().c_str());
        creature->ClearUnitState(UNIT_STATE_ROAMING_MOVE);
        creature->GetMap()->ScriptsStart(sWaypointScripts, waypoint.EventID, creature, NULL);
    }

    // Inform script
    MovementInform(creature, waypoint.ID);
    creature->UpdateWaypointID(waypoint.ID);
}

bool WaypointMovementGenerator<Creature>::StartMove(Creature* creature)
{
    if (!creature || !creature->IsAlive())
        return false;

    if (!i_path || i_path->empty())
        return false;

    bool transportPath = creature->GetTransport() != nullptr;

    if (IsArrivalDone)
    {
        if ((i_currentNode == i_path->size() - 1) && !Repeating) // If that's our last waypoint
        {
            WaypointData const& waypoint = i_path->at( i_currentNode );

            float x = waypoint.x;
            float y = waypoint.y;
            float z = waypoint.z;
            float o = creature->GetOrientation();

            if (!transportPath)
                creature->SetHomePosition(x, y, z, o);
            else
            {
                if (Transport* trans = creature->GetTransport())
                {
                    o -= trans->GetOrientation();
                    creature->SetTransportHomePosition(x, y, z, o);
                    trans->CalculatePassengerPosition(x, y, z, &o);
                    creature->SetHomePosition(x, y, z, o);
                }
                else
                    transportPath = false;
                // else if (vehicle) - this should never happen, vehicle offsets are const
            }

            return false;
        }

        i_currentNode = (i_currentNode + 1) % i_path->size();
    }

    IsArrivalDone = false;
    _reinitialized = false;
    i_recalculateSpeed = false;

    creature->AddUnitState(UNIT_STATE_ROAMING_MOVE);

    WaypointData const& previousWaypoint = i_path->at(i_previousNode);
    WaypointData const& waypoint = i_path->at(i_currentNode);
    Location loc(waypoint.x, waypoint.y, waypoint.z, waypoint.orientation);

    MoveSplineInit init(creature);

    //! If creature is on transport, we assume waypoints set in DB are already transport offsets
    if (transportPath)
    {
        init.DisableTransportPathTransformations();
        if (TransportBase* trans = creature->GetDirectTransport())
            trans->CalculatePassengerPosition(loc.x, loc.y, loc.z, &loc.orientation);
    }

    PathGenerator path(creature, !transportPath);

    bool result = false;

    // if using smooth pathing, calculate path from previous point (not from current creature's position) to current point
    if (_prefinalizePath && i_previousNode != i_currentNode)
    {
        Vector3 startPosition = Vector3(previousWaypoint.x, previousWaypoint.y, previousWaypoint.z);
        result = path.CalculatePath(waypoint.x, waypoint.y, waypoint.z, false, false, startPosition);
    }
    else
        result = path.CalculatePath(waypoint.x, waypoint.y, waypoint.z);

    if (result && !(path.GetPathType() & PATHFIND_NOPATH))
    {
        PointsArray& points = path.GetPath();

        // if using smooth pathing, don't forget include current creature's position in front of path
        if (_prefinalizePath && i_previousNode != i_currentNode)
            points.insert(points.begin(), Vector3(creature->GetPositionX(), creature->GetPositionY(), creature->GetPositionZMinusOffset()));

        init.MovebyPath(points);
    }
    else
        init.MoveTo(waypoint.x, waypoint.y, waypoint.z, false);

    if (LoadedFromDB)
    {
        switch (waypoint.MoveType)
        {
            case WAYPOINT_MOVE_TYPE_LAND:
                init.SetAnimation(ToGround);
                break;
            case WAYPOINT_MOVE_TYPE_TAKEOFF:
                init.SetAnimation(ToFly);
                break;
            case WAYPOINT_MOVE_TYPE_RUN:
                init.SetWalk(false);
                break;
            case WAYPOINT_MOVE_TYPE_WALK:
                init.SetWalk(true);
                break;
            default:
                break;
        }
    }

    if (waypoint.orientation != 0.0f)
        init.SetFacing(waypoint.orientation);

    init.Launch();

    //Call for creature group update
    if (creature->GetFormation() && creature->GetFormation()->getLeader() == creature)
        creature->GetFormation()->LeaderMoveTo(loc.x, loc.y, loc.z);

    // if smooth pathing has been broken in previous move packet (e.g. due to delay), update i_previousNode
    // and don't call old i_previousNode in OnArrived code (trigerred by _prefinalizePath in Update)
    if (!_prefinalizePath)
        i_previousNode = i_currentNode;

    _prefinalizePath = waypoint.Delay == 0 && i_currentNode != i_path->size() - 1;

    return true;
}

bool WaypointMovementGenerator<Creature>::DoUpdate(Creature* creature, uint32 diff)
{
    if (!creature || !creature->IsAlive())
        return false;

    // prevent a crash at empty waypoint path.
    if (!i_path || i_path->empty())
        return false;

    // prevent movement while casting spells with cast time or channel time
    if (creature->IsMovementPreventedByCasting())
    {
        if (i_nextMoveTime.GetExpiry() < 1)
            i_nextMoveTime.Reset(1);

        if (!creature->IsStopped())
            creature->StopMoving();

        creature->ClearUnitState(UNIT_STATE_ROAMING_MOVE);
        return true;
    }

    if (creature->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        if (i_nextMoveTime.GetExpiry() < 1)
            i_nextMoveTime.Reset(1);

        creature->ClearUnitState(UNIT_STATE_ROAMING_MOVE);
        return true;
    }

    if (Stopped())
    {
        if (CanMove(diff))
        {
            bool Resumed = StartMoveNow(creature);
            if (Resumed)
            {
                WaypointInform(creature, false, _stoppedOnNode ? i_currentNode : -1);
                _stoppedOnNode = false;
            }

            return Resumed;
        }
    }
    else
    {
        // Set home position at place on waypoint movement.
        if (creature->GetTransportGUID().IsEmpty())
            creature->SetHomePosition(creature->GetPosition());

        if (creature->IsStopped())
            Stop(creature, sWorld->getIntConfig(CONFIG_CREATURE_STOP_FOR_PLAYER));
        else if (creature->movespline->Finalized())
        {
            OnArrived(creature, i_currentNode);

            IsArrivalDone = true;

            if (!Stopped())
            {
                if (creature->IsStopped())
                    Stop(creature, sWorld->getIntConfig(CONFIG_CREATURE_STOP_FOR_PLAYER));
                else
                    return StartMoveNow(creature);
            }
        }
        else if (i_recalculateSpeed || _reinitialized) // speed changed during path execution, calculate remaining path and launch it once more
        {
            if (!Stopped())
                return StartMoveNow(creature);
        }
        else if (i_previousNode != i_currentNode) // in case of smooth pathing, call here i_previousNode point (currentPathIdx == 1)
        {
            if (creature->movespline->currentPathIdx() >= 1)
            {
                OnArrived(creature, i_previousNode);

                i_previousNode = i_currentNode;
            }
        }
        else if (_prefinalizePath)  // in case of smooth pathing, send new move packet, if near to last point in old move packet
        {
            if (creature->movespline->timeElapsed() <= 1000) // Anakin: 1000 is not blizzlike, I guess... but works good :)
            {
                IsArrivalDone = true;

                if (!Stopped())
                    return StartMoveNow(creature);
            }
        }
    }

    return true;
}

void WaypointMovementGenerator<Creature>::MovementInform(Creature* creature, uint32 pointID)
{
    if (creature->AI())
        creature->AI()->MovementInform(WAYPOINT_MOTION_TYPE, pointID);
}

void WaypointMovementGenerator<Creature>::WaypointInform(Creature* creature, bool stop, int32 point)
{
    if (creature->AI())
        creature->AI()->WaypointInform(stop, point);
}

void WaypointMovementGenerator<Creature>::Stop(Creature* creature, int32 time, bool stopOnNode /*=false*/)
{
    i_nextMoveTime.Reset(time);

    _stoppedOnNode = stopOnNode;

    WaypointInform(creature, true, _stoppedOnNode ? 0 : -1);
}

bool WaypointMovementGenerator<Creature>::GetResetPos(Creature*, float& x, float& y, float& z)
{
    // prevent a crash at empty waypoint path.
    if (!i_path || i_path->empty())
        return false;

    WaypointData const & waypoint = i_path->at(i_currentNode);

    x = waypoint.x;
    y = waypoint.y;
    z = waypoint.z;
    return true;
}


//----------------------------------------------------//

uint32 FlightPathMovementGenerator::GetPathAtMapEnd() const
{
    if (i_currentNode >= i_path.size())
        return i_path.size();

    uint32 curMapId = i_path[i_currentNode]->MapID;
    for (uint32 i = i_currentNode; i < i_path.size(); ++i)
        if (i_path[i]->MapID != curMapId)
            return i;

    return i_path.size();
}

#define SKIP_SPLINE_POINT_DISTANCE_SQ (40.0f * 40.0f)

bool IsNodeIncludedInShortenedPath(TaxiPathNodeEntry const* p1, TaxiPathNodeEntry const* p2)
{
    return p1->MapID != p2->MapID || std::pow(p1->Loc.X - p2->Loc.X, 2) + std::pow(p1->Loc.Y - p2->Loc.Y, 2) > SKIP_SPLINE_POINT_DISTANCE_SQ;
}

void FlightPathMovementGenerator::LoadPath(Player* player, uint32 startNode /*= 0*/)
{
    i_path.clear();
    i_currentNode = startNode;
    _pointsForPathSwitch.clear();
    std::deque<uint32> const& taxi = player->m_taxi.GetPath();
    for (uint32 src = 0, dst = 1; dst < taxi.size(); src = dst++)
    {
        uint32 path, cost;
        sObjectMgr->GetTaxiPath(taxi[src], taxi[dst], path, cost);
        if (path > sDBCManager->GetTaxiPathNodesByPath().size())
            return;

        TaxiPathNodeList const& nodes = sDBCManager->GetTaxiPathNodeList(path);
        if (!nodes.empty())
        {
            TaxiPathNodeEntry const* start = nodes[0];
            TaxiPathNodeEntry const* end = nodes[nodes.size() - 1];
            bool passedPreviousSegmentProximityCheck = false;
            for (uint32 i = 0; i < nodes.size(); ++i)
            {
                if (passedPreviousSegmentProximityCheck || !src || i_path.empty() || IsNodeIncludedInShortenedPath(i_path.back(), nodes[i]))
                {
                    if ((!src || (IsNodeIncludedInShortenedPath(start, nodes[i]) && i >= 2)) &&
                        (dst == taxi.size() - 1 || (IsNodeIncludedInShortenedPath(end, nodes[i]) && i < nodes.size() - 1)))
                    {
                        passedPreviousSegmentProximityCheck = true;
                        i_path.push_back(nodes[i]);
                    }
                }
                else
                {
                    i_path.pop_back();
                    --_pointsForPathSwitch.back().PathIndex;
                }
            }
        }

        _pointsForPathSwitch.push_back({ uint32(i_path.size() - 1), int32(cost) });
    }
}

void FlightPathMovementGenerator::DoInitialize(Player* player)
{
    Reset(player);
    InitEndGridInfo();
}

void FlightPathMovementGenerator::DoFinalize(Player* player)
{
    // remove flag to prevent send object build movement packets for flight state and crash (movement generator already not at top of stack)
    player->ClearUnitState(UNIT_STATE_IN_FLIGHT);

    player->Dismount();
    player->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_REMOVE_CLIENT_CONTROL | UNIT_FLAG_TAXI_FLIGHT);

    if (player->m_taxi.empty())
    {
        player->GetHostileRefManager().setOnlineOfflineState(true);
        // update z position to ground and orientation for landing point
        // this prevent cheating with landing  point at lags
        // when client side flight end early in comparison server side
        player->StopMoving();
        player->SetFallInformation(player->GetPositionZ());
    }

    player->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_TAXI_BENCHMARK);
}

void FlightPathMovementGenerator::DoReset(Player* player)
{
    float playerFlightSpeed = 32.0f;

    Unit::AuraEffectList const& mIncreaseTaxiFlightSpeed = player->GetAuraEffectsByType(SPELL_AURA_MOD_TAXI_FLIGHT_SPEED);
    for (Unit::AuraEffectList::const_iterator itr = mIncreaseTaxiFlightSpeed.begin(); itr != mIncreaseTaxiFlightSpeed.end(); ++itr)
        AddPct(playerFlightSpeed, (*itr)->GetAmount());

    player->GetHostileRefManager().setOnlineOfflineState(false);
    player->AddUnitState(UNIT_STATE_IN_FLIGHT);
    player->SetFlag(UNIT_FLAGS, UNIT_FLAG_REMOVE_CLIENT_CONTROL | UNIT_FLAG_TAXI_FLIGHT);

    MoveSplineInit init(player);
    uint32 end = GetPathAtMapEnd();
    for (uint32 i = i_currentNode; i != end; ++i)
    {
        Vector3 vertice(i_path[i]->Loc.X, i_path[i]->Loc.Y, i_path[i]->Loc.Z);
        init.Path().push_back(vertice);
    }
    init.SetFirstPointId(i_currentNode);
    init.SetFly();
    init.SetSmooth();
    init.SetUncompressed();
    init.SetWalk(true);
    init.SetVelocity(playerFlightSpeed);
    init.Launch();
}

bool FlightPathMovementGenerator::DoUpdate(Player* player, uint32 /*diff*/)
{
    uint32 pointId = (uint32)player->movespline->currentPathIdx();
    if (pointId > i_currentNode)
    {
        bool departureEvent = true;
        do
        {
            DoEventIfAny(player, i_path[i_currentNode], departureEvent);
            while (!_pointsForPathSwitch.empty() && _pointsForPathSwitch.front().PathIndex <= i_currentNode)
            {
                _pointsForPathSwitch.pop_front();
                player->m_taxi.NextTaxiDestination();
                if (!_pointsForPathSwitch.empty())
                {
                    player->UpdateCriteria(CRITERIA_TYPE_GOLD_SPENT_FOR_TRAVELLING, _pointsForPathSwitch.front().Cost);
                    player->ModifyMoney(-_pointsForPathSwitch.front().Cost);
                }
            }

            if (pointId == i_currentNode)
                break;

            if (i_currentNode == _preloadTargetNode)
                PreloadEndGrid();
            i_currentNode += (uint32)departureEvent;
            departureEvent = !departureEvent;
        }
        while (true);
    }

    return i_currentNode < (i_path.size() - 1);
}

void FlightPathMovementGenerator::SetCurrentNodeAfterTeleport()
{
    if (i_path.empty() || i_currentNode >= i_path.size())
        return;

    uint32 map0 = i_path[i_currentNode]->MapID;
    for (size_t i = i_currentNode + 1; i < i_path.size(); ++i)
    {
        if (i_path[i]->MapID != map0)
        {
            i_currentNode = i;
            return;
        }
    }
}

void FlightPathMovementGenerator::DoEventIfAny(Player* player, TaxiPathNodeEntry const* node, bool departure)
{
    if (uint32 eventid = departure ? node->DepartureEventID : node->ArrivalEventID)
    {
        TC_LOG_DEBUG("maps.script", "Taxi %s event %u of node %u of path %u for player %s", departure ? "departure" : "arrival", eventid, node->Index, node->Path, player->GetName().c_str());
        player->GetMap()->ScriptsStart(sEventScripts, eventid, player, player);
    }
}

bool FlightPathMovementGenerator::GetResetPos(Player*, float& x, float& y, float& z)
{
    TaxiPathNodeEntry const* node = i_path[i_currentNode];
    x = node->Loc.X;
    y = node->Loc.Y;
    z = node->Loc.Z;
    return true;
}

void FlightPathMovementGenerator::InitEndGridInfo()
{
    /*! Storage to preload flightmaster grid at end of flight. For multi-stop flights, this will
       be reinitialized for each flightmaster at the end of each spline (or stop) in the flight. */
    uint32 nodeCount = i_path.size();        //! Number of nodes in path.
    _endMapId = i_path[nodeCount - 1]->MapID; //! MapId of last node
    _preloadTargetNode = nodeCount - 3;
    _endGridX = i_path[nodeCount - 1]->Loc.X;
    _endGridY = i_path[nodeCount - 1]->Loc.Y;
}

void FlightPathMovementGenerator::PreloadEndGrid()
{
    // used to preload the final grid where the flightmaster is
    Map* endMap = sMapMgr->FindBaseNonInstanceMap(_endMapId);

    // Load the grid
    if (endMap)
    {
        TC_LOG_DEBUG("misc", "Preloading rid (%f, %f) for map %u at node index %u/%u", _endGridX, _endGridY, _endMapId, _preloadTargetNode, (uint32)(i_path.size() - 1));
        endMap->LoadGrid(_endGridX, _endGridY);
    }
    else
        TC_LOG_DEBUG("misc", "Unable to determine map to preload flightmaster grid");
}
