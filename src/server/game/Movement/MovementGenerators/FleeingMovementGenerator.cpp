/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Creature.h"
#include "CreatureAI.h"
#include "MapManager.h"
#include "FleeingMovementGenerator.h"
#include "PathGenerator.h"
#include "ObjectAccessor.h"
#include "MoveSplineInit.h"
#include "MoveSpline.h"
#include "Player.h"
#include "VMapFactory.h"

#define MIN_QUIET_DISTANCE 15.0f
#define MAX_QUIET_DISTANCE 30.0f

template<class T>
void FleeingMovementGenerator<T>::_setTargetLocation(T* owner)
{
    if (!owner)
        return;

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    _getPoint(owner, x, y, z);

    // Add LOS check for target point
    Position mypos = owner->GetPosition();

    bool isInLOS = owner->GetMap()->isInLineOfSight(mypos.m_positionX, mypos.m_positionY, mypos.m_positionZ + 2.0f, x, y, z + 2.0f,
        owner->GetPhases(), owner->GetTerrainSwaps(), VMAP::ModelIgnoreFlags::Nothing, true);

    if (!isInLOS)
    {
        i_nextCheckTime.Reset(100);
        return;
    }

    PathGenerator path(owner);
    bool result = path.CalculatePath(x, y, z);
    if (!result || (path.GetPathType() & (PATHFIND_NOPATH | PATHFIND_INCOMPLETE)))
    {
        i_nextCheckTime.Reset(100);
        return;
    }

    _reinitialized = false;

    owner->AddUnitState(UNIT_STATE_FLEEING_MOVE);

    MoveSplineInit init(owner);
    init.MovebyPath(path.GetPath());
    init.SetWalk(false);
    int32 traveltime = init.Launch();
    i_nextCheckTime.Reset(traveltime + Math::Rand(600, 1200));
}

template<class T>
void FleeingMovementGenerator<T>::_getPoint(T* owner, float &x, float &y, float &z)
{
    float dist_from_caster = 0.0f;
    float angle_to_caster = 0.0f;

    if (Unit* fright = ObjectAccessor::GetUnit(*owner, i_frightGUID))
    {
        dist_from_caster = fright->GetDistance(owner);
        if (dist_from_caster > 0.2f)
            angle_to_caster = fright->GetAngle(owner);
        else
            angle_to_caster = Math::Rand( 0, 2 * static_cast<float>(M_PI));
    }
    else
    {
        dist_from_caster = 0.0f;
        angle_to_caster = Math::Rand( 0, 2 * static_cast<float>(M_PI));
    }

    float dist = 0.0f;
    float angle = 0.0f;

    if (dist_from_caster < MIN_QUIET_DISTANCE)
    {
        dist = Math::Rand(0.8f, 1.3f) * (MIN_QUIET_DISTANCE - (dist_from_caster / 2));
        angle = angle_to_caster + Math::Rand(-static_cast<float>(M_PI) / 4, static_cast<float>(M_PI) / 4);
    }
    else if (dist_from_caster > MAX_QUIET_DISTANCE)
    {
        dist = Math::Rand(0.5f, 1.0f)*(MAX_QUIET_DISTANCE - MIN_QUIET_DISTANCE);
        angle = static_cast<float>(M_PI) + angle_to_caster + Math::Rand(-static_cast<float>(M_PI) / 4, static_cast<float>(M_PI) / 4);
    }
    else    // we are inside quiet range
    {
        dist = Math::Rand(0.7f, 1.0f)*(MAX_QUIET_DISTANCE - MIN_QUIET_DISTANCE);
        angle = Math::Rand(0, 2*static_cast<float>(M_PI));
    }

    // In GetFirstCollisionPosition we have automatic addtion owner's orientation
    // so now let's subtract it
    angle -= owner->GetOrientation();

    Position pos = owner->GetFirstCollisionPosition(dist, angle);
    x = pos.m_positionX;
    y = pos.m_positionY;
    z = pos.m_positionZ;
}

template<class T>
void FleeingMovementGenerator<T>::DoInitialize(T* owner)
{
    if (!owner)
        return;

    if (!owner->IsStopped())
        owner->StopMoving();

    owner->SetFlag(UNIT_FLAGS, UNIT_FLAG_FLEEING);
    owner->AddUnitState(UNIT_STATE_FLEEING);

    if (owner->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        _reinitialized = true;
        return;
    }

    _setTargetLocation(owner);
}

template<>
void FleeingMovementGenerator<Player>::DoFinalize(Player* owner)
{
    owner->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_FLEEING);
    owner->ClearUnitState(UNIT_STATE_FLEEING | UNIT_STATE_FLEEING_MOVE);
    owner->StopMoving();
}

template<>
void FleeingMovementGenerator<Creature>::DoFinalize(Creature* owner)
{
    owner->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_FLEEING);
    owner->ClearUnitState(UNIT_STATE_FLEEING | UNIT_STATE_FLEEING_MOVE);
    if (owner->GetVictim())
        owner->SetTarget(owner->GetVictim()->GetGUID());
}

template<class T>
void FleeingMovementGenerator<T>::DoReset(T* owner)
{
    DoInitialize(owner);
}

template<class T>
bool FleeingMovementGenerator<T>::DoUpdate(T* owner, uint32 time_diff)
{
    if (!owner || !owner->IsAlive())
        return false;

    if (owner->IsMovementPreventedByCasting())
    {
        owner->CastStop();
        i_nextCheckTime.Reset(0);
        owner->ClearUnitState(UNIT_STATE_FLEEING_MOVE);
        return true;
    }

    if (owner->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        i_nextCheckTime.Reset(0);
        owner->ClearUnitState(UNIT_STATE_FLEEING_MOVE);
        return true;
    }

    i_nextCheckTime.Update(time_diff);
    if (i_nextCheckTime.Passed() && (owner->movespline->Finalized() || _reinitialized))
        _setTargetLocation(owner);

    return true;
}

template void FleeingMovementGenerator<Player>::DoInitialize(Player*);
template void FleeingMovementGenerator<Creature>::DoInitialize(Creature*);
template void FleeingMovementGenerator<Player>::_getPoint(Player*, float&, float&, float&);
template void FleeingMovementGenerator<Creature>::_getPoint(Creature*, float&, float&, float&);
template void FleeingMovementGenerator<Player>::_setTargetLocation(Player*);
template void FleeingMovementGenerator<Creature>::_setTargetLocation(Creature*);
template void FleeingMovementGenerator<Player>::DoReset(Player*);
template void FleeingMovementGenerator<Creature>::DoReset(Creature*);
template bool FleeingMovementGenerator<Player>::DoUpdate(Player*, uint32);
template bool FleeingMovementGenerator<Creature>::DoUpdate(Creature*, uint32);

void TimedFleeingMovementGenerator::Finalize(Unit* owner)
{
    owner->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_FLEEING);
    owner->ClearUnitState(UNIT_STATE_FLEEING | UNIT_STATE_FLEEING_MOVE);
    if (Unit* victim = owner->GetVictim())
    {
        if (owner->IsAlive())
        {
            owner->AttackStop();
            owner->ToCreature()->AI()->AttackStart(victim);
        }
    }
}

bool TimedFleeingMovementGenerator::Update(Unit* owner, uint32 time_diff)
{
    if (!owner->IsAlive())
        return false;

    i_totalFleeTime.Update(time_diff);
    if (i_totalFleeTime.Passed())
        return false;

    // This calls grant-parent Update method hiden by FleeingMovementGenerator::Update(Creature &, uint32) version
    // This is done instead of casting Unit& to Creature& and call parent method, then we can use Unit directly
    return MovementGeneratorMedium< Creature, FleeingMovementGenerator<Creature> >::Update(owner, time_diff);
}
