/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_POINTMOVEMENTGENERATOR_H
#define TRINITY_POINTMOVEMENTGENERATOR_H

#include "MovementGenerator.h"
#include "FollowerReference.h"
#include "MoveSplineInit.h"

template<class T>
class PointMovementGenerator : public MovementGeneratorMedium< T, PointMovementGenerator<T> >
{
    public:
        PointMovementGenerator(uint32 _id, float _x, float _y, float _z, bool _generatePath, float _speed = 0.0f) : id(_id),
            i_x(_x), i_y(_y), i_z(_z), i_orient(0.0f), speed(_speed), m_generatePath(_generatePath), i_recalculateSpeed(false), hasOrient(false), _reinitialized(false) { }

        PointMovementGenerator(uint32 _id, Position const& _pos, float _orient, bool _generatePath = true) : id(_id), i_orient(_orient), speed(0.0f),
            m_generatePath(_generatePath), i_recalculateSpeed(false), hasOrient(true), _reinitialized(false)
        {
            i_x = _pos.GetPositionX();
            i_y = _pos.GetPositionY();
            i_z = _pos.GetPositionZ();
        }

        void DoInitialize(T*);
        void DoFinalize(T*);
        void DoReset(T*);
        bool DoUpdate(T*, uint32);

        void MovementInform(Player* player);
        void MovementInform(Creature* creature);

        void ReInitialize(T*);

        void unitSpeedChanged() override { i_recalculateSpeed = true; }

        MovementGeneratorType GetMovementGeneratorType() { return POINT_MOTION_TYPE; }

        void GetDestination(float& x, float& y, float& z) const { x = i_x; y = i_y; z = i_z; }

    private:
        uint32 id;
        float i_x, i_y, i_z, i_orient;
        float speed;
        bool m_generatePath;
        bool i_recalculateSpeed;
        bool hasOrient;

        bool _reinitialized;
};

class AssistanceMovementGenerator : public PointMovementGenerator<Creature>
{
    public:
        AssistanceMovementGenerator(float _x, float _y, float _z) :
            PointMovementGenerator<Creature>(0, _x, _y, _z, true) { }

        MovementGeneratorType GetMovementGeneratorType() override { return ASSISTANCE_MOTION_TYPE; }
        void Finalize(Unit*) override;
};

// Does almost nothing - just doesn't allows previous movegen interrupt current effect.
class EffectMovementGenerator : public MovementGenerator
{
    public:
        EffectMovementGenerator(uint32 id, float x, float y, float z, Unit* facingTarget) : _id(id), _x(x), _y(y), _z(z), _facing(facingTarget),
                                _orient(0.0f), _speed(0.0f),_height(0.0f), _orientFixed(false), _generatePath(true), _isFall(false),
                                _anim(AnimType::None), _arrivalSpellId(0), _arrivalSpellTargetGuid(ObjectGuid::Empty), _reinitialized(false) { }

        EffectMovementGenerator(uint32 id, float x, float y, float z, AnimType anim) : _id(id), _x(x), _y(y), _z(z), _facing(nullptr),
                                _orient(0.0f), _speed(0.0f),_height(0.0f), _orientFixed(false), _generatePath(true), _isFall(false),
                                _anim(anim), _arrivalSpellId(0), _arrivalSpellTargetGuid(ObjectGuid::Empty), _reinitialized(false) { }

        EffectMovementGenerator(uint32 id, float x, float y, float z, float speed, float height, bool orientFixed = true, bool generatePath = true,
                                float orient = 0.0f, uint32 arrivalSpellId = 0, ObjectGuid const& arrivalSpellTargetGuid = ObjectGuid::Empty) :
                                _id(id), _x(x), _y(y), _z(z), _facing(nullptr),
                                _orient(orient), _speed(speed), _height(height), _orientFixed(orientFixed), _generatePath(generatePath), _isFall(false),
                                _anim(AnimType::None), _arrivalSpellId(arrivalSpellId), _arrivalSpellTargetGuid(arrivalSpellTargetGuid), _reinitialized(false) { }

        EffectMovementGenerator(uint32 id, float x, float y, float z, bool IsFall) : _id(id), _x(x), _y(y), _z(z), _facing(nullptr),
                                _orient(0.0f), _speed(0.0f),_height(0.0f), _orientFixed(false), _generatePath(false), _isFall(IsFall),
                                _anim(AnimType::None), _arrivalSpellId(0), _arrivalSpellTargetGuid(ObjectGuid::Empty), _reinitialized(false) { }

        void Initialize(Unit*) override;
        void Finalize(Unit*) override;
        void Reset(Unit*) override { }
        bool Update(Unit*, uint32) override;

        void ReInitialize(Unit*);

        bool IsFallMovement() const { return _isFall; }

        MovementGeneratorType GetMovementGeneratorType() override { return EFFECT_MOTION_TYPE; }

    private:
        uint32 _id;
        float _x;
        float _y;
        float _z;
        float _orient;
        float _speed;
        float _height;
        bool _orientFixed;
        bool _generatePath;
        bool _isFall;
        Unit* _facing;
        AnimType _anim;
        uint32 _arrivalSpellId;
        ObjectGuid _arrivalSpellTargetGuid;

        bool _reinitialized;
};

#endif
