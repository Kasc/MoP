/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "PointMovementGenerator.h"
#include "Errors.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "World.h"
#include "MoveSplineInit.h"
#include "MoveSpline.h"
#include "Player.h"
#include "CreatureGroups.h"
#include "ObjectAccessor.h"
#include "BattlePet.h"

//----- Point Movement Generator
template<class T>
void PointMovementGenerator<T>::DoInitialize(T* unit)
{
    if (!unit->IsStopped())
        unit->StopMoving();

    unit->AddUnitState(UNIT_STATE_ROAMING | UNIT_STATE_ROAMING_MOVE);

    if (id == EVENT_CHARGE_PREPATH)
        return;

    if (unit->HasUnitState(UNIT_STATE_STUNNED | UNIT_STATE_ROOT))
    {
        _reinitialized = true;
        return;
    }

    // fix strange client visual bug, moving on z coordinate only switches orientation by 180 degrees(visual only)
    if (G3D::fuzzyEq(unit->GetPositionX(), i_x) && G3D::fuzzyEq(unit->GetPositionY(), i_y))
    {
        i_x += 0.2f * cos(unit->GetOrientation());
        i_y += 0.2f * sin(unit->GetOrientation());
    }

    MoveSplineInit init(unit);
    init.MoveTo(i_x, i_y, i_z, m_generatePath);

    if (speed > 0.0f)
        init.SetVelocity(speed);

    if (hasOrient)
        init.SetFacing(i_orient);

    init.Launch();

    // Call for creature group update
    if (Creature* creature = unit->ToCreature())
        if (creature->GetFormation() && creature->GetFormation()->getLeader() == creature)
            creature->GetFormation()->LeaderMoveTo(i_x, i_y, i_z);
}

template<class T>
bool PointMovementGenerator<T>::DoUpdate(T* unit, uint32 /*diff*/)
{
    if (!unit)
        return false;

    if (unit->HasUnitState(UNIT_STATE_STUNNED | UNIT_STATE_ROOT))
    {
        unit->ClearUnitState(UNIT_STATE_ROAMING_MOVE);
        return true;
    }

    unit->AddUnitState(UNIT_STATE_ROAMING_MOVE);

    bool arrived = unit->movespline->Finalized();

    if (id != EVENT_CHARGE_PREPATH)
    {
        if (arrived)
        {
            if (unit->GetMotionMaster()->GetMotionSlot(MOTION_SLOT_ACTIVE) == this)
            {
                Vector3 dest = unit->movespline->FinalDestination();

                if (unit->movespline->onTransport)
                    if (TransportBase* transport = unit->GetDirectTransport())
                        transport->CalculatePassengerPosition(dest.x, dest.y, dest.z);

                if (unit->IsHovering())
                    dest.z += unit->GetFloatValue(UNIT_HOVER_HEIGHT);

                bool arrivedDest = unit->GetPosition().IsInDist(dest.x, dest.y, dest.z, CONTACT_DISTANCE);
                if (arrivedDest)
                    return false;

                ReInitialize(unit);

                return true;
            }

            return false;
        }
        else if (i_recalculateSpeed || _reinitialized)
            ReInitialize(unit);
    }

    return !arrived;
}

template<class T>
void PointMovementGenerator<T>::DoFinalize(T* unit)
{
    unit->ClearUnitState(UNIT_STATE_ROAMING | UNIT_STATE_ROAMING_MOVE);

    if (unit->movespline->Finalized())
        MovementInform(unit);
}

template<class T>
void PointMovementGenerator<T>::DoReset(T* unit)
{
    if (!unit->IsStopped())
        unit->StopMoving();

    unit->AddUnitState(UNIT_STATE_ROAMING | UNIT_STATE_ROAMING_MOVE);
}

template<class T>
void PointMovementGenerator<T>::ReInitialize(T* unit)
{
    i_recalculateSpeed = false;
    _reinitialized = false;

    MoveSplineInit init(unit);
    init.MoveTo(i_x, i_y, i_z, m_generatePath);

    if (speed > 0.0f) // Default value for point motion type is 0.0, if 0.0 spline will use GetSpeed on unit
        init.SetVelocity(speed);

    if (hasOrient)
        init.SetFacing(i_orient);

    init.Launch();

    // Call for creature group update
    if (Creature* creature = unit->ToCreature())
        if (creature->GetFormation() && creature->GetFormation()->getLeader() == creature)
            creature->GetFormation()->LeaderMoveTo(i_x, i_y, i_z);
}

template <> void PointMovementGenerator<Player>::MovementInform(Player* player)
{
    // special case for Pet Battles (duels)
    if (id == EVENT_DUEL_PET_BATTLE)
        if (PetBattle* petBattle = sPetBattleSystem->GetPlayerPetBattle(player->GetGUID()))
            if (petBattle->GetType() == PET_BATTLE_TYPE_PVP_DUEL)
            {
                player->SetControlled(true, UNIT_STATE_ROOT);

                if (petBattle->AddPreBattleCount() >= 2)
                    petBattle->StartBattle();
            }
}

template <> void PointMovementGenerator<Creature>::MovementInform(Creature* unit)
{
    if (unit->AI())
        unit->AI()->MovementInform(POINT_MOTION_TYPE, id);
}

template void PointMovementGenerator<Player>::DoInitialize(Player*);
template void PointMovementGenerator<Creature>::DoInitialize(Creature*);
template void PointMovementGenerator<Player>::DoFinalize(Player*);
template void PointMovementGenerator<Creature>::DoFinalize(Creature*);
template void PointMovementGenerator<Player>::DoReset(Player*);
template void PointMovementGenerator<Creature>::DoReset(Creature*);
template bool PointMovementGenerator<Player>::DoUpdate(Player*, uint32);
template bool PointMovementGenerator<Creature>::DoUpdate(Creature*, uint32);

void AssistanceMovementGenerator::Finalize(Unit* unit)
{
    unit->ToCreature()->SetNoCallAssistance(false);
    unit->ToCreature()->CallAssistance();
    if (unit->IsAlive())
        unit->GetMotionMaster()->MoveSeekAssistanceDistract(sWorld->getIntConfig(CONFIG_CREATURE_FAMILY_ASSISTANCE_DELAY));
}

void EffectMovementGenerator::Initialize(Unit* unit)
{
    if (unit->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        if (unit->GetMotionMaster()->GetMotionSlot(MOTION_SLOT_ACTIVE) == this)
            _reinitialized = true;
        return;
    }

    switch (_id)
    {
        case EVENT_JUMP_OR_KNOCKBACK:
            unit->AddUnitState(UNIT_STATE_JUMPING);
            unit->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_JUMP);
            break;
        case EVENT_CHARGE:
            unit->AddUnitState(UNIT_STATE_CHARGING);
            break;
        default:
            break;
    }

    MoveSplineInit init(unit);

    init.MoveTo(_x, _y, _z, _generatePath);

    if (_facing)
        init.SetFacing(_facing);
    else if (_orient != 0.0f)
        init.SetFacing(_orient);

    if (_speed != 0.0f)
        init.SetVelocity(_speed);

    if (_height != 0.0f)
        init.SetParabolic(_height, 0);

    if (_anim != AnimType::None)
        init.SetAnimation(_anim);

    if (_isFall)
        init.SetFall();

    init.SetOrientationFixed(_orientFixed);

    init.Launch();
}

bool EffectMovementGenerator::Update(Unit* unit, uint32)
{
    if (unit->HasUnitState(UNIT_STATE_NOT_MOVE))
        return true;

    bool Arrived = unit->movespline->Finalized();

    if (Arrived)
    {
        if (unit->GetMotionMaster()->GetMotionSlot(MOTION_SLOT_ACTIVE) == this)
        {
            Vector3 dest = unit->movespline->FinalDestination();

            if (unit->movespline->onTransport)
                if (TransportBase* transport = unit->GetDirectTransport())
                    transport->CalculatePassengerPosition(dest.x, dest.y, dest.z);

            if (unit->IsHovering())
                dest.z += unit->GetFloatValue(UNIT_HOVER_HEIGHT);

            bool arrivedDest = unit->GetPosition().IsInDist(dest.x, dest.y, dest.z, CONTACT_DISTANCE);
            if (arrivedDest)
                return false;

            ReInitialize(unit);

            return true;
        }

        return false;
    }
    else if (_reinitialized)
        ReInitialize(unit);

    return !Arrived;
}

void EffectMovementGenerator::Finalize(Unit* unit)
{
    switch (_id)
    {
        case EVENT_JUMP_OR_KNOCKBACK:
            unit->ClearUnitState(UNIT_STATE_JUMPING);
            break;
        case EVENT_CHARGE:
            unit->ClearUnitState(UNIT_STATE_CHARGING);
            break;
        default:
            break;
    }

    // anain - finalizowac wszystkie efect tutaj
    if (_arrivalSpellId)
        unit->CastSpell(ObjectAccessor::GetUnit(*unit, _arrivalSpellTargetGuid), _arrivalSpellId, true);

    // Reset jump/fall move data
    if (_isFall || _id == EVENT_JUMP_OR_KNOCKBACK)
    {
        unit->ResetFall();

        if (_isFall)
            if (Player* player = unit->ToPlayer())
            {
                player->SetFall(false);
                player->HandleFall(unit->GetPosition());
            }
    }

    if (unit->GetTypeId() != TYPEID_UNIT)
        return;

    // Need restore previous movement since we have no proper states system
    if (unit->IsAlive() && !unit->HasUnitState(UNIT_STATE_CONFUSED | UNIT_STATE_FLEEING))
        if (Unit* victim = unit->GetVictim())
        {
            if (unit->ToCreature() && unit->IsAIEnabled)
                unit->GetAI()->AttackStart(victim);
            else
                unit->GetMotionMaster()->MoveChase(victim);
        }

    if (unit->ToCreature()->AI())
        unit->ToCreature()->AI()->MovementInform(EFFECT_MOTION_TYPE, _id);
}

void EffectMovementGenerator::ReInitialize(Unit* unit)
{
    _reinitialized = false;

    switch (_id)
    {
        case EVENT_JUMP_OR_KNOCKBACK:
            unit->AddUnitState(UNIT_STATE_JUMPING);
            unit->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_JUMP);
            break;
        case EVENT_CHARGE:
            unit->AddUnitState(UNIT_STATE_CHARGING);
            break;
        default:
            break;
    }

    MoveSplineInit init(unit);

    init.MoveTo(_x, _y, _z, _generatePath);

    if (_facing)
        init.SetFacing(_facing);
    else if (_orient != 0.0f)
        init.SetFacing(_orient);

    if (_speed != 0.0f)
        init.SetVelocity(_speed);

    if (_height != 0.0f)
        init.SetParabolic(_height, 0);

    if (_anim != AnimType::None)
        init.SetAnimation(_anim);

    if (_isFall)
        init.SetFall();

    init.SetOrientationFixed(_orientFixed);

    init.Launch();
}
