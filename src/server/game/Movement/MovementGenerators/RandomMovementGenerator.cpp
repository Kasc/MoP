/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Creature.h"
#include "MapManager.h"
#include "RandomMovementGenerator.h"
#include "ObjectAccessor.h"
#include "Map.h"
#include "Util.h"
#include "CreatureGroups.h"
#include "MoveSplineInit.h"
#include "MoveSpline.h"

#define RUNNING_CHANCE_RANDOMMV 20                                  //will be "1 / RUNNING_CHANCE_RANDOMMV"

template<>
void RandomMovementGenerator<Creature>::_setRandomLocation(Creature* creature)
{
    if (creature->IsMovementPreventedByCasting())
    {
        creature->CastStop();
        return;
    }

    _reinitialized = false;

    float respX, respY, respZ, respO, destX, destY, destZ, travelDistZ;
    creature->GetHomePosition(respX, respY, respZ, respO);
    Map const* map = creature->GetBaseMap();

    // For 2D/3D system selection
    bool is_water_ok = creature->CanSwim();
    bool is_air_ok = creature->CanFly();

    const float angle = Math::Rand( 0.f, static_cast<float>(M_PI*2.0f) );
    const float range = Math::Rand( 0.f, wander_distance );
    const float distanceX = range * std::cos(angle);
    const float distanceY = range * std::sin(angle);

    destX = respX + distanceX;
    destY = respY + distanceY;

    // prevent invalid coordinates generation
    Trinity::NormalizeMapCoord(destX);
    Trinity::NormalizeMapCoord(destY);

    float distanceZ = 0.0f;

    if (is_air_ok || (is_water_ok && creature->IsInWater()))
    {
        travelDistZ = range / 2.0f;
        distanceZ = Math::Rand(-travelDistZ, travelDistZ);
    }
    else
    {
        travelDistZ = range >= 10.0f ? 10.0f : range;
        distanceZ = Math::Rand(0.f, travelDistZ);
    }

    destZ = respZ + distanceZ;

    LiquidData liquid_status;
    ZLiquidStatus res = map->getLiquidStatus(destX, destY, destZ, creature->GetCollisionHeight(), MAP_ALL_LIQUIDS, creature->GetTerrainSwaps(), &liquid_status);
    if (res == LIQUID_MAP_NO_WATER)
    {
        if (is_air_ok)
        {
            float levelZ = map->GetHeight(destX, destY, destZ, creature->GetPhases(), creature->GetTerrainSwaps());
            if (destZ < levelZ)
                destZ = levelZ;
        }
        else
        {
            // The fastest way to get an accurate result 90% of the time.
            // Better result can be obtained like 99% accuracy with a ray light, but the cost is too high and the code is too long.
            destZ = map->GetHeight(destX, destY, destZ, creature->GetPhases(), creature->GetTerrainSwaps(), false);

            if (std::fabs(destZ - respZ) > travelDistZ)              // Map check
            {
                // Vmap Horizontal or above
                destZ = map->GetHeight(destX, destY, respZ, creature->GetPhases(), creature->GetTerrainSwaps());

                if (std::fabs(destZ - respZ) > travelDistZ)
                {
                    // Vmap Higher
                    destZ = map->GetHeight(destX, destY, respZ + travelDistZ, creature->GetPhases(), creature->GetTerrainSwaps());

                    // let's forget this bad coords where a z cannot be find and retry at next tick
                    if (std::fabs(destZ - respZ) > travelDistZ)
                    {
                        i_nextMoveTime.Reset(Math::Rand(50, 400));
                        return;
                    }
                }
            }
        }
    }
    else
    {
        if (res == LIQUID_MAP_ABOVE_WATER)
        {
            // point above water, creature can fly - everything's ok
            // point above water, creature can't fly - check below
            if (!is_air_ok)
            {
                // point in water and can't swim - return
                // point in water, creature can't fly and can swim - check below
                if (is_water_ok)
                {
                    // destZ is liquidlevel
                    destZ = std::max(liquid_status.level, map->GetDynamicHeight(destX, destY, destZ, creature->GetPhases()));
                }
                else
                {
                    i_nextMoveTime.Reset(Math::Rand(50, 400));
                    return;
                }
            }
        }
        else if (!is_water_ok)
        {
            i_nextMoveTime.Reset(Math::Rand(50, 400));
            return;
        }
        else
        {
            if (destZ < liquid_status.depth_level)
                destZ = liquid_status.depth_level;
        }
    }

    i_nextMoveTime.Reset(Math::Rand(5000, 10000));

    creature->AddUnitState(UNIT_STATE_ROAMING_MOVE);

    MoveSplineInit init(creature);
    init.MoveTo(destX, destY, destZ);
    init.SetWalk(true);
    init.Launch();

    //Call for creature group update
    if (creature->GetFormation() && creature->GetFormation()->getLeader() == creature)
        creature->GetFormation()->LeaderMoveTo(destX, destY, destZ);
}

template<>
void RandomMovementGenerator<Creature>::DoInitialize(Creature* creature)
{
    if (!creature->IsAlive())
        return;

    creature->AddUnitState(UNIT_STATE_ROAMING | UNIT_STATE_ROAMING_MOVE);

    if (creature->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        _reinitialized = true;
        return;
    }

    if (!wander_distance)
        wander_distance = creature->GetRespawnRadius();

    _setRandomLocation(creature);
}

template<>
void RandomMovementGenerator<Creature>::DoReset(Creature* creature)
{
    DoInitialize(creature);
}

template<>
void RandomMovementGenerator<Creature>::DoFinalize(Creature* creature)
{
    creature->ClearUnitState(UNIT_STATE_ROAMING | UNIT_STATE_ROAMING_MOVE);
    creature->SetWalk(false);
}

template<>
bool RandomMovementGenerator<Creature>::DoUpdate(Creature* creature, const uint32 diff)
{
    if (!creature || !creature->IsAlive())
        return false;

    if (creature->IsMovementPreventedByCasting())
    {
        creature->CastStop();
        i_nextMoveTime.Reset(0);
        creature->ClearUnitState(UNIT_STATE_ROAMING_MOVE);
        return true;
    }

    if (creature->HasUnitState(UNIT_STATE_NOT_MOVE))
    {
        i_nextMoveTime.Reset(0);  // Expire the timer
        creature->ClearUnitState(UNIT_STATE_ROAMING_MOVE);
        return true;
    }

    if (creature->movespline->Finalized() || _reinitialized)
    {
        i_nextMoveTime.Update(diff);
        if (i_nextMoveTime.Passed())
            _setRandomLocation(creature);
    }

    return true;
}

template<>
bool RandomMovementGenerator<Creature>::GetResetPos(Creature* creature, float& x, float& y, float& z)
{
    float radius;
    creature->GetRespawnPosition(x, y, z, NULL, &radius);

    // use current if in range
    if (creature->IsWithinDist2d(x, y, radius))
        creature->GetPosition(x, y, z);

    return true;
}
