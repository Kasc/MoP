/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "MovementInfo.h"
#include "Player.h"
#include "MovementStructures.h"
#include "Unit.h"
#include "Vehicle.h"

float tangent(float x);

void MovementInfo::ReadFromPacket(WorldPacket& data, ExtraMovementStatusElement* extras /*= NULL*/)
{
    MovementStatusElements const* sequence = GetMovementStatusElementsSequence(data.GetOpcode());
    if (!sequence)
    {
        TC_LOG_ERROR("network", "Unit::ReadMovementInfo: No movement sequence found for opcode %s", GetOpcodeNameForLogging(data.GetOpcode(), false).c_str());
        return;
    }

    bool hasMovementFlags = false;
    bool hasMovementFlags2 = false;
    bool hasTimestamp = false;
    bool hasOrientation = false;
    bool hasTransportData = false;
    bool hasTransportPrevTime = false;
    bool hasTransportVehicleId = false;
    bool hasPitch = false;
    bool hasFallData = false;
    bool hasFallDirection = false;
    bool hasSplineElevation = false;
    bool hasSpline = false;
    bool heightChangeFailed = false;
    bool hasRemoteTimeValid = false;
    bool hasCounter = false;

    uint32 RemovedForcedMovementCount = 0;
    uint32 counter = 0;

    ObjectGuid mguid;
    ObjectGuid tguid;

    // Reset extra elements
    if (extras)
        extras->Reset();

    for (; *sequence != MSEEnd; ++sequence)
    {
        MovementStatusElements const& element = *sequence;

        switch (element)
        {
            case MSEHasGuidByte0:
            case MSEHasGuidByte1:
            case MSEHasGuidByte2:
            case MSEHasGuidByte3:
            case MSEHasGuidByte4:
            case MSEHasGuidByte5:
            case MSEHasGuidByte6:
            case MSEHasGuidByte7:
                data.ReadGuidMask(mguid, (element - MSEHasGuidByte0));
                break;
            case MSEHasTransportGuidByte0:
            case MSEHasTransportGuidByte1:
            case MSEHasTransportGuidByte2:
            case MSEHasTransportGuidByte3:
            case MSEHasTransportGuidByte4:
            case MSEHasTransportGuidByte5:
            case MSEHasTransportGuidByte6:
            case MSEHasTransportGuidByte7:
                if (hasTransportData)
                    data.ReadGuidMask(tguid, (element - MSEHasTransportGuidByte0));
                break;
            case MSEGuidByte0:
            case MSEGuidByte1:
            case MSEGuidByte2:
            case MSEGuidByte3:
            case MSEGuidByte4:
            case MSEGuidByte5:
            case MSEGuidByte6:
            case MSEGuidByte7:
                data.ReadGuidBytes(mguid, (element - MSEGuidByte0));
                break;
            case MSETransportGuidByte0:
            case MSETransportGuidByte1:
            case MSETransportGuidByte2:
            case MSETransportGuidByte3:
            case MSETransportGuidByte4:
            case MSETransportGuidByte5:
            case MSETransportGuidByte6:
            case MSETransportGuidByte7:
                if (hasTransportData)
                    data.ReadGuidBytes(tguid, (element - MSETransportGuidByte0));
                break;
            case MSEHasMovementFlags:
                hasMovementFlags = !data.ReadBit();
                break;
            case MSEHasMovementFlags2:
                hasMovementFlags2 = !data.ReadBit();
                break;
            case MSEHasTimestamp:
                hasTimestamp = !data.ReadBit();
                break;
            case MSEHasOrientation:
                hasOrientation = !data.ReadBit();
                break;
            case MSEHasTransportData:
                hasTransportData = data.ReadBit();
                break;
            case MSEHasTransportPrevTime:
                if (hasTransportData)
                    hasTransportPrevTime = data.ReadBit();
                break;
            case MSEHasTransportVehicleId:
                if (hasTransportData)
                    hasTransportVehicleId = data.ReadBit();
                break;
            case MSEHasPitch:
                hasPitch = !data.ReadBit();
                break;
            case MSEHasFallData:
                hasFallData = data.ReadBit();
                break;
            case MSEHasFallDirection:
                if (hasFallData)
                    hasFallDirection = data.ReadBit();
                break;
            case MSEHasSplineElevation:
                hasSplineElevation = !data.ReadBit();
                break;
            case MSEHasSpline:
                hasSpline = data.ReadBit();
                break;
            case MSEHasHeightChangeFailed:
                heightChangeFailed = data.ReadBit();
                break;
            case MSEHasRemoteTimeValid:
                hasRemoteTimeValid = data.ReadBit();
                break;
            case MSEMovementFlags:
                if (hasMovementFlags)
                    flags = data.ReadBits(30);
                break;
            case MSEMovementFlags2:
                if (hasMovementFlags2)
                    flags2 = data.ReadBits(13);
                break;
            case MSETimestamp:
                if (hasTimestamp)
                    data >> time;
                break;
            case MSEPositionX:
                data >> pos.m_positionX;
                break;
            case MSEPositionY:
                data >> pos.m_positionY;
                break;
            case MSEPositionZ:
                data >> pos.m_positionZ;
                break;
            case MSEOrientation:
                if (hasOrientation)
                    pos.SetOrientation(data.read<float>());
                break;
            case MSETransportPositionX:
                if (hasTransportData)
                    data >> transport.pos.m_positionX;
                break;
            case MSETransportPositionY:
                if (hasTransportData)
                    data >> transport.pos.m_positionY;
                break;
            case MSETransportPositionZ:
                if (hasTransportData)
                    data >> transport.pos.m_positionZ;
                break;
            case MSETransportOrientation:
                if (hasTransportData)
                    transport.pos.SetOrientation(data.read<float>());
                break;
            case MSETransportSeat:
                if (hasTransportData)
                    data >> transport.seat;
                break;
            case MSETransportTime:
                if (hasTransportData)
                    data >> transport.time;
                break;
            case MSETransportPrevTime:
                if (hasTransportData && hasTransportPrevTime)
                    data >> transport.prevTime;
                break;
            case MSETransportVehicleId:
                if (hasTransportData && hasTransportVehicleId)
                    data >> transport.vehicleId;
                break;
            case MSEPitch:
                if (hasPitch)
                    pitch = G3D::wrap(data.read<float>(), float(-M_PI), float(M_PI));
                break;
            case MSEFallTime:
                if (hasFallData)
                    data >> fall.fallTime;
                break;
            case MSEFallVerticalSpeed:
                if (hasFallData)
                    data >> fall.zspeed;
                break;
            case MSEFallCosAngle:
                if (hasFallData && hasFallDirection)
                    data >> fall.cosAngle;
                break;
            case MSEFallSinAngle:
                if (hasFallData && hasFallDirection)
                    data >> fall.sinAngle;
                break;
            case MSEFallHorizontalSpeed:
                if (hasFallData && hasFallDirection)
                    data >> fall.xyspeed;
                break;
            case MSESplineElevation:
                if (hasSplineElevation)
                    data >> splineElevation;
                break;
            case MSERemovedForcesCount:
                RemovedForcedMovementCount = data.ReadBits(22);
                break;
            case MSERemovedForces:
                for (uint32 i = 0; i < RemovedForcedMovementCount; i++)
                    forcedMovement.RemovedForcedMovements.push_back(data.read<uint32>());
                break;
            case MSEHasCounter:
                hasCounter = !data.ReadBit();
                break;
            case MSECounter:
                if (hasCounter)
                    data >> counter;
                break;
            case MSEMovementCounter:
                data >> ackIndex;
                break;
            case MSEExtraElement:
                if (extras)
                    extras->ReadNextElement(data);
                else
                    TC_LOG_ERROR("network", "Unit::ReadMovementInfo: No Extra Elemet found for opcode %s", GetOpcodeNameForLogging(data.GetOpcode(), true).c_str());
                break;
            default:
                ASSERT(PrintInvalidSequenceElement(element, __FUNCTION__));
                break;
        }
    }

    guid = mguid;
    transport.guid = tguid;
}

HackDetectionTypes MovementInfo::SanitizeFlags(Unit* target, WorldSession* session)
{
    uint32 hackTypeDetect = HACK_DETECTION_NONE;

    //! Anti-cheat checks. Please keep them in seperate if () blocks to maintain a clear overview.
    //! Might be subject to latency, so just remove improper flags.
    #define REMOVE_VIOLATING_FLAGS(check, maskToRemove) \
        if (check) \
            RemoveMovementFlag((maskToRemove));
    #define REMOVE_VIOLATING_EXTRA_FLAGS(check, maskToRemove) \
        if (check) \
            RemoveExtraMovementFlag((maskToRemove));
    #define REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT(check, maskToRemove, hackType) \
    { \
        if (check) \
        { \
            RemoveMovementFlag((maskToRemove)); \
            hackTypeDetect |= hackType; \
        } \
    }

    Player* player = target->ToPlayer();

    /*
        1. Compare flags with specific aura types.
    */

    if (player)
    {
         /*! Cannot fly if no fly auras present. Exception is being a GM.
            Note that we check for account level instead of Player::IsGameMaster() because in some
            situations it may be feasable to use .gm fly on as a GM without having .gm on,
            e.g. aerial combat.
        */
        REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT(HasMovementFlag(MOVEMENTFLAG_FLYING | MOVEMENTFLAG_CAN_FLY) && player->GetSession()->GetSecurity() == SEC_PLAYER &&
            !player->HasAuraType(SPELL_AURA_FLY) && !player->HasAuraType(SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED),
            (MOVEMENTFLAG_MASK_MOVING_FLY | MOVEMENTFLAG_CAN_FLY), HACK_DETECTION_FLYING);

        //! Cannot hover without SPELL_AURA_HOVER
        REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_HOVER) && !player->HasAuraType(SPELL_AURA_HOVER),
            MOVEMENTFLAG_HOVER);
    }

    //! Cannot walk on water without SPELL_AURA_WATER_WALK - exception: death
    REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT(HasMovementFlag(MOVEMENTFLAG_WATERWALKING) && target->IsAlive() && !target->HasAuraType(SPELL_AURA_WATER_WALK) && !target->HasAuraType(SPELL_AURA_GHOST),
        MOVEMENTFLAG_WATERWALKING, HACK_DETECTION_WATER_WALKING);

    //! Cannot feather fall without SPELL_AURA_FEATHER_FALL
    REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT(HasMovementFlag(MOVEMENTFLAG_FALLING_SLOW) && !target->HasAuraType(SPELL_AURA_FEATHER_FALL),
        MOVEMENTFLAG_FALLING_SLOW, HACK_DETECTION_FALLING_SLOW);

     /*
        2. Proccess extra flags.
    */

    //! Cannot strafe with NO_STRAFE extra flag
    REMOVE_VIOLATING_FLAGS(HasExtraMovementFlag(MOVEMENTFLAG2_NO_STRAFE) && HasMovementFlag(MOVEMENTFLAG_MASK_STRAFING),
        MOVEMENTFLAG_MASK_STRAFING);

    //! Cannot fall with NO_JUMPING extra flag
    REMOVE_VIOLATING_FLAGS(HasExtraMovementFlag(MOVEMENTFLAG2_NO_JUMPING) && HasMovementFlag(MOVEMENTFLAG_FALLING),
        MOVEMENTFLAG_FALLING);

    //! Cannot pitch with MOVEMENTFLAG2_WATERWALKING_FULL_PITCH extra flag
    REMOVE_VIOLATING_FLAGS(HasExtraMovementFlag(MOVEMENTFLAG2_WATERWALKING_FULL_PITCH) && HasMovementFlag(MOVEMENTFLAG_MASK_PITCHING),
        MOVEMENTFLAG_MASK_PITCHING);

    //! Cannot trans from swim to fly without CAN_FLY flag
    REMOVE_VIOLATING_EXTRA_FLAGS(HasExtraMovementFlag(MOVEMENTFLAG2_CAN_SWIM_TO_FLY_TRANS) && !HasMovementFlag(MOVEMENTFLAG_CAN_FLY),
        MOVEMENTFLAG2_CAN_SWIM_TO_FLY_TRANS);

    //! Cannot turn while falling without SPELL_AURA_CAN_TURN_WHILE_FALLING
    REMOVE_VIOLATING_EXTRA_FLAGS(HasExtraMovementFlag(MOVEMENTFLAG2_CAN_TURN_WHILE_FALLING) && !target->HasAuraType(SPELL_AURA_TURN_WHILE_FALLING),
        MOVEMENTFLAG2_CAN_TURN_WHILE_FALLING);

    //! Interpolated flags are only player flags
    REMOVE_VIOLATING_EXTRA_FLAGS(HasExtraMovementFlag(MOVEMENTFLAG2_INTERPOLATED_MASK) && !player,
        MOVEMENTFLAG2_INTERPOLATED_MASK);

    /*
        3. Compare flags with other flags.
    */

    //! Cannot move forwards and backwards at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_FORWARD) && HasMovementFlag(MOVEMENTFLAG_BACKWARD),
        MOVEMENTFLAG_FORWARD | MOVEMENTFLAG_BACKWARD);

    //! Cannot strafe left and right at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_STRAFE_LEFT) && HasMovementFlag(MOVEMENTFLAG_STRAFE_RIGHT),
        MOVEMENTFLAG_STRAFE_LEFT | MOVEMENTFLAG_STRAFE_RIGHT);

    //! Cannot turn left and right at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_LEFT) && HasMovementFlag(MOVEMENTFLAG_RIGHT),
        MOVEMENTFLAG_LEFT | MOVEMENTFLAG_RIGHT);

    //! Cannot pitch up and down at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PITCH_UP) && HasMovementFlag(MOVEMENTFLAG_PITCH_DOWN),
        MOVEMENTFLAG_PITCH_UP | MOVEMENTFLAG_PITCH_DOWN);

    //! Cannot root and move at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_ROOT) && HasMovementFlag(MOVEMENTFLAG_MASK_MOVING),
        MOVEMENTFLAG_MASK_MOVING);

    //! Cannot ascend and descend at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_ASCENDING) && HasMovementFlag(MOVEMENTFLAG_DESCENDING),
        MOVEMENTFLAG_ASCENDING | MOVEMENTFLAG_DESCENDING);

    //! Flying flag is exclusive with can fly flag
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_FLYING) && !HasMovementFlag(MOVEMENTFLAG_CAN_FLY),
        MOVEMENTFLAG_FLYING);

     /*
        4. Compare flags with misc stuff
    */

    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_MASK_PITCHING) && !pitch,
        MOVEMENTFLAG_MASK_PITCHING);

    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_FALLING_FAR) && !fall.fallTime,
        MOVEMENTFLAG_FALLING_FAR);

    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_FALLING) && !fall.fallTime && !fall.cosAngle && !fall.sinAngle,
        MOVEMENTFLAG_FALLING);

    REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT(HasMovementFlag(MOVEMENTFLAG_FALLING) && fall.zspeed != 0.0f && target->GetLastVerticalSpeed() != 0.0f && fall.zspeed != target->GetLastVerticalSpeed(),
        MOVEMENTFLAG_FALLING, HACK_DETECTION_LOW_GRAVITY);

    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_SPLINE_ELEVATION) && (!splineElevation || G3D::fuzzyEq(splineElevation, 0.0f)),
        MOVEMENTFLAG_SPLINE_ELEVATION);

    REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT(HasMovementFlag(MOVEMENTFLAG_DISABLE_GRAVITY) && target->IsGravityEnabled(),
        MOVEMENTFLAG_DISABLE_GRAVITY, HACK_DETECTION_NO_FALLING);

    // Misc detect types
    if (session)
    {
        // HACK_DETECTION_WALL_CLIMBING
        if (HasMovementFlag(MOVEMENTFLAG_SPLINE_ELEVATION))
        {
            Position playerPos = target->GetPosition();

            float deltaZ = fabs(playerPos.GetPositionZ() - pos.GetPositionZ());
            float deltaXY = pos.GetExactDist2d(&playerPos);

            // Prevent divide by 0
            if (!G3D::fuzzyEq(deltaXY, 0.f))
            {
                float angle = Position::NormalizeOrientation(tangent(deltaZ / deltaXY));

                if (angle > 1.9f)
                {
                    RemoveMovementFlag(MOVEMENTFLAG_SPLINE_ELEVATION);
                    hackTypeDetect |= HACK_DETECTION_WALL_CLIMBING;
                }
            }
        }

        // HACK_DETECTION_SPEED (exclude Falling)
        {
            uint32 distance = static_cast<uint32>(pos.GetExactDist2d(target->GetPosition()));

            UnitMoveType moveType = MOVE_RUN;

            // we need to know HOW is the player moving
            if (target->HasMovementFlag(MOVEMENTFLAG_SWIMMING))
                moveType = MOVE_SWIM;
            else if (target->HasMovementFlag(MOVEMENTFLAG_FLYING | MOVEMENTFLAG_DISABLE_GRAVITY))
                moveType = MOVE_FLIGHT;
            else if (target->HasMovementFlag(MOVEMENTFLAG_WALKING))
                moveType = MOVE_WALK;

            // how many yards the player can do in one sec.
            float speed = target->GetSpeed(moveType) + fall.xyspeed;
            if (!speed && target->GetVehicle())
                speed = target->GetVehicleBase()->GetSpeed(moveType);

            // did the 1.0 factor to accept a margin of tolerance
            uint32 speedRate = static_cast<uint32>(speed * 1.0f);

            // how long the player took to move to here.
            uint32 timeDiff = getMSTimeDiff(target->GetMovementTimer(), time);
            if (!timeDiff)
                timeDiff = 1;

            // this is the distance doable by the player in 1 sec, using the time done to move to this point.
            uint32 clientSpeedRate = distance * 1000 / timeDiff;

            // adjust latency of player that moved this target
            uint32 latency = session->GetLatency();

            if (latency < 1000)
                if (clientSpeedRate > speedRate)
                {
                    RemoveMovementFlag(MOVEMENTFLAG_MASK_MOVING);
                    hackTypeDetect |= HACK_DETECTION_SPEED;
                }
        }
    }

    //! Cannot have elevated position z without MOVEMENTFLAG_SPLINE_ELEVATION flag
    if (!HasMovementFlag(MOVEMENTFLAG_SPLINE_ELEVATION) && !G3D::fuzzyEq(splineElevation, 0.0f))
    {
        target->GetMovementPosition().Relocate(pos.GetPositionX(), pos.GetPositionY(), splineElevation);
        splineElevation = 0.0f;
    }

     /*
        5. Proccess falling flags.
    */

    //! Cannot fall with non-falling flags at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_MASK_NOT_FALLING) && HasMovementFlag(MOVEMENTFLAG_FALLING),
        MOVEMENTFLAG_FALLING);

    //! Cannot fall far without falling flag at the same time
    REMOVE_VIOLATING_FLAGS(!HasMovementFlag(MOVEMENTFLAG_FALLING) && HasMovementFlag(MOVEMENTFLAG_FALLING_FAR),
        MOVEMENTFLAG_FALLING_FAR);

    //! Cannot fall far with non-falling far flags at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_MASK_NOT_FALLING_FAR) && HasMovementFlag(MOVEMENTFLAG_FALLING_FAR),
        MOVEMENTFLAG_FALLING_FAR);

     /*
        6. Compare pending flags
    */

    //! Pending flags are exclusive with falling flag
    REMOVE_VIOLATING_FLAGS(!HasMovementFlag(MOVEMENTFLAG_MASK_FALLING) && HasMovementFlag(MOVEMENTFLAG_MASK_PENDING),
        MOVEMENTFLAG_MASK_PENDING);

    //! Pending stop is exclusive with forward/backward mask
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_STOP) && !HasMovementFlag(MOVEMENTFLAG_MASK_FORWARD | MOVEMENTFLAG_MASK_BACKWARD),
        MOVEMENTFLAG_MASK_FALLING | MOVEMENTFLAG_PENDING_STOP);

    //! Pending strafe stop is exclusive with strafe left/right mask
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_STRAFE_STOP) && !HasMovementFlag(MOVEMENTFLAG_MASK_STRAFE_LEFT | MOVEMENTFLAG_MASK_STRAFE_RIGHT),
        MOVEMENTFLAG_MASK_FALLING | MOVEMENTFLAG_PENDING_STRAFE_STOP);

    //! Cannot move forward and have pending forward move at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_FORWARD) && HasMovementFlag(MOVEMENTFLAG_FORWARD),
        MOVEMENTFLAG_MASK_FALLING | MOVEMENTFLAG_PENDING_FORWARD);

    //! Cannot move backward and have pending backward move at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_BACKWARD) && HasMovementFlag(MOVEMENTFLAG_BACKWARD),
        MOVEMENTFLAG_BACKWARD);

    //! Cannot strafe left and have pending strafe left move at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_STRAFE_LEFT) && HasMovementFlag(MOVEMENTFLAG_STRAFE_LEFT),
        MOVEMENTFLAG_STRAFE_LEFT);

    //! Cannot strafe right and have pending strafe right move at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_STRAFE_RIGHT) && HasMovementFlag(MOVEMENTFLAG_STRAFE_RIGHT),
        MOVEMENTFLAG_STRAFE_RIGHT);

    //! Cannot root and have pending root at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_ROOT) && HasMovementFlag(MOVEMENTFLAG_ROOT),
        MOVEMENTFLAG_ROOT);

    //! Cannot root and have pending move at the same time
    REMOVE_VIOLATING_FLAGS(HasMovementFlag(MOVEMENTFLAG_PENDING_ROOT) && HasMovementFlag(MOVEMENTFLAG_MASK_PENDING_MOVING),
        MOVEMENTFLAG_MASK_PENDING_MOVING);

    #undef REMOVE_VIOLATING_FLAGS_WITH_HACK_DETECT
    #undef REMOVE_VIOLATING_EXTRA_FLAGS
    #undef REMOVE_VIOLATING_FLAGS

    if (session)
        return HackDetectionTypes(hackTypeDetect);

    return HACK_DETECTION_NONE;
}

char const* HackTypeNames[] =
{
    STRINGIZE(HACK_SPEED),
    STRINGIZE(HACK_FLYING),
    STRINGIZE(HACK_NO_FALLING),
    STRINGIZE(HACK_FALLING_SLOW),
    STRINGIZE(HACK_WATER_WALKING),
    STRINGIZE(HACK_WALL_CLIMBING),
    STRINGIZE(HACK_LOW_GRAVITY),
    STRINGIZE(HACK_CLIPPING)
};

void MovementInfo::GetHacksDetected(std::ostringstream& message, uint32 hacks)
{
    message << "Detected hacks: |cffffcc00 ";

    for (uint8 i = 0; hacks != 0; hacks >>= 1, ++i)
        if ((hacks & 1) != 0)
            message << HackTypeNames[i] << " ";

    message << "|r";
}
