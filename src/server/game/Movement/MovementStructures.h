/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_MOVEMENT_STRUCTURES_H
#define SF_MOVEMENT_STRUCTURES_H

#include "Opcodes.h"
#include "Object.h"
#include "Unit.h"

class ByteBuffer;

enum MovementStatusElements
{
    MSERemovedForcesCount,
    MSEHasCounter,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte2,
    MSEHasGuidByte3,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte7,
    MSEHasMovementFlags,
    MSEHasMovementFlags2,
    MSEHasTimestamp,
    MSEHasOrientation,
    MSEHasTransportData,
    MSEHasTransportGuidByte0,
    MSEHasTransportGuidByte1,
    MSEHasTransportGuidByte2,
    MSEHasTransportGuidByte3,
    MSEHasTransportGuidByte4,
    MSEHasTransportGuidByte5,
    MSEHasTransportGuidByte6,
    MSEHasTransportGuidByte7,
    MSEHasTransportPrevTime,
    MSEHasTransportVehicleId,
    MSEHasPitch,
    MSEHasFallData,
    MSEHasFallDirection,
    MSEHasSplineElevation,
    MSEHasSpline,
    MSEHasHeightChangeFailed,
    MSEHasRemoteTimeValid,

    MSERemovedForces,
    MSEMovementCounter,
    MSECounter,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEMovementFlags,
    MSEMovementFlags2,
    MSETimestamp,
    MSEPositionX,
    MSEPositionY,
    MSEPositionZ,
    MSEOrientation,
    MSETransportGuidByte0,
    MSETransportGuidByte1,
    MSETransportGuidByte2,
    MSETransportGuidByte3,
    MSETransportGuidByte4,
    MSETransportGuidByte5,
    MSETransportGuidByte6,
    MSETransportGuidByte7,
    MSETransportPositionX,
    MSETransportPositionY,
    MSETransportPositionZ,
    MSETransportOrientation,
    MSETransportSeat,
    MSETransportTime,
    MSETransportPrevTime,
    MSETransportVehicleId,
    MSEPitch,
    MSEFallTime,
    MSEFallVerticalSpeed,
    MSEFallCosAngle,
    MSEFallSinAngle,
    MSEFallHorizontalSpeed,
    MSESplineElevation,

    // Special
    MSEEnd,             // marks end of parsing
    MSEExtraElement,    // Used to signalize reading into ExtraMovementStatusElement, element sequence inside it is declared as separate array
                        // Allowed internal elements are: GUID markers (not transport), MSEExtraFloat, MSEExtraInt8, MSEExtraInt32, MSEExtra2Bits, MSEExtra19Bits

    // Extra elements
    MSEExtraFloat,
    MSEExtraInt8,
    MSEExtraInt32,
    MSEExtraBits2,

    // These extras are only for collision height packets - only write
    MSEExtraExclusiveBit,
    MSEExtraExclusiveData
};

#define MAX_EXTRA_INDEX 4

struct ExtraMovementStatusElementsData
{
    ExtraMovementStatusElementsData() : byteData(0), bitsData(0), exclusiveData(0)
    {
        memset(&intData, 0, sizeof(intData));
        memset(&floatData, 0.0f, sizeof(floatData));
    }

    void Reset()
    {
        guid.Clear();
        byteData = 0;
        bitsData = 0;
        exclusiveData = 0;
        memset(&intData, 0, sizeof(intData));
        memset(&floatData, 0.0f, sizeof(floatData));
    }

    ObjectGuid guid;
    int8 byteData;
    int32 bitsData;
    int32 intData[MAX_EXTRA_INDEX];
    float floatData[MAX_EXTRA_INDEX];
    int32 exclusiveData;
};

class ExtraMovementStatusElement
{
    public:
        ExtraMovementStatusElement() : _elements(nullptr), _index(0), _floatIndex(0), _intIndex(0) { }

        ExtraMovementStatusElement(MovementStatusElements const* elements) : _elements(elements), _index(0), _floatIndex(0), _intIndex(0)
        {
            _data.Reset();
        }

        void ReadNextElement(ByteBuffer& packet);
        void WriteNextElement(ByteBuffer& packet);

        void SetElements(MovementStatusElements const* elements)
        {
            _elements = elements;
        }

        void SetData(ExtraMovementStatusElementsData& data)
        {
            _data = data;
        }

        ExtraMovementStatusElementsData const& GetData() const
        {
            return _data;
        }

        void Reset()
        {
            _index = 0;
            _floatIndex = 0;
            _intIndex = 0;
        }

    private:
        MovementStatusElements const* _elements;
        ExtraMovementStatusElementsData _data;
        uint32 _index;
        uint32 _floatIndex;
        uint32 _intIndex;
};

bool PrintInvalidSequenceElement(MovementStatusElements element, char const* function);

MovementStatusElements const* GetMovementStatusElementsSequence(Opcodes opcode);
void GetExtraMovementStatusElements(Opcodes opcode, MoveStateChange const& stateChange, ExtraMovementStatusElement& extra);

#endif
