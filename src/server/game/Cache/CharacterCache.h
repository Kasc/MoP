/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CharacterCache_h__
#define CharacterCache_h__

#include "Define.h"
#include "ObjectGuid.h"
#include <string>

#include "Singleton/Singleton.hpp"

struct CharacterCacheEntry
{
    CharacterCacheEntry() : AccountId(0), Class(0), Race(0), Gender(0), Level(0), GuildId(0), IsDeleted(false) { }

    ObjectGuid Guid;
    std::string Name;
    uint32 AccountId;
    uint8 Class;
    uint8 Race;
    uint8 Gender;
    uint8 Level;
    uint32 GuildId;
    bool IsDeleted;
};

class CharacterCache
{
    public:
        CharacterCache();
        ~CharacterCache();

        void LoadCharacterCacheStorage();
        void AddCharacterCacheEntry(ObjectGuid const& guid, uint32 accountId, std::string const& name, uint8 gender, uint8 race, uint8 playerClass, uint8 level, bool deleted = false);
        void DeleteCharacterCacheEntry(ObjectGuid const& guid, std::string const& name);

        void UpdateCharacterData(ObjectGuid const& guid, std::string const& name, uint8* gender = nullptr, uint8* race = nullptr);
        void UpdateCharacterLevel(ObjectGuid const& guid, uint8 level);
        void UpdateCharacterAccountId(ObjectGuid const& guid, uint32 accountId);
        void UpdateCharacterGuildId(ObjectGuid const& guid, uint32 guildId);
        void UpdateCharacterDeleted(ObjectGuid const& guid, bool deleted, std::string const* name = nullptr);

        bool HasCharacterCacheEntry(ObjectGuid const& guid) const;
        CharacterCacheEntry const* GetCharacterCacheByGuid(ObjectGuid const& guid) const;
        CharacterCacheEntry const* GetCharacterCacheByName(std::string const& name) const;

        ObjectGuid GetCharacterGuidByName(std::string const& name) const;
        bool GetCharacterNameByGuid(ObjectGuid guid, std::string& name) const;
        uint32 GetCharacterTeamByGuid(ObjectGuid guid) const;
        uint32 GetCharacterAccountIdByGuid(ObjectGuid guid) const;
        uint32 GetCharacterAccountIdByName(std::string const& name) const;
        uint8 GetCharacterLevelByGuid(ObjectGuid guid) const;
        uint32 GetCharacterGuildIdByGuid(ObjectGuid guid) const;
};

#define sCharacterCache Tod::Singleton<CharacterCache>::GetSingleton()

#endif // CharacterCache_h__
