/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "LootMgr.h"
#include "Log.h"
#include "ObjectMgr.h"
#include "World.h"
#include "Util.h"
#include "SharedDefines.h"
#include "SpellMgr.h"
#include "SpellInfo.h"
#include "Group.h"
#include "GroupMgr.h"
#include "Player.h"
#include "Containers.h"
#include "Battleground.h"
#include "SpellAuraEffects.h"

static Rates const qualityToRate[MAX_ITEM_QUALITY] =
{
    RATE_DROP_ITEM_POOR,                                    // ITEM_QUALITY_POOR
    RATE_DROP_ITEM_NORMAL,                                  // ITEM_QUALITY_NORMAL
    RATE_DROP_ITEM_UNCOMMON,                                // ITEM_QUALITY_UNCOMMON
    RATE_DROP_ITEM_RARE,                                    // ITEM_QUALITY_RARE
    RATE_DROP_ITEM_EPIC,                                    // ITEM_QUALITY_EPIC
    RATE_DROP_ITEM_LEGENDARY,                               // ITEM_QUALITY_LEGENDARY
    RATE_DROP_ITEM_ARTIFACT,                                // ITEM_QUALITY_ARTIFACT
};

LootStore LootTemplates_Creature("creature_loot_template",           "creature entry",                  true);
LootStore LootTemplates_Disenchant("disenchant_loot_template",       "item disenchant id",              true);
LootStore LootTemplates_Fishing("fishing_loot_template",             "area id",                         true);
LootStore LootTemplates_Gameobject("gameobject_loot_template",       "gameobject entry",                true);
LootStore LootTemplates_Item("item_loot_template",                   "item entry",                      true);
LootStore LootTemplates_Mail("mail_loot_template",                   "mail template id",                false);
LootStore LootTemplates_Milling("milling_loot_template",             "item entry (herb)",               true);
LootStore LootTemplates_Pickpocketing("pickpocketing_loot_template", "creature pickpocket lootid",      true);
LootStore LootTemplates_Prospecting("prospecting_loot_template",     "item entry (ore)",                true);
LootStore LootTemplates_Reference("reference_loot_template",         "reference id",                    false);
LootStore LootTemplates_Skinning("skinning_loot_template",           "creature skinning id",            true);
LootStore LootTemplates_Spell("spell_loot_template",                 "spell id (random item creating)", false);

// Selects invalid loot items to be removed from group possible entries (before rolling)
struct LootGroupInvalidSelector : public std::unary_function<LootStoreItem*, bool>
{
    explicit LootGroupInvalidSelector(Loot const& loot, uint16 lootMode) : _loot(loot), _lootMode(lootMode) { }

    bool operator()(LootStoreItem* item) const
    {
        if (!(item->LootMode & _lootMode))
            return true;

        uint8 foundDuplicates = 0;
        for (std::vector<LootItem>::const_iterator itr = _loot.Items.begin(); itr != _loot.Items.end(); ++itr)
            if (itr->ItemId == item->ItemId)
                if (++foundDuplicates == _loot.MaxDuplicates)
                    return true;

        return false;
    }

private:
    Loot const& _loot;
    uint16 _lootMode;
};

class LootTemplate::LootGroup                               // A set of loot definitions for items (refs are not allowed)
{
    public:
        LootGroup() { }
        ~LootGroup();

        void AddEntry(LootStoreItem* item);                 // Adds an entry to the group (at loading stage)
        bool HasQuestDrop() const;                          // True if group includes at least 1 quest drop entry
        bool HasQuestDropForPlayer(Player const* player) const;
                                                            // The same for active quests of the player
        void Process(Loot& loot, uint16 lootMode) const;    // Rolls an item from the group (if any) and adds the item to the loot
        float RawTotalChance() const;                       // Overall chance for the group (without equal chanced items)
        float TotalChance() const;                          // Overall chance for the group

        void Verify(LootStore const& lootstore, uint32 id, uint8 group_id) const;
        void CheckLootRefs(LootTemplateMap const& store, LootIdSet* ref_set) const;
        LootStoreItemList* GetExplicitlyChancedItemList() { return &ExplicitlyChanced; }
        LootStoreItemList* GetEqualChancedItemList() { return &EqualChanced; }
        void CopyConditions(ConditionContainer conditions);
    private:
        LootStoreItemList ExplicitlyChanced;                // Entries with chances defined in DB
        LootStoreItemList EqualChanced;                     // Zero chances - every entry takes the same chance

        LootStoreItem const* Roll(Loot& loot, uint16 lootMode) const;   // Rolls an item from the group, returns NULL if all miss their chances

        // This class must never be copied - storing pointers
        LootGroup(LootGroup const&);
};

//Remove all data and free all memory
void LootStore::Clear()
{
    for (LootTemplateMap::const_iterator itr = lootTemplates.begin(); itr != lootTemplates.end(); ++itr)
        delete itr->second;
    lootTemplates.clear();
}

// Checks validity of the loot store
// Actual checks are done within LootTemplate::Verify() which is called for every template
void LootStore::Verify() const
{
    for (LootTemplateMap::const_iterator i = lootTemplates.begin(); i != lootTemplates.end(); ++i)
        i->second->Verify(*this, i->first);
}

// Loads a *_loot_template DB table into loot store
// All checks of the loaded template are called from here, no error reports at loot generation required
uint32 LootStore::LoadLootTable()
{
    LootTemplateMap::const_iterator tab;

    // Clearing store (for reloading case)
    Clear();

    //                                                  0     1    2          3       4              5         6        7         8
    QueryResult result = WorldDatabase.PQuery("SELECT Entry, Item, Reference, Chance, QuestRequired, LootMode, GroupId, MinCount, MaxCount FROM %s", GetName());

    if (!result)
        return 0;

    uint32 count = 0;

    do
    {
        Field* fields = result->Fetch();

        uint32  entry               = fields[0].GetUInt32();
        int32   itemId              = fields[1].GetInt32();
        uint32  reference           = fields[2].GetUInt32();
        float   chance              = fields[3].GetFloat();
        bool    needsquest          = fields[4].GetBool();
        uint16  lootmode            = fields[5].GetUInt16();
        uint8   groupid             = fields[6].GetUInt8();
        uint8   mincount            = fields[7].GetUInt8();
        uint8   maxcount            = fields[8].GetUInt8();

        uint8 type = itemId < 0 ? LOOT_ITEM_TYPE_CURRENCY : LOOT_ITEM_TYPE_ITEM;
        uint32 item = abs(itemId);

        if (type == LOOT_ITEM_TYPE_ITEM && maxcount > std::numeric_limits<uint8>::max())
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' entry %d item %d: maxcount value (%u) to large. must be less %u - skipped", GetName(), entry, item, maxcount, std::numeric_limits<uint8>::max());
            continue;                                   // error already printed to log/console.
        }

        if (groupid >= 1 << 7)                                     // it stored in 7 bit field
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: GroupId (%u) must be less %u - skipped", GetName(), entry, item, groupid, 1 << 7);
            return 0;
        }

        LootStoreItem* storeitem = new LootStoreItem(item, type, reference, chance, needsquest, lootmode, groupid, mincount, maxcount);

        if (!storeitem->IsValid(*this, entry))            // Validity checks
        {
            delete storeitem;
            continue;
        }

        // Looking for the template of the entry
                                                         // often entries are put together
        if (lootTemplates.empty() || tab->first != entry)
        {
            // Searching the template (in case template Id changed)
            tab = lootTemplates.find(entry);
            if (tab == lootTemplates.end())
            {
                std::pair< LootTemplateMap::iterator, bool > pr = lootTemplates.insert(LootTemplateMap::value_type(entry, new LootTemplate()));
                tab = pr.first;
            }
        }
        // else is empty - template Id and iter are the same
        // finally iter refers to already existed or just created <entry, LootTemplate>

        // Adds current row to the template
        tab->second->AddEntry(storeitem);
        ++count;
    }
    while (result->NextRow());

    Verify();                                           // Checks validity of the loot store

    return count;
}

bool LootStore::HaveQuestLootFor(uint32 loot_id) const
{
    LootTemplateMap::const_iterator itr = lootTemplates.find(loot_id);
    if (itr == lootTemplates.end())
        return false;

    // scan loot for quest items
    return itr->second->HasQuestDrop(lootTemplates);
}

bool LootStore::HaveQuestLootForPlayer(uint32 loot_id, Player* player) const
{
    LootTemplateMap::const_iterator tab = lootTemplates.find(loot_id);
    if (tab != lootTemplates.end())
        if (tab->second->HasQuestDropForPlayer(lootTemplates, player))
            return true;

    return false;
}

void LootStore::ResetConditions()
{
    for (LootTemplateMap::iterator itr = lootTemplates.begin(); itr != lootTemplates.end(); ++itr)
    {
        ConditionContainer empty;
        itr->second->CopyConditions(empty);
    }
}

LootTemplate const* LootStore::GetLootFor(uint32 loot_id) const
{
    LootTemplateMap::const_iterator tab = lootTemplates.find(loot_id);

    if (tab == lootTemplates.end())
        return NULL;

    return tab->second;
}

LootTemplate* LootStore::GetLootForConditionFill(uint32 loot_id)
{
    LootTemplateMap::iterator tab = lootTemplates.find(loot_id);

    if (tab == lootTemplates.end())
        return NULL;

    return tab->second;
}

uint32 LootStore::LoadAndCollectLootIds(LootIdSet& lootIdSet)
{
    uint32 count = LoadLootTable();

    for (LootTemplateMap::const_iterator tab = lootTemplates.begin(); tab != lootTemplates.end(); ++tab)
        lootIdSet.insert(tab->first);

    return count;
}

void LootStore::CheckLootRefs(LootIdSet* ref_set) const
{
    for (LootTemplateMap::const_iterator ltItr = lootTemplates.begin(); ltItr != lootTemplates.end(); ++ltItr)
        ltItr->second->CheckLootRefs(lootTemplates, ref_set);
}

void LootStore::ReportUnusedIds(LootIdSet const& lootIdSet) const
{
    // all still listed ids isn't referenced
    for (LootIdSet::const_iterator itr = lootIdSet.begin(); itr != lootIdSet.end(); ++itr)
        TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d isn't %s and not referenced from loot, and thus useless.", GetName(), *itr, GetEntryName());
}

void LootStore::ReportNonExistingId(uint32 lootId) const
{
    TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d does not exist", GetName(), lootId);
}

void LootStore::ReportNonExistingId(uint32 lootId, const char* ownerType, uint32 ownerId) const
{
    TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d does not exist but it is used by %s %d", GetName(), lootId, ownerType, ownerId);
}

//
// --------- LootStoreItem ---------
//

// Checks if the entry (quest, non-quest, reference) takes it's chance (at loot generation)
// RATE_DROP_ITEMS is no longer used for all types of entries
bool LootStoreItem::Roll(bool rate) const
{
    if (Chance >= 100.0f)
        return true;

    if (Reference > 0)                                   // reference case
        return Math::RollUnder(Chance* (rate ? sWorld->getRate(RATE_DROP_ITEM_REFERENCED) : 1.0f));

    if (Type == LOOT_ITEM_TYPE_ITEM)
    {
        ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(ItemId);
        float qualityModifier = pProto && rate ? sWorld->getRate(qualityToRate[pProto->GetQuality()]) : 1.0f;
        return Math::RollUnder(Chance * qualityModifier);
    }
    else if (Type == LOOT_ITEM_TYPE_CURRENCY)
        return Math::RollUnder(Chance);

    return false;
}

// Checks correctness of values
bool LootStoreItem::IsValid(LootStore const& store, uint32 entry) const
{
    if (GroupId && Type == LOOT_ITEM_TYPE_CURRENCY)
    {
        TC_LOG_ERROR("sql.sql", "Table '%s' entry %d currency %d: group is set, but currencies must not have group - skipped", store.GetName(), entry, ItemId, GroupId, 1 << 7);
        return false;
    }

    if (MinCount == 0)
    {
        TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: wrong MinCount (%d) - skipped", store.GetName(), entry, ItemId, MinCount);
        return false;
    }

    if (Reference == 0)                                      // item (quest or non-quest) entry, maybe grouped
    {
        if (Type == LOOT_ITEM_TYPE_ITEM)
        {
            ItemTemplate const* proto = sObjectMgr->GetItemTemplate(ItemId);
            if (!proto)
            {
                TC_LOG_ERROR("sql.sql", "Table '%s' entry %d item %d: item entry not listed in `item_template` - skipped", store.GetName(), entry, ItemId);
                return false;
            }
        }
        else if (Type == LOOT_ITEM_TYPE_CURRENCY)
        {
            CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(ItemId);
            if (!currency)
            {
                TC_LOG_ERROR("sql.sql", "Table '%s' entry %d: currency entry %u not exists - skipped", store.GetName(), entry, ItemId);
                return false;
            }
        }
        else
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' entry %d: has unknown item %u with type %u - skipped", store.GetName(), entry, ItemId, Type);
            return false;
        }

        if (Chance == 0 && GroupId == 0)                     // Zero chance is allowed for grouped entries only
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: equal-chanced grouped entry, but group not defined - skipped", store.GetName(), entry, ItemId);
            return false;
        }

        if (Chance != 0 && Chance < 0.000001f)             // loot with low chance
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: low chance (%f) - skipped",
                store.GetName(), entry, ItemId, Chance);
            return false;
        }

        if (MaxCount < MinCount)                       // wrong max count
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: MaxCount (%u) less that MinCount (%i) - skipped", store.GetName(), entry, ItemId, int32(MaxCount), MinCount);
            return false;
        }
    }
    else                                                    // if Reference loot
    {
        if (NeedsQuest)
            TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: quest required will be ignored", store.GetName(), entry, ItemId);
        else if (Chance == 0)                              // no chance for the Reference
        {
            TC_LOG_ERROR("sql.sql", "Table '%s' Entry %d Item %d: zero chance is specified for a Reference, skipped", store.GetName(), entry, ItemId);
            return false;
        }
    }
    return true;                                            // Referenced template existence is checked at whole store level
}

//
// --------- LootItem ---------
//

// Constructor, copies most fields from LootStoreItem and generates random count
LootItem::LootItem(LootStoreItem const& li)
{
    ItemId      = li.ItemId;
    Type        = li.Type;
    conditions  = li.conditions;
    NeedsQuest  = li.NeedsQuest;

    IsCurrency = Type == LOOT_ITEM_TYPE_CURRENCY;

    if (IsCurrency)
    {
        FreeForAll = false;
        FollowLootRules = false;
        RandomSuffix = 0;
        RandomPropertyId = 0;
        UpgradeId = 0;
    }
    else
    {
        ItemTemplate const* proto = sObjectMgr->GetItemTemplate(ItemId);
        FreeForAll = proto && (proto->GetFlags() & ITEM_PROTO_FLAG_MULTI_DROP);
        FollowLootRules = proto && (proto->FlagsCu & ITEM_FLAGS_CU_FOLLOW_LOOT_RULES);
        RandomSuffix = GenerateEnchSuffixFactor(ItemId);
        RandomPropertyId = Item::GenerateItemRandomPropertyId(ItemId);
        UpgradeId = sDB2Manager->GetRulesetItemUpgrade(ItemId);
    }

    Count = 0;
    IsLooted = 0;
    IsBlocked = 0;
    IsUnderthreshold = 0;
    IsCounted = 0;
    CanSave = true;
    AlreadyAskedForRoll = false;
}

// Basic checks for player/item compatibility - if false no chance to see the item in the loot
bool LootItem::AllowedForPlayer(Player const* player) const
{
    // DB conditions check
    if (!sConditionMgr->IsObjectMeetToConditions(const_cast<Player*>(player), conditions))
        return false;

    if (Type == LOOT_ITEM_TYPE_ITEM)
    {
        ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(ItemId);
        if (!pProto)
            return false;

        // not show loot for players without profession or those who already know the recipe
        if ((pProto->GetFlags() & ITEM_PROTO_FLAG_HIDE_UNUSABLE_RECIPE) && (!player->HasSkill(pProto->GetRequiredSkill()) || player->HasSpell(pProto->Effects[1]->SpellID)))
            return false;

        // not show loot for not own team
        if ((pProto->GetFlags2() & ITEM_PROTO_FLAG2_FACTION_HORDE) && player->GetTeam() != HORDE)
            return false;

        if ((pProto->GetFlags2() & ITEM_PROTO_FLAG2_FACTION_ALLIANCE) && player->GetTeam() != ALLIANCE)
            return false;

        // check quest requirements
        if (!(pProto->FlagsCu & ITEM_FLAGS_CU_IGNORE_QUEST_STATUS) && ((NeedsQuest || (pProto->GetStartQuest() && player->GetQuestStatus(pProto->GetStartQuest()) != QUEST_STATUS_NONE)) && !player->HasQuestForItem(ItemId)))
            return false;

        // Don't show bind-when-picked-up unique items if player already has the maximum allowed quantity.
        if (pProto->GetBonding() == BIND_ON_ACQUIRE && pProto->GetMaxCount() && player->GetItemCount(pProto->GetId(), true) >= pProto->GetMaxCount())
            return false;
    }
    else if (Type == LOOT_ITEM_TYPE_CURRENCY)
    {
        CurrencyTypesEntry const * currency = sCurrencyTypesStore.LookupEntry(ItemId);
        if (!currency)
            return false;

        if (!player->IsGameMaster())
        {
            if (currency->CategoryID == CURRENCY_CATEGORY_META_CONQUEST)
                return false;

            if (currency->CategoryID == CURRENCY_CATEGORY_ARCHAEOLOGY && !player->HasSkill(SKILL_ARCHAEOLOGY))
                return false;
        }
    }

    return true;
}

void LootItem::AddAllowedLooter(const Player* player)
{
    allowedGUIDs.insert(player->GetGUID().GetCounter());
}

//
// --------- Loot ---------
//

Loot* Loot::CreatePickPocketLoot(Creature* creature, Player* looter)
{
    creature->StartPickPocketRefillTimer();

    Loot* loot = new Loot(creature, looter, LOOT_PICKPOCKETING);

    if (uint32 lootid = creature->GetCreatureTemplate()->pickpocketLootId)
        loot->FillLoot(lootid, LootTemplates_Pickpocketing, looter, true);

    // Generate extra money for pick pocket loot
    const uint32 a = Math::Rand(0, creature->GetLevel() / 2);
    const uint32 b = Math::Rand(0, looter->GetLevel() / 2);
    loot->Gold = uint32(10 * (a + b) * sWorld->getRate(RATE_DROP_MONEY));

    return loot;
}

Loot* Loot::CreateCreatureLoot(Creature* creature, Player* looter)
{
    Loot* loot = new Loot(creature, looter, LOOT_CORPSE);

    if (uint32 lootid = creature->GetCreatureTemplate()->lootid)
        loot->FillLoot(lootid, LootTemplates_Creature, looter, false, false, creature->GetLootMode());

    // Initialize loot duplicate count depending on raid difficulty
    if (creature->GetMap()->Is25ManRaid())
        loot->MaxDuplicates = 3;

    return loot;
}

Loot* Loot::CreateSkinningLoot(Creature* creature, Player* looter)
{
    Loot* loot = new Loot(creature, looter, LOOT_SKINNING, SKILL_SKINNING);

    if (uint32 lootid = creature->GetCreatureTemplate()->SkinLootId)
        loot->FillLoot(lootid, LootTemplates_Skinning, looter, true);

    return loot;
}

Loot* Loot::CreateInsigniaLoot(Player* player, Player* looter)
{
    Loot* loot = new Loot(player, looter, LOOT_CORPSE);

    if (Battleground* bg = player->GetBattleground())
        if (bg->GetTypeID() == BATTLEGROUND_AV)
            loot->FillLoot(1, LootTemplates_Creature, looter, true);

    // It may need a better formula
    // Now it works like this: lvl10: ~6copper, lvl70: ~9silver
    loot->Gold = Math::Rand(50, 150) * 0.016f * std::pow(float(player->GetLevel()) / 5.76f, 2.5f) * sWorld->getRate(RATE_DROP_MONEY);

    return loot;
}

Loot* Loot::CreateFishingLoot(GameObject* go, Player* looter, uint16 lootMode /*= LOOT_MODE_DEFAULT*/)
{
    Loot* loot = new Loot(go, looter, LOOT_FISHING);

    uint32 zone, subzone;
    uint32 defaultzone = 1;
    go->GetZoneAndAreaId(zone, subzone);

    if (GameObjectTemplateAddon const* addon = go->GetTemplateAddon())
        loot->GenerateMoneyLoot(addon->mingold, addon->maxgold);

    // if subzone loot exist use it
    loot->FillLoot(subzone, LootTemplates_Fishing, looter, true, true, lootMode);
    if (loot->Empty())  //use this becase if zone or subzone has set LOOT_MODE_JUNK_FISH,Even if no normal drop, fishloot->FillLoot return true. it wrong.
    {
        // subzone no result, use zone loot
        loot->FillLoot(zone, LootTemplates_Fishing, looter, true, true, lootMode);
        // use zone 1 as default, somewhere fishing got nothing,becase subzone and zone not set, like Off the coast of Storm Peaks.
        if (loot->Empty())
            loot->FillLoot(defaultzone, LootTemplates_Fishing, looter, true, true, lootMode);
    }

    return loot;
}

Loot* Loot::CreateGameObjectLoot(GameObject* go, Player* looter, SkillType skillId /*= SKILL_NONE*/)
{
    Loot* loot = new Loot(go, looter, LOOT_CORPSE, skillId);

    Group* group = looter->GetGroup();
    bool groupRules = (group && go->GetGOInfo()->type == GAMEOBJECT_TYPE_CHEST && go->GetGOInfo()->chest.usegrouplootrules);

    // check current RR player and get next if necessary
    if (groupRules)
        group->UpdateLooterGuid(go, true);

    if (GameObjectTemplateAddon const* addon = go->GetTemplateAddon())
        loot->GenerateMoneyLoot(addon->mingold, addon->maxgold);

    if (uint32 lootid = go->GetGOInfo()->GetLootId())
        loot->FillLoot(lootid, LootTemplates_Gameobject, looter, !groupRules, false, go->GetLootMode());

    go->SetLootGenerationTime();

    if (groupRules && !loot->Empty())
        group->UpdateLooterGuid(go);

    return loot;
}

Loot* Loot::CreateDisenchantingLoot(Item* item, Player* looter)
{
    Loot* loot = new Loot(item, looter, LOOT_DISENCHANTING, SKILL_ENCHANTING);
    loot->FillLoot(item->GetTemplate()->DisenchantID, LootTemplates_Disenchant, looter, true);
    return loot;
}

Loot* Loot::CreateProspectingLoot(Item* item, Player* looter)
{
    Loot* loot = new Loot(item, looter, LOOT_PROSPECTING);
    loot->FillLoot(item->GetEntry(), LootTemplates_Prospecting, looter, true);
    return loot;
}

Loot* Loot::CreateMillingLoot(Item* item, Player* looter)
{
    Loot* loot = new Loot(item, looter, LOOT_MILLING);
    loot->FillLoot(item->GetEntry(), LootTemplates_Milling, looter, true);
    return loot;
}

Loot* Loot::CreateItemLoot(Item* item, Player* looter)
{
    Loot* loot = new Loot(item, looter, LOOT_CONTAINER);

    loot->GenerateMoneyLoot(item->GetTemplate()->MinMoneyLoot, item->GetTemplate()->MaxMoneyLoot);
    loot->FillLoot(item->GetEntry(), LootTemplates_Item, looter, true, loot->Gold != 0);

    // Force save the loot and money items that were just rolled
    //  Also saves the container item ID in Loot struct (not to DB)
    if (!loot->IsLooted())
        item->ItemContainerSaveLootToDB();

    return loot;
}

Loot::Loot(Object* owner, Player const* recipient, LootType type /*= LOOT_NONE*/, SkillType skillId /*= SKILL_NONE*/) :
    Gold(0), UnlootedCount(0), RoundRobinPlayer(), Type(type), MaxDuplicates(1), _rollTimer(0), _owner(owner),
    PersonalLoot(false), IsAoELoot(false), SkillID(skillId)
{
    _GUID = ObjectGuid::Create<HighGuid::LootObject>(sObjectMgr->GetGenerator<HighGuid::LootObject>().Generate());

    _recipient = recipient->GetGUID();
    if (Group const* group = recipient->GetGroup())
        _recipientGroup = group->GetGUID();
}

Loot::~Loot()
{
    for (QuestItemMap::const_iterator itr = PlayerCurrencies.begin(); itr != PlayerCurrencies.end(); ++itr)
        delete itr->second;
    PlayerCurrencies.clear();

    for (QuestItemMap::const_iterator itr = PlayerQuestItems.begin(); itr != PlayerQuestItems.end(); ++itr)
        delete itr->second;
    PlayerQuestItems.clear();

    for (QuestItemMap::const_iterator itr = PlayerFFAItems.begin(); itr != PlayerFFAItems.end(); ++itr)
        delete itr->second;
    PlayerFFAItems.clear();

    for (QuestItemMap::const_iterator itr = PlayerNonQuestNonFFANonCurrencyConditionalItems.begin(); itr != PlayerNonQuestNonFFANonCurrencyConditionalItems.end(); ++itr)
        delete itr->second;
    PlayerNonQuestNonFFANonCurrencyConditionalItems.clear();
}

Player* Loot::GetRecipient() const
{
    if (!_recipient)
        return nullptr;

    return ObjectAccessor::FindConnectedPlayer(_recipient);
}

Group* Loot::GetRecipientGroup() const
{
    if (!_recipientGroup)
        return nullptr;

    return sGroupMgr->GetGroupByGUID(_recipientGroup);
}

bool Loot::IsAllowed(Player const* player) const
{
    // Check if player is recipient
    if (player->GetGUID() == _recipient)
        return true;

    // Check if player is in the same group as recipient
    if (player->GetGroup() == GetRecipientGroup())
        return true;

    return false;
}

bool Loot::UpdateRollTimer(uint32 diff)
{
    if (!_rollTimer)
        return true;

    if (_rollTimer <= diff)
    {
        if (Group* group = GetRecipientGroup())
            group->EndRoll(this, nullptr);

        _rollTimer = 0;
    }
    else
        _rollTimer -= diff;

    return false;
}

PermissionTypes Loot::GetPermission(Player* player)
{
    Player* recipient = GetRecipient();
    Group* group = GetRecipientGroup();

    if (!group)
    {
        if (recipient == player)
            return OWNER_PERMISSION;
        else
            return NONE_PERMISSION;
    }
    else
    {
        if (group != player->GetGroup())
            return NONE_PERMISSION;

        switch (group->GetLootMethod())
        {
            case MASTER_LOOT:
                return (group->GetMasterLooterGuid() == player->GetGUID() ? MASTER_PERMISSION : RESTRICTED_PERMISSION);
            case FREE_FOR_ALL:
                return ALL_PERMISSION;
            case ROUND_ROBIN:
                return ROUND_ROBIN_PERMISSION;
            default:
                return GROUP_PERMISSION;
        }
    }
}

// Inserts the item into the loot (called by LootTemplate processors)
void Loot::AddItem(LootStoreItem const& item)
{
    uint32 count = Math::Rand(item.MinCount, item.MaxCount);

    uint32 maxStack = count;
    bool PartyItem = false;

    ItemTemplate const* proto = nullptr;

    if (item.Type == LOOT_ITEM_TYPE_ITEM)
    {
        proto = sObjectMgr->GetItemTemplate(item.ItemId);
        if (!proto)
            return;

        maxStack = proto->GetMaxStackSize();
        PartyItem = (proto->GetFlags() & ITEM_PROTO_FLAG_MULTI_DROP) != 0;
    }
    else if (Player* player = GetRecipient())
    {
        if (CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(item.ItemId))
            count *= player->GetTotalAuraMultiplierByMiscValue(SPELL_AURA_MOD_CURRENCY_LOOT, currency->CategoryID);
    }

    uint32 stacks = count / maxStack + ((count % maxStack) ? 1 : 0);

    std::vector<LootItem>& lootItems = item.NeedsQuest ? QuestItems : Items;
    uint32 limit = item.NeedsQuest ? MAX_NR_QUEST_ITEMS : MAX_NR_LOOT_ITEMS;

    for (uint32 i = 0; i < stacks && lootItems.size() < limit; ++i)
    {
        LootItem generatedLoot(item);
        generatedLoot.Count = std::min(count, maxStack);
        lootItems.push_back(generatedLoot);
        count -= maxStack;

        // non-conditional one-player only items are counted here,
        // currencies are counter in FillCurrencyLoot(),
        // free for all items are counted in FillFFALoot(),
        // non-ffa conditionals are counted in FillNonQuestNonFFAConditionalLoot()
        if (!item.NeedsQuest && item.conditions.empty() && !PartyItem && item.Type == LOOT_ITEM_TYPE_ITEM)
            ++UnlootedCount;
    }

    // process additional loot from SPELL_AURA_MOD_GATHERING_ITEMS_GAINED_PERCENT
    if (item.Type == LOOT_ITEM_TYPE_ITEM && SkillID != SKILL_NONE)
        if (Player* player = GetRecipient())
        {
            Unit::AuraEffectList const& gatheringAuraEffects = player->GetAuraEffectsByType(SPELL_AURA_MOD_GATHERING_ITEMS_GAINED_PERCENT);
            if (!gatheringAuraEffects.empty())
                for (AuraEffect const* aurEff : gatheringAuraEffects)
                {
                    // only common items
                    if (proto->GetQuality() > ITEM_QUALITY_NORMAL)
                        continue;

                    uint32 chance = aurEff->GetAmount();
                    if (!Math::RollUnder(chance))
                        continue;

                    uint32 maxAdditionalItemCount = Math::Rand(1, aurEff->GetMiscValue());

                    stacks = maxAdditionalItemCount / maxStack + ((maxAdditionalItemCount % maxStack) ? 1 : 0);

                    for (uint32 i = 0; i < stacks && lootItems.size() < limit; ++i)
                    {
                        LootItem generatedLoot(item);
                        generatedLoot.Count = std::min(maxAdditionalItemCount, maxStack);
                        lootItems.push_back(generatedLoot);
                        maxAdditionalItemCount -= maxStack;

                        // non-conditional one-player only items are counted here,
                        // currencies are counter in FillCurrencyLoot(),
                        // free for all items are counted in FillFFALoot(),
                        // non-ffa conditionals are counted in FillNonQuestNonFFAConditionalLoot()
                        if (!item.NeedsQuest && item.conditions.empty() && !PartyItem && item.Type == LOOT_ITEM_TYPE_ITEM)
                            ++UnlootedCount;
                    }
                }
        }
}

// Calls processor of corresponding LootTemplate (which handles everything including References)
bool Loot::FillLoot(uint32 lootId, LootStore const& store, Player* lootOwner, bool personal, bool noEmptyError, uint16 lootMode /*= LOOT_MODE_DEFAULT*/)
{
    // Must be provided
    if (!lootOwner)
        return false;

    LootTemplate const* tab = store.GetLootFor(lootId);

    if (!tab)
    {
        if (!noEmptyError)
            TC_LOG_ERROR("sql.sql", "Table '%s' loot id #%u used but it doesn't have records.", store.GetName(), lootId);
        return false;
    }

    Items.reserve(MAX_NR_LOOT_ITEMS);
    QuestItems.reserve(MAX_NR_QUEST_ITEMS);

    Group* group = lootOwner->GetGroup();

    // Personal Loot
    // Usable for LFR and MoP world bosses
    if (lootOwner->IsPersonalLootEligible() && group)
    {
        PersonalLoot = true;
        tab->ProcessPersonalLoot(*this, store.IsRatesAllowed(), lootMode, group);
    }
    else
    {
        tab->Process(*this, store.IsRatesAllowed(), lootMode);
        if (lootOwner->GetTypeId() == TYPEID_UNIT)
            GenerateMoneyLoot(lootOwner->ToCreature()->GetCreatureTemplate()->mingold, lootOwner->ToCreature()->GetCreatureTemplate()->maxgold);
    }

    // Setting access rights for group loot case
    if (!personal && group)
    {
        RoundRobinPlayer = lootOwner->GetGUID();

        for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            if (Player* player = itr->GetSource())   // should actually be looted object instead of lootOwner but looter has to be really close so doesnt really matter
                if (player->IsInMap(lootOwner))
                    FillNotNormalLootFor(player, player->IsAtGroupRewardDistance(lootOwner));

        for (uint8 i = 0; i < Items.size(); ++i)
        {
            if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(Items[i].ItemId))
                if (proto->GetQuality() < uint32(group->GetLootThreshold()))
                    Items[i].IsUnderthreshold = true;
        }
    }
    // ... for personal loot
    else
        FillNotNormalLootFor(lootOwner, true);

    return true;
}

void Loot::FillNotNormalLootFor(Player* player, bool presentAtLooting)
{
    uint32 plguid = player->GetGUID().GetCounter();

    QuestItemMap::const_iterator qmapitr = PlayerCurrencies.find(plguid);
    if (qmapitr == PlayerCurrencies.end())
        FillCurrencyLoot(player);

    qmapitr = PlayerQuestItems.find(plguid);
    if (qmapitr == PlayerQuestItems.end())
        FillQuestLoot(player);

    qmapitr = PlayerFFAItems.find(plguid);
    if (qmapitr == PlayerFFAItems.end())
        FillFFALoot(player);

    qmapitr = PlayerNonQuestNonFFANonCurrencyConditionalItems.find(plguid);
    if (qmapitr == PlayerNonQuestNonFFANonCurrencyConditionalItems.end())
        FillNonQuestNonFFANonCurrencyConditionalLoot(player, presentAtLooting);

    // if not auto-processed player will have to come and pick it up manually
    if (!presentAtLooting)
        return;

    // Process currency items
    uint32 max_slot = GetMaxSlotInLootFor(player);
    LootItem const* item = NULL;
    uint32 itemsSize = uint32(Items.size());
    for (uint32 i = 0; i < max_slot; ++i)
    {
        if (i < Items.size())
            item = &Items[i];
        else
            item = &QuestItems[i-itemsSize];

        if (!item->IsLooted && item->FreeForAll && item->AllowedForPlayer(player))
            if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(item->ItemId))
                if (proto->IsCurrencyToken())
                    player->StoreLootItem(i, this);
    }
}

QuestItemList* Loot::FillCurrencyLoot(Player* player)
{
    QuestItemList* ql = new QuestItemList();

    for (uint8 i = 0; i < Items.size(); ++i)
    {
        LootItem& item = Items[i];
        if (!item.IsLooted && item.IsCurrency && item.AllowedForPlayer(player))
        {
            ql->push_back(QuestItem(i));
            ++UnlootedCount;
        }
    }

    if (ql->empty())
    {
        delete ql;
        return NULL;
    }

    PlayerCurrencies[player->GetGUID().GetCounter()] = ql;
    return ql;
}

QuestItemList* Loot::FillFFALoot(Player* player)
{
    QuestItemList* ql = new QuestItemList();

    for (uint8 i = 0; i < Items.size(); ++i)
    {
        LootItem &item = Items[i];
        if (!item.IsLooted && item.FreeForAll && item.AllowedForPlayer(player))
        {
            ql->push_back(QuestItem(i));
            ++UnlootedCount;
        }
    }
    if (ql->empty())
    {
        delete ql;
        return NULL;
    }

    PlayerFFAItems[player->GetGUID().GetCounter()] = ql;
    return ql;
}

QuestItemList* Loot::FillQuestLoot(Player* player)
{
    if (Items.size() == MAX_NR_LOOT_ITEMS)
        return NULL;

    QuestItemList* ql = new QuestItemList();

    for (uint8 i = 0; i < QuestItems.size(); ++i)
    {
        LootItem &item = QuestItems[i];

        if (!item.IsLooted && (item.AllowedForPlayer(player) || (item.FollowLootRules && player->GetGroup() && ((player->GetGroup()->GetLootMethod() == MASTER_LOOT && player->GetGroup()->GetMasterLooterGuid() == player->GetGUID()) || player->GetGroup()->GetLootMethod() != MASTER_LOOT))))
        {
            ql->push_back(QuestItem(i));

            // quest items get blocked when they first appear in a
            // player's quest vector
            //
            // increase once if one looter only, looter-times if free for all
            if (item.FreeForAll || !item.IsBlocked)
                ++UnlootedCount;
            if (!player->GetGroup() || (player->GetGroup()->GetLootMethod() != GROUP_LOOT && player->GetGroup()->GetLootMethod() != ROUND_ROBIN))
                item.IsBlocked = true;

            if (Items.size() + ql->size() == MAX_NR_LOOT_ITEMS)
                break;
        }
    }
    if (ql->empty())
    {
        delete ql;
        return NULL;
    }

    PlayerQuestItems[player->GetGUID().GetCounter()] = ql;
    return ql;
}

QuestItemList* Loot::FillNonQuestNonFFANonCurrencyConditionalLoot(Player* player, bool presentAtLooting)
{
    QuestItemList* ql = new QuestItemList();

    for (uint8 i = 0; i < Items.size(); ++i)
    {
        LootItem &item = Items[i];
        if (!item.IsLooted && !item.IsCurrency && !item.FreeForAll && (item.AllowedForPlayer(player) || (item.FollowLootRules && player->GetGroup() && ((player->GetGroup()->GetLootMethod() == MASTER_LOOT && player->GetGroup()->GetMasterLooterGuid() == player->GetGUID()) || player->GetGroup()->GetLootMethod() != MASTER_LOOT))))
        {
            if (presentAtLooting)
                item.AddAllowedLooter(player);
            if (!item.conditions.empty())
            {
                ql->push_back(QuestItem(i));
                if (!item.IsCounted)
                {
                    ++UnlootedCount;
                    item.IsCounted = true;
                }
            }
        }
    }
    if (ql->empty())
    {
        delete ql;
        return NULL;
    }

    PlayerNonQuestNonFFANonCurrencyConditionalItems[player->GetGUID().GetCounter()] = ql;
    return ql;
}

//===================================================

void Loot::NotifyItemRemoved(uint8 lootIndex)
{
    // notify all players that are looting this that the item was removed
    // convert the index to the slot the player sees
    GuidSet::iterator i_next;
    for (GuidSet::iterator i = PlayersLooting.begin(); i != PlayersLooting.end(); i = i_next)
    {
        i_next = i;
        ++i_next;
        if (Player* player = ObjectAccessor::FindPlayer(*i))
            player->SendNotifyLootItemRemoved(this, lootIndex);
        else
            PlayersLooting.erase(i);
    }
}

void Loot::NotifyMoneyRemoved()
{
    // notify all players that are looting this that the money was removed
    GuidSet::iterator i_next;
    for (GuidSet::iterator i = PlayersLooting.begin(); i != PlayersLooting.end(); i = i_next)
    {
        i_next = i;
        ++i_next;
        if (Player* player = ObjectAccessor::FindPlayer(*i))
            player->SendNotifyLootMoneyRemoved(this);
        else
            PlayersLooting.erase(i);
    }
}

void Loot::NotifyQuestItemRemoved(uint8 questIndex)
{
    // when a free for all questitem is looted
    // all players will get notified of it being removed
    // (other questitems can be looted by each group member)
    // bit inefficient but isn't called often

    GuidSet::iterator i_next;
    for (GuidSet::iterator i = PlayersLooting.begin(); i != PlayersLooting.end(); i = i_next)
    {
        i_next = i;
        ++i_next;
        if (Player* player = ObjectAccessor::FindPlayer(*i))
        {
            QuestItemMap::const_iterator pq = PlayerQuestItems.find(player->GetGUID().GetCounter());
            if (pq != PlayerQuestItems.end() && pq->second)
            {
                // find where/if the player has the given item in it's vector
                QuestItemList& pql = *pq->second;

                uint8 j;
                for (j = 0; j < pql.size(); ++j)
                    if (pql[j].Index == questIndex)
                        break;

                if (j < pql.size())
                    player->SendNotifyLootItemRemoved(this, Items.size() + j);
            }
        }
        else
            PlayersLooting.erase(i);
    }
}

void Loot::GenerateMoneyLoot(uint32 minAmount, uint32 maxAmount)
{
    if (maxAmount > 0)
    {
        if (maxAmount <= minAmount)
            Gold = uint32(maxAmount * sWorld->getRate(RATE_DROP_MONEY));
        else if ((maxAmount - minAmount) < 32700)
            Gold = uint32(Math::Rand(minAmount, maxAmount) * sWorld->getRate(RATE_DROP_MONEY));
        else
            Gold = uint32(Math::Rand(minAmount >> 8, maxAmount >> 8) * sWorld->getRate(RATE_DROP_MONEY)) << 8;
    }
}

void Loot::DeleteLootItemFromContainerItemDB(uint32 itemID)
{
    // Deletes a single item associated with an openable item from the DB
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ITEMCONTAINER_ITEM);
    stmt->setUInt32(0, ContainerID.GetCounter());
    stmt->setUInt32(1, itemID);
    CharacterDatabase.Execute(stmt);

    // Mark the item looted to prevent resaving
    for (LootItemList::iterator _itr = Items.begin(); _itr != Items.end(); ++_itr)
    {
        if (_itr->ItemId != itemID)
            continue;

        _itr->CanSave = false;
        break;
    }
}

void Loot::DeleteLootMoneyFromContainerItemDB()
{
    // Deletes money loot associated with an openable item from the DB
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ITEMCONTAINER_MONEY);
    stmt->setUInt32(0, ContainerID.GetCounter());
    CharacterDatabase.Execute(stmt);
}

LootItem* Loot::LootItemInSlot(uint32 lootSlot, Player* player, QuestItem* *qitem, QuestItem* *ffaitem, QuestItem* *conditem, QuestItem* *currency)
{
    LootItem* item = NULL;
    bool IsLooted = true;
    if (lootSlot >= Items.size())
    {
        uint32 questSlot = lootSlot - Items.size();
        QuestItemMap::const_iterator itr = PlayerQuestItems.find(player->GetGUID().GetCounter());
        if (itr != PlayerQuestItems.end() && questSlot < itr->second->size())
        {
            QuestItem* qitem2 = &itr->second->at(questSlot);
            if (qitem)
                *qitem = qitem2;
            item = &QuestItems[qitem2->Index];
            IsLooted = qitem2->IsLooted;
        }
    }
    else
    {
        item = &Items[lootSlot];
        IsLooted = item->IsLooted;
        if (item->IsCurrency)
        {
            QuestItemMap::const_iterator itr = PlayerCurrencies.find(player->GetGUID().GetCounter());
            if (itr != PlayerCurrencies.end())
                for (QuestItemList::const_iterator iter = itr->second->begin(); iter != itr->second->end(); ++iter)
                    if (iter->Index == lootSlot)
                    {
                        QuestItem* currency2 = (QuestItem*)& (*iter);
                        if (currency)
                            *currency = currency2;
                        IsLooted = currency2->IsLooted;
                        break;
                    }
        }
        else if (item->FreeForAll)
        {
            QuestItemMap::const_iterator itr = PlayerFFAItems.find(player->GetGUID().GetCounter());
            if (itr != PlayerFFAItems.end())
                for (QuestItemList::const_iterator iter=itr->second->begin(); iter!= itr->second->end(); ++iter)
                    if (iter->Index == lootSlot)
                    {
                        QuestItem* ffaitem2 = (QuestItem*)&(*iter);
                        if (ffaitem)
                            *ffaitem = ffaitem2;
                        IsLooted = ffaitem2->IsLooted;
                        break;
                    }
        }
        else if (!item->conditions.empty())
        {
            QuestItemMap::const_iterator itr = PlayerNonQuestNonFFANonCurrencyConditionalItems.find(player->GetGUID().GetCounter());
            if (itr != PlayerNonQuestNonFFANonCurrencyConditionalItems.end())
                for (QuestItemList::const_iterator iter=itr->second->begin(); iter!= itr->second->end(); ++iter)
                    if (iter->Index == lootSlot)
                    {
                        QuestItem* conditem2 = (QuestItem*)&(*iter);
                        if (conditem)
                            *conditem = conditem2;
                        IsLooted = conditem2->IsLooted;
                        break;
                    }
        }
    }

    if (IsLooted)
        return NULL;

    return item;
}

uint32 Loot::GetMaxSlotInLootFor(Player* player) const
{
    QuestItemMap::const_iterator itr = PlayerQuestItems.find(player->GetGUID().GetCounter());
    return Items.size() + (itr != PlayerQuestItems.end() ?  itr->second->size() : 0);
}

// return true if there is any item that is lootable for any player (not quest item, FFA or conditional)
bool Loot::HasItemForAll() const
{
    // Gold is always lootable
    if (Gold)
        return true;

    for (LootItem const& item : Items)
        if (!item.IsLooted && !item.FreeForAll && item.conditions.empty())
            return true;

    return false;
}

// return true if there is any FFA, quest or conditional item for the player.
bool Loot::HasItemFor(Player* player) const
{
    QuestItemMap const& lootPlayerCurrencies = GetPlayerCurrencies();
    QuestItemMap::const_iterator cur_itr = lootPlayerCurrencies.find(player->GetGUID().GetCounter());
    if (cur_itr != lootPlayerCurrencies.end())
    {
        QuestItemList* cur_list = cur_itr->second;
        for (QuestItemList::const_iterator cui = cur_list->begin(); cui != cur_list->end(); ++cui)
        {
            const LootItem &item = Items[cui->Index];
            if (!cui->IsLooted && !item.IsLooted)
                return true;
        }
    }

    QuestItemMap const& lootPlayerQuestItems = GetPlayerQuestItems();
    QuestItemMap::const_iterator q_itr = lootPlayerQuestItems.find(player->GetGUID().GetCounter());
    if (q_itr != lootPlayerQuestItems.end())
    {
        QuestItemList* q_list = q_itr->second;
        for (QuestItemList::const_iterator qi = q_list->begin(); qi != q_list->end(); ++qi)
        {
            const LootItem &item = QuestItems[qi->Index];
            if (!qi->IsLooted && !item.IsLooted)
                return true;
        }
    }

    QuestItemMap const& lootPlayerFFAItems = GetPlayerFFAItems();
    QuestItemMap::const_iterator ffa_itr = lootPlayerFFAItems.find(player->GetGUID().GetCounter());
    if (ffa_itr != lootPlayerFFAItems.end())
    {
        QuestItemList* ffa_list = ffa_itr->second;
        for (QuestItemList::const_iterator fi = ffa_list->begin(); fi != ffa_list->end(); ++fi)
        {
            const LootItem &item = Items[fi->Index];
            if (!fi->IsLooted && !item.IsLooted)
                return true;
        }
    }

    QuestItemMap const& lootPlayerNonQuestNonFFAConditionalItems = GetPlayerNonQuestNonFFANonCurrencyConditionalItems();
    QuestItemMap::const_iterator nn_itr = lootPlayerNonQuestNonFFAConditionalItems.find(player->GetGUID().GetCounter());
    if (nn_itr != lootPlayerNonQuestNonFFAConditionalItems.end())
    {
        QuestItemList* conditional_list = nn_itr->second;
        for (QuestItemList::const_iterator ci = conditional_list->begin(); ci != conditional_list->end(); ++ci)
        {
            const LootItem &item = Items[ci->Index];
            if (!ci->IsLooted && !item.IsLooted)
                return true;
        }
    }

    return false;
}

// return true if there is any item over the group threshold (i.e. not underthreshold).
bool Loot::HasOverThresholdItem() const
{
    for (uint8 i = 0; i < Items.size(); ++i)
    {
        if (!Items[i].IsLooted && !Items[i].IsUnderthreshold && !Items[i].FreeForAll)
            return true;
    }

    return false;
}

void Loot::BuildLootResponse(Player* viewer, PermissionTypes permission)
{
    if (!viewer || permission == NONE_PERMISSION)
        return;

    ObjectGuid ownerGuid = GetOwner()->GetGUID();
    ObjectGuid lootedGuid = GetGUID();

    uint8 LootMethod = FREE_FOR_ALL;
    uint8 Threshold = ITEM_QUALITY_UNCOMMON;
    if (Group* group = viewer->GetGroup())
    {
        LootMethod = group->GetLootMethod();
        Threshold = group->GetLootThreshold();
    }

    bool Acquired = true;
    bool HasError = false;
    bool ShowThreshold = true;

    bool ShowLootMethod = LootMethod != GROUP_LOOT;
    bool LootTypePresent = Type != LOOT_NONE;
    bool HasGold = Gold > 0;

    ByteBuffer itemBytes;
    ByteBuffer currencyBytes;

    std::list<LootItemInfo> items;
    std::list<LootItemInfo> currencies;

    bool hasPermission = true;
    switch (permission)
    {
        case GROUP_PERMISSION:
        case MASTER_PERMISSION:
        case RESTRICTED_PERMISSION:
        {
            // if you are not the round-robin group looter, you can only see
            // blocked rolled items and quest items, and !ffa items
            for (uint8 i = 0; i < Items.size(); ++i)
                if (!Items[i].IsLooted && !Items[i].IsCurrency && !Items[i].FreeForAll && Items[i].conditions.empty() && Items[i].AllowedForPlayer(viewer))
                {
                    LootSlotType slotType = LOOT_SLOT_TYPE_ALLOW_LOOT;

                    if (Items[i].IsBlocked) // for ML & restricted IsBlocked = !IsUnderthreshold
                    {
                        switch (permission)
                        {
                            case GROUP_PERMISSION:
                                slotType = LOOT_SLOT_TYPE_ROLL_ONGOING;
                                break;
                            case MASTER_PERMISSION:
                                if (viewer->GetGroup() && viewer->GetGroup()->GetMasterLooterGuid() == viewer->GetGUID())
                                    slotType = LOOT_SLOT_TYPE_MASTER;
                                else
                                    slotType = LOOT_SLOT_TYPE_LOCKED;
                                break;
                            case RESTRICTED_PERMISSION:
                                slotType = LOOT_SLOT_TYPE_LOCKED;
                                break;
                            default:
                                continue;
                        }
                    }
                    else if (RoundRobinPlayer.IsEmpty() || viewer->GetGUID() == RoundRobinPlayer || !Items[i].IsUnderthreshold)
                    {
                        // no round robin owner or he has released the loot
                        // or it IS the round robin group owner
                        // => item is lootable
                        slotType = LOOT_SLOT_TYPE_ALLOW_LOOT;
                    }
                    else
                        continue; // item shall not be displayed.

                    items.push_back(LootItemInfo(i, slotType, &Items[i]));
                }
            break;
        }
        case ROUND_ROBIN_PERMISSION:
        {
            for (uint8 i = 0; i < Items.size(); ++i)
                if (!Items[i].IsLooted && !Items[i].IsCurrency && !Items[i].FreeForAll && Items[i].conditions.empty() && Items[i].AllowedForPlayer(viewer))
                {
                    if (!RoundRobinPlayer.IsEmpty() && viewer->GetGUID() != RoundRobinPlayer)
                        // item shall not be displayed.
                        continue;

                    items.push_back(LootItemInfo(i, LOOT_SLOT_TYPE_ROUND_RUBIN, &Items[i]));
                }
            break;
        }
        case ALL_PERMISSION:
        case OWNER_PERMISSION:
        {
            LootSlotType slotType = permission == OWNER_PERMISSION ? LOOT_SLOT_TYPE_OWNER : LOOT_SLOT_TYPE_ALLOW_LOOT;
            for (uint8 i = 0; i < Items.size(); ++i)
                if (!Items[i].IsLooted && !Items[i].IsCurrency && !Items[i].FreeForAll && Items[i].conditions.empty() && Items[i].AllowedForPlayer(viewer))
                    items.push_back(LootItemInfo(i, slotType, &Items[i]));
            break;
        }
        default:
            hasPermission = false;
            break;
    }

    if (hasPermission)
    {
        LootSlotType slotType = permission == OWNER_PERMISSION ? LOOT_SLOT_TYPE_OWNER : LOOT_SLOT_TYPE_ALLOW_LOOT;

        QuestItemMap const& lootPlayerCurrencies = GetPlayerCurrencies();
        QuestItemMap::const_iterator currency_itr = lootPlayerCurrencies.find(viewer->GetGUID().GetCounter());
        if (currency_itr != lootPlayerCurrencies.end())
        {
            QuestItemList* currency_list = currency_itr->second;
            for (QuestItemList::const_iterator ci = currency_list->begin(); ci != currency_list->end(); ++ci)
            {
                LootItem &item = Items[ci->Index];
                if (!ci->IsLooted && !item.IsLooted)
                    currencies.push_back(LootItemInfo(ci->Index, slotType, &item));
            }
        }

        QuestItemMap const& lootPlayerQuestItems = GetPlayerQuestItems();
        QuestItemMap::const_iterator q_itr = lootPlayerQuestItems.find(viewer->GetGUID().GetCounter());
        if (q_itr != lootPlayerQuestItems.end())
        {
            QuestItemList* q_list = q_itr->second;
            for (QuestItemList::const_iterator qi = q_list->begin(); qi != q_list->end(); ++qi)
            {
                LootItem& item = QuestItems[qi->Index];
                if (!qi->IsLooted && !item.IsLooted)
                {
                    LootSlotType finalSlotType = slotType;
                    uint8 index = Items.size() + (qi - q_list->begin());

                    if (item.FollowLootRules)
                    {
                        switch (permission)
                        {
                            case MASTER_PERMISSION:
                                finalSlotType = LOOT_SLOT_TYPE_MASTER;
                                break;
                            case RESTRICTED_PERMISSION:
                                finalSlotType = item.IsBlocked ? LOOT_SLOT_TYPE_LOCKED : LOOT_SLOT_TYPE_ALLOW_LOOT;
                                break;
                            case GROUP_PERMISSION:
                            case ROUND_ROBIN_PERMISSION:
                                finalSlotType = item.IsBlocked ? LOOT_SLOT_TYPE_ROLL_ONGOING : LOOT_SLOT_TYPE_ALLOW_LOOT;
                                break;
                            default:
                                break;
                        }
                    }

                    items.push_back(LootItemInfo(index, finalSlotType, &item));
                }
            }
        }

        QuestItemMap const& lootPlayerFFAItems = GetPlayerFFAItems();
        QuestItemMap::const_iterator ffa_itr = lootPlayerFFAItems.find(viewer->GetGUID().GetCounter());
        if (ffa_itr != lootPlayerFFAItems.end())
        {
            QuestItemList* ffa_list = ffa_itr->second;
            for (QuestItemList::const_iterator fi = ffa_list->begin(); fi != ffa_list->end(); ++fi)
            {
                LootItem& item = Items[fi->Index];
                if (!fi->IsLooted && !item.IsLooted)
                    items.push_back(LootItemInfo(fi->Index, slotType, &item));
            }
        }

        QuestItemMap const& lootPlayerNonQuestNonFFAConditionalItems = GetPlayerNonQuestNonFFANonCurrencyConditionalItems();
        QuestItemMap::const_iterator nn_itr = lootPlayerNonQuestNonFFAConditionalItems.find(viewer->GetGUID().GetCounter());
        if (nn_itr != lootPlayerNonQuestNonFFAConditionalItems.end())
        {
            QuestItemList* conditional_list = nn_itr->second;
            for (QuestItemList::const_iterator ci = conditional_list->begin(); ci != conditional_list->end(); ++ci)
            {
                LootItem& item = Items[ci->Index];
                if (!ci->IsLooted && !item.IsLooted)
                {
                    LootSlotType finalSlotType = slotType;
                    if (item.FollowLootRules)
                    {
                        switch (permission)
                        {
                            case MASTER_PERMISSION:
                                finalSlotType = LOOT_SLOT_TYPE_MASTER;
                                break;
                            case RESTRICTED_PERMISSION:
                                finalSlotType = item.IsBlocked ? LOOT_SLOT_TYPE_LOCKED : LOOT_SLOT_TYPE_ALLOW_LOOT;
                                break;
                            case GROUP_PERMISSION:
                            case ROUND_ROBIN_PERMISSION:
                                finalSlotType = item.IsBlocked ? LOOT_SLOT_TYPE_ROLL_ONGOING : LOOT_SLOT_TYPE_ALLOW_LOOT;
                                break;
                            default:
                                break;
                        }
                    }

                    items.push_back(LootItemInfo(ci->Index, finalSlotType, &item));
                }
            }
        }
    }

    WorldPacket data(SMSG_LOOT_RESPONSE, 2 * (1 + 8) + 6 + (ShowThreshold ? 1 : 0) + (HasGold ? 4 : 0) + (ShowLootMethod ? 1 : 0) + (LootTypePresent ? 1 : 0) + (HasError ? 1 : 0) +
                    items.size() * (1 + 4 + 4 + 4 + 4 + 4 + 4 + 1 + 4 + 1 + 4) + currencies.size() * (1 + 4 + 1 + 4));

    data.WriteBit(!ShowLootMethod);
    data.WriteBit(!LootTypePresent);

    data.WriteGuidMask(ownerGuid, 4);

    data.WriteBits(currencies.size(), 20);

    data.WriteGuidMask(lootedGuid, 2, 3, 7, 1);

    data.WriteGuidMask(ownerGuid, 6, 7);

    data.WriteBit(!HasGold);
    data.WriteBit(Acquired);
    data.WriteBit(!HasError);
    data.WriteBit(IsAoELoot);

    data.WriteGuidMask(ownerGuid, 5);

    data.WriteGuidMask(lootedGuid, 6);

    data.WriteBits(items.size(), 19);

    data.WriteGuidMask(lootedGuid, 0);

    for (auto & itr : items)
    {
        LootItem* item = itr.Item;

        uint32 SlotType = itr.Slot;
        uint8 Index = itr.Index;

        uint8 LootItemType = 0;
        uint8 ItemType = 3;

        bool CanTradeToTapList = false;
        bool ShowLootItemType = LootItemType != 0;
        bool ShowIndex = Index >= 0;

        data.WriteBits(SlotType, 3);
        data.WriteBit(CanTradeToTapList);
        data.WriteBit(!ShowLootItemType);
        data.WriteBit(!ShowIndex);
        data.WriteBits(ItemType, 2);

        itemBytes << uint32(item->RandomSuffix);
        itemBytes << uint32(item->Count);
        itemBytes << uint32(item->ItemId);

        // Dynamic modifiers
        if (item->UpgradeId)
        {
            uint32 Mask = 1 << ITEM_MODIFIER_UPGRADE_ID;
            uint32 UpgradeID = item->UpgradeId;

            itemBytes << uint32(2 * sizeof(uint32));
            itemBytes << uint32(Mask);
            itemBytes << uint32(UpgradeID);
        }
        else
            itemBytes << uint32(0);

        if (ShowLootItemType)
            itemBytes << uint8(LootItemType);

        itemBytes << int32(item->RandomPropertyId);

        if (ShowIndex)
            itemBytes << uint8(Index);

        itemBytes << uint32(sObjectMgr->GetItemTemplate(item->ItemId)->GetDisplayId());
    }

    data.WriteGuidMask(ownerGuid, 1, 0);

    for (auto & itr : currencies)
    {
        LootItem* item = itr.Item;

        uint32 SlotType = itr.Slot;
        uint32 Index = itr.Index;

        CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(item->ItemId);
        uint32 precision = (currency && (currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION)) ? CURRENCY_PRECISION : 1;
        uint32 currencyCount = item->Count * precision;

        data.WriteBits(SlotType, 3);

        currencyBytes << uint32(currencyCount);
        currencyBytes << uint8(Index);
        currencyBytes << uint32(item->ItemId);
    }

    data.WriteGuidMask(lootedGuid, 5);

    data.WriteGuidMask(ownerGuid, 3);

    data.WriteGuidMask(lootedGuid, 4);

    data.WriteBit(!ShowThreshold);

    data.WriteGuidMask(ownerGuid, 2);

    data.FlushBits();

    if (ShowThreshold)
        data << uint8(Threshold);

    data.append(itemBytes);

    data.WriteGuidBytes(lootedGuid, 2);

    if (HasGold)
        data << uint32(Gold);

    data.WriteGuidBytes(lootedGuid, 7);

    data.WriteGuidBytes(ownerGuid, 5);

    data.WriteGuidBytes(lootedGuid, 3);

    if (ShowLootMethod)
        data << uint8(LootMethod);

    data.WriteGuidBytes(lootedGuid, 4);

    if (LootTypePresent)
        data << uint8(Type);

    data.WriteGuidBytes(ownerGuid, 4);

    data.WriteGuidBytes(lootedGuid, 5);

    data.append(currencyBytes);

    data.WriteGuidBytes(ownerGuid, 2, 3);

    data.WriteGuidBytes(lootedGuid, 1);

    data.WriteGuidBytes(ownerGuid, 0);

    data.WriteGuidBytes(lootedGuid, 0);

    data.WriteGuidBytes(ownerGuid, 6, 7, 1);

    data.WriteGuidBytes(lootedGuid, 6);

    viewer->SendDirectMessage(&data);
}

//
// --------- LootTemplate::LootGroup ---------
//

LootTemplate::LootGroup::~LootGroup()
{
    while (!ExplicitlyChanced.empty())
    {
        delete ExplicitlyChanced.back();
        ExplicitlyChanced.pop_back();
    }

    while (!EqualChanced.empty())
    {
        delete EqualChanced.back();
        EqualChanced.pop_back();
    }
}

// Adds an entry to the group (at loading stage)
void LootTemplate::LootGroup::AddEntry(LootStoreItem* item)
{
    if (item->Chance != 0)
        ExplicitlyChanced.push_back(item);
    else
        EqualChanced.push_back(item);
}

// Rolls an item from the group, returns NULL if all miss their chances
LootStoreItem const* LootTemplate::LootGroup::Roll(Loot& loot, uint16 lootMode) const
{
    LootStoreItemList possibleLoot = ExplicitlyChanced;
    possibleLoot.remove_if(LootGroupInvalidSelector(loot, lootMode));

    if (!possibleLoot.empty())                             // First explicitly chanced entries are checked
    {
        float roll = Math::RandPct();

        for (LootStoreItemList::const_iterator itr = possibleLoot.begin(); itr != possibleLoot.end(); ++itr)   // check each explicitly chanced entry in the template and modify its chance based on quality.
        {
            LootStoreItem* item = *itr;
            if (item->Chance >= 100.0f)
                return item;

            roll -= item->Chance;
            if (roll < 0)
                return item;
        }
    }

    possibleLoot = EqualChanced;
    possibleLoot.remove_if(LootGroupInvalidSelector(loot, lootMode));
    if (!possibleLoot.empty())                              // If nothing selected yet - an item is taken from equal-chanced part
        return Trinity::Containers::SelectRandomContainerElement(possibleLoot);

    return NULL;                                            // Empty drop from the group
}

// True if group includes at least 1 quest drop entry
bool LootTemplate::LootGroup::HasQuestDrop() const
{
    for (LootStoreItemList::const_iterator i = ExplicitlyChanced.begin(); i != ExplicitlyChanced.end(); ++i)
        if ((*i)->NeedsQuest)
            return true;

    for (LootStoreItemList::const_iterator i = EqualChanced.begin(); i != EqualChanced.end(); ++i)
        if ((*i)->NeedsQuest)
            return true;

    return false;
}

// True if group includes at least 1 quest drop entry for active quests of the player
bool LootTemplate::LootGroup::HasQuestDropForPlayer(Player const* player) const
{
    for (LootStoreItemList::const_iterator i = ExplicitlyChanced.begin(); i != ExplicitlyChanced.end(); ++i)
        if (player->HasQuestForItem((*i)->ItemId))
            return true;

    for (LootStoreItemList::const_iterator i = EqualChanced.begin(); i != EqualChanced.end(); ++i)
        if (player->HasQuestForItem((*i)->ItemId))
            return true;

    return false;
}

void LootTemplate::LootGroup::CopyConditions(ConditionContainer /*conditions*/)
{
    for (LootStoreItemList::iterator i = ExplicitlyChanced.begin(); i != ExplicitlyChanced.end(); ++i)
        (*i)->conditions.clear();

    for (LootStoreItemList::iterator i = EqualChanced.begin(); i != EqualChanced.end(); ++i)
        (*i)->conditions.clear();
}

// Rolls an item from the group (if any takes its chance) and adds the item to the loot
void LootTemplate::LootGroup::Process(Loot& loot, uint16 lootMode) const
{
    if (LootStoreItem const* item = Roll(loot, lootMode))
        loot.AddItem(*item);
}

// Overall chance for the group without equal chanced items
float LootTemplate::LootGroup::RawTotalChance() const
{
    float result = 0;

    for (LootStoreItemList::const_iterator i=ExplicitlyChanced.begin(); i != ExplicitlyChanced.end(); ++i)
        if (!(*i)->NeedsQuest)
            result += (*i)->Chance;

    return result;
}

// Overall chance for the group
float LootTemplate::LootGroup::TotalChance() const
{
    float result = RawTotalChance();

    if (!EqualChanced.empty() && result < 100.0f)
        return 100.0f;

    return result;
}

void LootTemplate::LootGroup::Verify(LootStore const& lootstore, uint32 id, uint8 group_id) const
{
    float chance = RawTotalChance();
    if (chance > 101.0f)                                    /// @todo replace with 100% when DBs will be ready
        TC_LOG_ERROR("sql.sql", "Table '%s' entry %u group %d has total chance > 100%% (%f)", lootstore.GetName(), id, group_id, chance);

    if (chance >= 100.0f && !EqualChanced.empty())
        TC_LOG_ERROR("sql.sql", "Table '%s' entry %u group %d has items with chance=0%% but group total chance >= 100%% (%f)", lootstore.GetName(), id, group_id, chance);
}

void LootTemplate::LootGroup::CheckLootRefs(LootTemplateMap const& /*store*/, LootIdSet* ref_set) const
{
    for (LootStoreItemList::const_iterator ieItr = ExplicitlyChanced.begin(); ieItr != ExplicitlyChanced.end(); ++ieItr)
    {
        LootStoreItem* item = *ieItr;
        if (item->Reference > 0)
        {
            if (!LootTemplates_Reference.GetLootFor(item->Reference))
                LootTemplates_Reference.ReportNonExistingId(item->Reference, "Reference", item->ItemId);
            else if (ref_set)
                ref_set->erase(item->Reference);
        }
    }

    for (LootStoreItemList::const_iterator ieItr = EqualChanced.begin(); ieItr != EqualChanced.end(); ++ieItr)
    {
        LootStoreItem* item = *ieItr;
        if (item->Reference > 0)
        {
            if (!LootTemplates_Reference.GetLootFor(item->Reference))
                LootTemplates_Reference.ReportNonExistingId(item->Reference, "Reference", item->ItemId);
            else if (ref_set)
                ref_set->erase(item->Reference);
        }
    }
}

//
// --------- LootTemplate ---------
//

LootTemplate::~LootTemplate()
{
    for (LootStoreItemList::iterator i = Entries.begin(); i != Entries.end(); ++i)
        delete *i;

    for (size_t i = 0; i < Groups.size(); ++i)
        delete Groups[i];
}

// Adds an entry to the group (at loading stage)
void LootTemplate::AddEntry(LootStoreItem* item)
{
    if (item->GroupId > 0 && item->Reference == 0)            // Group
    {
        if (item->GroupId >= Groups.size())
            Groups.resize(item->GroupId, NULL);               // Adds new group the the loot template if needed
        if (!Groups[item->GroupId - 1])
            Groups[item->GroupId - 1] = new LootGroup();

        Groups[item->GroupId - 1]->AddEntry(item);            // Adds new entry to the group
    }
    else                                                      // Non-grouped entries and References are stored together
        Entries.push_back(item);
}

void LootTemplate::CopyConditions(const ConditionContainer& conditions)
{
    for (LootStoreItemList::iterator i = Entries.begin(); i != Entries.end(); ++i)
        (*i)->conditions.clear();

    for (LootGroups::iterator i = Groups.begin(); i != Groups.end(); ++i)
        if (LootGroup* group = *i)
            group->CopyConditions(conditions);
}

void LootTemplate::CopyConditions(LootItem* li) const
{
    // Copies the conditions list from a template item to a LootItem
    for (LootStoreItemList::const_iterator _iter = Entries.begin(); _iter != Entries.end(); ++_iter)
    {
        LootStoreItem* item = *_iter;
        if (item->ItemId != li->ItemId)
            continue;

        li->conditions = item->conditions;
        break;
    }
}

// Rolls for every item in the template and adds the rolled items the the loot
void LootTemplate::Process(Loot& loot, bool rate, uint16 lootMode, uint8 groupId) const
{
    if (groupId)                                            // Group Reference uses own processing of the group
    {
        if (groupId > Groups.size())
            return;                                         // Error message already printed at loading stage

        if (!Groups[groupId - 1])
            return;

        Groups[groupId - 1]->Process(loot, lootMode);
        return;
    }

    // Rolling non-grouped items
    for (LootStoreItemList::const_iterator i = Entries.begin(); i != Entries.end(); ++i)
    {
        LootStoreItem* item = *i;
        if (!(item->LootMode & lootMode))                       // Do not add if mode mismatch
            continue;

        if (!item->Roll(rate))
            continue;                                           // Bad luck for the entry

        if (item->Reference > 0)                            // References processing
        {
            LootTemplate const* Referenced = LootTemplates_Reference.GetLootFor(item->Reference);
            if (!Referenced)
                continue;                                       // Error message already printed at loading stage

            uint32 maxcount = uint32(float(item->MaxCount) * sWorld->getRate(RATE_DROP_ITEM_REFERENCED_AMOUNT));
            for (uint32 loop = 0; loop < maxcount; ++loop)      // Ref multiplicator
                Referenced->Process(loot, rate, lootMode, item->GroupId);
        }
        else                                                    // Plain entries (not a Reference, not grouped)
            loot.AddItem(*item);                                // Chance is already checked, just add
    }

    // Now processing groups
    for (LootGroups::const_iterator i = Groups.begin(); i != Groups.end(); ++i)
        if (LootGroup* group = *i)
            group->Process(loot, lootMode);
}

// Personal Loot
void LootTemplate::ProcessPersonalLoot(Loot& loot, bool rate, uint16 lootMode, Group* group) const
{
    std::unordered_map<uint32, LootStoreItemList> itemList;

    // Rolling non-grouped items
    for (LootStoreItemList::const_iterator i = Entries.begin(); i != Entries.end(); ++i)
    {
        LootStoreItem* item = *i;
        if (!(item->LootMode & lootMode))                       // Do not add if mode mismatch
            continue;

        if (item->Reference > 0)                            // References processing
        {
            if (!item->Roll(rate))
                continue;

            LootTemplate const* Referenced = LootTemplates_Reference.GetLootFor(item->Reference);
            if (!Referenced)
                continue;                                       // Error message already printed at loading stage

            uint32 maxcount = uint32(float(item->MaxCount) * sWorld->getRate(RATE_DROP_ITEM_REFERENCED_AMOUNT));
            for (uint32 loop = 0; loop < maxcount; ++loop)      // Ref multiplicator
                Referenced->Process(loot, rate, lootMode, item->GroupId);
        }
        else
        {
            ItemTemplate const* proto = sObjectMgr->GetItemTemplate(item->ItemId);
            if (!proto)
                continue;

            std::bitset<MAX_CLASSES * MAX_SPECIALIZATIONS> const& specs = proto->Specializations[1];
            if (specs.any())
            {
                // if all specs are included, than change specs to default
                if (specs.all())
                    itemList[CHAR_SPECIALIZATION_NONE].push_back(*i);
                else
                {
                    for (uint32 count = 0; count < (MAX_CLASSES * MAX_SPECIALIZATIONS); ++count)
                        if (specs[count])
                            itemList[specs[count]].push_back(*i);
                }
            }
            else
            {
                if (!item->Roll(rate))
                    continue;

                loot.AddItem(*item);
            }
        }
    }

    // Now processing groups
    for (LootGroups::const_iterator i = Groups.begin(); i != Groups.end(); ++i)
        if (LootGroup* group = *i)
            group->Process(loot, lootMode);

    // Now Distribute Personal Loot
    if (itemList.empty())
        return;

    for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
        if (Player* player = itr->GetSource())
        {
            uint32 itemId = 0;
            uint32 spec = CHAR_SPECIALIZATION_NONE;

            // 20% chance
            // otherwise, receive consolation bag
            if (Math::RollUnder(20.0f))
            {
                spec = player->GetUInt32Value(PLAYER_LOOT_SPEC_ID);
                if (!spec)
                    spec = player->GetDefaultSpecId();
            }

            LootStoreItemList& items = itemList[spec];
            if (items.empty())
                items = itemList[CHAR_SPECIALIZATION_NONE];

            if (LootStoreItem* item = Trinity::Containers::SelectRandomContainerElement(items))
                itemId = item->ItemId;

            if (!itemId)
                continue;

            int32 ItemRandomPropId = Item::GenerateItemRandomPropertyId(itemId);

            ItemPosCountVec dest;
            InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, 1);
            if (msg != EQUIP_ERR_OK)
            {
                player->SendEquipError(msg, NULL, NULL, itemId);
                player->SendItemRetrievalMail(itemId, 1);
                continue;
            }

            Item* newitem = player->StoreNewItem(dest, itemId, true, ItemRandomPropId);
            newitem->SetPersonalLooted(true);
            player->SendNewItem(newitem, 1, false, false, true);

            player->SendDisplayToast(TOAST_TYPE_ITEM, itemId, 1, false, newitem, spec);
        }
}

void LootTemplate::ProcessBonusLoot(Player* player) const
{
    player->RemoveAurasByType(SPELL_AURA_SHOW_CONFIRMATION_PROMPT);

    std::unordered_map<uint32, LootStoreItemList> itemList;

    // Rolling non-grouped items
    for (LootStoreItemList::const_iterator i = Entries.begin(); i != Entries.end(); ++i)
    {
        LootStoreItem* item = *i;

        if (item->Reference > 0)
            continue;

        ItemTemplate const* proto = sObjectMgr->GetItemTemplate(item->ItemId);
        if (!proto)
            continue;

        std::bitset<MAX_CLASSES * MAX_SPECIALIZATIONS> const& specs = proto->Specializations[1];
        if (specs.any())
        {
            // if all specs are included, than change specs to default
            if (specs.all())
                itemList[CHAR_SPECIALIZATION_NONE].push_back(*i);
            else
            {
                for (uint32 count = 0; count < (MAX_CLASSES * MAX_SPECIALIZATIONS); ++count)
                    if (specs[count])
                        itemList[specs[count]].push_back(*i);
            }
        }
    }

    // Now Distribute Bonus Loot
    uint32 itemId = 0;
    uint32 spec = CHAR_SPECIALIZATION_NONE;

    // 20% base chance
    // every bad luck protection stack increase it by 5%
    // otherwise, receive money
    float ItemChance = float(BONUS_LOOT_BAD_LUCK_BASE_CHANCE + player->GetBonusLootBadLuckProtectionValue());
    if (Math::RollUnder(ItemChance))
    {
        spec = player->GetUInt32Value(PLAYER_LOOT_SPEC_ID);
        if (!spec)
            spec = player->GetDefaultSpecId();
    }

    if (!itemList.empty())
    {
        LootStoreItemList& items = itemList[spec];
        if (items.empty())
            items = itemList[CHAR_SPECIALIZATION_NONE];

        if (LootStoreItem* item = Trinity::Containers::SelectRandomContainerElement(items))
            itemId = item->ItemId;
    }

    // if itemid not present, send money
    if (!itemId)
    {
        uint32 Gold = BONUS_LOOT_MONEY;
        player->SendDisplayToast(TOAST_TYPE_MONEY, 0, Gold, true, nullptr, spec);
        player->ModifyMoney(Gold);

        player->IncreaseBonusLootBadLuckProtectionValue(BONUS_LOOT_BAD_LUCK_PROTECT_STACK);
    }
    else
    {
        player->ResetBonusLootBadLuckProtection();

        int32 ItemRandomPropId = Item::GenerateItemRandomPropertyId(itemId);

        ItemPosCountVec dest;
        InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, 1);
        if (msg != EQUIP_ERR_OK)
        {
            player->SendEquipError(msg, NULL, NULL, itemId);
            player->SendItemRetrievalMail(itemId, 1);
            return;
        }

        Item* newitem = player->StoreNewItem(dest, itemId, true, ItemRandomPropId);
        player->SendNewItem(newitem, 1, false, false, true, true);

        player->SendDisplayToast(TOAST_TYPE_ITEM, itemId, 1, true, newitem, spec);
    }
}

// True if template includes at least 1 quest drop entry
bool LootTemplate::HasQuestDrop(LootTemplateMap const& store, uint8 groupId) const
{
    if (groupId)                                            // Group Reference
    {
        if (groupId > Groups.size())
            return false;                                   // Error message [should be] already printed at loading stage

        if (!Groups[groupId - 1])
            return false;

        return Groups[groupId-1]->HasQuestDrop();
    }

    for (LootStoreItemList::const_iterator i = Entries.begin(); i != Entries.end(); ++i)
    {
        LootStoreItem* item = *i;
        if (item->Reference > 0)                        // References
        {
            LootTemplateMap::const_iterator Referenced = store.find(item->Reference);
            if (Referenced == store.end())
                continue;                                   // Error message [should be] already printed at loading stage
            if (Referenced->second->HasQuestDrop(store, item->GroupId))
                return true;
        }
        else if (item->NeedsQuest)
            return true;                                    // quest drop found
    }

    // Now processing groups
    for (LootGroups::const_iterator i = Groups.begin(); i != Groups.end(); ++i)
        if (LootGroup* group = *i)
            if (group->HasQuestDrop())
                return true;

    return false;
}

// True if template includes at least 1 quest drop for an active quest of the player
bool LootTemplate::HasQuestDropForPlayer(LootTemplateMap const& store, Player const* player, uint8 groupId) const
{
    if (groupId)                                            // Group Reference
    {
        if (groupId > Groups.size())
            return false;                                   // Error message already printed at loading stage

        if (!Groups[groupId - 1])
            return false;

        return Groups[groupId - 1]->HasQuestDropForPlayer(player);
    }

    // Checking non-grouped entries
    for (LootStoreItemList::const_iterator i = Entries.begin(); i != Entries.end(); ++i)
    {
        LootStoreItem* item = *i;
        if (item->Reference > 0)                        // References processing
        {
            LootTemplateMap::const_iterator Referenced = store.find(item->Reference);
            if (Referenced == store.end())
                continue;                                   // Error message already printed at loading stage
            if (Referenced->second->HasQuestDropForPlayer(store, player, item->GroupId))
                return true;
        }
        else if (player->HasQuestForItem(item->ItemId))
            return true;                                    // active quest drop found
    }

    // Now checking groups
    for (LootGroups::const_iterator i = Groups.begin(); i != Groups.end(); ++i)
        if (LootGroup* group = *i)
            if (group->HasQuestDropForPlayer(player))
                return true;

    return false;
}

// Checks integrity of the template
void LootTemplate::Verify(LootStore const& lootstore, uint32 id) const
{
    // Checking group chances
    for (uint32 i = 0; i < Groups.size(); ++i)
        if (Groups[i])
            Groups[i]->Verify(lootstore, id, i + 1);

    /// @todo References validity checks
}

void LootTemplate::CheckLootRefs(LootTemplateMap const& store, LootIdSet* ref_set) const
{
    for (LootStoreItemList::const_iterator ieItr = Entries.begin(); ieItr != Entries.end(); ++ieItr)
    {
        LootStoreItem* item = *ieItr;
        if (item->Reference > 0)
        {
            if (!LootTemplates_Reference.GetLootFor(item->Reference))
                LootTemplates_Reference.ReportNonExistingId(item->Reference, "Reference", item->ItemId);
            else if (ref_set)
                ref_set->erase(item->Reference);
        }
    }

    for (LootGroups::const_iterator grItr = Groups.begin(); grItr != Groups.end(); ++grItr)
        if (LootGroup* group = *grItr)
            group->CheckLootRefs(store, ref_set);
}

bool LootTemplate::AddConditionItem(Condition* cond)
{
    if (!cond || !cond->isLoaded())//should never happen, checked at loading
    {
        TC_LOG_ERROR("loot", "LootTemplate::addConditionItem: condition is null");
        return false;
    }

    if (!Entries.empty())
    {
        for (LootStoreItemList::iterator i = Entries.begin(); i != Entries.end(); ++i)
        {
            if ((*i)->ItemId == uint32(cond->SourceEntry))
            {
                (*i)->conditions.push_back(cond);
                return true;
            }
        }
    }

    if (!Groups.empty())
    {
        for (LootGroups::iterator groupItr = Groups.begin(); groupItr != Groups.end(); ++groupItr)
        {
            LootGroup* group = *groupItr;
            if (!group)
                continue;

            LootStoreItemList* itemList = group->GetExplicitlyChancedItemList();
            if (!itemList->empty())
            {
                for (LootStoreItemList::iterator i = itemList->begin(); i != itemList->end(); ++i)
                {
                    if ((*i)->ItemId == uint32(cond->SourceEntry))
                    {
                        (*i)->conditions.push_back(cond);
                        return true;
                    }
                }
            }

            itemList = group->GetEqualChancedItemList();
            if (!itemList->empty())
            {
                for (LootStoreItemList::iterator i = itemList->begin(); i != itemList->end(); ++i)
                {
                    if ((*i)->ItemId == uint32(cond->SourceEntry))
                    {
                        (*i)->conditions.push_back(cond);
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool LootTemplate::IsReference(uint32 id)
{
    for (LootStoreItemList::const_iterator ieItr = Entries.begin(); ieItr != Entries.end(); ++ieItr)
        if ((*ieItr)->ItemId == id && (*ieItr)->Reference > 0)
            return true;

    return false;//not found or not Reference
}

void LoadLootTemplates_Creature()
{
    TC_LOG_INFO("server.loading", "Loading creature loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet, lootIdSetUsed;
    uint32 count = LootTemplates_Creature.LoadAndCollectLootIds(lootIdSet);

    // Remove real entries and check loot existence
    CreatureTemplateContainer const* ctc = sObjectMgr->GetCreatureTemplates();
    for (CreatureTemplateContainer::const_iterator itr = ctc->begin(); itr != ctc->end(); ++itr)
    {
        if (uint32 lootid = itr->second.lootid)
        {
            if (lootIdSet.find(lootid) == lootIdSet.end())
                LootTemplates_Creature.ReportNonExistingId(lootid, "Creature", itr->second.Entry);
            else
                lootIdSetUsed.insert(lootid);
        }
    }

    for (LootIdSet::const_iterator itr = lootIdSetUsed.begin(); itr != lootIdSetUsed.end(); ++itr)
        lootIdSet.erase(*itr);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Creature.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u creature loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 creature loot templates. DB table `creature_loot_template` is empty");
}

void LoadLootTemplates_Disenchant()
{
    TC_LOG_INFO("server.loading", "Loading disenchanting loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet, lootIdSetUsed;
    uint32 count = LootTemplates_Disenchant.LoadAndCollectLootIds(lootIdSet);

    for (ItemDisenchantLootEntry const* disenchant : sItemDisenchantLootStore)
    {
        uint32 lootid = disenchant->ID;
        if (lootIdSet.find(lootid) == lootIdSet.end())
            LootTemplates_Disenchant.ReportNonExistingId(lootid);
        else
            lootIdSetUsed.insert(lootid);
    }

    for (LootIdSet::const_iterator itr = lootIdSetUsed.begin(); itr != lootIdSetUsed.end(); ++itr)
        lootIdSet.erase(*itr);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Disenchant.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u disenchanting loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 disenchanting loot templates. DB table `disenchant_loot_template` is empty");
}

void LoadLootTemplates_Fishing()
{
    TC_LOG_INFO("server.loading", "Loading fishing loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    uint32 count = LootTemplates_Fishing.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    for (AreaTableEntry const* areaEntry : sAreaStore)
        if (lootIdSet.find(areaEntry->ID) != lootIdSet.end())
            lootIdSet.erase(areaEntry->ID);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Fishing.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u fishing loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 fishing loot templates. DB table `fishing_loot_template` is empty");
}

void LoadLootTemplates_Gameobject()
{
    TC_LOG_INFO("server.loading", "Loading gameobject loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet, lootIdSetUsed;
    uint32 count = LootTemplates_Gameobject.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    GameObjectTemplateContainer const* gotc = sObjectMgr->GetGameObjectTemplates();
    for (GameObjectTemplateContainer::const_iterator itr = gotc->begin(); itr != gotc->end(); ++itr)
    {
        if (uint32 lootid = itr->second.GetLootId())
        {
            if (lootIdSet.find(lootid) == lootIdSet.end())
                LootTemplates_Gameobject.ReportNonExistingId(lootid, "Gameobject", itr->second.entry);
            else
                lootIdSetUsed.insert(lootid);
        }
    }

    for (LootIdSet::const_iterator itr = lootIdSetUsed.begin(); itr != lootIdSetUsed.end(); ++itr)
        lootIdSet.erase(*itr);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Gameobject.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u gameobject loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 gameobject loot templates. DB table `gameobject_loot_template` is empty");
}

void LoadLootTemplates_Item()
{
    TC_LOG_INFO("server.loading", "Loading item loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    uint32 count = LootTemplates_Item.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    ItemTemplateContainer const* its = sObjectMgr->GetItemTemplateStore();
    for (ItemTemplateContainer::const_iterator itr = its->begin(); itr != its->end(); ++itr)
        if (lootIdSet.find(itr->second.GetId()) != lootIdSet.end() && itr->second.GetFlags() & ITEM_PROTO_FLAG_HAS_LOOT)
            lootIdSet.erase(itr->second.GetId());

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Item.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u item loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 item loot templates. DB table `item_loot_template` is empty");
}

void LoadLootTemplates_Milling()
{
    TC_LOG_INFO("server.loading", "Loading milling loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    uint32 count = LootTemplates_Milling.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    ItemTemplateContainer const* its = sObjectMgr->GetItemTemplateStore();
    for (ItemTemplateContainer::const_iterator itr = its->begin(); itr != its->end(); ++itr)
    {
        if (!(itr->second.GetFlags() & ITEM_PROTO_FLAG_IS_MILLABLE))
            continue;

        if (lootIdSet.find(itr->second.GetId()) != lootIdSet.end())
            lootIdSet.erase(itr->second.GetId());
    }

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Milling.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u milling loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 milling loot templates. DB table `milling_loot_template` is empty");
}

void LoadLootTemplates_Pickpocketing()
{
    TC_LOG_INFO("server.loading", "Loading pickpocketing loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet, lootIdSetUsed;
    uint32 count = LootTemplates_Pickpocketing.LoadAndCollectLootIds(lootIdSet);

    // Remove real entries and check loot existence
    CreatureTemplateContainer const* ctc = sObjectMgr->GetCreatureTemplates();
    for (CreatureTemplateContainer::const_iterator itr = ctc->begin(); itr != ctc->end(); ++itr)
    {
        if (uint32 lootid = itr->second.pickpocketLootId)
        {
            if (lootIdSet.find(lootid) == lootIdSet.end())
                LootTemplates_Pickpocketing.ReportNonExistingId(lootid, "Creature", itr->second.Entry);
            else
                lootIdSetUsed.insert(lootid);
        }
    }

    for (LootIdSet::const_iterator itr = lootIdSetUsed.begin(); itr != lootIdSetUsed.end(); ++itr)
        lootIdSet.erase(*itr);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Pickpocketing.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u pickpocketing loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 pickpocketing loot templates. DB table `pickpocketing_loot_template` is empty");
}

void LoadLootTemplates_Prospecting()
{
    TC_LOG_INFO("server.loading", "Loading prospecting loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    uint32 count = LootTemplates_Prospecting.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    ItemTemplateContainer const* its = sObjectMgr->GetItemTemplateStore();
    for (ItemTemplateContainer::const_iterator itr = its->begin(); itr != its->end(); ++itr)
    {
        if (!(itr->second.GetFlags() & ITEM_PROTO_FLAG_IS_PROSPECTABLE))
            continue;

        if (lootIdSet.find(itr->second.GetId()) != lootIdSet.end())
            lootIdSet.erase(itr->second.GetId());
    }

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Prospecting.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u prospecting loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 prospecting loot templates. DB table `prospecting_loot_template` is empty");
}

void LoadLootTemplates_Mail()
{
    TC_LOG_INFO("server.loading", "Loading mail loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    uint32 count = LootTemplates_Mail.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    for (uint32 i = 1; i < sMailTemplateStore.GetNumRows(); ++i)
        if (sMailTemplateStore.LookupEntry(i))
            if (lootIdSet.find(i) != lootIdSet.end())
                lootIdSet.erase(i);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Mail.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u mail loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 mail loot templates. DB table `mail_loot_template` is empty");
}

void LoadLootTemplates_Skinning()
{
    TC_LOG_INFO("server.loading", "Loading skinning loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet, lootIdSetUsed;
    uint32 count = LootTemplates_Skinning.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    CreatureTemplateContainer const* ctc = sObjectMgr->GetCreatureTemplates();
    for (CreatureTemplateContainer::const_iterator itr = ctc->begin(); itr != ctc->end(); ++itr)
    {
        if (uint32 lootid = itr->second.SkinLootId)
        {
            if (lootIdSet.find(lootid) == lootIdSet.end())
                LootTemplates_Skinning.ReportNonExistingId(lootid, "Creature", itr->second.Entry);
            else
                lootIdSetUsed.insert(lootid);
        }
    }

    for (LootIdSet::const_iterator itr = lootIdSetUsed.begin(); itr != lootIdSetUsed.end(); ++itr)
        lootIdSet.erase(*itr);

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Skinning.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u skinning loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 skinning loot templates. DB table `skinning_loot_template` is empty");
}

void LoadLootTemplates_Spell()
{
    TC_LOG_INFO("server.loading", "Loading spell loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    uint32 count = LootTemplates_Spell.LoadAndCollectLootIds(lootIdSet);

    // remove real entries and check existence loot
    for (uint32 spell_id = 1; spell_id < sSpellMgr->GetSpellInfoStoreSize(); ++spell_id)
    {
        SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spell_id);
        if (!spellInfo)
            continue;

        // possible cases
        if (!spellInfo->IsLootCrafting())
            continue;

        if (lootIdSet.find(spell_id) == lootIdSet.end())
        {
            // not report about not trainable spells (optionally supported by DB)
            // ignore 61756 (Northrend Inscription Research (FAST QA VERSION) for example
            if (!spellInfo->HasAttribute(SPELL_ATTR0_NOT_SHAPESHIFT) || (spellInfo->HasAttribute(SPELL_ATTR0_TRADESPELL)))
            {
                LootTemplates_Spell.ReportNonExistingId(spell_id, "Spell", spellInfo->Id);
            }
        }
        else
            lootIdSet.erase(spell_id);
    }

    // output error for any still listed (not Referenced from appropriate table) ids
    LootTemplates_Spell.ReportUnusedIds(lootIdSet);

    if (count)
        TC_LOG_INFO("server.loading", ">> Loaded %u spell loot templates in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
    else
        TC_LOG_ERROR("server.loading", ">> Loaded 0 spell loot templates. DB table `spell_loot_template` is empty");
}

void LoadLootTemplates_Reference()
{
    TC_LOG_INFO("server.loading", "Loading Reference loot templates...");

    uint32 oldMSTime = getMSTime();

    LootIdSet lootIdSet;
    LootTemplates_Reference.LoadAndCollectLootIds(lootIdSet);

    // check References and remove used
    LootTemplates_Creature.CheckLootRefs(&lootIdSet);
    LootTemplates_Fishing.CheckLootRefs(&lootIdSet);
    LootTemplates_Gameobject.CheckLootRefs(&lootIdSet);
    LootTemplates_Item.CheckLootRefs(&lootIdSet);
    LootTemplates_Milling.CheckLootRefs(&lootIdSet);
    LootTemplates_Pickpocketing.CheckLootRefs(&lootIdSet);
    LootTemplates_Skinning.CheckLootRefs(&lootIdSet);
    LootTemplates_Disenchant.CheckLootRefs(&lootIdSet);
    LootTemplates_Prospecting.CheckLootRefs(&lootIdSet);
    LootTemplates_Mail.CheckLootRefs(&lootIdSet);
    LootTemplates_Reference.CheckLootRefs(&lootIdSet);

    // output error for any still listed ids (not Referenced from any loot table)
    LootTemplates_Reference.ReportUnusedIds(lootIdSet);

    TC_LOG_INFO("server.loading", ">> Loaded refence loot templates in %u ms", GetMSTimeDiffToNow(oldMSTime));
}
