/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_LOOTMGR_H
#define TRINITY_LOOTMGR_H

#include "ItemEnchantmentMgr.h"
#include "ByteBuffer.h"
#include "RefManager.h"
#include "SharedDefines.h"
#include "ConditionMgr.h"
#include "Object.h"
#include "EventProcessor.h"
#include "Util.h"

#include <map>
#include <vector>
#include <list>

enum RollType
{
    ROLL_PASS         = 0,
    ROLL_NEED         = 1,
    ROLL_GREED        = 2,
    ROLL_DISENCHANT   = 3,
    MAX_ROLL_TYPE     = 4
};

enum RollMask
{
    ROLL_FLAG_TYPE_PASS         = 0x01,
    ROLL_FLAG_TYPE_NEED         = 0x02,
    ROLL_FLAG_TYPE_GREED        = 0x04,
    ROLL_FLAG_TYPE_DISENCHANT   = 0x08,

    ROLL_ALL_TYPE_NO_DISENCHANT = 0x07,
    ROLL_ALL_TYPE_MASK          = 0x0F
};

enum LootMisc
{
    ROLL_TIME                           = 60 * IN_MILLISECONDS,

    // BonusLoot
    BONUS_LOOT_TIMER                    = 4 * IN_MILLISECONDS,
    BONUS_LOOT_MONEY                    = 2850 * SILVER,
    BONUS_LOOT_BAD_LUCK_BASE_CHANCE     = 20,
    BONUS_LOOT_BAD_LUCK_PROTECT_STACK   = 5,

    // note: the client cannot show more than 16 items total
    MAX_NR_LOOT_ITEMS                   = 16,

    // unrelated to the number of quest items shown, just for reserve
    MAX_NR_QUEST_ITEMS                  = 3
};

enum LootMethod
{
    FREE_FOR_ALL                = 0,
    ROUND_ROBIN                 = 1,
    MASTER_LOOT                 = 2,
    GROUP_LOOT                  = 3,
    NEED_BEFORE_GREED           = 4
};

enum PermissionTypes
{
    ALL_PERMISSION              = 0,
    GROUP_PERMISSION            = 1,
    MASTER_PERMISSION           = 2,
    RESTRICTED_PERMISSION       = 3,
    ROUND_ROBIN_PERMISSION      = 4,
    OWNER_PERMISSION            = 5,
    NONE_PERMISSION             = 6
};

enum LootItemType
{
    LOOT_ITEM_TYPE_ITEM         = 0,
    LOOT_ITEM_TYPE_CURRENCY     = 1,
};

enum LootType
{
    LOOT_NONE                   = 0,
    LOOT_CORPSE                 = 1,
    LOOT_PICKPOCKETING          = 2,
    LOOT_FISHING                = 3,
    LOOT_DISENCHANTING          = 4,
    LOOT_CONTAINER              = 5,
    LOOT_SKINNING               = 6,
    LOOT_PROSPECTING            = 7,
    LOOT_MILLING                = 8
};

// type of Loot Item in Loot View
enum LootSlotType
{
    LOOT_SLOT_TYPE_MASTER       = 2,            // item can only be distributed by group loot master.
    LOOT_SLOT_TYPE_ALLOW_LOOT   = 3,            // player can loot the item.
    LOOT_SLOT_TYPE_OWNER        = 4,            // ignore binding confirmation and etc, for single player looting
    LOOT_SLOT_TYPE_LOCKED       = 5,            // item is shown in red. player cannot loot.
    LOOT_SLOT_TYPE_ROUND_RUBIN  = 6,            // player can loot the item (for round rubin).
    LOOT_SLOT_TYPE_ROLL_ONGOING = 7             // roll is ongoing. player cannot loot.
};

enum LootError
{
    LOOT_ERROR_NONE                     = -1,   // None
    LOOT_ERROR_DIDNT_KILL               = 0,    // You don't have permission to loot that corpse.
    LOOT_ERROR_TOO_FAR                  = 4,    // You are too far away to loot that corpse.
    LOOT_ERROR_BAD_FACING               = 5,    // You must be facing the corpse to loot it.
    LOOT_ERROR_LOCKED                   = 6,    // NYI Someone is already looting that corpse.
    LOOT_ERROR_NOT_STANDING             = 8,    // You need to be standing up to loot something!
    LOOT_ERROR_STUNNED                  = 9,    // You can't loot anything while stunned!
    LOOT_ERROR_PLAYER_NOT_FOUND         = 10,   // Player not found
    LOOT_ERROR_PLAY_TIME_EXCEEDED       = 11,   // NYI Maximum play time exceeded
    LOOT_ERROR_MASTER_INV_FULL          = 12,   // That player's inventory is full
    LOOT_ERROR_MASTER_UNIQUE_ITEM       = 13,   // Player has too many of that item already
    LOOT_ERROR_MASTER_OTHER             = 14,   // Can't assign item to that player
    LOOT_ERROR_ALREADY_PICKPOCKETED     = 15,   // Your target has already had its pockets picked
    LOOT_ERROR_NOT_WHILE_SHAPESHIFTED   = 16,   // You can't do that while shapeshifted.
    LOOT_ERROR_NO_LOOT                  = 17    // NYI There is no loot
};

enum ConsolationBags
{
    BOUNTY_OF_A_SUNDERED_LAND           = 89810,    // Raid: Dragon Soul (Siege of Wyrmrest Temple, Fall of Deathwing)
    DIVIDENS_OF_THE_EVERLASTING_SPRING  = 95617,    // Raid: Terrace of Endless Spring
    CACHE_OF_MOGUS_RICHES               = 95618,    // Raid: Mogu'shan Vaults (Guardians of Mogu'shan, The Vault of Mysteries)
    AMBER_ENCASED_TREASURE_TOUCH        = 95619,    // Raid: Heart of Fear (The Dread Approach, Nightmare of Shek'zeer)
    TREASUERS_OF_THE_THUNDER_KING       = 95343,    // Raid: Throne of Thunder (Last Stand of the Zandalari, Forgotten Depths, Halls of Flesh-Shaping, Pinnacle of Storms)
    COALESCED_TURMOIL                   = 104271,   // Raid: Siege of Orgrimmar (Vale of Eternal Sorrows, Gates of Retribution, The Underhold, Downfall)

    CACHE_OF_SHA_TOUCHED_GOLD           = 90839,    // WorldBoss: Sha of Anger
    MARAUDERS_GLEAMING_SACK_OF_GOLD     = 90840,    // WorldBoss: Galleon
    SHINY_PILE_OF_REFUSE                = 95601,    // WorldBoss: Oondasta
    STORMTOUCHED_CACHE                  = 95602,    // WorldBoss: Nalak
    CELESTIAL_TREASURE_BOX              = 104272,   // WorldBoss: Chi-Ji, Niuzao, Xuen, Yu'lon
    FLAME_SCARRED_CACHE_OF_OFFERINGS    = 104273    // WorldBoss: Ordos
};

enum DisplayedToastTypes
{
    TOAST_TYPE_ITEM                     = 0,
    TOAST_TYPE_CURRENCY                 = 2,
    TOAST_TYPE_MONEY                    = 3
};

class Player;
class Creature;
class GameObject;
class Item;
class LootStore;

struct LootStoreItem
{
    uint32  ItemId;                                         // id of the item
    uint8   Type;                                           // 0 = item, 1 = currency
    uint32  Reference;                                      // referenced TemplateleId
    float   Chance;                                         // chance to drop for both quest and non-quest items, chance to be used for refs
    bool    NeedsQuest  : 1;                                // quest drop (quest is required for item to drop)
    uint16  LootMode;
    uint8   GroupId     : 7;
    uint8   MinCount;                                       // mincount for drop items
    uint8   MaxCount    : 8;                                // max drop count for the item mincount or Ref multiplicator
    ConditionContainer conditions;                               // additional loot condition

    // Constructor
    // displayid is filled in IsValid() which must be called after
    LootStoreItem(uint32 _itemid, uint8 _type, uint32 _reference, float _chance, bool _NeedsQuest, uint16 _lootmode, uint8 _groupid, int32 _mincount, uint8 _maxcount)
        : ItemId(_itemid), Type(_type), Reference(_reference), Chance(_chance), NeedsQuest(_NeedsQuest),
        LootMode(_lootmode), GroupId(_groupid), MinCount(_mincount), MaxCount(_maxcount)
         { }

    bool Roll(bool rate) const;                             // Checks if the entry takes it's chance (at loot generation)
    bool IsValid(LootStore const& store, uint32 entry) const;
                                                            // Checks correctness of values
};

typedef std::set<uint32> AllowedLooterSet;

struct LootItem
{
    uint32  ItemId;
    uint8   Type;                                           // 0 = item, 1 = currency
    uint32  RandomSuffix;
    int32   RandomPropertyId;
    int32   UpgradeId;
    ConditionContainer conditions;                          // additional loot condition
    AllowedLooterSet allowedGUIDs;
    uint8   Count               : 8;
    bool    IsCurrency          : 1;
    bool    IsLooted            : 1;
    bool    IsBlocked           : 1;
    bool    FreeForAll          : 1;                          // free for all
    bool    IsUnderthreshold    : 1;
    bool    IsCounted           : 1;
    bool    NeedsQuest          : 1;                          // quest drop
    bool    FollowLootRules     : 1;
    bool    CanSave;
    bool    AlreadyAskedForRoll;

    // Constructor, copies most fields from LootStoreItem, generates random count and random suffixes/properties
    // Should be called for non-reference LootStoreItem entries only (reference = 0)
    explicit LootItem(LootStoreItem const& li);

    // Empty constructor for creating an empty LootItem to be filled in with DB data
    LootItem() : ItemId(0), RandomSuffix(0), RandomPropertyId(0), UpgradeId(0), Count(0), IsCurrency(false), IsLooted(false), IsBlocked(false),
                 FreeForAll(false), IsUnderthreshold(false), IsCounted(false), NeedsQuest(false), FollowLootRules(false),
                 CanSave(true), AlreadyAskedForRoll(false) { };

    // Basic checks for player/item compatibility - if false no chance to see the item in the loot
    bool AllowedForPlayer(Player const* player) const;
    void AddAllowedLooter(Player const* player);
    const AllowedLooterSet & GetAllowedLooters() const { return allowedGUIDs; }
};

struct QuestItem
{
    uint8   Index;                                          // position in quest_items;
    bool    IsLooted;

    QuestItem()
        : Index(0), IsLooted(false) { }

    QuestItem(uint8 _index, bool _islooted = false)
        : Index(_index), IsLooted(_islooted) { }
};

struct Loot;
class LootTemplate;

typedef std::vector<QuestItem> QuestItemList;
typedef std::vector<LootItem> LootItemList;
typedef std::map<uint32, QuestItemList*> QuestItemMap;
typedef std::list<LootStoreItem*> LootStoreItemList;
typedef std::unordered_map<uint32, LootTemplate*> LootTemplateMap;

typedef std::set<uint32> LootIdSet;

struct LootItemInfo
{
    uint8 Index;
    uint32 Slot;
    LootItem* Item;

    LootItemInfo()
        : Index(0), Slot(0), Item(NULL){ }

    LootItemInfo(uint32 index, uint32 slot, LootItem* item)
        : Index(index), Slot(slot), Item(item) { }
};

class LootStore
{
    public:
        explicit LootStore(char const* name, char const* entryName, bool ratesAllowed)
            : Name(name), EntryName(entryName), RatesAllowed(ratesAllowed) { }

        virtual ~LootStore() { Clear(); }

        void Verify() const;

        uint32 LoadAndCollectLootIds(LootIdSet& ids_set);
        void CheckLootRefs(LootIdSet* ref_set = NULL) const; // check existence reference and remove it from ref_set
        void ReportUnusedIds(LootIdSet const& ids_set) const;
        void ReportNonExistingId(uint32 lootId) const;
        void ReportNonExistingId(uint32 lootId, const char* ownerType, uint32 ownerId) const;

        bool HaveLootFor(uint32 loot_id) const { return lootTemplates.find(loot_id) != lootTemplates.end(); }
        bool HaveQuestLootFor(uint32 loot_id) const;
        bool HaveQuestLootForPlayer(uint32 loot_id, Player* player) const;

        LootTemplate const* GetLootFor(uint32 loot_id) const;
        void ResetConditions();
        LootTemplate* GetLootForConditionFill(uint32 loot_id);

        char const* GetName() const { return Name; }
        char const* GetEntryName() const { return EntryName; }
        bool IsRatesAllowed() const { return RatesAllowed; }

    protected:
        uint32 LoadLootTable();
        void Clear();

    private:
        LootTemplateMap lootTemplates;
        char const* Name;
        char const* EntryName;
        bool RatesAllowed;
};

class LootTemplate
{
    class LootGroup;                                       // A set of loot definitions for items (refs are not allowed inside)
    typedef std::vector<LootGroup*> LootGroups;

    public:
        LootTemplate() { }
        ~LootTemplate();

        // Adds an entry to the group (at loading stage)
        void AddEntry(LootStoreItem* item);
        // Rolls for every item in the template and adds the rolled items the the loot
        void Process(Loot& loot, bool rate, uint16 lootMode, uint8 groupId = 0) const;
        void CopyConditions(const ConditionContainer& conditions);
        void CopyConditions(LootItem* li) const;

        // True if template includes at least 1 quest drop entry
        bool HasQuestDrop(LootTemplateMap const& store, uint8 groupId = 0) const;
        // True if template includes at least 1 quest drop for an active quest of the player
        bool HasQuestDropForPlayer(LootTemplateMap const& store, Player const* player, uint8 groupId = 0) const;

        // Checks integrity of the template
        void Verify(LootStore const& store, uint32 Id) const;
        void CheckLootRefs(LootTemplateMap const& store, LootIdSet* ref_set) const;
        bool AddConditionItem(Condition* cond);
        bool IsReference(uint32 id);

        // Personal Loot
        void ProcessPersonalLoot(Loot& loot, bool rate, uint16 lootMode, Group* group) const;

        // Bonus Loot
        void ProcessBonusLoot(Player* player) const;

    private:
        LootStoreItemList Entries;                          // not grouped only
        LootGroups        Groups;                           // groups have own (optimised) processing, grouped entries go there

        // Objects of this class must never be copied, we are storing pointers in container
        LootTemplate(LootTemplate const&) = delete;
        LootTemplate& operator=(LootTemplate const&) = delete;
};

//=====================================================

class LootValidatorRef : public Reference<Loot, LootValidatorRef>
{
    public:
        LootValidatorRef() { }
        void TargetObjectDestroyLink() override { }
        void SourceObjectDestroyLink() override { }
};

//=====================================================

class LootValidatorRefManager : public RefManager<Loot, LootValidatorRef>
{
    public:
        typedef LinkedListHead::Iterator< LootValidatorRef > iterator;

        LootValidatorRef* getFirst() { return (LootValidatorRef*)RefManager<Loot, LootValidatorRef>::getFirst(); }

        iterator begin() { return iterator(getFirst()); }
        iterator end()   { return iterator(nullptr); }
};

//=====================================================
struct Loot
{
    QuestItemMap const& GetPlayerCurrencies() const { return PlayerCurrencies; }
    QuestItemMap const& GetPlayerQuestItems() const { return PlayerQuestItems; }
    QuestItemMap const& GetPlayerFFAItems() const { return PlayerFFAItems; }
    QuestItemMap const& GetPlayerNonQuestNonFFANonCurrencyConditionalItems() const { return PlayerNonQuestNonFFANonCurrencyConditionalItems; }

    Loot(Object* owner, Player const* recipient, LootType type = LOOT_NONE, SkillType skillId = SKILL_NONE);
    ~Loot();

    static Loot* CreatePickPocketLoot(Creature* creature, Player* looter);
    static Loot* CreateCreatureLoot(Creature* creature, Player* looter);
    static Loot* CreateSkinningLoot(Creature* creature, Player* looter);
    static Loot* CreateInsigniaLoot(Player* player, Player* looter);
    static Loot* CreateFishingLoot(GameObject* go, Player* looter, uint16 lootMode = LOOT_MODE_DEFAULT);
    static Loot* CreateGameObjectLoot(GameObject* go, Player* looter, SkillType skillId = SKILL_NONE);
    static Loot* CreateDisenchantingLoot(Item* item, Player* looter);
    static Loot* CreateProspectingLoot(Item* item, Player* looter);
    static Loot* CreateMillingLoot(Item* item, Player* looter);
    static Loot* CreateItemLoot(Item* item, Player* looter);

    ObjectGuid const& GetGUID() const { return _GUID; }

    Object* GetOwner() { return _owner; }

    Player* GetRecipient() const;
    Group* GetRecipientGroup() const;
    bool IsAllowed(Player const* player) const;

    void StartRollTimer(uint32 time) { _rollTimer = time; }

    // Returns false if rolling is ongoing to prevent corpse despawning
    bool UpdateRollTimer(uint32 diff);

    // For deleting items at loot removal since there is no backward interface to the Item()
    void DeleteLootItemFromContainerItemDB(uint32 itemID);
    void DeleteLootMoneyFromContainerItemDB();

    // if loot becomes invalid this reference is used to inform the listener
    void AddLootValidatorRef(LootValidatorRef* pLootValidatorRef)
    {
        _lootValidatorRefManager.insertFirst(pLootValidatorRef);
    }

    void BuildLootResponse(Player* viewer, PermissionTypes permission);

    bool Empty() const { return Items.empty() && Gold == 0; }
    bool IsLooted() const { return Gold == 0 && UnlootedCount == 0; }

    void NotifyItemRemoved(uint8 lootIndex);
    void NotifyQuestItemRemoved(uint8 questIndex);
    void NotifyMoneyRemoved();
    void AddLooter(ObjectGuid GUID) { PlayersLooting.insert(GUID); }
    void RemoveLooter(ObjectGuid GUID) { PlayersLooting.erase(GUID); }
    PermissionTypes GetPermission(Player* player);

    void GenerateMoneyLoot(uint32 minAmount, uint32 maxAmount);
    bool FillLoot(uint32 lootId, LootStore const& store, Player* lootOwner, bool personal, bool noEmptyError = false, uint16 lootMode = LOOT_MODE_DEFAULT);

    // Inserts the item into the loot (called by LootTemplate processors)
    void AddItem(LootStoreItem const & item);

    LootItem* LootItemInSlot(uint32 lootslot, Player* player, QuestItem** qitem = NULL, QuestItem** ffaitem = NULL, QuestItem** conditem = NULL, QuestItem** currency = NULL);
    uint32 GetMaxSlotInLootFor(Player* player) const;
    bool HasItemForAll() const;
    bool HasItemFor(Player* player) const;
    bool HasOverThresholdItem() const;

    std::vector<LootItem> Items;
    std::vector<LootItem> QuestItems;
    uint32 Gold;
    uint8 UnlootedCount;
    ObjectGuid RoundRobinPlayer;                            // GUID of the player having the Round-Robin ownership for the loot. If 0, round robin owner has released.
    LootType Type;                                          // required for achievement system
    uint8 MaxDuplicates;                                    // Max amount of items with the same entry that can drop (default is 1; on 25 man raid mode 3)
    bool PersonalLoot;                                      // Personal loot used in LFR and Pandaria world bosses
    bool IsAoELoot;                                         // For AoE Looting
    SkillType SkillID;                                      // Used for guild perk Bountiful Bags

    // GUID of container that holds this loot (item_instance.entry)
    // Only set for inventory items that can be right-click looted
    ObjectGuid ContainerID;

    private:
        void FillNotNormalLootFor(Player* player, bool presentAtLooting);
        QuestItemList* FillCurrencyLoot(Player* player);
        QuestItemList* FillFFALoot(Player* player);
        QuestItemList* FillQuestLoot(Player* player);
        QuestItemList* FillNonQuestNonFFANonCurrencyConditionalLoot(Player* player, bool presentAtLooting);

        GuidSet PlayersLooting;
        QuestItemMap PlayerCurrencies;
        QuestItemMap PlayerQuestItems;
        QuestItemMap PlayerFFAItems;
        QuestItemMap PlayerNonQuestNonFFANonCurrencyConditionalItems;

        // All rolls are registered here. They need to know, when the loot is not valid anymore
        LootValidatorRefManager _lootValidatorRefManager;

        // Group loot roll timer
        uint32 _rollTimer;

        // Loot GUID
        ObjectGuid _GUID;

        // Owner (object to which this loot belongs to)
        Object* _owner;

        // Recipient
        ObjectGuid _recipient;
        ObjectGuid _recipientGroup;
};

class BonusLootEvent : public BasicEvent
{
    public:
        BonusLootEvent(Player* player, LootTemplate const* tab) : _player(player), _tab(tab) { }

        bool Execute(uint64 /*execTime*/, uint32 /*diff*/) override
        {
            _tab->ProcessBonusLoot(_player);
            return true;
        }

    protected:
        Player* _player;
        LootTemplate const* _tab;
};

extern LootStore LootTemplates_Creature;
extern LootStore LootTemplates_Fishing;
extern LootStore LootTemplates_Gameobject;
extern LootStore LootTemplates_Item;
extern LootStore LootTemplates_Mail;
extern LootStore LootTemplates_Milling;
extern LootStore LootTemplates_Pickpocketing;
extern LootStore LootTemplates_Reference;
extern LootStore LootTemplates_Skinning;
extern LootStore LootTemplates_Disenchant;
extern LootStore LootTemplates_Prospecting;
extern LootStore LootTemplates_Spell;

void LoadLootTemplates_Creature();
void LoadLootTemplates_Fishing();
void LoadLootTemplates_Gameobject();
void LoadLootTemplates_Item();
void LoadLootTemplates_Mail();
void LoadLootTemplates_Milling();
void LoadLootTemplates_Pickpocketing();
void LoadLootTemplates_Skinning();
void LoadLootTemplates_Disenchant();
void LoadLootTemplates_Prospecting();

void LoadLootTemplates_Spell();
void LoadLootTemplates_Reference();

inline void LoadLootTables()
{
    LoadLootTemplates_Creature();
    LoadLootTemplates_Fishing();
    LoadLootTemplates_Gameobject();
    LoadLootTemplates_Item();
    LoadLootTemplates_Mail();
    LoadLootTemplates_Milling();
    LoadLootTemplates_Pickpocketing();
    LoadLootTemplates_Skinning();
    LoadLootTemplates_Disenchant();
    LoadLootTemplates_Prospecting();
    LoadLootTemplates_Spell();

    LoadLootTemplates_Reference();
}

#endif
