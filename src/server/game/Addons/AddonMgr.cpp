/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AddonMgr.h"
#include "DatabaseEnv.h"
#include "DBCStores.h"
#include "Log.h"
#include "Timer.h"

#include <openssl/md5.h>

void AddonMgr::LoadFromDB()
{
    //! Load known addons
    {
        uint32 oldMSTime = getMSTime();

        QueryResult result = CharacterDatabase.Query( "SELECT name, crc FROM addons" );
        if ( result )
        {
            uint32 addonsCount = 0;
            m_knownAddons.reserve( result->GetRowCount() );

            do
            {
                Field* fields = result->Fetch();

                SavedAddon addon;
                addon.Name = fields[ 0 ].GetString();
                addon.CRC = fields[ 1 ].GetUInt32();

                m_knownAddons.push_back( std::move( addon ) );

                ++addonsCount;
            }
            while ( result->NextRow() );

            TC_LOG_INFO( "server.loading", ">> Loaded %u known addons in %u ms", addonsCount, GetMSTimeDiffToNow( oldMSTime ) );
        }
        else
        {
            TC_LOG_INFO( "server.loading", ">> Loaded 0 known addons. DB table `addons` is empty!" );
        }
    }

    //! Load banned addons
    {
        uint32 oldMSTime = getMSTime();

        QueryResult result = CharacterDatabase.Query( "SELECT id, name, version, UNIX_TIMESTAMP(timestamp) FROM banned_addons" );
        if ( result )
        {
            uint32 addonsCount = 0;
            uint32 dbcMaxBannedAddon = sBannedAddOnsStore.GetNumRows();

            m_bannedAddons.reserve( result->GetRowCount() );

            do
            {
                Field* fields = result->Fetch();

                BannedAddon addon;
                addon.Id = fields[ 0 ].GetUInt32() + dbcMaxBannedAddon;
                addon.Timestamp = uint32( fields[ 3 ].GetUInt64() );

                std::string name = fields[ 1 ].GetString();
                std::string version = fields[ 2 ].GetString();

                MD5( reinterpret_cast< const uint8 * >( name.data() ), name.length(), &addon.NameMD5[0] );
                MD5( reinterpret_cast< const uint8 * >( version.data() ), version.length(), &addon.VersionMD5[0] );

                m_bannedAddons.push_back( std::move( addon ) );

                ++addonsCount;
            }
            while ( result->NextRow() );

            TC_LOG_INFO( "server.loading", ">> Loaded %u banned addons in %u ms", addonsCount, GetMSTimeDiffToNow( oldMSTime ) );
        }
        else
        {
            TC_LOG_INFO( "server.loading", ">> Loaded 0 known banned addons. DB table `banned_addons` is empty!" );
        }
    }
}

void AddonMgr::SaveAddon( const AddonInfo& addonInfo )
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement( CHAR_INS_ADDON );
    stmt->setString( 0, addonInfo.Name );
    stmt->setUInt32( 1, addonInfo.CRC );

    CharacterDatabase.Execute( stmt );

    SavedAddon addon;
    addon.Name = addonInfo.Name;
    addon.CRC = addonInfo.CRC;

    m_knownAddons.push_back( std::move( addon ) );
}

boost::optional< const  AddonMgr::SavedAddon& > AddonMgr::GetKnownAddon( const std::string& name ) const
{
    auto it = std::find_if( m_knownAddons.begin(), m_knownAddons.end(), [ &name ]( const SavedAddon& addon )
    {
        return addon.Name == name;
    } );

    if ( it == m_knownAddons.end() )
        return boost::optional< const SavedAddon& >();

    return boost::optional< const SavedAddon& >( *it );
}

const AddonMgr::BannedAddonsList& AddonMgr::GetBannedAddons() const
{
    return m_bannedAddons;
}
