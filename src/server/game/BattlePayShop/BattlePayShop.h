/*
 * Copyright (C) 2015-2017 Theatre of Dreams <http://www.theatreofdreams.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PAY_SHOP_H
#define BATTLE_PAY_SHOP_H

#include "Common.h"
#include "DatabaseEnv.h"
#include "BattlePayShopMgr.h"

class BattlePayShopCharBoost;
class Player;

enum BattlePayShopErrors
{
    BATTLE_PAY_SHOP_ERROR_OK                            = -1,
    BATTLE_PAY_SHOP_ERROR_OTHER                         = 0,
    BATTLE_PAY_SHOP_ERROR_PURCHASE_DENIED               = 1,
    BATTLE_PAY_SHOP_ERROR_PAYMENT_FAILED                = 2,
    BATTLE_PAY_SHOP_ERROR_INSUFFICIENT_BALANCE          = 28,
    BATTLE_PAY_SHOP_ERROR_PARENTAL_CONTROLS_NO_PURCHASE = 34
};

enum BattlePayShopResults
{
    BATTLE_PAY_SHOP_RESULT_OK               = 0,
    BATTLE_PAY_SHOP_RESULT_REGION_LOCKED    = 4
};

enum BattlePayShopPurchasingResults
{
    BATTLE_PAY_SHOP_PURCHASING_RESULT_WAIT_ON_CONFIRMATION  = 0,
    BATTLE_PAY_SHOP_PURCHASING_RESULT_INSTANT_CONFIRMATION  = 1
};

enum BattlePayShopPruchasingStatuses
{
    BATTLE_PAY_SHOP_PURCHASING_STATUS_CONFIRMED_PURCHASE    = 3,
    BATTLE_PAY_SHOP_PURCHASING_STATUS_ENDED_PURCHASE        = 6
};

enum BattlePayShopProductPriceAvailabilityFlags
{
    BATTLE_PAY_SHOP_PRODUCT_FLAG_NONE               = 0x0,
    BATTLE_PAY_SHOP_PRODUCT_FLAG_UNAVAILABLE        = 0x1,
    BATTLE_PAY_SHOP_PRODUCT_FLAG_SHOW_PRODUCT       = 0x4,
    BATTLE_PAY_SHOP_PRODUCT_FLAG_AVAILABLE          = 0x8,
};

enum BattlePayShopSaveProductFlags
{
    BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_OWNED         = 0x1,
    BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_PURCHASED     = 0x2,
    BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_DISTRIBUTED   = 0x4
};

enum BattlePayShopDitributingResults
{
    BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_HIDE_DISTRIBUTION   = 0,
    BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_SHOW_DISTRIBUTION   = 1
};

struct BattlePayShopPurchasedProduct
{
    BattlePayShopPurchasedProduct() : PurchaseID(UI64LIT(0)), product(nullptr) { }

    uint64 PurchaseID;
    BattlePayShopProduct const* product;
};

typedef std::unordered_map<uint32, BattlePayShopPurchasedProduct> PurchasedProductsMap;
typedef std::unordered_map<uint64, BattlePayShopPurchasedProduct> DistributedProductsMap;

typedef std::unordered_set<uint32> ProductsSet;

class BattlePayShop
{
public:
    BattlePayShop(WorldSession* session, uint32 accountId, std::string const& accountName);
    ~BattlePayShop();

    void LoadFromDB(PreparedQueryResult battleShopResult, PreparedQueryResult battleShopProductsResult);
    void SaveToDB();

    bool HasOwnedProduct(uint32 productId) const;

    void ModifyCurrency(uint64 value);
    uint64 GetCurrencyQuantity() const { return _currencyQuantity; }

    void AddPurchasedProduct(uint32 guid, uint64 purchaseId, BattlePayShopProduct const* product);
    void RemovePurchasedProduct(uint32 guid);
    BattlePayShopPurchasedProduct const* GetPurchasedProduct(uint32 guid) const;

    void AddDistributedProduct(uint64 distributedId, BattlePayShopPurchasedProduct const* purchasedProduct);
    void RemoveDistributedProduct(uint64 distributedId);
    BattlePayShopPurchasedProduct const* GetDistributedProduct(uint64 distributedId) const;

    void AddOwnedProduct(uint32 productId, BattlePayShopPurchasedProduct const* purchasedProduct);

    std::string const& GetWalletName() const { return _walletName; }

    // Char Boosting
    void StartBoostingChar(ObjectGuid characterGUID, TeamId team, TalentSpecialization specialization);
    void FinishBoostingChar(std::unordered_map<uint8, uint32> const* itemsToEquip);

    void UpdateCharBoosting(uint32 diff);

    ObjectGuid const& GetBoostingGUID() const { return _boostingGUID; }

    uint32 GetAccountId() const { return _accountID; }

    PurchasedProductsMap const& GetPurchasedProducts() const { return _purchasedProducts; }
    DistributedProductsMap const& GetDistributedProducts() const { return _distributedProducts; }

    void ResetSaveVariables();

private:
    WorldSession* _session;

    ProductsSet _ownedProducts;
    PurchasedProductsMap _purchasedProducts;
    DistributedProductsMap _distributedProducts;

    TeamId _boostingTeam;

    TalentSpecialization _boostingSpecialization;

    std::string _walletName;

    uint64 _currencyQuantity;
    uint64 _spentCurrencyQuantity;

    uint32 _accountID;
    uint32 _charBoostingTimer;
    ObjectGuid _boostingGUID;

    bool _currencyModified;
    bool _startedCharBoost;
    bool _finishedCharBoost;
    bool _productsToSave;

    BattlePayShopCharBoost* _battlePayShopCharBoost;
};

#endif
