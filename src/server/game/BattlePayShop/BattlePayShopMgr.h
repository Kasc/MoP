/*
 * Copyright (C) 2015-2017 Theatre of Dreams <http://www.theatreofdreams.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PAY_SHOP_MGR_H
#define BATTLE_PAY_SHOP_MGR_H

#include "Singleton/Singleton.hpp"

#include "Common.h"
#include "WorldSession.h"

#define MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE       50
#define MAX_BATTLE_PAY_SHOP_PRODUCT_DESCRIPTION_SIZE 500
#define MAX_BATTLE_PAY_SHOP_GROUP_NAME_SIZE          16

#define BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX -1

enum BattlePayShopCurrency
{
    BATTLE_PAY_SHOP_CURRENCY_DOLLAR         = 1,
    BATTLE_PAY_SHOP_CURRENCY_POUND          = 2,
    BATTLE_PAY_SHOP_CURRENCY_BATTLE_COIN    = 3,
    BATTLE_PAY_SHOP_CURRENCY_EURO           = 4,
    BATTLE_PAY_SHOP_CURRENCY_TEST           = 16,
    BATTLE_PAY_SHOP_CURRENCY_END
};

enum BattlePayShopGroupType
{
    BATTLE_PAY_SHOP_GROUP_TYPE_GRID     = 0,
    BATTLE_PAY_SHOP_GROUP_TYPE_SINGLE   = 1,
    BATTLE_PAY_SHOP_GROUP_TYPE_END
};

enum BattlePayShopBannerType
{
    BATTLE_PAY_SHOP_BANNER_TYPE_FEATURED    = 0,
    BATTLE_PAY_SHOP_BANNER_TYPE_DISCOUNT    = 1,
    BATTLE_PAY_SHOP_BANNER_TYPE_NEW         = 2,
    BATTLE_PAY_SHOP_BANNER_TYPE_END
};

enum BattlePayShopChoiseType
{
    BATTLE_PAY_SHOP_CHOISE_TYPE_NORMAL      = 0,
    BATTLE_PAY_SHOP_CHOISE_TYPE_UPGRADE     = 1
};

enum BattlePayShopProductTypes
{
    BATTLE_PAY_SHOP_PRODUCT_TYPE_DEFAULT    = 0,
    BATTLE_PAY_SHOP_PRODUCT_TYPE_RAF_REWARD = 1,
    BATTLE_PAY_SHOP_PRODUCT_TYPE_CHAR_BOOST = 2
};

struct BattlePayShopProduct
{
    BattlePayShopProduct() : Id(0), NormalPrice(0), CurrentPrice(0), ItemId(0), Quantity(0), ChoiceType(0), CreatureDisplayId(0), FileDataId(0), ServiceFlags(0) { }

    int32 Id;
    std::string Title;
    std::string Description;
    uint64 NormalPrice;
    uint64 CurrentPrice;
    uint32 ItemId;
    uint32 Quantity;
    uint32 CreatureDisplayId;
    uint32 FileDataId;
    uint8 ChoiceType;
    uint32 ServiceFlags;
};

struct BattlePayShopGroup
{
    BattlePayShopGroup() : Id(0), Order(0), FileDataId(0), Type(0) { }

    int32 Id;
    uint32 Order;
    std::string Name;
    uint32 FileDataId;
    uint8 Type;
};

struct BattlePayShopEntry
{
    BattlePayShopEntry() : Id(0), Order(0), GroupId(0), ProductId(0), Flags(0), Banner(0) { }

    int32 Id;
    uint32 Order;
    int32 GroupId;
    int32 ProductId;
    uint32 Flags;
    uint8 Banner;
};

typedef std::unordered_map<int32, BattlePayShopProduct> BattlePayShopProductMap;
typedef std::unordered_map<int32, BattlePayShopGroup> BattlePayShopGroupMap;
typedef std::unordered_map<int32, BattlePayShopEntry> BattlePayShopEntryMap;

class BattlePayShopMgr
{
    BattlePayShopMgr();
    ~BattlePayShopMgr();

    friend class Tod::Singleton<BattlePayShopMgr>;

public:
    bool IsStoreEnabled() { return m_enabled; }
    void SetEnableState(bool enabled) { m_enabled = enabled; }

    uint32 GetStoreCurrency() { return m_currency; }
    void SetStoreCurrency(uint32 currency) { m_currency = currency; }

    void Initialize()
    {
        m_enabled = sWorld->getBoolConfig(CONFIG_BATTLE_PAY_SHOP_ENABLED);
        m_currency = sWorld->getIntConfig(CONFIG_BATTLE_PAY_SHOP_CURRENCY);

        LoadFromDB();
    }

    void LoadFromDB();

    bool HasProductId(int32 productId);

    void GetProductsStore(BattlePayShopProductMap& products, bool IsOnCharSelectScreen);
    void GetGroupsStore(BattlePayShopGroupMap& groups, bool IsOnCharSelectScreen);
    void GetEntriesStore(BattlePayShopEntryMap& entries, bool IsOnCharSelectScreen);

    BattlePayShopProduct const* GetProduct(uint32 productId) const;

    bool IsProductUnavailableToPlayer(WorldSession* session, BattlePayShopProduct const& product) const;

    uint64 GeneratePurchaseID();
    uint64 GenerateDistributeID();

private:
    bool HasGroupId(int32 groupId);

    void LoadProductsFromDB();
    void LoadGroupsFromDB();
    void LoadEntriesFromDB();

    BattlePayShopProductMap m_productStore;
    BattlePayShopGroupMap m_groupStore;
    BattlePayShopEntryMap m_shopEntryStore;

    bool m_enabled;
    uint32 m_currency;

    uint64 _purchaseID;
    uint64 _distributeID;
};

#define sBattlePayShopMgr Tod::Singleton<BattlePayShopMgr>::GetSingleton()

#endif
