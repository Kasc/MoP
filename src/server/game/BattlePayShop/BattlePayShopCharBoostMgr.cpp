/*
 * Copyright (C) 2017-2017 Project Aurora
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePayShopCharBoostMgr.h"
#include "DatabaseEnv.h"
#include "DBCStores.h"
#include "DB2Stores.h"
#include "Log.h"
#include "Timer.h"
#include "Player.h"

static uint8 FindEquipSlot(uint32 invType, uint8& alternativeSlot)
{
    switch (invType)
    {
        case INVTYPE_HEAD:
            return EQUIPMENT_SLOT_HEAD;
        case INVTYPE_NECK:
            return EQUIPMENT_SLOT_NECK;
        case INVTYPE_SHOULDERS:
            return EQUIPMENT_SLOT_SHOULDERS;
        case INVTYPE_BODY:
            return EQUIPMENT_SLOT_BODY;
        case INVTYPE_CHEST:
            return EQUIPMENT_SLOT_CHEST;
        case INVTYPE_ROBE:
            return EQUIPMENT_SLOT_CHEST;
        case INVTYPE_WAIST:
            return EQUIPMENT_SLOT_WAIST;
        case INVTYPE_LEGS:
            return EQUIPMENT_SLOT_LEGS;
        case INVTYPE_FEET:
            return EQUIPMENT_SLOT_FEET;
        case INVTYPE_WRISTS:
            return EQUIPMENT_SLOT_WRISTS;
        case INVTYPE_HANDS:
            return EQUIPMENT_SLOT_HANDS;
        case INVTYPE_FINGER:
            alternativeSlot = EQUIPMENT_SLOT_FINGER2;
            return EQUIPMENT_SLOT_FINGER1;
        case INVTYPE_TRINKET:
            alternativeSlot = EQUIPMENT_SLOT_TRINKET2;
            return EQUIPMENT_SLOT_TRINKET1;
        case INVTYPE_CLOAK:
            return EQUIPMENT_SLOT_BACK;
        case INVTYPE_WEAPON:
            alternativeSlot = EQUIPMENT_SLOT_OFFHAND;
            return EQUIPMENT_SLOT_MAINHAND;
        case INVTYPE_SHIELD:
            return EQUIPMENT_SLOT_OFFHAND;
        case INVTYPE_RANGED:
            return EQUIPMENT_SLOT_MAINHAND;
        case INVTYPE_2HWEAPON:
            alternativeSlot = EQUIPMENT_SLOT_OFFHAND;
            return EQUIPMENT_SLOT_MAINHAND;
        case INVTYPE_TABARD:
            return EQUIPMENT_SLOT_TABARD;
        case INVTYPE_WEAPONMAINHAND:
            return EQUIPMENT_SLOT_MAINHAND;
        case INVTYPE_WEAPONOFFHAND:
            return EQUIPMENT_SLOT_OFFHAND;
        case INVTYPE_HOLDABLE:
            return EQUIPMENT_SLOT_OFFHAND;
        case INVTYPE_RANGEDRIGHT:
            return EQUIPMENT_SLOT_MAINHAND;
        default:
            break;
    }

    // no free position
    return NULL_SLOT;
}

BattlePayShopCharBoostMgr::BattlePayShopCharBoostMgr()
{
}

BattlePayShopCharBoostMgr::~BattlePayShopCharBoostMgr()
{
    _equipItems.clear();
    _miscItemsVector.clear();
    _professionsMap.clear();
    _spellsVector.clear();
}

void BattlePayShopCharBoostMgr::LoadFromDB()
{
    _LoadEquipItems();
    _LoadMiscItems();
    _LoadProfessions();
    _LoadSpells();
}

void BattlePayShopCharBoostMgr::_LoadEquipItems()
{
    _equipItems.clear();

    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_CHAR_BOOST_EQUIP_ITEMS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay CharBoost equip items, table `battle_pay_char_boost_equip_items` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        uint32 specializationId = fields[0].GetUInt32();
        uint32 itemId           = fields[1].GetUInt32();

        if (!sChrSpecializationStore.LookupEntry(specializationId))
        {
            TC_LOG_ERROR("sql.sql", "Specialization id %d defined in `battle_pay_char_boost_equip_items` does not exist, skipped!", specializationId);
            return;
        }

        ItemSparseEntry const* item = sItemSparseStore.LookupEntry(itemId);
        if (!item)
        {
            TC_LOG_ERROR("sql.sql", "Item with defined id %d in `battle_pay_char_boost_equip_items` does not exist, skipped!", itemId);
            return;
        }

        uint8 alternativeSlot = 0;
        uint8 equipSlot = FindEquipSlot(item->InventoryType, alternativeSlot);

        if (!_equipItems[specializationId][equipSlot])
            _equipItems[specializationId][equipSlot] = itemId;
        else if (alternativeSlot)
            _equipItems[specializationId][alternativeSlot] = itemId;

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay CharBoost equip items in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlePayShopCharBoostMgr::_LoadMiscItems()
{
    _miscItemsVector.clear();

    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_CHAR_BOOST_MISC_ITEMS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay CharBoost misc items, table `battle_pay_char_boost_misc_items` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        uint32 itemId       = fields[0].GetUInt32();
        int32 itemCount    = fields[1].GetInt32();

        if (!sItemStore.LookupEntry(itemId))
        {
            TC_LOG_ERROR("sql.sql", "Item with defined id %d in `battle_pay_char_boost_misc_items` does not exist, skipped!", itemId);
            return;
        }

        _miscItemsVector.push_back(std::make_pair(itemId, itemCount));

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay CharBoost misc items in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlePayShopCharBoostMgr::_LoadProfessions()
{
    _professionsMap.clear();

    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_CHAR_BOOST_PROFESSIONS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay CharBoost professions, table `battle_pay_char_boost_professions` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        uint32 classId      = fields[0].GetUInt32();
        uint32 profession   = fields[1].GetUInt32();

        if (!sChrClassesStore.LookupEntry(classId))
        {
            TC_LOG_ERROR("sql.sql", "Class id %d defined in `battle_pay_char_boost_professions` does not exist, skipped!", classId);
            return;
        }

        if (!sSkillLineStore.LookupEntry(profession))
        {
            TC_LOG_ERROR("sql.sql", "Skill id %d defined in `battle_pay_char_boost_professions` does not exist, skipped!", profession);
            return;
        }

        _professionsMap[classId].insert(profession);

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay CharBoost professions in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlePayShopCharBoostMgr::_LoadSpells()
{
    _spellsVector.clear();

    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_CHAR_BOOST_SPELLS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay CharBoost spells, table `battle_pay_char_boost_spells` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        uint32 spellId = fields[0].GetUInt32();

        if (!sSpellStore.LookupEntry(spellId))
        {
            TC_LOG_ERROR("sql.sql", "Spell with defined id %d in `battle_pay_char_boost_spells` does not exist, skipped!", spellId);
            return;
        }

        _spellsVector.push_back(spellId);

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay CharBoost spells in %u ms", count, GetMSTimeDiffToNow(oldMSTime));

}

EquipItemsMap const* BattlePayShopCharBoostMgr::GetEquipItems(uint32 specialization) const
{
    auto itr = _equipItems.find(specialization);
    if (itr != _equipItems.end())
        return &itr->second;

    return nullptr;
}

MiscItemsStore const& BattlePayShopCharBoostMgr::GetMiscItems() const
{
    return _miscItemsVector;
}

ProfessionsSet const* BattlePayShopCharBoostMgr::GetProfessions(uint32 classId) const
{
    auto itr = _professionsMap.find(classId);
    if (itr != _professionsMap.end())
        return &itr->second;

    return nullptr;
}

SpellsStore const& BattlePayShopCharBoostMgr::GetSpells() const
{
    return _spellsVector;
}
