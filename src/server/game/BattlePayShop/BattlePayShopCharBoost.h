/*
 * Copyright (C) 2017-2017 Project Aurora
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PAY_SHOP_CHAR_BOOST_H
#define BATTLE_PAY_SHOP_CHAR_BOOST_H

#include "BattlePayShop.h"
#include "BattlePayShopCharBoostMgr.h"

enum MiscStuff
{
    // Items
    ITEM_MAGE_MOUNTAINSAGE_WAND     = 101081,
    ITEM_PRIEST_COMMUNAL_WAND       = 101172,
    ITEM_WARLOCK_FELSOUL_WAND       = 101275,

    // Spells
    SPELL_PLATE_MAIL_ARMOR          = 750,
    SWIFT_PURPLE_WIND_RIDER_SPELL   = 32297, // Horde Mount
    SWIFT_PURPLE_GRYPGON_SPELL      = 32292, // Alliance Mount

    // Misc
    SKILL_ID_FIRST_AID              = 129,
    GOLD_150                        = 150 * GOLD,
    ITEM_BAG_MAX_COUNT              = 4
};

struct StartLocation
{
    int32 mapID;
    Position const pos;
};

StartLocation const startPosition[2] =
{
    { 870, { 786.9484f, 283.6864f, 503.4184f, 3.967363f } },    // alliance: Vale of Eternal Blossoms - Shrine of Seven Stars
    { 870, { 1698.234f, 892.4910f, 470.9241f, 0.384389f } }     // horde: Vale of Eternal Blossoms - Shrine of Two Moons
};

class BattlePayShopCharBoost
{
public:
    BattlePayShopCharBoost(BattlePayShop* battlePayShop);
    ~BattlePayShopCharBoost();

    bool ProcessCharBoost(uint32 guid, TeamId team, TalentSpecialization specialization);

    void GetProfessions(uint32 guid, std::unordered_set<uint32>& professions) const;
    void GetOldTalents(uint32 guid, std::vector<uint32>& spells) const;
    void GetOldSpellsFromSpecialization(uint32 guid, uint32 classId, std::vector<uint32>& spells) const;
    void GetOldEquippedItems(uint32 guid, std::vector<uint32>& items) const;

    uint32 PrepareMail(uint32 guid, SQLTransaction& trans) const;

private:

    BattlePayShop* _battlePayShop;
};

#endif
