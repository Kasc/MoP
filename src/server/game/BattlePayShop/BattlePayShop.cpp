/*
 * Copyright (C) 2015-2017 Theatre of Dreams <http://www.theatreofdreams.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePayShop.h"
#include "Player.h"
#include "BattlePayShopCharBoost.h"

BattlePayShop::BattlePayShop(WorldSession* session, uint32 accountId, std::string const& accountName) : _session(session), _accountID(accountId)
{
    _currencyQuantity = UI64LIT(0);
    _spentCurrencyQuantity = UI64LIT(0);

    _currencyModified = false;
    _startedCharBoost = false;
    _finishedCharBoost = false;
    _productsToSave = false;

    // assume that accountName will be our wallet name
    _walletName = accountName;

    _charBoostingTimer = 30 * IN_MILLISECONDS;
    _battlePayShopCharBoost = nullptr;
    _boostingTeam = TEAM_NEUTRAL;
    _boostingSpecialization = CHAR_SPECIALIZATION_NONE;
}

BattlePayShop::~BattlePayShop()
{
    if (!_finishedCharBoost)
        delete _battlePayShopCharBoost;
}

void BattlePayShop::LoadFromDB(PreparedQueryResult battleShopResult, PreparedQueryResult battleShopProductsResult)
{
    if (battleShopResult)
    {
        Field* fields = battleShopResult->Fetch();

        uint64 currencyQuantity         = fields[0].GetUInt64();
        uint64 spentCurrencyQuantity    = fields[1].GetUInt64();
        uint32 boostingGuid             = fields[2].GetUInt32();
        uint8 boostingTeam              = fields[3].GetUInt8();
        uint16 boostingSpec             = fields[4].GetUInt16();

        _currencyQuantity = currencyQuantity;
        _spentCurrencyQuantity = spentCurrencyQuantity;

        ObjectGuid boostingGUID = ObjectGuid::Create<HighGuid::Player>(boostingGuid);
        if (!boostingGUID.IsEmpty())
            StartBoostingChar(boostingGUID, TeamId(boostingTeam), TalentSpecialization(boostingSpec));
    }

    if (battleShopProductsResult)
    {
        do
        {
            Field* fields = battleShopProductsResult->Fetch();

            uint32 productId = fields[0].GetUInt32();
            uint8 productFlags = fields[1].GetUInt8();

            if (!sBattlePayShopMgr->HasProductId(productId))
            {
                TC_LOG_ERROR("battlepayshop.shop", "BattlePayShop::LoadFromDB: product id %u is invalid, skipped.", productId);
                continue;
            }

            if ((productFlags & BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_OWNED) != 0)
                _ownedProducts.insert(productId);

            BattlePayShopProduct const* product = sBattlePayShopMgr->GetProduct(productId);
            if (!product)
            {
                TC_LOG_ERROR("battlepayshop.shop", "BattlePayShop::LoadFromDB: Product provided by id %u does not exist, skipped.", productId);
                continue;
            }

            uint64 purchaseId = sBattlePayShopMgr->GeneratePurchaseID();

            BattlePayShopPurchasedProduct purchasedProduct;
            purchasedProduct.product = product;
            purchasedProduct.PurchaseID = purchaseId;

            int32 clientToken = -1;

            bool IsPurchased = false;
            if ((productFlags & BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_PURCHASED) != 0)
            {
                _purchasedProducts[uint32(clientToken)] = std::move(purchasedProduct);
                IsPurchased = true;
            }

            if ((productFlags & BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_DISTRIBUTED) != 0)
            {
                uint64 distributeId = sBattlePayShopMgr->GenerateDistributeID();

                if (IsPurchased)
                    _distributedProducts[distributeId] = _purchasedProducts[uint32(clientToken)];
                else
                    _distributedProducts[distributeId] = std::move(purchasedProduct);
            }

        } while (battleShopProductsResult->NextRow());
    }
}

void BattlePayShop::SaveToDB()
{
    SQLTransaction trans = LoginDatabase.BeginTransaction();

    PreparedStatement* stmt = nullptr;

    if (_currencyModified || _startedCharBoost || _finishedCharBoost)
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_ACCOUNT_BATTLE_PAY_SHOP);

        stmt->setUInt64(0, _currencyQuantity);
        stmt->setUInt64(1, _spentCurrencyQuantity);
        stmt->setUInt32(2, _boostingGUID.GetCounter());
        stmt->setUInt8(3, uint8(_boostingTeam));
        stmt->setUInt16(4, uint16(_boostingSpecialization));
        stmt->setUInt32(5, _accountID);

        trans->Append(stmt);
    }

    if (_productsToSave)
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_BATTLE_PAY_SHOP_PRODUCTS);

        stmt->setUInt32(0, _accountID);

        trans->Append(stmt);

        std::unordered_map<uint32, uint8> products;

        for (auto& itr : _ownedProducts)
            products[itr] |= BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_OWNED;

        for (auto& itr : _purchasedProducts)
            products[itr.second.product->Id] |= BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_PURCHASED;

        for (auto& itr : _distributedProducts)
            products[itr.second.product->Id] |= BATTLE_PAY_SHOP_SAVE_PRODUCT_FLAG_DISTRIBUTED;

        for (auto& itr : products)
        {
            stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_BATTLE_PAY_SHOP_PRODUCTS);
            stmt->setUInt32(0, _accountID);
            stmt->setUInt32(1, itr.first);
            stmt->setUInt8(2, itr.second);

            trans->Append(stmt);
        }
    }

    LoginDatabase.CommitTransaction(trans);
}

bool BattlePayShop::HasOwnedProduct(uint32 productId) const
{
    return _ownedProducts.count(productId) > 0;
}

void BattlePayShop::AddPurchasedProduct(uint32 guid, uint64 purchaseId, BattlePayShopProduct const* product)
{
    BattlePayShopPurchasedProduct purchasedProduct;
    purchasedProduct.product = product;
    purchasedProduct.PurchaseID = purchaseId;

    _purchasedProducts[guid] = std::move(purchasedProduct);

    _productsToSave = true;
}

void BattlePayShop::RemovePurchasedProduct(uint32 guid)
{
    _purchasedProducts.erase(guid);
}

BattlePayShopPurchasedProduct const* BattlePayShop::GetPurchasedProduct(uint32 guid) const
{
    auto itr = _purchasedProducts.find(guid);
    if (itr != _purchasedProducts.end())
        return &itr->second;

    return nullptr;
}

void BattlePayShop::AddDistributedProduct(uint64 distributedId, BattlePayShopPurchasedProduct const* purchasedProduct)
{
    _distributedProducts[distributedId] = *purchasedProduct;
}

void BattlePayShop::RemoveDistributedProduct(uint64 distributedId)
{
    _distributedProducts.erase(distributedId);
}

BattlePayShopPurchasedProduct const* BattlePayShop::GetDistributedProduct(uint64 distributedId) const
{
    auto itr = _distributedProducts.find(distributedId);
    if (itr != _distributedProducts.end())
        return &itr->second;

    return nullptr;
}

void BattlePayShop::AddOwnedProduct(uint32 productId, BattlePayShopPurchasedProduct const* purchasedProduct)
{
    _ownedProducts.insert(productId);

    BattlePayShopProduct const* product = purchasedProduct->product;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_DELIVERY_ENDED, 3 + 4 + 8);

    data.WriteBits(1, 22); // one item

    data << uint32(product->ItemId);
    data << uint64(purchasedProduct->PurchaseID);

    _session->SendPacket(&data);

    bool HasItem = product->ItemId > 0;
    bool HasServiceFlags = product->ServiceFlags > 0;

    if (HasItem) // Products provided with items
        _session->GetPlayer()->AddItem(product->ItemId, product->Quantity);
    else if (HasServiceFlags) // Services, excluding Char Boosting
        _session->GetPlayer()->SetAtLoginFlag(AtLoginFlags(product->ServiceFlags));
    else // only Character Boosting here
    {
        uint64 distributeId = sBattlePayShopMgr->GenerateDistributeID();

        AddDistributedProduct(distributeId, purchasedProduct);

        WorldPacket data(SMSG_BATTLE_PAY_SHOP_DELIVERY_STARTED, 8);
        data << uint64(distributeId);
        _session->SendPacket(&data);

        _session->SendBattlePayShopDistributingUpdate(ObjectGuid::Empty, distributeId, purchasedProduct, BATTLE_PAY_SHOP_DISTRIBUTING_RESULT_SHOW_DISTRIBUTION);
    }

    _productsToSave = true;
}

void BattlePayShop::ModifyCurrency(uint64 value)
{
    _currencyQuantity -= value;
    _spentCurrencyQuantity += value;

    _currencyModified = true;
}

void BattlePayShop::StartBoostingChar(ObjectGuid characterGUID, TeamId team, TalentSpecialization specialization)
{
    if (_battlePayShopCharBoost)
        return;

    _boostingGUID = characterGUID;
    _boostingTeam = team;
    _boostingSpecialization = specialization;

    _battlePayShopCharBoost = new BattlePayShopCharBoost(this);

    _startedCharBoost = true;
    _finishedCharBoost = false;

    _productsToSave = true;
}

void BattlePayShop::FinishBoostingChar(std::unordered_map<uint8, uint32> const* itemsToEquip)
{
    if (!_battlePayShopCharBoost)
        return;

    _finishedCharBoost = true;

    _productsToSave = true;

    TC_LOG_TRACE("battlepayshop.charboost",
        "CharBoosting: Character boosting for character %s has finished!", _boostingGUID.ToString().c_str());

    // copy _boostingGUID - we want to send it in packet (SMSG_BATTLE_PAY_SHOP_CHAR_BOOST_COMPLETED)
    // clearing _boostingGUID before sending packet is neccesary because it will trigger CMSG_ENUM_CHAR, which is responsible for displaying boosting message (see Player::BuildEnumData)
    // so we must clear _boostingGUID and then send packet SMSG_BATTLE_PAY_SHOP_CHAR_BOOST_COMPLETED.
    ObjectGuid boostedGuid = _boostingGUID;

    // successfully ended boosting
    if (itemsToEquip)
    {
        _boostingGUID.Clear();
        _boostingTeam = TEAM_NEUTRAL;
        _boostingSpecialization = CHAR_SPECIALIZATION_NONE;
    }

    delete _battlePayShopCharBoost;

    SaveToDB();
    ResetSaveVariables();

    _startedCharBoost = false;
    _finishedCharBoost = false;

    WorldPacket data(SMSG_BATTLE_PAY_SHOP_CHAR_BOOST_COMPLETED, 1 + 8 + 3 + (itemsToEquip ? (itemsToEquip->size() * (4)) : 0));

    data.WriteGuidMask(boostedGuid, 2, 0, 7, 5, 3, 4, 1);

    data.WriteBits(itemsToEquip ? itemsToEquip->size() : 0, 22);

    data.WriteGuidMask(boostedGuid, 6);

    data.FlushBits();

    data.WriteGuidBytes(boostedGuid, 7, 2, 6, 5);

    if (itemsToEquip)
        for (auto itr : *itemsToEquip)
            data << uint32(itr.second);

    data.WriteGuidBytes(boostedGuid, 0, 1, 3, 4);

    _session->SendPacket(&data);
}

void BattlePayShop::UpdateCharBoosting(uint32 diff)
{
    if (!_startedCharBoost || _finishedCharBoost)
        return;

    if (_charBoostingTimer <= diff)
    {
        _charBoostingTimer = 30 * IN_MILLISECONDS;
        _startedCharBoost = false;

        if (_battlePayShopCharBoost)
            if (!_battlePayShopCharBoost->ProcessCharBoost(_boostingGUID.GetCounter(), _boostingTeam, _boostingSpecialization))
                FinishBoostingChar(nullptr);
    }
    else
        _charBoostingTimer -= diff;
}

void BattlePayShop::ResetSaveVariables()
{
    _currencyModified = false;
    _productsToSave = false;
}