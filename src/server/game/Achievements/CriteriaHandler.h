/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CriteriaHandler_h__
#define CriteriaHandler_h__

#include "DBCEnums.h"
#include "ObjectGuid.h"
#include "Transaction.h"
#include "Common.h"

#include "Singleton/Singleton.hpp"

class Player;
class Unit;
class WorldPacket;
struct AchievementEntry;
struct CriteriaEntry;
struct CriteriaTreeEntry;
struct ModifierTreeEntry;
struct ScenarioStepEntry;

struct ModifierTreeNode
{
    ModifierTreeEntry const* Entry;
    std::vector<ModifierTreeNode const*> Children;
};

enum CriteriaFlagsCu
{
    CRITERIA_FLAG_CU_PLAYER                 = 0x01,
    CRITERIA_FLAG_CU_ACCOUNT                = 0x02,
    CRITERIA_FLAG_CU_GUILD                  = 0x04,
    CRITERIA_FLAG_CU_SCENARIO               = 0x08,
    CRITERIA_FLAG_CU_SHOW_CRITERIA_MEMBERS  = 0x10
};

struct Criteria
{
    uint32 ID = 0;
    CriteriaEntry const* Entry = nullptr;
    ModifierTreeNode const* Modifier = nullptr;
    uint32 FlagsCu = 0;
};

typedef std::vector<Criteria const*> CriteriaList;

struct CriteriaTree
{
    uint32 ID = 0;
    CriteriaTreeEntry const* Entry = nullptr;
    AchievementEntry const* Achievement = nullptr;
    ScenarioStepEntry const* ScenarioStep = nullptr;
    struct Criteria const* Criteria = nullptr;
    std::vector<CriteriaTree const*> Children;
};

typedef std::vector<CriteriaTree const*> CriteriaTreeList;

enum AchievementsAndCriteriaDBStatus
{
    ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE      = 0,
    ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW       = 1,
    ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE    = 2,
    ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED   = 3
};

struct CriteriaProgress
{
    uint64 Counter = 0;
    time_t Date = time_t(0);            // latest update time.
    ObjectGuid PlayerGUID;              // GUID of the player that last updated the criteria (for guild achievements)
    bool LoadedFromDB = false;
    AchievementsAndCriteriaDBStatus DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
};

enum ProgressType
{
    PROGRESS_SET,
    PROGRESS_ACCUMULATE,
    PROGRESS_HIGHEST
};

typedef std::unordered_map<uint32, CriteriaProgress> CriteriaProgressMap;

class CriteriaHandler
{
public:
    CriteriaHandler();
    virtual ~CriteriaHandler();

    void UpdateCriteria(CriteriaTypes type, uint64 miscValue1 = 0, uint64 miscValue2 = 0, uint64 miscValue3 = 0, Unit const* unit = nullptr, Player* referencePlayer = nullptr);

    virtual void SendAllData(Player* receiver) const = 0;

    void UpdateTimedCriteria(uint32 timeDiff);
    void StartCriteriaTimer(CriteriaTimedTypes type, uint32 entry, uint32 timeLost = 0);
    void RemoveCriteriaTimer(CriteriaTimedTypes type, uint32 entry);   // used for quest and scripted timed s

protected:
    virtual void SendCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const = 0;
    virtual void SendAccountCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const = 0;

    CriteriaProgress* GetCriteriaProgress(Criteria const* entry);
    void SetCriteriaProgress(Criteria const* criteria, uint64 changeValue, Player* referencePlayer, ProgressType progressType = PROGRESS_SET);
    void RemoveCriteriaProgress(Criteria const* criteria);
    virtual void SendCriteriaProgressRemoved(uint32 criteriaId) = 0;

    bool IsCompletedCriteriaTree(CriteriaTree const* tree);
    virtual bool CanUpdateCriteriaTree(Criteria const* criteria, CriteriaTree const* tree, Player* referencePlayer) const;
    virtual bool CanCompleteCriteriaTree(CriteriaTree const* tree);
    virtual void CompletedCriteriaTree(CriteriaTree const* tree, Player* referencePlayer) = 0;
    virtual void AfterCriteriaTreeUpdate(CriteriaTree const* /*tree*/, Player* /*referencePlayer*/) { }

    bool IsCompletedCriteria(Criteria const* criteria, uint64 requiredAmount);
    bool CanUpdateCriteria(Criteria const* criteria, CriteriaTreeList const* trees, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer);

    virtual void SendPacket(WorldPacket* data) const = 0;

    bool ConditionsSatisfied(CriteriaEntry const* criteria, Player* referencePlayer) const;
    bool RequirementsSatisfied(Criteria const* criteria, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) const;
    virtual bool RequiredAchievementSatisfied(uint32 /*achievementId*/) const { return false; }
    bool AdditionalRequirementsSatisfied(ModifierTreeNode const* parent, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) const;
    bool AdditionalRequirementSatisfied(ModifierTreeEntry const* modifier, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) const;

    virtual std::string GetOwnerInfo() const = 0;
    virtual CriteriaList const& GetCriteriaByType(CriteriaTypes type) const = 0;

    CriteriaProgressMap _criteriaProgress;
    std::map<uint32, uint32 /*ms time left*/> _timeCriteriaTrees;
};

class CriteriaMgr
{
    CriteriaMgr() { }
    ~CriteriaMgr();

public:
    static char const* GetCriteriaTypeString(CriteriaTypes type);
    static char const* GetCriteriaTypeString(uint32 type);

    friend class Tod::Singleton<CriteriaMgr>;

    CriteriaList const& GetPlayerCriteriaByType(CriteriaTypes type) const
    {
        return _criteriasByType[type];
    }

    CriteriaList const& GetGuildCriteriaByType(CriteriaTypes type) const
    {
        return _guildCriteriasByType[type];
    }

    CriteriaList const& GetScenarioCriteriaByType(CriteriaTypes type) const
    {
        return _scenarioCriteriasByType[type];
    }

    CriteriaTreeList const* GetCriteriaTreesByCriteria(uint32 criteriaId) const
    {
        auto itr = _criteriaTreeByCriteria.find(criteriaId);
        return itr != _criteriaTreeByCriteria.end() ? &itr->second : nullptr;
    }

    CriteriaList const& GetTimedCriteriaByType(CriteriaTimedTypes type) const
    {
        return _criteriasByTimedType[type];
    }

    CriteriaList const& GetCriteriasByFailEvent(CriteriaCondition condition) const
    {
        return _criteriasByFailEvent[condition];
    }

    template<typename Func>
    static void WalkCriteriaTree(CriteriaTree const* tree, Func const& func)
    {
        for (CriteriaTree const* node : tree->Children)
            WalkCriteriaTree(node, func);

        func(tree);
    }

    void LoadCriteriaModifiersTree();
    void LoadCriteriaList();
    CriteriaTree const* GetCriteriaTree(uint32 criteriaTreeId) const;
    Criteria const* GetCriteria(uint32 criteriaId) const;

private:

    std::unordered_map<uint32, CriteriaTree*> _criteriaTrees;
    std::unordered_map<uint32, Criteria*> _criteria;
    std::unordered_map<uint32, ModifierTreeNode*> _criteriaModifiers;

    std::unordered_map<uint32, CriteriaTreeList> _criteriaTreeByCriteria;

    // store criterias by type to speed up lookup
    CriteriaList _criteriasByType[CRITERIA_TYPE_TOTAL];
    CriteriaList _guildCriteriasByType[CRITERIA_TYPE_TOTAL];
    CriteriaList _scenarioCriteriasByType[CRITERIA_TYPE_TOTAL];

    CriteriaList _criteriasByTimedType[CRITERIA_TIMED_TYPE_MAX];

    CriteriaList _criteriasByFailEvent[CRITERIA_CONDITION_MAX];
};

#define sCriteriaMgr Tod::Singleton<CriteriaMgr>::GetSingleton()

#endif // CriteriaHandler_h__
