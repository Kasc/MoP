/*
* Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
* Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AchievementMgr.h"
#include "CellImpl.h"
#include "Chat.h"
#include "GridNotifiersImpl.h"
#include "Group.h"
#include "GuildMgr.h"
#include "Guild.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "BattlePetMgr.h"
#include "GameTime.h"

struct VisibleAchievementCheck
{
    AchievementEntry const* operator()(std::pair<uint32, CompletedAchievementData> const& val)
    {
        AchievementEntry const* achievement = sAchievementStore.LookupEntry(val.first);
        if (achievement && (achievement->Flags & ACHIEVEMENT_FLAG_HIDDEN) == 0 && val.second.DBStatus != ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            return achievement;
        return nullptr;
    }
};

namespace Trinity
{
    class AchievementChatBuilder
    {
        public:
            AchievementChatBuilder(Player* player, ChatMsg msgtype, int32 textId, uint32 ach_id)
                : i_player(player), i_msgtype(msgtype), i_textId(textId), i_achievementId(ach_id) { }

            void operator()(WorldPacket& data, LocaleConstant loc_idx)
            {
                uint8 gender = i_player->GetGender();

                BroadcastTextEntry const* broadcastText = sBroadcastTextStore.LookupEntry(i_textId);
                if (!broadcastText)
                    return;

                std::string text = sDB2Manager->GetBroadcastTextValue(broadcastText, loc_idx, gender);
                ChatHandler::BuildChatPacket(data, i_msgtype, LANG_UNIVERSAL, i_player, i_player, text, i_achievementId);
            }

        private:
            Player* i_player;
            ChatMsg i_msgtype;
            int32 i_textId;
            uint32 i_achievementId;
    };
} // namespace Trinity

AchievementMgr::AchievementMgr() : _achievementPoints(0) { }

AchievementMgr::~AchievementMgr() { }

/**
* called at player login. The player might have fulfilled some achievements when the achievement system wasn't working yet
*/
void AchievementMgr::CheckAllAchievementCriteria(Player* referencePlayer)
{
    // suppress sending packets
    for (uint32 i = 0; i < CRITERIA_TYPE_TOTAL; ++i)
        UpdateCriteria(CriteriaTypes(i), 0, 0, 0, nullptr, referencePlayer);
}

bool AchievementMgr::HasAchieved(uint32 achievementId) const
{
    auto itr = _completedAchievements.find(achievementId);
    if (itr == _completedAchievements.end())
        return false;

    if (itr->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
        return false;

    return itr->second.CompletedByThisCharacter;
}

bool AchievementMgr::HasAccountAchieved(uint32 achievementId) const
{
    auto itr = _completedAchievements.find(achievementId);
    if (itr == _completedAchievements.end())
        return false;

    if (itr->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
        return false;

    return true;
}

uint32 AchievementMgr::GetAchievementPoints() const
{
    return _achievementPoints;
}

ObjectGuid AchievementMgr::GetFirstAchievedCharacterOnAccount(uint32 achievementId) const
{
    auto itr = _completedAchievements.find(achievementId);
    if (itr == _completedAchievements.end())
        return ObjectGuid::Empty;

    if (itr->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
        return ObjectGuid::Empty;

    return itr->second.FirstGuid;
}

bool AchievementMgr::CanUpdateCriteriaTree(Criteria const* criteria, CriteriaTree const* tree, Player* referencePlayer) const
{
    AchievementEntry const* achievement = tree->Achievement;
    if (!achievement)
        return false;

    if (HasAchieved(achievement->ID))
    {
        TC_LOG_TRACE("criteria.achievement", "AchievementMgr::CanUpdateCriteriaTree: (Id: %u Type %s Achievement %u) Achievement already earned",
            criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type), achievement->ID);
        return false;
    }

    if (achievement->Supercedes != 0)
    {
        bool ParentNotCompleted = false;

        bool IsAccountAchiev = (achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT) != 0;
        if (IsAccountAchiev)
        {
            if (!HasAccountAchieved(achievement->Supercedes))
                ParentNotCompleted = true;
        }
        else if (!HasAchieved(achievement->Supercedes))
            ParentNotCompleted = true;

        if (ParentNotCompleted)
        {
            TC_LOG_TRACE("criteria.achievement", "AchievementMgr::CanUpdateCriteriaTree: (Id: %u Type %s Achievement %u) Not completed parent achievement %u",
                criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type), achievement->ID, achievement->Supercedes);
            return false;
        }
    }

    if (achievement->MapID != -1 && referencePlayer->GetMapId() != uint32(achievement->MapID))
    {
        TC_LOG_TRACE("criteria.achievement", "AchievementMgr::CanUpdateCriteriaTree: (Id: %u Type %s Achievement %u) Wrong map",
            criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type), achievement->ID);
        return false;
    }

    if ((achievement->Faction == ACHIEVEMENT_FACTION_HORDE    && referencePlayer->GetTeam() != HORDE) ||
        (achievement->Faction == ACHIEVEMENT_FACTION_ALLIANCE && referencePlayer->GetTeam() != ALLIANCE))
    {
        TC_LOG_TRACE("criteria.achievement", "AchievementMgr::CanUpdateCriteriaTree: (Id: %u Type %s Achievement %u) Wrong faction",
            criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type), achievement->ID);
        return false;
    }

    return CriteriaHandler::CanUpdateCriteriaTree(criteria, tree, referencePlayer);
}

bool AchievementMgr::CanCompleteCriteriaTree(CriteriaTree const* tree)
{
    AchievementEntry const* achievement = tree->Achievement;
    if (!achievement)
        return false;

    // counter can never complete
    if (achievement->Flags & ACHIEVEMENT_FLAG_COUNTER)
        return false;

    if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_REACH | ACHIEVEMENT_FLAG_REALM_FIRST_KILL))
    {
        // someone on this realm has already completed that achievement
        if (sAchievementMgr->IsRealmCompleted(achievement))
            return false;
    }

    return true;
}

void AchievementMgr::CompletedCriteriaTree(CriteriaTree const* tree, Player* referencePlayer)
{
    AchievementEntry const* achievement = tree->Achievement;
    if (!achievement)
        return;

    // counter can never complete
    if (achievement->Flags & ACHIEVEMENT_FLAG_COUNTER)
        return;

    // already completed and stored
    if (HasAchieved(achievement->ID))
        return;

    if (IsCompletedAchievement(achievement))
        CompletedAchievement(achievement, referencePlayer);
}

void AchievementMgr::AfterCriteriaTreeUpdate(CriteriaTree const* tree, Player* referencePlayer)
{
    AchievementEntry const* achievement = tree->Achievement;
    if (!achievement)
        return;

    // check again the completeness for SUMM and REQ COUNT achievements,
    // as they don't depend on the completed criteria but on the sum of the progress of each individual criteria
    if (achievement->Flags & ACHIEVEMENT_FLAG_SUMM)
        if (IsCompletedAchievement(achievement))
            CompletedAchievement(achievement, referencePlayer);

    if (std::vector<AchievementEntry const*> const* achRefList = sAchievementMgr->GetAchievementByReferencedId(achievement->ID))
        for (AchievementEntry const* refAchievement : *achRefList)
            if (IsCompletedAchievement(refAchievement))
                CompletedAchievement(refAchievement, referencePlayer);
}

bool AchievementMgr::IsCompletedAchievement(AchievementEntry const* entry)
{
    // counter can never complete
    if (entry->Flags & ACHIEVEMENT_FLAG_COUNTER)
        return false;

    CriteriaTree const* tree = sCriteriaMgr->GetCriteriaTree(entry->CriteriaTree);
    if (!tree)
        return false;

    // For SUMM achievements, we have to count the progress of each criteria of the achievement.
    // Oddly, the target count is NOT contained in the achievement, but in each individual criteria
    if (entry->Flags & ACHIEVEMENT_FLAG_SUMM)
    {
        uint64 progress = 0;
        CriteriaMgr::WalkCriteriaTree(tree, [this, &progress](CriteriaTree const* criteriaTree)
        {
            if (criteriaTree->Criteria)
                if (CriteriaProgress const* criteriaProgress = this->GetCriteriaProgress(criteriaTree->Criteria))
                    if (criteriaProgress->DBStatus != ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
                        progress += criteriaProgress->Counter;
        });
        return progress >= tree->Entry->CriteriaCount;
    }

    return IsCompletedCriteriaTree(tree);
}

bool AchievementMgr::RequiredAchievementSatisfied(uint32 achievementId) const
{
    return HasAchieved(achievementId);
}

PlayerAchievementMgr::PlayerAchievementMgr(Player* owner) : _owner(owner)
{
}

void PlayerAchievementMgr::Reset(AchievementEntry const* achievement)
{
    auto itr = _completedAchievements.find(achievement->ID);
    if (itr == _completedAchievements.end())
        return;

    SQLTransaction charTrans = CharacterDatabase.BeginTransaction();
    SQLTransaction authTrans = LoginDatabase.BeginTransaction();

    PreparedStatement* stmt = nullptr;

    bool IsAccountWide = (achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT) != 0;

    if (CriteriaTree const* tree = sCriteriaMgr->GetCriteriaTree(achievement->CriteriaTree))
    {
        for (CriteriaTree const* node : tree->Children)
        {
            uint32 criteriaId = node->ID;

            CriteriaProgressMap::const_iterator progress = _criteriaProgress.find(criteriaId);
            if (progress == _criteriaProgress.end())
                continue;

            SendCriteriaProgressRemoved(progress->first);

            _criteriaProgress.erase(progress);

            if (IsAccountWide)
            {
                stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_CRITERIA_PROGRESS_BY_CRITERIA);
                stmt->setUInt32(0, _owner->GetSession()->GetAccountId());
                stmt->setUInt32(1, criteriaId);
                authTrans->Append(stmt);
            }
            else
            {
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_ACHIEVEMENT_PROGRESS_BY_CRITERIA);
                stmt->setUInt32(0, _owner->GetGUID().GetCounter());
                stmt->setUInt32(1, criteriaId);
                charTrans->Append(stmt);
            }
        }
    }

    WorldPacket data(SMSG_ACHIEVEMENT_DELETED, 4 + 4);

    data << uint32(achievement->ID);
    data << uint32(0); // Immunities (garbage)

    SendPacket(&data);

    _completedAchievements.erase(itr);

    _achievementPoints -= achievement->Points;

    if (IsAccountWide)
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_ACHIEVEMENTS_BY_ACHIEVEMENT);
        stmt->setUInt32(0, _owner->GetSession()->GetAccountId());
        stmt->setUInt16(1, achievement->ID);
        authTrans->Append(stmt);
    }
    else
    {
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_ACHIEVEMENT_BY_ACHIEVEMENT);
        stmt->setUInt32(0, _owner->GetGUID().GetCounter());
        stmt->setUInt32(1, achievement->ID);
        charTrans->Append(stmt);
    }

    CharacterDatabase.CommitTransaction(charTrans);
    LoginDatabase.CommitTransaction(authTrans);
}

void PlayerAchievementMgr::LoadFromDB(PreparedQueryResult achievementResult, PreparedQueryResult criteriaResult, PreparedQueryResult accountAchievementResult, PreparedQueryResult accountCriteriaResult)
{
    // load account achievements first
    if (accountAchievementResult)
    {
        do
        {
            Field* fields = accountAchievementResult->Fetch();
            ObjectGuid first_guid = ObjectGuid::Create<HighGuid::Player>(fields[0].GetUInt32());
            uint32 achievementid = fields[1].GetUInt32();

            // must not happen: cleanup at server startup in sAchievementMgr->LoadCompletedAchievements()
            AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementid);
            if (!achievement)
                continue;

            CompletedAchievementData& ca = _completedAchievements[achievementid];
            ca.Date = time_t(fields[1].GetUInt32());
            ca.LoadedFromDB = true;
            ca.FirstGuid = first_guid;
            ca.CompletedByThisCharacter = false;
            ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;

            _achievementPoints += achievement->Points;
        }
        while (accountAchievementResult->NextRow());
    }

    // load character specific achievements after account achievements
    if (achievementResult)
    {
        do
        {
            Field* fields = achievementResult->Fetch();
            uint32 achievementid = fields[0].GetUInt32();

            // must not happen: cleanup at server startup in sAchievementMgr->LoadCompletedAchievements()
            AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementid);
            if (!achievement)
                continue;

            CompletedAchievementData& ca = _completedAchievements[achievementid];
            ca.Date = time_t(fields[1].GetUInt32());
            ca.LoadedFromDB = true;
            ca.CompletedByThisCharacter = true;
            ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;

            _achievementPoints += achievement->Points;
        } while (achievementResult->NextRow());
    }

    if (criteriaResult)
    {
        time_t now = time(NULL);
        do
        {
            Field* fields = criteriaResult->Fetch();
            uint32 id = fields[0].GetUInt32();
            uint64 counter = fields[1].GetUInt64();
            time_t date = time_t(fields[2].GetUInt32());

            Criteria const* criteria = sCriteriaMgr->GetCriteria(id);
            if (!criteria)
            {
                // Removing non-existing criterias for all characters
                TC_LOG_ERROR("criteria.achievement", "Non-existing achievement criteria %u data has been removed from the table `character_achievement_progress`.", id);

                PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_INVALID_ACHIEV_PROGRESS_CRITERIA);
                stmt->setUInt32(0, id);
                CharacterDatabase.Execute(stmt);

                continue;
            }

            if (criteria->Entry->StartTimer && time_t(date + criteria->Entry->StartTimer) < now)
                continue;

            CriteriaProgress& progress = _criteriaProgress[id];
            progress.Counter = counter;
            progress.Date = date;
            progress.LoadedFromDB = true;
            progress.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
        }
        while (criteriaResult->NextRow());
    }

    if (accountCriteriaResult)
    {
        time_t now = time(NULL);
        do
        {
            Field* fields = accountCriteriaResult->Fetch();
            uint32 id = fields[0].GetUInt32();
            uint64 counter = fields[1].GetUInt64();
            time_t date = time_t(fields[2].GetUInt32());

            Criteria const* criteria = sCriteriaMgr->GetCriteria(id);
            if (!criteria)
            {
                // we will remove not existed criteria for all characters
                TC_LOG_ERROR("achievement", "Non-existing achievement criteria %u data removed from table `account_achievement_progress`.", id);

                PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_INVALID_ACHIEV_PROGRESS_CRITERIA);
                stmt->setUInt32(0, id);
                LoginDatabase.Execute(stmt);
                continue;
            }

            if (criteria->Entry->StartTimer && time_t(date + criteria->Entry->StartTimer) < now)
                continue;

            if (_criteriaProgress.find(id) != _criteriaProgress.end())
            {
                TC_LOG_ERROR("achievement", "Achievement criteria progress %u found in both `character_achievement_progress` and `account_achievement_progress`. Character guid: %u, accountId: %u", id, _owner->GetGUID().GetCounter(), _owner->GetSession()->GetAccountId());
                continue;
            }

            CriteriaProgress& progress = _criteriaProgress[id];
            progress.Counter = counter;
            progress.Date = date;
            progress.LoadedFromDB = true;
            progress.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
        }
        while (accountCriteriaResult->NextRow());
    }
}

void PlayerAchievementMgr::SaveToDB(SQLTransaction& charTrans, SQLTransaction& authTrans)
{
    std::vector<uint32> removedAchievements;
    std::vector<uint32> removedCriterias;

    if (!_completedAchievements.empty())
    {
        for (auto& iter : _completedAchievements)
        {
            if (iter.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE)
                continue;

            uint32 achievementid = iter.first;
            AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementid);
            if (!achievement)
                continue;

            PreparedStatement* stmt = nullptr;

            switch (iter.second.DBStatus)
            {
                case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW:
                    if (achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT)
                    {
                        stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_ACHIEVEMENTS);
                        stmt->setUInt32(0, _owner->GetSession()->GetAccountId());
                        stmt->setUInt32(1, iter.second.FirstGuid.GetCounter());
                        stmt->setUInt32(2, achievementid);
                        stmt->setUInt32(3, uint32(iter.second.Date));
                        authTrans->Append(stmt);
                    }
                    else
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_ACHIEVEMENT);
                        stmt->setUInt32(0, _owner->GetGUID().GetCounter());
                        stmt->setUInt32(1, achievementid);
                        stmt->setUInt32(2, uint32(iter.second.Date));
                        charTrans->Append(stmt);
                    }
                    break;
                case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE:
                    removedAchievements.push_back(achievementid);

                    if (achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT)
                    {
                        stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_ACHIEVEMENTS_BY_ACHIEVEMENT);
                        stmt->setUInt32(0, _owner->GetSession()->GetAccountId());
                        stmt->setUInt32(1, achievementid);
                        authTrans->Append(stmt);
                    }
                    else
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_ACHIEVEMENT_BY_ACHIEVEMENT);
                        stmt->setUInt32(0, _owner->GetGUID().GetCounter());
                        stmt->setUInt32(1, achievementid);
                        charTrans->Append(stmt);
                    }
                    break;
                case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED:
                    if (achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT)
                    {
                        stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_ACCOUNT_ACHIEVEMENTS_BY_ACHIEVEMENT);
                        stmt->setUInt32(0, uint32(iter.second.Date));
                        stmt->setUInt32(1, _owner->GetSession()->GetAccountId());
                        stmt->setUInt32(2, achievementid);
                        authTrans->Append(stmt);
                    }
                    else
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_ACHIEVEMENT_BY_ACHIEVEMENT);
                        stmt->setUInt32(0, uint32(iter.second.Date));
                        stmt->setUInt32(1, _owner->GetGUID().GetCounter());
                        stmt->setUInt32(2, achievementid);
                        charTrans->Append(stmt);
                    }
                    break;
                default:
                    break;
            }

            iter.second.LoadedFromDB = true;
            iter.second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
        }
    }

    if (!_criteriaProgress.empty())
    {
        for (auto& iter : _criteriaProgress)
        {
            if (iter.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE)
                continue;

            if (!iter.second.Counter && iter.second.LoadedFromDB)
                iter.second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE;

            uint32 id = iter.first;
            Criteria const* criteria = sCriteriaMgr->GetCriteria(id);
            if (!criteria)
                continue;

            PreparedStatement* stmt = nullptr;

            switch (iter.second.DBStatus)
            {
                case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW:
                    if (criteria->FlagsCu & CRITERIA_FLAG_CU_ACCOUNT)
                    {
                        stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_CRITERIA_PROGRESS);
                        stmt->setUInt32(0, _owner->GetSession()->GetAccountId());
                        stmt->setUInt32(1, id);
                        stmt->setUInt64(2, iter.second.Counter);
                        stmt->setUInt32(3, uint32(iter.second.Date));
                        authTrans->Append(stmt);
                    }
                    else
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_ACHIEVEMENT_PROGRESS);
                        stmt->setUInt32(0, _owner->GetGUID().GetCounter());
                        stmt->setUInt32(1, iter.first);
                        stmt->setUInt64(2, iter.second.Counter);
                        stmt->setUInt32(3, uint32(iter.second.Date));
                        charTrans->Append(stmt);
                    }
                    break;
                case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE:
                    removedCriterias.push_back(id);

                    if (criteria->FlagsCu & CRITERIA_FLAG_CU_ACCOUNT)
                    {
                        stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_CRITERIA_PROGRESS_BY_CRITERIA);
                        stmt->setUInt32(0, _owner->GetSession()->GetAccountId());
                        stmt->setUInt32(1, id);
                        authTrans->Append(stmt);
                    }
                    else
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_ACHIEVEMENT_PROGRESS_BY_CRITERIA);
                        stmt->setUInt32(0, _owner->GetGUID().GetCounter());
                        stmt->setUInt32(1, id);
                        charTrans->Append(stmt);
                    }
                    break;
                case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED:
                    if (criteria->FlagsCu & CRITERIA_FLAG_CU_ACCOUNT)
                    {
                        stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_ACCOUNT_CRITERIA_PROGRESS);
                        stmt->setUInt64(0, iter.second.Counter);
                        stmt->setUInt32(1, uint32(iter.second.Date));
                        stmt->setUInt32(2, _owner->GetSession()->GetAccountId());
                        stmt->setUInt32(3, id);
                        authTrans->Append(stmt);
                    }
                    else
                    {
                        stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_ACHIEVEMENT_PROGRESS);
                        stmt->setUInt64(0, iter.second.Counter);
                        stmt->setUInt32(1, uint32(iter.second.Date));
                        stmt->setUInt32(2, _owner->GetGUID().GetCounter());
                        stmt->setUInt32(3, iter.first);
                        charTrans->Append(stmt);
                    }
                    break;
                default:
                    break;
            }

            iter.second.LoadedFromDB = true;
            iter.second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
        }
    }

    for (uint32 achievId : removedAchievements)
        _completedAchievements.erase(achievId);

    for (uint32 criteriaId : removedCriterias)
        _criteriaProgress.erase(criteriaId);

    removedAchievements.clear();
    removedCriterias.clear();
}

void PlayerAchievementMgr::ResetCriteria(CriteriaCondition condition, uint64 miscValue1, bool evenIfCriteriaComplete)
{
    TC_LOG_DEBUG("criteria.achievement", "PlayerAchievementMgr::ResetCriteria(%u, " UI64FMTD ")", condition, miscValue1);

    // disable for gamemasters with GM-mode enabled
    if (_owner->IsGameMaster())
        return;

    CriteriaList const& achievementCriteriaList = sCriteriaMgr->GetCriteriasByFailEvent(condition);
    for (Criteria const* achievementCriteria : achievementCriteriaList)
    {
        if (achievementCriteria->Entry->FailEvent != condition || (achievementCriteria->Entry->FailAsset && achievementCriteria->Entry->FailAsset != miscValue1))
            continue;

        std::vector<CriteriaTree const*> const* trees = sCriteriaMgr->GetCriteriaTreesByCriteria(achievementCriteria->ID);
        bool allComplete = true;
        for (CriteriaTree const* tree : *trees)
        {
            // don't update already completed criteria if not forced or achievement already complete
            if (!(IsCompletedCriteriaTree(tree) && !evenIfCriteriaComplete) || !HasAchieved(tree->Achievement->ID))
            {
                allComplete = false;
                break;
            }
        }

        if (allComplete)
            continue;

        RemoveCriteriaProgress(achievementCriteria);
    }
}

void PlayerAchievementMgr::SendAllData(Player* /*receiver*/) const
{
    VisibleAchievementCheck filterInvisible;

    std::unordered_map<uint32, std::pair<ObjectGuid, CriteriaProgress>> criteriaList;
    for (auto itr : _criteriaProgress)
    {
        if (itr.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        Criteria const* criteria = sCriteriaMgr->GetCriteria(itr.first);
        if (!criteria || criteria->Entry->Type == CRITERIA_TYPE_COMPLETE_ACHIEVEMENT)
            continue;

        ObjectGuid guid = !(criteria->FlagsCu & CRITERIA_FLAG_CU_ACCOUNT) ? _owner->GetGUID() : ObjectGuid::Empty;

        criteriaList[itr.first] = std::make_pair(guid, itr.second);
    }

    std::unordered_map<uint32, std::pair<ObjectGuid, CompletedAchievementData>> achievementList;
    for (auto itr : _completedAchievements)
    {
        AchievementEntry const* achievement = filterInvisible(itr);
        if (!achievement)
            continue;

        ObjectGuid guid = !(achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT) ? _owner->GetGUID() : ObjectGuid::Empty;

        achievementList[itr.first] = std::make_pair(guid, itr.second);
    }

    ByteBuffer criteriaData;
    ByteBuffer completedData;

    WorldPacket data(SMSG_ALL_ACHIEVEMENT_DATA, 5 +
        criteriaList.size() * (1 + 1 + 8 + 8 + 1 + 4 + 4 + 4 + 4) +
        achievementList.size() * (1 + 1 + 8 + 8 + 4 + 4 + 4 + 4));

    data.WriteBits(criteriaList.size(), 19);

    for (auto & itr : criteriaList)
    {
        ObjectGuid counter = ObjectGuid(itr.second.second.Counter);
        ObjectGuid guid = itr.second.first;

        data.WriteGuidMask(counter, 3);

        data.WriteGuidMask(guid, 3, 6);

        data.WriteGuidMask(counter, 0);

        data.WriteGuidMask(guid, 7);

        data.WriteGuidMask(counter, 1, 5);

        data.WriteGuidMask(guid, 2, 1);

        data.WriteGuidMask(counter, 7);

        data.WriteGuidMask(guid, 4, 0);

        data.WriteGuidMask(counter, 2);

        data.WriteGuidMask(guid, 5);

        data.WriteGuidMask(counter, 4);

        data.WriteBits(0, 4); // Flags

        data.WriteGuidMask(counter, 6);

        criteriaData.WriteGuidBytes(counter, 7);

        criteriaData << uint32(0);                              // TimeFromCreate

        criteriaData.WriteGuidBytes(counter, 6);

        criteriaData.WriteGuidBytes(guid, 1);

        criteriaData << uint32(itr.first);

        criteriaData.WriteGuidBytes(counter, 4);

        criteriaData.WriteGuidBytes(guid, 0, 4, 6);

        criteriaData.WriteGuidBytes(counter, 1, 5);

        criteriaData.WriteGuidBytes(guid, 7, 2);

        criteriaData.WriteGuidBytes(counter, 2, 0);

        criteriaData.WriteGuidBytes(guid, 3);

        criteriaData.WriteGuidBytes(counter, 3);

        criteriaData << uint32(0);                              // TimeFromStart

        criteriaData.WriteGuidBytes(guid, 5);

        criteriaData.AppendPackedTime(itr.second.second.Date);
    }

    data.WriteBits(achievementList.size(), 20);

    for (auto & itr : achievementList)
    {
        ObjectGuid guid = itr.second.first;
        ObjectGuid guid2 = HasAchieved(itr.first) ? guid : itr.second.second.FirstGuid;

        data.WriteGuidMask(guid2, 0, 7, 1, 5, 2, 4, 6, 3);

        completedData << uint32(itr.first);
        completedData << uint32(realmID);

        completedData.WriteGuidBytes(guid2, 5, 7);

        completedData << uint32(realmID);
        completedData.AppendPackedTime(itr.second.second.Date);

        completedData.WriteGuidBytes(guid2, 0, 4, 1, 6, 2, 3);
    }

    data.append(completedData);
    data.append(criteriaData);

    SendPacket(&data);
}

void PlayerAchievementMgr::SendAchievementInfo(Player* receiver, uint32 /*achievementId = 0 */) const
{
    VisibleAchievementCheck filterInvisible;

    std::unordered_map<uint32, CriteriaProgress> criteriaList;
    for (auto itr : _criteriaProgress)
    {
        if (itr.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        Criteria const* criteria = sCriteriaMgr->GetCriteria(itr.first);
        if (!criteria)
            continue;

        criteriaList[itr.first] = itr.second;
    }

    std::unordered_map<uint32, CompletedAchievementData> achievementList;
    for (auto itr : _completedAchievements)
    {
        AchievementEntry const* achievement = filterInvisible(itr);
        if (!achievement)
            continue;

        achievementList[itr.first] = itr.second;
    }

    WorldPacket data(SMSG_RESPOND_INSPECT_ACHIEVEMENTS, 1 + 8 + 5 + achievementList.size() * (1 + 8 + 4 + 4 + 4 + 4) + criteriaList.size() * (2 * (1 + 8) + 1 + 4 + 4 + 4 + 4));

    ObjectGuid guid = _owner->GetGUID();

    ByteBuffer criteriaData;
    ByteBuffer achievementsData;

    data.WriteGuidMask(guid, 3, 6, 0, 2);

    data.WriteBits(achievementList.size(), 20);
    data.WriteBits(criteriaList.size(), 19);

    for (auto & itr : criteriaList)
    {
        ObjectGuid counter = ObjectGuid(itr.second.Counter);
        ObjectGuid guid2 = guid; // for player achievements these guids are the same (only _owner can update own criterias)

        data.WriteGuidMask(guid2, 1, 4, 5);

        data.WriteGuidMask(counter, 7, 4, 3);

        data.WriteGuidMask(guid2, 7, 0, 6);

        data.WriteBits(0, 4);           // criteria progress flags

        data.WriteGuidMask(guid2, 2);

        data.WriteGuidMask(counter, 5, 6, 0, 2, 1);

        data.WriteGuidMask(guid2, 3);

        criteriaData.WriteGuidBytes(counter, 4);

        criteriaData << uint32(0);       // TimeFromStart

        criteriaData.WriteGuidBytes(counter, 1);

        criteriaData.WriteGuidBytes(guid2, 1);

        criteriaData.WriteGuidBytes(counter, 7);

        criteriaData << uint32(itr.first);

        criteriaData.WriteGuidBytes(guid2, 3);

        criteriaData.WriteGuidBytes(counter, 3, 5, 2);

        criteriaData.WriteGuidBytes(guid2, 4);

        criteriaData.WriteGuidBytes(counter, 0);

        criteriaData.WriteGuidBytes(guid2, 0);

        criteriaData << uint32(0);      // TimeFromCreate

        criteriaData.WriteGuidBytes(guid2, 7);

        criteriaData.AppendPackedTime(itr.second.Date);

        criteriaData.WriteGuidBytes(counter, 6);

        criteriaData.WriteGuidBytes(guid2, 2, 6, 5);
    }

    data.WriteGuidMask(guid, 5);

    for (auto & itr : achievementList)
    {
        ObjectGuid guid3 = *(itr.second.CompletingPlayers.begin());

        data.WriteGuidMask(guid3, 0, 2, 5, 4, 3, 6, 1, 7);

        achievementsData.WriteGuidBytes(guid3, 1, 0);

        achievementsData.AppendPackedTime(itr.second.Date);

        achievementsData << uint32(realmID);

        achievementsData << uint32(itr.first);

        achievementsData.WriteGuidBytes(guid3, 7, 4, 6, 2, 3, 5);

        achievementsData << uint32(realmID);
    }

    data.WriteGuidMask(guid, 4, 7, 1);

    data.FlushBits();

    data.WriteGuidBytes(guid, 5);

    data.append(achievementsData);
    data.append(criteriaData);

    data.WriteGuidBytes(guid, 0, 3, 6, 2, 7, 4, 1);

    receiver->SendDirectMessage(&data);
}

void PlayerAchievementMgr::CompletedAchievement(AchievementEntry const* achievement, Player* referencePlayer)
{
    // disable for gamemasters with GM-mode enabled
    if (_owner->IsGameMaster())
        return;

    if ((achievement->Faction == ACHIEVEMENT_FACTION_HORDE    && referencePlayer->GetTeam() != HORDE) ||
        (achievement->Faction == ACHIEVEMENT_FACTION_ALLIANCE && referencePlayer->GetTeam() != ALLIANCE))
        return;

    if (achievement->Flags & ACHIEVEMENT_FLAG_COUNTER || HasAchieved(achievement->ID))
        return;

    if (achievement->Flags & ACHIEVEMENT_FLAG_SHOW_IN_GUILD_NEWS)
        if (Guild* guild = referencePlayer->GetGuild())
            guild->AddGuildNews(GUILD_NEWS_PLAYER_ACHIEVEMENT, referencePlayer->GetGUID(), achievement->Flags & ACHIEVEMENT_FLAG_SHOW_IN_GUILD_HEADER, achievement->ID);

    if (!_owner->GetSession()->PlayerLoading())
        SendAchievementEarned(achievement);

    TC_LOG_DEBUG("criteria.achievement", "PlayerAchievementMgr::CompletedAchievement(%u). %s", achievement->ID, GetOwnerInfo().c_str());

    bool HasAccountAchiev = HasAccountAchieved(achievement->ID);

    CompletedAchievementData& ca = _completedAchievements[achievement->ID];
    ca.CompletedByThisCharacter = true;

    if (HasAccountAchiev)
        return;

    if (ca.LoadedFromDB)
        ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED;
    else
        ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW;

    ca.Date = time(nullptr);
    ca.FirstGuid = _owner->GetGUID();

    if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_REACH | ACHIEVEMENT_FLAG_REALM_FIRST_KILL))
        sAchievementMgr->SetRealmCompleted(achievement);

    if (!(achievement->Flags & ACHIEVEMENT_FLAG_TRACKING_FLAG))
        _achievementPoints += achievement->Points;

    UpdateCriteria(CRITERIA_TYPE_COMPLETE_ACHIEVEMENT, 0, 0, 0, NULL, referencePlayer);

    AchievementCategory const* category = sAchievementCategoryStore.LookupEntry(achievement->Category);
    if (!category)
        return;

    if (category->ID == ACHIEVEMENT_CATEGORY_ID_PET_BATTLE || category->ParentID == ACHIEVEMENT_CATEGORY_ID_PET_BATTLE)
        UpdateCriteria(CRITERIA_TYPE_EARN_PET_BATTLE_ACHIEVEMENT_POINTS, achievement->Points, 0, 0, nullptr, referencePlayer);
    else
        UpdateCriteria(CRITERIA_TYPE_EARN_ACHIEVEMENT_POINTS, achievement->Points, 0, 0, nullptr, referencePlayer);

    // reward items and titles if any
    AchievementRewardsVector const* rewards = sAchievementMgr->GetAchievementRewards(achievement);
    // no rewards
    if (!rewards || rewards->empty())
        return;

    for (AchievementRewardsVector::const_iterator reward = rewards->begin(); reward != rewards->end(); ++reward)
    {
        AchievementRewardTypes rewardType = reward->RewardType;
        if (rewardType == ACHIEVEMENT_REWARD_TYPE_NONE)
            continue;

        switch (rewardType)
        {
            case ACHIEVEMENT_REWARD_TYPE_FACTION_TITLE:
            {
                uint32 titleId = _owner->GetTeam() == ALLIANCE ? reward->FactionTitle.AllianceTitleID : reward->FactionTitle.HordeTitleID;
                if (CharTitlesEntry const* titleEntry = sCharTitlesStore.LookupEntry(titleId))
                    _owner->SetTitle(titleEntry);
                break;
            }
            case ACHIEVEMENT_REWARD_TYPE_GENDER_TITLE:
            {
                uint8 gender = _owner->GetByteValue(PLAYER_BYTES_3, PLAYER_BYTES_3_OFFSET_GENDER);
                uint32 titleId = gender == GENDER_MALE ? reward->GenderTitle.MaleTitleID : reward->GenderTitle.FemaleTitleID;
                if (CharTitlesEntry const* titleEntry = sCharTitlesStore.LookupEntry(titleId))
                    _owner->SetTitle(titleEntry);
                break;
            }
            case ACHIEVEMENT_REWARD_TYPE_MAIL_TEMPLATE:
            {
                MailDraft draft(uint16(reward->MailTemplate.MailID));

                SQLTransaction trans = CharacterDatabase.BeginTransaction();
                draft.SendMailTo(trans, _owner, MailSender(MAIL_CREATURE, reward->MailTemplate.SenderID));
                CharacterDatabase.CommitTransaction(trans);

                break;
            }
            case ACHIEVEMENT_REWARD_TYPE_MAIL:
            {
                std::string subject = reward->MailSubject;
                std::string text = reward->MailText;

                LocaleConstant localeConstant = _owner->GetSession()->GetSessionDbLocaleIndex();
                if (localeConstant >= LOCALE_enUS)
                {
                    if (AchievementRewardLocale const* loc = sAchievementMgr->GetAchievementRewardLocale(achievement))
                    {
                        ObjectMgr::GetLocaleString(loc->Subject, localeConstant, subject);
                        ObjectMgr::GetLocaleString(loc->Body, localeConstant, text);
                    }
                }

                MailDraft draft = MailDraft(subject, text);

                SQLTransaction trans = CharacterDatabase.BeginTransaction();

                if (reward->Mail.ItemID)
                    if (Item* item = Item::CreateItem(reward->Mail.ItemID, 1, _owner))
                    {
                        item->SaveToDB(trans);
                        draft.AddItem(item);
                    }

                draft.SendMailTo(trans, _owner, MailSender(MAIL_CREATURE, reward->Mail.SenderID));
                CharacterDatabase.CommitTransaction(trans);

                break;
            }
            case ACHIEVEMENT_REWARD_TYPE_BATTLE_PET_LOADOUT_FLAG:
                if (BattlePetMgr* battlePetMgr = _owner->GetBattlePetMgr())
                    battlePetMgr->SetLoadoutFlag(reward->BattlePetLoadoutFlag.BattlePetFlag);
                break;
            case ACHIEVEMENT_REWARD_TYPE_BATTLE_PET_LOADOUT_SLOT:
                if (BattlePetMgr* battlePetMgr = _owner->GetBattlePetMgr())
                    battlePetMgr->UnlockLoadoutSlot(reward->BattlePetLoadoutSlot.BattlePetSlot);
                break;
            default:
                break;
        }
    }
}

void PlayerAchievementMgr::SendCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const
{
    ObjectGuid guid = _owner->GetGUID();

    WorldPacket data(SMSG_CRITERIA_UPDATE_PLAYER, 1 + 8 + 4 + 4 + 4 + 4 + 4 + 8);

    data.WriteGuidMask(guid, 4, 6, 2, 3, 7, 1, 5, 0);

    data.WriteGuidBytes(guid, 3, 6, 2);

    data << uint32(criteria->ID);

    if (!criteria->Entry->StartTimer)
        data << uint32(0);
    else
        data << uint32(timedCompleted ? 1 : 0);

    data.WriteGuidBytes(guid, 5, 1);

    data.AppendPackedTime(progress->Date);

    data.WriteGuidBytes(guid, 4);

    data << uint32(timeElapsed);
    data << uint32(timeElapsed);

    data.WriteGuidBytes(guid, 7, 0);

    data << uint64(progress->Counter);

    SendPacket(&data);
}

void PlayerAchievementMgr::SendAccountCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const
{
    ObjectGuid accountId = ObjectGuid::Create<HighGuid::Account>(_owner->GetSession()->GetAccountId());
    ObjectGuid counter = ObjectGuid(progress->Counter);

    WorldPacket data(SMSG_CRITERIA_UPDATE_ACCOUNT);

    data.WriteGuidMask(counter, 4);

    data.WriteGuidMask(accountId, 2);

    data.WriteGuidMask(counter, 2);

    data.WriteGuidMask(accountId, 4);

    data.WriteGuidMask(counter, 0, 5);

    data.WriteGuidMask(accountId, 3);

    data.WriteGuidMask(counter, 3);

    data.WriteGuidMask(accountId, 6);

    data.WriteGuidMask(counter, 6);

    data.WriteGuidMask(accountId, 1, 7);

    data.WriteGuidMask(counter, 1);

    data.WriteBits(0, 4); // flags

    data.WriteGuidMask(accountId, 5);

    data.WriteGuidMask(counter, 7);

    data.WriteGuidMask(accountId, 0);

    data.WriteGuidBytes(accountId, 7);

    data << uint32(timeElapsed);

    data << uint32(criteria->ID);

    data.WriteGuidBytes(counter, 7);

    data << uint32(timeElapsed);

    data.WriteGuidBytes(accountId, 4, 3);

    data.AppendPackedTime(progress->Date);

    data.WriteGuidBytes(counter, 0, 1, 2, 3);

    data.WriteGuidBytes(accountId, 1);

    data.WriteGuidBytes(counter, 4, 5);

    data.WriteGuidBytes(accountId, 5, 2);

    data.WriteGuidBytes(counter, 6);

    data.WriteGuidBytes(accountId, 0, 6);

    SendPacket(&data);
}

void PlayerAchievementMgr::SendCriteriaProgressRemoved(uint32 criteriaId)
{
    WorldPacket data(SMSG_CRITERIA_DELETED, 4);

    data << uint32(criteriaId);

    SendPacket(&data);
}

void PlayerAchievementMgr::SendAchievementEarned(AchievementEntry const* achievement) const
{
    // Don't send for achievements with ACHIEVEMENT_FLAG_HIDDEN
    if (achievement->Flags & ACHIEVEMENT_FLAG_HIDDEN)
        return;

    TC_LOG_DEBUG("criteria.achievement", "PlayerAchievementMgr::SendAchievementEarned(%u)", achievement->ID);

    if (!(achievement->Flags & ACHIEVEMENT_FLAG_TRACKING_FLAG))
    {
        if (Guild* guild = sGuildMgr->GetGuildById(_owner->GetGuildId()))
        {
            Trinity::AchievementChatBuilder _builder(_owner, CHAT_MSG_GUILD_ACHIEVEMENT, BROADCAST_TEXT_ACHIEVEMENT_EARNED, achievement->ID);
            Trinity::LocalizedPacketDo<Trinity::AchievementChatBuilder> _localizer(_builder);
            guild->BroadcastWorker(_localizer, _owner);
        }

        if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_KILL | ACHIEVEMENT_FLAG_REALM_FIRST_REACH))
        {
            // broadcast realm first reached
            ObjectGuid guid = _owner->GetGUID();

            bool IsGuildAchiev = false;

            WorldPacket data(SMSG_ACHIEVEMENT_SERVER_FIRST, 1 + 8 + 1 + _owner->GetName().length() + 4);

            data.WriteGuidMask(guid, 5, 6, 3, 7, 0, 4);

            data.WriteBits(_owner->GetName().size(), 7);

            data.WriteGuidMask(guid, 2, 1);

            data.WriteBit(IsGuildAchiev);

            data.WriteGuidBytes(guid, 1);

            data.WriteString(_owner->GetName());

            data.WriteGuidBytes(guid, 0, 2);

            data << uint32(achievement->ID);

            data.WriteGuidBytes(guid, 6, 3, 4, 5, 7);

            sWorld->SendGlobalMessage(&data);
        }
        // if player is in world he can tell his friends about new achievement
        else if (_owner->IsInWorld())
        {
            Trinity::AchievementChatBuilder _builder(_owner, CHAT_MSG_ACHIEVEMENT, BROADCAST_TEXT_ACHIEVEMENT_EARNED, achievement->ID);
            Trinity::LocalizedPacketDo<Trinity::AchievementChatBuilder> _localizer(_builder);
            Trinity::PlayerDistWorker<Trinity::LocalizedPacketDo<Trinity::AchievementChatBuilder>> _worker(_owner, sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_SAY), _localizer);
            Cell::VisitWorldObjects(_owner, _worker, sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_SAY));
        }
    }

    ObjectGuid thisPlayerGuid = _owner->GetGUID();
    ObjectGuid firstPlayerOnAccountGuid = _owner->GetGUID();

    if (HasAccountAchieved(achievement->ID))
        firstPlayerOnAccountGuid = GetFirstAchievedCharacterOnAccount(achievement->ID);

    bool IsInitial = false;  // does not notify player ingame

    WorldPacket data(SMSG_ACHIEVEMENT_EARNED, 2 * (1 + 8) + 1 + 4 + 4 + 4 + 4);

    data.WriteGuidMask(thisPlayerGuid, 6, 2);

    data.WriteGuidMask(firstPlayerOnAccountGuid, 4, 5, 0, 3);

    data.WriteBit(IsInitial);

    data.WriteGuidMask(thisPlayerGuid, 7);

    data.WriteGuidMask(firstPlayerOnAccountGuid, 7, 1);

    data.WriteGuidMask(thisPlayerGuid, 3, 0, 4);

    data.WriteGuidMask(firstPlayerOnAccountGuid, 6);

    data.WriteGuidMask(thisPlayerGuid, 1);

    data.WriteGuidMask(firstPlayerOnAccountGuid, 2);

    data.WriteGuidMask(thisPlayerGuid, 5);

    data.WriteGuidBytes(thisPlayerGuid, 5);

    data.WriteGuidBytes(firstPlayerOnAccountGuid, 3);

    data.WriteGuidBytes(thisPlayerGuid, 6);

    data.WriteGuidBytes(firstPlayerOnAccountGuid, 6);

    data.AppendPackedTime(time(nullptr));

    data.WriteGuidBytes(thisPlayerGuid, 1);

    data.WriteGuidBytes(firstPlayerOnAccountGuid, 2, 0, 7);

    data.WriteGuidBytes(thisPlayerGuid, 3);

    data.WriteGuidBytes(firstPlayerOnAccountGuid, 4);

    data.WriteGuidBytes(thisPlayerGuid, 7);

    data << uint32(achievement->ID);

    data.WriteGuidBytes(thisPlayerGuid, 4);

    data.WriteGuidBytes(firstPlayerOnAccountGuid, 1);

    data.WriteGuidBytes(thisPlayerGuid, 0);

    data.WriteGuidBytes(firstPlayerOnAccountGuid, 5);

    data << uint32(realmID);
    data << uint32(realmID);

    data.WriteGuidBytes(thisPlayerGuid, 2);

    if (!(achievement->Flags & ACHIEVEMENT_FLAG_TRACKING_FLAG))
        _owner->SendMessageToSetInRange(&data, sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_SAY), true);
    else
        _owner->SendDirectMessage(&data);
}

void PlayerAchievementMgr::SendAllAccountCriteria() const
{
    std::unordered_map<uint32, CriteriaProgress> criteriaList;
    for (CriteriaProgressMap::const_iterator itr = _criteriaProgress.begin(); itr != _criteriaProgress.end(); ++itr)
    {
        if (itr->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        Criteria const* criteria = sCriteriaMgr->GetCriteria(itr->first);
        if (!criteria || (criteria->FlagsCu & CRITERIA_FLAG_CU_ACCOUNT) == 0)
            continue;

        criteriaList[itr->first] = itr->second;
    }

    ObjectGuid guid = ObjectGuid::Create<HighGuid::Account>(_owner->GetSession()->GetAccountId());

    ByteBuffer criteriaData;

    WorldPacket data(SMSG_ALL_ACCOUNT_CRITERIA, 3 + criteriaList.size() * (2 * (1 + 8) + 1 + 4 + 4 + 4 + 4));

    data.WriteBits(criteriaList.size(), 19);

    for (auto itr : criteriaList)
    {
        ObjectGuid counter = ObjectGuid(itr.second.Counter);

        data.WriteGuidMask(counter, 0);

        data.WriteGuidMask(guid, 7);

        data.WriteBits(0, 4);                   // flags

        data.WriteGuidMask(guid, 4);

        data.WriteGuidMask(counter, 6);

        data.WriteGuidMask(guid, 5, 2, 0, 6);

        data.WriteGuidMask(counter, 5, 7);

        data.WriteGuidMask(guid, 1, 3);

        data.WriteGuidMask(counter, 4, 3, 2, 1);

        criteriaData.WriteGuidBytes(counter, 2);

        criteriaData.WriteGuidBytes(guid, 2, 4);

        criteriaData.WriteGuidBytes(counter, 4);

        criteriaData.WriteGuidBytes(guid, 6, 0);

        criteriaData << uint32(0);              // timefromcreate

        criteriaData.WriteGuidBytes(guid, 1, 7);

        criteriaData.AppendPackedTime(itr.second.Date);

        criteriaData.WriteGuidBytes(counter, 7, 6, 5, 1);

        criteriaData.WriteGuidBytes(guid, 3);

        criteriaData << uint32(0);              // timefromstart

        criteriaData.WriteGuidBytes(guid, 5);

        criteriaData.WriteGuidBytes(counter, 3, 0);

        criteriaData << uint32(itr.first);
    }

    data.append(criteriaData);

    SendPacket(&data);
}

void PlayerAchievementMgr::SendPacket(WorldPacket* data) const
{
    _owner->SendDirectMessage(data);
}

CriteriaList const& PlayerAchievementMgr::GetCriteriaByType(CriteriaTypes type) const
{
    return sCriteriaMgr->GetPlayerCriteriaByType(type);
}

GuildAchievementMgr::GuildAchievementMgr(Guild* owner) : _owner(owner)
{
}

void GuildAchievementMgr::Reset(AchievementEntry const* achievement)
{
    auto itr = _completedAchievements.find(achievement->ID);
    if (itr == _completedAchievements.end())
        return;

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    PreparedStatement* stmt = nullptr;

    if (CriteriaTree const* tree = sCriteriaMgr->GetCriteriaTree(achievement->CriteriaTree))
    {
        for (CriteriaTree const* node : tree->Children)
        {
            uint32 criteriaId = node->ID;

            CriteriaProgressMap::const_iterator progress = _criteriaProgress.find(criteriaId);
            if (progress == _criteriaProgress.end())
                continue;

            SendCriteriaProgressRemoved(progress->first);

            _criteriaProgress.erase(progress);

            stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_ACHIEVEMENT_CRITERIA);
            stmt->setUInt32(0, _owner->GetId());
            stmt->setUInt32(1, criteriaId);
            trans->Append(stmt);
        }
    }

    WorldPacket data(SMSG_ACHIEVEMENT_GUILD_DELETED, 4 + 4 + 1 + 8);

    data << uint32(achievement->ID);

    data.AppendPackedTime(itr->second.Date);

    data.WriteGuidMask(_owner->GetGUID(), 0, 7, 2, 4, 5, 1, 6, 3);
    data.WriteGuidBytes(_owner->GetGUID(), 0, 5, 7, 6, 2, 1, 4, 3);

    SendPacket(&data);

    _completedAchievements.erase(itr);

    _achievementPoints -= achievement->Points;

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_ACHIEVEMENT);
    stmt->setUInt32(0, _owner->GetId());
    stmt->setUInt32(1, achievement->ID);
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);
}

void GuildAchievementMgr::LoadFromDB(PreparedQueryResult achievementResult, PreparedQueryResult criteriaResult)
{
    if (achievementResult)
    {
        do
        {
            Field* fields = achievementResult->Fetch();
            uint32 achievementid = fields[0].GetUInt32();

            // must not happen: cleanup at server startup in sAchievementMgr->LoadCompletedAchievements()
            AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementid);
            if (!achievement)
                continue;

            CompletedAchievementData& ca = _completedAchievements[achievementid];
            ca.Date = time_t(fields[1].GetUInt32());
            Tokenizer guids(fields[2].GetString(), ' ');
            for (uint32 i = 0; i < guids.size(); ++i)
                ca.CompletingPlayers.insert(ObjectGuid::Create<HighGuid::Player>(uint64(strtoull(guids[i], nullptr, 10))));

            ca.CompletedByThisCharacter = true;
            ca.LoadedFromDB = true;
            ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;

            _achievementPoints += achievement->Points;
        } while (achievementResult->NextRow());
    }

    if (criteriaResult)
    {
        time_t now = time(NULL);
        do
        {
            Field* fields = criteriaResult->Fetch();
            uint32 id = fields[0].GetUInt32();
            uint64 counter = fields[1].GetUInt64();
            time_t date = time_t(fields[2].GetUInt32());
            uint32 lowGuid = fields[3].GetUInt32();

            Criteria const* criteria = sCriteriaMgr->GetCriteria(id);
            if (!criteria)
            {
                // we will remove not existed criteria for all guilds
                TC_LOG_ERROR("criteria.achievement", "Non-existing achievement criteria %u data removed from table `guild_achievement_progress`.", id);

                PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_INVALID_ACHIEV_PROGRESS_CRITERIA_GUILD);
                stmt->setUInt32(0, uint16(id));
                CharacterDatabase.Execute(stmt);
                continue;
            }

            if (criteria->Entry->StartTimer && time_t(date + criteria->Entry->StartTimer) < now)
                continue;

            CriteriaProgress& progress = _criteriaProgress[id];
            progress.Counter = counter;
            progress.Date = date;
            progress.PlayerGUID = ObjectGuid::Create<HighGuid::Player>(lowGuid);

            progress.LoadedFromDB = true;
            progress.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;

        } while (criteriaResult->NextRow());
    }
}

void GuildAchievementMgr::SaveToDB(SQLTransaction& trans)
{
    std::vector<uint32> removedAchievements;
    std::vector<uint32> removedCriterias;

    PreparedStatement* stmt;
    std::ostringstream guidstr;
    for (auto& itr : _completedAchievements)
    {
        if (itr.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE)
            continue;

        switch (itr.second.DBStatus)
        {
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_ACHIEVEMENT);
                stmt->setUInt32(0, _owner->GetId());
                stmt->setUInt32(1, itr.first);
                stmt->setUInt32(2, uint32(itr.second.Date));
                for (ObjectGuid const& guid : itr.second.CompletingPlayers)
                    guidstr << guid.GetCounter() << ',';

                stmt->setString(3, guidstr.str());
                trans->Append(stmt);
                break;
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE:
                removedAchievements.push_back(itr.first);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_ACHIEVEMENT);
                stmt->setUInt32(0, _owner->GetId());
                stmt->setUInt32(1, itr.first);
                trans->Append(stmt);
                break;
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_ACHIEVEMENT);
                stmt->setUInt32(0, uint32(itr.second.Date));
                for (ObjectGuid const& guid : itr.second.CompletingPlayers)
                    guidstr << guid.GetCounter() << ',';

                stmt->setString(1, guidstr.str());
                stmt->setUInt32(2, _owner->GetId());
                stmt->setUInt32(3, itr.first);
                trans->Append(stmt);
                break;
            default:
                break;
        }

        guidstr.str("");

        itr.second.LoadedFromDB = true;
        itr.second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
    }

    for (auto& itr : _criteriaProgress)
    {
        if (itr.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE)
            continue;

        if (!itr.second.Counter && itr.second.LoadedFromDB)
            itr.second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE;

        switch (itr.second.DBStatus)
        {
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_ACHIEVEMENT_CRITERIA);
                stmt->setUInt32(0, _owner->GetId());
                stmt->setUInt32(1, itr.first);
                stmt->setUInt64(2, itr.second.Counter);
                stmt->setUInt32(3, uint32(itr.second.Date));
                stmt->setUInt32(4, itr.second.PlayerGUID.GetCounter());
                trans->Append(stmt);
                break;
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE:
                removedCriterias.push_back(itr.first);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_ACHIEVEMENT_CRITERIA);
                stmt->setUInt32(0, _owner->GetId());
                stmt->setUInt32(1, itr.first);
                trans->Append(stmt);
                break;
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_ACHIEVEMENT_CRITERIA);
                stmt->setUInt64(0, itr.second.Counter);
                stmt->setUInt32(1, uint32(itr.second.Date));
                stmt->setUInt32(2, itr.second.PlayerGUID.GetCounter());
                stmt->setUInt32(3, _owner->GetId());
                stmt->setUInt32(4, itr.first);
                trans->Append(stmt);
                break;
            default:
                break;
        }

        itr.second.LoadedFromDB = true;
        itr.second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
    }

    for (uint32 achievId : removedAchievements)
        _completedAchievements.erase(achievId);

    for (uint32 criteriaId : removedCriterias)
        _criteriaProgress.erase(criteriaId);

    removedAchievements.clear();
    removedCriterias.clear();
}

void GuildAchievementMgr::SendAllData(Player* receiver) const
{
    VisibleAchievementCheck filterInvisible;

    uint32 achievementCount = std::count_if(_completedAchievements.begin(), _completedAchievements.end(), filterInvisible);

    ByteBuffer achievementsData;

    WorldPacket data(SMSG_ACHIEVEMENT_GUILD_DATA, 3 + achievementCount * (1 + 8 + 4 + 4 + 4 + 4));

    data.WriteBits(achievementCount, 20);

    for (auto itr : _completedAchievements)
    {
        AchievementEntry const* achievement = filterInvisible(itr);
        if (!achievement)
            continue;

        ObjectGuid guid = *(itr.second.CompletingPlayers.begin());

        data.WriteGuidMask(guid, 0, 6, 2, 1, 7, 4, 5, 3);

        achievementsData << uint32(itr.first);

        achievementsData.WriteGuidBytes(guid, 5, 0);

        achievementsData.AppendPackedTime(itr.second.Date);

        achievementsData.WriteGuidBytes(guid, 4, 3, 1, 6, 2);

        achievementsData << uint32(realmID);
        achievementsData << uint32(realmID);

        achievementsData.WriteGuidBytes(guid, 7);
    }

    data.append(achievementsData);

    receiver->SendDirectMessage(&data);
}

void GuildAchievementMgr::SendAchievementInfo(Player* receiver, uint32 achievementId /*= 0*/) const
{
    AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementId);
    if (!achievement)
    {
        // send empty packet
        WorldPacket data(SMSG_CRITERIA_GUILD_DATA, 3);

        data.WriteBits(0, 19);

        data.FlushBits();

        receiver->SendDirectMessage(&data);

        return;
    }

    CriteriaTree const* tree = sCriteriaMgr->GetCriteriaTree(achievement->CriteriaTree);
    if (!tree)
    {
        // send empty packet
        WorldPacket data(SMSG_CRITERIA_GUILD_DATA, 3);

        data.WriteBits(0, 19);

        data.FlushBits();

        receiver->SendDirectMessage(&data);

        return;
    }

    std::vector<CriteriaProgressMap::const_iterator> criteriaVector;
    for (CriteriaTree const* node : tree->Children)
    {
        uint32 criteriaId = node->ID;
        CriteriaProgressMap::const_iterator progress = _criteriaProgress.find(criteriaId);
        if (progress == _criteriaProgress.end())
            continue;

        if (progress->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        criteriaVector.push_back(progress);
    }

    WorldPacket data(SMSG_CRITERIA_GUILD_DATA, 3 + criteriaVector.size() * (2 * (1 + 8) + 4 + 4 + 4 + 4 + 4));

    ByteBuffer criteriaData;

    data.WriteBits(criteriaVector.size(), 19);

    for (auto & itr : criteriaVector)
    {
        CriteriaProgressMap::const_iterator& progress = itr;
        uint32 criteriaId = progress->first;

        ObjectGuid counter = ObjectGuid(progress->second.Counter);
        ObjectGuid guid = progress->second.PlayerGUID;

        data.WriteGuidMask(guid, 2, 4, 6);

        data.WriteGuidMask(counter, 0);

        data.WriteGuidMask(guid, 7);

        data.WriteGuidMask(counter, 2, 5, 1, 4);

        data.WriteGuidMask(guid, 0, 3);

        data.WriteGuidMask(counter, 7, 6);

        data.WriteGuidMask(guid, 1);

        data.WriteGuidMask(counter, 3);

        data.WriteGuidMask(guid, 5);

        criteriaData.WriteGuidBytes(guid, 1);

        criteriaData.WriteGuidBytes(counter, 5);

        criteriaData.WriteGuidBytes(guid, 6);

        criteriaData << uint32(criteriaId);

        criteriaData.WriteGuidBytes(counter, 6);

        criteriaData << uint32(0); //  Date Created
        criteriaData << uint32(progress->second.Date);

        criteriaData.WriteGuidBytes(guid, 3);

        criteriaData.WriteGuidBytes(counter, 0);

        criteriaData.WriteGuidBytes(guid, 7);

        criteriaData.WriteGuidBytes(counter, 4, 2, 3);

        criteriaData.WriteGuidBytes(guid, 4);

        criteriaData.WriteGuidBytes(counter, 1);

        criteriaData.WriteGuidBytes(guid, 5, 0);

        criteriaData << uint32(0); // Flags

        criteriaData.WriteGuidBytes(guid, 2);

        criteriaData.WriteGuidBytes(counter, 7);

        criteriaData << uint32(0); // Date Started
    }

    data.append(criteriaData);

    receiver->SendDirectMessage(&data);
}

void GuildAchievementMgr::SendAllTrackedCriterias(Player* receiver, std::set<uint32> const& trackedCriterias) const
{
    std::vector<CriteriaProgressMap::const_iterator> criteriaVector;
    for (uint32 criteriaId : trackedCriterias)
    {
        CriteriaProgressMap::const_iterator progress = _criteriaProgress.find(criteriaId);
        if (progress == _criteriaProgress.end())
            continue;

        if (progress->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        criteriaVector.push_back(progress);
    }

    WorldPacket data(SMSG_CRITERIA_GUILD_DATA, 3 + criteriaVector.size() * (2 * (1 + 8) + 4 + 4 + 4 + 4 + 4));

    ByteBuffer criteriaData;

    data.WriteBits(criteriaVector.size(), 19);

    for (auto & itr : criteriaVector)
    {
        CriteriaProgressMap::const_iterator& progress = itr;
        uint32 criteriaId = progress->first;

        ObjectGuid counter = ObjectGuid(progress->second.Counter);
        ObjectGuid guid = progress->second.PlayerGUID;

        data.WriteGuidMask(guid, 2, 4, 6);

        data.WriteGuidMask(counter, 0);

        data.WriteGuidMask(guid, 7);

        data.WriteGuidMask(counter, 2, 5, 1, 4);

        data.WriteGuidMask(guid, 0, 3);

        data.WriteGuidMask(counter, 7, 6);

        data.WriteGuidMask(guid, 1);

        data.WriteGuidMask(counter, 3);

        data.WriteGuidMask(guid, 5);

        criteriaData.WriteGuidBytes(guid, 1);

        criteriaData.WriteGuidBytes(counter, 5);

        criteriaData.WriteGuidBytes(guid, 6);

        criteriaData << uint32(criteriaId);

        criteriaData.WriteGuidBytes(counter, 6);

        criteriaData << uint32(0); //  Date Created
        criteriaData << uint32(progress->second.Date);

        criteriaData.WriteGuidBytes(guid, 3);

        criteriaData.WriteGuidBytes(counter, 0);

        criteriaData.WriteGuidBytes(guid, 7);

        criteriaData.WriteGuidBytes(counter, 4, 2, 3);

        criteriaData.WriteGuidBytes(guid, 4);

        criteriaData.WriteGuidBytes(counter, 1);

        criteriaData.WriteGuidBytes(guid, 5, 0);

        criteriaData << uint32(0); // Flags

        criteriaData.WriteGuidBytes(guid, 2);

        criteriaData.WriteGuidBytes(counter, 7);

        criteriaData << uint32(0); // Date Started
    }

    data.append(criteriaData);

    receiver->SendDirectMessage(&data);
}

void GuildAchievementMgr::CompletedAchievement(AchievementEntry const* achievement, Player* referencePlayer)
{
    TC_LOG_DEBUG("criteria.achievement", "GuildAchievementMgr::CompletedAchievement(%u)", achievement->ID);

    if (achievement->Flags & ACHIEVEMENT_FLAG_COUNTER || HasAchieved(achievement->ID))
        return;

    CompletedAchievementData& ca = _completedAchievements[achievement->ID];
    ca.Date = time(NULL);
    ca.CompletedByThisCharacter = true;

    if (ca.LoadedFromDB)
        ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED;
    else
        ca.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW;

    if (achievement->Flags & ACHIEVEMENT_FLAG_SHOW_GUILD_MEMBERS)
    {
        if (referencePlayer->GetGuildId() == _owner->GetId())
            ca.CompletingPlayers.insert(referencePlayer->GetGUID());

        if (Group const* group = referencePlayer->GetGroup())
            for (GroupReference const* ref = group->GetFirstMember(); ref != NULL; ref = ref->next())
                if (Player const* groupMember = ref->GetSource())
                    if (groupMember->GetGuildId() == _owner->GetId())
                        ca.CompletingPlayers.insert(groupMember->GetGUID());
    }

    if (achievement->Flags & ACHIEVEMENT_FLAG_SHOW_IN_GUILD_NEWS)
        if (Guild* guild = referencePlayer->GetGuild())
            guild->AddGuildNews(GUILD_NEWS_GUILD_ACHIEVEMENT, ObjectGuid::Empty, achievement->Flags & ACHIEVEMENT_FLAG_SHOW_IN_GUILD_HEADER, achievement->ID, ca.CompletingPlayers);

    SendAchievementEarned(achievement);

    if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_REACH | ACHIEVEMENT_FLAG_REALM_FIRST_KILL))
        sAchievementMgr->SetRealmCompleted(achievement);

    if (!(achievement->Flags & ACHIEVEMENT_FLAG_TRACKING_FLAG))
        _achievementPoints += achievement->Points;

    UpdateCriteria(CRITERIA_TYPE_COMPLETE_ACHIEVEMENT, 0, 0, 0, NULL, referencePlayer);

    UpdateCriteria(CRITERIA_TYPE_EARN_GUILD_ACHIEVEMENT_POINTS, achievement->Points, 0, 0, NULL, referencePlayer);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    SaveToDB(trans);
    CharacterDatabase.CommitTransaction(trans);
}

void GuildAchievementMgr::SendCriteriaUpdate(Criteria const* entry, CriteriaProgress const* progress, uint32 /*timeElapsed*/, bool /*timedCompleted*/) const
{
    WorldPacket data(SMSG_CRITERIA_GUILD_DATA, 3 + 2 * (1 + 8) + 4 + 4 + 4 + 4 + 4);

    ObjectGuid counter = ObjectGuid(progress->Counter);
    ObjectGuid guid = progress->PlayerGUID;

    data.WriteGuidMask(guid, 2, 4, 6);

    data.WriteGuidMask(counter, 0);

    data.WriteGuidMask(guid, 7);

    data.WriteGuidMask(counter, 2, 5, 1, 4);

    data.WriteGuidMask(guid, 0, 3);

    data.WriteGuidMask(counter, 7, 6);

    data.WriteGuidMask(guid, 1);

    data.WriteGuidMask(counter, 3);

    data.WriteGuidMask(guid, 5);

    data.WriteGuidBytes(guid, 1);

    data.WriteGuidBytes(counter, 5);

    data.WriteGuidBytes(guid, 6);

    data << uint32(entry->ID);

    data.WriteGuidBytes(counter, 6);

    data << uint32(0); //  Date Created
    data << uint32(progress->Date);

    data.WriteGuidBytes(guid, 3);

    data.WriteGuidBytes(counter, 0);

    data.WriteGuidBytes(guid, 7);

    data.WriteGuidBytes(counter, 4, 2, 3);

    data.WriteGuidBytes(guid, 4);

    data.WriteGuidBytes(counter, 1);

    data.WriteGuidBytes(guid, 5, 0);

    data << uint32(0); // Flags

    data.WriteGuidBytes(guid, 2);

    data.WriteGuidBytes(counter, 7);

    data << uint32(0); // Date Started

    _owner->BroadcastPacketIfTrackingAchievement(&data, entry->ID);
}

void GuildAchievementMgr::SendCriteriaProgressRemoved(uint32 criteriaId)
{
    ObjectGuid guid = _owner->GetGUID();

    WorldPacket data(SMSG_CRITERIA_GUILD_DELETED, 1 + 8 + 4);

    data.WriteGuidMask(guid, 2, 4, 0, 3, 6, 7, 5, 1);
    data.WriteGuidBytes(guid, 6, 1, 2, 3, 4, 5, 7, 0);

    data << uint32(criteriaId);

    SendPacket(&data);
}

void GuildAchievementMgr::SendAchievementEarned(AchievementEntry const* achievement) const
{
    if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_KILL | ACHIEVEMENT_FLAG_REALM_FIRST_REACH))
    {
        ObjectGuid guid = _owner->GetGUID();

        bool IsGuildAchiev = true;

        WorldPacket data(SMSG_ACHIEVEMENT_SERVER_FIRST, 1 + 8 + 1 + _owner->GetName().length() + 4);

        data.WriteGuidMask(guid, 5, 6, 3, 7, 0, 4);

        data.WriteBits(_owner->GetName().size(), 7);

        data.WriteGuidMask(guid, 2, 1);

        data.WriteBit(IsGuildAchiev);

        data.WriteGuidBytes(guid, 1);

        data.WriteString(_owner->GetName());

        data.WriteGuidBytes(guid, 0, 2);

        data << uint32(achievement->ID);

        data.WriteGuidBytes(guid, 6, 3, 4, 5, 7);

        sWorld->SendGlobalMessage(&data);
    }

    ObjectGuid guid = _owner->GetGUID();

    WorldPacket data(SMSG_ACHIEVEMENT_GUILD_EARNED, 1 + 8 + 4 + 4);

    data.WriteGuidMask(guid, 5, 7, 1, 4, 2, 0, 3 ,6);

    data.WriteGuidBytes(guid, 7);

    data.AppendPackedTime(time(nullptr));

    data << uint32(achievement->ID);

    data.WriteGuidBytes(guid, 0, 5, 3, 2, 4, 1, 6);

    SendPacket(&data);
}

void GuildAchievementMgr::SendPacket(WorldPacket* data) const
{
    _owner->BroadcastPacket(data);
}

CriteriaList const& GuildAchievementMgr::GetCriteriaByType(CriteriaTypes type) const
{
    return sCriteriaMgr->GetGuildCriteriaByType(type);
}

std::string PlayerAchievementMgr::GetOwnerInfo() const
{
    std::ostringstream str;

    str << _owner->GetGUID().ToString().c_str();
    str << " Name: " << _owner->GetName().c_str();

    return str.str();
}

std::string GuildAchievementMgr::GetOwnerInfo() const
{
    std::ostringstream str;

    str << "Guild Id: " << _owner->GetId();
    str << " Name: " << _owner->GetName().c_str();

    return str.str();
}

std::vector<AchievementEntry const*> const* AchievementGlobalMgr::GetAchievementByReferencedId(uint32 id) const
{
    auto itr = _achievementListByReferencedId.find(id);
    return itr != _achievementListByReferencedId.end() ? &itr->second : NULL;
}

AchievementRewardsVector const* AchievementGlobalMgr::GetAchievementRewards(AchievementEntry const* achievement) const
{
    auto iter = _achievementRewards.find(achievement->ID);
    return iter != _achievementRewards.end() ? &iter->second : nullptr;
}

AchievementRewardLocale const* AchievementGlobalMgr::GetAchievementRewardLocale(AchievementEntry const* achievement) const
{
    auto iter = _achievementRewardLocales.find(achievement->ID);
    return iter != _achievementRewardLocales.end() ? &iter->second : NULL;
}

bool AchievementGlobalMgr::IsRealmCompleted(AchievementEntry const* achievement) const
{
    auto itr = _allCompletedAchievements.find(achievement->ID);
    if (itr == _allCompletedAchievements.end())
        return false;

    if (itr->second == std::chrono::system_clock::time_point::min())
        return false;

    if (itr->second == std::chrono::system_clock::time_point::max())
        return true;

    // Allow completing the realm first kill for entire minute after first person did it
    // it may allow more than one group to achieve it (highly unlikely)
    // but apparently this is how blizz handles it as well
    if (achievement->Flags & ACHIEVEMENT_FLAG_REALM_FIRST_KILL)
        return (GameTime::GetGameTimeSystemPoint() - itr->second) > Minutes(1);

    return true;
}

void AchievementGlobalMgr::SetRealmCompleted(AchievementEntry const* achievement)
{
    if (IsRealmCompleted(achievement))
        return;

    _allCompletedAchievements[achievement->ID] = GameTime::GetGameTimeSystemPoint();
}

//==========================================================
void AchievementGlobalMgr::LoadAchievementReferenceList()
{
    uint32 oldMSTime = getMSTime();

    if (sAchievementStore.GetNumRows() == 0)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 achievement references.");
        return;
    }

    uint32 count = 0;

    for (uint32 entryId = 0; entryId < sAchievementStore.GetNumRows(); ++entryId)
    {
        AchievementEntry const* achievement = sAchievementStore.LookupEntry(entryId);
        if (!achievement || !achievement->SharesCriteria)
            continue;

        _achievementListByReferencedId[achievement->SharesCriteria].push_back(achievement);
        ++count;
    }

    // Once Bitten, Twice Shy (10 player) - Icecrown Citadel
    if (AchievementEntry const* achievement = sAchievementStore.LookupEntry(4539))
        const_cast<AchievementEntry*>(achievement)->MapID = 631;    // Correct map requirement (currently has Ulduar);

    TC_LOG_INFO("server.loading", ">> Loaded %u achievement references in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void AchievementGlobalMgr::LoadCompletedAchievements()
{
    uint32 oldMSTime = getMSTime();

    // Populate _allCompletedAchievements with all realm first achievement ids to make multithreaded access safer
    // while it will not prevent races, it will prevent crashes that happen because std::unordered_map key was added
    // instead the only potential race will happen on value associated with the key
    for (AchievementEntry const* achievement : sAchievementStore)
        if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_REACH | ACHIEVEMENT_FLAG_REALM_FIRST_KILL))
            _allCompletedAchievements[achievement->ID] = std::chrono::system_clock::time_point::min();

    QueryResult result = CharacterDatabase.Query("SELECT achievement FROM character_achievement GROUP BY achievement");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 realm first completed achievements. DB table `character_achievement` is empty.");
        return;
    }

    do
    {
        Field* fields = result->Fetch();

        uint32 achievementId = fields[0].GetUInt32();
        AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementId);
        if (!achievement)
        {
            // Remove non-existing achievements from all characters
            TC_LOG_ERROR("criteria.achievement", "Non-existing achievement %u data has been removed from the table `character_achievement`.", achievementId);

            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_INVALID_ACHIEVMENT);
            stmt->setUInt32(0, achievementId);
            CharacterDatabase.Execute(stmt);

            continue;
        }
        else if (achievement->Flags & (ACHIEVEMENT_FLAG_REALM_FIRST_REACH | ACHIEVEMENT_FLAG_REALM_FIRST_KILL))
            _allCompletedAchievements[achievementId] = std::chrono::system_clock::time_point::max();
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %lu realm first completed achievements in %u ms.", (unsigned long)_allCompletedAchievements.size(), GetMSTimeDiffToNow(oldMSTime));
}

void AchievementGlobalMgr::LoadRewards()
{
    uint32 oldMSTime = getMSTime();

    _achievementRewards.clear();                           // need for reload case

    QueryResult result = WorldDatabase.Query("SELECT entry, type, miscValue1, miscValue2, subject, text FROM achievement_reward");
    if (!result)
    {
        TC_LOG_ERROR("server.loading", ">> Loaded 0 achievement rewards. DB table `achievement_reward` is empty.");
        return;
    }

    uint32 count = 0;

    do
    {
        Field* fields = result->Fetch();
        uint32 entry = fields[0].GetUInt32();
        AchievementEntry const* achievement = sAchievementStore.LookupEntry(entry);
        if (!achievement)
        {
            TC_LOG_ERROR("sql.sql", "Table `achievement_reward` contains a wrong achievement entry (Entry: %u), ignored.", entry);
            continue;
        }

        uint8 rewardType = fields[1].GetUInt8();
        if (rewardType >= MAX_ACHIEVEMENT_REWARD_TYPE)
        {
            TC_LOG_ERROR("sql.sql", "Table `achievement_reward` contains a wrong achievement type %u, ignored.", rewardType);
            continue;
        }

        uint32 miscValue1 = fields[2].GetUInt32();
        uint32 miscValue2 = fields[3].GetUInt32();
        std::string subject = fields[4].GetString();
        std::string text = fields[5].GetString();

        AchievementReward reward(rewardType, miscValue1, miscValue2, subject, text);

        switch (rewardType)
        {
            case ACHIEVEMENT_REWARD_TYPE_FACTION_TITLE:
            case ACHIEVEMENT_REWARD_TYPE_GENDER_TITLE:
                if (reward.Raw.MiscValue1)
                {
                    CharTitlesEntry const* titleEntry = sCharTitlesStore.LookupEntry(reward.Raw.MiscValue1);
                    if (!titleEntry)
                    {
                        TC_LOG_ERROR("sql.sql", "Table `achievement_reward` (Entry: %u) contains an invalid male title id (%u), set to 0", entry, reward.Raw.MiscValue1);
                        reward.Raw.MiscValue1 = 0;
                    }
                }

                if (reward.Raw.MiscValue2)
                {
                    CharTitlesEntry const* titleEntry = sCharTitlesStore.LookupEntry(reward.Raw.MiscValue2);
                    if (!titleEntry)
                    {
                        TC_LOG_ERROR("sql.sql", "Table `achievement_reward` (Entry: %u) contains an invalid female title id (%u), set to 0", entry, reward.Raw.MiscValue2);
                        reward.Raw.MiscValue2 = 0;
                    }
                }
                break;
            case ACHIEVEMENT_REWARD_TYPE_MAIL_TEMPLATE:
                if (reward.MailTemplate.SenderID)
                {
                    if (!sObjectMgr->GetCreatureTemplate(reward.MailTemplate.SenderID))
                    {
                        TC_LOG_ERROR("sql.sql", "Table `achievement_reward` (Entry: %u) contains an invalid creature entry %u as sender, mail reward skipped.", entry, reward.MailTemplate.SenderID);
                        reward.MailTemplate.SenderID = 0;
                    }
                }

                if (reward.MailTemplate.MailID)
                {
                    if (!sMailTemplateStore.LookupEntry(reward.MailTemplate.MailID))
                    {
                        TC_LOG_ERROR("sql.sql", "Table `achievement_reward` (Entry: %u) is using an invalid MailTemplateId (%u).", entry, reward.MailTemplate.MailID);
                        reward.MailTemplate.MailID = 0;
                    }
                }
                break;
            case ACHIEVEMENT_REWARD_TYPE_MAIL:
                if (reward.Mail.SenderID)
                {
                    if (!sObjectMgr->GetCreatureTemplate(reward.Mail.SenderID))
                    {
                        TC_LOG_ERROR("sql.sql", "Table `achievement_reward` (Entry: %u) contains an invalid creature entry %u as sender, mail reward skipped.", entry, reward.Mail.SenderID);
                        reward.Mail.SenderID = 0;
                    }
                }

                if (reward.Mail.ItemID)
                {
                    if (!sObjectMgr->GetItemTemplate(reward.Mail.ItemID))
                    {
                        TC_LOG_ERROR("sql.sql", "Table `achievement_reward` (Entry: %u) contains an invalid item id %u, reward mail will not contain the rewarded item.", entry, reward.Mail.ItemID);
                        reward.Mail.ItemID = 0;
                    }
                }
                break;
        }

        _achievementRewards[entry].push_back(reward);
        ++count;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u achievement rewards in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void AchievementGlobalMgr::LoadRewardLocales()
{
    uint32 oldMSTime = getMSTime();

    _achievementRewardLocales.clear();                       // need for reload case

    QueryResult result = WorldDatabase.Query("SELECT entry, subject_loc1, text_loc1, subject_loc2, text_loc2, subject_loc3, text_loc3, subject_loc4, text_loc4, "
        "subject_loc5, text_loc5, subject_loc6, text_loc6, subject_loc7, text_loc7, subject_loc8, text_loc8"
        " FROM locales_achievement_reward");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 achievement reward locale strings.  DB table `locales_achievement_reward` is empty.");
        return;
    }

    do
    {
        Field* fields = result->Fetch();

        uint32 entry = fields[0].GetUInt32();

        if (_achievementRewards.find(entry) == _achievementRewards.end())
        {
            TC_LOG_ERROR("sql.sql", "Table `locales_achievement_reward` (Entry: %u) contains locale strings for a non-existing achievement reward.", entry);
            continue;
        }

        AchievementRewardLocale& data = _achievementRewardLocales[entry];

        for (uint8 i = OLD_TOTAL_LOCALES - 1; i > 0; --i)
        {
            LocaleConstant locale = (LocaleConstant)i;
            ObjectMgr::AddLocaleString(fields[1 + 2 * (i - 1)].GetString(), locale, data.Subject);
            ObjectMgr::AddLocaleString(fields[1 + 2 * (i - 1) + 1].GetString(), locale, data.Body);
        }
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u achievement reward locale strings in %u ms.", uint32(_achievementRewardLocales.size()), GetMSTimeDiffToNow(oldMSTime));
}
