/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AccountMgr.h"
#include "CalendarMgr.h"
#include "CharacterCache.h"
#include "Chat.h"
#include "Config.hpp"
#include "DatabaseEnv.h"
#include "Guild.h"
#include "GuildFinderMgr.h"
#include "GuildMgr.h"
#include "Group.h"
#include "Language.h"
#include "Log.h"
#include "ScriptMgr.h"
#include "SocialMgr.h"
#include "SpellInfo.h"
#include "ReputationMgr.h"
#include <boost/dynamic_bitset.hpp>

#define MAX_GUILD_BANK_TAB_TEXT_LEN 500
#define EMBLEM_PRICE 10 * GOLD

inline uint32 _GetGuildBankTabPrice(uint8 tabId)
{
    switch (tabId)
    {
        case 0:
            return 100;
        case 1:
            return 250;
        case 2:
            return 500;
        case 3:
            return 1000;
        case 4:
            return 2500;
        case 5:
            return 5000;
        default:
            return 0;
    }
}

void Guild::SendCommandResult(WorldSession* session, GuildCommandType type, GuildCommandError errCode, std::string const& param)
{
    WorldPacket data(SMSG_GUILD_COMMAND_RESULT, 4 + 4 + 1 + param.size());

    data << uint32(type);
    data << uint32(errCode);

    data.WriteBits(param.size(), 8);

    data.WriteString(param);

    session->SendPacket(&data);
}

void Guild::SendSaveEmblemResult(WorldSession* session, GuildEmblemError errCode)
{
    WorldPacket data(SMSG_GUILD_EMBLEM_SAVE, 4);

    data << uint32(errCode);

    session->SendPacket(&data);
}

// LogHolder
Guild::LogHolder::~LogHolder()
{
    // Cleanup
    for (LogEntry* log : m_log)
        delete log;
}

// Adds event loaded from database to collection
inline void Guild::LogHolder::LoadEvent(LogEntry* entry)
{
    if (m_nextGUID == uint32(GUILD_EVENT_LOG_GUID_UNDEFINED))
        m_nextGUID = entry->GetGUID();

    m_log.push_front(entry);
}

// Adds new event happened in game.
// If maximum number of events is reached, oldest event is removed from collection.
inline void Guild::LogHolder::AddEvent(SQLTransaction& trans, LogEntry* entry)
{
    // Check max records limit
    if (m_log.size() >= m_maxRecords)
    {
        LogEntry* oldEntry = m_log.front();
        delete oldEntry;
        m_log.pop_front();
    }

    // Add event to list
    m_log.push_back(entry);

    // Save to DB
    entry->SaveToDB(trans);
}

inline uint32 Guild::LogHolder::GetNextGUID()
{
    // Next guid was not initialized. It means there are no records for this holder in DB yet.
    // Start from the beginning.
    if (m_nextGUID == uint32(GUILD_EVENT_LOG_GUID_UNDEFINED))
        m_nextGUID = 0;
    else
        m_nextGUID = (m_nextGUID + 1) % m_maxRecords;

    return m_nextGUID;
}

// EventLogEntry
void Guild::EventLogEntry::SaveToDB(SQLTransaction& trans) const
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_EVENTLOG);
    stmt->setUInt32(0, m_guildId);
    stmt->setUInt32(1, m_guid);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);

    uint8 index = 0;
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_EVENTLOG);
    stmt->setUInt32(  index, m_guildId);
    stmt->setUInt32(++index, m_guid);
    stmt->setUInt8 (++index, uint8(m_eventType));
    stmt->setUInt32(++index, m_playerGuid1);
    stmt->setUInt32(++index, m_playerGuid2);
    stmt->setUInt8 (++index, m_newRank);
    stmt->setUInt64(++index, m_timestamp);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

void Guild::EventLogEntry::WritePacket(WorldPacket& data, ByteBuffer& content) const
{
    ObjectGuid guid1 = ObjectGuid::Create<HighGuid::Player>(m_playerGuid1);
    ObjectGuid guid2 = ObjectGuid::Create<HighGuid::Player>(m_playerGuid2);

    data.WriteGuidMask(guid1, 6, 1);

    data.WriteGuidMask(guid2, 5, 1, 3, 0, 4);

    data.WriteGuidMask(guid1, 4);

    data.WriteGuidMask(guid2, 7);

    data.WriteGuidMask(guid1, 0, 2, 7, 3, 5);

    data.WriteGuidMask(guid2, 2, 6);

    content.WriteGuidBytes(guid1, 5, 4);

    content.WriteGuidBytes(guid2, 6);

    content.WriteGuidBytes(guid1, 2);

    content.WriteGuidBytes(guid2, 4);

    // Event type
    content << uint8(m_eventType);

    content.WriteGuidBytes(guid2, 0);

    content.WriteGuidBytes(guid1, 7, 3);

    content.WriteGuidBytes(guid2, 5, 2);

    content.WriteGuidBytes(guid1, 0);

    // Event timestamp
    content << uint32(::time(nullptr) - m_timestamp);

    content.WriteGuidBytes(guid1, 1, 6);

    content.WriteGuidBytes(guid2, 7, 1);

    // New Rank
    content << uint8(m_newRank);

    content.WriteGuidBytes(guid2, 3);
}

// BankEventLogEntry
void Guild::BankEventLogEntry::SaveToDB(SQLTransaction& trans) const
{
    uint8 index = 0;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_EVENTLOG);
    stmt->setUInt32(  index, m_guildId);
    stmt->setUInt32(++index, m_guid);
    stmt->setUInt8 (++index, m_bankTabId);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);

    index = 0;
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_BANK_EVENTLOG);
    stmt->setUInt32(  index, m_guildId);
    stmt->setUInt32(++index, m_guid);
    stmt->setUInt8 (++index, m_bankTabId);
    stmt->setUInt8 (++index, uint8(m_eventType));
    stmt->setUInt32(++index, m_playerGuid);
    stmt->setUInt64(++index, m_itemOrMoney);
    stmt->setUInt16(++index, m_itemStackCount);
    stmt->setUInt8 (++index, m_destTabId);
    stmt->setUInt64(++index, m_timestamp);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

void Guild::BankEventLogEntry::WritePacket(WorldPacket& data, ByteBuffer& content) const
{
    ObjectGuid logGuid = ObjectGuid::Create<HighGuid::Player>(m_playerGuid);

    bool hasItem = m_eventType == GUILD_BANK_LOG_DEPOSIT_ITEM || m_eventType == GUILD_BANK_LOG_WITHDRAW_ITEM ||
                   m_eventType == GUILD_BANK_LOG_MOVE_ITEM || m_eventType == GUILD_BANK_LOG_MOVE_ITEM2;

    bool itemMoved = (m_eventType == GUILD_BANK_LOG_MOVE_ITEM || m_eventType == GUILD_BANK_LOG_MOVE_ITEM2);

    bool hasStack = (hasItem && m_itemStackCount > 1) || itemMoved;

    data.WriteBit(IsMoneyEvent());

    data.WriteGuidMask(logGuid, 0, 2, 3, 6, 5, 4);

    data.WriteBit(hasStack);
    data.WriteBit(hasItem);

    data.WriteGuidMask(logGuid, 7, 1);

    data.WriteBit(itemMoved);

    content.WriteGuidBytes(logGuid, 1, 7);

    if (itemMoved)
        content << uint8(m_destTabId);

    content.WriteGuidBytes(logGuid, 2);

    content << uint32(time(NULL) - m_timestamp);
    content << uint8(m_eventType);

    content.WriteGuidBytes(logGuid, 0, 4);

    if (hasItem)
        content << uint32(m_itemOrMoney);

    if (IsMoneyEvent())
        content << uint64(m_itemOrMoney);

    content.WriteGuidBytes(logGuid, 6);

    if (hasStack)
        content << uint32(m_itemStackCount);

    content.WriteGuidBytes(logGuid, 5, 3);
}

void Guild::NewsLogEntry::SaveToDB(SQLTransaction& trans) const
{
    uint8 index = 0;
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_NEWS);
    stmt->setUInt32(  index, m_guildId);
    stmt->setUInt32(++index, GetGUID());
    stmt->setUInt8 (++index, GetType());
    stmt->setUInt32(++index, GetPlayerGuid());
    stmt->setUInt32(++index, GetFlags());
    stmt->setUInt32(++index, GetValue());
    stmt->setUInt64(++index, GetTimestamp());
    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

void Guild::NewsLogEntry::WritePacket(WorldPacket& data, ByteBuffer& content) const
{
    ByteBuffer guidData;

    ObjectGuid guid = GetPlayerGuid();

    data.WriteGuidMask(guid, 3);

    data.WriteBits(guildAchievementPlayers.size(), 24);

    for (ObjectGuid memberGuid : guildAchievementPlayers)
    {
        data.WriteGuidMask(memberGuid, 7, 3, 6, 2, 4, 5, 1, 0);
        guidData.WriteGuidBytes(memberGuid, 4, 6, 5, 2, 7, 0, 1, 3);
    }

    data.WriteGuidMask(guid, 4, 5, 6, 0, 7, 2, 1);

    content.WriteGuidBytes(guid, 2);

    content.append(guidData);

    content << uint32(GetValue());

    content.WriteGuidBytes(guid, 1, 7, 4, 3, 5);

    content << uint32(0);
    content << uint32(GetType());
    content << uint32(GetFlags());

    content.WriteGuidBytes(guid, 6);

    content.AppendPackedTime(GetTimestamp());

    content << uint32(GetGUID());

    content.WriteGuidBytes(guid, 0);
}

// RankInfo
void Guild::RankInfo::LoadFromDB(Field* fields)
{
    m_rankId            = fields[1].GetUInt8();
    m_name              = fields[2].GetString();
    m_rights            = fields[3].GetUInt32();
    m_bankMoneyPerDay   = fields[4].GetUInt32();

    if (m_rankId == GR_GUILDMASTER)                     // Prevent loss of leader rights
        m_rights |= GR_RIGHT_ALL;
}

void Guild::RankInfo::SaveToDB(SQLTransaction& trans) const
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_RANK);
    stmt->setUInt32(0, m_guildId);
    stmt->setUInt8 (1, m_rankId);
    stmt->setString(2, m_name);
    stmt->setUInt32(3, m_rights);
    stmt->setUInt32(4, m_bankMoneyPerDay);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

void Guild::RankInfo::CreateMissingTabsIfNeeded(uint8 tabs, SQLTransaction& trans, bool logOnCreate /* = false */)
{
    for (uint8 i = 0; i < tabs; ++i)
    {
        GuildBankRightsAndSlots& rightsAndSlots = m_bankTabRightsAndSlots[i];
        if (rightsAndSlots.GetTabId() == i)
            continue;

        rightsAndSlots.SetTabId(i);
        if (m_rankId == GR_GUILDMASTER)
            rightsAndSlots.SetGuildMasterValues();

        if (logOnCreate)
            TC_LOG_ERROR("guild", "Guild %u has broken Tab %u for rank %u. Created default tab.", m_guildId, i, m_rankId);

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_BANK_RIGHT);
        stmt->setUInt32(0, m_guildId);
        stmt->setUInt8(1, i);
        stmt->setUInt8(2, m_rankId);
        stmt->setUInt8(3, rightsAndSlots.GetRights());
        stmt->setUInt32(4, rightsAndSlots.GetSlots());
        trans->Append(stmt);
    }
}

void Guild::RankInfo::SetName(std::string const& name)
{
    if (m_name == name)
        return;

    m_name = name;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_RANK_NAME);
    stmt->setString(0, m_name);
    stmt->setUInt8 (1, m_rankId);
    stmt->setUInt32(2, m_guildId);
    CharacterDatabase.Execute(stmt);
}

void Guild::RankInfo::SetRights(uint32 rights)
{
    if (m_rankId == GR_GUILDMASTER)                     // Prevent loss of leader rights
        rights = GR_RIGHT_ALL;

    if (m_rights == rights)
        return;

    m_rights = rights;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_RANK_RIGHTS);
    stmt->setUInt32(0, m_rights);
    stmt->setUInt8 (1, m_rankId);
    stmt->setUInt32(2, m_guildId);
    CharacterDatabase.Execute(stmt);
}

void Guild::RankInfo::SetBankMoneyPerDay(uint32 money)
{
    if (m_bankMoneyPerDay == money)
        return;

    m_bankMoneyPerDay = money;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_RANK_BANK_MONEY);
    stmt->setUInt32(0, money);
    stmt->setUInt8 (1, m_rankId);
    stmt->setUInt32(2, m_guildId);
    CharacterDatabase.Execute(stmt);
}

void Guild::RankInfo::SetBankTabSlotsAndRights(GuildBankRightsAndSlots rightsAndSlots, bool saveToDB)
{
    if (m_rankId == GR_GUILDMASTER)                     // Prevent loss of leader rights
        rightsAndSlots.SetGuildMasterValues();

    GuildBankRightsAndSlots& guildBR = m_bankTabRightsAndSlots[rightsAndSlots.GetTabId()];
    guildBR = rightsAndSlots;

    if (saveToDB)
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_BANK_RIGHT);
        stmt->setUInt32(0, m_guildId);
        stmt->setUInt8 (1, guildBR.GetTabId());
        stmt->setUInt8 (2, m_rankId);
        stmt->setUInt8 (3, guildBR.GetRights());
        stmt->setUInt32(4, guildBR.GetSlots());
        CharacterDatabase.Execute(stmt);
    }
}

// BankTab
void Guild::BankTab::LoadFromDB(Field* fields)
{
    m_name = fields[2].GetString();
    m_icon = fields[3].GetString();
    m_text = fields[4].GetString();
}

bool Guild::BankTab::LoadItemFromDB(Field* fields)
{
    uint8 slotId = fields[20].GetUInt8();
    uint32 itemGuid = fields[21].GetUInt32();
    uint32 itemEntry = fields[22].GetUInt32();

    if (slotId >= GUILD_BANK_MAX_SLOTS)
    {
        TC_LOG_ERROR("guild", "Invalid slot for item (GUID: %u, id: %u) in guild bank, skipped.", itemGuid, itemEntry);
        return false;
    }

    ItemTemplate const* proto = sObjectMgr->GetItemTemplate(itemEntry);
    if (!proto)
    {
        TC_LOG_ERROR("guild", "Unknown item (GUID: %u, id: %u) in guild bank, skipped.", itemGuid, itemEntry);
        return false;
    }

    Item* pItem = NewItemOrBag(proto);
    if (!pItem->LoadFromDB(itemGuid, ObjectGuid::Empty, fields, itemEntry))
    {
        TC_LOG_ERROR("guild", "Item (GUID %u, id: %u) not found in item_instance, deleting from guild bank!", itemGuid, itemEntry);

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_NONEXISTENT_GUILD_BANK_ITEM);
        stmt->setUInt32(0, m_guildId);
        stmt->setUInt8 (1, m_tabId);
        stmt->setUInt8 (2, slotId);
        CharacterDatabase.Execute(stmt);

        delete pItem;
        return false;
    }

    pItem->AddToWorld();
    m_items[slotId] = pItem;

    return true;
}

// Deletes contents of the tab from the world (and from DB if necessary)
void Guild::BankTab::Delete(SQLTransaction& trans, bool removeItemsFromDB)
{
    for (uint8 slotId = 0; slotId < GUILD_BANK_MAX_SLOTS; ++slotId)
        if (Item* pItem = m_items[slotId])
        {
            pItem->RemoveFromWorld();
            if (removeItemsFromDB)
                pItem->DeleteFromDB(trans);
            delete pItem;
            pItem = nullptr;
        }
}

void Guild::BankTab::SetInfo(std::string const& name, std::string const& icon)
{
    if (m_name == name && m_icon == icon)
        return;

    m_name = name;
    m_icon = icon;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_BANK_TAB_INFO);
    stmt->setString(0, m_name);
    stmt->setString(1, m_icon);
    stmt->setUInt32(2, m_guildId);
    stmt->setUInt8 (3, m_tabId);
    CharacterDatabase.Execute(stmt);
}

void Guild::BankTab::SetText(std::string const& text)
{
    if (m_text == text)
        return;

    m_text = text;
    utf8truncate(m_text, MAX_GUILD_BANK_TAB_TEXT_LEN);          // DB and client size limitation

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_BANK_TAB_TEXT);
    stmt->setString(0, m_text);
    stmt->setUInt32(1, m_guildId);
    stmt->setUInt8 (2, m_tabId);
    CharacterDatabase.Execute(stmt);
}

// Sets/removes contents of specified slot.
// If pItem == NULL contents are removed.
bool Guild::BankTab::SetItem(SQLTransaction& trans, uint8 slotId, Item* item)
{
    if (slotId >= GUILD_BANK_MAX_SLOTS)
        return false;

    m_items[slotId] = item;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_ITEM);
    stmt->setUInt32(0, m_guildId);
    stmt->setUInt8 (1, m_tabId);
    stmt->setUInt8 (2, slotId);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);

    if (item)
    {
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_BANK_ITEM);
        stmt->setUInt32(0, m_guildId);
        stmt->setUInt8 (1, m_tabId);
        stmt->setUInt8 (2, slotId);
        stmt->setUInt32(3, item->GetGUID().GetCounter());
        CharacterDatabase.ExecuteOrAppend(trans, stmt);

        item->SetGuidValue(ITEM_CONTAINED_IN, ObjectGuid::Empty);
        item->SetGuidValue(ITEM_OWNER, ObjectGuid::Empty);
        item->FSetState(ITEM_NEW);
        item->SaveToDB(trans);                                 // Not in inventory and can be saved standalone
    }

    return true;
}

void Guild::BankTab::SendText(Guild const* guild, WorldSession* session) const
{
    WorldPacket data(SMSG_GUILD_BANK_QUERY_TEXT_RESULT, 4 + 2 + m_text.size());

    data << uint32(m_tabId);

    data.WriteBits(m_text.length(), 14);

    data.FlushBits();

    data.WriteString(m_text);

    if (session)
        session->SendPacket(&data);
    else
        guild->BroadcastPacket(&data);
}

// Member
void Guild::Member::SetStats(Player* player)
{
    m_name      = player->GetName();
    m_level     = player->GetLevel();
    m_class     = player->GetClass();
    m_gender    = player->GetGender();
    m_zoneId    = player->GetZoneId();
    m_accountId = player->GetSession()->GetAccountId();
}

void Guild::Member::SetStats(std::string const& name, uint8 level, uint8 _class, uint8 gender, uint32 zoneId, uint32 accountId)
{
    m_name      = name;
    m_level     = level;
    m_class     = _class;
    m_gender    = gender;
    m_zoneId    = zoneId;
    m_accountId = accountId;
}

void Guild::Member::SetPublicNote(std::string const& publicNote)
{
    if (m_publicNote == publicNote)
        return;

    m_publicNote = publicNote;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_MEMBER_PNOTE);
    stmt->setString(0, publicNote);
    stmt->setUInt32(1, m_guid.GetCounter());
    CharacterDatabase.Execute(stmt);
}

void Guild::Member::SetOfficerNote(std::string const& officerNote)
{
    if (m_officerNote == officerNote)
        return;

    m_officerNote = officerNote;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_MEMBER_OFFNOTE);
    stmt->setString(0, officerNote);
    stmt->setUInt32(1, m_guid.GetCounter());
    CharacterDatabase.Execute(stmt);
}

void Guild::Member::ChangeRank(uint8 newRank)
{
    m_rankId = newRank;

    // Update rank information in player's field, if he is online.
    if (Player* player = FindPlayer())
        player->SetRank(newRank);

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_MEMBER_RANK);
    stmt->setUInt8 (0, newRank);
    stmt->setUInt32(1, m_guid.GetCounter());
    CharacterDatabase.Execute(stmt);
}

void Guild::Member::SaveToDB(SQLTransaction& trans) const
{
    PreparedStatement* stmt;

    CharacterDatabaseStatements statement;

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_GUILD_MEMBER);
    stmt->setUInt32(0, m_guid.GetCounter());

    PreparedQueryResult result = CharacterDatabase.Query(stmt);
    if (!result)
        statement = CHAR_INS_GUILD_MEMBER;
    else
        statement = CHAR_UPD_GUILD_MEMBER;

    stmt = CharacterDatabase.GetPreparedStatement(statement);

    stmt->setUInt8 (0, m_rankId);
    stmt->setString(1, m_publicNote);
    stmt->setString(2, m_officerNote);
    stmt->setUInt32(3, m_weekReputation);
    stmt->setUInt64(4, m_weekActivity);
    stmt->setUInt64(5, m_totalActivity);
    stmt->setUInt32(6, m_totalReputation);
    stmt->setUInt32(7, m_achievementPoints);
    stmt->setUInt32(8, m_guildId);
    stmt->setUInt32(9, m_guid.GetCounter());

    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

// Loads member's data from database.
// If member has broken fields (level, class) returns false.
// In this case member has to be removed from guild.
bool Guild::Member::LoadFromDB(Field* fields)
{
    m_publicNote = fields[3].GetString();
    m_officerNote = fields[4].GetString();
    m_weekReputation = fields[5].GetUInt32();
    m_weekActivity = fields[6].GetUInt64();
    m_totalActivity = fields[7].GetUInt64();

    for (uint8 i = 0; i < GUILD_BANK_MAX_TABS; ++i)
        m_bankWithdraw[i] = fields[8 + i].GetUInt32();

    m_bankWithdrawMoney = fields[13].GetUInt64();

    m_totalReputation = fields[17].GetInt32();
    m_achievementPoints = fields[18].GetUInt32();

    SetStats(fields[19].GetString(),
             fields[20].GetUInt8(),                         // characters.level
             fields[21].GetUInt8(),                         // characters.class
             fields[22].GetUInt8(),                         // characters.gender
             fields[23].GetUInt16(),                        // characters.zone
             fields[24].GetUInt32());                       // characters.account

    m_logoutTime = fields[25].GetUInt32();                  // characters.logout_time

    if (!CheckStats())
        return false;

    if (!m_zoneId)
    {
        TC_LOG_DEBUG("guild", "%s has broken zone-data", m_guid.ToString().c_str());
        m_zoneId = Player::GetZoneIdFromDB(m_guid);
    }

    ResetFlags();

    return true;
}

// Validate player fields. Returns false if corrupted fields are found.
bool Guild::Member::CheckStats() const
{
    if (m_level < 1)
    {
        TC_LOG_ERROR("guild", "%s has a broken data in field `characters`.`level`, deleting him from guild!", m_guid.ToString().c_str());
        return false;
    }

    if (m_class < CLASS_WARRIOR || m_class >= MAX_CLASSES)
    {
        TC_LOG_ERROR("guild", "%s has a broken data in field `characters`.`class`, deleting him from guild!", m_guid.ToString().c_str());
        return false;
    }

    return true;
}

// Decreases amount of slots left for today.
void Guild::Member::UpdateBankTabWithdrawValue(SQLTransaction& trans, uint8 tabId, uint32 amount)
{
    m_bankWithdraw[tabId] += amount;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_MEMBER_WITHDRAW_TABS);
    stmt->setUInt32(0, m_guid.GetCounter());

    for (uint8 i = 0; i < GUILD_BANK_MAX_TABS;)
    {
        uint32 withdraw = m_bankWithdraw[i++];
        stmt->setUInt32(i, withdraw);
    }

    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

// Decreases amount of money left for today.
void Guild::Member::UpdateBankMoneyWithdrawValue(SQLTransaction& trans, uint64 amount)
{
    m_bankWithdrawMoney += amount;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_MEMBER_WITHDRAW_MONEY);
    stmt->setUInt32(0, m_guid.GetCounter());
    stmt->setUInt64(1, m_bankWithdrawMoney);
    CharacterDatabase.ExecuteOrAppend(trans, stmt);
}

void Guild::Member::ResetValues(bool weekly /* = false*/)
{
    for (uint8 tabId = 0; tabId < GUILD_BANK_MAX_TABS; ++tabId)
        m_bankWithdraw[tabId] = 0;

    m_bankWithdrawMoney = 0;

    if (weekly)
    {
        m_weekActivity = 0;
        m_weekReputation = 0;
    }
}

void Guild::Member::IncreaseActivity(uint32 xp)
{
    m_totalActivity += xp;
    m_weekActivity += xp;
}

void Guild::Member::IncreaseReputations(Player* player, uint32 newRepGain)
{
    if (newRepGain < 0)
        newRepGain = 0;

    if (player)
        player->SetReputation(GUILD_REP, newRepGain);

    m_totalReputation = newRepGain;
    m_weekReputation = newRepGain;

    SendGuildReputationWeeklyCap();
}

std::unordered_set<uint32> Guild::Member::GetDefaultSkillSpells(uint32 skillId, uint32 skillValue) const
{
    std::unordered_set<uint32> spells;

    uint32 classMask = 1 << (GetClass() - 1);

    for (SkillLineAbilityEntry const* ability : sSkillLineAbilityStore)
    {
        if (ability->SkillLine != skillId)
            continue;

        SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(ability->SpellID);
        if (!spellInfo)
            continue;

        if (ability->AquireMethod != SKILL_LINE_ABILITY_LEARNED_ON_SKILL_VALUE && ability->AquireMethod != SKILL_LINE_ABILITY_LEARNED_ON_SKILL_LEARN)
            continue;

        // Maybe check also for race?

        // Check class if set
        if (ability->ClassMask && !(ability->ClassMask & classMask))
            continue;

        // check level, skip class spells if not high enough
        if (GetLevel() < spellInfo->SpellLevel)
            continue;

        if (skillValue >= ability->MinSkillLineRank)
            spells.insert(ability->SpellID);
    }

    return spells;
}

// EmblemInfo
void EmblemInfo::ReadPacket(WorldPacket& recv)
{
    recv >> m_backgroundColor;
    recv >> m_style;
    recv >> m_borderColor;
    recv >> m_borderStyle;
    recv >> m_color;
}

bool EmblemInfo::ValidateEmblemColors()
{
    return sGuildColorBackgroundStore.LookupEntry(m_backgroundColor) &&
           sGuildColorBorderStore.LookupEntry(m_borderColor) &&
           sGuildColorEmblemStore.LookupEntry(m_color);
}

bool EmblemInfo::LoadFromDB(Field* fields)
{
    m_style             = fields[3].GetUInt8();
    m_color             = fields[4].GetUInt8();
    m_borderStyle       = fields[5].GetUInt8();
    m_borderColor       = fields[6].GetUInt8();
    m_backgroundColor   = fields[7].GetUInt8();

    return ValidateEmblemColors();
}

void EmblemInfo::SaveToDB(uint32 guildId) const
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_EMBLEM_INFO);
    stmt->setUInt32(0, m_style);
    stmt->setUInt32(1, m_color);
    stmt->setUInt32(2, m_borderStyle);
    stmt->setUInt32(3, m_borderColor);
    stmt->setUInt32(4, m_backgroundColor);
    stmt->setUInt32(5, guildId);
    CharacterDatabase.Execute(stmt);
}

// MoveItemData
bool Guild::MoveItemData::CheckItem(uint32& splitedAmount)
{
    ASSERT(m_pItem);

    if (splitedAmount > m_pItem->GetCount())
        return false;

    if (splitedAmount == m_pItem->GetCount())
        splitedAmount = 0;

    return true;
}

bool Guild::MoveItemData::CanStore(Item* pItem, bool swap, bool sendError)
{
    m_vec.clear();

    InventoryResult msg = CanStore(pItem, swap);
    if (sendError && msg != EQUIP_ERR_OK)
        m_pPlayer->SendEquipError(msg, pItem);

    return (msg == EQUIP_ERR_OK);
}

bool Guild::MoveItemData::CloneItem(uint32 count)
{
    ASSERT(m_pItem);

    m_pClonedItem = m_pItem->CloneItem(count);
    if (!m_pClonedItem)
    {
        m_pPlayer->SendEquipError(EQUIP_ERR_ITEM_NOT_FOUND, m_pItem);
        return false;
    }

    return true;
}

void Guild::MoveItemData::LogAction(MoveItemData* pFrom) const
{
    ASSERT(pFrom->GetItem());

    sScriptMgr->OnGuildItemMove(m_pGuild, m_pPlayer, pFrom->GetItem(),
        pFrom->IsBank(), pFrom->GetContainer(), pFrom->GetSlotId(),
        IsBank(), GetContainer(), GetSlotId());
}

inline void Guild::MoveItemData::CopySlots(SlotIds& ids) const
{
    for (ItemPosCountVec::const_iterator itr = m_vec.begin(); itr != m_vec.end(); ++itr)
        ids.insert(uint8(itr->pos));
}

// PlayerMoveItemData
bool Guild::PlayerMoveItemData::InitItem()
{
    m_pItem = m_pPlayer->GetItemByPos(m_container, m_slotId);
    if (m_pItem)
    {
        // Anti-WPE protection. Do not move non-empty bags to bank.
        if (m_pItem->IsNotEmptyBag())
        {
            m_pPlayer->SendEquipError(EQUIP_ERR_DESTROY_NONEMPTY_BAG, m_pItem);
            m_pItem = nullptr;
        }
        // Bound items cannot be put into bank.
        else if (!m_pItem->CanBeTraded())
        {
            m_pPlayer->SendEquipError(EQUIP_ERR_CANT_SWAP, m_pItem);
            m_pItem = nullptr;
        }
    }

    return (m_pItem != nullptr);
}

void Guild::PlayerMoveItemData::RemoveItem(SQLTransaction& trans, MoveItemData* /*pOther*/, uint32 splitedAmount)
{
    if (splitedAmount)
    {
        m_pItem->SetCount(m_pItem->GetCount() - splitedAmount);
        m_pItem->SetState(ITEM_CHANGED, m_pPlayer);
        m_pPlayer->SaveInventoryAndGoldToDB(trans);
    }
    else
    {
        m_pPlayer->MoveItemFromInventory(m_container, m_slotId, true);
        m_pItem->DeleteFromInventoryDB(trans);
        m_pItem = NULL;
    }
}

Item* Guild::PlayerMoveItemData::StoreItem(SQLTransaction& trans, Item* pItem)
{
    ASSERT(pItem);

    m_pPlayer->MoveItemToInventory(m_vec, pItem, true);
    m_pPlayer->SaveInventoryAndGoldToDB(trans);

    return pItem;
}

void Guild::PlayerMoveItemData::LogBankEvent(SQLTransaction& trans, MoveItemData* pFrom, uint32 count) const
{
    ASSERT(pFrom);

    // Bank -> Char
    m_pGuild->_LogBankEvent(trans, GUILD_BANK_LOG_WITHDRAW_ITEM, pFrom->GetContainer(), m_pPlayer->GetGUID().GetCounter(),
        pFrom->GetItem()->GetEntry(), count);
}

inline InventoryResult Guild::PlayerMoveItemData::CanStore(Item* pItem, bool swap)
{
    return m_pPlayer->CanStoreItem(m_container, m_slotId, m_vec, pItem, swap);
}

// BankMoveItemData
bool Guild::BankMoveItemData::InitItem()
{
    m_pItem = m_pGuild->_GetItem(m_container, m_slotId);
    return (m_pItem != nullptr);
}

bool Guild::BankMoveItemData::HasStoreRights(MoveItemData* pOther) const
{
    ASSERT(pOther);

    // Do not check rights if item is being swapped within the same bank tab
    if (pOther->IsBank() && pOther->GetContainer() == m_container)
        return true;

    return m_pGuild->_MemberHasTabRights(m_pPlayer->GetGUID(), m_container, GUILD_BANK_RIGHT_DEPOSIT_ITEM);
}

bool Guild::BankMoveItemData::HasWithdrawRights(MoveItemData* pOther) const
{
    ASSERT(pOther);

    // Do not check rights if item is being swapped within the same bank tab
    if (pOther->IsBank() && pOther->GetContainer() == m_container)
        return true;

    int32 slots = 0;
    if (Member const* member = m_pGuild->GetMember(m_pPlayer->GetGUID()))
        slots = m_pGuild->_GetMemberRemainingSlots(member, m_container);

    return slots != 0;
}

void Guild::BankMoveItemData::RemoveItem(SQLTransaction& trans, MoveItemData* pOther, uint32 splitedAmount)
{
    ASSERT(m_pItem);

    if (splitedAmount)
    {
        m_pItem->SetCount(m_pItem->GetCount() - splitedAmount);
        m_pItem->FSetState(ITEM_CHANGED);
        m_pItem->SaveToDB(trans);
    }
    else
    {
        m_pGuild->_RemoveItem(trans, m_container, m_slotId);
        m_pItem = nullptr;
    }

    // Decrease amount of player's remaining items (if item is moved to different tab or to player)
    if (!pOther->IsBank() || pOther->GetContainer() != m_container)
        m_pGuild->_UpdateMemberWithdrawSlots(trans, m_pPlayer->GetGUID(), m_container);
}

Item* Guild::BankMoveItemData::StoreItem(SQLTransaction& trans, Item* pItem)
{
    if (!pItem)
        return nullptr;

    BankTab* pTab = m_pGuild->GetBankTab(m_container);
    if (!pTab)
        return nullptr;

    Item* pLastItem = pItem;
    for (ItemPosCountVec::const_iterator itr = m_vec.begin(); itr != m_vec.end(); )
    {
        ItemPosCount pos(*itr);
        ++itr;

        TC_LOG_DEBUG("guild", "GUILD STORAGE: StoreItem tab = %u, slot = %u, item = %u, count = %u",
            m_container, m_slotId, pItem->GetEntry(), pItem->GetCount());
        pLastItem = _StoreItem(trans, pTab, pItem, pos, itr != m_vec.end());
    }

    return pLastItem;
}

void Guild::BankMoveItemData::LogBankEvent(SQLTransaction& trans, MoveItemData* pFrom, uint32 count) const
{
    ASSERT(pFrom->GetItem());

    if (pFrom->IsBank())
        // Bank -> Bank
        m_pGuild->_LogBankEvent(trans, GUILD_BANK_LOG_MOVE_ITEM, pFrom->GetContainer(), m_pPlayer->GetGUID().GetCounter(),
            pFrom->GetItem()->GetEntry(), count, m_container);
    else
        // Char -> Bank
        m_pGuild->_LogBankEvent(trans, GUILD_BANK_LOG_DEPOSIT_ITEM, m_container, m_pPlayer->GetGUID().GetCounter(),
            pFrom->GetItem()->GetEntry(), count);
}

void Guild::BankMoveItemData::LogAction(MoveItemData* pFrom) const
{
    MoveItemData::LogAction(pFrom);
    if (!pFrom->IsBank() && m_pPlayer->GetSession()->HasPermission(rbac::RBAC_PERM_LOG_GM_TRADE)) /// @todo Move this to scripts
    {
        sLog->outCommand(m_pPlayer->GetSession()->GetAccountId(),
            "GM %s (%s) (Account: %u) deposit item: %s (Entry: %d Count: %u) to guild bank named: %s (Guild ID: %u)",
            m_pPlayer->GetName().c_str(), m_pPlayer->GetGUID().ToString().c_str(), m_pPlayer->GetSession()->GetAccountId(),
            pFrom->GetItem()->GetTemplate()->GetDefaultLocaleName(), pFrom->GetItem()->GetEntry(), pFrom->GetItem()->GetCount(),
            m_pGuild->GetName().c_str(), m_pGuild->GetId());
    }
}

Item* Guild::BankMoveItemData::_StoreItem(SQLTransaction& trans, BankTab* pTab, Item* pItem, ItemPosCount& pos, bool clone) const
{
    uint8 slotId = uint8(pos.pos);
    uint32 count = pos.count;
    if (Item* pItemDest = pTab->GetItem(slotId))
    {
        pItemDest->SetCount(pItemDest->GetCount() + count);
        pItemDest->FSetState(ITEM_CHANGED);
        pItemDest->SaveToDB(trans);
        if (!clone)
        {
            pItem->RemoveFromWorld();
            pItem->DeleteFromDB(trans);
            delete pItem;
        }

        return pItemDest;
    }

    if (clone)
        pItem = pItem->CloneItem(count);
    else
        pItem->SetCount(count);

    if (pItem && pTab->SetItem(trans, slotId, pItem))
        return pItem;

    return nullptr;
}

// Tries to reserve space for source item.
// If item in destination slot exists it must be the item of the same entry
// and stack must have enough space to take at least one item.
// Returns false if destination item specified and it cannot be used to reserve space.
bool Guild::BankMoveItemData::_ReserveSpace(uint8 slotId, Item* pItem, Item* pItemDest, uint32& count)
{
    uint32 requiredSpace = pItem->GetMaxStackCount();
    if (pItemDest)
    {
        // Make sure source and destination items match and destination item has space for more stacks.
        if (pItemDest->GetEntry() != pItem->GetEntry() || pItemDest->GetCount() >= pItem->GetMaxStackCount())
            return false;

        requiredSpace -= pItemDest->GetCount();
    }

    // Let's not be greedy, reserve only required space
    requiredSpace = std::min(requiredSpace, count);

    // Reserve space
    ItemPosCount pos(slotId, requiredSpace);
    if (!pos.isContainedIn(m_vec))
    {
        m_vec.push_back(pos);
        count -= requiredSpace;
    }

    return true;
}

void Guild::BankMoveItemData::CanStoreItemInTab(Item* pItem, uint8 skipSlotId, bool merge, uint32& count)
{
    for (uint8 slotId = 0; (slotId < GUILD_BANK_MAX_SLOTS) && (count > 0); ++slotId)
    {
        // Skip slot already processed in CanStore (when destination slot was specified)
        if (slotId == skipSlotId)
            continue;

        Item* pItemDest = m_pGuild->_GetItem(m_container, slotId);
        if (pItemDest == pItem)
            pItemDest = nullptr;

        // If merge skip empty, if not merge skip non-empty
        if ((pItemDest != nullptr) != merge)
            continue;

        _ReserveSpace(slotId, pItem, pItemDest, count);
    }
}

InventoryResult Guild::BankMoveItemData::CanStore(Item* pItem, bool swap)
{
    TC_LOG_DEBUG("guild", "GUILD STORAGE: CanStore() tab = %u, slot = %u, item = %u, count = %u",
        m_container, m_slotId, pItem->GetEntry(), pItem->GetCount());

    uint32 count = pItem->GetCount();

    // Soulbound items cannot be moved
    if (pItem->IsSoulBound())
        return EQUIP_ERR_DROP_BOUND_ITEM;

    // Make sure destination bank tab exists
    if (m_container >= m_pGuild->_GetPurchasedTabsSize())
        return EQUIP_ERR_WRONG_BAG_TYPE;

    // Slot explicitely specified. Check it.
    if (m_slotId != NULL_SLOT)
    {
        Item* pItemDest = m_pGuild->_GetItem(m_container, m_slotId);
        // Ignore swapped item (this slot will be empty after move)
        if ((pItemDest == pItem) || swap)
            pItemDest = nullptr;

        if (!_ReserveSpace(m_slotId, pItem, pItemDest, count))
            return EQUIP_ERR_CANT_STACK;

        if (count == 0)
            return EQUIP_ERR_OK;
    }

    // Slot was not specified or it has not enough space for all the items in stack
    // Search for stacks to merge with
    if (pItem->GetMaxStackCount() > 1)
    {
        CanStoreItemInTab(pItem, m_slotId, true, count);
        if (count == 0)
            return EQUIP_ERR_OK;
    }

    // Search free slot for item
    CanStoreItemInTab(pItem, m_slotId, false, count);
    if (count == 0)
        return EQUIP_ERR_OK;

    return EQUIP_ERR_BANK_FULL;
}

// Guild
Guild::Guild():
    m_id(0),
    m_createdDate(0),
    m_accountsNumber(0),
    m_bankMoney(0),
    m_eventLog(nullptr),
    m_newsLog(nullptr),
    m_achievementMgr(this),
    _level(1),
    _experience(0),
    _todayExperience(0),
    m_guildFlags(0)
{
    memset(&m_bankEventLog, 0, (GUILD_BANK_MAX_TABS + 1) * sizeof(LogHolder*));
}

Guild::~Guild()
{
    SQLTransaction temp(nullptr);
    _DeleteBankItems(temp);

    // Cleanup
    delete m_eventLog;
    m_eventLog = nullptr;

    delete m_newsLog;
    m_newsLog = nullptr;

    for (uint8 tabId = 0; tabId <= GUILD_BANK_MAX_TABS; ++tabId)
    {
        delete m_bankEventLog[tabId];
        m_bankEventLog[tabId] = nullptr;
    }

    for (Members::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
    {
        delete itr->second;
        itr->second = nullptr;
    }
}

// Creates new guild with default data and saves it to database.
bool Guild::Create(Player* pLeader, std::string const& name)
{
    // Check if guild with such name already exists
    if (sGuildMgr->GetGuildByName(name))
        return false;

    WorldSession* pLeaderSession = pLeader->GetSession();
    if (!pLeaderSession)
        return false;

    m_id = sGuildMgr->GenerateGuildId();
    m_Guid = ObjectGuid::Create<HighGuid::Guild>(m_id);
    m_leaderGuid = pLeader->GetGUID();
    m_name = name;
    m_info = "";
    m_motd = "No message set.";
    m_bankMoney = 0;
    m_createdDate = ::time(nullptr);
    _level = 1;
    _experience = 0;
    _todayExperience = 0;
    _CreateLogHolders();

    AddGuildNews(GUILD_NEW_CREATED, m_leaderGuid, 0, 0);

    TC_LOG_DEBUG("guild", "GUILD: creating guild [%s] for leader %s (%u)",
        name.c_str(), pLeader->GetName().c_str(), m_leaderGuid.GetCounter());

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_MEMBERS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    uint8 index = 0;
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD);
    stmt->setUInt32(  index, m_id);
    stmt->setString(++index, name);
    stmt->setUInt32(++index, m_leaderGuid.GetCounter());
    stmt->setString(++index, m_info);
    stmt->setString(++index, m_motd);
    stmt->setUInt64(++index, uint32(m_createdDate));
    stmt->setUInt32(++index, m_emblemInfo.GetStyle());
    stmt->setUInt32(++index, m_emblemInfo.GetColor());
    stmt->setUInt32(++index, m_emblemInfo.GetBorderStyle());
    stmt->setUInt32(++index, m_emblemInfo.GetBorderColor());
    stmt->setUInt32(++index, m_emblemInfo.GetBackgroundColor());
    stmt->setUInt64(++index, m_bankMoney);
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);
    _CreateDefaultGuildRanks(pLeaderSession->GetSessionDbLocaleIndex()); // Create default ranks
    bool ret = AddMember(m_leaderGuid, GR_GUILDMASTER);                  // Add guildmaster

    if (ret)
    {
        Member* leader = GetMember(m_leaderGuid);
        if (leader)
            _SendSetNewGuildMaster(nullptr, leader, false);

        sScriptMgr->OnGuildCreate(this, pLeader, name);
    }

    return ret;
}

// Disbands guild and deletes all related data from database
void Guild::Disband()
{
    // Call scripts before guild data removed from database
    sScriptMgr->OnGuildDisband(this);

    WorldPacket data(SMSG_GUILD_EVENT_DISBANDED, 0);
    BroadcastPacket(&data);

    // Remove all members
    while (!m_members.empty())
    {
        Members::const_iterator itr = m_members.begin();
        DeleteMember(itr->second->GetGUID(), true);
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_RANKS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_TABS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    // Free bank tab used memory and delete items stored in them
    _DeleteBankItems(trans, true);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_ITEMS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_RIGHTS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_EVENTLOGS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_EVENTLOGS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ALL_GUILD_ACHIEVEMENTS);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ALL_GUILD_ACHIEVEMENT_CRITERIA);
    stmt->setUInt32(0, m_id);
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);

    sGuildFinderMgr->DeleteGuild(GetGUID());

    sGuildMgr->RemoveGuild(m_id);
}

void Guild::SaveExperiences()
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_EXPERIENCES);
    stmt->setUInt64(0, GetExperience());
    stmt->setUInt64(1, GetTodayExperience());
    stmt->setUInt32(2, GetId());
    CharacterDatabase.Execute(stmt);
}

void Guild::UpdateMembers()
{
    if (membersChanged.empty())
        return;

    std::unordered_set<Member const*> members = membersChanged;
    membersChanged.clear();

    std::vector<Player*> m_MembersToSend;
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        if (Player* player = itr->second->FindPlayer())
            if (player->GetSession()->HasGuildRosterTimer())
                m_MembersToSend.push_back(player);

    if (m_MembersToSend.empty())
        return;

    uint32 pubNoteLength = 0;
    uint32 offNoteLength = 0;
    uint32 nameLength = 0;
    for (Member const* member : members)
    {
        pubNoteLength += member->GetPublicNote().length();
        offNoteLength += member->GetOfficerNote().length();
        nameLength += member->GetName().length();
    }

    for (Player* player : m_MembersToSend)
    {
        WorldPacket data(SMSG_GUILD_UPDATE_ROSTER);

        ByteBuffer memberData;

        data.WriteBits(members.size(), 17);

        for (Member const* member : members)
        {
            std::vector<std::pair<uint32, Profession>> professionsData(MAX_PRIMARY_PROFESSIONS);

            auto professions = memberProfessions.equal_range(member->GetGUID());
            if (professions.first != professions.second)
            {
                uint8 i = 0;
                for (auto prof = professions.first; prof != professions.second && i < MAX_PRIMARY_PROFESSIONS; ++prof, ++i)
                {
                    Profession memberProfession;
                    memberProfession.Rank = prof->second.second.Rank;
                    memberProfession.Step = prof->second.second.Step;

                    professionsData[i] = std::make_pair(prof->second.first, memberProfession);
                }
            }

            bool HasScrollOfResurrection = false;
            bool HasAuthenticator = false;

            size_t officerNoteLength = 0;
            std::string officerNote = "";
            if (_HasRankRight(player, GR_RIGHT_VIEWOFFNOTE))
            {
                officerNoteLength = member->GetOfficerNote().length();
                officerNote = member->GetOfficerNote();
            }

            ObjectGuid guid = member->GetGUID();

            data.WriteGuidMask(guid, 5);

            data.WriteBits(member->GetPublicNote().length(), 8);

            data.WriteGuidMask(guid, 1);

            data.WriteBits(officerNoteLength, 8);
            data.WriteBits(member->GetName().length(), 6);

            data.WriteBit(HasScrollOfResurrection);

            data.WriteGuidMask(guid, 6, 7, 4);

            data.WriteBit(HasAuthenticator);

            data.WriteGuidMask(guid, 0, 3, 2);

            for (uint8 i = 0; i < MAX_PRIMARY_PROFESSIONS; ++i)
            {
                memberData << int32(professionsData[i].second.Rank);
                memberData << int32(professionsData[i].second.Step);
                memberData << uint32(professionsData[i].first);
            }

            memberData.WriteGuidBytes(guid, 2);

            memberData << float(member->IsOnline() ? 0.0f : float(::time(nullptr) - member->GetLogoutTime()) / DAY);
            memberData << uint32(member->GetTotalReputation());

            memberData.WriteGuidBytes(guid, 0);

            memberData << uint32(member->GetZoneId());
            memberData << uint32(sWorld->getIntConfig(CONFIG_GUILD_WEEKLY_REP_CAP) - member->GetWeekReputation());
            memberData << uint32(member->GetRankId());

            memberData.WriteGuidBytes(guid, 4, 6);

            memberData << uint32(realmID);

            memberData.WriteGuidBytes(guid, 1, 7);

            memberData << uint32(member->GetAchievementPoints());
            memberData << uint64(member->GetTotalActivity());

            memberData.WriteGuidBytes(guid, 3);

            memberData << uint8(member->GetGender());

            memberData.WriteString(officerNote);

            memberData.WriteString(member->GetPublicNote());

            memberData << uint8(member->GetFlags());

            memberData.WriteString(member->GetName());

            memberData << uint8(member->GetClass());
            memberData << uint64(member->GetWeekActivity());

            memberData.WriteGuidBytes(guid, 5);

            memberData << uint8(member->GetLevel());
        }

        data.append(memberData);

        player->SendDirectMessage(&data);
    }
}

void Guild::UpdateMemberData(Player* player, uint8 dataid, uint32 value, ...)
{
    if (Member* member = GetMember(player->GetGUID()))
    {
        switch (dataid)
        {
            case GUILD_MEMBER_DATA_ZONEID:
                member->SetZoneId(value);
                break;
            case GUILD_MEMBER_DATA_ACHIEVEMENT_POINTS:
                member->SetAchievementPoints(value);
                break;
            case GUILD_MEMBER_DATA_LEVEL:
                member->SetLevel(value);
                break;
            case GUILD_MEMBER_DATA_PROFESSION_SKILL_UPDATE:
            case GUILD_MEMBER_DATA_PROFESSION_LEARNED:
            {
                va_list args;
                va_start(args, value);

                uint16 newValue = va_arg(args, uint16);
                uint16 newStep = va_arg(args, uint16);

                UpdateProfessionSkill(member, value, newValue, newStep, dataid == GUILD_MEMBER_DATA_PROFESSION_LEARNED);

                va_end(args);
                break;
            }
            case GUILD_MEMBER_DATA_PROFESSION_UNLEARNED:
                RemoveProfession(member->GetGUID(), value);
                break;
            case GUILD_MEMBER_DATA_PROFESSION_SPELL_ADDED:
                AddProfessionRecipe(member->GetGUID(), value);
                break;
            default:
                TC_LOG_ERROR("guild", "Guild::UpdateMemberData: Called with incorrect DATAID %u (value %u)", dataid, value);
                return;
        }

        membersChanged.insert(member);
    }
}

void Guild::OnPlayerStatusChange(Player* player, uint32 flag, bool state)
{
    if (Member* member = GetMember(player->GetGUID()))
    {
        if (state)
            member->AddFlag(flag);
        else
            member->RemFlag(flag);

        membersChanged.insert(member);
    }
}

bool Guild::SetName(std::string const& name)
{
    m_name = name;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_NAME);
    stmt->setString(0, name);
    stmt->setUInt32(1, GetId());
    CharacterDatabase.Execute(stmt);

    ObjectGuid guid = GetGUID();

    WorldPacket data(SMSG_GUILD_RENAMED, 1 + 8 + 1 + name.length());

    data.WriteGuidMask(guid, 0, 7, 3, 1, 5, 6);

    data.WriteBits(name.length(), 7);

    data.WriteGuidMask(guid, 2, 4);

    data.WriteString(name);

    data.WriteGuidBytes(guid, 3, 2, 6, 7, 5, 0, 1, 4);

    BroadcastPacket(&data);

    SetGuildFlag(GUILD_FLAG_RENAME, false);

    return true;
}

void Guild::HandleRoster(WorldSession* session)
{
    uint32 pubNoteLength = 0;
    uint32 offNoteLength = 0;
    uint32 nameLength = 0;

    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
    {
        Member* member = itr->second;

        pubNoteLength += member->GetPublicNote().length();
        offNoteLength += member->GetOfficerNote().length();
        nameLength += member->GetName().length();
    }

    ByteBuffer memberData;

    WorldPacket data(SMSG_GUILD_ROSTER, 5 + 4 + 4 + m_info.length() + m_motd.length() + 4 + m_members.size() * (1 + 8 + 3 + 1 + 4 +
                    PLAYER_MAX_PRIMARY_PROF * (4 + 4 + 4) + 1 + 1 + 4 + 4 + 8 + 1 + 4 + 4 + 4 + 8 + 4) + pubNoteLength + offNoteLength + nameLength);

    data.WriteBits(m_members.size(), 17);
    data.WriteBits(m_motd.length(), 10);

    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
    {
        Member* member = itr->second;

        std::vector<std::pair<uint32, Profession>> professionsData(MAX_PRIMARY_PROFESSIONS);

        auto professions = memberProfessions.equal_range(member->GetGUID());
        if (professions.first != professions.second)
        {
            uint8 i = 0;
            for (auto prof = professions.first; prof != professions.second && i < MAX_PRIMARY_PROFESSIONS; ++prof, ++i)
            {
                Profession memberProfession;
                memberProfession.Rank = prof->second.second.Rank;
                memberProfession.Step = prof->second.second.Step;

                professionsData[i] = std::make_pair(prof->second.first, memberProfession);
            }
        }

        bool HasScrollOfResurrection = false;
        bool HasAuthenticator = false;

        size_t officerNoteLength = 0;
        std::string officerNote = "";
        if (_HasRankRight(session->GetPlayer(), GR_RIGHT_VIEWOFFNOTE))
        {
            officerNoteLength = member->GetOfficerNote().length();
            officerNote = member->GetOfficerNote();
        }

        ObjectGuid guid = member->GetGUID();

        data.WriteBits(officerNoteLength, 8);

        data.WriteGuidMask(guid, 5);

        data.WriteBit(HasScrollOfResurrection);

        data.WriteBits(member->GetPublicNote().length(), 8);

        data.WriteGuidMask(guid, 7, 0, 6);

        data.WriteBits(member->GetName().length(), 6);

        data.WriteBit(HasAuthenticator);

        data.WriteGuidMask(guid, 3, 4, 1, 2);

        memberData << uint8(member->GetClass());
        memberData << uint32(member->GetTotalReputation());

        memberData.WriteString(member->GetName());

        memberData.WriteGuidBytes(guid, 0);

        for (uint8 i = 0; i < MAX_PRIMARY_PROFESSIONS; ++i)
        {
            memberData << int32(professionsData[i].second.Step);
            memberData << uint32(professionsData[i].first);
            memberData << int32(professionsData[i].second.Rank);
        }

        memberData << uint8(member->GetLevel());
        memberData << uint8(member->GetFlags());
        memberData << uint32(member->GetZoneId());
        memberData << uint32(sWorld->getIntConfig(CONFIG_GUILD_WEEKLY_REP_CAP) - member->GetWeekReputation());

        memberData.WriteGuidBytes(guid, 3);

        memberData << uint64(member->GetTotalActivity());

        memberData.WriteString(officerNote);

        memberData << float(member->IsOnline() ? 0.0f : float(::time(nullptr) - member->GetLogoutTime()) / DAY);
        memberData << uint8(member->GetGender());
        memberData << uint32(member->GetRankId());
        memberData << uint32(realmID);

        memberData.WriteGuidBytes(guid, 5, 7);

        memberData.WriteString(member->GetPublicNote());

        memberData.WriteGuidBytes(guid, 4);

        memberData << uint64(member->GetWeekActivity());
        memberData << uint32(member->GetAchievementPoints());

        memberData.WriteGuidBytes(guid, 6, 1, 2);
    }

    data.WriteBits(m_info.length(), 11);

    data.append(memberData);

    data << uint32(m_accountsNumber);

    data.AppendPackedTime(m_createdDate);

    data.WriteString(m_info);

    data << uint32(sWorld->getIntConfig(CONFIG_GUILD_WEEKLY_REP_CAP));

    data.WriteString(m_motd);

    data << uint32(m_guildFlags);

    session->SendPacket(&data);
}

void Guild::HandleQuery(WorldSession* session /*= nullptr*/)
{
    ObjectGuid guid = GetGUID();

    bool HasData = true;

    uint32 stringSize = 0;
    for (uint8 i = 0; i < _GetRanksSize(); i++)
        stringSize += m_ranks[i].GetName().length();

    WorldPacket data(SMSG_GUILD_QUERY_RESPONSE, 1 + 8 + 1 + (HasData ? (1 + 8 + 4 + _GetRanksSize() * (1 + 4 + 4) + 4 + 4 + 4 + 4 + m_name.length() + 4 + 4 + stringSize) : 0));

    data.WriteGuidMask(guid, 5);

    data.WriteBit(HasData);

    if (HasData)
    {
        data.WriteBits(_GetRanksSize(), 21);

        data.WriteGuidMask(guid, 5, 1, 4, 7);

        for (uint8 i = 0; i < _GetRanksSize(); i++)
            data.WriteBits(m_ranks[i].GetName().length(), 7);

        data.WriteGuidMask(guid, 3, 2, 0, 6);

        data.WriteBits(m_name.length(), 7);
    }

    data.WriteGuidMask(guid, 3, 7, 2, 1, 0, 4, 6);

    data.FlushBits();

    if (HasData)
    {
        data << uint32(m_emblemInfo.GetBorderStyle());
        data << uint32(m_emblemInfo.GetStyle());

        data.WriteGuidBytes(guid, 2, 7);

        data << uint32(m_emblemInfo.GetColor());
        data << uint32(realmID);

        for (uint8 i = 0; i < _GetRanksSize(); i++)
        {
            data << uint32(i);
            data << uint32(m_ranks[i].GetId());
            data.WriteString(m_ranks[i].GetName());
        }

        data.WriteString(m_name);

        data << uint32(m_emblemInfo.GetBorderColor());

        data.WriteGuidBytes(guid, 5, 4);

        data << uint32(m_emblemInfo.GetBackgroundColor());

        data.WriteGuidBytes(guid, 1, 6, 0, 3);
    }

    data.WriteGuidBytes(guid, 2, 6, 4, 0, 7, 3, 5, 1);

    if (session)
        session->SendPacket(&data);
    else
        BroadcastPacket(&data);
}

void Guild::SendGuildRankInfo(WorldSession* session /*=nullptr*/) const
{
    uint32 stringSize = 0;
    for (uint8 i = 0; i < _GetRanksSize(); i++)
        stringSize += m_ranks[i].GetName().length();

    WorldPacket data(SMSG_GUILD_RANK, 3 + _GetRanksSize() * (1 + 4 + 4 + GUILD_BANK_MAX_TABS * (4 + 4) + 4 + 4) + stringSize);

    data.WriteBits(_GetRanksSize(), 17);

    for (uint8 i = 0; i < _GetRanksSize(); i++)
        data.WriteBits(m_ranks[i].GetName().length(), 7);

    data.FlushBits();

    for (uint8 i = 0; i < _GetRanksSize(); i++)
    {
        RankInfo const* rankInfo = GetRankInfo(i);

        data << uint32(i);
        data << uint32(rankInfo->GetBankMoneyPerDay());

        for (uint8 j = 0; j < GUILD_BANK_MAX_TABS; ++j)
        {
            data << uint32(rankInfo->GetBankTabSlotsPerDay(j));
            data << uint32(rankInfo->GetBankTabRights(j));
        }

        data.WriteString(rankInfo->GetName());

        data << uint32(rankInfo->GetId());
        data << uint32(rankInfo->GetRights());
    }

    if (session)
        session->SendPacket(&data);
    else
        BroadcastPacket(&data);
}

void Guild::HandleSetAchievementTracking(WorldSession* session, std::set<uint32> const& achievementIds)
{
    Player* player = session->GetPlayer();
    if (!player)
        return;

    if (Member* member = GetMember(player->GetGUID()))
    {
        std::set<uint32> criteriaIds;

        for (uint32 achievementId : achievementIds)
            if (AchievementEntry const* achievement = sAchievementStore.LookupEntry(achievementId))
                if (CriteriaTree const* tree = sCriteriaMgr->GetCriteriaTree(achievement->CriteriaTree))
                {
                    CriteriaMgr::WalkCriteriaTree(tree, [&criteriaIds](CriteriaTree const* node)
                    {
                        if (node->Criteria)
                            criteriaIds.insert(node->Criteria->ID);
                    });
                }

        if (criteriaIds.empty())
            return;

        member->SetTrackedCriteriaIds(criteriaIds);
        m_achievementMgr.SendAllTrackedCriterias(player, member->GetTrackedCriteriaIds());
    }
}

void Guild::HandleSetMOTD(WorldSession* session, std::string const& motd)
{
    if (m_motd == motd)
        return;

    // Player must have rights to set MOTD
    if (!_HasRankRight(session->GetPlayer(), GR_RIGHT_SETMOTD))
        SendCommandResult(session, GUILD_COMMAND_EDIT_MOTD, ERR_GUILD_PERMISSIONS);
    else
    {
        m_motd = motd;

        sScriptMgr->OnGuildMOTDChanged(this, motd);

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_MOTD);
        stmt->setString(0, motd);
        stmt->setUInt32(1, m_id);
        CharacterDatabase.Execute(stmt);

        SendGuildMOTD(session, true);
    }
}

void Guild::HandleSetInfo(WorldSession* session, std::string const& info)
{
    if (m_info == info)
        return;

    // Player must have rights to set guild's info
    if (_HasRankRight(session->GetPlayer(), GR_RIGHT_MODIFY_GUILD_INFO))
    {
        m_info = info;

        sScriptMgr->OnGuildInfoChanged(this, info);

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_INFO);
        stmt->setString(0, info);
        stmt->setUInt32(1, m_id);
        CharacterDatabase.Execute(stmt);
    }
}

void Guild::HandleSetEmblem(WorldSession* session, const EmblemInfo& emblemInfo)
{
    Player* player = session->GetPlayer();
    if (!_IsLeader(player))
        SendSaveEmblemResult(session, ERR_GUILDEMBLEM_NOTGUILDMASTER); // "Only guild leaders can create emblems."
    else if (!player->HasEnoughMoney(uint64(EMBLEM_PRICE)))
        SendSaveEmblemResult(session, ERR_GUILDEMBLEM_NOTENOUGHMONEY); // "You can't afford to do that."
    else
    {
        player->ModifyMoney(-int64(EMBLEM_PRICE));

        m_emblemInfo = emblemInfo;
        m_emblemInfo.SaveToDB(m_id);

        SendSaveEmblemResult(session, ERR_GUILDEMBLEM_SUCCESS); // "Guild Emblem saved."

        HandleQuery();

        UpdateCriteria(CRITERIA_TYPE_BUY_GUILD_TABARD, player, 1);
    }
}

void Guild::_SendSetNewGuildMaster(Member const* guildMaster, Member const* newGuildMaster, bool replace) const
{
    ObjectGuid gumGuid = guildMaster ? guildMaster->GetGUID() : ObjectGuid::Empty;
    ObjectGuid newGumGuid = newGuildMaster ? newGuildMaster->GetGUID() : ObjectGuid::Empty;

    uint32 guildMasterLength = guildMaster ? guildMaster->GetName().length() : 0;
    uint32 newGuildMasterLength = newGuildMaster ? newGuildMaster->GetName().length() : 0;

    WorldPacket data(SMSG_GUILD_SET_GUILD_MASTER, 2 * (1 + 8) + 2 + guildMasterLength + newGuildMasterLength + 4 + 4);

    data.WriteGuidMask(newGumGuid, 4, 2, 7);

    data.WriteGuidMask(gumGuid, 4);

    data.WriteBits(guildMasterLength, 6);

    data.WriteGuidMask(gumGuid, 0);

    data.WriteGuidMask(newGumGuid, 6, 3);

    data.WriteBit(replace);

    data.WriteGuidMask(newGumGuid, 1, 0);

    data.WriteGuidMask(gumGuid, 1, 7, 3, 6, 2);

    data.WriteBits(newGuildMasterLength, 6);

    data.WriteGuidMask(gumGuid, 5);

    data.WriteGuidMask(newGumGuid, 5);

    data.FlushBits();

    data.WriteGuidBytes(newGumGuid, 5, 6);

    if (guildMasterLength)
        data.WriteString(guildMaster->GetName());

    if (newGuildMasterLength)
        data.WriteString(newGuildMaster->GetName());

    data.WriteGuidBytes(newGumGuid, 3, 4);

    data << int32(realmID);

    data.WriteGuidBytes(gumGuid, 6);

    data.WriteGuidBytes(newGumGuid, 0);

    data.WriteGuidBytes(gumGuid, 5);

    data.WriteGuidBytes(newGumGuid, 2, 7);

    data.WriteGuidBytes(gumGuid, 7, 4);

    data << int32(realmID);

    data.WriteGuidBytes(newGumGuid, 1);

    data.WriteGuidBytes(gumGuid, 2, 1, 3, 0);

    BroadcastPacket(&data);
}

void Guild::HandleSetNewGuildMaster(WorldSession* session, std::string const& name)
{
    Player* player = session->GetPlayer();

    // Only the guild master can throne a new guild master
    if (!_IsLeader(player))
        SendCommandResult(session, GUILD_COMMAND_CHANGE_LEADER, ERR_GUILD_PERMISSIONS);
    // Old GM must be a guild member
    else if (Member* oldGuildMaster = GetMember(player->GetGUID()))
    {
        // Same for the new one
        if (Member* newGuildMaster = GetMember(name))
        {
            _SetLeaderGUID(newGuildMaster);
            oldGuildMaster->ChangeRank(_GetLowestRankId());
            _SendSetNewGuildMaster(oldGuildMaster, newGuildMaster, false);
        }
    }
}

void Guild::HandleDethroneGuildMaster(WorldSession* session)
{
    if (Member* oldGuildMaster = GetMember(GetLeaderGUID()))
    {
        if ((time(nullptr) - oldGuildMaster->GetLogoutTime()) >= (DAY * 90))
        {
            if (Member* newGuildMaster = GetMember(session->GetPlayer()->GetGUID()))
            {
                if (newGuildMaster->GetRankId() == 1)
                {
                    _SetLeaderGUID(newGuildMaster);
                    oldGuildMaster->ChangeRank(_GetLowestRankId());
                    _SendSetNewGuildMaster(oldGuildMaster, newGuildMaster, true);
                }
                else
                    SendCommandResult(session, GUILD_COMMAND_CHANGE_LEADER, ERR_GUILD_PERMISSIONS);
            }
        }
    }
}

void Guild::HandleSetBankTabInfo(WorldSession* session, uint8 tabId, std::string const& name, std::string const& icon)
{
    BankTab* tab = GetBankTab(tabId);
    if (!tab)
    {
        TC_LOG_ERROR("guild", "Guild::HandleSetBankTabInfo: Player %s trying to change bank tab info from unexisting tab %d.",
                       session->GetPlayerInfo().c_str(), tabId);
        return;
    }

    if (!_HasRankRight(session->GetPlayer(), GR_RIGHT_MODIFY_BANK_TAB))
    {
        TC_LOG_ERROR("guild", "Guild::HandleSetBankTabInfo: Player %s is trying to modify bank tab info but doesn't have permission!",
            session->GetPlayerInfo().c_str());
        return;
    }

    tab->SetInfo(name, icon);

    WorldPacket data(SMSG_GUILD_EVENT_BANK_TAB_MODIFIED, 2 + icon.length() + name.length() + 4);

    data.WriteBits(icon.length(), 9);
    data.WriteBits(name.length(), 7);

    data.WriteString(name);

    data << uint32(tabId);

    data.WriteString(icon);

    BroadcastPacket(&data);
}

void Guild::HandleSetMemberNote(WorldSession* session, std::string const& note, ObjectGuid guid, bool isPublic)
{
    // Player must have rights to set public/officer note
    if (!_HasRankRight(session->GetPlayer(), isPublic ? GR_RIGHT_EPNOTE : GR_RIGHT_EOFFNOTE))
        SendCommandResult(session, GUILD_COMMAND_PUBLIC_NOTE, ERR_GUILD_PERMISSIONS);
    else if (Member* member = GetMember(guid))
    {
        if (isPublic)
            member->SetPublicNote(note);
        else
            member->SetOfficerNote(note);

        ObjectGuid memberGuid = member->GetGUID();

        WorldPacket data(SMSG_GUILD_MEMBER_UPDATE_NOTE, 1 + 8 + 2 + note.length());

        data.WriteGuidMask(memberGuid, 2);

        data.WriteBits(note.length(), 8);

        data.WriteBit(isPublic);

        data.WriteGuidMask(memberGuid, 5, 0, 4, 3, 1, 6, 7);

        data.FlushBits();

        data.WriteGuidBytes(memberGuid, 7, 5, 0, 1);

        data.WriteString(note);

        data.WriteGuidBytes(memberGuid, 3, 6, 4, 2);

        if (session)
            session->SendPacket(&data);
        else
            BroadcastPacket(&data);
    }
}

void Guild::HandleSetRankInfo(WorldSession* session, uint8 rankId, std::string const& name, uint32 rights, uint32 moneyPerDay, const GuildBankRightsAndSlotsVec& rightsAndSlots)
{
    // Only leader can modify ranks
    if (!_IsLeader(session->GetPlayer()))
        SendCommandResult(session, GUILD_COMMAND_CHANGE_RANK, ERR_GUILD_PERMISSIONS);
    else if (RankInfo* rankInfo = GetRankInfo(rankId))
    {
        TC_LOG_DEBUG("guild", "Changed RankName to '%s', rights to 0x%08X", name.c_str(), rights);

        rankInfo->SetName(name);
        rankInfo->SetRights(rights);
        _SetRankBankMoneyPerDay(rankId, moneyPerDay);

        for (GuildBankRightsAndSlots const& bankRights : rightsAndSlots)
            _SetRankBankTabRightsAndSlots(rankId, bankRights);

        WorldPacket data(SMSG_GUILD_EVENT_RANK_CHANGED, 4);

        data << uint32(rankId);

        BroadcastPacket(&data);
    }
}

void Guild::HandleBuyBankTab(WorldSession* session, uint8 tabId)
{
    Player* player = session->GetPlayer();
    if (!player)
        return;

    Member const* member = GetMember(player->GetGUID());
    if (!member)
        return;

    if (_GetPurchasedTabsSize() >= GUILD_BANK_MAX_TABS)
        return;

    if (tabId != _GetPurchasedTabsSize())
        return;

    // Do not get money for bank tabs that the GM bought, we had to buy them already.
    // This is just a speedup check, GetGuildBankTabPrice will return 0.
    if (tabId < GUILD_BANK_MAX_TABS - 2) // 7th tab is actually the 6th
    {
        uint32 tabCost = _GetGuildBankTabPrice(tabId) * GOLD;
        if (!tabCost)
            return;

        if (!player->HasEnoughMoney(uint64(tabCost)))                   // Should not happen, this is checked by client
            return;

        player->ModifyMoney(-int64(tabCost));
    }

    _CreateNewBankTab();

    WorldPacket data(SMSG_GUILD_EVENT_BANK_TAB_ADDED, 0);
    BroadcastPacket(&data);

    SendPermissions(session); /// Hack to force client to update permissions

    UpdateCriteria(CRITERIA_TYPE_BUY_GUILD_BANK_SLOTS, player, 1);
}

void Guild::HandleInviteMember(WorldSession* session, std::string const& name)
{
    Player* pInvitee = ObjectAccessor::FindConnectedPlayerByName(name);
    if (!pInvitee)
    {
        SendCommandResult(session, GUILD_COMMAND_INVITE, ERR_GUILD_PLAYER_NOT_FOUND_S, name);
        return;
    }

    Player* player = session->GetPlayer();
    // Do not show invitations from ignored players
    if (pInvitee->GetSocial()->HasIgnore(player->GetGUID()))
        return;

    // Auto decline invite
    if (pInvitee->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_AUTO_DECLINE_GUILD))
    {
        player->SendDeclineGuildInvitation(pInvitee->GetName(), true);
        return;
    }

    if (!sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_GUILD) && pInvitee->GetTeam() != player->GetTeam())
    {
        SendCommandResult(session, GUILD_COMMAND_INVITE, ERR_GUILD_NOT_ALLIED, name);
        return;
    }

    // Invited player cannot be in same guild
    if (pInvitee->GetGuildId() == player->GetGuildId())
    {
        SendCommandResult(session, GUILD_COMMAND_INVITE, ERR_ALREADY_IN_GUILD_S, name);
        return;
    }

    // Invited player cannot be invited
    if (pInvitee->GetGuildIdInvited())
    {
        SendCommandResult(session, GUILD_COMMAND_INVITE, ERR_ALREADY_INVITED_TO_GUILD_S, name);
        return;
    }

    // Inviting player must have rights to invite
    if (!_HasRankRight(player, GR_RIGHT_INVITE))
    {
        SendCommandResult(session, GUILD_COMMAND_INVITE, ERR_GUILD_PERMISSIONS);
        return;
    }

    SendCommandResult(session, GUILD_COMMAND_INVITE, ERR_GUILD_COMMAND_SUCCESS, name);

    TC_LOG_DEBUG("guild", "Player %s invited %s to join his Guild", player->GetName().c_str(), name.c_str());

    pInvitee->SetLastGuildInviterGUID(player->GetGUID());
    pInvitee->SetGuildIdInvited(m_id);
    _LogEvent(GUILD_EVENT_LOG_INVITE_PLAYER, player->GetGUID().GetCounter(), pInvitee->GetGUID().GetCounter());

    ObjectGuid oldGuildGuid = pInvitee->GetGuildId() ? ObjectGuid(HighGuid::Guild, pInvitee->GetGuildId()) : ObjectGuid::Empty;
    ObjectGuid newGuildGuid = GetGUID();

    WorldPacket data(SMSG_GUILD_INVITE, 2 * (1 + 8) + 3 + m_name.length() + player->GetName().length() + pInvitee->GetGuildName().length() + 36);

    data.WriteGuidMask(newGuildGuid, 4);

    data.WriteBits(m_name.length(), 7);

    data.WriteGuidMask(oldGuildGuid, 4);

    data.WriteGuidMask(newGuildGuid, 6);

    data.WriteGuidMask(oldGuildGuid, 2, 1, 5, 7);

    data.WriteGuidMask(newGuildGuid, 0);

    data.WriteGuidMask(oldGuildGuid, 3);

    data.WriteGuidMask(newGuildGuid, 5);

    data.WriteGuidMask(oldGuildGuid, 6);

    data.WriteBits(player->GetName().length(), 6);

    data.WriteGuidMask(newGuildGuid, 1, 3);

    data.WriteGuidMask(oldGuildGuid, 0);

    data.WriteGuidMask(newGuildGuid, 2);

    data.WriteBits(pInvitee->GetGuildName().length(), 7);

    data.WriteGuidMask(newGuildGuid, 7);

    data.FlushBits();

    data.WriteGuidBytes(newGuildGuid, 1);

    data << uint32(m_emblemInfo.GetBackgroundColor());

    data.WriteGuidBytes(newGuildGuid, 4);

    data.WriteString(player->GetName());

    data << uint32(m_emblemInfo.GetBorderStyle());

    data.WriteGuidBytes(oldGuildGuid, 7);

    data.WriteGuidBytes(newGuildGuid, 0, 2);

    data << uint32(m_emblemInfo.GetColor());

    data.WriteGuidBytes(oldGuildGuid, 2, 5);

    data << uint32(GetLevel());
    data << uint32(pInvitee->GetGuildId() ? realmID : 0);

    data.WriteGuidBytes(newGuildGuid, 7, 3);

    data.WriteGuidBytes(oldGuildGuid, 4);

    data << uint32(m_emblemInfo.GetBorderColor());

    data.WriteString(m_name);

    data << uint32(realmID);
    data << uint32(m_emblemInfo.GetStyle());

    data.WriteGuidBytes(oldGuildGuid, 0);

    data.WriteString(pInvitee->GetGuildName());

    data.WriteGuidBytes(newGuildGuid, 5);

    data << uint32(realmID);

    data.WriteGuidBytes(oldGuildGuid, 1);

    data.WriteGuidBytes(newGuildGuid, 6);

    data.WriteGuidBytes(oldGuildGuid, 3, 6);

    pInvitee->SendDirectMessage(&data);
}

void Guild::HandleAcceptMember(WorldSession* session)
{
    Player* player = session->GetPlayer();
    if (!sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_GUILD) &&
        player->GetTeam() != sCharacterCache->GetCharacterTeamByGuid(GetLeaderGUID()))
        return;

    AddMember(player->GetGUID());
}

void Guild::HandleLeaveMember(WorldSession* session)
{
    Player* player = session->GetPlayer();
    bool disband = false;

    // If leader is leaving
    if (_IsLeader(player))
    {
        if (m_members.size() > 1)
            // Leader cannot leave if he is not the last member
            SendCommandResult(session, GUILD_COMMAND_QUIT, ERR_GUILD_LEADER_LEAVE);
        else if (GetLevel() >= sWorld->getIntConfig(CONFIG_GUILD_UNDELETABLE_LEVEL))
            SendCommandResult(session, GUILD_COMMAND_QUIT, ERR_GUILD_UNDELETABLE_DUE_TO_LEVEL);
        else
        {
            // Guild is disbanded if leader leaves.
            Disband();
            disband = true;
        }
    }
    else
    {
        _LogEvent(GUILD_EVENT_LOG_LEAVE_GUILD, player->GetGUID().GetCounter());

        DeleteMember(player->GetGUID(), false, false);
        _SendRemovePlayerFromGuild(player->GetGUID(), player->GetName());
    }

    sCalendarMgr->RemovePlayerGuildEventsAndSignups(player->GetGUID(), GetId());

    if (disband)
        delete this;
}

void Guild::HandleRemoveMember(WorldSession* session, ObjectGuid guid)
{
    Player* player = session->GetPlayer();

    // Player must have rights to remove members
    if (!_HasRankRight(player, GR_RIGHT_REMOVE))
        SendCommandResult(session, GUILD_COMMAND_REMOVE, ERR_GUILD_PERMISSIONS);
    else if (Member* member = GetMember(guid))
    {
        std::string name = member->GetName();

        // Guild masters cannot be removed
        if (member->IsRank(GR_GUILDMASTER))
            SendCommandResult(session, GUILD_COMMAND_REMOVE, ERR_GUILD_LEADER_LEAVE);
        // Do not allow to remove player with the same rank or higher
        else
        {
            Member const* memberMe = GetMember(player->GetGUID());
            if (!memberMe || member->IsRankNotLower(memberMe->GetRankId()))
                SendCommandResult(session, GUILD_COMMAND_REMOVE, ERR_GUILD_RANK_TOO_HIGH_S, name);
            else
            {
                _LogEvent(GUILD_EVENT_LOG_UNINVITE_PLAYER, player->GetGUID().GetCounter(), guid.GetCounter());

                // After call to DeleteMember pointer to member becomes invalid
                DeleteMember(guid, false, true);
                _SendRemovePlayerFromGuild(guid, name, player->GetGUID(), player->GetName());
            }
        }
    }
}

void Guild::HandleUpdateMemberRank(WorldSession* session, ObjectGuid guid, bool demote)
{
    Player* player = session->GetPlayer();

    GuildCommandType type = demote ? GUILD_COMMAND_DEMOTE : GUILD_COMMAND_PROMOTE;

    // Player must have rights to promote
    if (!_HasRankRight(player, demote ? GR_RIGHT_DEMOTE : GR_RIGHT_PROMOTE))
        SendCommandResult(session, type, ERR_GUILD_PERMISSIONS);
    // Promoted player must be a member of guild
    else if (Member* member = GetMember(guid))
    {
        std::string name = member->GetName();
        // Player cannot promote himself
        if (member->IsSamePlayer(player->GetGUID()))
        {
            SendCommandResult(session, type, ERR_GUILD_NAME_INVALID);
            return;
        }

        Member const* memberMe = GetMember(player->GetGUID());
        ASSERT(memberMe);

        uint8 rankId = memberMe->GetRankId();

        if (demote)
        {
            // Player can demote only lower rank members
            if (member->IsRankNotLower(rankId))
            {
                SendCommandResult(session, type, ERR_GUILD_RANK_TOO_HIGH_S, name);
                return;
            }
            // Lowest rank cannot be demoted
            if (member->GetRankId() >= _GetLowestRankId())
            {
                SendCommandResult(session, type, ERR_GUILD_RANK_TOO_LOW_S, name);
                return;
            }
        }
        else
        {
            // Allow to promote only to lower rank than member's rank
            // member->GetRankId() + 1 is the highest rank that current player can promote to
            if (member->IsRankNotLower(rankId + 1))
            {
                SendCommandResult(session, type, ERR_GUILD_RANK_TOO_HIGH_S, name);
                return;
            }
        }

        uint32 newRankId = member->GetRankId() + (demote ? 1 : -1);
        _SendGuildRanksUpdate(player->GetGUID(), member->GetGUID(), newRankId);
    }
}

void Guild::HandleSetMemberRank(WorldSession* session, ObjectGuid targetGuid, ObjectGuid setterGuid, uint32 rank)
{
    Player* player = session->GetPlayer();

    Member* member = GetMember(targetGuid);
    if (!member)
        return;

    GuildRankRights rights = GR_RIGHT_PROMOTE;
    GuildCommandType type = GUILD_COMMAND_PROMOTE;

    if (!IsMember(targetGuid))
        return;

    if (rank > member->GetRankId())
    {
        rights = GR_RIGHT_DEMOTE;
        type = GUILD_COMMAND_DEMOTE;
    }

    // Promoted player must be a member of guild
    if (!_HasRankRight(player, rights))
    {
        SendCommandResult(session, type, ERR_GUILD_PERMISSIONS);
        return;
    }

    // Player cannot promote himself
    if (member->IsSamePlayer(player->GetGUID()))
    {
        SendCommandResult(session, type, ERR_GUILD_NAME_INVALID);
        return;
    }

    _SendGuildRanksUpdate(setterGuid, targetGuid, rank);
}

void Guild::HandleAddNewRank(WorldSession* session, std::string const& name)
{
    uint8 size = _GetRanksSize();
    if (size >= GUILD_RANKS_MAX_COUNT)
        return;

    // Only leader can add new rank
    if (_IsLeader(session->GetPlayer()))
        if (_CreateRank(name, GR_RIGHT_GCHATLISTEN | GR_RIGHT_GCHATSPEAK))
        {
            WorldPacket data(SMSG_GUILD_EVENT_RANKS_UPDATED, 0);
            BroadcastPacket(&data);
        }
}

void Guild::HandleRemoveRank(WorldSession* session, uint8 rankId)
{
    // Cannot remove rank if total count is minimum allowed by the client or is not leader
    if (_GetRanksSize() <= GUILD_RANKS_MIN_COUNT || rankId >= _GetRanksSize() || !_IsLeader(session->GetPlayer()))
        return;

    // Delete bank rights for rank
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_RIGHTS_FOR_RANK);
    stmt->setUInt32(0, m_id);
    stmt->setUInt8(1, rankId);
    CharacterDatabase.Execute(stmt);

    // Delete rank
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_RANK);
    stmt->setUInt32(0, m_id);
    stmt->setUInt8(1, rankId);
    CharacterDatabase.Execute(stmt);

    m_ranks.erase(m_ranks.begin() + rankId);

    WorldPacket data(SMSG_GUILD_EVENT_RANKS_UPDATED, 0);
    BroadcastPacket(&data);
}

void Guild::HandleSwapRanks(WorldSession* session, uint32 id, bool up)
{
    RankInfo* rankinfo = nullptr;
    RankInfo* rankinfo2 = nullptr;

    uint32 id2 = id - (-1 + 2 * uint8(up));

    for (uint32 i = 0; i < m_ranks.size(); ++i)
    {
        if (m_ranks[i].GetId() == id)
            rankinfo = &m_ranks[i];
        if (m_ranks[i].GetId() == id2)
            rankinfo2 = &m_ranks[i];
    }

    if (!rankinfo || !rankinfo2)
        return;

    RankInfo tmp = *rankinfo2;

    rankinfo2->SetName(rankinfo->GetName());
    rankinfo2->SetRights(rankinfo->GetRights());

    rankinfo->SetName(tmp.GetName());
    rankinfo->SetRights(tmp.GetRights());

    HandleQuery();
    //HandleRoster();                                             // Broadcast for tab rights update
    SendGuildRankInfo();
}

void Guild::HandleMemberDepositMoney(WorldSession* session, uint64 amount, bool cashFlow /*=false*/)
{
    // guild bank cannot have more than MAX_MONEY_AMOUNT
    amount = std::min(amount, MAX_MONEY_AMOUNT - m_bankMoney);
    if (!amount)
        return;

    Player* player = session->GetPlayer();

    // Call script after validation and before money transfer.
    sScriptMgr->OnGuildMemberDepositMoney(this, player, amount);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    _ModifyBankMoney(trans, amount, true);
    if (!cashFlow)
    {
        player->ModifyMoney(-int64(amount));
        player->SaveGoldToDB(trans);
    }

    _LogBankEvent(trans, cashFlow ? GUILD_BANK_LOG_CASH_FLOW_DEPOSIT : GUILD_BANK_LOG_DEPOSIT_MONEY, uint8(0), player->GetGUID().GetCounter(), amount);
    CharacterDatabase.CommitTransaction(trans);

    WorldPacket data(SMSG_GUILD_EVENT_BANK_MONEY_CHANGED, 8);

    data << uint64(m_bankMoney);

    BroadcastPacket(&data);

    if (player->GetSession()->HasPermission(rbac::RBAC_PERM_LOG_GM_TRADE))
    {
        sLog->outCommand(player->GetSession()->GetAccountId(),
            "GM %s (Account: %u) deposit money (Amount: " UI64FMTD ") to guild bank (Guild ID %u)",
            player->GetName().c_str(), player->GetSession()->GetAccountId(), amount, m_id);
    }
}

bool Guild::HandleMemberWithdrawMoney(WorldSession* session, uint64 amount, bool repair)
{
    //clamp amount to MAX_MONEY_AMOUNT, Players can't hold more than that anyway
    amount = std::min(amount, MAX_MONEY_AMOUNT);

    if (m_bankMoney < amount)                               // Not enough money in bank
        return false;

    Player* player = session->GetPlayer();

    Member* member = GetMember(player->GetGUID());
    if (!member)
        return false;

    if (!_HasRankRight(player, repair ? GR_RIGHT_WITHDRAW_REPAIR : GR_RIGHT_WITHDRAW_GOLD))
        return false;

    if (_GetMemberRemainingMoney(member) < int64(amount))   // Check if we have enough slot/money today
       return false;

    // Call script after validation and before money transfer.
    sScriptMgr->OnGuildMemberWitdrawMoney(this, player, amount, repair);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    // Add money to player (if required)
    if (!repair)
    {
        if (!player->ModifyMoney(amount))
            return false;

        player->SaveGoldToDB(trans);
    }
    else
        UpdateCriteria(CRITERIA_TYPE_SPENT_GOLD_GUILD_REPAIRS, player, amount);

    // Update remaining money amount
    member->UpdateBankMoneyWithdrawValue(trans, amount);
    // Remove money from bank
    _ModifyBankMoney(trans, amount, false);

    // Log guild bank event
    _LogBankEvent(trans, repair ? GUILD_BANK_LOG_REPAIR_MONEY : GUILD_BANK_LOG_WITHDRAW_MONEY, uint8(0), player->GetGUID().GetCounter(), amount);
    CharacterDatabase.CommitTransaction(trans);

    WorldPacket data(SMSG_GUILD_EVENT_BANK_MONEY_CHANGED, 8);

    data << uint64(m_bankMoney);

    BroadcastPacket(&data);

    return true;
}

void Guild::HandleMemberLogout(WorldSession* session)
{
    Player* player = session->GetPlayer();
    if (Member* member = GetMember(player->GetGUID()))
    {
        member->SetStats(player);
        member->UpdateLogoutTime();
        member->ResetFlags();

        SQLTransaction trans(nullptr);
        member->SaveToDB(trans);
    }

    SendPresenceStatus(session, false);
}

void Guild::HandleDisband(WorldSession* session)
{
    // Only leader can disband guild
    if (_IsLeader(session->GetPlayer()))
    {
        Disband();
        TC_LOG_DEBUG("guild", "Guild Successfully Disbanded");
        delete this;
    }
}

void Guild::HandleGuildPartyRequest(WorldSession* session)
{
    Player* player = session->GetPlayer();
    Group* group = player->GetGroup();

    // Make sure player is a member of the guild and that he is in a group.
    if (!IsMember(player->GetGUID()) || !group)
        return;

    bool IsGuildGroup = group->IsGuildGroupForPlayer(player) > 0;

    WorldPacket data(SMSG_GUILD_PARTY_STATE_RESPONSE, 4 + 4 + 4 + 1);

    data << uint32(0);                                                              // Needed guild members
    data << float(0.f);                                                             // Guild XP multiplier
    data << uint32(0);                                                              // Current guild members

    data.WriteBit(IsGuildGroup);                                                    // Is guild group

    data.FlushBits();

    session->SendPacket(&data);
}

void Guild::SendEventLog(WorldSession* session) const
{
    uint32 size = m_eventLog->GetSize();
    GuildLog* logs = m_eventLog->GetGuildLog();

    if (!logs)
        return;

    WorldPacket data(SMSG_GUILD_EVENT_LOG_QUERY_RESULT, 3 + size * (2 * (1 + 8) + 1 + 4 + 1));

    ByteBuffer buffer;

    data.WriteBits(size, 21);

    for (LogEntry* log : *logs)
    {
        EventLogEntry* event = (EventLogEntry*)(log);
        event->WritePacket(data, buffer);
    }

    data.append(buffer);

    session->SendPacket(&data);
}

void Guild::SendNewsUpdate(WorldSession* session)
{
    uint32 size = m_newsLog->GetSize();
    GuildLog* logs = m_newsLog->GetGuildLog();

    if (!logs)
        return;

    uint32 achievementMembersSize = 0;
    for (LogEntry const* log : *logs)
    {
        NewsLogEntry* news = (NewsLogEntry*)(log);
        achievementMembersSize += news->guildAchievementPlayers.size();
    }

    WorldPacket data(SMSG_GUILD_NEWS_UPDATE, 3 + size * (1 + 8 + 3 + 4 + 4 + 4 + 4 + 4 + 4) + achievementMembersSize * (1 + 8));

    ByteBuffer buffer;

    data.WriteBits(size, 19);

    for (LogEntry const* log : *logs)
    {
        NewsLogEntry* news = (NewsLogEntry*)(log);
        news->WritePacket(data, buffer);
    }

    data.append(buffer);

    session->SendPacket(&data);
}

void Guild::SendBankLog(WorldSession* session, uint8 tabId) const
{
    // GUILD_BANK_MAX_TABS send by client for money log
    if (tabId < _GetPurchasedTabsSize() || tabId == GUILD_BANK_MAX_TABS)
    {
        LogHolder* bankEventLog = m_bankEventLog[tabId];

        uint32 size = bankEventLog->GetSize();
        GuildLog* logs = bankEventLog->GetGuildLog();

        if (!logs)
            return;

        bool HasCashFlowPerk = GetLevel() >= 5 && tabId == GUILD_BANK_MAX_TABS;

        WorldPacket data(SMSG_GUILD_BANK_LOG_QUERY_RESULT, 4 + 3 + (HasCashFlowPerk ? 8 : 0) + size * (1 + 8 + 1 + 1 + 4 + 1 + 4 + 8 + 4));

        ByteBuffer buffer;

        data << uint32(tabId);

        data.WriteBit(HasCashFlowPerk);

        data.WriteBits(size, 21);

        for (LogEntry const* log : *logs)
        {
            BankEventLogEntry* bank = (BankEventLogEntry*)(log);
            bank->WritePacket(data, buffer);
        }

        data.append(buffer);

        // CashFlowContribution
        if (HasCashFlowPerk)
            data << uint64(0);

        session->SendPacket(&data);
    }
}

void Guild::SendBankTabText(WorldSession* session, uint8 tabId) const
{
    if (BankTab const* tab = GetBankTab(tabId))
        tab->SendText(this, session);
}

void Guild::SendPermissions(WorldSession* session) const
{
    Member const* member = GetMember(session->GetPlayer()->GetGUID());
    if (!member)
        return;

    uint8 rankId = member->GetRankId();

    WorldPacket data(SMSG_GUILD_PERMISSIONS_QUERY_RESULTS, 4 + 4 + 4 + 4 + 3 + GUILD_BANK_MAX_TABS * (4 + 4));

    data << uint32(rankId);
    data << uint32(_GetRankBankMoneyPerDay(rankId));
    data << uint32(_GetPurchasedTabsSize());
    data << uint32(_GetRankRights(rankId));

    data.WriteBits(GUILD_BANK_MAX_TABS, 21);

    data.FlushBits();

    for (uint8 tabId = 0; tabId < GUILD_BANK_MAX_TABS; ++tabId)
    {
        data << uint32(_GetMemberRemainingSlots(member, tabId));
        data << uint32(_GetRankBankTabRights(rankId, tabId));
    }

    session->SendPacket(&data);
}

void Guild::SendMoneyInfo(WorldSession* session) const
{
    Member const* member = GetMember(session->GetPlayer()->GetGUID());
    if (!member)
        return;

    int64 amount = _GetMemberRemainingMoney(member);

    WorldPacket data(SMSG_GUILD_BANK_MONEY_WITHDRAWN, 8);

    data << int64(amount);

    session->SendPacket(&data);
}

void Guild::SendLoginInfo(WorldSession* session)
{
    Player* player = session->GetPlayer();
    Member* member = GetMember(player->GetGUID());
    if (!member)
        return;

    SendGuildMOTD(session);
    SendGuildRankInfo(session);
    SendPresenceStatus(session, true);

    if (!sWorld->getBoolConfig(CONFIG_GUILD_LEVELING_ENABLED))
        return;

    for (GuildPerkSpellsEntry const* entry : sGuildPerkSpellsStore)
        if (entry->GuildLevel <= GetLevel())
            player->LearnSpell(entry->SpellID, true);

    member->SendGuildReputationWeeklyCap();
    m_achievementMgr.SendAllData(player);

    member->SetStats(player);
    member->AddFlag(GUILDMEMBER_STATUS_ONLINE);

    UpdateCriteria(CRITERIA_TYPE_ON_LOGIN, player);
}

void Guild::SendGuildMOTD(WorldSession* session, bool change)
{
    WorldPacket data(SMSG_GUILD_EVENT_MOTD, 2 + m_motd.length());

    data.WriteBits(m_motd.size(), 10);

    data.FlushBits();

    data.WriteString(m_motd.c_str());

    if (!change)
        session->SendPacket(&data);
    else
        BroadcastPacket(&data);
}

void Guild::SendPresenceStatus(WorldSession* session, bool online)
{
    Player* player = session->GetPlayer();
    ObjectGuid guid = player->GetGUID();

    WorldPacket data(SMSG_GUILD_EVENT_PRESENCE_CHANGE, 1 + 8 + 1 + 4 + player->GetName().size());

    data.WriteGuidMask(guid, 0, 6);

    data.WriteBit(0); // Mobile

    data.WriteGuidMask(guid, 2, 5, 3);

    data.WriteBits(player->GetName().size(), 6);

    data.WriteGuidMask(guid, 1, 7, 4);

    data.WriteBit(online);

    data.WriteGuidBytes(guid, 3, 2, 0);

    data << uint32(realmID);

    data.WriteGuidBytes(guid, 6);

    data.WriteString(player->GetName());

    data.WriteGuidBytes(guid, 4, 5, 7, 1);

    BroadcastPacket(&data);
}

// Loading methods
bool Guild::LoadFromDB(Field* fields)
{
    m_id            = fields[0].GetUInt32();

    m_Guid          = ObjectGuid::Create<HighGuid::Guild>(m_id);

    m_name          = fields[1].GetString();
    m_leaderGuid    = ObjectGuid::Create<HighGuid::Player>(fields[2].GetUInt32());

    if (!m_emblemInfo.LoadFromDB(fields))
    {
        TC_LOG_ERROR("guild", "Guild %u has invalid emblem colors (Background: %u, Border: %u, Emblem: %u), skipped.",
            m_id, m_emblemInfo.GetBackgroundColor(), m_emblemInfo.GetBorderColor(), m_emblemInfo.GetColor());
        return false;
    }

    m_info          = fields[8].GetString();
    m_motd          = fields[9].GetString();
    m_createdDate   = time_t(fields[10].GetUInt32());
    m_bankMoney     = fields[11].GetUInt64();
    _level          = fields[12].GetUInt32();
    m_guildFlags    = fields[13].GetUInt32();
    _experience     = fields[14].GetUInt64();
    _todayExperience = fields[15].GetUInt64();

    uint8 purchasedTabs = fields[16].GetUInt8();
    if (purchasedTabs > GUILD_BANK_MAX_TABS)
        purchasedTabs = GUILD_BANK_MAX_TABS;

    m_bankTabs.resize(purchasedTabs);
    for (uint8 i = 0; i < purchasedTabs; ++i)
        m_bankTabs[i] = new BankTab(m_id, i);

    _CreateLogHolders();

    return true;
}

void Guild::LoadRankFromDB(Field* fields)
{
    RankInfo rankInfo(m_id);

    rankInfo.LoadFromDB(fields);

    m_ranks.push_back(rankInfo);
}

bool Guild::LoadMemberFromDB(Field* fields)
{
    uint32 lowguid = fields[1].GetUInt32();
    ObjectGuid playerGuid(HighGuid::Player, lowguid);

    Member* member = new Member(m_id, playerGuid, fields[2].GetUInt8());
    if (!member->LoadFromDB(fields))
    {
        _DeleteMemberFromDB(lowguid);
        delete member;
        return false;
    }

    // Load professions
    QueryResult result = CharacterDatabase.PQuery("SELECT skill, value, max FROM character_skills WHERE guid = '%u'", lowguid);
    if (result)
    {
        do
        {
            Field* skill = result->Fetch();
            int32 skillId = skill[0].GetInt32();
            int32 value = skill[1].GetInt32();
            int32 max = skill[2].GetInt32();

            SkillLineEntry const* skillLine = sSkillLineStore.LookupEntry(skillId);
            if (!skillLine)
                continue;

            if (skillLine->CategoryID == SKILL_CATEGORY_PROFESSION)
            {
                int32 step = max / 75;

                Profession profession;
                profession.Rank = value;
                profession.Step = step;

                // Get default recipes which aren't saved in DB
                std::unordered_set<uint32> defaultRecipes = member->GetDefaultSkillSpells(skillId, value);
                profession.Recipes.insert(defaultRecipes.begin(), defaultRecipes.end());

                // Get list of recipes from DB
                QueryResult res = CharacterDatabase.PQuery("SELECT guid, spell FROM character_spell WHERE guid = '%u'", lowguid);
                if (res)
                {
                    do
                    {
                        uint32 spellId = (*res)[1].GetUInt32();
                        if (SpellInfo const* spell = sSpellMgr->GetSpellInfo(spellId))
                            if (spell->HasAttribute(SPELL_ATTR0_TRADESPELL))
                                profession.Recipes.insert(spellId);
                    } while (res->NextRow());
                }

                memberProfessions.insert(MemberProfessionContainer::value_type(member->GetGUID(), std::make_pair(skillId, profession)));

                guildProfessions[skillId].insert(profession.Recipes.begin(), profession.Recipes.end());

                for (uint32 recipe : profession.Recipes)
                    guildRecipes[recipe].insert(member->GetGUID());
            }

        } while (result->NextRow());
    }

    sCharacterCache->UpdateCharacterGuildId(playerGuid, GetId());
    m_members[lowguid] = member;

    return true;
}

void Guild::LoadBankRightFromDB(Field* fields)
{
                                           // tabId              rights                slots
    GuildBankRightsAndSlots rightsAndSlots(fields[1].GetUInt8(), fields[3].GetUInt8(), fields[4].GetUInt32());
                                  // rankId
    _SetRankBankTabRightsAndSlots(fields[2].GetUInt8(), rightsAndSlots, false);
}

bool Guild::LoadEventLogFromDB(Field* fields)
{
    if (m_eventLog->CanInsert())
    {
        m_eventLog->LoadEvent(new EventLogEntry(
            m_id,                                       // guild id
            fields[1].GetUInt32(),                      // guid
            time_t(fields[6].GetUInt32()),              // timestamp
            GuildEventLogTypes(fields[2].GetUInt8()),   // event type
            fields[3].GetUInt32(),                      // player guid 1
            fields[4].GetUInt32(),                      // player guid 2
            fields[5].GetUInt8()));                     // rank
        return true;
    }

    return false;
}

bool Guild::LoadBankEventLogFromDB(Field* fields)
{
    uint8 dbTabId = fields[1].GetUInt8();

    bool isMoneyTab = (dbTabId == GUILD_BANK_MONEY_LOGS_TAB);

    if (dbTabId < _GetPurchasedTabsSize() || isMoneyTab)
    {
        uint8 tabId = isMoneyTab ? uint8(GUILD_BANK_MAX_TABS) : dbTabId;
        LogHolder* pLog = m_bankEventLog[tabId];
        if (pLog->CanInsert())
        {
            uint32 guid = fields[2].GetUInt32();
            GuildBankEventLogTypes eventType = GuildBankEventLogTypes(fields[3].GetUInt8());
            if (BankEventLogEntry::IsMoneyEvent(eventType))
            {
                if (!isMoneyTab)
                {
                    TC_LOG_ERROR("guild", "GuildBankEventLog ERROR: MoneyEvent(LogGuid: %u, Guild: %u) does not belong to money tab (%u), ignoring...", guid, m_id, dbTabId);
                    return false;
                }
            }
            else if (isMoneyTab)
            {
                TC_LOG_ERROR("guild", "GuildBankEventLog ERROR: non-money event (LogGuid: %u, Guild: %u) belongs to money tab, ignoring...", guid, m_id);
                return false;
            }
            pLog->LoadEvent(new BankEventLogEntry(
                m_id,                                   // guild id
                guid,                                   // guid
                time_t(fields[8].GetUInt32()),          // timestamp
                dbTabId,                                // tab id
                eventType,                              // event type
                fields[4].GetUInt32(),                  // player guid
                fields[5].GetUInt32(),                  // item or money
                fields[6].GetUInt16(),                  // itam stack count
                fields[7].GetUInt8()));                 // dest tab id
        }
    }

    return true;
}

void Guild::LoadGuildNewsLogFromDB(Field* fields)
{
    if (!m_newsLog->CanInsert())
        return;

    m_newsLog->LoadEvent(new NewsLogEntry(
    m_id,                                       // guild id
    fields[1].GetUInt32(),                      // guid
    fields[6].GetUInt32(),                      // timestamp //64 bits?
    GuildNews(fields[2].GetUInt8()),            // type
    fields[3].GetUInt32(),                      // player guid
    fields[4].GetUInt32(),                      // Flags
    fields[5].GetUInt32()));                    // value
}

void Guild::LoadBankTabFromDB(Field* fields)
{
    uint8 tabId = fields[1].GetUInt8();

    if (tabId >= _GetPurchasedTabsSize())
        TC_LOG_ERROR("guild", "Invalid tab (tabId: %u) in guild bank, skipped.", tabId);
    else
        m_bankTabs[tabId]->LoadFromDB(fields);
}

bool Guild::LoadBankItemFromDB(Field* fields)
{
    uint8 tabId = fields[19].GetUInt8();

    if (tabId >= _GetPurchasedTabsSize())
    {
        TC_LOG_ERROR("guild", "Invalid tab for item (GUID: %u, id: #%u) in guild bank, skipped.",
            fields[21].GetUInt32(), fields[22].GetUInt32());
        return false;
    }

    return m_bankTabs[tabId]->LoadItemFromDB(fields);
}

// Validates guild data loaded from database. Returns false if guild should be deleted.
bool Guild::Validate()
{
    // Validate ranks data
    // GUILD RANKS represent a sequence starting from 0 = GUILD_MASTER (ALL PRIVILEGES) to max 9 (lowest privileges).
    // The lower rank id is considered higher rank - so promotion does rank-- and demotion does rank++
    // Between ranks in sequence cannot be gaps - so 0, 1, 2, 4 is impossible
    // Min ranks count is 2 and max is 10.
    bool broken_ranks = false;

    uint8 ranks = _GetRanksSize();
    if (ranks < GUILD_RANKS_MIN_COUNT || ranks > GUILD_RANKS_MAX_COUNT)
    {
        TC_LOG_ERROR("guild", "Guild %u has invalid number of ranks, creating new...", m_id);
        broken_ranks = true;
    }
    else
    {
        for (uint8 rankId = 0; rankId < ranks; ++rankId)
        {
            RankInfo* rankInfo = GetRankInfo(rankId);
            if (rankInfo->GetId() != rankId)
            {
                TC_LOG_ERROR("guild", "Guild %u has broken rank id %u, creating default set of ranks...", m_id, rankId);
                broken_ranks = true;
            }
            else
            {
                SQLTransaction trans = CharacterDatabase.BeginTransaction();
                rankInfo->CreateMissingTabsIfNeeded(_GetPurchasedTabsSize(), trans, true);
                CharacterDatabase.CommitTransaction(trans);
            }
        }
    }

    if (broken_ranks)
    {
        m_ranks.clear();
        _CreateDefaultGuildRanks(DEFAULT_LOCALE);
    }

    // Validate members' data
    for (Members::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        if (itr->second->GetRankId() > _GetRanksSize())
            itr->second->ChangeRank(_GetLowestRankId());

    // Repair the structure of the guild.
    // If the guildmaster doesn't exist or isn't member of the guild
    // attempt to promote another member.
    Member* pLeader = GetMember(m_leaderGuid);
    if (!pLeader)
    {
        DeleteMember(m_leaderGuid);
        // If no more members left, disband guild
        if (m_members.empty())
        {
            Disband();
            return false;
        }
    }
    else if (!pLeader->IsRank(GR_GUILDMASTER))
        _SetLeaderGUID(pLeader);

    // Check config if multiple guildmasters are allowed
    if (!Tod::GetConfig().Get<bool>("Guild.AllowMultipleGuildMaster", 0))
        for (Members::iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
            if (itr->second->GetRankId() == GR_GUILDMASTER && !itr->second->IsSamePlayer(m_leaderGuid))
                itr->second->ChangeRank(GR_OFFICER);

    _UpdateAccountsNumber();

    return true;
}

// Broadcasts
void Guild::BroadcastToGuild(WorldSession* session, bool officerOnly, std::string const& msg, uint32 language) const
{
    if (session && session->GetPlayer() && _HasRankRight(session->GetPlayer(), officerOnly ? GR_RIGHT_OFFCHATSPEAK : GR_RIGHT_GCHATSPEAK))
    {
        WorldPacket data;
        ChatHandler::BuildChatPacket(data, officerOnly ? CHAT_MSG_OFFICER : CHAT_MSG_GUILD, Language(language), session->GetPlayer(), NULL, msg);
        for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
            if (Player* player = itr->second->FindConnectedPlayer())
                if (player->GetSession() && _HasRankRight(player, officerOnly ? GR_RIGHT_OFFCHATLISTEN : GR_RIGHT_GCHATLISTEN) &&
                    !player->GetSocial()->HasIgnore(session->GetPlayer()->GetGUID()))
                    player->SendDirectMessage(&data);
    }
}

void Guild::BroadcastAddonToGuild(WorldSession* session, bool officerOnly, std::string const& msg, std::string const& prefix) const
{
    if (session && session->GetPlayer() && _HasRankRight(session->GetPlayer(), officerOnly ? GR_RIGHT_OFFCHATSPEAK : GR_RIGHT_GCHATSPEAK))
    {
        WorldPacket data;
        ChatHandler::BuildChatPacket(data, officerOnly ? CHAT_MSG_OFFICER : CHAT_MSG_GUILD, LANG_ADDON, session->GetPlayer(), NULL, msg, 0, "", DEFAULT_LOCALE, prefix);
        for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
            if (Player* player = itr->second->FindPlayer())
                if (player->GetSession() && _HasRankRight(player, officerOnly ? GR_RIGHT_OFFCHATLISTEN : GR_RIGHT_GCHATLISTEN) &&
                    !player->GetSocial()->HasIgnore(session->GetPlayer()->GetGUID()) &&
                    player->GetSession()->IsAddonRegistered(prefix))
                        player->SendDirectMessage(&data);
    }
}

void Guild::BroadcastPacketToRank(WorldPacket* packet, uint8 rankId) const
{
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        if (itr->second->IsRank(rankId))
            if (Player* player = itr->second->FindConnectedPlayer())
                player->SendDirectMessage(packet);
}

void Guild::BroadcastPacket(WorldPacket* packet) const
{
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        if (Player* player = itr->second->FindPlayer())
            player->SendDirectMessage(packet);
}

void Guild::BroadcastPacketIfTrackingAchievement(WorldPacket* packet, uint32 criteriaId) const
{
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        if (itr->second->IsTrackingCriteriaId(criteriaId))
            if (Player* player = itr->second->FindPlayer())
                player->SendDirectMessage(packet);
}

void Guild::MassInviteToEvent(WorldSession* session, uint8 minLevel, uint8 maxLevel, uint8 minRank)
{
    std::vector<std::pair<ObjectGuid, uint8>> invitedMembers;
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
    {
        // not sure if needed, maybe client checks it as well
        if (invitedMembers.size() >= CALENDAR_MAX_INVITES)
        {
            if (Player* player = session->GetPlayer())
                sCalendarMgr->SendCalendarCommandResult(player->GetGUID(), CALENDAR_ERROR_INVITES_EXCEEDED);
            return;
        }

        Member* member = itr->second;
        if (!member)
            continue;

        uint8 level = sCharacterCache->GetCharacterLevelByGuid(member->GetGUID());
        if (member->GetGUID() == session->GetPlayer()->GetGUID() || level < minLevel || level > maxLevel || !member->IsRankNotLower(minRank))
            continue;

        invitedMembers.emplace_back(member->GetGUID(), level);
    }

    ByteBuffer inviteeData;

    WorldPacket data(SMSG_CALENDAR_EVENT_INITIAL_INVITE, 3 + invitedMembers.size() * (1 + 8 + 1));

    data.WriteBits(invitedMembers.size(), 23);

    for (auto & itr : invitedMembers)
    {
        ObjectGuid guid = itr.first;
        uint8 level = itr.second;

        data.WriteGuidMask(guid, 1, 7, 5, 0, 4, 3, 6, 2);

        inviteeData << uint8(level);

        inviteeData.WriteGuidBytes(guid, 3, 5, 4, 6, 7, 0, 2, 1);
    }

    data.append(inviteeData);

    session->SendPacket(&data);
}

// Members handling
bool Guild::AddMember(ObjectGuid guid, uint8 rankId)
{
    Player* player = ObjectAccessor::FindConnectedPlayer(guid);
    // Player cannot be in guild
    if (player)
    {
        if (player->GetGuildId() != 0)
            return false;
    }
    else if (sCharacterCache->GetCharacterGuildIdByGuid(guid) != 0)
        return false;

    // Remove all player signs from another petitions
    // This will be prevent attempt to join many guilds and corrupt guild data integrity
    Player::RemovePetitionsAndSigns(guid);

    uint32 lowguid = guid.GetCounter();

    // If rank was not passed, assign lowest possible rank
    if (rankId == GUILD_RANK_NONE)
        rankId = _GetLowestRankId();

    Member* member = new Member(m_id, guid, rankId);
    std::string name;
    if (player)
    {
        m_members[lowguid] = member;
        player->SetInGuild(m_id);
        player->SetGuildIdInvited(0);
        player->SetRank(rankId);
        player->SetGuildLevel(GetLevel());

        member->IncreaseReputations(player, 1);

        member->SetAchievementPoints(player->GetAchievementPoints());

        SendLoginInfo(player->GetSession());
        name = player->GetName();
    }
    else
    {
        member->ResetFlags();

        bool ok = false;
        // Player must exist
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_DATA_FOR_GUILD);
        stmt->setUInt32(0, lowguid);

        if (PreparedQueryResult result = CharacterDatabase.Query(stmt))
        {
            Field* fields = result->Fetch();
            name = fields[0].GetString();
            member->SetStats(
                name,
                fields[1].GetUInt8(),
                fields[2].GetUInt8(),
                fields[3].GetUInt8(),
                fields[4].GetUInt16(),
                fields[5].GetUInt32());

            ok = member->CheckStats();
        }

        if (!ok)
        {
            delete member;
            return false;
        }

        m_members[lowguid] = member;
        sCharacterCache->UpdateCharacterGuildId(guid, GetId());
    }

    SQLTransaction trans(nullptr);
    member->SaveToDB(trans);

    _UpdateAccountsNumber();
    _LogEvent(GUILD_EVENT_LOG_JOIN_GUILD, lowguid);
    _SendPlayerJoinGuild(guid, name.c_str());
    sGuildFinderMgr->RemoveAllMembershipRequestsFromPlayer(guid);

    // Call scripts if member was succesfully added (and stored to database)
    sScriptMgr->OnGuildAddMember(this, player, rankId);

    return true;
}

void Guild::DeleteMember(ObjectGuid guid, bool isDisbanding, bool isKicked, bool canDeleteGuild)
{
    uint32 lowguid = guid.GetCounter();

    // Guild master can be deleted when loading guild and guid doesn't exist in characters table
    // or when he is removed from guild by gm command
    if (m_leaderGuid == guid && !isDisbanding)
    {
        Member* oldLeader = nullptr;
        Member* newLeader = nullptr;
        for (Guild::Members::iterator i = m_members.begin(); i != m_members.end(); ++i)
        {
            if (i->first == lowguid)
                oldLeader = i->second;
            else if (!newLeader || newLeader->GetRankId() > i->second->GetRankId())
                newLeader = i->second;
        }

        if (!newLeader)
        {
            Disband();
            if (canDeleteGuild)
                delete this;

            return;
        }

        _SetLeaderGUID(newLeader);

        // If player not online data in data field will be loaded from guild tabs no need to update it !!
        if (Player* newLeaderPlayer = newLeader->FindPlayer())
            newLeaderPlayer->SetRank(GR_GUILDMASTER);

        // If leader does not exist (at guild loading with deleted leader) do not send broadcasts
        if (oldLeader)
        {
            _SendSetNewGuildMaster(oldLeader, newLeader, false);
            _SendRemovePlayerFromGuild(oldLeader->GetGUID(), oldLeader->GetName());
        }
    }

    // Call script on remove before member is actually removed from guild (and database)
    sScriptMgr->OnGuildRemoveMember(this, guid, isDisbanding, isKicked);

    if (Member* member = GetMember(guid))
    {
        membersChanged.erase(member);
        delete member;
    }

    m_members.erase(lowguid);

    // If player not online data in data field will be loaded from guild tabs no need to update it !!
    Player* player = ObjectAccessor::FindConnectedPlayer(guid);
    if (player)
    {
        player->SetInGuild(0);
        player->SetRank(0);
        player->SetGuildLevel(0);
        player->SetReputation(GUILD_REP, 0);

        for (GuildPerkSpellsEntry const* entry : sGuildPerkSpellsStore)
            if (entry->GuildLevel <= GetLevel())
                player->RemoveSpell(entry->SpellID, false, false);
    }
    else
        sCharacterCache->UpdateCharacterGuildId(guid, 0);

    _DeleteMemberFromDB(lowguid);
    if (!isDisbanding)
        _UpdateAccountsNumber();
}

bool Guild::ChangeMemberRank(ObjectGuid guid, uint8 newRank)
{
    if (newRank <= _GetLowestRankId())                    // Validate rank (allow only existing ranks)
        if (Member* member = GetMember(guid))
        {
            member->ChangeRank(newRank);
            return true;
        }

    return false;
}

bool Guild::IsMember(ObjectGuid guid) const
{
    Members::const_iterator itr = m_members.find(guid.GetCounter());
    return itr != m_members.end();
}

// Bank (items move)
void Guild::SwapItems(Player* player, uint8 tabId, uint8 slotId, uint8 destTabId, uint8 destSlotId, uint32 splitedAmount)
{
    if (tabId >= _GetPurchasedTabsSize() || slotId >= GUILD_BANK_MAX_SLOTS ||
        destTabId >= _GetPurchasedTabsSize() || destSlotId >= GUILD_BANK_MAX_SLOTS)
        return;

    if (tabId == destTabId && slotId == destSlotId)
        return;

    BankMoveItemData from(this, player, tabId, slotId);
    BankMoveItemData to(this, player, destTabId, destSlotId);
    _MoveItems(&from, &to, splitedAmount);
}

void Guild::SwapItemsWithInventory(Player* player, bool toChar, uint8 tabId, uint8 slotId, uint8 playerBag, uint8 playerSlotId, uint32 splitedAmount)
{
    if ((slotId >= GUILD_BANK_MAX_SLOTS && slotId != NULL_SLOT) || tabId >= _GetPurchasedTabsSize())
        return;

    BankMoveItemData bankData(this, player, tabId, slotId);
    PlayerMoveItemData charData(this, player, playerBag, playerSlotId);
    if (toChar)
        _MoveItems(&bankData, &charData, splitedAmount);
    else
        _MoveItems(&charData, &bankData, splitedAmount);
}

// Bank tabs
void Guild::SetBankTabText(uint8 tabId, std::string const& text)
{
    if (BankTab* pTab = GetBankTab(tabId))
    {
        pTab->SetText(text);
        pTab->SendText(this, nullptr);
    }
}

// Private methods
void Guild::_CreateLogHolders()
{
    m_eventLog = new LogHolder(sWorld->getIntConfig(CONFIG_GUILD_EVENT_LOG_COUNT));
    m_newsLog = new LogHolder(sWorld->getIntConfig(CONFIG_GUILD_NEWS_LOG_COUNT));

    for (uint8 tabId = 0; tabId <= GUILD_BANK_MAX_TABS; ++tabId)
    {
        LogHolder* holder = new LogHolder(sWorld->getIntConfig(CONFIG_GUILD_BANK_EVENT_LOG_COUNT));
        m_bankEventLog[tabId] = holder;
    }
}

void Guild::_CreateNewBankTab()
{
    uint8 tabId = _GetPurchasedTabsSize();                      // Next free id
    m_bankTabs.push_back(new BankTab(m_id, tabId));

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_TAB);
    stmt->setUInt32(0, m_id);
    stmt->setUInt8 (1, tabId);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GUILD_BANK_TAB);
    stmt->setUInt32(0, m_id);
    stmt->setUInt8 (1, tabId);
    trans->Append(stmt);

    ++tabId;
    for (Guild::RankInfo& rankInfo : m_ranks)
        rankInfo.CreateMissingTabsIfNeeded(tabId, trans, false);

    CharacterDatabase.CommitTransaction(trans);
}

void Guild::_CreateDefaultGuildRanks(LocaleConstant loc)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_RANKS);
    stmt->setUInt32(0, m_id);
    CharacterDatabase.Execute(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GUILD_BANK_RIGHTS);
    stmt->setUInt32(0, m_id);
    CharacterDatabase.Execute(stmt);

    _CreateRank(sObjectMgr->GetTrinityString(LANG_GUILD_MASTER,   loc), GR_RIGHT_ALL);
    _CreateRank(sObjectMgr->GetTrinityString(LANG_GUILD_OFFICER,  loc), GR_RIGHT_ALL);
    _CreateRank(sObjectMgr->GetTrinityString(LANG_GUILD_VETERAN,  loc), GR_RIGHT_GCHATLISTEN | GR_RIGHT_GCHATSPEAK);
    _CreateRank(sObjectMgr->GetTrinityString(LANG_GUILD_MEMBER,   loc), GR_RIGHT_GCHATLISTEN | GR_RIGHT_GCHATSPEAK);
    _CreateRank(sObjectMgr->GetTrinityString(LANG_GUILD_INITIATE, loc), GR_RIGHT_GCHATLISTEN | GR_RIGHT_GCHATSPEAK);
}

bool Guild::_CreateRank(std::string const& name, uint32 rights)
{
    uint8 newRankId = _GetRanksSize();
    if (newRankId >= GUILD_RANKS_MAX_COUNT)
        return false;

    // Ranks represent sequence 0, 1, 2, ... where 0 means guildmaster
    RankInfo info(m_id, newRankId, name, rights, 0);
    m_ranks.push_back(info);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    info.CreateMissingTabsIfNeeded(_GetPurchasedTabsSize(), trans);
    info.SaveToDB(trans);
    CharacterDatabase.CommitTransaction(trans);

    return true;
}

// Updates the number of accounts that are in the guild
// Player may have many characters in the guild, but with the same account
void Guild::_UpdateAccountsNumber()
{
    // We use a set to be sure each element will be unique
    std::set<uint32> accountsIdSet;
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        accountsIdSet.insert(itr->second->GetAccountId());

    m_accountsNumber = accountsIdSet.size();
}

// Detects if player is the guild master.
// Check both leader guid and player's rank (otherwise multiple feature with
// multiple guild masters won't work)
bool Guild::_IsLeader(Player* player) const
{
    if (player->GetGUID() == m_leaderGuid)
        return true;

    if (const Member* member = GetMember(player->GetGUID()))
        return member->IsRank(GR_GUILDMASTER);

    return false;
}

void Guild::_DeleteBankItems(SQLTransaction& trans, bool removeItemsFromDB)
{
    for (uint8 tabId = 0; tabId < _GetPurchasedTabsSize(); ++tabId)
    {
        m_bankTabs[tabId]->Delete(trans, removeItemsFromDB);
        delete m_bankTabs[tabId];
        m_bankTabs[tabId] = nullptr;
    }

    m_bankTabs.clear();
}

bool Guild::_ModifyBankMoney(SQLTransaction& trans, uint64 amount, bool add)
{
    if (add)
        m_bankMoney += amount;
    else
    {
        // Check if there is enough money in bank.
        if (m_bankMoney < amount)
            return false;
        m_bankMoney -= amount;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_BANK_MONEY);
    stmt->setUInt64(0, m_bankMoney);
    stmt->setUInt32(1, m_id);
    trans->Append(stmt);

    return true;
}

void Guild::_SetLeaderGUID(Member* pLeader)
{
    if (!pLeader)
        return;

    m_leaderGuid = pLeader->GetGUID();
    pLeader->ChangeRank(GR_GUILDMASTER);

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_LEADER);
    stmt->setUInt32(0, m_leaderGuid.GetCounter());
    stmt->setUInt32(1, m_id);
    CharacterDatabase.Execute(stmt);
}

void Guild::_SetRankBankMoneyPerDay(uint8 rankId, uint32 moneyPerDay)
{
    if (RankInfo* rankInfo = GetRankInfo(rankId))
        rankInfo->SetBankMoneyPerDay(moneyPerDay);
}

void Guild::_SetRankBankTabRightsAndSlots(uint8 rankId, GuildBankRightsAndSlots rightsAndSlots, bool saveToDB)
{
    if (rightsAndSlots.GetTabId() >= _GetPurchasedTabsSize())
        return;

    if (RankInfo* rankInfo = GetRankInfo(rankId))
        rankInfo->SetBankTabSlotsAndRights(rightsAndSlots, saveToDB);
}

inline std::string Guild::_GetRankName(uint8 rankId) const
{
    if (const RankInfo* rankInfo = GetRankInfo(rankId))
        return rankInfo->GetName();

    return "<unknown>";
}

inline uint32 Guild::_GetRankRights(uint8 rankId) const
{
    if (const RankInfo* rankInfo = GetRankInfo(rankId))
        return rankInfo->GetRights();

    return 0;
}

inline uint32 Guild::_GetRankBankMoneyPerDay(uint8 rankId) const
{
    if (const RankInfo* rankInfo = GetRankInfo(rankId))
        return rankInfo->GetBankMoneyPerDay();

    return 0;
}

inline int32 Guild::_GetRankBankTabSlotsPerDay(uint8 rankId, uint8 tabId) const
{
    if (tabId < _GetPurchasedTabsSize())
        if (const RankInfo* rankInfo = GetRankInfo(rankId))
            return rankInfo->GetBankTabSlotsPerDay(tabId);

    return 0;
}

inline int8 Guild::_GetRankBankTabRights(uint8 rankId, uint8 tabId) const
{
    if (const RankInfo* rankInfo = GetRankInfo(rankId))
        return rankInfo->GetBankTabRights(tabId);

    return 0;
}

inline int32 Guild::_GetMemberRemainingSlots(Member const* member, uint8 tabId) const
{
    if (member)
    {
        uint8 rankId = member->GetRankId();
        if (rankId == GR_GUILDMASTER)
            return static_cast<int32>(GUILD_WITHDRAW_SLOT_UNLIMITED);

        if ((_GetRankBankTabRights(rankId, tabId) & GUILD_BANK_RIGHT_VIEW_TAB) != 0)
        {
            int32 remaining = _GetRankBankTabSlotsPerDay(rankId, tabId) - member->GetBankTabWithdrawValue(tabId);
            if (remaining > 0)
                return remaining;
        }
    }

    return 0;
}

inline int64 Guild::_GetMemberRemainingMoney(Member const* member) const
{
    if (member)
    {
        uint8 rankId = member->GetRankId();
        if (rankId == GR_GUILDMASTER)
            return std::numeric_limits<int64>::max();

        if ((_GetRankRights(rankId) & (GR_RIGHT_WITHDRAW_REPAIR | GR_RIGHT_WITHDRAW_GOLD)) != 0)
        {
            int64 remaining = (int64(_GetRankBankMoneyPerDay(rankId)) * GOLD) - member->GetBankMoneyWithdrawValue();
            if (remaining > 0)
                return remaining;
        }
    }

    return 0;
}

inline void Guild::_UpdateMemberWithdrawSlots(SQLTransaction& trans, ObjectGuid guid, uint8 tabId)
{
    if (Member* member = GetMember(guid))
        member->UpdateBankTabWithdrawValue(trans, tabId, 1);
}

inline bool Guild::_MemberHasTabRights(ObjectGuid guid, uint8 tabId, uint32 rights) const
{
    if (const Member* member = GetMember(guid))
    {
        // Leader always has full rights
        if (member->IsRank(GR_GUILDMASTER) || m_leaderGuid == guid)
            return true;

        return (_GetRankBankTabRights(member->GetRankId(), tabId) & rights) == rights;
    }

    return false;
}

// Add new event log record
inline void Guild::_LogEvent(GuildEventLogTypes eventType, uint32 playerGuid1, uint32 playerGuid2, uint8 newRank)
{
    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    m_eventLog->AddEvent(trans, new EventLogEntry(m_id, m_eventLog->GetNextGUID(), eventType, playerGuid1, playerGuid2, newRank));
    CharacterDatabase.CommitTransaction(trans);

    sScriptMgr->OnGuildEvent(this, uint8(eventType), playerGuid1, playerGuid2, newRank);
}

// Add new bank event log record
void Guild::_LogBankEvent(SQLTransaction& trans, GuildBankEventLogTypes eventType, uint8 tabId, uint32 lowguid, uint32 itemOrMoney, uint16 itemStackCount, uint8 destTabId)
{
    if (tabId > GUILD_BANK_MAX_TABS)
        return;

    // not logging moves within the same tab
    if (eventType == GUILD_BANK_LOG_MOVE_ITEM && tabId == destTabId)
        return;

    uint8 dbTabId = tabId;
    if (BankEventLogEntry::IsMoneyEvent(eventType))
    {
        tabId = GUILD_BANK_MAX_TABS;
        dbTabId = GUILD_BANK_MONEY_LOGS_TAB;
    }

    LogHolder* pLog = m_bankEventLog[tabId];
    pLog->AddEvent(trans, new BankEventLogEntry(m_id, pLog->GetNextGUID(), eventType, dbTabId, lowguid, itemOrMoney, itemStackCount, destTabId));

    sScriptMgr->OnGuildBankEvent(this, uint8(eventType), tabId, lowguid, itemOrMoney, itemStackCount, destTabId);
}

inline Item* Guild::_GetItem(uint8 tabId, uint8 slotId) const
{
    if (const BankTab* tab = GetBankTab(tabId))
        return tab->GetItem(slotId);

    return nullptr;
}

inline void Guild::_RemoveItem(SQLTransaction& trans, uint8 tabId, uint8 slotId)
{
    if (BankTab* pTab = GetBankTab(tabId))
        pTab->SetItem(trans, slotId, nullptr);
}

void Guild::_MoveItems(MoveItemData* pSrc, MoveItemData* pDest, uint32 splitedAmount)
{
    // 1. Initialize source item
    if (!pSrc->InitItem())
        return; // No source item

    // 2. Check source item
    if (!pSrc->CheckItem(splitedAmount))
        return; // Source item or splited amount is invalid
    /*
    if (pItemSrc->GetCount() == 0)
    {
        TC_LOG_FATAL("guild", "Guild::SwapItems: Player %s(GUIDLow: %u) tried to move item %u from tab %u slot %u to tab %u slot %u, but item %u has a stack of zero!",
            player->GetName(), player->GetGUID().GetCounter(), pItemSrc->GetEntry(), tabId, slotId, destTabId, destSlotId, pItemSrc->GetEntry());
        //return; // Commented out for now, uncomment when it's verified that this causes a crash!!
    }
    // */

    // 3. Check destination rights
    if (!pDest->HasStoreRights(pSrc))
        return; // Player has no rights to store item in destination

    // 4. Check source withdraw rights
    if (!pSrc->HasWithdrawRights(pDest))
        return; // Player has no rights to withdraw items from source

    // 5. Check split
    if (splitedAmount)
    {
        // 5.1. Clone source item
        if (!pSrc->CloneItem(splitedAmount))
            return; // Item could not be cloned

        // 5.2. Move splited item to destination
        _DoItemsMove(pSrc, pDest, true, splitedAmount);
    }
    else // 6. No split
    {
        // 6.1. Try to merge items in destination (pDest->GetItem() == NULL)
        if (!_DoItemsMove(pSrc, pDest, false)) // Item could not be merged
        {
            // 6.2. Try to swap items
            // 6.2.1. Initialize destination item
            if (!pDest->InitItem())
                return;

            // 6.2.2. Check rights to store item in source (opposite direction)
            if (!pSrc->HasStoreRights(pDest))
                return; // Player has no rights to store item in source (opposite direction)

            if (!pDest->HasWithdrawRights(pSrc))
                return; // Player has no rights to withdraw item from destination (opposite direction)

            // 6.2.3. Swap items (pDest->GetItem() != NULL)
            _DoItemsMove(pSrc, pDest, true);
        }
    }

    // 7. Send changes
    _SendBankContentUpdate(pSrc, pDest);
}

bool Guild::_DoItemsMove(MoveItemData* pSrc, MoveItemData* pDest, bool sendError, uint32 splitedAmount)
{
    Item* pDestItem = pDest->GetItem();
    bool swap = (pDestItem != nullptr);

    Item* pSrcItem = pSrc->GetItem(splitedAmount);
    // 1. Can store source item in destination
    if (!pDest->CanStore(pSrcItem, swap, sendError))
        return false;

    // 2. Can store destination item in source
    if (swap)
        if (!pSrc->CanStore(pDestItem, true, true))
            return false;

    // GM LOG (@todo move to scripts)
    pDest->LogAction(pSrc);
    if (swap)
        pSrc->LogAction(pDest);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    // 3. Log bank events
    pDest->LogBankEvent(trans, pSrc, pSrcItem->GetCount());
    if (swap)
        pSrc->LogBankEvent(trans, pDest, pDestItem->GetCount());

    // 4. Remove item from source
    pSrc->RemoveItem(trans, pDest, splitedAmount);

    // 5. Remove item from destination
    if (swap)
        pDest->RemoveItem(trans, pSrc);

    // 6. Store item in destination
    pDest->StoreItem(trans, pSrcItem);

    // 7. Store item in source
    if (swap)
        pSrc->StoreItem(trans, pDestItem);

    CharacterDatabase.CommitTransaction(trans);

    return true;
}

void Guild::_SendBankContentUpdate(MoveItemData* pSrc, MoveItemData* pDest) const
{
    ASSERT(pSrc->IsBank() || pDest->IsBank());

    uint8 tabId = 0;
    SlotIds slots;
    if (pSrc->IsBank()) // B ->
    {
        tabId = pSrc->GetContainer();
        slots.insert(pSrc->GetSlotId());
        if (pDest->IsBank()) // B -> B
        {
            // Same tab - add destination slots to collection
            if (pDest->GetContainer() == pSrc->GetContainer())
                pDest->CopySlots(slots);
            else // Different tabs - send second message
            {
                SlotIds destSlots;
                pDest->CopySlots(destSlots);
                _SendBankContentUpdate(pDest->GetContainer(), destSlots);
            }
        }
    }
    else if (pDest->IsBank()) // C -> B
    {
        tabId = pDest->GetContainer();
        pDest->CopySlots(slots);
    }

    _SendBankContentUpdate(tabId, slots);

    WorldPacket data(SMSG_GUILD_EVENT_BANK_CONTENTS_CHANGED, 0);
    BroadcastPacket(&data);
}

void Guild::_SendBankContentUpdate(uint8 tabId, SlotIds slots) const
{
    if (BankTab const* tab = GetBankTab(tabId))
    {
        bool FullUpdate = false;
        uint32 TabSize = 0;

        std::unordered_map<uint8, Item*> items;
        std::unordered_map<uint8, std::vector<std::pair<uint32, uint32>>> itemEnchants;
        std::unordered_map<uint8, std::vector<uint32>> itemDynamics;

        uint32 enchantSize = 0;
        uint32 dynamicSize = 0;
        for (uint32 slotId : slots)
        {
            Item* tabItem = tab->GetItem(slotId);
            items[slotId] = tabItem;

            // Enchants
            if (tabItem)
            {
                for (uint32 ench = TEMP_ENCHANTMENT_SLOT; ench < MAX_ENCHANTMENT_SLOT; ++ench)
                    if (uint32 enchantId = tabItem->GetEnchantmentId(EnchantmentSlot(ench)))
                        itemEnchants[slotId].push_back(std::make_pair(ench, enchantId));

                enchantSize += itemEnchants[slotId].size();
            }

            // Dynamic modifiers
            uint32 mask = tabItem ? tabItem->GetUInt32Value(ITEM_MODIFIERS_MASK) : 0;

            itemDynamics[slotId].push_back(mask);

            if (mask)
                for (uint8 i = 0; mask != 0; mask >>= 1, ++i)
                    if ((mask & 1) != 0)
                        itemDynamics[slotId].push_back(tabItem->GetModifier(ItemModifier(i)));

            dynamicSize += itemDynamics[slotId].size();
        }

        ByteBuffer itemData;

        WorldPacket data(SMSG_GUILD_BANK_LIST, 4 + 4 + 8 + 5 + items.size() * (3 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4) + enchantSize * (4 + 4) + dynamicSize * (4));

        data << uint32(tabId);

        size_t pos = data.wpos();
        data << uint32(0);                  // item withdrawal limit placeholder (sent later)

        data << uint64(m_bankMoney);

        data.WriteBit(FullUpdate);

        data.WriteBits(TabSize, 21);
        data.WriteBits(items.size(), 18);

        for (auto & itr : items)
        {
            uint32 slotID = itr.first;
            Item* tabItem = itr.second;

            bool IsLocked = false;

            data.WriteBit(IsLocked);

            data.WriteBits(itemEnchants[slotID].size(), 21);

            itemData << uint32(0);
            itemData << uint32(0);

            for (auto & itr : itemEnchants[slotID])
            {
                itemData << uint32(itr.first);
                itemData << uint32(itr.second);
            }

            itemData << uint32(tabItem ? tabItem->GetEnchantmentId(EnchantmentSlot(PERM_ENCHANTMENT_SLOT)) : 0);

            /* START OF DYNAMIC FIELDS PART */

            size_t dynamicSize = itemDynamics[slotID].size();

            // all dynamic modifiers sent as uint32
            itemData << uint32(dynamicSize * sizeof(uint32));

            // handle all dynamic modifiers
            for (uint32 modifier : itemDynamics[slotID])
                itemData << uint32(modifier);

            /* END OF DYNAMIC FIELDS PART */

            itemData << uint32(tabItem ? tabItem->GetEntry() : 0);
            itemData << uint32(tabItem ? abs(tabItem->GetSpellCharges()) : 0);
            itemData << uint32(tabItem ? tabItem->GetCount() : 0);
            itemData << uint32(slotID);
            itemData << int32(tabItem ? tabItem->GetItemRandomPropertyId() : 0);
            itemData << uint32(tabItem ? tabItem->GetItemSuffixFactor() : 0);
        }

        data.append(itemData);

        for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
            if (_MemberHasTabRights(itr->second->GetGUID(), tabId, GUILD_BANK_RIGHT_VIEW_TAB))
                if (Player* player = itr->second->FindPlayer())
                {
                    data.put<uint32>(pos, uint32(_GetMemberRemainingSlots(itr->second, tabId)));
                    player->SendDirectMessage(&data);
                }
    }
}

void Guild::SendBankList(WorldSession* session, uint8 tabId, bool fullUpdate) const
{
    Member const* member = GetMember(session->GetPlayer()->GetGUID());
    if (!member) // Shouldn't happen, just in case
        return;

    std::unordered_map<uint8, Item*> items;
    std::unordered_map<uint8, std::vector<std::pair<uint32, uint32>>> itemEnchants;
    std::unordered_map<uint8, std::vector<uint32>> itemDynamics;

    uint32 enchantSize = 0;
    uint32 dynamicSize = 0;
    if (fullUpdate && _MemberHasTabRights(session->GetPlayer()->GetGUID(), tabId, GUILD_BANK_RIGHT_VIEW_TAB))
        if (BankTab const* tab = GetBankTab(tabId))
            for (uint8 slotId = 0; slotId < GUILD_BANK_MAX_SLOTS; ++slotId)
                if (Item* tabItem = tab->GetItem(slotId))
                    if (ItemTemplate const* itemTemplate = tabItem->GetTemplate())
                    {
                        items[slotId] = tabItem;

                        // Enchants
                        for (uint32 ench = TEMP_ENCHANTMENT_SLOT; ench < MAX_ENCHANTMENT_SLOT; ++ench)
                            if (uint32 enchantId = tabItem->GetEnchantmentId(EnchantmentSlot(ench)))
                                itemEnchants[slotId].push_back(std::make_pair(ench, enchantId));

                        enchantSize += itemEnchants[slotId].size();

                        // Dynamic modifiers
                        uint32 mask = tabItem->GetUInt32Value(ITEM_MODIFIERS_MASK);

                        itemDynamics[slotId].push_back(mask);

                        if (mask)
                            for (uint8 i = 0; mask != 0; mask >>= 1, ++i)
                                if ((mask & 1) != 0)
                                    itemDynamics[slotId].push_back(tabItem->GetModifier(ItemModifier(i)));

                        dynamicSize += itemDynamics[slotId].size();
                    }

    std::vector<BankTab*> tabs;

    uint32 tabIconSize = 0;
    uint32 tabNameSize = 0;
    if (fullUpdate)
        for (uint8 i = 0; i < _GetPurchasedTabsSize(); ++i)
        {
            tabs.push_back(m_bankTabs[i]);

            tabIconSize += m_bankTabs[i]->GetIcon().length();
            tabNameSize += m_bankTabs[i]->GetName().length();
        }

    ByteBuffer itemData;
    ByteBuffer tabData;

    WorldPacket data(SMSG_GUILD_BANK_LIST, 4 + 4 + 8 + 5 + tabs.size() * (2 + 4) + tabIconSize + tabNameSize +
                    items.size() * (3 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4 + 4) + enchantSize * (4 + 4) + dynamicSize * (4));

    data << uint32(tabId);
    data << uint32(_GetMemberRemainingSlots(member, tabId));
    data << uint64(m_bankMoney);

    data.WriteBit(fullUpdate);

    data.WriteBits(tabs.size(), 21);
    data.WriteBits(items.size(), 18);

    for (uint8 i = 0; i < tabs.size(); ++i)
    {
        BankTab* tab = tabs[i];

        data.WriteBits(tab->GetIcon().length(), 9);
        data.WriteBits(tab->GetName().length(), 7);

        tabData << uint32(i);

        tabData.WriteString(tab->GetIcon());
        tabData.WriteString(tab->GetName());
    }

    for (auto & itr : items)
    {
        uint32 slotID = itr.first;
        Item* tabItem  = itr.second;

        bool IsLocked = false;

        data.WriteBit(IsLocked);

        data.WriteBits(itemEnchants[slotID].size(), 21);

        itemData << uint32(0);
        itemData << uint32(0);

        for (auto & itr : itemEnchants[slotID])
        {
            itemData << uint32(itr.first);
            itemData << uint32(itr.second);
        }

        itemData << uint32(tabItem->GetEnchantmentId(EnchantmentSlot(PERM_ENCHANTMENT_SLOT)));

        /* START OF DYNAMIC FIELDS PART */

        size_t dynamicSize = itemDynamics[slotID].size();

        // all dynamic modifiers sent as uint32
        itemData << uint32(dynamicSize * sizeof(uint32));

        // handle all dynamic modifiers
        for (uint32 modifier : itemDynamics[slotID])
            itemData << uint32(modifier);

        /* END OF DYNAMIC FIELDS PART */

        itemData << uint32(tabItem->GetEntry());
        itemData << uint32(abs(tabItem->GetSpellCharges()));
        itemData << uint32(tabItem->GetCount());
        itemData << uint32(slotID);
        itemData << int32(tabItem->GetItemRandomPropertyId());
        itemData << uint32(tabItem->GetItemSuffixFactor());
    }

    data.append(itemData);
    data.append(tabData);

    session->SendPacket(&data);
}

void Guild::_SendGuildRanksUpdate(ObjectGuid setterGuid, ObjectGuid targetGuid, uint32 rank)
{
    Member* member = GetMember(targetGuid);
    ASSERT(member);

    WorldPacket data(SMSG_GUILD_RANKS_UPDATE, 2 * (1 + 8) + 1 + 4);

    data.WriteGuidMask(targetGuid, 5, 6);

    data.WriteGuidMask(setterGuid, 0, 1);

    data.WriteGuidMask(targetGuid, 3);

    data.WriteGuidMask(setterGuid, 4);

    data.WriteGuidMask(targetGuid, 2);

    data.WriteGuidMask(setterGuid, 6, 3, 7);

    data.WriteGuidMask(targetGuid, 4, 0, 1);

    data.WriteGuidMask(setterGuid, 2);

    data.WriteGuidMask(targetGuid, 7);

    data.WriteBit(rank < member->GetRankId()); // 1 == higher, 0 = lower

    data.WriteGuidMask(setterGuid, 5);

    data.WriteGuidBytes(targetGuid, 2);

    data.WriteGuidBytes(setterGuid, 1);

    data.WriteGuidBytes(targetGuid, 6, 1, 4);

    data.WriteGuidBytes(setterGuid, 0);

    data << uint32(rank);

    data.WriteGuidBytes(setterGuid, 3, 7);

    data.WriteGuidBytes(targetGuid, 7);

    data.WriteGuidBytes(setterGuid, 2);

    data.WriteGuidBytes(targetGuid, 3, 4);

    data.WriteGuidBytes(setterGuid, 6, 5);

    data.WriteGuidBytes(targetGuid, 0);

    data.WriteGuidBytes(setterGuid, 4);

    BroadcastPacket(&data);

    member->ChangeRank(rank);
}

void Guild::GiveXP(uint64 xp, Player* source)
{
    if (!sWorld->getBoolConfig(CONFIG_GUILD_LEVELING_ENABLED))
        return;

    if (GetLevel() >= sWorld->getIntConfig(CONFIG_GUILD_MAX_LEVEL))
        xp = 0; // SMSG_GUILD_XP_GAIN is always sent, even for no gains

    WorldPacket data(SMSG_GUILD_XP_GAIN, 8);

    data << uint64(xp);

    source->SendDirectMessage(&data);

    _experience += xp;
    _todayExperience += xp;

    if (Member* member = GetMember(source->GetGUID()))
    {
        member->IncreaseActivity(xp);
        membersChanged.insert(member);

        // Reputation Gain for Player
        // @todo: different rate depends on xp source
        // source: http://wow.gamepedia.com/Guild_reputation
        if (source->GetReputationRank(GUILD_REP) != REP_EXALTED)
        {
            uint32 repGain = ceil(xp / 360.0f);

            // apply guil rep multiplier from aura (not stacking-get highest)
            AddPct(repGain, source->GetMaxPositiveAuraModifier(SPELL_AURA_MOD_GUILD_REPUTATION_GAIN_PCT));

            repGain = std::min(repGain, sWorld->getIntConfig(CONFIG_GUILD_WEEKLY_REP_CAP) - member->GetWeekReputation());
            if (source->GetReputation(GUILD_REP) + repGain >= uint32(ReputationMgr::Reputation_Cap))
                repGain = ReputationMgr::Reputation_Cap - source->GetReputation(GUILD_REP);

            if (repGain)
                member->IncreaseReputations(source, repGain);
        }
    }

    if (!xp)
        return;

    uint32 oldLevel = GetLevel();

    // Ding, mon!
    while (GetExperience() >= sGuildMgr->GetXPForGuildLevel(GetLevel()) && GetLevel() < sWorld->getIntConfig(CONFIG_GUILD_MAX_LEVEL))
    {
        _experience -= sGuildMgr->GetXPForGuildLevel(GetLevel());
        ++_level;

        // Find all guild perks to learn
        std::vector<uint32> perksToLearn;
        for (GuildPerkSpellsEntry const* entry : sGuildPerkSpellsStore)
            if (entry->GuildLevel > oldLevel && entry->GuildLevel <= GetLevel())
                perksToLearn.push_back(entry->SpellID);

        // Notify all online players that guild level changed and learn perks
        for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
        {
            if (Player* player = itr->second->FindPlayer())
            {
                player->SetGuildLevel(GetLevel());
                for (size_t i = 0; i < perksToLearn.size(); ++i)
                    player->LearnSpell(perksToLearn[i], true);
            }
        }

        AddGuildNews(GUILD_NEWS_LEVEL_UP, ObjectGuid::Empty, 0, _level);
        UpdateCriteria(CRITERIA_TYPE_REACH_GUILD_LEVEL, source, GetLevel());

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_LEVEL);
        stmt->setUInt32(0, GetLevel());
        stmt->setUInt32(1, GetId());
        CharacterDatabase.Execute(stmt);

        SaveExperiences();

        ++oldLevel;
    }
}

void Guild::SendGuildXP(WorldSession* session /* = NULL */) const
{
    Member const* member = GetMember(session->GetPlayer()->GetGUID());

    WorldPacket data(SMSG_GUILD_XP, 8 + 8 + 8 + 8);

    data << uint64(GetExperience());
    data << uint64(member ? member->GetWeekActivity() : 0);
    data << uint64(member ? member->GetTotalActivity() : 0);
    data << uint64(sGuildMgr->GetXPForGuildLevel(GetLevel()) - GetExperience());    // XP missing for next level

    session->SendPacket(&data);
}

void Guild::Member::SendGuildReputationWeeklyCap()
{
    Player* player = FindPlayer();
    if (!player)
        return;

    uint32 cap = sWorld->getIntConfig(CONFIG_GUILD_WEEKLY_REP_CAP) - GetWeekReputation();

    WorldPacket data(SMSG_GUILD_REPUTATION_WEEKLY_CAP, 4);

    data << uint32(cap);

    player->GetSession()->SendPacket(&data);
}

void Guild::ResetTimes(bool weekly)
{
    _todayExperience = 0;
    for (Members::const_iterator itr = m_members.begin(); itr != m_members.end(); ++itr)
    {
        itr->second->ResetValues(weekly);
        if (Player* player = itr->second->FindPlayer())
        {
            //SendGuildXP(player->GetSession());
            WorldPacket data(SMSG_GUILD_MEMBER_DAILY_RESET, 0);  // tells the client to request bank withdrawal limit
            player->SendDirectMessage(&data);
        }
    }
}

void Guild::AddGuildNews(uint8 type, ObjectGuid guid, uint32 flags, uint32 value)
{
    uint32 lowGuid = guid.GetCounter();
    NewsLogEntry* news = new NewsLogEntry(m_id, m_newsLog->GetNextGUID(), GuildNews(type), lowGuid, flags, value);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    m_newsLog->AddEvent(trans, news);
    CharacterDatabase.CommitTransaction(trans);

    WorldPacket data(SMSG_GUILD_NEWS_UPDATE, 3 + 1 + 8 + 3 + 4 + 4 + 4 + 4 + 4 + 4);

    ByteBuffer buffer;

    data.WriteBits(1, 19); // size, we are only sending 1 news here

    news->WritePacket(data, buffer);

    data.append(buffer);

    BroadcastPacket(&data);
}

void Guild::AddGuildNews(uint8 type, ObjectGuid guid, uint32 flags, uint32 value, GuidSet& achievementPlayers)
{
    uint32 lowGuid = guid.GetCounter();

    NewsLogEntry* news = new NewsLogEntry(m_id, m_newsLog->GetNextGUID(), GuildNews(type), lowGuid, flags, value);

    if (!achievementPlayers.empty())
        news->guildAchievementPlayers.insert(achievementPlayers.begin(), achievementPlayers.end());

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    m_newsLog->AddEvent(trans, news);
    CharacterDatabase.CommitTransaction(trans);

    WorldPacket data(SMSG_GUILD_NEWS_UPDATE, 3 + 1 + 8 + 3 + 4 + 4 + 4 + 4 + 4 + 4 + news->guildAchievementPlayers.size() * (1 + 8));

    ByteBuffer buffer;

    data.WriteBits(1, 19); // size, we are only sending 1 news here

    news->WritePacket(data, buffer);

    data.append(buffer);

    BroadcastPacket(&data);
}

bool Guild::HasAchieved(uint32 achievementId) const
{
    return m_achievementMgr.HasAchieved(achievementId);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player)
{
    m_achievementMgr.UpdateCriteria(type, 0, 0, 0, nullptr, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit)
{
    m_achievementMgr.UpdateCriteria(type, 0, 0, 0, unit, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1)
{
    m_achievementMgr.UpdateCriteria(type, miscValue1, 0, 0, unit, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1, uint64 miscValue2)
{
    m_achievementMgr.UpdateCriteria(type, miscValue1, miscValue2, 0, unit, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3)
{
    m_achievementMgr.UpdateCriteria(type, miscValue1, miscValue2, miscValue3, unit, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1)
{
    m_achievementMgr.UpdateCriteria(type, miscValue1, 0, 0, nullptr, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1, uint64 miscValue2)
{
    m_achievementMgr.UpdateCriteria(type, miscValue1, miscValue2, 0, nullptr, player);
}

void Guild::UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3)
{
    m_achievementMgr.UpdateCriteria(type, miscValue1, miscValue2, miscValue3, nullptr, player);
}

void Guild::HandleNewsSetSticky(WorldSession* session, uint32 newsId, bool sticky)
{
    GuildLog* logs = m_newsLog->GetGuildLog();
    GuildLog::iterator itr = logs->begin();
    while (itr != logs->end() && (*itr)->GetGUID() != newsId)
        ++itr;

    if (itr == logs->end())
    {
        TC_LOG_DEBUG("guild", "HandleNewsSetSticky: [%s] requested unknown newsId %u - Sticky: %u",
            session->GetPlayerInfo().c_str(), newsId, sticky);
        return;
    }

    NewsLogEntry* news = (NewsLogEntry*)(*itr);
    news->SetSticky(sticky);

    TC_LOG_DEBUG("guild", "HandleNewsSetSticky: [%s] chenged newsId %u sticky to %u",
        session->GetPlayerInfo().c_str(), newsId, sticky);

    WorldPacket data(SMSG_GUILD_NEWS_UPDATE, 3 + 1 + 8 + 3 + 4 + 4 + 4 + 4 + 4 + 4);

    ByteBuffer buffer;

    data.WriteBits(1, 19);

    news->WritePacket(data, buffer);

    data.append(buffer);

    session->SendPacket(&data);
}

void Guild::HandleSetBankTabNote(WorldSession* session, uint32 tabId, std::string note)
{
    BankTab* bankTab = GetBankTab(tabId);
    if (!bankTab)
    {
        TC_LOG_ERROR("guild", "Guild::HandleSetBankTabNote: Player %s is trying to modify bank tab note for non existing tab %d.",
            session->GetPlayerInfo().c_str(), tabId);
        return;
    }

    if (!_HasRankRight(session->GetPlayer(), GR_RIGHT_MODIFY_BANK_TAB))
    {
        TC_LOG_ERROR("guild", "Guild::HandleSetBankTabNote: Player %s is trying to modify a bank tab note but doesn't have permission!",
            session->GetPlayerInfo().c_str());
        return;
    }

    bankTab->SetText(note);

    WorldPacket data(SMSG_GUILD_EVENT_BANK_TAB_TEXT_CHANGED, 4);

    data << uint32(tabId);

    BroadcastPacket(&data);
}

void Guild::UpdateMemberAchievementPoints(Player* player, uint32 achievementPoints)
{
    if (Member* member = GetMember(player->GetGUID()))
        member->SetAchievementPoints(achievementPoints);
}

void Guild::_SendPlayerJoinGuild(ObjectGuid playerGuid, std::string playerName)
{
    WorldPacket data(SMSG_GUILD_JOIN, 1 + 8 + 1 + 4 + playerName.length());

    data.WriteGuidMask(playerGuid, 6, 1, 3);

    data.WriteBits(playerName.length(), 6);

    data.WriteGuidMask(playerGuid, 7, 4, 2, 5, 0);

    data.FlushBits();

    data.WriteGuidBytes(playerGuid, 2, 4, 1, 6, 5);

    data << uint32(realmID);

    data.WriteGuidBytes(playerGuid, 3, 0);

    data.WriteString(playerName);

    data.WriteGuidBytes(playerGuid, 7);

    BroadcastPacket(&data);
}

void Guild::_SendRemovePlayerFromGuild(ObjectGuid removedGuid, std::string removedName, ObjectGuid kickerGuid /*= 0*/, std::string kickerName /*= ""*/)
{
    bool kicked = !kickerGuid.IsEmpty();

    WorldPacket data(SMSG_GUILD_LEAVE, 1 + 8 + 1 + removedName.length() + 4 + (kicked ? (1 + 8 + 2 + kickerName.length() + 4) : 0));

    data.WriteGuidMask(removedGuid, 2);

    data.WriteBits(removedName.length(), 6);

    data.WriteGuidMask(removedGuid, 6, 5);

    data.WriteBit(kicked);

    if (kicked)
    {
        data.WriteBit(0); // unk bool
        data.WriteBit(0); // unk bool

        data.WriteBits(kickerName.length(), 6);

        data.WriteGuidMask(kickerGuid, 1, 3, 4, 2, 5, 7, 6, 0);

        data.WriteBit(0);
    }

    data.WriteGuidMask(removedGuid, 1, 0, 3, 4, 7);

    data.FlushBits();

    if (kicked)
    {
        data.WriteGuidBytes(kickerGuid, 1, 3, 5, 2, 0, 4, 6, 7);

        data.WriteString(kickerName);

        data << uint32(realmID);
    }

    data.WriteString(removedName);

    data.WriteGuidBytes(removedGuid, 1);

    data << uint32(realmID);

    data.WriteGuidBytes(removedGuid, 0, 4, 2, 3, 6, 5, 7);

    BroadcastPacket(&data);
}

void Guild::FillRecipeBitset(std::vector<uint8>& result, std::unordered_set<uint32> recipes)
{
    boost::dynamic_bitset<uint8> bits(GUILD_RECIPES_BITS);

    for (uint32 spellId : recipes)
    {
        SkillLineAbilityEntry const* skillInfo = sDBCManager->GetSkillLineAbilityBySpell(spellId);
        if (!skillInfo)
            continue;

        if (skillInfo->UniqueBit)
            bits.set(skillInfo->UniqueBit);
    }

    boost::to_block_range(bits, std::back_inserter(result));
}

void Guild::SendGuildKnowRecipes(WorldSession* session)
{
    size_t skillLineBitSize = 0;

    std::vector<std::pair<uint32, std::vector<uint8>>> skillLineVector;
    for (auto const& item : guildProfessions)
    {
        std::vector<uint8> SkillLineBitArray;
        FillRecipeBitset(SkillLineBitArray, item.second);

        skillLineVector.push_back(std::make_pair(item.first, SkillLineBitArray));

        skillLineBitSize += SkillLineBitArray.size();
    }

    WorldPacket data(SMSG_GUILD_RECIPES, 2 + skillLineVector.size() * (4) + skillLineBitSize * (1));

    data.WriteBits(skillLineVector.size(), 14);

    data.FlushBits();

    for (auto const& item : skillLineVector)
    {
        if (!item.second.empty())
            data.append(item.second.data(), item.second.size());

        data << uint32(item.first);
    }

    session->SendPacket(&data);
}

void Guild::SendGuildMemberRecipes(WorldSession* session, ObjectGuid memberGuid, uint32 skillID)
{
    int32 skillRank = 0;
    int32 skillStep = 0;

    std::vector<uint8> SkillLineBitArray;

    auto professions = memberProfessions.equal_range(memberGuid);
    if (professions.first != professions.second)
        for (auto prof = professions.first; prof != professions.second; ++prof)
            if (prof->second.first == skillID)
            {
                FillRecipeBitset(SkillLineBitArray, prof->second.second.Recipes);

                skillRank = prof->second.second.Rank;
                skillStep = prof->second.second.Step;
            }

    WorldPacket data(SMSG_GUILD_MEMBER_RECIPES, 1 + 8 + 4 + 4 + SkillLineBitArray.size());

    data.WriteGuidMask(memberGuid, 3, 2, 4, 1, 7, 0, 5, 6);

    data << int32(skillRank);

    data.WriteGuidBytes(memberGuid, 0, 3);

    data << int32(skillStep);

    data.WriteGuidBytes(memberGuid, 7, 5, 1, 2);

    if (!SkillLineBitArray.empty())
        data.append(SkillLineBitArray.data(), SkillLineBitArray.size());

    data.WriteGuidBytes(memberGuid, 6);

    data << uint32(skillID);

    data.WriteGuidBytes(memberGuid, 4);

    session->SendPacket(&data);
}

void Guild::SendGuildMembersForRecipe(WorldSession* session, uint32 skillSpellID, uint32 skillLineID)
{
    GuidUnorderedSet& guids = guildRecipes[skillSpellID];
    if (guids.empty())
        return;

    ByteBuffer guidData;

    WorldPacket data(SMSG_GUILD_MEMBERS_FOR_RECIPE, 4 + 4 + 3 + guids.size() * (1 + 8));

    data << uint32(skillLineID);
    data << uint32(skillSpellID);

    data.WriteBits(guids.size(), 24);

    for (ObjectGuid const& guid : guids)
    {
        data.WriteGuidMask(guid, 4, 1, 0, 7, 2, 6, 5, 3);
        guidData.WriteGuidBytes(guid, 0, 5, 4, 1, 3, 7, 6, 2);
    }

    data.append(guidData);

    session->SendPacket(&data);
}

void Guild::UpdateProfessionSkill(Member* member, uint32 skillId, uint16 newValue, uint32 step, bool newProfession)
{
    ObjectGuid memberGUID = member->GetGUID();

    auto professions = memberProfessions.equal_range(memberGUID);
    if (professions.first != professions.second)
    {
        if (!newProfession) // update already existing profession
        {
            for (auto prof = professions.first; prof != professions.second; ++prof)
            {
                if (prof->second.first == skillId)
                {
                    prof->second.second.Rank = newValue;
                    prof->second.second.Step = step;
                }
            }
        }
        else // add new profession to already existing member
        {
            Profession profession;
            profession.Rank = newValue;
            profession.Step = step;

            // Get default recipes which aren't saved in DB
            std::unordered_set<uint32> defaultRecipes = member->GetDefaultSkillSpells(skillId, newValue);
            profession.Recipes.insert(defaultRecipes.begin(), defaultRecipes.end());

            // Get list of recipes from DB
            QueryResult res = CharacterDatabase.PQuery("SELECT guid, spell FROM character_spell WHERE guid = '%u'", memberGUID.GetCounter());
            if (res)
            {
                do
                {
                    uint32 spellId = (*res)[1].GetUInt32();
                    if (SpellInfo const* spell = sSpellMgr->GetSpellInfo(spellId))
                        if (spell->HasAttribute(SPELL_ATTR0_TRADESPELL))
                            profession.Recipes.insert(spellId);
                } while (res->NextRow());
            }

            memberProfessions.emplace(MemberProfessionContainer::value_type(memberGUID, std::make_pair(skillId, profession)));

            guildProfessions[skillId].insert(profession.Recipes.begin(), profession.Recipes.end());

            for (uint32 recipe : profession.Recipes)
                guildRecipes[recipe].insert(memberGUID);
        }
    }
    else if (newProfession) // add new profession to non-existing member in map
    {
        Profession profession;
        profession.Rank = newValue;
        profession.Step = step;

        // Get default recipes which aren't saved in DB
        std::unordered_set<uint32> defaultRecipes = member->GetDefaultSkillSpells(skillId, newValue);
        profession.Recipes.insert(defaultRecipes.begin(), defaultRecipes.end());

        // Get list of recipes from DB
        QueryResult res = CharacterDatabase.PQuery("SELECT guid, spell FROM character_spell WHERE guid = '%u'", memberGUID.GetCounter());
        if (res)
        {
            do
            {
                uint32 spellId = (*res)[1].GetUInt32();
                if (SpellInfo const* spell = sSpellMgr->GetSpellInfo(spellId))
                    if (spell->HasAttribute(SPELL_ATTR0_TRADESPELL))
                        profession.Recipes.insert(spellId);
            } while (res->NextRow());
        }

        memberProfessions.insert(MemberProfessionContainer::value_type(memberGUID, std::make_pair(skillId, profession)));

        guildProfessions[skillId].insert(profession.Recipes.begin(), profession.Recipes.end());

        for (uint32 recipe : profession.Recipes)
            guildRecipes[recipe].insert(memberGUID);
    }
}

void Guild::RemoveProfession(ObjectGuid member, uint32 skillId)
{
    auto professions = memberProfessions.equal_range(member);
    if (professions.first == professions.second)
        return;

    // remove from member professions
    for (auto prof = professions.first; prof != professions.second; ++prof)
        if (prof->second.first == skillId)
        {
            for (uint32 recipe : prof->second.second.Recipes)
            {
                guildProfessions[skillId].erase(recipe);
                guildRecipes[recipe].erase(member);
            }

            prof = memberProfessions.erase(prof);
        }
}

void Guild::AddProfessionRecipe(ObjectGuid member, uint32 spellId)
{
    SkillLineAbilityEntry const* skillInfo = sDBCManager->GetSkillLineAbilityBySpell(spellId);
    if (!skillInfo)
        return;

    SpellInfo const* spell = sSpellMgr->GetSpellInfo(spellId);
    if (!spell)
        return;

    if (!spell->HasAttribute(SPELL_ATTR0_TRADESPELL))
        return;

    auto professions = memberProfessions.equal_range(member);
    // when member already has profession, simply add new recipe
    if (professions.first != professions.second)
    {
        for (auto prof = professions.first; prof != professions.second; ++prof)
            if (prof->second.first == skillInfo->SkillLine)
                prof->second.second.Recipes.insert(spellId);

        guildProfessions[skillInfo->SkillLine].insert(spellId);

        guildRecipes[spellId].insert(member);
    }
}

void Guild::SetGuildFlag(uint32 flag, bool apply)
{
    bool Changed = false;

    if (apply)
    {
        if ((m_guildFlags & flag) == 0)
        {
            m_guildFlags |= flag;
            Changed = true;
        }
    }
    else
    {
        if ((m_guildFlags & flag) != 0)
        {
            m_guildFlags &= ~flag;
            Changed = true;
        }
    }

    if (!Changed)
        false;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GUILD_FLAGS);
    stmt->setUInt32(0, GetGuildFlags());
    stmt->setUInt32(1, GetId());
    CharacterDatabase.Execute(stmt);

    switch (flag)
    {
        case GUILD_FLAG_RENAME:
        {
            WorldPacket data(SMSG_GUILD_FLAGGED_FOR_RENAME, 1);

            data.WriteBit(apply);

            data.FlushBits();

            BroadcastPacket(&data);

            break;
        }
        default:
            break;
    }
}

bool Guild::IsAllowedToChangeGuildName(ObjectGuid guid) const
{
    if (Member const* member = GetMember(guid))
        if (member->IsRank(GR_GUILDMASTER) || member->IsRank(GR_OFFICER))
            return true;

    return false;
}
