/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_GUILDMGR_H
#define SF_GUILDMGR_H

#include "Guild.h"

#include "Singleton/Singleton.hpp"

class GuildMgr
{
    friend class Tod::Singleton<GuildMgr>;

    GuildMgr();
    ~GuildMgr();

    public:
        Guild* GetGuildByLeader(ObjectGuid guid) const;
        Guild* GetGuildById(uint32 guildId) const;
        Guild* GetGuildByGuid(ObjectGuid guid) const;
        Guild* GetGuildByName(std::string const& guildName) const;

        std::string GetGuildNameById(uint32 guildId) const;

        void LoadGuildXpForLevel();
        void LoadGuildRewards();

        void LoadGuilds();
        void AddGuild(Guild* guild);
        void RemoveGuild(uint32 guildId);

        void SaveGuilds();
        void UpdateMembers();

        uint32 GenerateGuildId();
        void SetNextGuildId(uint32 Id)
        {
            NextGuildId = Id;
        }

        uint32 GetXPForGuildLevel(uint8 level) const;
        std::vector<GuildReward> const& GetGuildRewards() const
        {
            return GuildRewards;
        }

        void ResetTimes(bool week);

    protected:
        typedef std::unordered_map<uint32, Guild*> GuildContainer;
        uint32 NextGuildId;
        GuildContainer GuildStore;
        std::vector<uint64> GuildXPperLevel;
        std::vector<GuildReward> GuildRewards;
};

#define sGuildMgr Tod::Singleton<GuildMgr>::GetSingleton()

#endif
