/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Scenario.h"
#include "Player.h"
#include "ScenarioMgr.h"
#include "InstanceSaveMgr.h"
#include "ObjectMgr.h"

Scenario::Scenario(ScenarioData const* scenarioData, Map const* map) : _data(scenarioData), _currentstep(nullptr)
{
    ASSERT(_data);

    _difficulty = map->GetDifficultyID();

    for (auto step : _data->Steps)
        SetStepState(step.second, SCENARIO_STEP_NOT_STARTED);

    if (_data->BonusStep)
        SetStepState(_data->BonusStep, SCENARIO_STEP_NOT_STARTED);
}

Scenario::~Scenario()
{
    for (ObjectGuid guid : _players)
        if (Player* player = ObjectAccessor::FindPlayer(guid))
            SendBootPlayer(player);

    _players.clear();
}

void Scenario::CompleteStep(ScenarioStepEntry const* step)
{
    if (!step)
        return;

    if (step->IsBonusObjective())
        return;

    uint8 NextStepID = step->Step + 1;

    ScenarioStepEntry const* newStep = nullptr;

    auto itr = _data->Steps.find(NextStepID);
    if (itr != _data->Steps.end())
        newStep = itr->second;

    SetStep(newStep);

    if (IsComplete())
        CompleteScenario();
    else
        TC_LOG_ERROR("scenario", "Scenario::CompleteStep: Scenario (id: %u, step: %u) was completed, but could not determine new step, or validate scenario completion.", step->ScenarioID, step->ID);
}

void Scenario::CompleteScenario()
{
    ScenarioEntry const* scenario = _data->Entry;
    ASSERT(scenario);

    for (ObjectGuid guid : _players)
        if (Player* player = ObjectAccessor::FindPlayer(guid))
        {
            player->UpdateCriteria(CRITERIA_TYPE_COMPLETE_SCENARIO_COUNT, scenario->ID);
            player->UpdateCriteria(CRITERIA_TYPE_COMPLETE_SCENARIO, scenario->ID);
        }

    WorldPacket data(SMSG_SCENARIO_COMPLETED, 4);

    data << uint32(scenario->ID);

    SendPacket(&data);
}

void Scenario::SetStep(ScenarioStepEntry const* step, bool sendPacket /*= true*/)
{
    // remove old step timer
    if (_currentstep)
        RemoveCriteriaTimer(CRITERIA_TIMED_TYPE_SCENARIO_STAGE, _currentstep->Step);

    _currentstep = step;

    uint32 ScenarioID = _data->Entry->ID;
    int32 CurrentStep = -1;

    if (step)
    {
        SetStepState(step, SCENARIO_STEP_IN_PROGRESS);

        StartCriteriaTimer(CRITERIA_TIMED_TYPE_SCENARIO_STAGE, step->Step);

        CurrentStep = int32(step->Step);
    }

    if (!sendPacket)
        return;

    uint32 BonusStep = 0;
    bool IsBonusStepCompleted = false;
    if (ScenarioStepEntry const* bonusStep = GetBonusStep())
    {
        BonusStep = bonusStep->Step;
        IsBonusStepCompleted = GetStepState(bonusStep) == SCENARIO_STEP_DONE;
    }

    bool IsScenarioCompleted = IsComplete();

    std::unordered_map<uint32, CriteriaProgress> criteriaList;
    for (auto itr : _criteriaProgress)
    {
        if (itr.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        criteriaList[itr.first] = itr.second;
    }

    uint32 WaveCurrentID = 0;
    uint32 WaveMaxID = 0;
    uint32 Difficulty = 0;
    uint32 TimerDuration = 0;

    ByteBuffer criteriaData;

    WorldPacket data(SMSG_SCENARIO_STATE, 4 + 4 + 4 + 4 + 4 + 4 + 4 + 3 + criteriaList.size() * (2 * (1 + 8) + 1 + 4 + 4 + 4 + 4));

    data << uint32(WaveCurrentID);
    data << uint32(TimerDuration);
    data << uint32(WaveMaxID);
    data << uint32(ScenarioID);
    data << uint32(Difficulty);
    data << uint32(BonusStep);
    data << int32(CurrentStep);

    data.WriteBits(criteriaList.size(), 19);

    data.WriteBit(IsBonusStepCompleted);

    for (auto & itr : criteriaList)
    {
        ObjectGuid counter = ObjectGuid(itr.second.Counter);
        ObjectGuid guid = itr.second.PlayerGUID;

        data.WriteGuidMask(counter, 3, 5);

        data.WriteGuidMask(guid, 2);

        data.WriteGuidMask(counter, 6);

        data.WriteGuidMask(guid, 7);

        data.WriteGuidMask(counter, 1, 7, 0);

        data.WriteGuidMask(guid, 3);

        data.WriteGuidMask(counter, 4);

        data.WriteGuidMask(guid, 0, 5, 1, 4);

        data.WriteGuidMask(counter, 2);

        data.WriteGuidMask(guid, 6);

        data.WriteBits(0, 4); // Flags

        criteriaData.WriteGuidBytes(guid, 3, 7, 5);

        criteriaData << uint32(0); // timeElapsd

        criteriaData.WriteGuidBytes(guid, 2);

        criteriaData << uint32(0); // timeCreate

        criteriaData.WriteGuidBytes(guid, 6, 4);

        criteriaData.AppendPackedTime(itr.second.Date);

        criteriaData.WriteGuidBytes(guid, 1);

        criteriaData.WriteGuidBytes(counter, 3, 2, 1, 5, 4, 7);

        criteriaData.WriteGuidBytes(guid, 0);

        criteriaData.WriteGuidBytes(counter, 6, 0);

        criteriaData << uint32(itr.first);
    }

    data.WriteBit(IsScenarioCompleted);

    if (!criteriaData.empty())
        data.append(criteriaData);
    else
        data.FlushBits();

    SendPacket(&data);
}

void Scenario::OnPlayerEnter(Player* player)
{
    _players.insert(player->GetGUID());

    SendScenarioState(player);
}

void Scenario::OnPlayerExit(Player* player)
{
    _players.erase(player->GetGUID());
    SendBootPlayer(player);
}

bool Scenario::IsComplete()
{
    for (auto step : _data->Steps)
        if (GetStepState(step.second) != SCENARIO_STEP_DONE)
            return false;

    return true;
}

ScenarioStepState Scenario::GetStepState(ScenarioStepEntry const* step)
{
    auto itr = _stepStates.find(step);
    if (itr == _stepStates.end())
        return SCENARIO_STEP_INVALID;

    return itr->second;
}

void Scenario::SendCriteriaUpdate(Criteria const * criteria, CriteriaProgress const * progress, uint32 timeElapsed, bool timedCompleted) const
{
    ObjectGuid counter = ObjectGuid(progress->Counter);
    ObjectGuid guid = progress->PlayerGUID;

    uint32 Flags = 0;
    if (criteria->Entry->StartTimer)
        Flags = timedCompleted ? 1 : 0;

    WorldPacket data(SMSG_SCENARIO_PROGRESS_UPDATE, 2 * (1 + 8) + 1 + 4 + 4 + 4 + 4);

    data.WriteGuidMask(guid, 6, 4, 5);

    data.WriteGuidMask(counter, 1, 5, 6, 7, 4);

    data.WriteGuidMask(guid, 1);

    data.WriteGuidMask(counter, 0);

    data.WriteGuidMask(guid, 2, 3);

    data.WriteGuidMask(counter, 2, 3);

    data.WriteGuidMask(guid, 0);

    data.WriteBits(Flags, 4);

    data.WriteGuidMask(guid, 7);

    data.WriteGuidBytes(counter, 5, 2);

    data.WriteGuidBytes(guid, 7, 4);

    data.WriteGuidBytes(counter, 4, 0);

    data.WriteGuidBytes(guid, 1, 2);

    data << uint32(timeElapsed);

    data.WriteGuidBytes(counter, 3);

    data << uint32(criteria->ID);

    data.WriteGuidBytes(counter, 1);

    data.AppendPackedTime(progress->Date);

    data << uint32(timeElapsed);

    data.WriteGuidBytes(guid, 0, 3, 6);

    data.WriteGuidBytes(counter, 7, 6);

    data.WriteGuidBytes(guid, 5);

    SendPacket(&data);
}

bool Scenario::CanUpdateCriteriaTree(Criteria const * /*criteria*/, CriteriaTree const * tree, Player * /*referencePlayer*/) const
{
    ScenarioStepEntry const* step = tree->ScenarioStep;
    if (!step)
        return false;

    if (step->ScenarioID != _data->Entry->ID)
        return false;

    ScenarioStepEntry const* currentStep = GetStep();
    if (!currentStep)
        return false;

    if (step->IsBonusObjective())
        return true;

    return currentStep == step;
}

bool Scenario::CanCompleteCriteriaTree(CriteriaTree const* tree)
{
    ScenarioStepEntry const* step = tree->ScenarioStep;
    if (!step)
        return false;

    if (step->ScenarioID != _data->Entry->ID)
        return false;

    if (step->IsBonusObjective())
        return !IsComplete();

    if (step != GetStep())
        return false;

    return true;
}

void Scenario::CompletedCriteriaTree(CriteriaTree const* tree, Player* /*referencePlayer*/)
{
    ScenarioStepEntry const* step = tree->ScenarioStep;
    if (!step)
        return;

    if (!step->IsBonusObjective() && step != GetStep())
        return;

    if (GetStepState(step) == SCENARIO_STEP_DONE)
        return;

    SetStepState(step, SCENARIO_STEP_DONE);

    CompleteStep(step);
}

void Scenario::SendPacket(WorldPacket* data) const
{
    for (ObjectGuid guid : _players)
        if (Player* player = ObjectAccessor::FindPlayer(guid))
            player->SendDirectMessage(data);
}

ScenarioStepEntry const* Scenario::GetFirstStep()
{
    for (auto step : _data->Steps)
        if (GetStepState(step.second) != SCENARIO_STEP_DONE)
            return step.second;

    return nullptr;
}

ScenarioStepEntry const* Scenario::GetBonusStep() const
{
    if (_difficulty != DIFFICULTY_HC_SCENARIO)
        return nullptr;

    return _data->BonusStep;
}

void Scenario::SendScenarioState(Player* player)
{
    uint32 ScenarioID = _data->Entry->ID;

    int32 CurrentStep = -1;
    if (ScenarioStepEntry const* step = GetStep())
        CurrentStep = int32(step->Step);

    uint32 BonusStep = 0;
    bool IsBonusStepCompleted = false;
    if (ScenarioStepEntry const* bonusStep = GetBonusStep())
    {
        BonusStep = bonusStep->Step;
        IsBonusStepCompleted = GetStepState(bonusStep) == SCENARIO_STEP_DONE;
    }

    bool IsScenarioCompleted = IsComplete();

    std::unordered_map<uint32, CriteriaProgress> criteriaList;
    for (auto itr : _criteriaProgress)
    {
        if (itr.second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
            continue;

        criteriaList[itr.first] = itr.second;
    }

    uint32 WaveCurrentID = 0;
    uint32 WaveMaxID = 0;
    uint32 Difficulty = 0;
    uint32 TimerDuration = 0;

    ByteBuffer criteriaData;

    WorldPacket data(SMSG_SCENARIO_STATE, 4 + 4 + 4 + 4 + 4 + 4 + 4 + 3 + criteriaList.size() * (2 * (1 + 8) + 1 + 4 + 4 + 4 + 4));

    data << uint32(WaveCurrentID);
    data << uint32(TimerDuration);
    data << uint32(WaveMaxID);
    data << uint32(ScenarioID);
    data << uint32(Difficulty);
    data << uint32(BonusStep);
    data << int32(CurrentStep);

    data.WriteBits(criteriaList.size(), 19);

    data.WriteBit(IsBonusStepCompleted);

    for (auto & itr : criteriaList)
    {
        ObjectGuid counter = ObjectGuid(itr.second.Counter);
        ObjectGuid guid = itr.second.PlayerGUID;

        data.WriteGuidMask(counter, 3, 5);

        data.WriteGuidMask(guid, 2);

        data.WriteGuidMask(counter, 6);

        data.WriteGuidMask(guid, 7);

        data.WriteGuidMask(counter, 1, 7, 0);

        data.WriteGuidMask(guid, 3);

        data.WriteGuidMask(counter, 4);

        data.WriteGuidMask(guid, 0, 5, 1, 4);

        data.WriteGuidMask(counter, 2);

        data.WriteGuidMask(guid, 6);

        data.WriteBits(0, 4); // Flags

        criteriaData.WriteGuidBytes(guid, 3, 7, 5);

        criteriaData << uint32(0); // timeElapsed

        criteriaData.WriteGuidBytes(guid, 2);

        criteriaData << uint32(0); // timeCreate

        criteriaData.WriteGuidBytes(guid, 6, 4);

        criteriaData.AppendPackedTime(itr.second.Date);

        criteriaData.WriteGuidBytes(guid, 1);

        criteriaData.WriteGuidBytes(counter, 3, 2, 1, 5, 4, 7);

        criteriaData.WriteGuidBytes(guid, 0);

        criteriaData.WriteGuidBytes(counter, 6, 0);

        criteriaData << uint32(itr.first);
    }

    data.WriteBit(IsScenarioCompleted);

    if (!criteriaData.empty())
        data.append(criteriaData);
    else
        data.FlushBits();

    player->SendDirectMessage(&data);
}

CriteriaList const& Scenario::GetCriteriaByType(CriteriaTypes type) const
{
    return sCriteriaMgr->GetScenarioCriteriaByType(type);
}

void Scenario::SendBootPlayer(Player* player)
{
    ScenarioEntry const* scenario = _data->Entry;
    ASSERT(scenario);

    WorldPacket data(SMSG_SCENARIO_BOOT, 4);

    data << int32(scenario->ID);

    player->SendDirectMessage(&data);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player)
{
    CriteriaHandler::UpdateCriteria(type, 0, 0, 0, nullptr, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit)
{
    CriteriaHandler::UpdateCriteria(type, 0, 0, 0, unit, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1)
{
    CriteriaHandler::UpdateCriteria(type, miscValue1, 0, 0, unit, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1, uint64 miscValue2)
{
    CriteriaHandler::UpdateCriteria(type, miscValue1, miscValue2, 0, unit, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3)
{
    CriteriaHandler::UpdateCriteria(type, miscValue1, miscValue2, miscValue3, unit, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1)
{
    CriteriaHandler::UpdateCriteria(type, miscValue1, 0, 0, nullptr, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1, uint64 miscValue2)
{
    CriteriaHandler::UpdateCriteria(type, miscValue1, miscValue2, 0, nullptr, player);
}

void Scenario::UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3)
{
    CriteriaHandler::UpdateCriteria(type, miscValue1, miscValue2, miscValue3, nullptr, player);
}
