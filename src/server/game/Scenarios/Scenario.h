/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef Scenario_h__
#define Scenario_h__

#include "CriteriaHandler.h"
#include "Map.h"

struct ScenarioData;
struct ScenarioStepEntry;

enum ScenarioStepState
{
    SCENARIO_STEP_INVALID       = 0,
    SCENARIO_STEP_NOT_STARTED   = 1,
    SCENARIO_STEP_IN_PROGRESS   = 2,
    SCENARIO_STEP_DONE          = 3
};

class Scenario : public CriteriaHandler
{
    public:
        Scenario(ScenarioData const* scenarioData, Map const* map);
        ~Scenario();

        void SetStep(ScenarioStepEntry const* step, bool sendPacket = true);

        virtual void CompleteStep(ScenarioStepEntry const* step);
        virtual void CompleteScenario();

        virtual void OnPlayerEnter(Player* player);
        virtual void OnPlayerExit(Player* player);
        virtual void Update(uint32 /*diff*/) { }

        bool IsComplete();

        void SetStepState(ScenarioStepEntry const* step, ScenarioStepState state) { _stepStates[step] = state; }

        ScenarioStepState GetStepState(ScenarioStepEntry const* step);
        ScenarioStepEntry const* GetStep() const { return _currentstep; }
        ScenarioStepEntry const* GetFirstStep();
        ScenarioStepEntry const* GetBonusStep() const;

        void SendScenarioState(Player* player);
        void SendBootPlayer(Player* player);

        ScenarioData const* GetScenarioData() const { return _data; }

        void UpdateCriteria(CriteriaTypes type, Player* player);
        void UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit);
        void UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1);
        void UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1, uint64 miscValue2);
        void UpdateCriteria(CriteriaTypes type, Player* player, Unit* unit, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3);
        void UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1);
        void UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1, uint64 miscValue2);
        void UpdateCriteria(CriteriaTypes type, Player* player, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3);

    protected:
        GuidUnorderedSet _players;

        void SendAccountCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const override { }
        void SendCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const override;

        void SendCriteriaProgressRemoved(uint32 /*criteriaId*/) override { }

        bool CanUpdateCriteriaTree(Criteria const* criteria, CriteriaTree const* tree, Player* referencePlayer) const override;
        bool CanCompleteCriteriaTree(CriteriaTree const* tree) override;
        void CompletedCriteriaTree(CriteriaTree const* tree, Player* referencePlayer) override;
        void AfterCriteriaTreeUpdate(CriteriaTree const* /*tree*/, Player* /*referencePlayer*/) override { }

        void SendPacket(WorldPacket* data) const override;

        void SendAllData(Player* /*receiver*/) const override { }

        CriteriaList const& GetCriteriaByType(CriteriaTypes type) const override;
        ScenarioData const* _data;

    private:
        ScenarioStepEntry const* _currentstep;
        std::unordered_map<ScenarioStepEntry const*, ScenarioStepState> _stepStates;
        Difficulty _difficulty;
};

#endif // Scenario_h__
