/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "InstanceScenario.h"
#include "Player.h"
#include "InstanceSaveMgr.h"
#include "ObjectMgr.h"
#include "ScenarioMgr.h"

InstanceScenario::InstanceScenario(Map const* map, ScenarioData const* scenarioData) : Scenario(scenarioData, map), _map(map)
{
    ASSERT(_map);

    LoadInstanceData(_map->GetInstanceId());

    if (ScenarioStepEntry const* step = GetFirstStep())
        SetStep(step, false);
    else
        TC_LOG_ERROR("scenario", "Scenario::Scenario: Could not launch Scenario (id: %u), found no valid scenario step", scenarioData->Entry->ID);
}

void InstanceScenario::SaveToDB()
{
    if (_criteriaProgress.empty())
        return;

    DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(_map->GetDifficultyID());
    if (!difficultyEntry || difficultyEntry->Flags & DIFFICULTY_FLAG_CHALLENGE_MODE) // Map should have some sort of "CanSave" boolean that returns whether or not the map is savable. (Challenge modes cannot be saved for example)
        return;

    uint32 id = _map->GetInstanceId();
    if (!id)
    {
        TC_LOG_DEBUG("scenario", "Scenario::SaveToDB: Can not save scenario progress without an instance save. Map::GetInstanceId() did not return an instance save.");
        return;
    }

    std::vector<uint32> removedCriterias;

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    for (auto iter = _criteriaProgress.begin(); iter != _criteriaProgress.end(); ++iter)
    {
        if (iter->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE)
            continue;

        if (!iter->second.Counter && iter->second.LoadedFromDB)
            iter->second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE;

        PreparedStatement* stmt = nullptr;

        switch (iter->second.DBStatus)
        {
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_SCENARIO_INSTANCE_CRITERIA);
                stmt->setUInt32(0, id);
                stmt->setUInt32(1, iter->first);
                stmt->setUInt64(2, iter->second.Counter);
                stmt->setUInt32(3, uint32(iter->second.Date));
                stmt->setUInt32(4, iter->second.PlayerGUID.GetCounter());
                trans->Append(stmt);
                break;
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE:
                removedCriterias.push_back(iter->first);
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_SCENARIO_INSTANCE_CRITERIA);
                stmt->setUInt32(0, id);
                stmt->setUInt32(1, iter->first);
                trans->Append(stmt);
                break;
            case ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_SCENARIO_INSTANCE_CRITERIA);
                stmt->setUInt64(0, iter->second.Counter);
                stmt->setUInt32(1, uint32(iter->second.Date));
                stmt->setUInt32(2, iter->second.PlayerGUID.GetCounter());
                stmt->setUInt32(3, id);
                stmt->setUInt32(4, iter->first);
                trans->Append(stmt);
                break;
        }

        iter->second.LoadedFromDB = true;
        iter->second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
    }

    for (uint32 criteriaId : removedCriterias)
        _criteriaProgress.erase(criteriaId);

    removedCriterias.clear();

    CharacterDatabase.CommitTransaction(trans);
}

void InstanceScenario::LoadInstanceData(uint32 instanceId)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_SCENARIO_INSTANCE_CRITERIA_FOR_INSTANCE);
    stmt->setUInt32(0, instanceId);

    PreparedQueryResult result = CharacterDatabase.Query(stmt);
    if (result)
    {
        SQLTransaction trans = CharacterDatabase.BeginTransaction();
        time_t now = time(nullptr);

        std::vector<CriteriaTree const*> criteriaTrees;
        do
        {
            Field* fields   = result->Fetch();
            uint32 id       = fields[0].GetUInt32();
            uint64 counter  = fields[1].GetUInt64();
            time_t date     = time_t(fields[2].GetUInt32());
            uint32 lowGuid  = fields[3].GetUInt32();

            Criteria const* criteria = sCriteriaMgr->GetCriteria(id);
            if (!criteria)
            {
                // Removing non-existing criteria data for all instances
                TC_LOG_ERROR("criteria.instancescenarios", "Removing scenario criteria %u data from the table `instance_scenario_progress`.", id);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_SCENARIO_INSTANCE_CRITERIA);
                stmt->setUInt32(0, instanceId);
                stmt->setUInt32(1, id);
                trans->Append(stmt);
                continue;
            }

            if (criteria->Entry->StartTimer && time_t(date + criteria->Entry->StartTimer) < now)
                continue;

            CriteriaProgress& progress = _criteriaProgress[id];
            progress.Counter = counter;
            progress.Date = date;
            progress.PlayerGUID = ObjectGuid::Create<HighGuid::Player>(lowGuid);

            progress.LoadedFromDB = true;
            progress.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;

            if (CriteriaTreeList const* trees = sCriteriaMgr->GetCriteriaTreesByCriteria(criteria->ID))
                for (CriteriaTree const* tree : *trees)
                    criteriaTrees.push_back(tree);
        }
        while (result->NextRow());

        CharacterDatabase.CommitTransaction(trans);

        for (CriteriaTree const* tree : criteriaTrees)
        {
            ScenarioStepEntry const* step = tree->ScenarioStep;
            if (!step)
                continue;

            if (IsCompletedCriteriaTree(tree))
                SetStepState(step, SCENARIO_STEP_DONE);
        }
    }
}

std::string InstanceScenario::GetOwnerInfo() const
{
    std::ostringstream str;

    str << "Instance ID: " << _map->GetInstanceId();

    return str.str();
}

void InstanceScenario::SendPacket(WorldPacket* data) const
{
    _map->SendToPlayers(data);
}
