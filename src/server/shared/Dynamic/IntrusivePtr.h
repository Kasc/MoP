/*
 * Copyright( C ) 2015 - 2016 Theatre of Dreams < http://www.theatreofdreams.eu/>
 *
 * This program is free software; you can redistribute it and / or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or( at your
 * option ) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.If not, see < http://www.gnu.org/licenses/>.
 *
 * Author: lukaasm <lukaasm@gmail.com>
*/

#ifndef TOD_INTRUSIVEPTR_H
#define TOD_INTRUSIVEPTR_H

namespace Tod
{
    template< class T >
    class IntrusivePtr
    {
    public:
        IntrusivePtr( T * ptr = nullptr )
            : m_object( nullptr )
        {
            Reset( ptr );
        }

        IntrusivePtr( const IntrusivePtr & ptr )
            : IntrusivePtr( ptr.m_object )
        {
        }

        ~IntrusivePtr()
        {
            if ( m_object == nullptr )
                return;

            m_object->RemRef();

            if ( m_object->GetRefCounter() == 0 )
            {
                delete m_object;
            }
        }

        T * GetPtr() const
        {
            return m_object;
        }

        T * operator ->( ) const
        {
            return m_object;
        }

        T & operator *( ) const
        {
            return *m_object;
        }

        void operator = ( T * ptr )
        {
            Reset( ptr );
        }

        void operator = ( const IntrusivePtr & ptr )
        {
            Reset( ptr.m_object );
        }

        void Reset( T * ptr = nullptr )
        {
            if ( m_object != nullptr )
            {
                m_object->RemRef();
            }

            m_object = ptr;

            if ( m_object != nullptr )
            {
                m_object->AddRef();
            }
        }

        bool IsNull() const
        {
            return m_object == nullptr;
        }

        bool operator == ( const T & ptr ) const
        {
            return m_object == ptr;
        }

        bool operator != ( const T & ptr ) const
        {
            return m_object != ptr;
        }

        bool operator == ( const IntrusivePtr & ptr ) const
        {
            return m_object == ptr.m_object;
        }

        bool operator != ( const IntrusivePtr & ptr ) const
        {
            return m_object != ptr.m_object;
        }

        bool operator < ( const IntrusivePtr & ptr ) const
        {
            return m_object != ptr.m_object;
        }

    private:
        T* m_object;
    };
}

#endif
