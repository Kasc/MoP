/*
 * Copyright (C) 2017-2017 Project Aurora
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "npc_pet_pri_".
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "PassiveAI.h"
#include "PetAI.h"

enum PriestSpells
{
    SPELL_PRIEST_GLYPH_OF_SHADOWFIEND       = 58228,
    SPELL_PRIEST_SHADOWFIEND_DEATH          = 57989,
    SPELL_PRIEST_SHADOWFIEND_IMPROVED       = 37570,
    SPELL_PRIEST_T12_SHADOW_2P              = 99154,
    SPELL_PRIEST_SHADOWFLAME                = 99155,

    SPELL_PRIEST_LIGHTWELL_CHARGES_IMPROVED = 59907,
    SPELL_PRIEST_LIGHTWELL_CHARGES          = 126150,
    SPELL_PRIEST_GLYPH_DEEP_WELLS           = 55673,
    SPELL_PRIEST_LIGHTSPRING_DRIVER         = 126138,
    NPC_PRIEST_LIGHTWELL_IMPROVED           = 31897,

    SPELL_PRIEST_SPECTRAL_GUISE_CLONE       = 119012,
    SPELL_PRIEST_SPECTRAL_GUISE_CHARGES     = 119030,
    SPELL_PRIEST_SPECTRAL_GUISE_STEALTH     = 119032,
    SPELL_PRIEST_INITIALIZE_IMAGES          = 102284,

    SPELL_PRIEST_PSYFIEND_PSYCHIC_TERROR    = 113792
};

class npc_pet_pri_lightwell : public CreatureScript
{
    public:
        npc_pet_pri_lightwell() : CreatureScript("npc_pet_pri_lightwell") { }

        struct npc_pet_pri_lightwellAI : public PassiveAI
        {
            npc_pet_pri_lightwellAI(Creature* creature) : PassiveAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                uint32 chargesSpell = SPELL_PRIEST_LIGHTWELL_CHARGES;
                if (me->GetEntry() == NPC_PRIEST_LIGHTWELL_IMPROVED)
                    chargesSpell = SPELL_PRIEST_LIGHTWELL_CHARGES_IMPROVED;
                else
                    DoCast(SPELL_PRIEST_LIGHTSPRING_DRIVER);

                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(chargesSpell);
                if (!chargesSpell)
                    return;

                int32 stackAmount = spellInfo->StackAmount;
                if (AuraEffect const* aurEff = summoner->GetAuraEffect(SPELL_PRIEST_GLYPH_DEEP_WELLS, EFFECT_0))
                    stackAmount += aurEff->GetAmount();

                int32 charges = stackAmount;

                CustomSpellValues values;
                values.AddSpellMod(SPELLVALUE_AURA_STACK, stackAmount);
                values.AddSpellMod(SPELLVALUE_AURA_CHARGES, charges);

                me->CastCustomSpell(chargesSpell, values, me);
            }

            void EnterEvadeMode(EvadeReason /*why*/) override
            {
                if (!me->IsAlive())
                    return;

                me->DeleteThreatList();
                me->CombatStop(true);
                me->ResetPlayerDamageReq();
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_pri_lightwellAI(creature);
        }
};

class npc_pet_pri_shadowfiend_or_mindbender : public CreatureScript
{
    public:
        npc_pet_pri_shadowfiend_or_mindbender() : CreatureScript("npc_pet_pri_shadowfiend_or_mindbender") { }

        struct npc_pet_pri_shadowfiend_or_mindbenderAI : public PetAI
        {
            npc_pet_pri_shadowfiend_or_mindbenderAI(Creature* creature) : PetAI(creature) { }

            void IsSummonedBy(Unit* summoner) override
            {
                if (summoner->HasAura(SPELL_PRIEST_GLYPH_OF_SHADOWFIEND))
                    DoCast(SPELL_PRIEST_SHADOWFIEND_DEATH);

                // Item - Priest T12 Shadow 2P Bonus
                if (summoner->GetShapeshiftForm() == FORM_SHADOWFORM)
                    if (summoner->HasAura(SPELL_PRIEST_T12_SHADOW_2P))
                        DoCast(SPELL_PRIEST_SHADOWFLAME);
            }
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return new npc_pet_pri_shadowfiend_or_mindbenderAI(creature);
        }
};

class npc_pet_pri_spectral_guise : public CreatureScript
{
    public:
        npc_pet_pri_spectral_guise() : CreatureScript("npc_pet_pri_spectral_guise") { }

        struct npc_pet_pri_spectral_guiseAI : public PassiveAI
        {
            npc_pet_pri_spectral_guiseAI(Creature* c) : PassiveAI(c) { }

            void IsSummonedBy(Unit* owner) override
            {
                me->SetLevel(owner->GetLevel());
                me->SetMaxHealth(owner->GetMaxHealth() / 2);
                me->SetHealth(me->GetMaxHealth());

                owner->CastSpell(me, SPELL_PRIEST_INITIALIZE_IMAGES, true);
                owner->CastSpell(me, SPELL_PRIEST_SPECTRAL_GUISE_CLONE, true);

                DoCast(me, SPELL_PRIEST_SPECTRAL_GUISE_CHARGES, true);
                DoCast(owner, SPELL_PRIEST_SPECTRAL_GUISE_STEALTH, true);

                me->SetControlled(true, UNIT_STATE_ROOT);
            }
        };

        CreatureAI* GetAI(Creature *creature) const override
        {
            return new npc_pet_pri_spectral_guiseAI(creature);
        }
};

class npc_pet_pri_void_tentril : public CreatureScript
{
    public:
        npc_pet_pri_void_tentril() : CreatureScript("npc_pet_pri_void_tentril") { }

        struct npc_pet_pri_void_tentrilAI : public ScriptedAI
        {
            npc_pet_pri_void_tentrilAI(Creature* c) : ScriptedAI(c)
            {
                me->SetReactState(REACT_DEFENSIVE);
                SetCombatMovement(false);
                me->SetFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
            }

            void IsSummonedBy(Unit* owner) override
            {
                me->SetFaction(owner->GetFaction());
                me->SetLevel(owner->GetLevel());
                me->SetMaxHealth(CalculatePct(owner->GetMaxHealth(), 2.5f));
                me->SetHealth(me->GetMaxHealth());
            }

            void EnterEvadeMode(EvadeReason /*why*/) override
            {
            }
        };

        CreatureAI* GetAI(Creature *creature) const override
        {
            return new npc_pet_pri_void_tentrilAI(creature);
        }
};

class npc_pet_pri_psyfiend : public CreatureScript
{
    public:
        npc_pet_pri_psyfiend() : CreatureScript("npc_pet_pri_psyfiend") { }

        struct npc_pet_pri_psyfiendAI : public ScriptedAI
        {
            npc_pet_pri_psyfiendAI(Creature* c) : ScriptedAI(c)
            {
                me->SetReactState(REACT_DEFENSIVE);
                SetCombatMovement(false);
                me->SetFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
            }

            void IsSummonedBy(Unit* owner) override
            {
                me->SetFaction(owner->GetFaction());
                me->SetLevel(owner->GetLevel());
                me->SetMaxHealth(CalculatePct(owner->GetMaxHealth(), 2.5f));
                me->SetHealth(me->GetMaxHealth());
            }

            void Reset() override
            {
                _psychicTerrorTimer = 1.5 * IN_MILLISECONDS;
            }

            void EnterEvadeMode(EvadeReason /*why*/) override
            {
            }

            void UpdateAI(uint32 diff) override
            {
                if (_psychicTerrorTimer <= diff)
                {
                    _psychicTerrorTimer = 1.5 * IN_MILLISECONDS;
                    DoCast(SPELL_PRIEST_PSYFIEND_PSYCHIC_TERROR);
                }
                else
                    _psychicTerrorTimer -= diff;
            }

        private:
            uint32 _psychicTerrorTimer;
        };

        CreatureAI* GetAI(Creature *creature) const override
        {
            return new npc_pet_pri_psyfiendAI(creature);
        }
};

void AddSC_priest_pet_scripts()
{
    new npc_pet_pri_lightwell();
    new npc_pet_pri_shadowfiend_or_mindbender();
    new npc_pet_pri_spectral_guise();
    new npc_pet_pri_void_tentril();
    new npc_pet_pri_psyfiend();
}
