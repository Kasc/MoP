/*
* Copyright (C) 2015-2017 Theatre of Dreams <http://www.theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptMgr.h"
#include "Player.h"
#include "Transport.h"

enum DeeprunTramSecurity
{
    MAP_DEEPRUN_TRAM    = 369,

    GO_SUBWAY           = 218203,
    MAX_SUBWAY_COUNT    = 6
};

Position const SubwaysPos[MAX_SUBWAY_COUNT] =
{
    { 4.580650f, 28.20970f, 7.01107f, 1.5708f },
    { 4.528070f, 8.435290f, 7.01107f, 1.5708f },
    { -45.4005f, 2492.790f, 6.98860f, 1.5708f },
    { -45.4007f, 2512.150f, 6.98860f, 1.5708f },
    { -45.3934f, 2472.930f, 6.98860f, 1.5708f },
    { 4.498830f, -11.3475f, 7.01107f, 1.5708f }
};

static G3D::Quat worldRotation = G3D::Quat(0.0f, 0.0f, 0.7071066f, 0.7071069f);
static G3D::Quat parentRotation = G3D::Quat(0.0f, 0.0f, 1.0f, -0.0000000437114f);

class world_map_deeprun_tram_security : public WorldMapScript
{
    public:
        world_map_deeprun_tram_security() : WorldMapScript("world_map_deeprun_tram_security", MAP_DEEPRUN_TRAM)
        {
            _initializedTrams = false;
            _removingTramsTimer = 0;
        }

        void OnPlayerEnter(Map* map, Player* player) override
        {
            _players.insert(player);

            if (_removingTramsTimer)
                _removingTramsTimer = 0;
            else if (!_initializedTrams)
            {
                _initializedTrams = true;

                for (uint8 i = 0; i < MAX_SUBWAY_COUNT; ++i)
                {
                    GameObject* transport = new Transport();

                    if (!transport->Create(GO_SUBWAY + i, map, SubwaysPos[i], worldRotation, 255, GO_STATE_READY))
                    {
                        delete transport;
                        continue;
                    }

                    transport->SetParentRotation(parentRotation);

                    transport->InitModel();

                    uint32 spawnId = sObjectMgr->GenerateGameObjectSpawnId();
                    transport->SetSpawnId(spawnId);

                    transport->SetActive(true);
                    map->AddToMap(transport);

                    _trams.push_back(transport);
                }
            }
        }

        void OnPlayerLeave(Map* map, Player* player) override
        {
            _players.erase(player);

            if (_players.empty())
                _removingTramsTimer = 60 * IN_MILLISECONDS;
        }

        void OnUpdate(Map* map, uint32 diff) override
        {
            if (_initializedTrams && _removingTramsTimer > 0)
            {
                if (_removingTramsTimer <= diff)
                {
                    _initializedTrams = false;
                    _removingTramsTimer = 0;

                    for (GameObject* transport : _trams)
                        map->RemoveFromMap(transport, true);

                    _trams.clear();
                }
                else
                    _removingTramsTimer -= diff;
            }
        }

    private:
        bool _initializedTrams;
        uint32 _removingTramsTimer;
        std::vector<GameObject*> _trams;
        std::unordered_set<Player*> _players;
};

void AddSC_world_maps()
{
    new world_map_deeprun_tram_security();
}
