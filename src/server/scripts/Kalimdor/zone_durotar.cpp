/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedEscortAI.h"
#include "SpellScript.h"
#include "Player.h"
#include "Unit.h"
#include "Vehicle.h"
#include "Creature.h"
#include "Pet.h"
#include "Player.h"
#include "ScriptedGossip.h"

/*######
## Quest 25134: Lazy Peons
## npc_lazy_peon
######*/

enum LazyPeonYells
{
    SAY_SPELL_HIT        = 0
};

enum LazyPeon
{
    QUEST_LAZY_PEONS    = 25134,
    GO_LUMBERPILE       = 175784,
    SPELL_BUFF_SLEEP    = 17743,
    SPELL_AWAKEN_PEON   = 19938
};

class npc_lazy_peon : public CreatureScript
{
public:
    npc_lazy_peon() : CreatureScript("npc_lazy_peon") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_lazy_peonAI(creature);
    }

    struct npc_lazy_peonAI : public ScriptedAI
    {
        npc_lazy_peonAI(Creature* creature) : ScriptedAI(creature)
        {
            Initialize();
        }

        void Initialize()
        {
            RebuffTimer = 0;
            work = false;
        }

        uint32 RebuffTimer;
        bool work;

        void Reset() override
        {
            Initialize();
        }

        void MovementInform(uint32 /*type*/, uint32 id) override
        {
            if (id == 1)
                work = true;
        }

        void SpellHit(Unit* caster, const SpellInfo* spell) override
        {
            if (spell->Id != SPELL_AWAKEN_PEON)
                return;

            Player* player = caster->ToPlayer();
            if (player && player->GetQuestStatus(QUEST_LAZY_PEONS) == QUEST_STATUS_INCOMPLETE)
            {
                player->KilledMonsterCredit(me->GetEntry(), me->GetGUID());
                Talk(SAY_SPELL_HIT, caster);
                me->RemoveAllAuras();
                if (GameObject* Lumberpile = me->FindNearestGameObject(GO_LUMBERPILE, 20))
                    me->GetMotionMaster()->MovePoint(1, Lumberpile->GetPositionX() - 1, Lumberpile->GetPositionY(), Lumberpile->GetPositionZ());
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (work == true)
                me->HandleEmoteCommand(EMOTE_ONESHOT_WORK_CHOPWOOD);
            if (RebuffTimer <= diff)
            {
                DoCast(me, SPELL_BUFF_SLEEP);
                RebuffTimer = 300000; //Rebuff agian in 5 minutes
            }
            else
                RebuffTimer -= diff;
            if (!UpdateVictim())
                return;
            DoMeleeAttackIfReady();
        }
    };
};

enum VoodooSpells
{
    SPELL_BREW      = 16712, // Special Brew
    SPELL_GHOSTLY   = 16713, // Ghostly
    SPELL_HEX1      = 16707, // Hex
    SPELL_HEX2      = 16708, // Hex
    SPELL_HEX3      = 16709, // Hex
    SPELL_GROW      = 16711, // Grow
    SPELL_LAUNCH    = 16716  // Launch (Whee!)
};

// 17009
class spell_voodoo : public SpellScriptLoader
{
public:
    spell_voodoo() : SpellScriptLoader("spell_voodoo") { }

    class spell_voodoo_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_voodoo_SpellScript);

        bool Validate(SpellInfo const* /*spellInfo*/) override
        {
            if (!sSpellMgr->GetSpellInfo(SPELL_BREW) || !sSpellMgr->GetSpellInfo(SPELL_GHOSTLY) ||
                !sSpellMgr->GetSpellInfo(SPELL_HEX1) || !sSpellMgr->GetSpellInfo(SPELL_HEX2) ||
                !sSpellMgr->GetSpellInfo(SPELL_HEX3) || !sSpellMgr->GetSpellInfo(SPELL_GROW) ||
                !sSpellMgr->GetSpellInfo(SPELL_LAUNCH))
                return false;
            return true;
        }

        void HandleDummy(SpellEffIndex /*effIndex*/)
        {
            uint32 spellid = RAND(SPELL_BREW, SPELL_GHOSTLY, RAND(SPELL_HEX1, SPELL_HEX2, SPELL_HEX3), SPELL_GROW, SPELL_LAUNCH);
            if (Unit* target = GetHitUnit())
                GetCaster()->CastSpell(target, spellid, false);
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_voodoo_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_voodoo_SpellScript();
    }
};

/*###########################
ECHO ISLES ##################
###########################*/

enum tikiSpells
{
    SPELL_TIKI_TARGET_DEATH     = 71240,
    SPELL_TIKI_TARGET_VISUAL    = 71064,
    SPELL_TIKI_TARGET_VISUAL_2  = 71065,
    SPELL_TIKI_TARGET_VISUAL_3  = 71066,

    // the arts of [class]
    SPELL_FROST_NOVA            = 122,
    SPELL_CHARGE                = 100,
    SPELL_STEADY_SHOT           = 56641,
    SPELL_MOONFIRE              = 8921,
    SPELL_TIGER_PALM            = 100787,
    SPELL_PRIMAL_STRIKE         = 73899,
    SPELL_EVISCERATE            = 2098,
    SPELL_CORRUPTION            = 172
};

uint32 const KILL_CREDIT_ART    = 44175; // the arts of [class]

class npc_tiki_target : public CreatureScript
{
public:
    npc_tiki_target() : CreatureScript("npc_tiki_target") { }

    struct npc_tiki_targetAI : ScriptedAI
    {
        npc_tiki_targetAI(Creature* creature) : ScriptedAI(creature)
        {
            Initialize();
        }

        void Initialize()
        {
            // 3 different types of masks on tiki targets
            // pick random from those 3 and then cast this spell
            spell = Math::Rand(SPELL_TIKI_TARGET_VISUAL, SPELL_TIKI_TARGET_VISUAL_3);
            DoCast(spell);
            died = false;
        }

        void AttackStart(Unit* target) override { }
        void EnterCombat(Unit* target) override { }

        void Reset() override
        {
            Initialize();
        }

        void EnterEvadeMode(EvadeReason why) override
        {
            if (!_EnterEvadeMode(why))
                return;

            me->RemoveAllAuras();
            Initialize();
        }

        void SpellHit(Unit* caster, const SpellInfo* spell) override
        {
            switch (spell->Id)
            {
                case SPELL_FROST_NOVA:
                case SPELL_CHARGE:
                case SPELL_STEADY_SHOT:
                case SPELL_MOONFIRE:
                case SPELL_TIGER_PALM:
                case SPELL_PRIMAL_STRIKE:
                case SPELL_EVISCERATE:
                case SPELL_CORRUPTION:
                    if (caster->GetTypeId() == TYPEID_PLAYER)
                        caster->ToPlayer()->KilledMonsterCredit(KILL_CREDIT_ART);
                    break;
                default:
                    break;
            }
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            // Darkspear novices constantly attack tiki targets but they're not supposed to deal damage to them
            if (attacker->GetTypeId() != TYPEID_PLAYER)
                damage = 0;

            if (me->HealthBelowPctDamaged(5, damage) && !died)
            {
                damage = 0;
                died = true;
                // remove tiki target masks
                DoCast(SPELL_TIKI_TARGET_DEATH);
                me->RemoveAllAuras();
                // those should spawn 5-10 secs after being 'killed' and its handled in creature table
                // but it also adds corpse despawn time and in this case it shouldnt
                me->SetCorpseDelay(0);
                me->DespawnOrUnsummon(1500);
                // might be a spell? recheck when spellwork is complete
                if (attacker->GetTypeId() == TYPEID_PLAYER)
                    attacker->ToPlayer()->KilledMonsterCredit(me->GetEntry());
            }
        }
    private:
        bool died;
        uint32 spell;
    };

    CreatureAI *GetAI(Creature* creature) const override
    {
        return new npc_tiki_targetAI(creature);
    }
};

enum provingEntries
{
    NPC_JAILOR                  = 39062,
    DARKSPEAR_CAGE              = 201968,
    NPC_NEKALI                  = 38242,
    NPC_SPITESCALE_SCOUT        = 38142,

    GOSSIP_WHILE_EVENT_ACTIVE   = 31024,
    GOSSIP_WHILE_EVENT_INACTIVE = 15251,
    SAY_1                       = 0, // Get in the pit and show us your stuff, boy/girl
    SAY_2                       = 0  // different creature than SAY_1, Spitescale scout in this case
};

enum provingPitQuests
{
    PROVING_PIT_SHAMAN          = 24762,
    PROVING_PIT_WARRIOR         = 24642,
    PROVING_PIT_DRUID           = 24768,
    PROVING_PIT_HUNTER          = 24780,
    PROVING_PIT_MAGE            = 24754,
    PROVING_PIT_WARLOCK         = 26276,
    PROVING_PIT_MONK            = 31161,
    PROVING_PIT_ROGUE           = 24774,
    PROVING_PIT_PRIEST          = 24786
};

enum pitEvents
{
    EVENT_START_MOVING          = 1,
    EVENT_OPEN_CAGE             = 2,
    EVENT_CLOSE_CAGE            = 3,
    EVENT_MOVE_2                = 4,
    EVENT_MOVE_3                = 5,
    EVENT_FINISH_EVENT          = 6  // only in case when scout is still alive after 60 seconds
};

const Position firstJailorMovepos[4] =
{
    { -1136.29f, -5428.75f, 13.65f, 0.0f },
    { -1136.33f, -5417.20f, 13.26f, 0.0f },
    { -1136.92f, -5429.74f, 13.66f, 0.0f },
    { -1143.16f, -5429.67f, 13.86f, 0.0f }
};

const Position secondJailorMovepos[4] =
{
    { -1159.44f, -5518.32f, 12.18f, 0.0f },
    { -1152.93f, -5518.66f, 11.99f, 0.0f },
    { -1159.44f, -5518.32f, 12.18f, 0.0f },
    { -1159.16f, -5530.16f, 11.95f, 0.0f }
};

class npc_jailor_proving_pit : public CreatureScript
{
public:
    npc_jailor_proving_pit() : CreatureScript("npc_jailor_proving_pit") { }

    struct npc_jailor_proving_pitAI : public ScriptedAI
    {
        npc_jailor_proving_pitAI(Creature* creature) : ScriptedAI(creature)
        {
            Initialize();
        }

        void Initialize()
        {
            eventActive = false;
            cageGUID.Clear();
            scoutscaleGUID.Clear();
            dbGUID = me->GetSpawnId();
        }

        void SetData(uint32 type, uint32 value) override
        {
            if (type == 4 && value == 4)
            {
                eventActive = false;
                if (Creature* scout = ObjectAccessor::GetCreature(*me, scoutscaleGUID))
                {
                    scout->SetCorpseDelay(0);
                    scout->SetRespawnDelay(10);
                }
            }
        }

        bool GossipHello(Player* player) override
        {
            if (!eventActive &&
                player->GetQuestStatus(PROVING_PIT_SHAMAN) == QUEST_STATUS_INCOMPLETE   ||
                player->GetQuestStatus(PROVING_PIT_WARRIOR) == QUEST_STATUS_INCOMPLETE  ||
                player->GetQuestStatus(PROVING_PIT_DRUID) == QUEST_STATUS_INCOMPLETE    ||
                player->GetQuestStatus(PROVING_PIT_HUNTER) == QUEST_STATUS_INCOMPLETE   ||
                player->GetQuestStatus(PROVING_PIT_MAGE) == QUEST_STATUS_INCOMPLETE     ||
                player->GetQuestStatus(PROVING_PIT_WARLOCK) == QUEST_STATUS_INCOMPLETE  ||
                player->GetQuestStatus(PROVING_PIT_MONK) == QUEST_STATUS_INCOMPLETE     ||
                player->GetQuestStatus(PROVING_PIT_ROGUE) == QUEST_STATUS_INCOMPLETE    ||
                player->GetQuestStatus(PROVING_PIT_PRIEST) == QUEST_STATUS_INCOMPLETE)
                AddGossipItemFor(player, 10974, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);

            SendGossipMenuFor(player, eventActive ? GOSSIP_WHILE_EVENT_ACTIVE : GOSSIP_WHILE_EVENT_INACTIVE, me->GetGUID());

            return true;
        }

        bool GossipSelect(Player* player, uint32 sender, uint32 action) override
        {
            if (!eventActive)
            {
                eventActive = true;
                player->KilledMonsterCredit(me->GetEntry());
                events.ScheduleEvent(EVENT_START_MOVING, 1000);
                Talk(SAY_1, player);
                CloseGossipMenuFor(player);

                return true;
            }

            return false;
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type != POINT_MOTION_TYPE)
                return;

            switch (id)
            {
                case 1:
                    events.ScheduleEvent(EVENT_MOVE_2, 100);
                    break;
                case 2:
                    events.ScheduleEvent(EVENT_OPEN_CAGE, 5000);
                    break;
                case 3:
                    events.ScheduleEvent(EVENT_MOVE_3, 100);
                    break;
                default:
                    break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_START_MOVING:
                        if (dbGUID == 309155)
                            me->GetMotionMaster()->MovePoint(1, firstJailorMovepos[0]);
                        else
                            me->GetMotionMaster()->MovePoint(1, secondJailorMovepos[0]);
                        break;
                    case EVENT_OPEN_CAGE:
                        // Rewrite when we implement zone scripts
                        if (GameObject* cage = me->FindNearestGameObject(DARKSPEAR_CAGE, 50.0f))
                        {
                            cageGUID = cage->GetGUID();
                            cage->SetGoState(GO_STATE_ACTIVE);
                        }

                        if (Creature* scout = me->FindNearestCreature(NPC_SPITESCALE_SCOUT, 50.0f))
                        {
                            scoutscaleGUID = scout->GetGUID();

                            float x, y, z;
                            scout->GetClosePoint(x, y, z, scout->GetCombatReach() / 3, 7.0f);
                            scout->GetMotionMaster()->MovePoint(10, x, y, z);
                        }

                        if (dbGUID == 309155)
                            me->GetMotionMaster()->MovePoint(3, firstJailorMovepos[3]);
                        else
                            me->GetMotionMaster()->MovePoint(3, secondJailorMovepos[3]);

                        events.ScheduleEvent(EVENT_FINISH_EVENT, 60000);
                        events.ScheduleEvent(EVENT_CLOSE_CAGE, 4000);
                        break;
                    case EVENT_CLOSE_CAGE:
                        if (GameObject* cage = ObjectAccessor::GetGameObject(*me, cageGUID))
                            cage->SetGoState(GO_STATE_READY);

                        if (Creature* scout = ObjectAccessor::GetCreature(*me, scoutscaleGUID))
                        {
                            scout->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
                            scout->AI()->Talk(SAY_2);
                        }
                        break;
                    case EVENT_MOVE_2:
                        if (dbGUID == 309155)
                            me->GetMotionMaster()->MovePoint(2, firstJailorMovepos[1]);
                        else
                            me->GetMotionMaster()->MovePoint(2, secondJailorMovepos[1]);
                        break;
                    case EVENT_MOVE_3:
                        me->GetMotionMaster()->MoveTargetedHome();
                        break;
                    case EVENT_FINISH_EVENT:
                        if (Creature* scout = ObjectAccessor::GetCreature(*me, scoutscaleGUID))
                            if (scout->IsAlive())
                            {
                                eventActive = false;
                                scout->DisappearAndDie();
                            }
                        break;
                    default:
                        break;
                }
            }
        }
    private:
        EventMap events;
        bool eventActive;
        ObjectGuid cageGUID;
        ObjectGuid scoutscaleGUID;
        uint32 dbGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_jailor_proving_pitAI(creature);
    }
};

enum voljinSay
{
    VOLJIN_SAY_1                = 0, // I have sometin' to show ya. It be easier to understand if ya see it yourself.
    VOLJIN_SAY_2                = 1, // Tha Darkspear are 'ere because I led dem here.  Orgrimmar be no home as long as it be under Hellscream's hand.
    VOLJIN_SAY_3                = 2, // Still, I fear I was lettin' my temper drive me ta bein' rash. Thrall devoted himself to makin' the Horde what it is, so I've no eagerness to be leavin' it on a whim.  Dis will be needin' much more thought.
    VOLJIN_SAY_4                = 3, // But dese be worries for older minds.  Ya still have much to learn.  Go help tha people of tha Darkspear.  I am sure we'll be speakin' again real soon.
    VOLJIN_SAY_5                = 4, // Tha Sea Witch is dead, our home is reclaimed, but our future still be uncertain.
    VOLJIN_SAY_6                = 5, // I've no love for Garrosh, dat much is sure, but leavin' tha Horde is not a decision I be takin' lightly.
    VOLJIN_SAY_7                = 6, // Dere's only one with tha answers I seek. Ya can stay and watch if ya like.
    VOLJIN_SAY_8                = 7, // Thrall!  I am glad ya be well.  Dere were rumors otherwise.
    VOLJIN_SAY_9                = 8, // I must beg ya council, my friend.  I can't be standin' by Garrosh while he be turnin' our people against each other for tha sake of war.  My respect for you does not extend to dis new Horde... I am tinkin' of leadin' my people away.
    VOLJIN_SAY_10               = 9, // I understand, brotha. I will tink on this and be troublin' ya no furtha. You have a world to be savin'.
    VOLJIN_SAY_11               = 10, // Ya be strong and proud, youngblood.  Tha Darkspear will be honored ta have you fight beside d'em.
    VOLJIN_SAY_12               = 11, // Dese be dark times, but our people will be stayin' with tha Horde for tha good of all.
    VOLJIN_SAY_13               = 12, // Thrall's words be true. Dey always be. The Horde is much more den a few old, stubborn leaders and a handful of heroes from Northrend. The people be cryin' Garrosh's name... at least for now.
    VOLJIN_SAY_14               = 13, // Still, I be hopin' Thrall will return to us one day. Tha future right now be lookin' very grim... and very bloody.
    VOLJIN_SAY_15               = 14, // Go now. Make tha Darkspear proud. Dere are many wars ahead of us, an' I'm sure ya be havin' a part to play in all of dem.

    THRALL_SAY_0                = 0, // Vol'jin.  It's good to see you, brother.
    THRALL_SAY_1                = 1, // Indeed.  Someone did try to kill me, but that is not my greatest concern at this moment.  The world itself calls for my aid.
    THRALL_SAY_2                = 2, // Vol'jin, I chose Garrosh because he has the strength to lead our people in these trying times.
    THRALL_SAY_3                = 3, // For all my supposed wisdom, there have been moments that I've barely been able to hold the Horde together. The Wrath Gate and Undercity displayed that clearly enough.
    THRALL_SAY_4                = 4, // The Horde cries for a hero of old. An orc of true blood that will bow to no human and bear no betrayal. A warrior that will make our people proud again. Garrosh can be that hero.
    THRALL_SAY_5                = 5, // I did not make this decision lightly, Vol'jin. I know our alliances will suffer for it. I know the Horde will be irreversibly changed. But I made this choice with confidence that Garrosh is exactly what the Horde needs.
    THRALL_SAY_6                = 6, // I'm trusting you and the other leaders to not let this divide our people. You are stronger than that.
    THRALL_SAY_7                = 7  // Throm'ka, old friend.
};

enum voljinEvents
{
    // more than expected
    EVENT_SCENE_1               = 1,
    EVENT_SCENE_2               = 2,
    EVENT_SCENE_3               = 3,
    EVENT_SCENE_4               = 4,
    EVENT_SCENE_5               = 5,

    // an ancient enemy
    EVENT_ANC_SCENE_1           = 6,
    EVENT_ANC_SCENE_2           = 7,
    EVENT_ANC_SCENE_3           = 8,
    EVENT_ANC_SCENE_4           = 9,
    EVENT_ANC_SCENE_5           = 10,
    EVENT_ANC_SCENE_6           = 11,
    EVENT_ANC_SCENE_7           = 12,
    EVENT_ANC_SCENE_8           = 13,
    EVENT_ANC_SCENE_9           = 14,
    EVENT_ANC_SCENE_10          = 15,
    EVENT_ANC_SCENE_11          = 16,
    EVENT_ANC_SCENE_12          = 17,
    EVENT_ANC_SCENE_13          = 18,
    EVENT_ANC_SCENE_14          = 19,
    EVENT_ANC_SCENE_15          = 20,
    EVENT_ANC_SCENE_16          = 21,
    EVENT_ANC_SCENE_17          = 22,
    EVENT_ANC_SCENE_18          = 23,
    EVENT_ANC_SCENE_19          = 24,
    EVENT_ANC_SCENE_20          = 25,
    EVENT_ANC_SCENE_21          = 26
};

enum voljinMisc
{
    SPELL_RITE_OF_VISION        = 73169,
    SPELL_SMOKE                 = 73158,

    NPC_VISION_OF_GARROSH       = 38938,
    NPC_VISION_OF_VOLJIN        = 38953,
    NPC_ECHO_ISLES_BUNNY        = 38003,
    GO_VISION_BRAZIER           = 202444,
    NPC_VISION_OF_THRALL        = 38939,

    QUEST_MORE_THAN_EXPECTED    = 24763,
    QUEST_MORE_THAN_EXPECTED2   = 24755,
    QUEST_MORE_THAN_EXPECTED3   = 24775,
    QUEST_MORE_THAN_EXPECTED4   = 24781,
    QUEST_MORE_THAN_EXPECTED5   = 24787,
    QUEST_MORE_THAN_EXPECTED6   = 31163,
    QUEST_MORE_THAN_EXPECTED7   = 24643,
    QUEST_MORE_THAN_EXPECTED8   = 24769,
    QUEST_MORE_THAN_EXPECTED9   = 26277,
    QUEST_ANCIENT_ENEMY         = 24814,

    ACTION_SCENE_GARROSH        = 1,
    ACTION_SCENE_THRALL         = 2
};

class npc_voljin_echo_isles : public CreatureScript
{
public:
    npc_voljin_echo_isles() : CreatureScript("npc_voljin_echo_isles") { }

    struct npc_voljin_echo_islesAI : public ScriptedAI
    {
        npc_voljin_echo_islesAI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            eventIsActive = false;
            events.Reset();
        }

        void QuestReward(Player* player, Quest const* quest, uint32 opt) override
        {
            if (eventIsActive)
                return;

            switch (quest->ID)
            {
                case QUEST_MORE_THAN_EXPECTED:
                case QUEST_MORE_THAN_EXPECTED2:
                case QUEST_MORE_THAN_EXPECTED3:
                case QUEST_MORE_THAN_EXPECTED4:
                case QUEST_MORE_THAN_EXPECTED5:
                case QUEST_MORE_THAN_EXPECTED6:
                case QUEST_MORE_THAN_EXPECTED7:
                case QUEST_MORE_THAN_EXPECTED8:
                case QUEST_MORE_THAN_EXPECTED9:
                    eventIsActive = true;
                    Talk(VOLJIN_SAY_1);
                    events.ScheduleEvent(EVENT_SCENE_1, 1000);
                    DoCast(SPELL_RITE_OF_VISION);
                    break;
                case QUEST_ANCIENT_ENEMY:
                    eventIsActive = true;
                    Talk(VOLJIN_SAY_5);
                    events.ScheduleEvent(EVENT_ANC_SCENE_1, 8000);
                    break;
                default:
                    break;
            }
        }

        void SetData(uint32 type, uint32 value) override
        {
            if (type == 2 && value == 2)
            {
                me->RemoveAurasDueToSpell(SPELL_RITE_OF_VISION);
                events.ScheduleEvent(EVENT_SCENE_2, 1000);
            }
        }

        void DoAction(int32 param) override
        {
            if (eventIsActive)
                return;

            switch (param)
            {
                case ACTION_SCENE_GARROSH:
                    eventIsActive = true;
                    Talk(VOLJIN_SAY_1);
                    events.ScheduleEvent(EVENT_SCENE_1, 1000);
                    DoCast(SPELL_RITE_OF_VISION);
                    break;
                case ACTION_SCENE_THRALL:
                    eventIsActive = true;
                    Talk(VOLJIN_SAY_5);
                    events.ScheduleEvent(EVENT_ANC_SCENE_1, 8000);
                    break;
                default:
                    break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_SCENE_1:
                        if (Creature* garrosh = me->SummonCreature(NPC_VISION_OF_GARROSH, -1321.64f, -5610.25f, 25.4541f, 2.46091f))
                            garroshGUID = garrosh->GetGUID();

                        if (Creature* voljin = me->SummonCreature(NPC_VISION_OF_VOLJIN, -1323.86f, -5608.56f, 25.4523f, 5.48033f))
                            voljinGUID = voljin->GetGUID();

                        if (GameObject* go = me->FindNearestGameObject(GO_VISION_BRAZIER, 30.0f))
                        {
                            go->UseDoorOrButton();
                            brazierGUID = go->GetGUID();
                        }

                        if (Creature* bunny = me->FindNearestCreature(NPC_ECHO_ISLES_BUNNY, 15.0f))
                        {
                            bunny->CastSpell(bunny, SPELL_SMOKE, TRIGGERED_FULL_MASK);
                            bunnyGUID = bunny->GetGUID();
                        }
                        me->SetFacingTo(4.05f);
                        break;
                    case EVENT_SCENE_2:
                        Talk(VOLJIN_SAY_2);
                        me->SetFacingTo(0.82f);
                        events.ScheduleEvent(EVENT_SCENE_3, 9000);
                        break;
                    case EVENT_SCENE_3:
                        Talk(VOLJIN_SAY_3);
                        events.ScheduleEvent(EVENT_SCENE_4, 16000);
                        break;
                    case EVENT_SCENE_4:
                        Talk(VOLJIN_SAY_4);

                        if (Creature* visionGarrosh = ObjectAccessor::GetCreature(*me, garroshGUID))
                            visionGarrosh->DespawnOrUnsummon();

                        if (Creature* visionVoljin = ObjectAccessor::GetCreature(*me, voljinGUID))
                            visionVoljin->DespawnOrUnsummon();

                        if (Creature* bunny = ObjectAccessor::GetCreature(*me, bunnyGUID))
                            bunny->RemoveAurasDueToSpell(SPELL_SMOKE);

                        if (GameObject* brazier = ObjectAccessor::GetGameObject(*me, brazierGUID))
                            brazier->ResetDoorOrButton();
                        events.ScheduleEvent(EVENT_SCENE_5, 8000);
                        break;
                    case EVENT_SCENE_5:
                        Reset();
                        break;
                    case EVENT_ANC_SCENE_1:
                        Talk(VOLJIN_SAY_6);

                        events.ScheduleEvent(EVENT_ANC_SCENE_2, 8000);
                        break;
                    case EVENT_ANC_SCENE_2:
                        Talk(VOLJIN_SAY_7);

                        events.ScheduleEvent(EVENT_ANC_SCENE_3, 5000);
                        break;
                    case EVENT_ANC_SCENE_3:
                        DoCast(SPELL_RITE_OF_VISION);
                        if (GameObject* go = me->FindNearestGameObject(GO_VISION_BRAZIER, 30.0f))
                        {
                            go->UseDoorOrButton();
                            brazierGUID = go->GetGUID();
                        }

                        if (Creature* bunny = me->FindNearestCreature(NPC_ECHO_ISLES_BUNNY, 15.0f))
                        {
                            bunny->CastSpell(bunny, SPELL_SMOKE, TRIGGERED_FULL_MASK);
                            bunnyGUID = bunny->GetGUID();
                        }
                        me->SetFacingTo(4.05f);
                        if (Creature* thrall = me->SummonCreature(NPC_VISION_OF_THRALL, -1323.307f, -5609.903f, 25.30504f, 0.9250245f))
                            thrallGUID = thrall->GetGUID();
                        events.ScheduleEvent(EVENT_ANC_SCENE_4, 2000);
                        break;
                    case EVENT_ANC_SCENE_4:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_0);

                        events.ScheduleEvent(EVENT_ANC_SCENE_6, 4000);
                        break;
                    case EVENT_ANC_SCENE_6:
                        Talk(VOLJIN_SAY_8);

                        events.ScheduleEvent(EVENT_ANC_SCENE_7, 6000);
                        break;
                    case EVENT_ANC_SCENE_7:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_1);

                        events.ScheduleEvent(EVENT_ANC_SCENE_8, 12000);
                        break;
                    case EVENT_ANC_SCENE_8:
                        Talk(VOLJIN_SAY_9);

                        events.ScheduleEvent(EVENT_ANC_SCENE_9, 18000);
                        break;
                    case EVENT_ANC_SCENE_9:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_2);

                        events.ScheduleEvent(EVENT_ANC_SCENE_10, 7000);
                        break;
                    case EVENT_ANC_SCENE_10:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_3);

                        events.ScheduleEvent(EVENT_ANC_SCENE_11, 12000);
                        break;
                    case EVENT_ANC_SCENE_11:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_4);

                        events.ScheduleEvent(EVENT_ANC_SCENE_12, 18000);
                        break;
                    case EVENT_ANC_SCENE_12:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_5);

                        events.ScheduleEvent(EVENT_ANC_SCENE_13, 17000);
                        break;
                    case EVENT_ANC_SCENE_13:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_6);

                        events.ScheduleEvent(EVENT_ANC_SCENE_14, 9000);
                        break;
                    case EVENT_ANC_SCENE_14:
                        Talk(VOLJIN_SAY_10);

                        events.ScheduleEvent(EVENT_ANC_SCENE_15, 10000);
                        break;
                    case EVENT_ANC_SCENE_15:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->AI()->Talk(THRALL_SAY_7);

                        events.ScheduleEvent(EVENT_ANC_SCENE_16, 5000);
                        break;
                    case EVENT_ANC_SCENE_16:
                        if (Creature* thrall = ObjectAccessor::GetCreature(*me, thrallGUID))
                            thrall->DespawnOrUnsummon();

                        if (Creature* bunny = ObjectAccessor::GetCreature(*me, bunnyGUID))
                            bunny->RemoveAurasDueToSpell(SPELL_SMOKE);

                        if (GameObject* brazier = ObjectAccessor::GetGameObject(*me, brazierGUID))
                            brazier->ResetDoorOrButton();

                        Talk(VOLJIN_SAY_11);
                        me->RemoveAurasDueToSpell(SPELL_RITE_OF_VISION);
                        me->SetFacingTo(0.82f);
                        events.ScheduleEvent(EVENT_ANC_SCENE_17, 8000);
                        break;
                    case EVENT_ANC_SCENE_17:
                        Talk(VOLJIN_SAY_12);

                        events.ScheduleEvent(EVENT_ANC_SCENE_18, 9000);
                        break;
                    case EVENT_ANC_SCENE_18:
                        Talk(VOLJIN_SAY_13);

                        events.ScheduleEvent(EVENT_ANC_SCENE_19, 18000);
                        break;
                    case EVENT_ANC_SCENE_19:
                        Talk(VOLJIN_SAY_14);

                        events.ScheduleEvent(EVENT_ANC_SCENE_20, 10000);
                        break;
                    case EVENT_ANC_SCENE_20:
                        Talk(VOLJIN_SAY_15);

                        events.ScheduleEvent(EVENT_ANC_SCENE_21, 13000);
                        break;
                    case EVENT_ANC_SCENE_21:
                        Reset();
                        break;
                    default:
                        break;
                }
            }
        }

        bool GossipHello(Player* player) override
        {
            // menu_id voljin 11126 for cutscene more than expected
            // menu_id voljin 11127 for cutscene after an ancient enemy
            if (player->GetQuestStatus(QUEST_ANCIENT_ENEMY) != QUEST_STATUS_REWARDED &&
                (player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED2) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED3) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED4) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED5) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED6) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED7) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED8) == QUEST_STATUS_REWARDED ||
                    player->GetQuestStatus(QUEST_MORE_THAN_EXPECTED9) == QUEST_STATUS_REWARDED))
                AddGossipItemFor(player, 11126, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
            else if (player->GetQuestStatus(QUEST_ANCIENT_ENEMY) == QUEST_STATUS_REWARDED)
                AddGossipItemFor(player, 11127, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);

            SendGossipMenuFor(player, 15468, me->GetGUID());

            return true;
        }

        bool GossipSelect(Player* player, uint32 /*sender*/, uint32 action) override
        {
            switch (action)
            {
                case GOSSIP_ACTION_INFO_DEF + 1:
                    DoAction(ACTION_SCENE_GARROSH);
                    CloseGossipMenuFor(player);
                    break;
                case GOSSIP_ACTION_INFO_DEF + 2:
                    DoAction(ACTION_SCENE_THRALL);
                    CloseGossipMenuFor(player);
                    break;
                default:
                    break;
            }

            return true;
        }

    private:
        EventMap events;
        bool eventIsActive;
        ObjectGuid garroshGUID; // vision of garrosh
        ObjectGuid voljinGUID; // vision of voljin
        ObjectGuid thrallGUID; // vision of thrall
        ObjectGuid brazierGUID;
        ObjectGuid bunnyGUID; // echo isles bunny
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_voljin_echo_islesAI(creature);
    }
};

enum zuniBridge
{
    NPC_ECHO_ISLES_BUNNY_B  = 38003, // bunnies on the bridge
    BUNNY_GUID_1            = 309249,
    BUNNY_GUID_2            = 309251,
};

enum zuniSay
{
    SAY_ZUNI_1              = 0,
    SAY_ZUNI_2              = 1,
    SAY_ZUNI_3              = 2,
    SAY_ZUNI_4              = 3,
};

class npc_zuni_bridge : public CreatureScript
{
public:
    npc_zuni_bridge() : CreatureScript("npc_zuni_bridge") { }

    struct npc_zuni_bridgeAI : public ScriptedAI
    {
        npc_zuni_bridgeAI(Creature* creature) : ScriptedAI(creature)
        {
            found1 = false, found2 = false;
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetTypeId() == TYPEID_PLAYER)
            {
                Talk(SAY_ZUNI_1);
                me->GetMotionMaster()->MoveFollow(summoner, 1.0f, 0.0f);
            }
        }

        void SetData(uint32 type, uint32 value) override
        {
            if (type == 1 && value == 1)
            {
                me->SetFacingTo(5.00f);
                Talk(SAY_ZUNI_4);
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type != POINT_MOTION_TYPE)
                return;

            if (pointId == 1)
                me->DespawnOrUnsummon(40000);
        }

        void MoveInLineOfSight(Unit* who) override
        {
            if (who->GetEntry() == NPC_ECHO_ISLES_BUNNY_B && who->IsWithinDist(me, 5.0f))
            {
                switch (who->ToCreature()->GetSpawnId())
                {
                    case BUNNY_GUID_1:
                        if (!found1)
                        {
                            found1 = true;
                            Talk(SAY_ZUNI_2);
                        }
                        break;
                    case BUNNY_GUID_2:
                        if (!found2)
                        {
                            found2 = true;
                            Talk(SAY_ZUNI_3);
                            me->GetMotionMaster()->Clear();
                            me->GetMotionMaster()->MovePoint(1, -1551.98f, -5293.17f, 9.00f);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    private:
        bool found1, found2;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_zuni_bridgeAI(creature);
    }
};

enum viciousSpells
{
    SPELL_LASSO_CREDIT          = 70943,
    SPELL_AT_FINISH_CREDIT      = 70941,
    SPELL_RAPTOR_ROPE           = 70927,
    SPELL_ROPE_THROW            = 70918,
    SPELL_ROPE_TIGTHEN          = 70919,
    SPELL_RIDE_NAMED_RAPTOR     = 70925,
    SPELL_EXIT_VEHICLE          = 50630
};

enum viciousMisc
{
    NPC_SWIFTCLAW_38002         = 38002,
    QUEST_YOUNG_AND_VICIOUS     = 24626,
    BOSS_EMOTE_SWIFCLAW         = 0
};

enum viciousEvents
{
    EVENT_CHECK                 = 1,
    EVENT_BOARD                 = 2
};

class npc_swiftclaw_38002 : public CreatureScript
{
public:
    npc_swiftclaw_38002() : CreatureScript("npc_swiftclaw_38002") { }

    struct npc_swiftclaw_38002AI : ScriptedAI
    {
        npc_swiftclaw_38002AI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            me->SetWalk(true);
            me->SetSpeed(MOVE_WALK, 4.0f);
            me->SetReactState(REACT_PASSIVE);
        }

        void OnCharmed(bool /*apply*/) override { }

        void IsSummonedBy(Unit* summoner) override
        {
            boardingPlayerGUID = summoner->GetGUID();
            DoCast(summoner, SPELL_ROPE_THROW, true);
            DoCast(summoner, SPELL_ROPE_TIGTHEN, true);
            DoCast(summoner, SPELL_LASSO_CREDIT, true);
            events.ScheduleEvent(EVENT_BOARD, 1500);
            events.ScheduleEvent(EVENT_CHECK, 4000);
        }

        void PassengerBoarded(Unit* passenger, int8 /*seat*/, bool apply) override
        {
            if (!apply)
                return;

            Talk(BOSS_EMOTE_SWIFCLAW);

            if (Unit* player = ObjectAccessor::GetPlayer(*me, boardingPlayerGUID))
            {
                player->RemoveAurasDueToSpell(SPELL_ROPE_THROW);
                player->RemoveAurasDueToSpell(SPELL_ROPE_TIGTHEN);
            }
        }

        bool AreaAndWaterChecker()
        {
            if (me->IsInWater())
                return true;

            if (me->GetAreaId() != 4863 && me->GetAreaId() != 6453 && me->GetAreaId() != 4875)
                return true;

            return false;
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_BOARD:
                        if (Unit* player = ObjectAccessor::GetPlayer(*me, boardingPlayerGUID))
                            player->CastSpell(me, SPELL_RIDE_NAMED_RAPTOR);
                        break;
                    case EVENT_CHECK:
                        if (AreaAndWaterChecker())
                        {
                            DoCast(SPELL_EXIT_VEHICLE);
                            me->DespawnOrUnsummon(2000);
                        }
                        events.ScheduleEvent(EVENT_CHECK, 2000);
                        break;
                    default:
                        break;
                }
            }
        }
    private:
        EventMap events;
        ObjectGuid boardingPlayerGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_swiftclaw_38002AI(creature);
    }
};

class npc_swiftclaw_37989 : public CreatureScript
{
public:
    npc_swiftclaw_37989() : CreatureScript("npc_swiftclaw_37989") { }

    struct npc_swiftclaw_37989AI : ScriptedAI
    {
        npc_swiftclaw_37989AI(Creature* creature) : ScriptedAI(creature)
        {
            Initialize();
        }

        void Initialize()
        {
            me->GetMotionMaster()->MovePath(me->GetEntry() * 10, true);
        }

        void SpellHit(Unit* caster, SpellInfo const* spell) override
        {
            if (spell->Id == SPELL_RAPTOR_ROPE)
            {
                me->GetMotionMaster()->MoveIdle();
                caster->SummonCreature(NPC_SWIFTCLAW_38002, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ() + 1.5f, me->GetOrientation());
                me->DespawnOrUnsummon(1);
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_swiftclaw_37989AI(creature);
    }
};

enum thrasherMisc
{
    SPELL_RIDE_VEHICLE          = 46598
};

G3D::Vector3 const thrasherMovePath[5] = // sniff
{
    { -1312.705f, -5559.656f, 21.17837f },
    { -1291.705f, -5562.156f, 21.42837f },
    { -1277.705f, -5571.906f, 21.17837f },
    { -1266.705f, -5583.656f, 20.92837f },
    { -1251.205f, -5587.906f, 20.92837f },
};

G3D::Vector3 const secondThrasherMovePath[19] = // sniff
{
    { -1179.94f, -5636.813f, 14.80129f  },
    { -1136.19f, -5617.654f, 14.87229f  },
    { -1105.19f, -5619.904f, 14.62229f  },
    { -1081.19f, -5627.654f, 14.62229f  },
    { -1058.44f, -5646.404f, 15.37229f  },
    { -1040.44f, -5645.154f, 14.37229f  },
    { -1024.19f, -5631.404f, 14.87229f  },
    { -1017.44f, -5605.154f, 15.12229f  },
    { -1004.94f, -5587.404f, 14.62229f  },
    { -988.69f,  -5578.404f, 15.12229f  },
    { -967.44f,  -5571.404f, 14.37229f  },
    { -935.93f,  -5559.94f,  0.33f      },
    { -927.441f, -5542.754f, -1.131686f },
    { -833.2631f, -5539.315f, -0.77475f },
    { -821.0131f, -5548.815f, 1.475255f },
    { -816.7631f, -5550.315f, 1.475255f },
    { -815.5131f, -5551.065f, 1.975255f },
    { -812.7631f, -5551.815f, 3.725255f },
    { -812.7631f, -5551.815f, 3.725255f },
};

const Position jumpPosition = { -1217.768f, -5601.535f, 14.37584f };

class npc_bloodtalon_thrasher_vehicle : public CreatureScript
{
public:
    npc_bloodtalon_thrasher_vehicle() : CreatureScript("npc_bloodtalon_thrasher_vehicle") { }

    struct npc_bloodtalon_thrasher_vehicleAI : npc_escortAI
    {
        npc_bloodtalon_thrasher_vehicleAI(Creature* creature) : npc_escortAI(creature)
        {
            for (uint32 i = 0; i < 5; ++i)
                AddWaypoint(i, thrasherMovePath[i].x, thrasherMovePath[i].y, thrasherMovePath[i].z);

            Start(false, true);

            SetDespawnAtEnd(false);
            SetDespawnAtFar(false);

            Initialize();
        }

        void Initialize()
        {
            me->SetReactState(REACT_PASSIVE);
        }

        void OnCharmed(bool /*apply*/) override { }

        void WaypointReached(uint32 id) override
        {
            switch (id)
            {
                case 5:
                    me->GetMotionMaster()->MoveJump(jumpPosition, 10.0f, 27.44744f);
                    break;
                case 23:
                    DoCast(SPELL_EXIT_VEHICLE);
                    break;
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == EFFECT_MOTION_TYPE)
            {
                for (uint32 i = 0; i < 19; ++i)
                    AddWaypoint(i + 5, thrasherMovePath[i].x, thrasherMovePath[i].y, thrasherMovePath[i].z);

                Start(false, true);
            }
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetTypeId() == TYPEID_PLAYER)
                DoCast(summoner, SPELL_RIDE_VEHICLE);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_bloodtalon_thrasher_vehicleAI(creature);
    }
};

enum shoreEntries
{
    NPC_DARKSPEAR_SHAMAN        = 38326,
    NPC_DARKSPEAR_TRIBESMAN     = 38324,
    NPC_NAGA_SIREN              = 38301,
    NPC_NAGA_WAVETHRASHER       = 38300,
    NPC_SPITESCALE_BUNNY        = 38560,
    NPC_ZUNI_CAVE               = 38932
};

enum shoreSpells
{
    // shaman
    SPELL_SH_CHAIN_HEAL         = 72014,
    SPELL_SH_LIGHT_BOLT         = 73212,
    // Siren
    SPELL_SIREN_SHOOT           = 6660,
    SPELL_SIREN_CHAIN_LIGHTNING = 15117,
    SPELL_SIREN_WATER_BOLT      = 32011,
    // Wavethrasher
    SPELL_THRASHER_FROST_CLEAVE = 79810
};

enum shoreEvents
{
    EVENT_CHAIN_HEAL            = 1,
    EVENT_LIGHTNING_BOLT        = 2,

    EVENT_CHAIN_LIGHTNING       = 3,
    EVENT_WATER_BOLT            = 4,
    EVENT_SHOOT                 = 5,

    EVENT_FROST_CLEAVE          = 6
};

class npc_at_echo_shore_fighters : public CreatureScript
{
public:
    npc_at_echo_shore_fighters() : CreatureScript("npc_at_echo_shore_fighters") { }

    struct npc_at_echo_shore_fightersAI : ScriptedAI
    {
        npc_at_echo_shore_fightersAI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            // call any motionmaster to trigger MoveInLineOfSight, otherwise they will stay in one place after load and never attack anything
            me->GetMotionMaster()->MoveTargetedHome();
            events.Reset();
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (attacker->GetTypeId() != TYPEID_PLAYER && me->HealthBelowPctDamaged(Math::Rand(50, 85), damage) && attacker->GetEntry() != NPC_ZUNI_CAVE)
                damage = 0;
        }

        void EnterCombat(Unit* attacker) override
        {
            switch (me->GetEntry())
            {
                case NPC_DARKSPEAR_SHAMAN:
                    events.ScheduleEvent(EVENT_CHAIN_HEAL, 8000);
                    events.ScheduleEvent(EVENT_LIGHTNING_BOLT, 3500);
                    break;
                case NPC_NAGA_SIREN:
                    events.ScheduleEvent(EVENT_CHAIN_LIGHTNING, 9000);
                    events.ScheduleEvent(EVENT_WATER_BOLT, 1500);
                    events.ScheduleEvent(SPELL_SIREN_SHOOT, 3000);
                    break;
                case NPC_NAGA_WAVETHRASHER:
                    events.ScheduleEvent(EVENT_FROST_CLEAVE, 2000);
                    break;
                default:
                    break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    // darkspear shaman
                    case EVENT_CHAIN_HEAL:
                        if (Unit* target = DoSelectLowestHpFriendly(50.0f))
                            DoCast(target, SPELL_SH_CHAIN_HEAL);
                        events.ScheduleEvent(EVENT_CHAIN_HEAL, 15000);
                        break;
                    case EVENT_LIGHTNING_BOLT:
                        DoCast(SPELL_SH_LIGHT_BOLT);
                        events.ScheduleEvent(EVENT_LIGHTNING_BOLT, Math::Rand(4000, 8000));
                        break;
                        // siren
                    case EVENT_CHAIN_LIGHTNING:
                        DoCast(SPELL_SIREN_CHAIN_LIGHTNING);
                        events.ScheduleEvent(EVENT_CHAIN_LIGHTNING, Math::Rand(9000, 20000));
                        break;
                    case EVENT_WATER_BOLT:
                        DoCast(SPELL_SIREN_WATER_BOLT);
                        events.ScheduleEvent(EVENT_WATER_BOLT, Math::Rand(4500, 9000));
                        break;
                        // wavethrasher
                    case EVENT_FROST_CLEAVE:
                        DoCast(SPELL_THRASHER_FROST_CLEAVE);
                        events.ScheduleEvent(EVENT_FROST_CLEAVE, Math::Rand(3000, 12000));
                        break;
                    case EVENT_SHOOT:
                        DoCast(SPELL_SIREN_SHOOT);
                        events.ScheduleEvent(EVENT_SHOOT, Math::Rand(4500, 14000));
                        break;
                    default:
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_at_echo_shore_fightersAI(creature);
    }
};

enum ZuniCaveSpells
{
    SPELL_ZUNI_EARTH_SHOCK           = 73255,
    SPELL_ZUNI_HEALING_WAVE          = 73253,
    SPELL_ZUNI_LIGHTNING_BOLT        = 73254
};

enum ZuniCaveEvents
{
    EVENT_ZUNI_HEALING_WAVE          = 1,
    EVENT_ZUNI_EARTH_SHOCK           = 2,
    EVENT_ZUNI_LIGHTNING_BOLT        = 3
};

enum ZuniCaveTalks
{
    SAY_CAVE_ZUNI_0                  = 0, // Ha! I caught up with ya! Let's stick together in 'ere, okay?
    SAY_CAVE_ZUNI_1                  = 1  // Go suck on a trident.
                                          // Ya mest with da wrong trolls, fishhead.
                                          // If I fished ya up, I'd throw ya back. Waste of good fish meat.
                                          // Die snakefish!
                                          // Ya momma was bait fish.
                                          // Ya about to be darkspeared.
                                          // Wow. Ya even ugly for a naga.
                                          // Oi mon, it reeks of bad fish in 'ere.
                                          // Ya mest with da wrong trolls, fishhead.
};

// Guardian zuni in cave(echo isles)
class npc_zuni_guardian_cave : public CreatureScript
{
public:
    npc_zuni_guardian_cave() : CreatureScript("npc_zuni_guardian_cave") { }

    struct npc_zuni_guardian_caveAI : ScriptedAI
    {
        npc_zuni_guardian_caveAI(Creature* creature) : ScriptedAI(creature) { }

        void IsSummonedBy(Unit* summoner) override
        {
            playerGUID = summoner->GetGUID();
            Talk(SAY_CAVE_ZUNI_0);
        }

        void EnterCombat(Unit* attacker) override
        {
            if (Math::Rand(0, 100) < 40)
                Talk(SAY_CAVE_ZUNI_1);

            events.ScheduleEvent(EVENT_ZUNI_HEALING_WAVE, 2500);
            events.ScheduleEvent(EVENT_ZUNI_EARTH_SHOCK, 1000);
            events.ScheduleEvent(EVENT_ZUNI_LIGHTNING_BOLT, 1000);
        }

        void MovementInform(uint32 /*type*/, uint32 /*pointId*/) override
        {
            if (me->GetAreaId() != 4913)
                me->DespawnOrUnsummon();
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ZUNI_EARTH_SHOCK:
                        if (Unit* target = me->GetVictim())
                            if (target->HasUnitState(UNIT_STATE_CASTING))
                                me->CastSpell(me->GetVictim(), SPELL_ZUNI_EARTH_SHOCK, TRIGGERED_IGNORE_POWER_AND_REAGENT_COST);

                        events.ScheduleEvent(EVENT_ZUNI_EARTH_SHOCK, Math::Rand(8000, 20000));
                        break;
                    case EVENT_ZUNI_LIGHTNING_BOLT:
                        me->CastSpell(me->GetVictim(), SPELL_ZUNI_LIGHTNING_BOLT, TRIGGERED_IGNORE_POWER_AND_REAGENT_COST);

                        events.ScheduleEvent(EVENT_ZUNI_LIGHTNING_BOLT, Math::Rand(3000, 6000));
                        break;
                    case EVENT_ZUNI_HEALING_WAVE:
                        if (Unit* owner = ObjectAccessor::GetPlayer(*me, playerGUID))
                            if (owner->HealthBelowPct(50))
                                me->CastSpell(owner, SPELL_ZUNI_HEALING_WAVE, TRIGGERED_IGNORE_POWER_AND_REAGENT_COST);

                        events.ScheduleEvent(EVENT_ZUNI_HEALING_WAVE, 10000);
                        break;
                    default:
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    private:
        ObjectGuid playerGUID;
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_zuni_guardian_caveAI(creature);
    }
};

enum AncientEnemySpells
{
    // vol'jin
    SPELL_GOSSIP_CREDIT_QUEST       = 73589,
    SPELL_SHADOW_SHOCK              = 73087,
    SPELL_VOL_SHOOT                 = 85710,
    SPELL_DELUGE_OF_SHADOWS         = 72044,
    SPELL_SHADOW_SURGE              = 73013,

    // Vanira
    SPELL_HEALING_STREAM_TOTEM      = 71984,
    SPELL_MANA_STREAM_TOTEM         = 73393,
    SPELL_VANIRA_TELEPORT           = 73334,

    // zuni
    SPELL_ZUNI_COVE_LIGHT_BOLT      = 73254,
    SPELL_FROST_EXPLOSION           = 69252,
    SPELL_ZUNI_COVE_FEIGN_DEATH     = 29266,

    // Zarjira
    SPELL_ZAR_SUMMON_BUNNY          = 72046,
    SPELL_ZAR_FROZEN_TORRENT        = 72045,
    SPELL_ZAR_FROST_BOLT            = 46987,
    SPELL_MANIFESTATION             = 72056,
    SPELL_FREEZING_BURST            = 73297,
    SPELL_SEA_WITCH_CREDIT          = 73534,
    SPELL_MANIFESTATION_AURA        = 73295,
    SPELL_FREEZING_TOUCH            = 73004,

    // Manifestation
    SPELL_SOUL_SCAR                 = 73432,

    // Misc
    SPELL_ENERGY_BEAM               = 73294
};

enum AncientEnemyMisc
{
    QUEST_AN_ANCIENT_ENEMY          = 24814,

    NPC_COVE_ZUNI                   = 38423,
    NPC_COVE_VANIRA                 = 38437,
    NPC_BUNNY_CHANNEL               = 38452,
    NPC_COVE_ZARJIRA                = 38306,
    NPC_COVE_VOLJIN                 = 38225,
    NPC_FIRE_OF_THE_SEAS            = 38542,
    NPC_SEA_WITCH_BUNNY             = 38452,
    NPC_HEALING_TOTEM               = 38428,
    NPC_MANA_TOTEM                  = 39051,

    PHASE_NONE                      = 1,
    PHASE_ONE                       = 2,
    PHASE_FROZEN                    = 3,
    PHASE_TWO                       = 4,
    PHASE_THREE                     = 5,

    FACTION_HOSTILITY_TOWARDS_ZARJI = 1770,
    FACTION_HOSTILITY_TOWARDS_VOL   = 2102
};

enum AncientEnemyEvents
{
    // voljin
    EVENT_VOL_BEGIN                 = 1,
    EVENT_VOL_2                     = 2,
    EVENT_VOL_SHADOW_SHOCK          = 3,
    EVENT_VOL_SHOOT                 = 4,
    EVENT_VOL_DELUGE_OF_SHADOWS     = 5,
    EVENT_HANDLE_SECOND_FREEZE      = 6,
    EVENT_HANDLE_SECOND_FREEZE2     = 7,
    EVENT_COVE_SCENE                = 8,
    EVENT_COVE_SCENE2               = 9,
    EVENT_COVE_SCENE3               = 10,
    EVENT_COVE_SCENE4               = 11,
    EVENT_COVE_RESET_EVERYTHING     = 12,
    EVENT_VOL_SHADOW_SURGE          = 13,

    // vanira
    EVENT_VAN_BEGIN                 = 1,
    EVENT_VAN_HEAL_TOTEM            = 2,
    EVENT_VAN_MANA_TOTEM            = 3,
    EVENT_COVE_SCENE5               = 4,
    EVENT_COVE_SCENE6               = 5,
    EVENT_COVE_SCENE7               = 6,
    EVENT_COVE_SCENE8               = 7,
    EVENT_COVE_SCENE9               = 8,

    // zarjira
    EVENT_ZAR_BEGIN                 = 1,
    EVENT_ZAR_SUMMON_BUNNY          = 2,
    EVENT_ZAR_FROST_BOLT            = 3,
    EVENT_ZAR_MANIFESTATION         = 4,
    EVENT_ZAR_FREEZING_BURST        = 5,
    EVENT_ZAR_ZUNI_SCENE            = 6,
    EVENT_ZAR_ZUNI_SCENE2           = 7
};

enum AncientEnemyActions
{
    ACTION_VANIRA_BEGIN             = 1,
    ACTION_COVE_ZAR_BEGIN           = 2,
    ACTION_BEGIN_FIGHT              = 3,
    ACTION_DELUGE_OF_SHADOWS        = 4,
    ACTION_REMOVE_FREEZE            = 5,
    ACTION_HANDLE_SECOND_FREEZE     = 6,
    ACTION_ZARJIRA_DEATH            = 7,
};

enum AncientEnemyTalk
{
    // voljin
    TALK_COVE_VOL_0                 = 0, // Ya were foolish to come 'ere, Sea Witch. Ya escaped our vengeance once, but the Darkspear Tribe will not abide ya trespassin' again.
    TALK_COVE_VOL_1                 = 1, // bossEmote She's drawing power from the fires! Stamp out the braziers, quickly!
    TALK_COVE_VOL_2                 = 2, // It be done. Our ancient enemy is defeated.
    TALK_COVE_VOL_3                 = 3, // I been waitin' a long time for a chance to avenge my father. A great weight has been lifted from my shoulders.
    TALK_COVE_VOL_4                 = 4, // I must be returnin' ta Darkspear Hold. Please meet me there once Vanira is done with her healin' of the boy.

    // zarjira
    TALK_COVE_ZAR_0                 = 0, // You are weak Vol'jin, like your father was weak. Today I will finish what I started long ago - the Darkspear shall be wiped from existence!
    TALK_COVE_ZAR_1                 = 1, // Leave the fire alone, pesky trolls! Just give up and die.
    TALK_COVE_ZAR_2                 = 2, // Not so fast, little troll!

    // Vanira
    TALK_COVE_VANI_0                = 0, // Take care of her spirits! We be handlin' Zar'jira.
    TALK_COVE_VANI_1                = 1, // ZUNI! NOOOO!
    TALK_COVE_VANI_2                = 2, // I'm afraid there's nothin' I can do for our brother... her power tore away at his soul.
    TALK_COVE_VANI_3                = 3, // I'll send some watchers ta get his body so we can offer a proper farewell... I wish I could do more.
    TALK_COVE_VANI_4                = 4, // Vol'jin rushed off in eagerness, but I can take us back to safety. Just give me the word when ya ready, mon.

    // Zuni
    TALK_COVE_ZUNI_0                = 0  // I'll get the fires dis time!
};

Position const movePos[6] =
{
    { -714.90f, -5579.91f, 25.49f },
    { -729.90f, -5601.05f, 25.49f },
    { -727.01f, -5603.85f, 25.49f },
    { -716.48f, -5581.84f, 25.49f },
    { -717.10f, -5590.55f, 25.49f }, // voljin
    { -715.86f, -5589.48f, 25.49f } // bunny spawn destination
};

class npc_voljin_cove : public CreatureScript
{
public:
    npc_voljin_cove() : CreatureScript("npc_voljin_cove") { }

    struct npc_voljin_coveAI : ScriptedAI
    {
        npc_voljin_coveAI(Creature* creature) : ScriptedAI(creature)
        {
            SetCombatMovement(false);
        }

        void Reset() override
        {
            eventInProgress = false;
            events.Reset();
            me->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
            events.SetPhase(PHASE_NONE);
            me->RestoreFaction();
        }

        bool GossipHello(Player* player) override
        {
            if (!eventInProgress && player->GetQuestStatus(QUEST_AN_ANCIENT_ENEMY) == QUEST_STATUS_INCOMPLETE)
                AddGossipItemFor(player, 11020, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);

            SendGossipMenuFor(player, 15318, me->GetGUID());

            return true;
        }

        bool GossipSelect(Player* player, uint32 sender, uint32 action) override
        {
            if (!eventInProgress)
            {
                events.SetPhase(PHASE_ONE);
                playerGUID = player->GetGUID();
                eventInProgress = true;
                DoCast(player, SPELL_GOSSIP_CREDIT_QUEST);
                events.ScheduleEvent(EVENT_VOL_BEGIN, 100);
                me->RemoveFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                CloseGossipMenuFor(player);

                return true;
            }

            return false;
        }

        void EnterCombat(Unit* who) override
        {
            events.ScheduleEvent(EVENT_VOL_SHADOW_SHOCK, 500);
            events.ScheduleEvent(EVENT_VOL_SHOOT, 1500);
            events.ScheduleEvent(EVENT_VOL_SHADOW_SURGE, 5000);
            events.ScheduleEvent(EVENT_COVE_RESET_EVERYTHING, 600000);
        }

        void AttackStart(Unit* attacker) override
        {
            AttackStartCaster(attacker, 17.0f);
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
                case ACTION_BEGIN_FIGHT:
                {
                    if (Creature* zarjira = ObjectAccessor::GetCreature(*me, zarjiraGUID))
                    {
                        if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                        {
                            if (Creature* zuni = ObjectAccessor::GetCreature(*me, zuniGUID))
                            {
                                events.SetPhase(PHASE_TWO);
                                me->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NON_ATTACKABLE);
                                zarjira->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NON_ATTACKABLE);
                                vanira->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NON_ATTACKABLE);
                                zuni->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NON_ATTACKABLE);
                                me->SetFaction(FACTION_HOSTILITY_TOWARDS_ZARJI);
                                vanira->SetFaction(FACTION_HOSTILITY_TOWARDS_ZARJI);
                                zuni->SetFaction(FACTION_HOSTILITY_TOWARDS_ZARJI);
                                zarjira->SetFaction(FACTION_HOSTILITY_TOWARDS_VOL);
                                vanira->AI()->AttackStart(zarjira);
                                zuni->AI()->AttackStart(zarjira);
                                zarjira->AI()->AttackStartCaster(me, 17.0f);
                                AttackStartCaster(zarjira, 17.0f);
                            }
                        }
                    }
                    break;
                }
                case ACTION_DELUGE_OF_SHADOWS:
                    events.ScheduleEvent(EVENT_VOL_DELUGE_OF_SHADOWS, 100);
                    break;
                case ACTION_REMOVE_FREEZE:
                    if (Creature* zuni = ObjectAccessor::GetCreature(*me, zuniGUID))
                    {
                        if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                        {
                            zuni->RemoveAurasDueToSpell(SPELL_FREEZING_TOUCH);
                            vanira->RemoveAurasDueToSpell(SPELL_FREEZING_TOUCH);
                        }
                    }
                    break;
                case ACTION_HANDLE_SECOND_FREEZE:
                    events.ScheduleEvent(EVENT_HANDLE_SECOND_FREEZE, 3000);
                    break;
                case ACTION_ZARJIRA_DEATH:
                    events.SetPhase(PHASE_THREE);
                    me->SetWalk(true);
                    me->GetMotionMaster()->MovePoint(2, movePos[4]);
                    break;
                default:
                    break;
            }
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type != POINT_MOTION_TYPE)
                return;

            switch (id)
            {
                case 1:
                    Talk(TALK_COVE_VOL_0);
                    events.ScheduleEvent(EVENT_VOL_2, 12000);
                    break;
                case 2:
                    events.ScheduleEvent(EVENT_COVE_SCENE, 100);
                    me->SetFacingTo(0.87f);
                    break;
                default:
                    break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!(events.IsInPhase(PHASE_ONE) || events.IsInPhase(PHASE_THREE)))
                if (!UpdateVictim())
                    return;

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_VOL_BEGIN:
                    {
                        if (Creature* vanira = me->FindNearestCreature(NPC_COVE_VANIRA, 30.0f))
                            vaniraGUID = vanira->GetGUID();

                        if (Creature* zuni = me->FindNearestCreature(NPC_COVE_ZUNI, 30.0f))
                            zuniGUID = zuni->GetGUID();

                        me->GetMotionMaster()->MovePoint(1, movePos[1]);
                        if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                            vanira->AI()->DoAction(ACTION_VANIRA_BEGIN);
                        break;
                    }
                    case EVENT_VOL_2:
                        if (Creature* zarjira = me->FindNearestCreature(NPC_COVE_ZARJIRA, 60.0f))
                        {
                            zarjira->AI()->DoAction(ACTION_COVE_ZAR_BEGIN);
                            zarjiraGUID = zarjira->GetGUID();
                        }
                        break;
                    case EVENT_VOL_SHADOW_SHOCK:
                        if (Unit* target = me->GetVictim())
                            DoCast(target, SPELL_SHADOW_SHOCK);
                        events.ScheduleEvent(EVENT_VOL_SHADOW_SHOCK, Math::Rand(6000, 12000));
                        break;
                    case EVENT_VOL_SHOOT:
                        if (Unit* target = me->GetVictim())
                            DoCast(target, SPELL_VOL_SHOOT, true);
                        events.ScheduleEvent(EVENT_VOL_SHOOT, Math::Rand(1500, 4000));
                        break;
                    case EVENT_VOL_DELUGE_OF_SHADOWS:
                        if (Player* player = ObjectAccessor::GetPlayer(*me, playerGUID))
                            Talk(TALK_COVE_VOL_1, player);

                        DoCast(SPELL_DELUGE_OF_SHADOWS);
                        break;
                    case EVENT_VOL_SHADOW_SURGE:
                        DoCast(SPELL_SHADOW_SURGE);

                        events.ScheduleEvent(EVENT_VOL_SHADOW_SURGE, Math::Rand(10000, 25000));
                        break;
                    case EVENT_HANDLE_SECOND_FREEZE:
                    {
                        if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                        {
                            if (Creature* zuni = ObjectAccessor::GetCreature(*me, zuniGUID))
                            {
                                zuni->RemoveAurasDueToSpell(SPELL_FREEZING_TOUCH);
                                vanira->RemoveAurasDueToSpell(SPELL_FREEZING_TOUCH);
                                zuni->AttackStop();
                                zuni->SetReactState(REACT_PASSIVE);
                                zuni->AI()->Talk(TALK_COVE_ZUNI_0);
                                zuni->SetWalk(false);
                                events.ScheduleEvent(EVENT_HANDLE_SECOND_FREEZE2, 1000);
                            }
                        }
                        break;
                    }
                    case EVENT_HANDLE_SECOND_FREEZE2:
                        if (Creature* zuni = ObjectAccessor::GetCreature(*me, zuniGUID))
                            zuni->GetMotionMaster()->MovePoint(1, movePos[0]);
                        break;
                    case EVENT_COVE_SCENE:
                        Talk(TALK_COVE_VOL_2);

                        events.ScheduleEvent(EVENT_COVE_SCENE2, 7000);
                        break;
                    case EVENT_COVE_SCENE2:
                        Talk(TALK_COVE_VOL_3);

                        events.ScheduleEvent(EVENT_COVE_SCENE3, 12000);
                        break;
                    case EVENT_COVE_SCENE3:
                        Talk(TALK_COVE_VOL_4);

                        events.ScheduleEvent(EVENT_COVE_SCENE4, 12000);
                        break;
                    case EVENT_COVE_SCENE4:
                        if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                            vanira->AI()->SetData(2, 2);
                        me->GetMotionMaster()->MoveTargetedHome();
                        me->DespawnOrUnsummon(4000);
                        break;
                    case EVENT_COVE_RESET_EVERYTHING:
                        if (Creature* zuni = ObjectAccessor::GetCreature(*me, zuniGUID))
                        {
                            if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                            {
                                if (Creature* zarjira = ObjectAccessor::GetCreature(*me, zarjiraGUID))
                                {
                                    zuni->AI()->EnterEvadeMode();
                                    vanira->AI()->EnterEvadeMode();
                                    zarjira->AI()->EnterEvadeMode();
                                    EnterEvadeMode();
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    private:
        ObjectGuid zuniGUID;
        ObjectGuid vaniraGUID;
        ObjectGuid zarjiraGUID;
        ObjectGuid playerGUID;
        bool eventInProgress;
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_voljin_coveAI(creature);
    }
};

class npc_zarjira_cove : public CreatureScript
{
public:
    npc_zarjira_cove() : CreatureScript("npc_zarjira_cove") { }

    struct npc_zarjira_coveAI : ScriptedAI
    {
        npc_zarjira_coveAI(Creature* creature) : ScriptedAI(creature)
        {
            SetCombatMovement(false);
        }

        void Reset() override
        {
            events.SetPhase(PHASE_NONE);
            fireCounter = 0;
            events.Reset();
            me->SetFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NON_ATTACKABLE);
            me->RestoreFaction();

            std::vector<Creature*> fireUnits;
            me->GetCreatureListWithEntryInGrid(fireUnits, NPC_FIRE_OF_THE_SEAS, 60.0f);
            if (!fireUnits.empty())
                for (Creature* target : fireUnits)
                    target->SetFlag(UNIT_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        }

        void DoAction(int32 param) override
        {
            if (param == ACTION_COVE_ZAR_BEGIN)
            {
                events.SetPhase(PHASE_ONE);
                events.ScheduleEvent(EVENT_ZAR_BEGIN, 15000);
                Talk(TALK_COVE_ZAR_0);
            }
        }

        void CastManifestations()
        {
            /* HACK, remove when SPELL_EFFECT_160 IS IMPLEMENTED */
            ThreatContainer::StorageType threatList = me->GetThreatManager().getThreatList();
            for (ThreatContainer::StorageType::const_iterator itr = threatList.begin(); itr != threatList.end(); ++itr)
            {
                if (Unit* target = ObjectAccessor::GetPlayer(*me, (*itr)->GetUnitGuid()))
                    if (target->IsWithinDist(me, 60.0f))
                        target->CastSpell(target, SPELL_MANIFESTATION_AURA, TRIGGERED_FULL_MASK);
            }
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (me->HealthBelowPct(75) && events.IsInPhase(PHASE_ONE))
            {
                events.SetPhase(PHASE_FROZEN);
                events.CancelEvent(EVENT_ZAR_MANIFESTATION);
                DoCast(SPELL_FREEZING_BURST, true);

                std::vector<Creature*> fireUnits;
                me->GetCreatureListWithEntryInGrid(fireUnits, NPC_FIRE_OF_THE_SEAS, 60.0f);
                if (!fireUnits.empty())
                    for (Creature* target : fireUnits)
                    {
                        target->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                        target->AI()->DoCast(me, SPELL_ENERGY_BEAM);
                    }
                DoCast(SPELL_ZAR_SUMMON_BUNNY);
                DoCast(SPELL_ZAR_FROZEN_TORRENT);
                if (Creature* voljin = ObjectAccessor::GetCreature(*me, voljinGUID))
                    voljin->AI()->DoAction(ACTION_DELUGE_OF_SHADOWS);
            }

            if (me->HealthBelowPct(10) && events.IsInPhase(PHASE_TWO))
            {
                events.CancelEvent(EVENT_ZAR_MANIFESTATION);
                events.SetPhase(PHASE_FROZEN);
                DoCast(SPELL_FREEZING_BURST, true);
                events.ScheduleEvent(EVENT_ZAR_ZUNI_SCENE, 7000);
                if (Creature* voljin = ObjectAccessor::GetCreature(*me, voljinGUID))
                    voljin->AI()->DoAction(ACTION_HANDLE_SECOND_FREEZE);
            }
        }

        void JustDied(Unit* killer) override
        {
            DoCastAOE(SPELL_SEA_WITCH_CREDIT);
            if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
            {
                if (Creature* voljin = ObjectAccessor::GetCreature(*me, voljinGUID))
                {
                    voljin->AI()->DoAction(ACTION_ZARJIRA_DEATH);
                    vanira->AI()->DoAction(ACTION_ZARJIRA_DEATH);
                }
            }
        }

        void SetData(uint32 type, uint32 param) override
        {
            if (param == 1 && type == 1)
            {
                ++fireCounter;
                if (fireCounter >= 3)
                {
                    fireCounter = 0;
                    events.SetPhase(PHASE_TWO);
                    Talk(TALK_COVE_ZAR_1);
                    events.ScheduleEvent(EVENT_ZAR_MANIFESTATION, 2000);
                    if (Creature* voljin = ObjectAccessor::GetCreature(*me, voljinGUID))
                    {
                        me->InterruptNonMeleeSpells(true, SPELL_ZAR_FROZEN_TORRENT);
                        voljin->InterruptNonMeleeSpells(true, SPELL_DELUGE_OF_SHADOWS);
                        voljin->AI()->DoAction(ACTION_REMOVE_FREEZE);
                    }
                }
            }
        }

        void EnterCombat(Unit* who) override
        {
            events.SetPhase(PHASE_ONE);
            events.ScheduleEvent(EVENT_ZAR_FROST_BOLT, 1500);
            events.ScheduleEvent(EVENT_ZAR_MANIFESTATION, 5000);
        }

        void AttackStart(Unit* attacker) override
        {
            AttackStartCaster(attacker, 25.0f);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!(events.IsInPhase(PHASE_ONE)))
                if (!UpdateVictim())
                    return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ZAR_BEGIN:
                        if (Creature* voljin = me->FindNearestCreature(NPC_COVE_VOLJIN, 50.0f))
                        {
                            voljin->AI()->DoAction(ACTION_BEGIN_FIGHT);
                            voljinGUID = voljin->GetGUID();
                        }
                        break;
                    case EVENT_ZAR_MANIFESTATION:
                        CastManifestations();
                        events.ScheduleEvent(EVENT_ZAR_MANIFESTATION, Math::Rand(4000, 10000));
                        break;
                    case EVENT_ZAR_ZUNI_SCENE:
                        if (Creature* zuni = me->FindNearestCreature(NPC_COVE_ZUNI, 30.0f))
                        {
                            if (Creature* vanira = me->FindNearestCreature(NPC_COVE_VANIRA, 30.0f))
                            {
                                zuniGUID = zuni->GetGUID();
                                vaniraGUID = vanira->GetGUID();
                                me->SetFacingTo(me->GetAngle(zuni->GetPositionX(), zuni->GetPositionY()));
                                me->AttackStop();
                                me->SetReactState(REACT_PASSIVE);
                                Talk(TALK_COVE_ZAR_2);
                                events.ScheduleEvent(EVENT_ZAR_ZUNI_SCENE2, 4000);
                            }
                        }
                        break;
                    case EVENT_ZAR_ZUNI_SCENE2:
                        if (Creature* zuni = ObjectAccessor::GetCreature(*me, zuniGUID))
                        {
                            if (Creature* vanira = ObjectAccessor::GetCreature(*me, vaniraGUID))
                            {
                                vanira->AI()->Talk(TALK_COVE_VANI_1);
                                zuni->AI()->DoCast(SPELL_FROST_EXPLOSION);
                                me->Kill(zuni, false);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            DoSpellAttackIfReady(SPELL_ZAR_FROST_BOLT);
        }
    private:
        EventMap events;
        ObjectGuid voljinGUID;
        ObjectGuid vaniraGUID;
        ObjectGuid zuniGUID;
        uint8 fireCounter;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_zarjira_coveAI(creature);
    }
};

class npc_vanira_cove : public CreatureScript
{
public:
    npc_vanira_cove() : CreatureScript("npc_vanira_cove") { }

    struct npc_vanira_coveAI : ScriptedAI
    {
        npc_vanira_coveAI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            events.Reset();
            me->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
            me->RestoreFaction();
            cutSceneFinished = false;
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
                case ACTION_VANIRA_BEGIN:
                    me->RemoveFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                    me->GetMotionMaster()->MovePoint(1, movePos[2]);
                    break;
                case ACTION_ZARJIRA_DEATH:
                    events.SetPhase(PHASE_TWO);
                    me->GetMotionMaster()->MovePoint(2, movePos[3]);
                    events.CancelEvent(EVENT_VAN_HEAL_TOTEM);
                    events.CancelEvent(EVENT_VAN_MANA_TOTEM);
                    break;
                default:
                    break;
            }
        }

        void SetData(uint32 type, uint32 value) override
        {
            if (type == 2 && value == 2)
                events.ScheduleEvent(EVENT_COVE_SCENE6, 100);
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type != POINT_MOTION_TYPE)
                return;

            if (id == 2)
            {
                me->SetFacingTo(0.87f);
                me->SetStandState(UNIT_STAND_STATE_KNEEL);
            }
        }

        bool GossipHello(Player* player) override
        {
            if (cutSceneFinished && player->GetQuestStatus(QUEST_AN_ANCIENT_ENEMY) == QUEST_STATUS_COMPLETE)
                AddGossipItemFor(player, 11107, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);

            SendGossipMenuFor(player, 15458, me->GetGUID());

            return true;
        }

        bool GossipSelect(Player* player, uint32 /*sender*/, uint32 /*action*/) override
        {
            DoCast(player, SPELL_VANIRA_TELEPORT);

            return true;
        }

        void EnterCombat(Unit* who) override
        {
            Talk(TALK_COVE_VANI_0);
            events.ScheduleEvent(EVENT_VAN_HEAL_TOTEM, 2500);
            events.ScheduleEvent(EVENT_VAN_MANA_TOTEM, 4500);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!(events.IsInPhase(PHASE_TWO)))
                if (!UpdateVictim())
                    return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_VAN_HEAL_TOTEM:
                        DoCast(SPELL_HEALING_STREAM_TOTEM);
                        events.ScheduleEvent(EVENT_VAN_HEAL_TOTEM, 65000);
                        break;
                    case EVENT_VAN_MANA_TOTEM:
                        DoCast(SPELL_MANA_STREAM_TOTEM);
                        events.ScheduleEvent(EVENT_VAN_MANA_TOTEM, 60000);
                        break;
                    case EVENT_COVE_SCENE6:
                        Talk(TALK_COVE_VANI_2);

                        events.ScheduleEvent(EVENT_COVE_SCENE7, 10000);
                        break;
                    case EVENT_COVE_SCENE7:
                        Talk(TALK_COVE_VANI_3);
                        me->SetStandState(UNIT_STAND_STATE_STAND);
                        events.ScheduleEvent(EVENT_COVE_SCENE8, 11000);
                        break;
                    case EVENT_COVE_SCENE8:
                        Talk(TALK_COVE_VANI_4);
                        me->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                        cutSceneFinished = true;
                        me->DespawnOrUnsummon(60000);
                        break;
                    default:
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    private:
        EventMap events;
        bool cutSceneFinished;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_vanira_coveAI(creature);
    }
};

enum NajtessData
{
    SPELL_ORB_OF_CORRUPTION     = 79782,
    SPELL_SHRINK                = 73424,
    SPELL_CSA_DUMMY             = 58178,

    EVENT_ORB                   = 0,
    EVENT_SHRINK                = 1,
    EVENT_CSA                   = 2,

    MAX_SEATS                   = 4
};

class npc_najtess : public CreatureScript
{
public:
    npc_najtess() : CreatureScript("npc_najtess") { }

    struct npc_najtessAI : ScriptedAI
    {
        npc_najtessAI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            SeatID = -1;
            events.Reset();
        }

        void EnterCombat(Unit* target) override
        {
            events.ScheduleEvent(EVENT_ORB, Math::Rand(5000, 8000));
            events.ScheduleEvent(EVENT_SHRINK, Math::Rand(2000, 4000));
            events.ScheduleEvent(EVENT_CSA, Math::Rand(3000, 4000));

        }

        void EnterEvadeMode(EvadeReason why) override
        {
            ScriptedAI::EnterEvadeMode(why);

            me->RemoveAllAurasByType(SPELL_AURA_CONTROL_VEHICLE);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ORB:
                        DoCastVictim(SPELL_ORB_OF_CORRUPTION);
                        events.ScheduleEvent(EVENT_ORB, Math::Rand(24000, 32000));
                        break;
                    case EVENT_SHRINK:
                        DoCastVictim(SPELL_SHRINK);
                        events.ScheduleEvent(EVENT_SHRINK, Math::Rand(34000, 38000));
                        break;
                    case EVENT_CSA:
                        DoCastVictim(SPELL_CSA_DUMMY);
                        break;
                    default:
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }

        void SpellHitTarget(Unit* target, SpellInfo const* spellInfo) override
        {
            if (spellInfo->Id != SPELL_CSA_DUMMY)
                return;

            if (SeatID > MAX_SEATS)
                return;

            target->EnterVehicle(me, SeatID);
            ++SeatID;
        }

    private:
        int8 SeatID;
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_najtessAI(creature);
    }
};

class darkspear_pit_areatrigger : public AreaTriggerScript
{
public:
    darkspear_pit_areatrigger() : AreaTriggerScript("darkspear_pit_areatrigger") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player && player->GetQuestStatus(QUEST_YOUNG_AND_VICIOUS) == QUEST_STATUS_INCOMPLETE &&
            player->GetQuestStatus(QUEST_YOUNG_AND_VICIOUS) != QUEST_STATUS_REWARDED)
        {
            player->CastSpell(player, SPELL_AT_FINISH_CREDIT);
            if (Unit* raptor = player->GetVehicleBase())
                if (Vehicle* veh = raptor->GetVehicleKit())
                {
                    raptor->GetMotionMaster()->MoveConfused();
                    raptor->ToCreature()->DespawnOrUnsummon(5000);
                    veh->RemoveAllPassengers();
                }
            return true;
        }
        return false;
    }
};

class spell_wounded_46577 : public SpellScriptLoader
{
public:
    spell_wounded_46577() : SpellScriptLoader("spell_wounded_46577") { }

    class spell_wounded_46577SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_wounded_46577SpellScript);

        void HandleScript(SpellEffIndex /*effIndex*/)
        {
            if (Unit* caster = GetCaster())
            {
                float health = Math::Rand( 0.30f, 0.60f);
                caster->SetHealth(caster->GetMaxHealth() * health);
                caster->ToCreature()->SetRegeneratingHealth(false);
                caster->SetStandState(UNIT_STAND_STATE_KNEEL);
            }
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_wounded_46577SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_wounded_46577SpellScript();
    }
};

class spell_summon_channel_bunny_72046 : public SpellScriptLoader
{
public:
    spell_summon_channel_bunny_72046() : SpellScriptLoader("spell_summon_channel_bunny_72046") { }

    class spell_summon_channel_bunny_72046SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_summon_channel_bunny_72046SpellScript);

        void ModDest(SpellDestination& dest)
        {
            static Position const spawnPosition = { movePos[5].GetPositionX(), movePos[5].GetPositionY(), movePos[5].GetPositionZ(), movePos[6].GetOrientation() };
            dest.Relocate(spawnPosition);
        }

        void Register() override
        {
            OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_summon_channel_bunny_72046SpellScript::ModDest, EFFECT_0, TARGET_DEST_CASTER);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_summon_channel_bunny_72046SpellScript();
    }
};

enum ZuniAura
{
    NPC_ZUNI_GUARDIAN       = 38932,
    SPELL_SUMMON_ZUNI       = 73131
};

class spell_summon_zuni_aura : public SpellScriptLoader
{
public:
    spell_summon_zuni_aura() : SpellScriptLoader("spell_summon_zuni_aura") { }

    class spell_summon_zuni_aura_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_summon_zuni_aura_AuraScript);

        void HandlePeriodic(AuraEffect const* /*aurEff*/)
        {
            PreventDefaultAction();
            if (Player* caster = GetCaster()->ToPlayer())
            {
                std::vector<TempSummon*> minionList;
                caster->GetAllMinionsByEntry(minionList, NPC_ZUNI_GUARDIAN);
                if (!minionList.empty())
                    caster->CastSpell(caster, SPELL_SUMMON_ZUNI);
            }
        }

        void Register() override
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_summon_zuni_aura_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_summon_zuni_aura_AuraScript();
    }
};

void AddSC_durotar()
{
    // AI
    new npc_lazy_peon();
    new spell_voodoo();
    new npc_tiki_target();
    new npc_jailor_proving_pit();
    new npc_voljin_echo_isles();
    new npc_zuni_bridge();
    new npc_swiftclaw_38002();
    new npc_swiftclaw_37989();
    new darkspear_pit_areatrigger();
    new npc_bloodtalon_thrasher_vehicle();
    new npc_at_echo_shore_fighters();
    new npc_zuni_guardian_cave();
    new npc_voljin_cove();
    new npc_vanira_cove();
    new npc_zarjira_cove();
    new npc_najtess();

    // Spells
    new spell_wounded_46577();
    new spell_summon_channel_bunny_72046();
    new spell_summon_zuni_aura();
}
