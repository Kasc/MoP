/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is where scripts' loading functions should be declared:

void AddSC_boss_ordos();
void AddSC_deepwind_gorge();                 // Deepwind Gorge
void AddSC_silvershard_mines();              // Silvershard Mines
//void AddSC_zone_dread_wastes();
//void AddSC_zone_isle_of_giants();
//void AddSC_zone_isle_of_thunder();
//void AddSC_zone_krasarang_wilds();
//void AddSC_zone_kun_lai_summit();
//void AddSC_zone_the_jade_forest();
//void AddSC_zone_the_situation_in_dalaran();
//void AddSC_zone_the_veiled_stair();
//void AddSC_zone_timeless_isle();
//void AddSC_zone_townlong_steppes();
//void AddSC_zone_vale_of_eternal_blossoms();
//void AddSC_zone_valley_of_the_four_winds();
//void AddSC_zone_wandering_island();

void AddPandariaScripts()
{
    AddSC_boss_ordos();
    AddSC_deepwind_gorge();
    AddSC_silvershard_mines();
    //AddSC_zone_dread_wastes();
    //AddSC_zone_isle_of_giants();
    //AddSC_zone_isle_of_thunder();
    //AddSC_zone_krasarang_wilds();
    //AddSC_zone_kun_lai_summit();
    //AddSC_zone_the_jade_forest();
    //AddSC_zone_the_situation_in_dalaran();
    //AddSC_zone_the_veiled_stair();
    //AddSC_zone_timeless_isle();
    //AddSC_zone_townlong_steppes();
    //AddSC_zone_vale_of_eternal_blossoms();
    //AddSC_zone_valley_of_the_four_winds();
    //AddSC_zone_wandering_island();
}

