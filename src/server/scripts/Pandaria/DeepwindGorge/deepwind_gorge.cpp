/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Battleground.h"
#include "BattlegroundDG.h"

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

class npc_deepwind_gorge_capture_point : public CreatureScript
{
public:
    npc_deepwind_gorge_capture_point() : CreatureScript("npc_deepwind_gorge_capture_point") { }

    struct npc_deepwind_gorge_capture_pointAI : public ScriptedAI
    {
        npc_deepwind_gorge_capture_pointAI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            DoCast(BG_DG_SPELL_CAPTURE_POINT_NEUTRAL);
        }

        void SpellHit(Unit* caster, SpellInfo const* spellInfo) override
        {
            if (spellInfo->Id != BG_DG_CAPTURING_BANNER_TRIGGER)
                return;

            Player* player = caster->ToPlayer();
            if (!player)
                return;

            Battleground* bg = player->GetBattleground();
            if (!bg)
                return;

            bg->EventPlayerClickedOnFlag(player, me);

            // force update spellclick flag
            me->ForceValuesUpdateAtIndex(UNIT_NPC_FLAGS);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_deepwind_gorge_capture_pointAI(creature);
    }
};

enum DeepwindGorgeMineCartEnum
{
    NPC_ALLIANCE_MINE_CART = 71071,

    SPELL_ALLIANCE_MINE_CART_VISUAL = 141551,
    SPELL_HORDE_MINE_CART_VISUAL = 141555,
    SPELL_CHAINS_VISUAL = 141553
};

class npc_deepwind_gorge_mine_cart : public CreatureScript
{
public:
    npc_deepwind_gorge_mine_cart() : CreatureScript("npc_deepwind_gorge_mine_cart") { }

    struct npc_deepwind_gorge_mine_cartAI : public ScriptedAI
    {
        npc_deepwind_gorge_mine_cartAI(Creature* creature) : ScriptedAI(creature) { }

        void IsSummonedBy(Unit* summoner) override
        {
            uint32 visualSpellID = (me->GetEntry() == NPC_ALLIANCE_MINE_CART) ? SPELL_ALLIANCE_MINE_CART_VISUAL : SPELL_HORDE_MINE_CART_VISUAL;

            me->CastSpell(me, visualSpellID, true);

            me->CastSpell(summoner, SPELL_CHAINS_VISUAL, false);

            me->GetMotionMaster()->MoveFollow(summoner, PET_FOLLOW_DIST, float(M_PI));
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_deepwind_gorge_mine_cartAI(creature);
    }
};

void AddSC_deepwind_gorge()
{
    new npc_deepwind_gorge_capture_point();
    new npc_deepwind_gorge_mine_cart();
}
