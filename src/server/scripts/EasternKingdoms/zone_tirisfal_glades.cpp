/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "Vehicle.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"

enum eTirisfalSpells
{
    SPELL_RIGOR_MORTIS          = 73523,
    SPELL_RIGOR_MORTIS_FLAG     = 78695,
    SPELL_SUMMON_MARSHAL        = 91873,
    SPELL_SUMMON_LILIAN         = 91874,
    SPELL_SUMMON_VALDRED        = 91876,
    SPELL_REVERESE_RIDE_VEHICLE = 91945,
    SPELL_SLUMPED_OVER          = 91935,
    SPELL_RIDE_VEHICLE          = 46598,
    SPELL_YELLOW_ARROW          = 92230,
    SPELL_PERM_FEIGN_DEATH      = 29266,
    SPELL_EJECT_ALL_PASSENGERS  = 79737,
    SPELL_SUMMON_DARNELL_1      = 91576,
    SPELL_ROTBRAIN_FIREBALL     = 9053,
    SPELL_ROTBRAIN_IMMOLATE     = 11962,
    SPELL_ROTBRAIN_RESTORE_MANA = 92158,
    SPELL_ROTBRAIN_ENRAGE       = 63227,
    SPELL_ROTBRAIN_SLAM         = 79881,
    SPELL_MARSHAL_HEROIC_LEAP   = 83015,
    SPELL_MARSHAL_WHIRLWIND     = 83016,

    SPELL_PUDDLEJUMPER_FORCE    = 73110,
    SPELL_MINOR_ORACLE_FORCE    = 73434,
    SPELL_SEE_QUEST_INVIS_SPEC  = 73437,
    SPELL_SEE_QUEST_INVIS_SPARK = 73438,

    SPELL_LIL_SHADOWY_AURA      = 73304,
    SPELL_LIL_SHADOW_HOP        = 73453,
    SPELL_CLEAR_ALL             = 28471,

    SPELL_COSMETIC_DUST_CLOUD   = 43258,
    SPELL_GHOUL_TRANSFORM       = 3287,
    SPELL_PERMA_FEIGN_DEATH     = 130966,

    SPELL_LEAPING_BITE          = 82797,
    SPELL_SUMMON_WORGEN_INFIL   = 73150,

    SPELL_TAD_POLE_FRIGHT_AURA  = 73134,
    SPELL_TAD_POLE_TRIGGERED    = 73133,

    SPELL_LIL_VOS_HOP           = 73308,
    SPELL_LIL_VOS_BRAIN_DESTROY = 73307,
    SPELL_LIL_VOS_DEATH_GRIP    = 73309,
    SPELL_LIL_VOS_SHADOW_NOVA   = 32711,
    SPELL_DEATH_GRIP_LIL        = 64431,
    SPELL_LIL_VOS_SHA_UBER      = 73305,
    SPELL_SANDERS_FOOT_NOOSE    = 73444
};

enum eTirisfalEvents
{
    EVENT_ROTBRAIN_FIREBALL     = 1,
    EVENT_ROTBRAIN_IMMOLATE     = 2,
    EVENT_ROTBRAIN_RESTORE_MANA = 3,
    EVENT_ROTBRAIN_ENRAGE       = 4,
    EVENT_ROTBRAIN_SLAM         = 5,
    EVENT_MARSHAL_WHIRLWIND     = 6,
    EVENT_MARSHAL_HEROIC_LEAP   = 7,

    EVENT_LIL_0                 = 1,
    EVENT_LIL_1                 = 2,
    EVENT_LIL_2                 = 3,
    EVENT_LIL_3                 = 4,
    EVENT_LIL_4                 = 5,
    EVENT_LIL_5                 = 6,
    EVENT_LIL_6                 = 7,
    EVENT_LIL_7                 = 8,
    EVENT_LIL_8                 = 9,

    EVENT_JOHAN_0               = 1,
    EVENT_JOHAN_1               = 2,

    EVENT_HOUND_LEAPING_BITE    = 1,
    EVENT_HOUND_INFILTRATOR     = 2,

    EVENT_WORGEN_0              = 1,
    EVENT_WORGEN_1              = 2,

    EVENT_LIL_GUARDIAN_0        = 1,
    EVENT_LIL_GUARDIAN_1        = 2,
    EVENT_LIL_GUARDIAN_2        = 3,
    EVENT_LIL_GUARDIAN_3        = 4,
    EVENT_LIL_GUARDIAN_4        = 5,
    EVENT_LIL_GUARDIAN_5        = 6,
    EVENT_LIL_GUARDIAN_6        = 7,
    EVENT_LIL_GUARDIAN_7        = 8,
    EVENT_LIL_GUARDIAN_8        = 9,
    EVENT_LIL_GUARDIAN_9        = 10,
    EVENT_LIL_GUARDIAN_10       = 11,
    EVENT_LIL_GUARDIAN_11       = 12,
    EVENT_LIL_GUARDIAN_12       = 13
};

enum eTirisfalEntries
{
    NPC_DARNELL_UD_FIRST        = 49141,
    NPC_DARNELL_UD_SECOND       = 49337,
    NPC_SCARLET_CORPSE          = 49340,
    NPC_ROTBRAIN_BERSERK        = 49422,
    NPC_ROTBRAIN_MAGUS          = 49423,
    NPC_DEATHGUARD_PROTECTOR    = 49428,
    NPC_VILE_FIN_MINOR_ORACLE   = 1544,
    NPC_CAPTURED_ORACLE         = 39078,
    NPC_CAPTURED_PUDDLEJUMPER   = 38923,
    NPC_SCARLET_LT_GEBLER       = 39002,
    NPC_DEATHGUARD_SWALLON      = 39196,
    NPC_DARKHOUND_1             = 1547,
    NPC_DARKHOUND_2             = 1548,
    NPC_DARKHOUND_3             = 1549,
    NPC_VILE_FIN_TADPOLE        = 38937,
    NPC_LT_SANDERS_TRIGGER      = 38936,
    NPC_LILIAN_VOSS_GUARDIAN    = 66185,
    NPC_SCARLET_VANGUARD        = 1540,
    NPC_SCARLET_NEOPHYTE        = 1539,
    NPC_BENEDICTUS_VOSS         = 39097,
    NPC_SCARLET_BODYGUARD       = 1660,
    NPC_CAPTAIN_MELRACHE        = 1665,
    NPC_BENEDICTUS_KC           = 39098,
    NPC_LT_SANDERS              = 13158
};

enum eTirisfalTalk
{
    SAY_DARNELL_0               = 0, //Greetings, $n.
    SAY_DARNELL_1               = 1, //The Shadow Grave is this way.  Follow me, $n.
    SAY_DARNELL_2               = 2, //This way!
    SAY_DARNELL_3               = 3, //Random texts

    SAY_DARNELL2_0              = 0, //Hello again.
    SAY_DARNELL2_1              = 1, //I know the way to Deathknell. Come with me!
    SAY_DARNELL2_2              = 2, //Good, you're still here. Now, where's Deathguard Saltain?
    SAY_DARNELL2_3              = 3, //Ah, here he is.
    SAY_DARNELL2_4              = 4, //Let's get moving, $n. Saltain said that we'd fine some corpses up here.
    SAY_DARNELL3_5              = 5, //I think I see some corpses up ahead. Let's go, $n! You do the searching and fighting. I'll do the lifting.

    SAY_ROTBRAIN_0              = 0, //Who am I?
    SAY_ROTBRAIN_1              = 1, //Look what they've done to me!

    SAY_MARSHAL_0               = 0, //BLEEAAAGGHHH! I'm a monster, don't look at me!

    SAY_LIL_0                   = 0, // Yes, my... wait, be quiet! I hear the lieutenant approaching.
    SAY_LIL_1                   = 1, // Gebler, you came! What did he say?
    SAY_LIL_2                   = 2, // What? NO! This can't be! Gebler, you know me... we were friends once!
    SAY_LIL_3                   = 3, // Gebler, father, why would you...
    SAY_LIL_4                   = 4, // The world of the living may have turned its back on me, but I'm no damned Scourge. Just go.

    SAY_GEB_0                   = 0, // The time has come, my little captive... word has come back from your father.
    SAY_GEB_1                   = 1, // High Priest Voss denounces you as a daughter. He's ordered that you be executed immediately.
    SAY_GEB_2                   = 2, // The High Priest sends his regrets. He had hoped that one day you would be a powerful weapon against our enemies.
    SAY_GEB_3                   = 3, // Unfortunately, you were too dangerous in life, and you're far too dangerous in undeath. I will enjoy killing you, you Scourged witch...

    SAY_JOH_0                   = 0, // I... I... don't... feel... right...
    SAY_JOH_1                   = 1, // My mind... my flesh... I'm... rotting...!

    SAY_WORGEN_INF              = 0, // You deserve to lose this land, for what you people did to us! Now back off, or face the wrath of the worgen!

    SAY_LILIAN_VOSS_0           = 0, // Father!
    SAY_LILIAN_VOSS_1           = 1, // Shut up.
    SAY_LILIAN_VOSS_2           = 2, // You raised me to be a killer.  How am I doing, daddy?
    SAY_LILIAN_VOSS_3           = 3, // But wait... I remember now.  You taught me to only kill the undead.  So do you want me to kill myself, daddy?
    SAY_LILIAN_VOSS_4           = 4, // Then again, why kill myself... when I can kill YOU instead!

    SAY_BENEDICTUS_0            = 0, // Lilian... you're... it's so nice to see you well.
    SAY_BENEDICTUS_1            = 1, // I, ah...
    SAY_BENEDICTUS_2            = 2  // Lilian, I...
};

enum eTirisfalQuest
{
    QUEST_THE_SHADOW_GRAVE      = 28608,
    QUEST_THE_WAKENING          = 24960,
    QUEST_BEYOND_THE_GRAVE      = 25089,
    QUEST_RECRUITMENT           = 26800,
    QUEST_NIGHT_WEB_HOLLOW      = 24973,
    QUEST_JOHAANS_EXPERIMENT    = 24977,
    QUEST_ESCAPE_FROM_GILNEAS   = 24992,
    QUEST_HAVE_YOU_SEEN         = 25039
};

enum eTirisfalGladesDatas
{
    DATA_MOVE_TO_CHAMBER        = 0,
    DATA_MOVE_BELOW_CHAMBER     = 1,
    DATA_MOVE_INSIDE_CHAMBER    = 2,
    DATA_REACHED_GATE           = 3,
    DATA_REACHED_SALTAIN        = 4,
    DATA_DARNELL_LAUNCH_PATH    = 5,
    DATA_SCARLET_CORPSE_GUID    = 6,

    DATA_GEBREL_REACHED         = 0
};

enum eTirisfalMisc
{
    PHASE_LILIAN_GRAVEYARD      = 1660,
    PHASE_MARSHAL_GRAVEYARD     = 1661,
    PHASE_VALDRED_GRAVEYARD     = 1662,
    PHASE_ROTBRAIN_BATTLE       = 369,

    POINT_ID_DARNELL_END        = 6,
    POINT_ID_SCARLET            = 7,
    POINT_ID_JOHAN_DEATH        = 4,

    FACTION_ROTBRAIN_HOSTILE    = 14,

    LILIAN_GOSSIP_MENU_ID       = 11136,

    SWALLON_PATH_ID             = 391960,

    POINT_WORGEN_NEAR_SUMMONER  = 1,

    ACTION_SHADOW_NOVA          = 1
};

Position const darnellMoveToChamber = { 1661.595215f, 1662.722412f, 141.849396f };
Position const darnellMoveBelowChamber = { 1657.860596f, 1678.265625f, 120.719391f };
Position const geblerSpawnPos = { 2447.084229f, 1596.149902f, 68.299385f, 2.306856f };
Position const lilianNearFather = { 3073.676270f, -557.192993f, 126.718147f, 0.104670f };
Position const lilianExit = { 3037.529785f, -559.860107f, 121.358803f, 3.167723f };

class npc_darnell_ud_first : public CreatureScript
{
public:
    npc_darnell_ud_first() : CreatureScript("npc_darnell_ud_first") { }

    struct npc_darnell_ud_first_AI : public ScriptedAI
    {
        npc_darnell_ud_first_AI(Creature* creature) : ScriptedAI(creature)
        {
            movedTo = false;
            movedBelow = false;
            insideChamber = false;
        }

        void SetData(uint32 type, uint32 data) override
        {
            switch (type)
            {
                case DATA_MOVE_BELOW_CHAMBER:
                    if (movedBelow)
                        break;
                    movedBelow = true;
                    if (Player* summoner = ObjectAccessor::GetPlayer(*me, playerGUID))
                        Talk(SAY_DARNELL_2, summoner);
                    // This is guardian, he will keep following you forever and when follow is done
                    // movePath will execute. We need to expire follow movement and MovePath will be executed
                    me->GetMotionMaster()->MovementExpired();
                    me->GetMotionMaster()->MovePath(me->GetEntry() * 10, false);
                    break;
                case DATA_MOVE_TO_CHAMBER:
                    if (movedTo)
                        break;
                    movedTo = true;
                    if (Player* summoner = ObjectAccessor::GetPlayer(*me, playerGUID))
                        Talk(SAY_DARNELL_1, summoner);
                    me->GetMotionMaster()->MovePoint(1, darnellMoveToChamber);
                    break;
                case DATA_MOVE_INSIDE_CHAMBER:
                    if (insideChamber)
                        break;
                    insideChamber = true;
                    me->GetMotionMaster()->MovePath(me->GetEntry() * 11, true);
                    break;
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (!insideChamber)
                return;

            if (type == WAYPOINT_MOTION_TYPE)
            {
                if (Math::RollOver(50))
                {
                    if (Player* summoner = ObjectAccessor::GetPlayer(*me, playerGUID))
                        Talk(SAY_DARNELL_3, summoner);
                }
            }
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetTypeId() == TYPEID_PLAYER)
            {
                playerGUID = summoner->GetGUID();
                Talk(SAY_DARNELL_0, summoner);
            }
        }
    private:
        bool movedTo, movedBelow, insideChamber;
        ObjectGuid playerGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_darnell_ud_first_AI(creature);
    }
};

class npc_darnell_ud_second : public CreatureScript
{
public:
    npc_darnell_ud_second() : CreatureScript("npc_darnell_ud_second") { }

    struct npc_darnell_ud_second_AI : public ScriptedAI
    {
        npc_darnell_ud_second_AI(Creature* creature) : ScriptedAI(creature)
        {
            // Prevent multiple occurances of talks/paths when it was already executed
            // it is called when owners steps onto areatrigger
            reachedGate = false;
            reachedSaltain = false;
            pathLaunched = false;
            me->SetReactState(REACT_PASSIVE);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetTypeId() == TYPEID_PLAYER)
            {
                summonerGUID = summoner->GetGUID();
                Talk(SAY_DARNELL2_0, summoner);
            }
        }

        void SetGUID(ObjectGuid guid, int32 data) override
        {
            if (data == DATA_SCARLET_CORPSE_GUID)
                scarletGUID = guid;
        }

        void PassengerBoarded(Unit* pass, int8 /*seat*/, bool apply) override
        {
            if (pass->GetEntry() == NPC_SCARLET_CORPSE && apply)
            {
                if (!me->HasAura(SPELL_SLUMPED_OVER))
                    DoCast(SPELL_SLUMPED_OVER);

                pass->RemoveAurasDueToSpell(SPELL_YELLOW_ARROW);
                pass->RemoveAurasDueToSpell(SPELL_PERM_FEIGN_DEATH);
                pass->RemoveFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
            }
            else if (pass->GetEntry() == NPC_SCARLET_CORPSE && !apply)
                pass->ToCreature()->DespawnOrUnsummon(2s);
        }

        void SetData(uint32 type, uint32 data) override
        {
            switch (type)
            {
                case DATA_REACHED_GATE:
                {
                    if (reachedGate)
                        break;

                    reachedGate = true;

                    if (Player* player = ObjectAccessor::GetPlayer(*me, summonerGUID))
                    {
                        Talk(SAY_DARNELL2_2, player);
                        me->GetMotionMaster()->MovementExpired();
                        me->GetMotionMaster()->MovePath(me->GetEntry() * 11, false);
                    }
                    break;
                }
                case DATA_REACHED_SALTAIN:
                {
                    if (reachedSaltain)
                        break;

                    reachedSaltain = true;
                    EnterEvadeMode(EVADE_REASON_OTHER);
                    break;
                }
                case DATA_DARNELL_LAUNCH_PATH:
                {
                    if (pathLaunched)
                        break;

                    pathLaunched = true;
                    if (Player* player = ObjectAccessor::GetPlayer(*me, summonerGUID))
                    {
                        me->GetMotionMaster()->MovementExpired();
                        Talk(SAY_DARNELL2_1, player);
                        me->GetMotionMaster()->MovePath(me->GetEntry() * 10, false);
                    }
                    break;
                }
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == WAYPOINT_MOTION_TYPE && pointId == POINT_ID_DARNELL_END && reachedGate)
                Talk(SAY_DARNELL2_3);
            else if (type == POINT_MOTION_TYPE && pointId == POINT_ID_SCARLET)
            {
                Player* player = ObjectAccessor::GetPlayer(*me, summonerGUID);
                Creature* scarlet = ObjectAccessor::GetCreature(*me, scarletGUID);
                if (!player || !scarlet)
                    return;

                me->CastSpell(player, SPELL_REVERESE_RIDE_VEHICLE, true);
                scarlet->CastSpell(me, SPELL_RIDE_VEHICLE, true);
                EnterEvadeMode(EVADE_REASON_OTHER);
            }
        }
    private:
        ObjectGuid summonerGUID;
        bool reachedGate, reachedSaltain, pathLaunched;
        ObjectGuid scarletGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_darnell_ud_second_AI(creature);
    }
};

//! Handles 3 NPCs
//! IDs: 49423, 49422, 49428
class npc_rotbrain_battle : public CreatureScript
{
public:
    npc_rotbrain_battle() : CreatureScript("npc_rotbrain_battle") { }

    struct npc_rotbrain_battle_AI : public ScriptedAI
    {
        npc_rotbrain_battle_AI(Creature* creature) : ScriptedAI(creature)
        {
            if (me->IsInPhase(PHASE_ROTBRAIN_BATTLE) && me->GetEntry() != NPC_DEATHGUARD_PROTECTOR)
                me->SetFaction(FACTION_ROTBRAIN_HOSTILE);

            if (Math::RollUnder(50) && me->GetEntry() != NPC_DEATHGUARD_PROTECTOR)
                Talk(SAY_ROTBRAIN_0);
        }

        void Reset() override
        {
            events.Reset();
        }

        void EnterCombat(Unit* who) override
        {
            if (me->GetEntry() == NPC_DEATHGUARD_PROTECTOR)
            {
                ScriptedAI::EnterCombat(who);
                return;
            }

            if (Math::RollUnder(35))
                Talk(SAY_ROTBRAIN_1);

            if (me->GetEntry() == NPC_ROTBRAIN_MAGUS)
            {
                events.ScheduleEvent(EVENT_ROTBRAIN_FIREBALL, 1s);
                events.ScheduleEvent(EVENT_ROTBRAIN_IMMOLATE, 6s);
                events.ScheduleEvent(EVENT_ROTBRAIN_RESTORE_MANA, 15s);
            }
            else
            {
                events.ScheduleEvent(EVENT_ROTBRAIN_ENRAGE, 5s);
                events.ScheduleEvent(EVENT_ROTBRAIN_SLAM, 8s);
            }
        }

        void AttackStart(Unit* who) override
        {
            if (me->GetEntry() == NPC_ROTBRAIN_MAGUS)
                AttackStartCaster(who, 6.0f);
            else
                ScriptedAI::AttackStart(who);
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (attacker->GetTypeId() == TYPEID_UNIT && me->HealthBelowPctDamaged(80, damage))
                damage = 0;
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            if (me->HasUnitState(UNIT_STATE_CASTING))
                return;

            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    // Rotbrain magus
                    case EVENT_ROTBRAIN_FIREBALL:
                        DoCastVictim(SPELL_ROTBRAIN_FIREBALL);
                        events.Repeat(6s);
                        break;
                    case EVENT_ROTBRAIN_IMMOLATE:
                        DoCastVictim(SPELL_ROTBRAIN_IMMOLATE);
                        events.Repeat(20s);
                        break;
                    case EVENT_ROTBRAIN_RESTORE_MANA:
                        DoCast(SPELL_ROTBRAIN_RESTORE_MANA);
                        events.Repeat(30s);
                        break;
                    // Rotbrain berserk
                    case EVENT_ROTBRAIN_ENRAGE:
                        DoCast(SPELL_ROTBRAIN_ENRAGE);
                        events.Repeat(25s);
                        break;
                    case EVENT_ROTBRAIN_SLAM:
                        DoCastVictim(SPELL_ROTBRAIN_SLAM);
                        events.Repeat(7s);
                        break;
                    default:
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_rotbrain_battle_AI(creature);
    }
};

class npc_marshal_redpath : public CreatureScript
{
public:
    npc_marshal_redpath() : CreatureScript("npc_marshal_redpath") { }

    struct npc_marshal_redpath_AI : public ScriptedAI
    {
        npc_marshal_redpath_AI(Creature* creature) : ScriptedAI(creature)
        {
            if (me->IsInPhase(PHASE_ROTBRAIN_BATTLE))
                me->SetFaction(FACTION_ROTBRAIN_HOSTILE);
        }

        void EnterCombat(Unit* who) override
        {
            Talk(SAY_MARSHAL_0);
            events.ScheduleEvent(EVENT_MARSHAL_WHIRLWIND, 4s);
            ScriptedAI::EnterCombat(who);
        }

        void Reset()
        {
            events.Reset();
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_MARSHAL_WHIRLWIND:
                        DoCast(SPELL_MARSHAL_WHIRLWIND);
                        events.ScheduleEvent(EVENT_MARSHAL_HEROIC_LEAP, 5s);
                        events.Repeat(18s);
                        break;
                    case EVENT_MARSHAL_HEROIC_LEAP:
                        if (Unit* target = SelectTarget(SELECT_TARGET_FARTHEST, 0))
                            if (me->GetDistance(target) >= 8.0f)
                                DoCast(target, SPELL_MARSHAL_HEROIC_LEAP, false);
                        break;
                    default:
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }

    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_marshal_redpath_AI(creature);
    }
};

class at_deathknell_near_mordo : public AreaTriggerScript
{
public:
    at_deathknell_near_mordo() : AreaTriggerScript("at_deathknell_near_mordo") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player->GetQuestStatus(QUEST_BEYOND_THE_GRAVE) == QUEST_STATUS_COMPLETE &&
            player->GetQuestStatus(QUEST_BEYOND_THE_GRAVE) != QUEST_STATUS_REWARDED)
        {
            std::vector<TempSummon*> guardianList;
            player->GetAllMinionsByEntry(guardianList, NPC_DARNELL_UD_SECOND);

            if (!guardianList.empty())
            {
                for (TempSummon* summon : guardianList)
                    summon->AI()->SetData(DATA_DARNELL_LAUNCH_PATH, DATA_DARNELL_LAUNCH_PATH);
                return true;
            }
        }
        return false;
    }
};

class at_deathknell_starting_darnell : public AreaTriggerScript
{
public:
    at_deathknell_starting_darnell() : AreaTriggerScript("at_deathknell_starting_darnell") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player->GetQuestStatus(QUEST_THE_SHADOW_GRAVE) == QUEST_STATUS_INCOMPLETE &&
            player->GetQuestStatus(QUEST_THE_SHADOW_GRAVE) != QUEST_STATUS_REWARDED)
        {
            std::vector<TempSummon*> guardianList;
            player->GetAllMinionsByEntry(guardianList, NPC_DARNELL_UD_FIRST);

            if (!guardianList.empty())
            {
                for (TempSummon* summon : guardianList)
                    summon->AI()->SetData(DATA_MOVE_TO_CHAMBER, DATA_MOVE_TO_CHAMBER);
                return true;
            }
        }
        return false;
    }
};

class at_deathknell_starting_darnell_chamber : public AreaTriggerScript
{
public:
    at_deathknell_starting_darnell_chamber() : AreaTriggerScript("at_deathknell_starting_darnell_chamber") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player->GetQuestStatus(QUEST_THE_SHADOW_GRAVE) == QUEST_STATUS_INCOMPLETE &&
            player->GetQuestStatus(QUEST_THE_SHADOW_GRAVE) != QUEST_STATUS_REWARDED)
        {
            std::vector<TempSummon*> guardianList;
            player->GetAllMinionsByEntry(guardianList, NPC_DARNELL_UD_FIRST);

            if (!guardianList.empty())
            {
                for (TempSummon* summon : guardianList)
                    summon->AI()->SetData(DATA_MOVE_BELOW_CHAMBER, DATA_MOVE_BELOW_CHAMBER);
                return true;
            }
        }
        return false;
    }
};

class at_deathknell_starting_darnell_inside_chamber : public AreaTriggerScript
{
public:
    at_deathknell_starting_darnell_inside_chamber() : AreaTriggerScript("at_deathknell_starting_darnell_inside_chamber") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player->GetQuestStatus(QUEST_THE_SHADOW_GRAVE) == QUEST_STATUS_INCOMPLETE &&
            player->GetQuestStatus(QUEST_THE_SHADOW_GRAVE) != QUEST_STATUS_REWARDED)
        {
            std::vector<TempSummon*> guardianList;
            player->GetAllMinionsByEntry(guardianList, NPC_DARNELL_UD_FIRST);

            if (!guardianList.empty())
            {
                for (TempSummon* summon : guardianList)
                    summon->AI()->SetData(DATA_MOVE_INSIDE_CHAMBER, DATA_MOVE_INSIDE_CHAMBER);
                return true;
            }
        }
        return false;
    }
};

class at_deathknell_gate_areatrigger : public AreaTriggerScript
{
public:
    at_deathknell_gate_areatrigger() : AreaTriggerScript("at_deathknell_gate_areatrigger") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player->GetQuestStatus(QUEST_BEYOND_THE_GRAVE) == QUEST_STATUS_COMPLETE &&
            player->GetQuestStatus(QUEST_BEYOND_THE_GRAVE) != QUEST_STATUS_REWARDED)
        {
            std::vector<TempSummon*> guardianList;
            player->GetAllMinionsByEntry(guardianList, NPC_DARNELL_UD_SECOND);

            if (!guardianList.empty())
            {
                for (TempSummon* summon : guardianList)
                    summon->AI()->SetData(DATA_REACHED_GATE, DATA_REACHED_GATE);
                return true;
            }
        }
        return false;
    }
};

class at_deathknell_near_saltain : public AreaTriggerScript
{
public:
    at_deathknell_near_saltain() : AreaTriggerScript("at_deathknell_near_saltain") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/, bool /*entered*/) override
    {
        if (player->GetQuestStatus(QUEST_BEYOND_THE_GRAVE) == QUEST_STATUS_COMPLETE &&
            player->GetQuestStatus(QUEST_BEYOND_THE_GRAVE) != QUEST_STATUS_REWARDED)
        {
            std::vector<TempSummon*> guardianList;
            player->GetAllMinionsByEntry(guardianList, NPC_DARNELL_UD_SECOND);

            if (!guardianList.empty())
            {
                for (TempSummon* summon : guardianList)
                    summon->AI()->SetData(DATA_REACHED_SALTAIN, DATA_REACHED_SALTAIN);
                return true;
            }
        }
        return false;
    }
};

class spell_undead_valkyr_res_73524 : public SpellScriptLoader
{
public:
    spell_undead_valkyr_res_73524() : SpellScriptLoader("spell_undead_valkyr_res_73524") { }

    class spell_undead_valkyr_res_73524_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_undead_valkyr_res_73524_SpellScript);

        void HandleHit(SpellEffIndex /*effIndex*/)
        {
            if (GetHitUnit()->HasAura(SPELL_RIGOR_MORTIS))
                GetHitUnit()->RemoveAurasDueToSpell(SPELL_RIGOR_MORTIS);
            GetHitUnit()->CastSpell(GetHitUnit(), SPELL_RIGOR_MORTIS_FLAG, true);
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_undead_valkyr_res_73524_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_undead_valkyr_res_73524_SpellScript();
    }
};

class spell_wakening_summoning_spells_q_24960 : public SpellScriptLoader
{
public:
    spell_wakening_summoning_spells_q_24960() : SpellScriptLoader("spell_wakening_summoning_spells_q_24960") { }

    class spell_wakening_summoning_spells_q_24960_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_wakening_summoning_spells_q_24960_SpellScript);

        void HandleHit(SpellEffIndex /*EffIndex*/)
        {
            auto phaseToRemove = 0;

            switch (GetSpellInfo()->Id)
            {
                case SPELL_SUMMON_LILIAN:
                    phaseToRemove = PHASE_LILIAN_GRAVEYARD;
                    break;
                case SPELL_SUMMON_MARSHAL:
                    phaseToRemove = PHASE_MARSHAL_GRAVEYARD;
                    break;
                case SPELL_SUMMON_VALDRED:
                    phaseToRemove = PHASE_VALDRED_GRAVEYARD;
                    break;
            }

            if (!phaseToRemove)
                return;

            GetCaster()->SetInPhase(phaseToRemove, true, false);
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_wakening_summoning_spells_q_24960_SpellScript::HandleHit, EFFECT_1, SPELL_EFFECT_KILL_CREDIT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_wakening_summoning_spells_q_24960_SpellScript();
    }
};

class spell_tirisfal_guardian_aura_91777 : public SpellScriptLoader
{
public:
    spell_tirisfal_guardian_aura_91777() : SpellScriptLoader("spell_tirisfal_guardian_aura_91777") { }

    class spell_tirisfal_guardian_aura_91777_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_tirisfal_guardian_aura_91777_AuraScript);

        void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            if (Player* owner = GetTarget()->ToPlayer())
            {
                //! Either blizzard uses server-side stuff to handle phasing
                //! or this single aura handles multiple phasings, i couldnt find anything else in sniffs
                //! First if handles phasing with the wakening quest and else if handles everything else

                // first phasing
                if (owner->GetQuestStatus(QUEST_THE_WAKENING) != QUEST_STATUS_COMPLETE &&
                    owner->GetQuestStatus(QUEST_THE_WAKENING) != QUEST_STATUS_REWARDED)
                {
                    Quest const* quest = sObjectMgr->GetQuestTemplate(QUEST_THE_WAKENING);
                    if (!quest)
                        return;

                    if (owner->GetQuestObjectiveData(quest, 0) == 0)
                        owner->SetInPhase(PHASE_LILIAN_GRAVEYARD, true, true);

                    if (owner->GetQuestObjectiveData(quest, 1) == 0)
                        owner->SetInPhase(PHASE_MARSHAL_GRAVEYARD, true, true);

                    if (owner->GetQuestObjectiveData(quest, 2) == 0)
                        owner->SetInPhase(PHASE_VALDRED_GRAVEYARD, true, true);
                }
                // second phasing
                else if (owner->GetQuestStatus(QUEST_NIGHT_WEB_HOLLOW) == QUEST_STATUS_REWARDED)
                {
                    owner->SetInPhase(PHASE_ROTBRAIN_BATTLE, true, true);
                }
            }
        }

        void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            GetTarget()->SetInPhase(PHASE_LILIAN_GRAVEYARD, true, false);
            GetTarget()->SetInPhase(PHASE_MARSHAL_GRAVEYARD, true, false);
            GetTarget()->SetInPhase(PHASE_VALDRED_GRAVEYARD, true, false);
            GetTarget()->SetInPhase(PHASE_ROTBRAIN_BATTLE, true, false);
        }

        void Register() override
        {
            OnEffectApply += AuraEffectApplyFn(spell_tirisfal_guardian_aura_91777_AuraScript::OnApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            AfterEffectRemove += AuraEffectRemoveFn(spell_tirisfal_guardian_aura_91777_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_tirisfal_guardian_aura_91777_AuraScript();
    }
};

//! Two spells: IDs[91576, 91938]
class spell_summon_darnell_91576_91938 : public SpellScriptLoader
{
public:
    spell_summon_darnell_91576_91938() :  SpellScriptLoader("spell_summon_darnell_91576_91938") { }

    class spell_summon_darnell_91576_91938_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_summon_darnell_91576_91938_SpellScript);

        void HandleSummon(SpellEffIndex effIndex)
        {
            if (Player* player = GetCaster()->ToPlayer())
            {
                std::vector<TempSummon*> vGuardian;
                player->GetAllMinionsByEntry(vGuardian, static_cast<uint32>(GetSpellInfo()->GetEffect(effIndex)->MiscValue));

                if (!vGuardian.empty())
                    PreventHitDefaultEffect(effIndex);
            }
        }

        void Register() override
        {
            OnEffectHit += SpellEffectFn(spell_summon_darnell_91576_91938_SpellScript::HandleSummon, EFFECT_0, SPELL_EFFECT_SUMMON);
        }
    };

    class spell_summon_darnell_91576_91938_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_summon_darnell_91576_91938_AuraScript);

        void OnRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
        {
            if (Player* player = GetTarget()->ToPlayer())
            {
                std::vector<TempSummon*> vGuardian;
                player->GetAllMinionsByEntry(vGuardian, GetSpellInfo()->Id == SPELL_SUMMON_DARNELL_1 ? NPC_DARNELL_UD_FIRST : NPC_DARNELL_UD_SECOND);

                for (TempSummon* summon : vGuardian)
                {
                    summon->GetMotionMaster()->MovementExpired();
                    summon->HandleEmoteCommand(EMOTE_ONESHOT_WAVE);
                    if (player->GetQuestStatus(QUEST_RECRUITMENT) == QUEST_STATUS_REWARDED)
                        summon->CastSpell(summon, SPELL_EJECT_ALL_PASSENGERS, TRIGGERED_FULL_MASK);
                    summon->DespawnOrUnsummon(2s);
                }
            }
        }

        void Register() override
        {
            OnEffectRemove += AuraEffectRemoveFn(spell_summon_darnell_91576_91938_AuraScript::OnRemove, EFFECT_1, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_summon_darnell_91576_91938_SpellScript();
    }

    AuraScript* GetAuraScript() const override
    {
        return new spell_summon_darnell_91576_91938_AuraScript();
    }
};

class spell_eject_all_passengers_79737 : public SpellScriptLoader
{
public:
    spell_eject_all_passengers_79737() : SpellScriptLoader("spell_eject_all_passengers_79737") { }

    class spell_eject_all_passengers_79737_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_eject_all_passengers_79737_SpellScript);

        void RemoveVehicleAuras()
        {
            if (Vehicle* vehicle = GetHitUnit()->GetVehicleKit())
            {
                uint32 seatCount = vehicle->GetSeatCount();
                Position pos = GetHitUnit()->GetPosition();
                float radius = 2.0f;
                float angle = 0.0f;
                float calculateAngle = 2 * M_PI / seatCount;

                for (uint32 i = 0; i < seatCount; angle += calculateAngle, ++i)
                {
                    if (Unit* pass = vehicle->GetPassenger(i))
                    {
                        pass->ExitVehicle();
                        float x = pos.GetPositionX() + radius * cosf(angle);
                        float y = pos.GetPositionY() + radius * sinf(angle);
                        pass->GetMotionMaster()->MoveJump(x, y, pos.GetPositionZ(), 0.0f, 5.0f, 5.0f);
                    }
                }
            }
        }

        void Register() override
        {
            AfterHit += SpellHitFn(spell_eject_all_passengers_79737_SpellScript::RemoveVehicleAuras);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_eject_all_passengers_79737_SpellScript();
    }
};

class spell_select_scarlet_corpse_91942 : public SpellScriptLoader
{
public:
    spell_select_scarlet_corpse_91942() : SpellScriptLoader("spell_select_scarlet_corpse_91942") { }

    class spell_select_scarlet_corpse_91942_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_select_scarlet_corpse_91942_SpellScript);

        void HandleHit(SpellEffIndex /*effIndex*/)
        {
            if (Player* player = GetCaster()->ToPlayer())
            {
                std::vector<TempSummon*> vGuardian;
                player->GetAllMinionsByEntry(vGuardian, NPC_DARNELL_UD_SECOND);

                for (TempSummon* summon : vGuardian)
                {
                    summon->AI()->SetGUID(GetHitUnit()->GetGUID(), DATA_SCARLET_CORPSE_GUID);
                    float x, y, z;
                    GetHitUnit()->GetNearContactPoint(summon, x, y, z, 0.5f);
                    summon->GetMotionMaster()->MovePoint(POINT_ID_SCARLET, x, y, z, true);
                }
            }
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_select_scarlet_corpse_91942_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_select_scarlet_corpse_91942_SpellScript();
    }
};
// END OF DEATHKNELL

class spell_murloc_leash_73108 : public SpellScriptLoader
{
public:
    spell_murloc_leash_73108() : SpellScriptLoader("spell_murloc_leash_73108") { }

    class spell_murloc_leash_73108_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_murloc_leash_73108_AuraScript);

        void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            if (Creature* murloc = GetTarget()->ToCreature())
                murloc->SetControlled(true, UNIT_STATE_STUNNED);
        }

        void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes mode)
        {
            if (Creature* murloc = GetTarget()->ToCreature())
            {
                murloc->DespawnOrUnsummon();

                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_EXPIRE)
                    return;

                if (murloc->GetEntry() == NPC_VILE_FIN_MINOR_ORACLE)
                    murloc->CastSpell(GetCaster(), SPELL_MINOR_ORACLE_FORCE, true);
                else
                    murloc->CastSpell(GetCaster(), SPELL_PUDDLEJUMPER_FORCE, true);
            }
        }

        void Register() override
        {
            OnEffectApply += AuraEffectApplyFn(spell_murloc_leash_73108_AuraScript::OnApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            AfterEffectRemove += AuraEffectRemoveFn(spell_murloc_leash_73108_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_murloc_leash_73108_AuraScript();
    }
};

class npc_sedrick_calston : public CreatureScript
{
public:
    npc_sedrick_calston() : CreatureScript("npc_sedrick_calston") { }

    struct npc_sedrick_calston_AI : public ScriptedAI
    {
        npc_sedrick_calston_AI(Creature* creature) : ScriptedAI(creature) { }

        void MoveInLineOfSight(Unit* who) override
        {
            if ((who->GetEntry() == NPC_CAPTURED_ORACLE || who->GetEntry() == NPC_CAPTURED_PUDDLEJUMPER) && who->IsWithinDist(me, 1.0f))
            {
                ObjectGuid playerGUID;

                if (who->IsSummon())
                    playerGUID = who->ToTempSummon()->GetSummonerGUID();

                Player* player = ObjectAccessor::GetPlayer(*me, playerGUID);

                if (!player)
                    return;

                if (player->HasAura(SPELL_SEE_QUEST_INVIS_SPEC) || player->HasAura(SPELL_SEE_QUEST_INVIS_SPARK))
                    return;

                player->KilledMonsterCredit(38887, ObjectGuid::Empty);
                // move this to spell_area when we figure out how to do this
                // if we deliver captured oracle we're supposed to always get SPECKLE INVISIBLITY DETECTION aura, otherwise we should get sparky aura
                player->CastSpell(player, who->GetEntry() == NPC_CAPTURED_ORACLE ? SPELL_SEE_QUEST_INVIS_SPARK : SPELL_SEE_QUEST_INVIS_SPEC, true);
                who->ToCreature()->DespawnOrUnsummon(2s);
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_sedrick_calston_AI(creature);
    }
};

class npc_lilian_voss_cage : public CreatureScript
{
public:
    npc_lilian_voss_cage() : CreatureScript("npc_lilian_voss_cage") { }

    struct npc_lilian_voss_cage_AI : public ScriptedAI
    {
        npc_lilian_voss_cage_AI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            eventInProgress = false;
            geblerGUID.Clear();
            playerGUID.Clear();
            events.Reset();
            me->SetUInt32Value(UNIT_EMOTE_STATE, 0);
            me->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
            me->RemoveAurasDueToSpell(SPELL_LIL_SHADOWY_AURA);
        }

        void SetData(uint32 /*type*/, uint32 data) override
        {
            if (data == DATA_GEBREL_REACHED)
            {
                if (Creature* geb = ObjectAccessor::GetCreature(*me, geblerGUID))
                {
                    geb->AI()->Talk(SAY_GEB_0);
                    me->SetFacingTo(me->GetAngle(geb->GetPositionX(), geb->GetPositionY()));
                    events.ScheduleEvent(EVENT_LIL_0, 5s);
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!eventInProgress)
                return;

            Creature* geb = ObjectAccessor::GetCreature(*me, geblerGUID);
            if (!geb)
                return;

            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_LIL_0:
                        Talk(SAY_LIL_1);
                        events.ScheduleEvent(EVENT_LIL_1, 4s);
                        break;
                    case EVENT_LIL_1:
                        geb->AI()->Talk(SAY_GEB_1);
                        events.ScheduleEvent(EVENT_LIL_2, 9s);
                        break;
                    case EVENT_LIL_2:
                        Talk(SAY_LIL_2);
                        events.ScheduleEvent(EVENT_LIL_3, 2s);
                        break;
                    case EVENT_LIL_3:
                        geb->AI()->Talk(SAY_GEB_2);
                        events.ScheduleEvent(EVENT_LIL_4, 7s);
                        break;
                    case EVENT_LIL_4:
                        geb->AI()->Talk(SAY_GEB_3);
                        geb->SetUInt32Value(UNIT_EMOTE_STATE, EMOTE_STATE_READY1H);
                        events.ScheduleEvent(EVENT_LIL_5, 4s);
                        break;
                    case EVENT_LIL_5:
                        DoCast(SPELL_LIL_SHADOWY_AURA);
                        events.ScheduleEvent(EVENT_LIL_6, 3s);
                        break;
                    case EVENT_LIL_6:
                        DoCast(geb, SPELL_LIL_SHADOW_HOP, true);
                        events.ScheduleEvent(EVENT_LIL_7, 4s);
                        break;
                    case EVENT_LIL_7:
                        me->SetUInt32Value(UNIT_EMOTE_STATE, EMOTE_STATE_COWER);
                        Talk(SAY_LIL_3);
                        events.ScheduleEvent(EVENT_LIL_8, 6s);
                        break;
                    case EVENT_LIL_8:
                        Talk(SAY_LIL_4);
                        if (Player* player = ObjectAccessor::GetPlayer(*me, playerGUID))
                            player->KilledMonsterCredit(me->GetEntry(), me->GetGUID());
                        Reset();
                        break;
                    default:
                        break;
                }
            }
        }

        bool GossipSelect(Player* player, uint32 menuId, uint32 /*gossipListId*/) override
        {
            if (menuId == LILIAN_GOSSIP_MENU_ID && !eventInProgress)
            {
                me->RemoveFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                Talk(SAY_LIL_0);
                eventInProgress = true;
                playerGUID = player->GetGUID();
                CloseGossipMenuFor(player);
                if (Creature* gebler = me->SummonCreature(NPC_SCARLET_LT_GEBLER, geblerSpawnPos))
                {
                    geblerGUID = gebler->GetGUID();
                    gebler->GetMotionMaster()->MovePath(NPC_SCARLET_LT_GEBLER * 10, false);
                }

                return true;
            }

            return false;
        }
    private:
        bool eventInProgress;
        ObjectGuid playerGUID;
        ObjectGuid geblerGUID;
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_lilian_voss_cage_AI(creature);
    }
};

class spell_lilian_shadow_hop : public SpellScriptLoader
{
public:
    spell_lilian_shadow_hop() : SpellScriptLoader("spell_lilian_shadow_hop") { }

    class spell_lilian_shadow_hop_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_lilian_shadow_hop_AuraScript);

        void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            if (Creature* geb = GetTarget()->ToCreature())
            {
                geb->DespawnOrUnsummon(10s);
                geb->CastSpell(geb, SPELL_LIL_SHADOWY_AURA, true);
                geb->KillSelf(false);
            }

            if (Creature* lil = GetCaster()->ToCreature())
            {
                float x, y, z, o;
                lil->GetRespawnPosition(x, y, z, &o);
                lil->GetMotionMaster()->MoveJump(x, y, z, o, 5.0f, 9.0f);
            }
        }

        void Register() override
        {
            AfterEffectRemove += AuraEffectRemoveFn(spell_lilian_shadow_hop_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_lilian_shadow_hop_AuraScript();
    }
};

class npc_captured_zaelot : public CreatureScript
{
public:
    npc_captured_zaelot() : CreatureScript("npc_captured_zaelot") { }

    struct npc_captured_zaelot_AI : public ScriptedAI
    {
        npc_captured_zaelot_AI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            events.Reset();
            me->RemoveAllAuras();
            me->SetUInt32Value(UNIT_EMOTE_STATE, 0);
            me->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER);
            me->LoadCreaturesAddon();
        }

        void QuestReward(Player* /*player*/, Quest const* quest, uint32 /*opt*/) override
        {
            if (quest->ID == QUEST_JOHAANS_EXPERIMENT)
            {
                if (guardGUID.IsEmpty())
                {
                    std::vector<Creature*> guardList;
                    me->GetCreatureListWithEntryInGrid(guardList, NPC_DEATHGUARD_SWALLON, 25.0f);

                    for (Creature* target : guardList)
                    {
                        guardGUID = target->GetGUID();
                        target->StopMoving();
                        target->SetOrientation(target->GetAngle(me->GetPositionX(), me->GetPositionX()));
                    }
                }

                me->SetUInt32Value(UNIT_EMOTE_STATE, 0);
                me->RemoveFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER);
                Talk(SAY_JOH_0);
                events.ScheduleEvent(EVENT_JOHAN_0, 4s);
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == WAYPOINT_MOTION_TYPE && pointId == POINT_ID_JOHAN_DEATH)
            {
                if (Creature* swallon = ObjectAccessor::GetCreature(*me, guardGUID))
                    swallon->GetMotionMaster()->MovePath(SWALLON_PATH_ID, true);
                DoCast(SPELL_PERM_FEIGN_DEATH);
                me->DespawnOrUnsummon(15s);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_JOHAN_0:
                        DoCast(me, SPELL_COSMETIC_DUST_CLOUD, true);
                        DoCast(me, SPELL_GHOUL_TRANSFORM, true);
                        events.ScheduleEvent(EVENT_JOHAN_1, 2s);
                        break;
                    case EVENT_JOHAN_1:
                        me->GetMotionMaster()->MovePath(me->GetEntry() * 10, false);
                        Talk(SAY_JOH_1);
                        break;
                }
            }
        }
    private:
        EventMap events;
        ObjectGuid guardGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_captured_zaelot_AI(creature);
    }
};

class npc_tirisfal_darkhounds : public CreatureScript
{
public:
    npc_tirisfal_darkhounds() : CreatureScript("npc_tirisfal_darkhounds") { }

    struct npc_tirisfal_darkhounds_AI : public ScriptedAI
    {
        npc_tirisfal_darkhounds_AI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            events.Reset();
        }

        void EnterCombat(Unit* /*who*/) override
        {
            events.ScheduleEvent(EVENT_HOUND_LEAPING_BITE, 2s);
            events.ScheduleEvent(EVENT_HOUND_INFILTRATOR, 11s);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_HOUND_LEAPING_BITE:
                        DoCastVictim(SPELL_LEAPING_BITE);
                        events.Repeat(12s);
                        break;
                    case EVENT_HOUND_INFILTRATOR:
                        // once per combat
                        if (!me->GetVictim()) // just in case?
                            break;

                        if (Player* player = me->GetVictim()->ToPlayer())
                            if (player->GetQuestStatus(QUEST_HAVE_YOU_SEEN) != QUEST_STATUS_REWARDED &&
                                player->GetQuestStatus(QUEST_HAVE_YOU_SEEN) != QUEST_STATUS_COMPLETE &&
                                player->GetQuestStatus(QUEST_ESCAPE_FROM_GILNEAS) != QUEST_STATUS_COMPLETE &&
                                player->GetQuestStatus(QUEST_ESCAPE_FROM_GILNEAS) != QUEST_STATUS_REWARDED)
                            {
                                if (Math::RollUnder(50))
                                    player->CastSpell(player, SPELL_SUMMON_WORGEN_INFIL, true);
                            }
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_tirisfal_darkhounds_AI(creature);
    }
};

class npc_worgen_infiltrator_tirisfal : public CreatureScript
{
public:
    npc_worgen_infiltrator_tirisfal() : CreatureScript("npc_worgen_infiltrator_tirisfal") { }

    struct npc_worgen_infiltrator_tirisfal_AI : public ScriptedAI
    {
        npc_worgen_infiltrator_tirisfal_AI(Creature* creature) : ScriptedAI(creature) { }

        void IsSummonedBy(Unit* who) override
        {
            if (who->GetTypeId() != TYPEID_PLAYER)
                return;

            me->SetFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_IMMUNE_TO_PC);
            playerGUID = who->GetGUID();
            Unit* attacker = nullptr;
            if (!who->GetAttackers().empty())
                attacker = *who->GetAttackers().begin();

            if (attacker)
            {
                attackerGUID = attacker->GetGUID();
                Position pos = attacker->GetNearPosition(1.0f, me->GetAngle(attacker->GetPositionX(), attacker->GetPositionY()));
                me->GetMotionMaster()->MovePoint(POINT_WORGEN_NEAR_SUMMONER, pos, true);
            }
            else
            {
                Position pos = who->GetNearPosition(1.0f, me->GetAngle(who->GetPositionX(), who->GetPositionY()));
                me->GetMotionMaster()->MovePoint(POINT_WORGEN_NEAR_SUMMONER, pos, true);
            }
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == POINT_MOTION_TYPE && pointId == POINT_WORGEN_NEAR_SUMMONER)
            {
                me->RemoveAllAuras(); // remove stealth and so on
                events.Reset();
                events.ScheduleEvent(EVENT_WORGEN_0, 1.5s);
                if (Creature* attacker = ObjectAccessor::GetCreature(*me, attackerGUID))
                    me->Kill(attacker);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_WORGEN_0:
                        if (Player* player = ObjectAccessor::GetPlayer(*me, playerGUID))
                            me->SetFacingTo(me->GetAngle(player->GetPositionX(), player->GetPositionY()));
                        Talk(SAY_WORGEN_INF);
                        events.ScheduleEvent(EVENT_WORGEN_1, 3s);
                        break;
                    case EVENT_WORGEN_1:
                        if (Player* player = ObjectAccessor::GetPlayer(*me, playerGUID))
                        {
                            // There is no spell in sniffs that adds this quest, so we have to do it this way
                            if (Quest const* quest = sObjectMgr->GetQuestTemplate(QUEST_ESCAPE_FROM_GILNEAS))
                            {
                                if (player->CanAddQuest(quest, true) && player->CanTakeQuest(quest, false))
                                {
                                    player->AddQuestAndCheckCompletion(quest, player);
                                    player->PlayerTalkClass->SendQuestGiverQuestDetails(quest, player->GetGUID(), true);
                                }
                            }
                        }
                        me->DespawnOrUnsummon(3s);
                        break;
                }
            }
        }
    private:
        EventMap events;
        ObjectGuid playerGUID;
        ObjectGuid attackerGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_worgen_infiltrator_tirisfal_AI(creature);
    }
};

class spell_tadpole_frightening_aura : public SpellScriptLoader
{
public:
    spell_tadpole_frightening_aura() : SpellScriptLoader("spell_tadpole_frightening_aura") { }

    class spell_tadpole_frightening_aura_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_tadpole_frightening_aura_AuraScript);

        void PeriodicTick(AuraEffect const* /*aurEff*/)
        {
            PreventDefaultAction();

            std::vector<Creature*> murlocs;
            GetCreatureListWithEntryInGrid(murlocs, GetCaster(), NPC_VILE_FIN_TADPOLE, 8.0f);
            murlocs.erase(std::remove_if(murlocs.begin(), murlocs.end(), Trinity::AnyDeadUnitCheck()), murlocs.end());
            if (murlocs.empty())
                return;

            GetCaster()->CastSpell(GetCaster(), SPELL_TAD_POLE_TRIGGERED);
        }

        void Register() override
        {
            OnEffectPeriodic += AuraEffectPeriodicFn(spell_tadpole_frightening_aura_AuraScript::PeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_tadpole_frightening_aura_AuraScript();
    }
};

class spell_tadpole_fright_triggered : public SpellScriptLoader
{
public:
    spell_tadpole_fright_triggered() : SpellScriptLoader("spell_tadpole_fright_triggered") { }

    class spell_tadpole_fright_triggered_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_tadpole_fright_triggered_AuraScript);

        void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            if (!GetTarget()->IsDead())
                GetTarget()->GetMotionMaster()->MoveTargetedHome();
        }

        void Register() override
        {
            AfterEffectRemove += AuraEffectRemoveFn(spell_tadpole_fright_triggered_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_FEAR, AURA_EFFECT_HANDLE_REAL);
        }
    };

    class spell_tadpole_fright_triggered_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_tadpole_fright_triggered_SpellScript);

        void HandleHit(SpellEffIndex effIndex)
        {
            if (Aura* aura = GetHitUnit()->GetAura(SPELL_TAD_POLE_TRIGGERED))
                if (aura->GetStackAmount() >= 3)
                    if (Creature* murloc = GetHitUnit()->ToCreature())
                        if (Player* player = GetCaster()->ToPlayer())
                        {
                            player->KilledMonsterCredit(murloc->GetEntry(), murloc->GetGUID());
                            murloc->DespawnOrUnsummon();
                        }
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_tadpole_fright_triggered_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_tadpole_fright_triggered_SpellScript();
    }

    AuraScript* GetAuraScript() const override
    {
        return new spell_tadpole_fright_triggered_AuraScript();
    }
};

class npc_lilian_voss_guardian : public CreatureScript
{
public:
    npc_lilian_voss_guardian() : CreatureScript("npc_lilian_voss_guardian") { }

    struct npc_lilian_voss_guardian_AI : public ScriptedAI
    {
        npc_lilian_voss_guardian_AI(Creature* creature) : ScriptedAI(creature)
        {
            eventInProgress = false;
        }

        void IsSummonedBy(Unit* summoner) override
        {
            summonerGUID = summoner->GetGUID();
            DoCast(me, SPELL_LIL_SHADOWY_AURA, true);
        }

        void EnterCombat(Unit* /*who*/) override
        {
            events.ScheduleEvent(EVENT_LIL_GUARDIAN_0, 3s); // shadow hop
        }

        void SpellHitTarget(Unit* target, SpellInfo const* spell) override
        {
            if (target->GetEntry() == NPC_SCARLET_NEOPHYTE || target->GetEntry() == NPC_SCARLET_VANGUARD)
            {
                if (spell->Id == SPELL_LIL_VOS_SHADOW_NOVA)
                    me->Kill(target);
            }
        }

        void DoAction(int32 param) override
        {
            if (param == ACTION_SHADOW_NOVA)
                events.ScheduleEvent(EVENT_LIL_GUARDIAN_1, 1.5s);
        }

        void MoveInLineOfSight(Unit* who) override
        {
            ScriptedAI::MoveInLineOfSight(who);

            if (who->GetEntry() == NPC_BENEDICTUS_VOSS && who->IsWithinDist(me, 20.0f) && !eventInProgress)
            {
                me->SetFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
                eventInProgress = true;
                events.Reset();
                events.ScheduleEvent(EVENT_LIL_GUARDIAN_2, 1s);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim() && !eventInProgress)
                return;

            events.Update(diff);

            while (auto eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_LIL_GUARDIAN_0:
                    {
                        Player* player = ObjectAccessor::GetPlayer(*me, summonerGUID);
                        if (!player)
                            break;

                        if ((player->GetAttackers().size() + me->GetAttackers().size()) >= 4)
                            DoCast(SPELL_LIL_VOS_DEATH_GRIP);
                        else
                            DoCastVictim(SPELL_LIL_VOS_HOP);

                        events.Repeat(7s);
                        break;
                    }
                    case EVENT_LIL_GUARDIAN_1:
                        me->CastSpell(nullptr, SPELL_LIL_VOS_SHADOW_NOVA, TRIGGERED_IGNORE_POWER_AND_REAGENT_COST);
                        break;
                    case EVENT_LIL_GUARDIAN_2:
                    {
                        std::vector<Creature*> creatureList;
                        Trinity::AnyUnitInObjectRangeCheck checker(me, 25.0f);
                        Trinity::CreatureListSearcher<Trinity::AnyUnitInObjectRangeCheck> searcher(me, creatureList, checker);
                        Cell::VisitGridObjects(me, searcher, 20.0f);

                        for (Creature* creature : creatureList)
                        {
                            switch (creature->GetEntry())
                            {
                                case NPC_BENEDICTUS_VOSS:
                                    fatherGUID = creature->GetGUID();
                                    creature->SetFlag(UNIT_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_NON_ATTACKABLE);
                                    break;
                                case NPC_CAPTAIN_MELRACHE:
                                    captainGUID = creature->GetGUID();
                                    creature->SetFaction(35);
                                    break;
                                case NPC_SCARLET_BODYGUARD:
                                    bodyguardGUID = creature->GetGUID();
                                    creature->SetFaction(35);
                                    break;
                                default:
                                    break;
                            }
                        }
                        Talk(SAY_LILIAN_VOSS_0);
                        me->GetMotionMaster()->MovePoint(1, lilianNearFather, true);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_3, 4s);
                        break;
                    }
                    case EVENT_LIL_GUARDIAN_3:
                        if (Creature* voss = ObjectAccessor::GetCreature(*me, fatherGUID))
                            voss->AI()->Talk(SAY_BENEDICTUS_0);

                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_4, 4s);
                        break;
                    case EVENT_LIL_GUARDIAN_4:
                        Talk(SAY_LILIAN_VOSS_1);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_5, 3s);
                        break;
                    case EVENT_LIL_GUARDIAN_5:
                        if (Creature* captain = ObjectAccessor::GetCreature(*me, captainGUID))
                            DoCast(captain, SPELL_LIL_VOS_HOP);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_6, 3s);
                        break;
                    case EVENT_LIL_GUARDIAN_6:
                        if (Creature* bodyguard = ObjectAccessor::GetCreature(*me, bodyguardGUID))
                            DoCast(bodyguard, SPELL_LIL_VOS_HOP);
                        Talk(SAY_LILIAN_VOSS_2);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_7, 4s);
                        break;
                    case EVENT_LIL_GUARDIAN_7:
                        if (Creature* voss = ObjectAccessor::GetCreature(*me, fatherGUID))
                            voss->AI()->Talk(SAY_BENEDICTUS_1);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_8, 2s);
                        break;
                    case EVENT_LIL_GUARDIAN_8:
                        Talk(SAY_LILIAN_VOSS_3);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_9, 5s);
                        break;
                    case EVENT_LIL_GUARDIAN_9:
                        if (Creature* voss = ObjectAccessor::GetCreature(*me, fatherGUID))
                            voss->AI()->Talk(SAY_BENEDICTUS_2);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_10, 3s);
                        break;
                    case EVENT_LIL_GUARDIAN_10:
                        DoCast(SPELL_LIL_VOS_SHA_UBER);
                        Talk(SAY_LILIAN_VOSS_4);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_11, 5s);
                        break;
                    case EVENT_LIL_GUARDIAN_11:
                        if (Creature* voss = ObjectAccessor::GetCreature(*me, fatherGUID))
                            DoCast(voss, SPELL_LIL_VOS_HOP);
                        events.ScheduleEvent(EVENT_LIL_GUARDIAN_12, 5s);
                        break;
                    case EVENT_LIL_GUARDIAN_12:
                        if (Player* player = ObjectAccessor::GetPlayer(*me, summonerGUID))
                            player->KilledMonsterCredit(NPC_BENEDICTUS_KC, ObjectGuid::Empty);
                        me->GetMotionMaster()->MovePoint(2, lilianExit, true);
                        me->DespawnOrUnsummon(6s);
                        break;
                    default:
                        break;
                }
            }
            DoMeleeAttackIfReady();
        }
    private:
        ObjectGuid summonerGUID;
        ObjectGuid fatherGUID;
        ObjectGuid bodyguardGUID;
        ObjectGuid captainGUID;
        EventMap events;
        bool eventInProgress;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_lilian_voss_guardian_AI(creature);
    }
};

class spell_lilian_voss_hop : public SpellScriptLoader
{
public:
    spell_lilian_voss_hop() : SpellScriptLoader("spell_lilian_voss_hop") { }

    class spell_lilian_voss_hop_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_lilian_voss_hop_AuraScript);

        void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            GetCaster()->CastSpell(GetTarget(), SPELL_LIL_VOS_BRAIN_DESTROY, true);
        }

        void Register() override
        {
            AfterEffectRemove += AuraEffectRemoveFn(spell_lilian_voss_hop_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_lilian_voss_hop_AuraScript();
    }
};

class LilVossDeathGripSelector
{
public:
    bool operator()(WorldObject* object) const
    {
        if (object->GetEntry() == NPC_SCARLET_NEOPHYTE)
            return false;

        if (object->GetEntry() == NPC_SCARLET_VANGUARD)
            return false;

        return true;
    }
};

class spell_lilian_death_grip : public SpellScriptLoader
{
public:
    spell_lilian_death_grip() : SpellScriptLoader("spell_lilian_death_grip") { }

    class spell_lilian_death_grip_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_lilian_death_grip_SpellScript);

        void FilterTargets(std::vector<WorldObject*>& targets)
        {
            targets.erase(std::remove_if(targets.begin(), targets.end(), LilVossDeathGripSelector()), targets.end());

            if (!targets.empty())
            {
                if (Creature* lil = GetCaster()->ToCreature())
                    lil->AI()->DoAction(ACTION_SHADOW_NOVA);
            }
        }

        void HandleHit(SpellEffIndex /*effIndex*/)
        {
            GetHitUnit()->CastSpell(GetCaster(), SPELL_DEATH_GRIP_LIL, true);
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_lilian_death_grip_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_DEST_AREA_ENEMY);
            OnEffectHitTarget += SpellEffectFn(spell_lilian_death_grip_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_lilian_death_grip_SpellScript();
    }
};

void AddSC_tirisfal_glades()
{
    // Creature scripts
    new npc_darnell_ud_first();
    new npc_darnell_ud_second();
    new npc_rotbrain_battle();
    new npc_marshal_redpath();
    new npc_sedrick_calston();
    new npc_lilian_voss_cage();
    new npc_captured_zaelot();
    new npc_tirisfal_darkhounds();
    new npc_worgen_infiltrator_tirisfal();
    new npc_lilian_voss_guardian();

    // Spell & Aura scripts
    new spell_undead_valkyr_res_73524();
    new spell_tirisfal_guardian_aura_91777();
    new spell_wakening_summoning_spells_q_24960();
    new spell_select_scarlet_corpse_91942();
    new spell_summon_darnell_91576_91938();
    new spell_eject_all_passengers_79737();
    new spell_murloc_leash_73108();
    new spell_lilian_shadow_hop();
    new spell_tadpole_frightening_aura();
    new spell_tadpole_fright_triggered();
    new spell_lilian_voss_hop();
    new spell_lilian_death_grip();

    // Areatrigger scripts
    new at_deathknell_starting_darnell_inside_chamber();
    new at_deathknell_starting_darnell_chamber();
    new at_deathknell_starting_darnell();
    new at_deathknell_near_mordo();
    new at_deathknell_near_saltain();
    new at_deathknell_gate_areatrigger();
}
