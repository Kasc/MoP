/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HOTFIXDATABASE_H
#define _HOTFIXDATABASE_H

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"

class HotfixDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
        HotfixDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) { }
        HotfixDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) { }

        //- Loads database type specific prepared statements
        void DoPrepareStatements() override;
};

typedef DatabaseWorkerPool<HotfixDatabaseConnection> HotfixDatabaseWorkerPool;

enum HotfixDatabaseStatements
{
    /*  Naming standard for defines:
        {DB}_{SEL/INS/UPD/DEL/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */

    HOTFIX_SEL_BATTLE_PET_ABILITY,
    HOTFIX_SEL_BATTLE_PET_ABILITY_EFFECT,
    HOTFIX_SEL_BATTLE_PET_ABILITY_STATE,
    HOTFIX_SEL_BATTLE_PET_ABILITY_TURN,
    HOTFIX_SEL_BATTLE_PET_BREED_QUALITY,
    HOTFIX_SEL_BATTLE_PET_BREED_STATE,
    HOTFIX_SEL_BATTLE_PET_EFFECT_PROPERTIES,
    HOTFIX_SEL_BATTLE_PET_SPECIES,
    HOTFIX_SEL_BATTLE_PET_SPECIES_STATE,
    HOTFIX_SEL_BATTLE_PET_SPECIES_XABILITY,
    HOTFIX_SEL_BATTLE_PET_STATE,
    HOTFIX_SEL_BATTLE_PET_VISUAL,

    HOTFIX_SEL_BROADCAST_TEXT,

    HOTFIX_SEL_CREATURE,
    HOTFIX_SEL_CREATURE_DIFFICULTY,

    HOTFIX_SEL_CURVE,
    HOTFIX_SEL_CURVE_POINT,

    HOTFIX_SEL_DEVICE_BLACKLIST,
    HOTFIX_SEL_DRIVER_BLACKLIST,

    HOTFIX_SEL_GAMEOBJECTS,

    HOTFIX_SEL_ITEM,
    HOTFIX_SEL_ITEM_CURRENCY_COST,
    HOTFIX_SEL_ITEM_EXTENDED_COST,
    HOTFIX_SEL_ITEM_SPARSE,
    HOTFIX_SEL_ITEM_BATTLE_PET,
    HOTFIX_SEL_ITEM_MOUNT_SPELL,
    HOTFIX_SEL_ITEM_UPGRADE,

    HOTFIX_SEL_KEY_CHAIN,

    HOTFIX_SEL_LOCALE,

    HOTFIX_SEL_LOCATION,

    HOTFIX_SEL_MAP_CHALLENGE_MODE,

    HOTFIX_SEL_MARKETING_PROMOTIONS_XLOCALE,

    HOTFIX_SEL_PATH,
    HOTFIX_SEL_PATH_NODE,
    HOTFIX_SEL_PATH_NODE_PROPERTY,
    HOTFIX_SEL_PATH_PROPERTY,

    HOTFIX_SEL_QUEST_PACKAGE_ITEM,

    HOTFIX_SEL_RULESET_ITEM_UPGRADE,
    HOTFIX_SEL_RULESET_RAID_LOOT_UPGRADE,

    HOTFIX_SEL_SCENE_SCRIPT,
    HOTFIX_SEL_SCENE_SCRIPT_PACKAGE,
    HOTFIX_SEL_SCENE_SCRIPT_PACKAGE_MEMBER,

    HOTFIX_SEL_SPELL_EFFECT_CAMERA_SHAKES,
    HOTFIX_SEL_SPELL_MISSILE,
    HOTFIX_SEL_SPELL_MISSILE_MOTION,
    HOTFIX_SEL_SPELL_REAGENTS,

    HOTFIX_SEL_SPELL_VISUAL,
    HOTFIX_SEL_SPELL_VISUAL_EFFECT_NAME,
    HOTFIX_SEL_SPELL_VISUAL_KIT,
    HOTFIX_SEL_SPELL_VISUAL_KIT_AREA_MODEL,
    HOTFIX_SEL_SPELL_VISUAL_KIT_MODEL_ATTACH,
    HOTFIX_SEL_SPELL_VISUAL_MISSILE,

    HOTFIX_SEL_VIGNETTE,

    MAX_HOTFIXDATABASE_STATEMENTS
};

#endif
