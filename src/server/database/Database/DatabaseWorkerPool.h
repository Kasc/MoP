/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DATABASEWORKERPOOL_H
#define _DATABASEWORKERPOOL_H

#include <ace/Thread_Mutex.h>
#include <ace/OS_NS_dirent.h>
#include <mysqld_error.h>

#include "Common.h"
#include "MySQLConnection.h"
#include "Transaction.h"
#include "DatabaseWorker.h"
#include "PreparedStatement.h"
#include "Log.h"
#include "QueryCallback.h"
#include "QueryResult.h"
#include "QueryHolder.h"
#include "AdhocStatement.h"

#include <memory>

#define MIN_MYSQL_SERVER_VERSION 50100u
#define MIN_MYSQL_CLIENT_VERSION 50100u

class PingOperation : public SQLOperation
{
    //! Operation for idle delaythreads
    bool Execute() override
    {
        m_conn->Ping();
        return true;
    }
};

template <class T>
class DatabaseWorkerPool
{
public:
    DatabaseWorkerPool()
        : m_isOpen( false )
        , m_messageQueue( 8 * 1024 * 1024, 8 * 1024 * 1024 )
        , m_queue( &m_messageQueue )
    {
        m_connections.resize( IDX_SIZE );

        WPFatal( mysql_thread_safe(), "Used MySQL library isn't thread-safe." );
        WPFatal( mysql_get_client_version() >= MIN_MYSQL_CLIENT_VERSION, "TrinityCore does not support MySQL versions below 5.1" );
    }

    bool Open( const std::string& infoString, uint8 async_threads, uint8 synch_threads, std::string& pathToBase )
    {
        m_isOpen = true;
        m_connectionInfo = std::make_unique< MySQLConnectionInfo >( infoString );

        TC_LOG_INFO( "sql.driver", "Opening DatabasePool '%s'. Asynchronous connections: %u, synchronous connections: %u.", GetDatabaseName(), async_threads, synch_threads );

        // apply sql updates only once!
        // and only for one synch connection!
        bool sqlUpdateAlreadyApplied = false;

        m_connections[ IDX_SYNCH ].reserve( synch_threads );
        for ( uint8 i = 0; i < synch_threads; ++i )
        {
            auto connection = std::make_unique< T >( *m_connectionInfo );

            bool result = connection->Open();
            if ( result )
            {
                if ( !sqlUpdateAlreadyApplied )
                {
                    sqlUpdateAlreadyApplied = true;
                    LoadSQLUpdate( connection.get(), pathToBase );
                }

                result = connection->PrepareStatements();
                if ( result )
                {
                    // only check mysql version if connection is valid
                    WPFatal( mysql_get_server_version( connection->GetHandle() ) >= MIN_MYSQL_SERVER_VERSION, "TrinityCore does not support MySQL versions below 5.1" );

                    m_connections[ IDX_SYNCH ].push_back( std::move( connection ) );
                }
            }

            m_isOpen &= result;
        }

        m_connections[ IDX_ASYNC ].reserve( async_threads );
        for ( uint8 i = 0; i < async_threads; ++i )
        {
            auto connection = std::make_unique< T >( &m_queue, *m_connectionInfo );

            bool result = connection->Open();

            if ( result )
            {
                result = connection->PrepareStatements();
                if ( result )
                    m_connections[ IDX_ASYNC ].push_back( std::move( connection ) );
            }

            m_isOpen &= result;
        }

        if ( m_isOpen )
            TC_LOG_INFO( "sql.driver", "DatabasePool '%s' opened successfully. %u total connections running.", GetDatabaseName(), ( m_connections[ IDX_SYNCH ].size() + m_connections[ IDX_ASYNC ].size() ) );
        else
            TC_LOG_ERROR( "sql.driver", "DatabasePool %s NOT opened. There were errors opening the MySQL connections. Check your SQLDriverLogFile for specific errors. Read wiki at http://collab.kpsn.org/display/tc/TrinityCore+Home", GetDatabaseName() );

        return m_isOpen;
    }

    bool IsOpen() const
    {
        return m_isOpen;
    }

    void Close()
    {
        if ( !m_isOpen )
            return;

        TC_LOG_INFO( "sql.driver", "Closing down DatabasePool '%s'.", GetDatabaseName() );

        m_queue.queue()->close();

        m_connections[ IDX_ASYNC ].clear();

        TC_LOG_INFO( "sql.driver", "Asynchronous connections on DatabasePool '%s' terminated. Proceeding with synchronous connections.", GetDatabaseName() );

        m_connections[ IDX_SYNCH ].clear();

        TC_LOG_INFO( "sql.driver", "All connections on DatabasePool '%s' closed.", GetDatabaseName() );
    }

    /**
        Delayed one-way statement methods.
    */

    //! Enqueues a one-way SQL operation in string format that will be executed asynchronously.
    //! This method should only be used for queries that are only executed once, e.g during startup.
    void Execute( const char* sql )
    {
        if ( !sql )
            return;

        BasicStatementTask* task = new BasicStatementTask( sql );
        Enqueue( task );
    }

    //! Enqueues a one-way SQL operation in string format -with variable args- that will be executed asynchronously.
    //! This method should only be used for queries that are only executed once, e.g during startup.
    void PExecute( const char* sql, ... )
    {
        if ( !sql )
            return;

        va_list ap;
        char szQuery[ MAX_QUERY_LEN ];
        va_start( ap, sql );
        vsnprintf( szQuery, MAX_QUERY_LEN, sql, ap );
        va_end( ap );

        Execute( szQuery );
    }

    //! Enqueues a one-way SQL operation in prepared statement format that will be executed asynchronously.
    //! Statement must be prepared with CONNECTION_ASYNC flag.
    void Execute( PreparedStatement* stmt )
    {
        PreparedStatementTask* task = new PreparedStatementTask( stmt );
        Enqueue( task );
    }

    /**
        Direct synchronous one-way statement methods.
    */

    //! Directly executes a one-way SQL operation in string format, that will block the calling thread until finished.
    //! This method should only be used for queries that are only executed once, e.g during startup.
    void DirectExecute( const char* sql, T* conn = nullptr )
    {
        if ( !sql )
            return;

        if ( !conn )
            conn = GetFreeConnection();

        conn->Execute( sql );
        conn->Unlock();
    }

    //! Directly executes a one-way SQL operation in string format -with variable args-, that will block the calling thread until finished.
    //! This method should only be used for queries that are only executed once, e.g during startup.
    void DirectPExecute( const char* sql, ... )
    {
        if ( !sql )
            return;

        va_list ap;
        char szQuery[ MAX_QUERY_LEN ];
        va_start( ap, sql );
        vsnprintf( szQuery, MAX_QUERY_LEN, sql, ap );
        va_end( ap );

        return DirectExecute( szQuery );
    }

    void DirectPExecute( T* conn, const char* sql, ... )
    {
        if ( !sql )
            return;

        va_list ap;
        char szQuery[ MAX_QUERY_LEN ];
        va_start( ap, sql );
        vsnprintf( szQuery, MAX_QUERY_LEN, sql, ap );
        va_end(ap);

        return DirectExecute(szQuery, conn);
    }

    //! Directly executes a one-way SQL operation in prepared statement format, that will block the calling thread until finished.
    //! Statement must be prepared with the CONNECTION_SYNCH flag.
    void DirectExecute( PreparedStatement* stmt )
    {
        T* t = GetFreeConnection();
        t->Execute( stmt );
        t->Unlock();

        //! Delete proxy-class. Not needed anymore
        delete stmt;
    }

    /**
        Synchronous query (with resultset) methods.
    */

    //! Directly executes an SQL query in string format that will block the calling thread until finished.
    //! Returns reference counted auto pointer, no need for manual memory management in upper level code.
    QueryResult Query( const char* sql, T* conn = NULL )
    {
        if ( !conn )
            conn = GetFreeConnection();

        ResultSet* result = conn->Query( sql );
        conn->Unlock();
        if ( !result || !result->GetRowCount() || !result->NextRow() )
        {
            delete result;
            return QueryResult( NULL );
        }

        return QueryResult( result );
    }

    //! Directly executes an SQL query in string format -with variable args- that will block the calling thread until finished.
    //! Returns reference counted auto pointer, no need for manual memory management in upper level code.
    QueryResult PQuery( const char* sql, T* conn, ... )
    {
        if ( !sql )
            return QueryResult( NULL );

        va_list ap;
        char szQuery[ MAX_QUERY_LEN ];
        va_start( ap, conn );
        vsnprintf( szQuery, MAX_QUERY_LEN, sql, ap );
        va_end( ap );

        return Query( szQuery, conn );
    }

    //! Directly executes an SQL query in string format -with variable args- that will block the calling thread until finished.
    //! Returns reference counted auto pointer, no need for manual memory management in upper level code.
    QueryResult PQuery( const char* sql, ... )
    {
        if ( !sql )
            return QueryResult( NULL );

        va_list ap;
        char szQuery[ MAX_QUERY_LEN ];
        va_start( ap, sql );
        vsnprintf( szQuery, MAX_QUERY_LEN, sql, ap );
        va_end( ap );

        return Query( szQuery );
    }

    //! Directly executes an SQL query in prepared format that will block the calling thread until finished.
    //! Returns reference counted auto pointer, no need for manual memory management in upper level code.
    //! Statement must be prepared with CONNECTION_SYNCH flag.
    PreparedQueryResult Query( PreparedStatement* stmt )
    {
        T* t = GetFreeConnection();

        PreparedResultSet* ret = t->Query( stmt );
        t->Unlock();

        //! Delete proxy-class. Not needed anymore
        delete stmt;

        if ( !ret || !ret->GetRowCount() )
        {
            delete ret;
            return PreparedQueryResult( NULL );
        }

        return PreparedQueryResult( ret );
    }

    /**
        Asynchronous query (with resultset) methods.
    */

    //! Enqueues a query in string format that will set the value of the QueryResultFuture return object as soon as the query is executed.
    //! The return value is then processed in ProcessQueryCallback methods.
    QueryCallback AsyncQuery( const char* sql )
    {
        BasicStatementTask* task = new BasicStatementTask( sql, true );
        auto result = task->GetFuture();

        Enqueue( task );

        return QueryCallback(std::move(result));
    }

    //! Enqueues a query in prepared format that will set the value of the PreparedQueryResultFuture return object as soon as the query is executed.
    //! The return value is then processed in ProcessQueryCallback methods.
    //! Statement must be prepared with CONNECTION_ASYNC flag.
    QueryCallback AsyncQuery( PreparedStatement* stmt )
    {
        PreparedStatementTask* task = new PreparedStatementTask( stmt, true );
        auto result = task->GetFuture();

        Enqueue( task );

        return QueryCallback(std::move(result));
    }

    //! Enqueues a vector of SQL operations (can be both adhoc and prepared) that will set the value of the QueryResultHolderFuture
    //! return object as soon as the query is executed.
    //! The return value is then processed in ProcessQueryCallback methods.
    //! Any prepared statements added to this holder need to be prepared with the CONNECTION_ASYNC flag.
    QueryResultHolderFuture DelayQueryHolder( SQLQueryHolder* holder )
    {
        SQLQueryHolderTask* task = new SQLQueryHolderTask( holder );
        auto result = task->GetFuture();

        Enqueue( task );
        return result;
    }

    /**
        Transaction context methods.
    */

    //! Begins an automanaged transaction pointer that will automatically rollback if not commited. (Autocommit=0)
    SQLTransaction BeginTransaction()
    {
        return SQLTransaction( new Transaction );
    }

    //! Enqueues a collection of one-way SQL operations (can be both adhoc and prepared). The order in which these operations
    //! were appended to the transaction will be respected during execution.
    void CommitTransaction( SQLTransaction transaction )
    {
#ifdef TRINITY_DEBUG
        //! Only analyze transaction weaknesses in Debug mode.
        //! Ideally we catch the faults in Debug mode and then correct them,
        //! so there's no need to waste these CPU cycles in Release mode.
        switch ( transaction->GetSize() )
        {
            case 0:
                TC_LOG_DEBUG( "sql.driver", "Transaction contains 0 queries. Not executing." );
                return;
            case 1:
                TC_LOG_DEBUG( "sql.driver", "Warning: Transaction only holds 1 query, consider removing Transaction context in code." );
                break;
            default:
                break;
        }
#endif // TRINITY_DEBUG

        Enqueue( new TransactionTask( transaction ) );
    }

    void DirectCommitTransaction( SQLTransaction& transaction )
    {
        T* conn = GetFreeConnection();

        int errorCode = conn->ExecuteTransaction( transaction );
        if ( !errorCode )
        {
            conn->Unlock();      // OK, operation succesful
            return;
        }

        if ( errorCode == ER_LOCK_DEADLOCK )
        {
            uint8 loopBreaker = 5;
            for ( uint8 i = 0; i < loopBreaker; ++i )
            {
                if ( !conn->ExecuteTransaction( transaction ) )
                    break;
            }
        }

        //! Clean up now.
        transaction->Cleanup();

        conn->Unlock();
    }

    //! Method used to execute prepared statements in a diverse context.
    //! Will be wrapped in a transaction if valid object is present, otherwise executed standalone.
    void ExecuteOrAppend( SQLTransaction & trans, PreparedStatement* stmt )
    {
        if ( trans.get() == nullptr )
        {
            Execute( stmt );
        }
        else
        {
            trans->Append( stmt );
        }
    }

    //! Method used to execute ad-hoc statements in a diverse context.
    //! Will be wrapped in a transaction if valid object is present, otherwise executed standalone.
    void ExecuteOrAppend( SQLTransaction & trans, const char* sql )
    {
        if ( trans.get() == nullptr )
        {
            Execute( sql );
        }
        else
        {
            trans->Append( sql );
        }
    }

    /**
        Other
    */

    //! Automanaged (internally) pointer to a prepared statement object for usage in upper level code.
    //! Pointer is deleted in this->DirectExecute(PreparedStatement*), this->Query(PreparedStatement*) or PreparedStatementTask::~PreparedStatementTask.
    //! This object is not tied to the prepared statement on the MySQL context yet until execution.
    PreparedStatement* GetPreparedStatement( uint32 index )
    {
        return new PreparedStatement( index );
    }

    //! Apply escape string'ing for current collation. (utf8)
    void EscapeString( std::string& str )
    {
        if ( str.empty() )
            return;

        char* buf = new char[ str.size() * 2 + 1 ];
        EscapeString( buf, str.c_str(), str.size() );
        str = buf;
        delete[] buf;
    }

    void EscapeString( std::string& str, T* conn )
    {
        if ( str.empty() )
            return;

        char* buf = new char[ str.size() * 2 + 1 ];
        EscapeString( buf, str.c_str(), str.size(), conn );
        str = buf;
        delete[] buf;
    }

    //! Keeps all our MySQL connections alive, prevent the server from disconnecting us.
    void KeepAlive()
    {
        for ( uint8 connectionIdx = 0; connectionIdx < m_connections[ IDX_SYNCH ].size(); ++connectionIdx )
        {
            auto & connection = m_connections[ IDX_SYNCH ][ connectionIdx ];
            if ( connection->LockIfReady() )
            {
                connection->Ping();
                connection->Unlock();
            }
        }

        //! Assuming all worker threads are free, every worker thread will receive 1 ping operation request
        //! If one or more worker threads are busy, the ping operations will not be split evenly, but this doesn't matter
        //! as the sole purpose is to prevent connections from idling.
        for ( size_t i = 0; i < m_connections[ IDX_ASYNC ].size(); ++i )
        {
            Enqueue( new PingOperation );
        }
    }

    void LoadSQLUpdate( T* connection, std::string const& pathToBase )
    {
        if ( pathToBase.empty() )
            return;

        // files to be applied
        std::vector< std::string > files;

        // already applied before (from db)
        std::set< std::string > alreadyAppliedFiles;

        // Get updates that were alraedy applied before
        if ( QueryResult result = Query( "SELECT `update` FROM `updates`", connection ) )
        {
            do
                alreadyAppliedFiles.insert( result->Fetch()[ 0 ].GetString() );
            while ( result->NextRow() );
        }

        // Record current working directory
        char cwd[ PATH_MAX ];
        ACE_OS::getcwd( cwd, PATH_MAX );

        // Change current directory to sql/updates/(path)
        if ( -1 == ACE_OS::chdir( pathToBase.c_str() ) )
            TC_LOG_FATAL( "sql.driver", "Can't change directory to %s: %s", pathToBase.c_str(), strerror( errno ) );

        // get files in sql/updates/(path)/ directory
        if ( ACE_DIR* dir = ACE_OS::opendir( pathToBase.c_str() ) )
        {
            while ( ACE_DIRENT* entry = ACE_OS::readdir( dir ) )
                // continue only if file is not already applied
                if ( alreadyAppliedFiles.find( entry->d_name ) == alreadyAppliedFiles.end() )
                    // make sure the file is an .sql one
                    if ( !strcmp( entry->d_name + strlen( entry->d_name ) - 4, ".sql" ) )
                        files.push_back( entry->d_name );

            ACE_OS::closedir( dir );
        }
        else
            TC_LOG_FATAL( "sql.driver", "Can't open %s: %s", pathToBase.c_str(), strerror( errno ) );

        // sort our files in ascending order
        std::sort( files.begin(), files.end() );

        // iterate not applied files now
        for ( size_t j = 0; j < files.size(); ++j )
        {
            if ( connection->ExecuteFile( files[ j ].c_str() ) )
            {
                EscapeString( files[ j ], connection );
                DirectPExecute( connection, "INSERT INTO `updates` VALUES ('%s', NOW())", files[ j ].c_str() );
            }
            else
                TC_LOG_FATAL( "sql.driver", "Failed to apply %s. See db_errors.log for more details.", files[ j ].c_str() );
        }

        // Return to original working directory
        if ( -1 == ACE_OS::chdir( cwd ) )
            TC_LOG_FATAL( "sql.driver", "Can't change directory to %s: %s", cwd, strerror( errno ) );
    }

private:
    unsigned long EscapeString( char *to, const char *from, unsigned long length, T* connection = nullptr )
    {
        if ( !to || !from || !length )
            return 0;

        if ( !connection )
            connection = m_connections[ IDX_SYNCH ][ 0 ].get();

        return mysql_real_escape_string( connection->GetHandle(), to, from, length );
    }

    void Enqueue( SQLOperation* op )
    {
        m_queue.enqueue( op );
    }

    //! Gets a free connection in the synchronous connection pool.
    //! Caller MUST call t->Unlock() after touching the MySQL context to prevent deadlocks.
    T* GetFreeConnection()
    {
        size_t connectionsCount = m_connections[ IDX_SYNCH ].size();

        //! Block forever until a connection is free
        for ( size_t connectionIdx = 0; ; ++connectionIdx )
        {
            auto & connection = m_connections[ IDX_SYNCH ][ connectionIdx % connectionsCount ];
            if ( connection->LockIfReady() )
                return connection.get();
        }

        return nullptr;
    }

    char const* GetDatabaseName() const
    {
        return m_connectionInfo->database.c_str();
    }

private:
    enum _internalIndex
    {
        IDX_ASYNC,
        IDX_SYNCH,
        IDX_SIZE
    };

    typedef std::unique_ptr< T >                DatabaseConnection;
    typedef std::vector< DatabaseConnection >   DatabaseConnectionsPool;

    ACE_Message_Queue<ACE_SYNCH>                     m_messageQueue;         //! Message Queue used by ACE_Activation_Queue
    ACE_Activation_Queue                             m_queue;                //! Queue shared by async worker threads.
    std::vector< DatabaseConnectionsPool >           m_connections;
    std::unique_ptr<MySQLConnectionInfo>             m_connectionInfo;
    bool                                             m_isOpen;
};

#endif
