/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AuthServer.hpp"

#include "MySQLThreading.h"
#include "SystemConfig.h"

#include "RealmList.h"

#include "Configuration/Config.hpp"
#include "openssl/crypto.h"
#include "ace/TP_Reactor.h"
#include "ace/Dev_Poll_Reactor.h"

#include <boost/asio/signal_set.hpp>
#include <chrono>

LoginDatabaseWorkerPool LoginDatabase;

namespace Tod
{
    AuthServer::~AuthServer()
    {
        StopDatabaseWorkers();
    }

    void AuthServer::Run()
    {
        PrintWelcomeInfo();

#if defined (ACE_HAS_EVENT_POLL) || defined (ACE_HAS_DEV_POLL)
        m_reactor = std::make_unique< ACE_Reactor >( new ACE_Dev_Poll_Reactor( ACE::max_handles(), 1 ), 1 );
#else
        m_reactor = std::make_unique< ACE_Reactor >( new ACE_TP_Reactor(), true );
#endif

        ACE_Reactor::instance( m_reactor.get(), false );

        TC_LOG_DEBUG( "server.authserver", "Max allowed open files is %d", ACE::max_handles() );

        if ( !CreatePidFile() )
            return;

        // Initialize the database connection
        if ( !StartDatabaseWorkers() )
            return;

        // Get the list of realms for the server
        int32 stateUpdateDelay = Tod::GetConfig().Get<int>( "RealmsStateUpdateDelay", 20 );
        sRealmList->Initialize( stateUpdateDelay );

        if ( sRealmList->size() == 0 )
        {
            TC_LOG_ERROR( "server.authserver", "No valid realms specified." );
            return;
        }

        if ( !SetupListenServer() )
            return;

        StartProcessLoop();
    }

    bool AuthServer::SetupListenServer()
    {
        int32 rmport = Tod::GetConfig().Get<int>( "RealmServerPort", 3724 );
        if ( rmport < 0 || rmport > 0xFFFF )
        {
            TC_LOG_ERROR( "server.authserver", "Specified port out of allowed range (1-65535)" );
            return false;
        }

        std::string bind_ip = Tod::GetConfig().Get<std::string>( "BindIP", "0.0.0.0" );

        ACE_INET_Addr bind_addr( uint16( rmport ), bind_ip.c_str() );
        if ( m_acceptor.open( bind_addr, m_reactor.get(), ACE_NONBLOCK ) == -1 )
        {
            TC_LOG_ERROR( "server.authserver", "Auth server can not bind to %s:%d", bind_ip.c_str(), rmport );
            return false;
        }

        return true;
    }

    bool AuthServer::CreatePidFile()
    {
        std::string pidFile = Tod::GetConfig().Get<std::string>( "PidFile", "" );
        if ( pidFile.empty() )
            return true;

        uint32 pid = CreatePIDFile( pidFile );
        if ( pid != 0 )
        {
            TC_LOG_INFO( "server.authserver", "Daemon PID: %u\n", pid );
            return true;
        }

        return false;
    }

    void AuthServer::PrintWelcomeInfo()
    {
        TC_LOG_INFO( "server.authserver", "%s (authserver)", _FULLVERSION );
        TC_LOG_INFO( "server.authserver", "<Ctrl-C> to stop.\n" );

        TC_LOG_INFO( "server.authserver", "  TTTTTTTTTTTTTTTTTTTTTTT                  DDDDDDDDDDDDD        " );
        TC_LOG_INFO( "server.authserver", "  T:::::::::::::::::::::T                  D::::::::::::DDD     " );
        TC_LOG_INFO( "server.authserver", "  T:::::::::::::::::::::T                  D:::::::::::::::DD   " );
        TC_LOG_INFO( "server.authserver", "  T:::::TT:::::::TT:::::T                  DDD:::::DDDDD:::::D  " );
        TC_LOG_INFO( "server.authserver", "  TTTTTT  T:::::T  TTTTT   ooooooooooo       D:::::D    D:::::D " );
        TC_LOG_INFO( "server.authserver", "          T:::::T        oo:::::::::::oo     D:::::D     D:::::D" );
        TC_LOG_INFO( "server.authserver", "          T:::::T       o:::::::::::::::o    D:::::D     D:::::D" );
        TC_LOG_INFO( "server.authserver", "          T:::::T       o:::::ooooo:::::o    D:::::D     D:::::D" );
        TC_LOG_INFO( "server.authserver", "          T:::::T       o::::o     o::::o    D:::::D     D:::::D" );
        TC_LOG_INFO( "server.authserver", "          T:::::T       o::::o     o::::o    D:::::D     D:::::D" );
        TC_LOG_INFO( "server.authserver", "          T:::::T       o::::o     o::::o    D:::::D     D:::::D" );
        TC_LOG_INFO( "server.authserver", "          T:::::T       o::::o     o::::o    D:::::D    D:::::D " );
        TC_LOG_INFO( "server.authserver", "        TT:::::::TT     o:::::ooooo:::::   DDD:::::DDDDD:::::D  " );
        TC_LOG_INFO( "server.authserver", "        T:::::::::T     o:::::::::::::::   D:::::::::::::::DD   " );
        TC_LOG_INFO( "server.authserver", "        T:::::::::T      oo:::::::::::oo   D::::::::::::DDD     " );
        TC_LOG_INFO( "server.authserver", "        TTTTTTTTTTT        ooooooooooo     DDDDDDDDDDDDD        " );
        TC_LOG_INFO( "server.authserver", "                                                          " );
        TC_LOG_INFO( "server.authserver", "            Project Theatre of Dreams 2015(c)             " );
        TC_LOG_INFO( "server.authserver", "       Based on <http://www.projectskyfire.org/> \n       " );

        const std::string& configFile = Tod::GetConfig().GetFilename();
        TC_LOG_INFO( "server.authserver", "Using configuration file %s.", configFile.c_str() );

        TC_LOG_INFO( "server.authserver", "%s (Library: %s)", OPENSSL_VERSION_TEXT, SSLeay_version( SSLEAY_VERSION ) );
    }

    bool AuthServer::StartDatabaseWorkers()
    {
        MySQL::Library_Init();

        std::string sqlUpdatesPath = Tod::GetConfig().Get<std::string>( "DatabaseUpdater.PathToUpdates", "" );
        if ( !sqlUpdatesPath.size() || ( *sqlUpdatesPath.rbegin() != '\\' && *sqlUpdatesPath.rbegin() != '/' ) )
#if PLATFORM == PLATFORM_WINDOWS
            sqlUpdatesPath += '\\';
#else
            sqlUpdatesPath += '/';
#endif

        std::string pathToBase = "";
        if ( Tod::GetConfig().Get<bool>( "DatabaseUpdater.Enabled", false ) )
            pathToBase = sqlUpdatesPath + "auth";

        std::string dbString = Tod::GetConfig().Get<std::string>( "LoginDatabaseInfo", "" );
        if ( dbString.empty() )
        {
            TC_LOG_ERROR( "server.authserver", "Database not specified" );
            return false;
        }

        int32 asyncThreads = Tod::GetConfig().Get<int>( "LoginDatabase.WorkerThreads", 1 );
        if ( asyncThreads < 1 || asyncThreads > 32 )
        {
            TC_LOG_ERROR( "server.authserver", "Improper value specified for LoginDatabase.WorkerThreads, defaulting to 1." );
            asyncThreads = 1;
        }

        int32 synchThreads = Tod::GetConfig().Get<int>( "LoginDatabase.SynchThreads", 1 );
        if ( synchThreads < 1 || synchThreads > 32 )
        {
            TC_LOG_ERROR( "server.authserver", "Improper value specified for LoginDatabase.SynchThreads, defaulting to 1." );
            synchThreads = 1;
        }

        if ( !LoginDatabase.Open( dbString, uint8( asyncThreads ), uint8( synchThreads ), pathToBase ) )
        {
            TC_LOG_ERROR( "server.authserver", "Cannot connect to Authserver database %s", dbString.c_str() );
            return false;
        }

        TC_LOG_INFO( "server.authserver", "Started auth database connection pool." );
        sLog->SetRealmId( 0 );
        return true;
    }

    void AuthServer::StopDatabaseWorkers()
    {
        LoginDatabase.Close();

        MySQL::Library_End();
    }

    void AuthServer::SetProcessAffinity()
    {
#if defined(_WIN32) || defined(__linux__)

        ///- Handle affinity for multiple processors and process priority
        uint32 affinity = Tod::GetConfig().Get<int>( "UseProcessors", 0 );
        bool highPriority = Tod::GetConfig().Get<bool>( "ProcessPriority", false );

#ifdef _WIN32 // Windows

        HANDLE hProcess = GetCurrentProcess();
        if ( affinity > 0 )
        {
            ULONG_PTR appAff;
            ULONG_PTR sysAff;

            if ( GetProcessAffinityMask( hProcess, &appAff, &sysAff ) )
            {
                // remove non accessible processors
                ULONG_PTR currentAffinity = affinity & appAff;

                if ( !currentAffinity )
                    TC_LOG_ERROR( "server.authserver", "Processors marked in UseProcessors bitmask (hex) %x are not accessible for the authserver. Accessible processors bitmask (hex): %x", affinity, appAff );
                else if ( SetProcessAffinityMask( hProcess, currentAffinity ) )
                    TC_LOG_INFO( "server.authserver", "Using processors (bitmask, hex): %x", currentAffinity );
                else
                    TC_LOG_ERROR( "server.authserver", "Can't set used processors (hex): %x", currentAffinity );
            }
        }

        if ( highPriority )
        {
            if ( SetPriorityClass( hProcess, HIGH_PRIORITY_CLASS ) )
            {
                TC_LOG_INFO( "server.authserver", "authserver process priority class set to HIGH" );
            }
            else
            {
                TC_LOG_ERROR( "server.authserver", "Can't set authserver process priority class." );
            }
        }

#else // Linux

        if ( affinity > 0 )
        {
            cpu_set_t mask;
            CPU_ZERO( &mask );

            for ( unsigned int i = 0; i < sizeof( affinity ) * 8; ++i )
                if ( affinity & ( 1 << i ) )
                    CPU_SET( i, &mask );

            if ( sched_setaffinity( 0, sizeof( mask ), &mask ) )
                TC_LOG_ERROR( "server.authserver", "Can't set used processors (hex): %x, error: %s", affinity, strerror( errno ) );
            else
            {
                CPU_ZERO( &mask );
                sched_getaffinity( 0, sizeof( mask ), &mask );
                TC_LOG_INFO( "server.authserver", "Using processors (bitmask, hex): %lx", *( __cpu_mask* )( &mask ) );
            }
        }

        if ( highPriority )
        {
            if ( setpriority( PRIO_PROCESS, 0, PROCESS_HIGH_PRIORITY ) )
                TC_LOG_ERROR( "server.authserver", "Can't set authserver process priority class, error: %s", strerror( errno ) );
            else
                TC_LOG_INFO( "server.authserver", "authserver process priority class set to %i", getpriority( PRIO_PROCESS, 0 ) );
        }

#endif
#endif
    }

    void AuthServer::StartProcessLoop()
    {
        SetProcessAffinity();

        bool stopInstance = false;

        boost::asio::signal_set signals( m_ios, SIGINT, SIGTERM );
        signals.async_wait( [ &stopInstance ]( boost::system::error_code error, int sigNo )
        {
            stopInstance = true;

            TC_LOG_INFO( "server.authserver", "Signal: %d received. Terminating ....", sigNo );
        } );

        // maximum counter for next ping
        std::chrono::duration<double> pingDelay( ( double )Tod::GetConfig().Get<int>( "MaxPingTime", 30 ) * MINUTE );

        std::chrono::time_point< std::chrono::system_clock > timerStart;
        timerStart = std::chrono::system_clock::now();

        while ( !stopInstance )
        {
            m_ios.poll();

            ACE_Time_Value interval( 0, 100000 );
            if ( m_reactor->run_reactor_event_loop( interval ) == -1 )
                break;

            auto timerEnd = std::chrono::system_clock::now();

            std::chrono::duration<double> diff( timerEnd - timerStart );
            if ( diff.count() > pingDelay.count() )
            {
                timerStart = timerEnd;

                TC_LOG_INFO( "server.authserver", "Ping MySQL to keep connection alive" );
                LoginDatabase.KeepAlive();
            }
        }
    }
}
