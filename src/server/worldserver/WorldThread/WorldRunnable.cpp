/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "ObjectAccessor.h"
#include "World.h"
#include "WorldSocketMgr.h"
#include "Database/DatabaseEnv.h"
#include "ScriptMgr.h"
#include "BattlegroundMgr.h"
#include "MapManager.h"
#include "Timer.h"
#include "WorldRunnable.h"
#include "OutdoorPvPMgr.h"
#include "InstanceSaveMgr.h"

#define WORLD_SLEEP_CONST 50

#ifdef _WIN32
#include "ServiceWin32.h"
extern int m_ServiceStatus;
#endif

#include <boost/asio/signal_set.hpp>

void WorldRunnable::Run()
{
#ifdef _WIN32
    boost::asio::signal_set signals( m_ioService, SIGINT, SIGTERM, SIGBREAK );
#else
    boost::asio::signal_set signals( m_ioService, SIGINT, SIGTERM );
#endif

    signals.async_wait( []( boost::system::error_code error, int sigNo )
    {
        switch ( sigNo )
        {
            case SIGINT:
                World::StopNow( RESTART_EXIT_CODE );
                break;
            case SIGTERM:
#ifdef _WIN32
            case SIGBREAK:
                if ( m_ServiceStatus != 1 )
#endif
                    World::StopNow( SHUTDOWN_EXIT_CODE );
                break;
            default:
                return;
        }

        TC_LOG_INFO( "server.worldserver", "Signal: %d received. Terminating ....", sigNo );
    } );

    uint32 realCurrTime = 0;
    uint32 realPrevTime = getMSTime();

    sScriptMgr->OnStartup();

    ///- While we have not World::m_stopEvent, update the world
    while ( !World::IsStopped() && !m_stopRequested )
    {
        ++World::m_worldLoopCounter;
        realCurrTime = getMSTime();

        uint32 diff = getMSTimeDiff( realPrevTime, realCurrTime );

        sWorld->Update( diff );
        realPrevTime = realCurrTime;

        uint32 executionTimeDiff = getMSTimeDiff(realCurrTime, getMSTime());
        // we know exactly how long it took to update the world, if the update took less than WORLD_SLEEP_CONST, sleep for WORLD_SLEEP_CONST - world update time
        if (executionTimeDiff < WORLD_SLEEP_CONST)
            std::this_thread::sleep_for(std::chrono::milliseconds(WORLD_SLEEP_CONST - executionTimeDiff));

#ifdef _WIN32
        if ( m_ServiceStatus == 0 )
        {
            World::StopNow( SHUTDOWN_EXIT_CODE );
        }

        while ( m_ServiceStatus == 2 )
        {
            std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
        }
#endif

        m_ioService.poll();
    }

    sScriptMgr->OnShutdown();

    sWorld->KickAll();                                       // save and kick all players
    sWorld->UpdateSessions( 1 );                             // real players unload required UpdateSessions call

    // unload battleground templates before different singletons destroyed
    sBattlegroundMgr->DeleteAllBattlegrounds();

    sInstanceSaveMgr->Unload();

    sWorldSocketMgr->StopNetwork();

    sOutdoorPvPMgr->Die();                    // unload it before MapManager

    sMapMgr->UnloadAll();                     // unload all grids (including locked in memory)
    sScriptMgr->Unload();
}

void WorldRunnable::Wait()
{
    m_thread.join();
}
