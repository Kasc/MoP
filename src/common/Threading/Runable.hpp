/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOD_THREADING_RUNNABLE_HPP
#define TOD_THREADING_RUNNABLE_HPP

#include <thread>
#include <boost/asio/io_service.hpp>
#include <atomic>

namespace Tod
{
    namespace Threading
    {
        class Runnable
        {
        public:
            void                    Start();
            void                    Stop();

        protected:
            virtual void            Run() = 0;

            std::atomic< bool >     m_stopRequested;
            boost::asio::io_service m_ioService;
            std::thread             m_thread;
        };
    }
}

#endif /* TOD_THREADING_RUNNABLE_HPP */
