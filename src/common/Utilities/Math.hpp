/*
* Copyright (C) 2016-2016 Project SkyFire <http://www.projectskyfire.org/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MATH_H__
#define _MATH_H__

#include "Define.h"
#include "Duration.h"

#include <random>
#include <type_traits>
#include <chrono>

#ifdef WIN32
namespace std
{
    //! GO AROUND STD ISSUE, where uint8 can't be used as param to uniform_int_distribution
    template<>
    struct _Is_IntType< uint8 > : true_type
    {
    };
}
#endif

template <typename T, typename = typename std::is_enum<T>::type>
struct safe_underlying_type {
    using type = void;
};

template <typename T>
struct safe_underlying_type<T, std::true_type> {
    using type = std::underlying_type_t<T>; 
};

namespace Math
{
    std::mt19937 & GetGenerator();

    namespace Detail
    {
        //! handle int values
        template< typename T, std::enable_if_t< std::is_integral< T >::value >* = nullptr >
        inline T GenerateNumber( T min, T max )
        {
            return std::uniform_int_distribution< T >( min, max )( GetGenerator() );
        }

        //! handle floating point
        template< typename T, std::enable_if_t< std::is_floating_point< T >::value >* = nullptr >
        inline T GenerateNumber( T min, T max )
        {
            return std::uniform_real_distribution< T >( min, max )( GetGenerator() );
        }

        //! handle chrono duration
        template< typename T, std::enable_if_t< std::is_chrono_duration< T >::value >* = nullptr >
        inline T GenerateNumber( T min, T max )
        {
            return T( GenerateNumber( min.count(), max.count() ) );
        }

        //! unpack enum value
        template< typename T, std::enable_if_t< std::is_enum<T>::value >* = nullptr, typename U = typename safe_underlying_type< T >::type >
        inline U GenerateNumber( T min, T max )
        {
            return GenerateNumber( static_cast< U >( min ), static_cast< U >( max ) );
        }
    }

    template< typename T, typename U >
    inline auto Rand( T min, U max )
    {
        using R = std::common_type_t< T, U >;
        return Detail::GenerateNumber( static_cast< R >( min ), static_cast< R >( max ) );
    }

    //! return value in range 0 - 100
    template< typename T = float >
    inline auto RandPct()
    {
        return Rand( static_cast< T >( 0 ), static_cast< T >( 100 ) );
    }

    // true if roll is lower than specified value ( value in range 0-100 )
    template< typename T = float >
    inline bool RollUnder( T limes )
    {
        //! required for enums, which are unpacked in Rand
        using R = decltype( RandPct< T >() );
        return static_cast< R >( limes ) > RandPct< T >();
    }

    // true if roll is greater than specified value ( value in range 0-100 )
    template< typename T = float >
    inline bool RollOver( T limes )
    {
        //! required for enums, which are unpacked in Rand
        using R = decltype( RandPct< T >() );
        return static_cast< R >( limes ) < RandPct< T >();
    }

    inline uint32 WeightedRand(size_t count, double const* chances)
    {
        std::discrete_distribution<uint32> dd(chances, chances + count);
        return dd(GetGenerator());
    }
}

inline void ApplyPercentModFloatVar( float& var, float val, bool apply )
{
    if ( val == -100.0f )     // prevent set var to zero
        val = -99.99f;
    var *= ( apply ? ( 100.0f + val ) / 100.0f : 100.0f / ( 100.0f + val ) );
}

// Percentage calculation
template <class T, class U>
inline T CalculatePct( T base, U pct )
{
    return T( base * static_cast< float >( pct ) / 100.0f );
}

template <class T, class U>
inline T AddPct( T &base, U pct )
{
    return base += CalculatePct( base, pct );
}

template <class T, class U>
inline T ApplyPct( T &base, U pct )
{
    return base = CalculatePct( base, pct );
}

template <class T>
inline T RoundToInterval( T& num, T floor, T ceil )
{
    return num = std::min( std::max( num, floor ), ceil );
}

#endif
