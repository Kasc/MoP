/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DURATION_H_
#define _DURATION_H_

#include <chrono>

namespace std
{
    //!add type trait, so we can determine if arg is chrono::duration
    template< typename T >
    struct is_chrono_duration : false_type
    {
    };

    template< typename Rep, typename Period >
    struct is_chrono_duration< chrono::duration< Rep, Period > > : true_type
    {
    };
}

/// Milliseconds shorthand typedef.
typedef std::chrono::milliseconds Milliseconds;

/// Seconds shorthand typedef.
typedef std::chrono::seconds Seconds;

/// Minutes shorthand typedef.
typedef std::chrono::minutes Minutes;

/// Hours shorthand typedef.
typedef std::chrono::hours Hours;

/// Allow to use chrono_literals: s, ms, h, m, EXAMPLE: Milliseconds( 50 ) == 50ms, Seconds( 2 ) == 2s, Minutes( 33 ) == 33m, Hours( 1 ) == 1h
using namespace std::chrono_literals;

#endif // _DURATION_H_
